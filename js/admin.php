<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon" />
	<meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
    <?php if(!Yii::app()->user->isAdmin()):?>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/responsive-full-background-image.css" rel="stylesheet">
    <?php endif;?>

	<!-- radio button css-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/yellow.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css">
    <!-- Morris Charts CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery1_11.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php
    $cs=Yii::app()->clientScript;

    $cs->scriptMap=array(
        'jquery.js'=>false,
        'jquery.min.js'=>false,
    );?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/icheck.js"></script>
    <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.form.js"></script>


    
    
    
    
    <!-- Morris Charts JavaScript -->
    <!--<script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris-data.js"></script>-->

</head>

<body>
<?php if(Yii::app()->user->isAdmin()):?>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo Yii::app()->createUrl('administration')?>" class="navbar-brand" >
           <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/>
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
<?php if(Yii::app()->user->isAdmin()):
        $this->widget('zii.widgets.CMenu',array(
        'items'=>array(
            array('label'=>'Home', 'url'=>array('/administration')),
            array('label'=>'Students', 'url'=>array('/user/admin/studentlist'), 'visible'=>UserModule::isAllowedStudent()),
            array('label'=>'Parents', 'url'=>array('/user/admin/parentlist'), 'visible'=>UserModule::isAllowedStudent()),
            array('label'=>'Admins', 'url'=>array('/user/admin/adminlist'), 'visible'=>UserModule::isSuperAdmin()),            
            array('label'=>'Plan', 'url'=>array('/plan/admin'), 'visible'=>UserModule::isSuperAdmin()),
            array('label'=>'Coupons', 'url'=>array('/coupons/admin'), 'visible'=>UserModule::isSuperAdmin()),
            array('label'=>'Questions', 'url'=>array('/questions/admin'), 'visible'=>UserModule::isAllowedQuestion()),
            array('label'=>'Mock', 'url'=>array('/mock/admin'), 'visible'=>UserModule::isAllowedQuestion()),
            array('label'=>'Masters','url'=>'javascript:void(0)',
                    'linkOptions'=>array('class'=>'dropdown-toggle js-activated','data-toggle'=>'dropdown','aria-expanded'=>'true'),
                    'items'=>array(
                                    array('label'=>'Publishers', 'url'=>array('/publishers/admin')),
                                    array('label'=>'Syllabus', 'url'=>array('/syllabus/admin')),
                                    array('label'=>'Standard', 'url'=>array('/standard/admin')),
//                                    array('label'=>'Subject', 'url'=>array('/subject/admin')),
                                    array('label'=>'Chapter', 'url'=>array('/chapter/admin')),
                                    array('label'=>'Topic', 'url'=>array('/topic/admin')),
                                    array('label'=>'Teachers', 'url'=>array('/teachers/admin')),
                                    array('label'=>'Levels', 'url'=>array('/levels/admin')),
                                    array('label'=>'Question Status', 'url'=>array('/questionstatusmaster/admin')),
//                                    array('label'=>'Question Types', 'url'=>array('/types/admin')),
                    )
                    ,'itemOptions'=>array('class'=>'dropdown'), 'visible'=>UserModule::isSuperAdmin()),
            array('label'=>'Report','url'=>'javascript:void(0)',
                    'linkOptions'=>array('class'=>'dropdown-toggle js-activated','data-toggle'=>'dropdown','aria-expanded'=>'true'),
                    'items'=>array(
                                    array('label'=>'Subscription', 'url'=>array('/plan/report')),
                                    array('label'=>'Progress', 'url'=>array('/student/Studentprogressreport')),//                                  
                    )
                    ,'itemOptions'=>array('class'=>'dropdown'), 'visible'=>UserModule::isSuperAdmin()),                    
            array('label'=>'Logout', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
        ),
        'htmlOptions'=>array('class'=>'nav navbar-right top-nav'),
        'submenuHtmlOptions' => array(
            'class' => 'dropdown-menu',
        ),
    ));
    endif;?>
        </div>
        <?php if(Yii::app()->user->isAdmin()):
            $this->widget('zii.widgets.CMenu', array(
                'items'=>$this->menu,
                'firstItemCssClass'=>'nav-header',
                'activeCssClass'=>'active',
                'htmlOptions'=>array('class'=>'nav navbar-nav side-nav'),
            ));
        endif;
        ?>
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div>
<?php else:?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
        </div>
<?php endif;?>
</body>
</html>

<!--<script>
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_flat-yellow',
    radioClass: 'iradio_flat-yellow'
  });
});
</script>-->


