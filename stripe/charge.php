<?php
 error_reporting( E_ALL );
  require_once('./config.php');

  $token  = $_POST['stripeToken'];

	try {
	   $amount=($_POST['amount']*100);
	  $charge = \Stripe\Charge::create(array(
		"amount" => $amount, // amount in cents, again
		"currency" => $_POST['currency_code'],
		"source" => $token,
		"description" => $_POST['item_name'],
		"receipt_email"=>$_POST['email']
		));
        if($_POST['source']=='parent'):
		  header( 'Location:../parent/Paymentcompletestripe?supredent='.$_POST['idst'].'&application='.base64_encode($_POST['plan']).'&setup='.base64_encode('Completed').'&transaction='.$charge->id.'&voucher='.base64_encode($_POST['voucherid']));
        else:
            header( 'Location:../user/registration/Paymentcompletestripe?supredent='.$_POST['idst'].'&application='.base64_encode($_POST['plan']).'&setup='.base64_encode('Completed').'&transaction='.$charge->id.'&voucher='.base64_encode($_POST['voucherid']));
        endif;            
                    		
	} catch(\Stripe\Error\Card $e) {
        if($_POST['source']=='parent'):
            header( 'Location:../parent/Paymentcompletestripe?cancel=yes&message='.urldecode($e->getMessage()));
        else:
            header( 'Location:../user/registration/Paymentcompletestripe?cancel=yes&message='.urldecode($e->getMessage()));
        endif;	   
		        		

	}
?>