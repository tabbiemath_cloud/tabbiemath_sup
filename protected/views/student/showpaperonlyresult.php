
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="contentarea">
        <h1 class="h125"> </h1>


        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">Here is your test result</h1>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div style="text-align: right;">
                        View Solution <img id="viewsolution" data-toggle="modal" data-target="#solutionModel" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" style="margin-left:10px;cursor: pointer">

                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table" style="background-color: #ffffff;">
                        <tbody class="font3 wirisfont">
                        <?php echo $testresult;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="solutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>
            </div>
            <div class="panel-heading">
                <div style="text-align: right;">
                    <span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" target="_blank"><i class="fa fa-print fa-lg"></i></a>

                </div>
            </div>
            <div class="modal-body" >
                <table class="table" style="background-color: #ffffff;">
                    <thead>

                    </thead>
                    <tbody class="font3 wirisfont" id="showsolution">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php Yii::app()->clientScript->registerScript('showmarks', "
    $('#viewsolution').click(function(){
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/GetSolution')."',
          data: { testid: '".$test->TM_STU_TT_Id."'}
        })
          .done(function( html ) {
             $('#showsolution').html( html );
          });
    });
		$('.pie_progress').asPieProgress({
            namespace: 'pie_progress'
        });
		$('.pie_progress').asPieProgress('go');       

");?>

<?php Yii::app()->clientScript->registerScript('sendmail', "
    $(document).on('click','.showSolutionItem',function(){
            var item= $(this).attr('data-reff');
            $('.sendmail').slideUp('fast');
            $('.suggestiontest'+item).slideDown('slow');



    });

    $(document).on('click','#mailSend',function(){
var questionReff=$(this).data('reff');
var questionId=$(this).data('id');
var comments= $('#comment'+questionId).val();
comments=comments.trim();
if(comments.length>0){
    $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {
$('.mailSendComplete'+questionId).slideDown();
setTimeout(function(){
$('.mailSendComplete'+questionId).hide();
$('.sendmail').slideUp();
 $('.comment'+questionId).val('');
}, 6000);
 })
}
   else{
$('.comment'+questionId).css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
    $('.comment'+questionId).val('');
  $('.comment'+questionId).attr('placeholder', 'Please enter your comments before clicking send');

setTimeout(function() {
                    $('.comment'+questionId).css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
               $('.comment'+questionId).attr('placeholder', 'Enter your comments here');
                }, 5000);
                return false;

                return false;

}

    });
");?>
<?php Yii::app()->clientScript->registerScript('sendmailtest', "
    $(document).on('click','.showSolutionItemtest',function(){
            var item= $(this).attr('data-reff');
            $('.sendmailtest').slideUp('fast');
            $('.suggestion'+item).slideDown('slow')


    });

    $(document).on('click','#mailSendtest',function(){
    var questionId=$(this).data('id');
    var comments= $('#comments'+questionId).val();
    comments=comments.trim();
    if(comments.length>0){

        var count=0;
    
        var questionReff=$(this).data('reff');
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/SendMail')."',
          data: { questionId: questionId,questionReff: questionReff,comments: comments}
    
        })
        .done  (function(data, textStatus, jqXHR)        {
        $('.mailSendCompletetest'+questionId).slideDown();
        setTimeout(function(){
        $('.mailSendCompletetest'+questionId).hide();
        $('.sendmailtest').slideUp();
        var comments= $('.comments'+questionId).val('');
        }, 6000);
         })
    }
else{
    $('.comments'+questionId).css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
    $('.comments'+questionId).attr('placeholder', 'Please enter your comments before clicking send');
    $('.comments'+questionId).val('');    
    setTimeout(function() {
    $('.comments'+questionId).css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
    $('.comments'+questionId).attr('placeholder', 'Enter your comments here');
    }, 5000);
    return false;
}
    });
");
?>
<?php Yii::app()->clientScript->registerScript('togglestar', "
$(document).on('click','.startoggle',function(){

if ( $(this).find('span.glyphicon').hasClass('sty')) {

var deleteflag=0;
}
else{
var deleteflag=1


}
var displaystar=$(this);


  var questionId=$(this).attr('data-questionId');
  var questionType=$(this).attr('data-questionTypeId');
  var testId=$(this).attr('data-testId');

                $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/redo')."',
              data: { questonid: questionId, type: questionType, testid: testId, deleteflag: deleteflag },
              success:function(data)
              {


                    displaystar.find('span.glyphicon').toggleClass('sty');

              }


            });



});");?>
<script>        
       function DisableBackButton() {
            window.history.forward()
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
        window.onunload = function () { void (0) }
</script>