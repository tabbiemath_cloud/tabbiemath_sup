
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
    <!-- new design -->
        <div class="bs-example">
			<h1 class="h125">Your weekly status</h1>
            <div class="panel panel-default">
                    <table class="rwd-tables">                    
                        <tr>
                            <th></th>
                            <th>This Week</th>
                            <th>Total</th>                            
                        </tr>
                        <?php  if (Yii::app()->session['quick'] || Yii::app()->session['difficulty']):?>
                        <tr>
                            <td><b>Revisions attempted</b></td>
                            <td><span class="rwd-tables thead"><b>This Week</b></span><span class="rwd-tables tbody"><?php echo $revisionweekstatus;?></span></td>
                            <td><span class="rwd-tables thead"><b>Total</b></span><span class="rwd-tables tbody"><?php echo $revisiontotal;?></span></td>                                                        
                        </tr>
                        <?php 
                        endif;
                        if (Yii::app()->session['challenge']):
                        ?>
                        <tr>
                            <td><b>Challenges attempted</b></td>
                            <td><span class="rwd-tables thead"><b>This Week</b></span><span class="rwd-tables tbody"><?php echo $challengeweekstatus;?></span></td>
                            <td><span class="rwd-tables thead"><b>Total</b></span><span class="rwd-tables tbody"><?php echo $challengetotal;?></span></td>                                                                                    
                        </tr>
                        <?php 
                        endif;
                        if (Yii::app()->session['mock']):
                        ?>                        
                        <tr>
                            <td><b>Mocks attempted</b></td>
                            <td><span class="rwd-tables thead"><b>This Week</b></span><span class="rwd-tables tbody"><?php echo $mockweekstatus;?></span></td>
                            <td><span class="rwd-tables thead"><b>Total</b></span><span class="rwd-tables tbody"><?php echo $mocktotal;?></span></td>                                                                                    
                        </tr> 
                        <?php endif;?>
                        <tr>
                        <td ><b>Last login</b></td>
                        <td colspan="2"><b><?php echo $lastlogin;?></b></td>
                        </tr>                                               
                    </table>                
            </div>            
        </div>
        <div class="bs-example">
            <h1 class="h125">Overall Performance: %Completed vs % Correct</h1>
            <div class="panel panel-default clearfix">
                <div class="col-md-12">
        			<div class="row" style="margin-top: 15px;margin-bottom: 15px;">
        					<div class="col-md-12">
        						<div class="col-md-4">
        							<span class="attmt"> </span>% attempted
        						</div>
        						<div class="col-md-4">
        							<span class="coattmt"> </span>% correct form attempted
        						</div>
        						<div class="col-md-4">
        							<span class="noattmt"> </span>% not attempted
        						</div>
        					</div>
        			</div>                    
                    <div class="col-xs-12 col-sm-8 col-md-8 padtop20">
                    
                    <?php 
                        //$minimumarray=array();
                       // $chapterarray=array();
                        foreach($chapters AS $key=>$chapter): 
                        
                                                                                      
                                $getquestions=$this->GetChapterCount($chapter->TM_TP_Id,Yii::app()->user->id);
                                /*if($key<5):
                                    $minimumarray[$key]=$getquestions['correct'];
                                    $chapterarray[$key]=$chapter->TM_TP_Id;                                
                                else:
                                    $newminimumarray=$this->ClosestNumber($minimumarray,$getquestions['correct'],$chapterarray,$chapter->TM_TP_Id);
                                    if($newminimumarray):
                                        $minimumarray=$newminimumarray[0];
                                        $chapterarray=$newminimumarray[1];
                                    endif;
                                endif;*/
                    ?>   
                    <div class="row" style=" margin-bottom: 20px;"> 
                    	<div class="col-xs-12 col-md-4 text-left">
                    		<h6 style="margin-top: 7px;margin-bottom: 16px;"><?php echo $chapter->TM_TP_Name;?></h6>
                    	</div>
                    	<div class="col-xs-12 col-md-8">
                    		<div class="progress" style="background-color:#a3a3a3;">
                                <?php if($getquestions['correct']>0):?>
                        			<div class="progress-bar progress-bar-danger progrescompleted"  style="width: <?php echo $getquestions['average'];?>%; text-align: right;">
                        				<span class="attempted"> <?php echo $getquestions['average'];?>% </span>
                                        <div class="progress-bar progress-bar-success progresssuccess" style="width: <?php echo $getquestions['correct'];?>%;">
                            				<span class="correct" ><?php echo $getquestions['correct'];?>%</span>
                            			</div>                                        
                        			</div>
                        			
                                <?php else:?>
                                    <div class="progress-bar progress-bar-danger"  style="width: <?php echo $getquestions['average'];?>%; text-align: right;">
                        				<span class="attempted"> <?php echo $getquestions['average'];?>% </span>
                        			</div>
                                <?php endif;?> 
                    		</div>  
                    	</div>
                    <!-- end LP -->
                    
                    </div>                                         				
                     <?php  endforeach;?>                                    
                    </div>
                    <div class="col-sm-4 pull-right text-center padtop20">
                    	<b>Overall % completed</b>
                    	<div class="row"  style="margin-bottom: 40px;">
                    	  <div class="col-xs-6 col-sm-6">										
                    			<div class="pie_progress4" role="progressbar" data-goal="<?php echo $progressstatus['average']?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress4__number"><?php echo $progressstatus['average']?>%</span>							
                    			</div>
                    			<p>Me</p>
                    	  </div>
                    	  <div class="col-xs-6 col-sm-6">
                    		<div class="pie_progress4 " role="progressbar" data-goal="<?php echo $progressstatus['restaverage'];?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress4__number"><?php echo $progressstatus['restaverage'];?>%</span>							
                    		</div>
                    			<p>Rest of the crowd</p>
                    	  </div>
                    	</div>
                    	
                    	<strong>Overall : Correct vs Incorrect</strong><br>
                    	<small>(From attempted topies)</small>
                    	<div class="row">
                    	  <div class="col-xs-6 col-sm-6">
                    		<div class="pie_progress3" role="progressbar" data-goal="<?php echo $progressstatus['correct']?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress3__number"><?php echo $progressstatus['correct']?>%</span>							
                    		</div>
                            <p>Me</p>
                    	  </div>
                    	  <div class="col-xs-6 col-sm-6">
                    		<div class="pie_progress3" role="progressbar" data-goal="<?php echo $progressstatus['restcorrect'];?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress3__number"><?php echo $progressstatus['restcorrect'];?>%</span>							
                    		</div>
                            <p>Rest of the crowd</p>
                    	  </div>
                    	</div>
</div>                    
                </div>              
            </div>
        </div>                    
    <!-- new design ends-->                           
        <div class="bs-example">
            <h1 class="h125">Suggested topics to revise on based on past performance</h1>
            <div class="panel panel-default clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 padtop20">
                <div class="row">
                <div class="col-xs-12 col-md-3 text-left">
				    <b>Topic</b>
				</div>
                <div class="col-xs-12 col-md-3 text-left">
					<b>Chapter</b>
				</div>
                </div>
                
                <?php                 
                foreach($topicstatus AS $topic):
                $chapter=Chapter::model()->findByPk($topic['TM_SN_Topic_Id']);  
                if($topic['completed']>0):
                    $average=($topic['completed']/$topic['total'])*100;                
                    if($topic['attempted']>0):
                        $correct=round(($topic['attempted']/$topic['completed'])*100);
                    else:
                        $correct=0;
                    endif;
                    $average=round($average);
                else:
                    $average=0;
                    $correct=0;
                endif;                                           
                ?>
                <div class="row" style=" margin-bottom: 20px;">                     	
						<div class="col-xs-12 col-md-3 text-left">
							<h6 style="margin-top: 7px;margin-bottom: 16px;"><?php echo $topic['TM_SN_Name'];?></h6>
						</div>
                        <div class="col-xs-12 col-md-3 text-left">
							<h6 style="margin-top: 7px;margin-bottom: 16px;"><?php echo $chapter->TM_TP_Name;?></h6>
						</div>                        
                    	<div class="col-xs-12 col-md-6">
                    		<div class="progress" >                               
                        			<div class="progress-bar progress-bar-danger progrescompleted"  style="width: <?php echo $average;?>%; text-align: right;">
                        				<span class="attempted"> &nbsp;</span>
                                        
                        			<div class="progress-bar progress-bar-success progresssuccess" style="width: <?php echo  $correct;?>%;">
                        				<span class="correct" ><?php echo  $correct;?>%</span>
                        			</div>
                        			</div>                                 
                    		</div>  
                    	</div>
                    <!-- end LP -->
                    
                    </div>                   
                <?php
                endforeach;?>
                </div>            
            </div>
        </div>                                            
    </div>
</div>    <?php Yii::app()->clientScript->registerScript('showmarks', "
    $('#viewsolution').click(function(){
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/GetSolution')."',
          data: { testid: '".$test->TM_STU_TT_Id."'}
        })
          .done(function( html ) {
             $('#showsolution').html( html );
          });
    });
		$('.pie_progress4').asPieProgress({
            namespace: 'pie_progress'
        });
		$('.pie_progress4').asPieProgress('go');
        
		$('.pie_progress3').asPieProgress({
            namespace: 'pie_progress'
        });
		$('.pie_progress3').asPieProgress('go');                

");?>
<script>
$(document).ready(function() {
alert('fully loaded');
})
</script>