<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
    <?php if(Yii::app()->session['school']):?>
<div class="bs-example">


    <h1 class="h125" style="padding-top: 10px;">You have the following Tasks</h1>

            <div class="panel panel-default">
            <?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
                    </div>
                <?php endif;?>
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="tab-pane fade active in" >
                    <table class="rwd-tables">
                        <tbody id="homeworklistbody" >
                        <tr>
                            <th>Class</th>
                            <th>Task</th>
                            <th>Published On</th>
                            <th>Published By</th>
                            <th>Due By</th>                             
                            <th>Comments</th>
                            <th ></th>
                            <th></th>
                            <th></th>                            
                        </tr>                        
                        <?php
                    if(count($studenthomeworks)>0):
                    foreach ($studenthomeworks AS $homework):
                       
                        $criteria=new CDbCriteria;
                        $criteria->condition='TM_HQ_Homework_Id='.$homework->homework->TM_SCH_Id.' AND TM_HQ_Type NOT IN (6,7)';
                        $totalonline=HomeworkQuestions::model()->count($criteria);
                        //echo $totalonline;
                        $criteria=new CDbCriteria;
                        $criteria->condition='TM_HQ_Homework_Id='.$homework->homework->TM_SCH_Id.' AND TM_HQ_Type IN (6,7)';
                        $totalpaper=HomeworkQuestions::model()->count($criteria);
                        $totalquestions=$totalonline+$totalpaper;
                        ?>
                        <!-- code by amall -->
                            <tr id="mockrow<?php echo $homework->TM_STHW_Id;?>">
                                <td ><span class="rwd-tables thead"><b>Class</b></span><span class="rwd-tables tbody"><?php echo Standard::model()->findByPk($homework->homework->TM_SCH_Master_Stand_Id)->TM_SD_Name;?></span></td>
                                <td ><span class="rwd-tables thead"><b>Task</b></span><span class="rwd-tables tbody"><?php echo $homework->homework->TM_SCH_Name;?></span></td>
                                <td ><span class="rwd-tables thead"><b>Published On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($homework->TM_STHW_PublishDate)); ?></span></td>
                                <td ><span class="rwd-tables thead"><b>Published By</b></span><span class="rwd-tables tbody"><?php echo User::model()->findByPk($homework->homework->TM_SCH_CreatedBy)->profile->firstname; ?></span></td>
                                <td ><span class="rwd-tables thead"><b>Due By</b></span><span class="rwd-tables tbody"><?php echo Yii::app()->dateFormatter->format("dd-MM-y",strtotime($homework->homework->TM_SCH_DueDate)) ?></span></td>
                                <td ><span class="rwd-tables thead"><b>Comments</b></span><span class="rwd-tables tbody"><?php echo $homework->homework->TM_SCH_Comments; ?></span></td>

                                <?php if($homework->TM_STHW_Type==1 || $homework->TM_STHW_Type==3 ):?>
                                    <td class="text-right" colspan="3">
                                        <?php if($this->HasWorksheeturl($homework->TM_STHW_HomeWork_Id)=='0'): ?>
                                         <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>
                                          <?php else: ?>
                                           <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a> 
                                            <?php endif;?>
                                        <?php if($homework->homework->TM_SCH_Video_Url==''):?>
                                            <?php if($this->HasAnswersheets($homework->TM_STHW_HomeWork_Id)=='0'):?>
                                                <a href="javascript:void(0)" class="homework" title="Upload Answer" data-toggle="modal" data-target="#myModal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span  class="glyphicon glyphicon-cloud-upload colorchnage"></span></a>
                                            <?php else: ?>
                                                <a href="javascript:void(0)" class="homework" title="Manage Answer Sheets" data-toggle="modal" data-target="#myModal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-cloud-upload"></span></a>
                                            <?php endif;?>
                                            <?php echo "<a class='btn btn-warning printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'>Print</a>";?>
                                            <?php /*echo "<a href='" .Yii::app()->createUrl('student/printworksheet', array('id' => $homework->TM_STHW_HomeWork_Id)). "' target='_blank' title='Print Worksheet'><span class='glyphicon glyphicon-print'></span></a>";*/?>
                                            <?php echo "<a href='" .Yii::app()->createUrl('student/homeworksubmit', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning hwsubmit'>Submit</a>";?>
                                        <?php else:?>
                                            <?php echo "<a href='" .$homework->homework->TM_SCH_Video_Url. "' class='btn btn-warning' target='_blank'>View</a>";?>
                                            <?php echo "<a href='" .Yii::app()->createUrl('student/homeworksubmit', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning ressubmit'>Complete</a>";?>
                                        <?php endif;?>
                                    </td>
                                    
                                <?php elseif($homework->TM_STHW_Type==2):if($totalonline!=0 & $totalpaper==0):?>
                                <td class="text-right" colspan="3">
                                     <?php if($this->HasWorksheeturl($homework->TM_STHW_HomeWork_Id)=='0'): ?>
                                     <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>
                                      <?php else:?>
                                      <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                      <?php endif;?>
                                                    <?php
                                    if ($homework->TM_STHW_Status > 0):
                                        if ($homework->TM_STHW_Status == '1'):
                                            echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                            echo "<a href='" .Yii::app()->createUrl('student/homeworktest', array('id' => $homework->TM_STHW_HomeWork_Id,'action'=>'checkheader')). "' class='btn btn-warning'>Continue</a>";
                                        elseif($homework->TM_STHW_Status == '3'):
                                            if($this->HasAnswersheets($homework->TM_STHW_HomeWork_Id)=='0'):
                                                echo '<a href="javascript:void(0)" class="homework" title="Upload Answer" data-toggle="modal" data-target="#myModal" data-value="'.$homework->TM_STHW_HomeWork_Id.'"><span class="glyphicon glyphicon-cloud-upload"></span></a> ';
                                            else:
                                                echo '<a href="javascript:void(0)" class="homework" title="Manage Answer Sheets" data-toggle="modal" data-target="#myModal" data-value="'.$homework->TM_STHW_HomeWork_Id.'"><span style="color:#ffa900;" class="glyphicon glyphicon-cloud-upload"></span></a> ';
                                            endif;                                        
                                            //echo "<a href='javascript:void(0)' class='homework btn btn-warning' title='Upload Answer' data-toggle='modal' data-target='#myModal' data-value='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-cloud-upload'></span></a>";
                                            echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                            echo "<a href='" .Yii::app()->createUrl('student/homeworksubmit', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning hwsubmit'>Submit</a>";
                                        elseif($homework->TM_STHW_Status == '6'):
                                            echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                            echo "<a href='" .Yii::app()->createUrl('student/homeworksubmit', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning hwsubmit'>Submit</a>";
                                        endif;
                                    else:

                                        echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                        echo "<a href='" .Yii::app()->createUrl('student/starthomework', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning'>Start</a>";
                                    endif;
                                    ?>
                                    </td>
                                <?php elseif($totalonline!=0 & $totalpaper!=0):?>
                                <td class="text-right" colspan="3">
                                                    <?php
                                    if ($homework->TM_STHW_Status > 0):
                                        if ($homework->TM_STHW_Status == '1'):
                                            if($this->HasWorksheeturl($homework->TM_STHW_HomeWork_Id)=='0'): ?>
                                             <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>
                                              <?php else:?>
                                              <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                              <?php endif;
                                            echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                            echo "<a href='" .Yii::app()->createUrl('student/homeworktest', array('id' => $homework->TM_STHW_HomeWork_Id,'action'=>'checkheader')). "' class='btn btn-warning'>Continue</a>";
                                        elseif($homework->TM_STHW_Status == '2'):
                                            if($this->HasWorksheeturl($homework->TM_STHW_HomeWork_Id)=='0'): ?>
                                             <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>
                                              <?php else:?>
                                              <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                              <?php endif;
                                            echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                            echo "<a href='" .Yii::app()->createUrl('student/showhomeworkpaper', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning'>Continue</a>";
                                        elseif($homework->TM_STHW_Status == '3'):
                                            if($this->HasAnswersheets($homework->TM_STHW_HomeWork_Id)=='0'):
                                                echo '<a href="javascript:void(0)" class="homework" title="Upload Answer" data-toggle="modal" data-target="#myModal" data-value="'.$homework->TM_STHW_HomeWork_Id.'"><span class="glyphicon glyphicon-cloud-upload"></span></a> ';
                                            else:
                                                echo '<a href="javascript:void(0)" class="homework" title="Manage Answer sheets" data-toggle="modal" data-target="#myModal" data-value="'.$homework->TM_STHW_HomeWork_Id.'"><span style="color:#ffa900;" class="glyphicon glyphicon-cloud-upload"></span></a> ';
                                            endif; 
                                            if($this->HasWorksheeturl($homework->TM_STHW_HomeWork_Id)=='0'): ?>
                                         <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>
                                          <?php else:?>
                                          <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                          <?php endif;
                                            echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                            echo "<a href='" .Yii::app()->createUrl('student/homeworksubmit', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning hwsubmit'>Submit</a>";
                                        elseif($homework->TM_STHW_Status == '6'):
                                            if($this->HasWorksheeturl($homework->TM_STHW_HomeWork_Id)=='0'): ?>
                                         <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>
                                          <?php else:?>
                                          <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                          <?php endif;
                                            echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                            echo "<a href='" .Yii::app()->createUrl('student/homeworksubmit', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning hwsubmit'>Submit</a>";
                                        endif;
                                    else:
                                        if($this->HasWorksheeturl($homework->TM_STHW_HomeWork_Id)=='0'): ?>
                                         <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>
                                          <?php else:?>
                                          <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $homework->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                          <?php endif;
                                        echo "<a class='printworksh' title='Print Worksheet' data-backdrop='static' data-toggle='modal' data-value='".$homework->TM_STHW_HomeWork_Id."' href='".$homework->TM_STHW_HomeWork_Id."'><span class='glyphicon glyphicon-print'></span></a>";
                                        echo "<a href='" .Yii::app()->createUrl('student/starthomework', array('id' => $homework->TM_STHW_HomeWork_Id)). "' class='btn btn-warning'>Start</a>";
                                    endif;
                                    ?>
                                </td>

                                <?php endif;endif;?>
                                <!--ends-->
                            </tr>                        
                    <?php endforeach;
                    else:?>
                        <tr>
                            <td colspan="7" >No Tasks Assigned</td>
                            <td></td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>

            <div id="myModalanswers" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <form action="<?php echo Yii::app()->request->baseUrl."/student/UploadAnswersheet";?>" onsubmit="return Checkfiles();" id="answersheetform" enctype="multipart/form-data" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Manage Uploaded Answer sheet</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <form action="<?php echo Yii::app()->request->baseUrl."/student/UploadAnswersheet";?>" onsubmit="return Checkfiles();" id="answersheetform" enctype="multipart/form-data" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Upload Answer sheet</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'homework-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                                        <label class="control-label">Select Files</label>
               <!--  <?php 
                echo $form->fileField($model, 'TM_STHWAR_Filename',array('class'=>'file','id'=>'file', 'accept'=>'image/*', 'multiple'=>true));
                 ?> -->
                    <div style="display: flex;">
                    <input style="width: 100%"  id="file" accept='image/*' type="file" class="file" name="file">

                     <img id="loader_process" src="<?php echo Yii::app()->request->baseUrl."/images/preloader.gif" ?>" style="width: 23px;margin-left: 0px; display: none;" >
                      <button type="button" class="btn btn-warning" id="uploadimg">Upload</button>
                    </div>
                                        <input type="hidden" name="homework_id" id="homework_id">
                                        <div id="answersheetlist"></div>
                                      
                                    </div>
                                    <div class="modal-footer">
                                        <span class="alert alert-success pull-left uploadinfo" style="display: none;"> </span>
                                        <p style="float: left;">*You can select multiple images and upload</p>
                                         <p style="float: left;">*Files can uploaded after submitting also, from History</p>
                                        <br><br>
                                        <p  style="float: left; color: red; display: none;" id="imgonlymsg">Only image files are allowed.!</p>
                                      
                                        <button type="button" class="btn btn-warning refresgpage" data-dismiss="modal">Ok</button>
                                          <?php $this->endWidget(); ?>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>



                 <div id="urlmodal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <form action="<?php echo Yii::app()->request->baseUrl."/student/Uploadurl";?>" onsubmit="return Checkfiles();" id="answersheetform" enctype="multipart/form-data" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Enter Url</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'homework-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                                        <label class="control-label">Enter File</label>
                <?php 
                echo $form->textField($model1, 'TM_STHWARU_Url',array('class'=>'form-control','id'=>'url'));
                 ?>
                                     <!--    <input id="input-1" type="file" class="file" name="Answersheet"> -->

                                        <input type="hidden" name="homeworkid" id="homeworkid">
                                        <div id="answersheetlist"></div>
                                      
                                    </div>
                                    <div class="modal-footer">
                                        <span class="alert alert-success pull-left uploadinfo" style="display: none;"> </span>
                                        <button type="button" class="btn btn-default" id="submitbutton">Ok</button>
                                        <button type="button" id="closebutton" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <?php $this->endWidget(); ?>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>  
<?php 
if(count($studentmarkings)>0):
?>        
<div class="bs-example">


    <h1 class="h125" style="padding-top: 10px;">You have the following Tasks to mark</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="tab-pane fade active in" >
                    <table class="rwd-tables">
                        <tbody id="homeworklistbody" >
                        <tr>
                            <th>Task</th>
                            <th>Assigned By</th>
                            <th>Assigned On</th>
                            <th></th>
                            <th></th>
                            <th></th>                            
                        </tr>                        
                        <?php
                    if(count($studentmarkings)>0):
                    foreach ($studentmarkings AS $markings):

                        ?>

                        <tr id="markrow<?php echo $markings->TM_HWM_Id;?>">
                            <td ><span class="rwd-tables thead"><b>Task</b></span><span class="rwd-tables tbody"><?php echo $markings->homework->TM_SCH_Name;?></span></td>
                            <td ><span class="rwd-tables thead"><b>Assigned By</b></span><span class="rwd-tables tbody"><?php echo Homeworkmarking::getAssignee($markings->TM_HWM_Assigned_By);?></span></td>
                            <td ><span class="rwd-tables thead"><b>Assigned On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($markings->TM_HWM_Assigned_On)); ?></span></td>                          
                            <td colspan="3" class="text-right"><span >
                            <?php
                                echo "<a href='" .Yii::app()->createUrl('student/Markhomework', array('id' => $markings->TM_HWM_Homework_Id)). "' class='btn btn-warning'>Mark</a>";
                            ?>
                           </span></td>

                        </tr>
                        <!-- ends -->
                    <?php endforeach;
                    else:?>
                        <tr>
                            <td colspan="7" >No Tasks assigned for marking</td>
                            <td></td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>                    
                </div>

            </div>
        </div>           
    <?php
    endif;
     endif;?>
    <?php if(Yii::app()->session['quick'] || Yii::app()->session['difficulty']):?>
        <div class="bs-example">
            <h1 class="h125">You have the following open self revisions</h1>
                <div class="panel panel-default">

            
                <div class="tab-pane fade active in" id="revisions">

                        <table class="rwd-tables">
                           <tbody id="revisionlistbody">                           
                            <tr>
                                <th><b>Published On</b></th>
                                <th><b>Topic</b></th>
                                <th><b>Questions</b></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>                                
                            </tr>
                            <?php
                            if(count($studenttests)>0):
                            foreach ($studenttests AS $tests):
                                ?>
                                <tr id="<?php echo 'testrow' . $tests->TM_STU_TT_Id;?>">
                                    <td ><span class="rwd-tables thead"><b>Published On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($tests->TM_STU_TT_CreateOn)); ?></span></td>

                                    <td ><span class="rwd-tables thead"><b>Topic</b></span><span class="rwd-tables tbody"><?php
                                        foreach ($tests->chapters AS $key => $chapter):
                                            echo ($key != '0' ? ' ,' : '') . $chapter->chapters->TM_TP_Name;
                                        endforeach;?></span></td>
                                    <td><span class="rwd-tables thead"><b>Questions</b></span><span class="rwd-tables tbody"><?php echo $this->GetQuestionCount($tests->TM_STU_TT_Id);?></span>
                                    
                                    </td>
                                    <td colspan="3" class="text-right">
                                        <a href="javascript::void(0)" title="Print Revision">
                                            <span class="glyphicon glyphicon-print print_rev" data-id=<?= $tests->TM_STU_TT_Id ?>  aria-hidden="true"></span>
                                        </a>
                                        <?php                                       
                                        if ($tests->TM_STU_TT_Status > 0):
                                            if ($tests->TM_STU_TT_Status == '1'):
                                                echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/RevisionTest', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Continue</a>";
                                            else:
                                                echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/showpaper', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Continue</a>";
                                            endif;
                                        else:
                                            echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/starttest', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Start</a>";
                                        endif;
                                        ?>
                                    </td>


                                    <td class="text-right">
                                       <span  data-toggle="tooltip" data-placement="top" title="Delete Revision"> <?php echo CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/trash.png', '', array('class' => 'hvr-buzz-out','id' => 'some-name-'.uniqid())),
                                               Yii::app()->createUrl('student/deletetest', array('id' => $tests->TM_STU_TT_Id)),
                                            array(
                                                'type' => 'POST',
                                                'dataType' => 'json',
                                                'success' => 'function(data){
                                                        if(data["error"]=="no")
                                                        {
                                                            $("#testrow' . $tests->TM_STU_TT_Id . '").remove();
                                                            if(data["testcount"]==0)
                                                            {
                                                               $("#revisionlistbody").append("<tr><td colspan=\'6\' class=\'text-right\'  ><a href=\''.Yii::app()->createUrl('student/revision').'\' class=\'btn btn-warning\'>Add</a></td><td></td></tr>")
                                                            }
                                                        }
                                                    }'
                                            ),
                                            array('confirm' => 'Are you sure you want to delete this revision?')
                                        );?></span>

                                    </td>
                                </tr>
                            <?php endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="6" class="text-right" ><a href="<?php echo Yii::app()->createUrl('student/revision');?>" class="btn btn-warning">Add</a> </td>
                                <td></td>
                            </tr>
                            <?php endif;?>                            
                            </tbody>
                        </table>


                </div>
                </div>


            </div>
        </div>
    <?php endif;?>

    <?php if(Yii::app()->session['challenge']):?>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">You have the following open challenges</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="tab-pane fade active in">
                    <table class="rwd-tables">
                        
                        <tbody id="challengelistbody" >
                        <tr class="bro_top_none">
                            <th><b>Set By</b></th>
                            <th><b>Set On</b></th>
                            <th><b>Questions</b></th>
                            <th><b>Set For</b></th>
                            <th></th>
                            <th></th>
                            <th></th>

                        </tr>

                        <?php
                        if(count($challenges)>0):
                        foreach ($challenges AS $challenge):

                            ?>
                            <tr id="challengerow<?php echo $challenge->challenge->TM_CL_Id;?>">
                                <td><span class="rwd-tables thead"><b>Set By</b></span><span class="rwd-tables tbody"><?php echo($challenge->challenge->TM_CL_Chalanger_Id == Yii::app()->user->id ? 'Me' : $this->GetChallenger($challenge->challenge->TM_CL_Chalanger_Id));?></span></td>
                                <td><span class="rwd-tables thead"><b>Set On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($challenge->challenge->TM_CL_Created_On)); ?></span></td>
                                <td>
                                    <span class="rwd-tables thead"><b>Questions</b></span><span class="rwd-tables tbody"><?php 
                                    $count=$this->GetCountChallengeQuestion($challenge->challenge->TM_CL_Id);
                                    $questioncount=($count<$challenge->challenge->TM_CL_QuestionCount?$count:$challenge->challenge->TM_CL_QuestionCount);
                                    echo $questioncount;?></span>
                                </td>
                                <td id="buddies<?php echo $challenge->challenge->TM_CL_Id;?>">
                                <span class="rwd-tables thead"><b>Set For</b></span><span class="rwd-tables tbody"><?php echo $this->GetChallengeInvitees($challenge->challenge->TM_CL_Id, Yii::app()->user->id);?></span>
                                </td>                               
                                <td class="text-right">
                                    <?php if($challenge->challenge->TM_CL_Chalanger_Id==Yii::app()->user->id):?>
                                        <span data-toggle="tooltip" data-placement="top" title="Invite Buddies">
                                        <a href="javascript:void(0)" target="_blank" class="btn btn-info choosebuddies" data-id="<?php echo $challenge->challenge->TM_CL_Id;?>" data-toggle="modal" data-target="#invitModel" style="width: 30px;"><span class="fa fa-users" aria-hidden="true" ></span></a></span>                                     
                                    <?php endif;?>
                                </td>
                                
                                <?php echo $this->getChallengeLinks($challenge->challenge->TM_CL_Id, Yii::app()->user->id);?>

                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="6" class="font2 text-right" ><a href="<?php echo Yii::app()->createUrl('student/challenge');?>" class="btn btn-warning">Add </a> </td>
                            <td></td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal fade " id="invitModel" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Invite Buddies</h4>
                            </div>
                            <form action="" method="post" id="inviteform">
                            <div class="modal-body" >
                                <input type="hidden" name="challengeid" id="challengeid" value="32">
                                <input type="text" id="usersuggest" name="invitebuddies[]">
                                <span class="alert alert-danger error" role="alert" style="display: none;"></span>
                            </div>
                            <div class="modal-footer">
                                <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                                <button type="button" class="btn btn-default" id="invitebuddies" >Invite</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
<div id="myinvite" class="modal fade" role="dialog" style="padding-top:20%;">
  <div class="modal-dialog modal-sm modal-sm_plus">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Challenge position</h4>
      </div>
      <div class="modal-body modal-body_padding">
      <div class="row">
        <ul class='popu-cart' role='menu' style='display: block !important;'>                  
             
            </ul>
            </div>
      </div>
    </div>

  </div>
</div>            
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>
    <?php endif;?>
    <?php if(Yii::app()->session['mock']):?>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">You have the following Worksheets</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="tab-pane fade active in" >
                    <table class="rwd-tables">
                        <tbody id="mocklistbody" >
                        <tr>
                            <th>Worksheets</th>
                            <th>Description</th>
                            <th>Created On</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>                            
                        </tr>                        
                        <?php
                        if(count($studentmocks)>0):
                        foreach ($studentmocks AS $mock):
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type NOT IN (6,7)';
                            $totalonline=MockQuestions::model()->count($criteria);
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type IN (6,7)';
                            $totalpaper=MockQuestions::model()->count($criteria);
                            $totalquestions=$totalonline+$totalpaper;
                            ?>
                            <tr id="mockrow<?php echo $mock->TM_STMK_Id;?>">
                                <td ><span class="rwd-tables thead"><b>Mock</b></span><span class="rwd-tables tbody"><?php echo $mock->mocks->TM_MK_Name;?></span></td>
                                <td ><span class="rwd-tables thead"><b>Description</b></span><span class="rwd-tables tbody"><?php echo $mock->mocks->TM_MK_Description;?></span></td>
                                <td ><span class="rwd-tables thead"><b>Created On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($mock->TM_STMK_Date)); ?></span></td>
                                <td colspan="3" class="text-right"><span >
                                        <?php                                       
                                        if ($mock->TM_STMK_Status > 0):
                                            if ($mock->TM_STMK_Status == '1'):
                                                echo "<a href='" .Yii::app()->createUrl('student/mocktest', array('id' => $mock->TM_STMK_Id,'action'=>'checkheader')). "' class='btn btn-warning'>Continue</a>";
                                            else:
                                                echo "<a href='" .Yii::app()->createUrl('student/Showmockpaper', array('id' => $mock->TM_STMK_Id)). "' class='btn btn-warning'>Continue</a>";
                                            endif;
                                        else:
                                            echo "<a href='" .Yii::app()->createUrl('student/mocktest', array('id' => $mock->TM_STMK_Id,'action'=>'checkheader')). "' class='btn btn-warning'>Start</a>";
                                        endif;
                                        ?>
                                   </span></td>
                                <td class="text-right" ><span data-toggle="tooltip" data-placement="top" title="Delete Mock"><?php echo CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/trash.png', '', array('class' => 'hvr-buzz-out')),
                                        Yii::app()->request->baseUrl.'/student/deletemock/' . $mock->TM_STMK_Id,
                                        array(
                                            'type' => 'POST',
                                            'dataType' => 'json',
                                            'success' => 'function(data)
                                                {
                                                    if(data["error"]=="no")
                                                    {
                                                        $("#mockrow'.$mock->TM_STMK_Id. '").remove();
                                                        if(data["testcount"]==0)
                                                        {
                                                            $("#mocklistbody").append("<tr><td colspan=\'6\' class=\'text-right\'  ><a href=\''.Yii::app()->createUrl('student/mock').'\' class=\'btn btn-warning\'>Add</a></td><td></td></tr>");       
                                                        }
                                                    }
                                                }'
                                        ),
                                        array('confirm' => 'Are you sure you want to delete this mock?')
                                    );?></span></td>
                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="6" class="text-right" ><a href="<?php echo Yii::app()->createUrl('student/mock');?>" class="btn btn-warning">Add</a> </td>
                            <td></td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>
    <?php endif;?>
    </div>


</div><?php Yii::app()->clientScript->registerScript('sendmailreminder', "
 $(document).on('click','.reminder',function(){
    var recepientid= $(this).attr('data-id');
    var challengeid= $(this).attr('data-challenge');
    var item=$(this);
    $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMailReminders')."',
              data: { recepientid: recepientid,challengeid:challengeid},
              beforeSend: function() {
                item.text('sending reminder..')
              }
    })
    .done  (function(data, textStatus, jqXHR){
        $('.rem'+recepientid).slideUp();
        $('.remsend'+recepientid).slideDown();        
        setTimeout(function(){
            item.text(' Send Reminder')
            $('.remsend'+recepientid).slideUp();
        },6000)

    });
 });"); ?>
<script type="text/javascript">
    $(function(){
        
        function getparams()
        {            
            alert(document.getElementById('challengeid').value);
        }
        var suggest=$('#usersuggest').magicSuggest({
            allowFreeEntries: false,
            maxSelection: 10,                       
            data:'<?php echo Yii::app()->request->baseUrl."/site/getBuddies";?>'                       
        });
        $('.choosebuddies').click(function()
        {
            $('.nobuddies').hide();
            var challengeid=$(this).attr('data-id');
            $('#challengeid').val(challengeid);
            suggest.clear(); 
            suggest.setDataUrlParams({id:challengeid});
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/site/getPlanBuddies";?>',
                data: {id:challengeid},
                success: function(data){
                    if(data=='0')
                    {
                        $('.nobuddies').html('You have no buddies for the selected plan').show();
                    }
                }
                });
                          
        });
        $('#invitebuddies').click(function(){
            var challengeid=$('#challengeid').val()                      
            if(suggest.getValue()!='')
            {
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Invitebuddies";?>',
                data: $('#inviteform').serialize(),
                success: function(data){
                    if(data!='no')
                    {
                        $('.inviteinfo').show();
                        $('.inviteinfo').text('Invite sent successful.');
                        $('#inviteform')[0].reset();
                        setTimeout(function(){
                            $('.close').trigger('click');
                            $('.inviteinfo').hide();
                        },3000);
                        $('#buddies'+challengeid).html(data);
                    }
                    else
                    {                       
                        $('.inviteinfo').html('Invite failed. Please try again later.');
                        $('.inviteinfo').show();
                        setTimeout(function(){
                            $('.inviteinfo').hide();
                        },3000);
                    }
                }
                });
            }
            else
            {
                $('.inviteinfo').show();
                $('.inviteinfo').text('Please select users.');
            }

        });
    });
    /*$(document).on('click','.hover',function(){
        var id=$(this).attr('data-id');
        var tooltip=$(this).attr('data-tooltip');
        $('.tooltiptable').hide();
        $('#tooltip'+id).fadeIn();
    });*/
    $(document).on('click','.hover',function(){
        $('.popu-cart').html(''); 
        var id=$(this).attr('data-id');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetChallengeBuddies";?>',
                data: {challenge:id},
                success: function(data){
                    if(data!='no')
                    {
                        $('.popu-cart').html(data);                        
                    }
                }
            });
    });
    $(document).on('click','#close',function(){
        $('.tooltiptable').hide();

    });
</script>
    <script type="text/javascript">
      $(document).on("click", '.deleteworksheet', function(event) { 
            var worksheet=$(this).data('value');
            
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/deleteWorksheet";?>',
                data: {worksheet:worksheet},
                success: function(data){
                    if(data!='no')
                    {
                        $('#file'+worksheet).remove();                        
                    }
                }
                })
            
        });

           $(document).on("click", '.homeworkurl', function() { 
                var hwid=$(this).data('value');
                $('#homeworkid').val(hwid);
                $('#homework_id').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Getworksheeturl";?>',
                data: {homework:hwid},
                success: function(data){
                  $('#url').val(data);
                        //$('#url').html(data);                        
                    
                }
                });                 
            });
        $(document).on("click", '#submitbutton', function() { 
           var homeworkid = $("#homeworkid").val();
           var url =$("#url").val();
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Uploadurl";?>',
                data: {homeworkid:homeworkid,url:url},
                success: function(data){
                   // $(this).css("text-align", "center"); 
                    location.reload();
                  //$('#url').val(data);
                        //$('#url').html(data);                        
                    
                }
                });
        });

        $(document).ready(function(){
            $('.homework').click(function(){
                var hwid=$(this).data('value');
                $('#homeworkid').val(hwid);
                $('#homework_id').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetWorksheets";?>',
                data: {homework:hwid},
                success: function(data){
                    if(data!='no')
                    {
                        $('#answersheetlist').html(data);                        
                    }
                }
                });                 
            });
            $('.homeworkuploads').click(function(){
                var hwid=$(this).data('value');
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetWorksheets";?>',
                data: {homework:hwid},
                success: function(data){
                    if(data!='no')
                    {
                        $('#answersheetlist').html(data);                        
                    }
                }
                });                
            });
            
            /*$("#answersheetform").submit(function(){
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                $.ajax(
                    {
                        url : formURL,
                        type: "POST",
                        data : postData,
                        success:function(data, textStatus, jqXHR)
                        {
                            if(data!='Yes')
                            {
                                $('.uploadinfo').show();
                                $('.uploadinfo').text('Upload successful.');
                                setTimeout(function(){
                                    $('.close').trigger('click');
                                    $('.uploadinfo').hide();
                                },3000);
                            }
                            else
                            {
                                $('.inviteinfo').html('Upload failed. Please try again later.');
                                $('.uploadinfo').show();
                                setTimeout(function(){
                                    $('.uploadinfo').hide();
                                },3000);
                            }
                        }
                    });
                e.preventDefault(); //STOP default action

            });*/
        });
    </script>
<script type="text/javascript">
    function Checkfiles()
    {
        var fup = document.getElementById('input-1');
        var fileName = fup.value;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

    if(ext =="PDF" || ext=="pdf" || ext=="jpg" || ext=="JPG" || || ext=="jpeg" || ext=="JPEG" || ext=="gif" || ext=="png" || ext=="one")
    {
        return true;
    }
    else
    {
        $('.uploadinfo').html('Upload PDF,JPEG,GIF,PNG,Onenote Files only');
        $('.uploadinfo').show();        
        setTimeout(function(){
            $('.uploadinfo').hide();
        },3000);       

        return false;
    }
    }
</script>
<script>
    $(document).ready(function () {
        $('.hwsubmit').on('click',function(e, data){

            if(!data){
                handleDelete(e, 1,$(this));
            }else{
                window.location = $(this).attr('href');
            }
        });
        $('.ressubmit').on('click',function(e, data){

            if(!data){
                resourceSubmit(e, 1,$(this));
            }else{
                window.location = $(this).attr('href');
            }
        });
    });
    function handleDelete(e, stop,item){
        if(stop){
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "have you uploaded/handed in your homework?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText:"No",
                    confirmButtonColor: "#FDBF43",
                    confirmButtonText: "Yes, Submit!",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        item.trigger('click', {});
                    }
                });
        }
    };
    function resourceSubmit(e, stop,item){
        if(stop){
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    //text: "have you uploaded/handed in your homework?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText:"No",
                    confirmButtonColor: "#FDBF43",
                    confirmButtonText: "Yes, Submit!",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        item.trigger('click', {});
                    }
                });
        }
    };

    
    $( 'body' ).on( 'click', '.print_rev', function () {
         var worksheetid = $(this).data('id');
        // $('#showworksheet').html('');
        window.open('/student/Printrevision?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
    });
    $( 'body' ).on( 'click', '.printworksh', function () {
         var worksheetid = $(this).attr('href');
        ///$('#showworksheet').html('');
        window.open('/teachers/printworksheet?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
    });



</script>

    <script type="text/javascript"> 
        $(document).ready(function() { 

        $("#file").change(function() { 
        var fileName = document.getElementById("file").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $("#imgonlymsg").hide();
            //TO DO
        }else{
           
            $("#imgonlymsg").show();
            return false;
        }
        });      
        $("#uploadimg").click(function() { 

        var fileName = document.getElementById("file").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $("#imgonlymsg").hide();
            //TO DO
        }else{
           
            $("#imgonlymsg").show();
            return false;
        } 
                $('#loader_process').show();
                $("#imgonlymsg").hide();
                var fd = new FormData(); 
                var files = $('#file')[0].files[0]; 
                var homework_id = $('#homework_id').val();
                fd.append('file', files); 
                fd.append('homework_id', homework_id); 

                console.log(fd);
                $.ajax({ 
                    url: 'student/uploadanswersheet', 
                    type: 'post', 
                    data: fd, 
                    contentType: false, 
                    processData: false, 
                    success: function(response){ 
                        if(response != 0){ 
                $('.colorchnage').css('color', '#ffa900');
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetWorksheets";?>',
                data: {homework:homework_id},
                success: function(data){
                    if(data!='no')
                    {
                        $('#answersheetlist').html(data);
                        $('#file').val(''); 
                        $('#loader_process').hide();                        
                    }
                }
                }); 

                        } 
                        else{ 
                            console.log('file not uploaded'); 
                        } 
                    }, 
                }); 
            }); 
    
        }); 
    </script> 