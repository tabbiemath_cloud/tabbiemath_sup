<style>
	.glyphicon
	{
		margin-right: 0px;
	}
</style>
<style type="text/css">
	@media only screen and (max-width: 1300px) {
		.col-md-3 {
			padding-left: 5px;
			padding-right: 5px;
		}
	}
	@media only screen and (max-width: 1200px) {
		.col-md-3 {
			width:33.33%;
			float: left;
			text-align: center;
		}
	}
	@media only screen and (max-width: 998px) {
		.col-md-3 {
			width: 50%;
		}
	}
	@media only screen and (max-width: 767px) {
		.col-md-3 {
			width: 50%;
		}
	}
	@media only screen and (max-width: 500px) {
		.col-md-3 {
			width: 100%;
			text-align:center;
		}
	}

</style>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <div class="contentarea">

            <!----------------------------Mocks------------------------------------------------------------------------------------------------------------------------------------------------>
		<div class="bs-example">
			<h1 class="h125"></h1>
			<form method="get">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-sm-2 pull-right">
							<input type="submit" class="btn btn-warning" name="search" value="Search">
						</div>
						<div class="col-sm-4 pull-right">
							<input type="text" class="form-control" name="mock" value="<?php echo ($formvals['mock']? $formvals['mock']:'');?>" placeholder="Search topics">
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<?php
					foreach($mockheaders AS $mockheader):
						$headers=explode(',', $mockheader['TM_MT_Tags']);
						foreach($headers AS $header):
							?>

							<button type="submit" class="btn btn-warning" style="border: none;background: none;text-decoration: underline;color: blue;font-weight: bold" name="tags" value="<?php echo $header;?>"><?php echo $header;?></button> <span>|</span>

						<?php endforeach;endforeach;?>
				</div>
			</form>
			<h1 class="h125">Select a worksheet and start revision</h1>

					<?php foreach($mocks AS $mock):
						if($mock['TM_MK_Worksheet']==''):
							$worksheet="#Worksheetmodal";
							$style="";
						else:
							$worksheet="";
							$style="style='cursor: not-allowed;'";
						endif;
						$name = strlen($mock['TM_MK_Name']) > 50 ? substr($mock['TM_MK_Name'],0,50)."..." : $mock['TM_MK_Name'];
						$desc = strlen($mock['TM_MK_Description']) > 50 ? substr($mock['TM_MK_Description'],0,50)."..." : $mock['TM_MK_Description'];
						$createdby=Mock::model()->GetCreator($mock->TM_MK_CreatedBy);
						$onlineqstns=MockQuestions::model()->count(array('condition' => "TM_MQ_Mock_Id='".$mock->TM_MK_Id."' AND TM_MQ_Type NOT IN(6,7)"));
						?>
						<div class="col-md-3">
							<div class="thumbnail" style="height: 400px;display: block">
								<div class="caption" style="min-height: 55px">
									<b><?php echo $name;?></b>
									<?php /*if($createdby!='System'):*/?><!--
										<div style="text-align: right">
											<img src="<?php /*echo Yii::app()->request->baseUrl."/images/noimage2.png"*/?>" class="img-circle" style="width: 35px;height: 35px;border: 2px solid #ffa900;">
										</div>
									--><?php /*endif;*/?>
								</div>
								<a class="viewworksheet" <?php echo $style;?> title="View Worksheet" data-backdrop="static" data-toggle="modal" data-target="" data-value="<?php echo $mock['TM_MK_Id'];?>" href="">
									<?php if($mock['TM_MK_Thumbnail']!=''):?>
										<img src="<?php echo Yii::app()->request->baseUrl."/worksheets/".$mock['TM_MK_Thumbnail']?>" style="height: 250px;" alt="<?php echo $mock['TM_MK_Thumbnail'];?>">
									<?php else:?>
										<img src="<?php echo Yii::app()->request->baseUrl."/images/worksheet.jpg"?>" style="height: 250px;" alt="">
									<?php endif;?>
								</a>
								<div class="caption">
									<p style="min-height: 38px;"><?php echo $desc;?></p>
									<a title="Print Worksheet" filter="" class="btn btn-warning viewworksheet" data-backdrop="static" data-toggle="modal" data-target="" data-value="<?php echo $mock['TM_MK_Id'];?>" href=""><span class="glyphicon glyphicon-print"></span></a>
									<a title="Print Worksheet Solution" data-backdrop="static" class="btn btn-warning printsolut" href="" data-value="<?php echo $mock['TM_MK_Id'];?>"><span class="glyphicon glyphicon-list-alt"></span></a>
									<?php if($mock['TM_MK_Link_Resource']==1){ ?>
									<a title="Resources" filter="" class="btn btn-warning"  target="_blank" href="linkresources?resource=<?php echo $mock['TM_MK_Id']; ?>"><span class="glyphicon glyphicon-facetime-video"></span></a>

									<?php } if($onlineqstns>0):
										echo "<a href='" .Yii::app()->createUrl('student/startmock', array('id' => $mock->TM_MK_Id)). "' class='btn btn-warning'>Start</a>";
										else:
										echo "<a href='javascript:void(0);' class='btn btn-warning' style='visibility:hidden'>Start</a>";
										endif;
										?>
								</div>
							</div>
						</div>
					<?php endforeach;?>
					<!--<div class="tab-pane fade active in" id="revisions">
						<table class="rwd-tables">												
							<tbody>										
								<tr class="bro_top_none">
									<th><b>Worksheets</b></th>
									<th><b>Description</b></th>
                                    <th></th>

								</tr>
								<?php /*foreach($mocks AS $mock):
                                    $criteria=new CDbCriteria;
                                    $criteria->condition='TM_STMK_Mock_Id=:mockid AND TM_STMK_Student_Id=:studentid';
                                    $criteria->params=array(':mockid'=>$mock->TM_MK_Id,':studentid'=>Yii::app()->user->id);
                                    $studentmock=StudentMocks::model()->find($criteria);
                                    $onlineqstns=MockQuestions::model()->count(array('condition' => "TM_MQ_Mock_Id='".$mock->TM_MK_Id."' AND TM_MQ_Type NOT IN(6,7)"));

                                    */?>
								<tr id="mockrow<?php /*echo $mock->TM_MK_Id;*/?>">
								
								
									<td><span class="rwd-tables thead"><b>Mock</b></span><span class="rwd-tables tbody"><?php /*echo $mock->TM_MK_Name;*/?></span></td>
									<td><span class="rwd-tables thead"><b>Description</b></span><span class="rwd-tables tbody"><?php /*echo $mock->TM_MK_Description;*/?></span></td>
									<td class="text-right">
                                        <?php /*echo "<a href='" .Yii::app()->createUrl('student/printmock', array('id' => $mock->TM_MK_Id)). "' class='btn btn-warning' title='Print Worksheet' target='_blank'><span class='glyphicon glyphicon-print'></span></a>";*/?>
                                       &emsp;&emsp;&emsp;
									    <?php /*echo "<a href='" .Yii::app()->createUrl('student/printmocksolution', array('id' => $mock->TM_MK_Id)). "'  class='btn btn-warning' title='Print Worksheet Solution' target='_blank'><span class='glyphicon glyphicon-list-alt'></span></a>";*/?>
                                       &emsp;&emsp;&emsp;
										<?php /*
                                        if($onlineqstns>0):
                                            echo "<a href='" .Yii::app()->createUrl('student/startmock', array('id' => $mock->TM_MK_Id)). "' class='btn btn-warning'>Start</a>";
                                        else:
                                            echo "<a href='javascript:void(0);' class='btn btn-warning' style='visibility:hidden'>Start</a>";
                                        endif;
                                        */?>
                                    </td>												
								</tr>
								<?php /*endforeach;*/?>
		
							</tbody>	
						</table>
					</div>-->

		</div>
		<!----------------------------self revisions End----------------------------------------------------------------------------------------------------------------------------------------------->
		
		
        </div>


    </div>

<div class="modal fade " id="Worksheetmodal" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">View Worksheet</h4>
			</div>
			<div class="panel-heading">
				<div style="text-align: right;" id="printbtn">

				</div>
			</div>
			<div class="modal-body" >
				<table class="table ">
					<thead>
					<tr id="preloaderrow">
						<th>
							<div class='col-md-12'><img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader"></div>
						</th>
					</tr>
					</thead>
					<tbody class="font3 wirisfont" id="showworksheet">

					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
	// $('.viewworksheet').click(function(){
	// 	var worksheetid = $(this).data('value');
	// 	$('#showworksheet').html('');
	// 	$.ajax({
	// 		type: 'POST',
	// 		dataType:'json',
	// 		url: '<?php echo Yii::app()->request->baseUrl."/student/Viewworksheet";?>',
	// 		beforeSend : function(){
	// 			$('#preloaderrow').show();
	// 		},
	// 		data: {worksheetid:worksheetid},
	// 		success: function(data){
	// 			$('#preloaderrow').hide();
	// 			$('#printbtn').html(data.print);
	// 			$('#showworksheet').html(data.result);
	// 		}
	// 	});
	// });
	 $('.viewworksheet').click(function(){
        var worksheetid = $(this).data('value');
        // $('#showworksheet').html('');
        window.open('../mock/printmock?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
       
    });
	 $('.printsolut').click(function(){
        var worksheetid = $(this).data('value');
        // $('#showworksheet').html('');
        window.open('../teachers/printmocksolution?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
       
    });
</script>
	
	
	
	
	
	
	
