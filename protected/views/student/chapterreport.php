<?php
/* @var $this PlanController */
/* @var $model Plan */
$this->menu=array(
    array('label'=>'', 'class'=>'nav-header'),
);
?>
<h3>Chapter Progress Report</h3>
<div class="row brd1">

    <div class="col-lg-12">
        <form method="get" id="searchform">
            <table class="table table-bordered table-hover table-striped admintable">
                <tr><th colspan="5"><h3><?php echo UserModule::t("Search"); ?></h3></th></tr>
                <tr>
                    <th>School</th>
                    <th >Plan</th>
                    <th >Chapter</th>
                    <th colspan="2">
                        <input type="radio" class="date" <?php echo ($formvals['date']=='before'?'checked="checked"':'');?> name="date" value="before" id="before">Before a date
                        <input type="radio" class="date" <?php echo ($formvals['date']=='after'?'checked="checked"':'');?> name="date" value="after" id="after">After a date
                        <input type="radio" class="dates" <?php echo ($formvals['date']=='between' || $formvals['date']=='' ?'checked="checked"':'');?> name="date" value="between" id="days">Between dates
                    </th>
                </tr>
                <tr>
                    <td>
                        <select name="school" id="school" class="form-control">
                            <?php
                            $data=CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');

                            echo CHtml::tag('option',
                                array('value'=>''),'Select School',true);
                            foreach($data as $value=>$name)
                            {
                                if($formvals['school']==$value):
                                    $selected=array('value'=>$value,'selected'=>'selected');
                                else:
                                    $selected=array('value'=>$value);
                                endif;
                                echo CHtml::tag('option',
                                    $selected,CHtml::encode($name),true);
                            }
                            if($formvals['school']=='0'):
                                $selectedother=array('value'=>0,'selected'=>'selected');
                            else:
                                $selectedother=array('value'=>0);
                            endif;
                            echo CHtml::tag('option',$selectedother,'Other',true);
                            ?>
                        </select>
                        <input type="text" name="schooltext" id="schooltext" class="form-control" value="<?php echo ($formvals['schooltext']? $formvals['schooltext']:'');?>" />
                    </td>
                    <td >
                        <select name="plan" id="plan" class="form-control" >
                            <option value="">Select Plan</option>
                            <?php
                            $plans=Plan::model()->findAll(array('condition'=>'TM_PN_Status=0'));

                            foreach($plans AS $plan):
                                if($formvals['plan']==$plan->TM_PN_Id):
                                    $selected='selected';
                                else:
                                    $selected='';
                                endif;;
                                echo "<option value='".$plan->TM_PN_Id."' ".$selected.">".$plan->TM_PN_Name."</option>";
                            endforeach;
                            ?>
                        </select>
                        <span style="color:red;display: none;;" class="error errordiv">Select A Plan</span>
                    </td>
                    <td>
                        <select name="chapter" id="chapter" class="form-control" >
                            <option value="">Select Chapter</option>
                            <?php
                            if(count($chapters)>0):
                                foreach($chapters AS $chapter):
                                    if($chapter['TM_TP_Id']==$_GET['chapter']):
                                        $selected='selected';
                                    else:
                                        $selected='';
                                    endif;
                            ?>
                            <option value="<?php echo $chapter['TM_TP_Id'];?>" <?php echo $selected;?> ><?php echo $chapter['TM_TP_Name'];?></option>
                            <?php endforeach;
                            endif;?>
                        </select>
                        <span style="color:red;display: none;;" class="error errordiv">Select A Chapter</span>
                    </td>
                    <td><input type="date" class="form-control" value="<?php echo ($formvals['fromdate']!=''?$formvals['fromdate']:'');?>" id="fromdate" name="fromdate" >
                        <span style="color:red;display: none;;" class="error errordivdate">Select A Date</span></td>
                    <td><input type="date" class="form-control" value="<?php echo ($formvals['todate']!=''?$formvals['todate']:'');?>" id="todate" name="todate" ></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td ><?php if(count($formvals)>0):?><a href="<?php echo Yii::app()->createUrl('student/Downloadchapter',array('plan'=>$formvals['plan'],'chapter'=>$formvals['chapter'],'school'=>$formvals['school'],'date'=>$formvals['date'],'fromdate'=>$formvals['fromdate'],'todate'=>$formvals['todate']))?>" class="btn btn-warning btn-md">Export</a><?php endif;?></td>
                    <td><button class="btn btn-warning btn-lg btn-block" name="search" value="search" type="submit">Search</button></td>
                    <td ><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="button">Clear</button></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<?php if($dataitems!=''):?>
    <div class="row brd1" style="width: 250%;">
        <div class="col-lg-12">
            <div class="grid-view" id="yw0">
                <table class="table table-bordered table-hover table-striped admintable">
                    <thead>
                    <tr >
                        <th rowspan="2">Student Username</th>
                        <th rowspan="2">Student Name</th>
                        <th rowspan="2">School</th>
                        <th rowspan="2">Parent Username</th>
                        <th rowspan="2">Parent Email</th>
                        <th rowspan="2">Subbscription</th>
                        <th rowspan="2">Number of Revisions</th>
                        <th rowspan="2">Total Number of Revisions</th>
                        <th rowspan="2">Number of Mocks</th>
                        <th rowspan="2">Total Number of Mocks</th>
                        <th rowspan="2">Number of Challenges</th>
                        <th rowspan="2">Total Number of Challenges</th>
                        <th rowspan="2">Number of Practices</th>
                        <th rowspan="2">Total Number of Practices</th>
                        <th rowspan="2">Points Scored</th>
                        <th rowspan="2">Total Points Scored</th>
                        <?php
                        if($_GET['chapter']!='none'):
                        $chapter=Chapter::model()->findByPk($_GET['chapter']);
                        ?>
                            <th colspan="2" ><?php echo $chapter['TM_TP_Name'];?></td>
                        <?php
                        endif;?>
                    </tr>
                    <tr>
                        <?php
                        if($_GET['chapter']!='none'):
                        ?>
                            <th>completed %</td>
                            <th>correct %</td>
                        <?php
                        endif;?>
                    </tr>
                    </thead>
                    <tbody id="reportitems">
                    <?php echo $dataitems;?>
                    </tbody>
                </table>
            </div>
        </div>
        <ul class="pagination" >
            <li><a href="#">Previous</a></li>
            <?php echo $nopages;?>
        </ul>
    </div>
<?php endif;?>
<script type="text/javascript">
    $(function(){
        $('#clear').click(function(){
            $('#student ').val('');
            $('#parent ').val('');
            $('#school ').val('');
            $('#plan ').val('');
            $('#chapter ').val('');
            return true;
        });
        $('#searchform').submit(function(){
            var error=0
            $('.error').hide()
            if($('#plan').val()=='')
            {
                $('.errordiv').show();
                error=1;

            }
            else
            {
                $('.errordiv').hide();
            }
            if($('#fromdate').val()=='')
            {
                $('.errordivdate').show();
                error=1;
            }
            else
            {
                $('.errordivdate').hide();
            }
            if(error==1)
            {
                return false;
            }
            else
            {
                return true;
            }
        });
        $('.date').click(function(){
            $('#fromdate').attr('disabled',false);
            $('#todate').attr('disabled',true);
        })
        $('.dates').click(function(){
            $('#fromdate').attr('disabled',false);
            $('#todate').attr('disabled',false);
        })
        <?php
            if($formvals['date']=='before'):
                echo  "$('#fromdate').attr('disabled',false);";
                echo  "$('#todate').attr('disabled',true);";
            elseif($formvals['date']=='after'):
                echo  "$('#fromdate').attr('disabled',false);";
                echo  "$('#todate').attr('disabled',true);";
            elseif($formvals['date']=='between'):
                echo  "$('#fromdate').attr('disabled',false);";
                echo  "$('#todate').attr('disabled',false);";
            endif;
        ?>
    });
</script>
<script type="text/javascript">
    $(function(){
        $('#schooltext').hide();
        $('#school').change(function(){
            if($(this).val()=='0')
            {
                $('#schooltext').show();
            }
            else
            {
                $('#schooltext').hide();
            }
        });
        $('#plan').change(function(){
            var plan=$('#plan').val();
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('student/getchapters');?>',
                dataType: "json",
                data: {plan:plan},
                success: function(data){
                    $('#chapter').html(data.result);
                }
            });
        });
    });

</script>