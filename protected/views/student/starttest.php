
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="contentarea">
        <h1 class="h125"> </h1>
        <div class="bs-example">
            <div class="panel panel-default">


                <div class="row">
                    <div class="col-md-4 col-lg-3 col-xs-9 col-lg-offset-0 col-md-offset-0 col-xs-offset-1">

                    </div>

                    <div class="col-md-12 col-lg-12 col-xs-12 text12525">
                        <h1 style="font-weight: 700;">
                            You are about to begin your revision.
                        </h1>
                    </div>

                    <div class="col-md-12" style="text-align: center;">
                        <h5 style="margin: 20px;"> <?php echo 'This test contains '.$totalquestions.' Questions.';?></h5>
                        <h5 style="margin: 20px;">Please keep your pen and paper ready. You will be doing all your working out on paper. If test has online component, you will enter your answers online. For paper only, you will be working out in paper. Online questions can be corrected by the system and you will be given scores at the end of your test.<!-- You can upload your answer sheet to the assignment for future reference.-->
 Good luck ! </h5>


                       <?php
                        if($totalpaper!=0 & $totalpaper!=$totalquestions):
                            echo '<h4 class="testclass" style="margin: 20px;color: rgb(255, 169, 0);">There are 2 parts to the test.</h4>
                                <ul class="list-group">
                                <li class="list-group-item">Part 1 will be online test('.$totalonline.').</li>
                                <li class="list-group-item">Part 2 will be Paper test ('.$totalpaper.').</li>
                                </ul>';
                        elseif($totalpaper==$totalquestions):
                            echo '<h4 class="testclass" style="margin: 20px;color: rgb(255, 169, 0);">This is a paper  test.</h4>';
                        else:
                            echo '<h4 class="testclass" style="margin: 20px;color: rgb(255, 169, 0);">This is an online test.</h4>';
                        endif;
                       ?>
                    </div>
                    <div class="col-md-12"  style="margin-bottom: 20px;text-align: center;">
                        <?php if($totalpaper==$totalquestions):?>
                            <a href="<?php if($test->TM_STU_TT_Mode=='0' || $test->TM_STU_TT_Mode=='1'): echo Yii::app()->createUrl('student/showpaper',array('id'=>$test->TM_STU_TT_Id)); endif;?>">
                                <button class="btn btn-warning" name="startTest" >Proceed To Part2</button>
                            </a>
                        <?php else:?>
                            <a href="<?php if($test->TM_STU_TT_Mode=='0' || $test->TM_STU_TT_Mode=='1'): echo Yii::app()->createUrl('student/RevisionTest',array('id'=>$test->TM_STU_TT_Id)); endif;?>">
                                <button class="btn btn-warning" name="startTest" >Proceed</button>
                            </a>
                        <?php endif;?>

                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<?php Yii::app()->clientScript->registerScript('showmarks', "

 $('.pie_progress').asPieProgress({
            namespace: 'pie_progress'
        });

        // Example with grater loading time - loads longer
        $('.pie_progress--slow').asPieProgress({
            namespace: 'pie_progress',
            goal: 1000,
            min: 0,
            max: 1000,
            speed: 200,
            easing: 'linear'
        });

        // Example with grater loading time - loads longer
        $('.pie_progress--countdown').asPieProgress({
            namespace: 'pie_progress',
            easing: 'linear',
            first: 120,
            max: 120,
            goal: 0,
            speed: 1200, // 120 s * 1000 ms per s / 100
            numberCallback: function(n){
                var minutes = Math.floor(this.now/60);
                var seconds = this.now % 60;
                if(seconds < 10) {
                    seconds = '0' + seconds;
                }
                return minutes + ': ' + seconds;
            }
        });
        $('#button_start').on('click', function(){
            $('.pie_progress').asPieProgress('start');
        });
        $('#button_finish').on('click', function(){
            $('.pie_progress').asPieProgress('finish');
        });
        $('#button_go').on('click', function(){
            $('.pie_progress').asPieProgress('go',80);
        });
        $('#button_go_percentage').on('click', function(){
            $('.pie_progress').asPieProgress('go','".$test->TM_STU_TT_Percentage."%');
        });
        $('#button_stop').on('click', function(){
            $('.pie_progress').asPieProgress('stop');
        });
        $('#button_reset').on('click', function(){
            $('.pie_progress').asPieProgress('reset');
        });
        $( '#button_go_percentage' ).trigger( 'click' );
");?>
