
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="contentarea">
        <h1 class="h125"> </h1>
        <div class="bs-example">
            <div class="panel panel-default">


                <div class="row">
                   <div class="col-md-4 col-lg-3 col-xs-9 col-lg-offset-0 col-md-offset-0 col-xs-offset-1" style="
                        margin-top: 30px;
                        margin-bottom: 30px;
                    ">
                       <!--  <div class="pie_progress" role="progressbar" data-goal="<?php echo $test->TM_STMK_Percentage;?>" aria-valuemin="0" aria-valuemax="100">
                            <span class="pie_progress__number">0%</span>
                        </div> -->
                       <?php if(Yii::app()->session['userlevel']==1): ?>
                        <div style="
                            height: 150px;
                            width: 150px;
                            border: 3px solid #800000;
                            border-radius: 75px;
                            display: table;
                            margin: auto;
                        ">
                       <?php else: ?>
                           <div style="
                            height: 150px;
                            width: 150px;
                            border: 3px solid orange;
                            border-radius: 75px;
                            display: table;
                            margin: auto;
                            ">
                       <?php endif; ?>
                               <h2 style="text-align: center;display: table-cell;vertical-align: middle;"><?php echo $test->TM_STMK_Percentage;?>%</h2>
                           </div>

                    </div>


                    <div class="col-md-6 col-lg-6 col-xs-12 text12525">
                        <h1 style="font-weight: 700;">You scored <?php echo $test->TM_STMK_Marks.'/'.$test->TM_STMK_TotalMarks;?></h1>
                        <!--<h4 style="font-size: 14px;">All hai the top scorer !!!</h4>-->

                        <h4 style="font-weight: 700;"><?php echo $this->ResultMessage($test->TM_STMK_Percentage);?></h4>
                        <?php
                        $level=Levels::model()->findByPk($pointlevels['studentlevel']);
                        if($pointlevels['newlevel']):
                            ?>
                            <p class="yf">YOU ARE NOW A <?php echo strtoupper($level->TM_LV_Name);?> !!</p>
                        <?php endif;?>                        
                        <?php if($pointlevels['newlevel']):?>
                            <img class="img-responsive" style="margin: auto" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$level->TM_LV_icon.'?size=large&type=level&width=150&height=150';?>" alt="">
                            <h1 style="font-weight: 700;margin-bottom: 20px;">Congratulations !!</h1>
                        <?php endif;

                        if(Yii::app()->session['userlevel']==1):

                        else:
                            $level=Levels::model()->find(array('condition'=>'TM_LV_Minpoint >'.$pointlevels['total'].'','order'=>'TM_LV_Minpoint ASC'));
                            if($level & !$pointlevels['newlevel']):
                                $nextpoint=$level->TM_LV_Minpoint-$pointlevels['total'];
                                echo '<h4 style="font-weight: 700;margin-bottom: 20px;">'.$nextpoint.' more points to be a '.strtoupper($level->TM_LV_Name).'. Go for it !!!</h4>';
                            endif;
                        endif;
                        ?>
                    </div>

                    <div class="col-md-3">
                        <?php if($test->TM_STMK_Percentage>=40):?>
                            <i class="fa fa-thumbs-up fa-6 hvr-buzz-out mCS_img_loaded"></i>
                        <?php else:?>
                            <i class="fa fa-thumbs-down fa-6 hvr-buzz-out mCS_img_loaded"></i>
                        <?php endif;?>
                    </div>
                </div>


            </div>
        </div>

        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">Here is your mock result</h1>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div style="text-align: right;">
                        View Solution <img id="viewsolution" data-toggle="modal" data-target="#solutionModel" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" style="margin-left:10px;cursor: pointer">

                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table" style="background-color: #ffffff;">
                        <thead>

                        </thead>
                        <tbody class="font3 wirisfont">
                        <?php echo $testresult;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="solutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>

            </div>
            <div class="panel-heading">
                <div style="text-align: right;">
<!--                    <span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('student/GetpdfMockAnswer',array('id'=>$id));?>" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('student/GetpdfMockAnswer',array('id'=>$id));?>" target="_blank"><i class="fa fa-print fa-lg"></i></a>-->

                </div>
            </div>
            <div class="modal-body" >
                <table class="table ">
                    <thead>

                    </thead>
                    <tbody class="font3 wirisfont" id="showsolution">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php Yii::app()->clientScript->registerScript('showmarks', "
    $('#viewsolution').click(function(){
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/GetmockSolution')."',
          data: { testid: '".$test->TM_STMK_Id."'}
        })
          .done(function( html ) {
             $('#showsolution').html( html );
          });
    });
	$('.pie_progress').asPieProgress({
            namespace: 'pie_progress'
        });

            $('.pie_progress').asPieProgress('go','".floor($test->TM_STMK_Percentage)."%');
");?>

<?php Yii::app()->clientScript->registerScript('sendmail', "
    $(document).on('click','.showSolutionItem',function(){
            var item= $(this).attr('data-reff');
            $('.sendmail').slideUp('fast');
            $('.suggestiontest'+item).slideDown('slow');



    });

    $(document).on('click','#mailSend',function(){
var questionReff=$(this).data('reff');
var questionId=$(this).data('id');
var comments= $('#comment'+questionId).val();
comments=comments.trim();
if(comments.length>0){
    $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {
$('.mailSendComplete'+questionId).slideDown();
setTimeout(function(){
$('.mailSendComplete'+questionId).hide();
$('.sendmail').slideUp();
 $('.comment'+questionId).val('');
}, 6000);
 })
}
   else{
$('.comment'+questionId).css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
    $('.comment'+questionId).val('');
  $('.comment'+questionId).attr('placeholder', 'Please enter your comments before clicking send');

setTimeout(function() {
                    $('.comment'+questionId).css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
               $('.comment'+questionId).attr('placeholder', 'Enter your comments here');
                }, 5000);
                return false;

                return false;

}

    });
");?>


<?php Yii::app()->clientScript->registerScript('sendmailtest', "
    $(document).on('click','.showSolutionItemtest',function(){
            var item= $(this).attr('data-reff');
            $('.sendmailtest').slideUp('fast');
            $('.suggestion'+item).slideDown('slow')


    });

    $(document).on('click','#mailSendtest',function(){
    var questionId=$(this).data('id');
    var comments= $('#comments'+questionId).val();
    comments=comments.trim();
    if(comments.length>0){

        var count=0;
    
        var questionReff=$(this).data('reff');
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/SendMail')."',
          data: { questionId: questionId,questionReff: questionReff,comments: comments}
    
        })
        .done  (function(data, textStatus, jqXHR)        {
        $('.mailSendCompletetest'+questionId).slideDown();
        setTimeout(function(){
        $('.mailSendCompletetest'+questionId).hide();
        $('.sendmailtest').slideUp();
        var comments= $('.comments'+questionId).val('');
        }, 6000);
         })
    }
else{
    $('.comments'+questionId).css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
    $('.comments'+questionId).attr('placeholder', 'Please enter your comments before clicking send');
    $('.comments'+questionId).val('');    
    setTimeout(function() {
    $('.comments'+questionId).css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
    $('.comments'+questionId).attr('placeholder', 'Enter your comments here');
    }, 5000);
    return false;
}
    });
");
?>