<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
        <h1 class="h125"> </h1>

        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">

                <div class="list-group wizard-menu">


                    <li class="list-group-item">
                        <div class="col-md-12">
                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/goloden.png', '', array('class' => 'tocenter')); ?>
                            <h1 class="text-center">Leaderboard</h1>
                        </div>

                        <div class="clearfix"></div>

                    </li>

                    <li class="list-group-item nonbrd">
                        <div class="col-xs-6 col-sm-6 ">
                            <?php 
                                if(Yii::app()->session['school']):
                                    $url='student/schoolLeaderboard';
                                else:
                                    $url='student/leaderboard';
                                endif;    
                            ?>
                            <a class="btn btn-success btn-outline btn-sm outlbutmgr darkfont" href="<?php echo Yii::app()->createUrl($url,array('startdate'=>date('Y-m-d'),'mode'=>'month'))?>">Month</a><br />
                            <?php if($mode=='month'):?><span class="name">(<?php echo $dates[2];?>)</span><br><?php endif;?>
                        </div>
                        <div class="col-xs-6 col-sm-6 rits">
                            <a class="btn btn-success btn-outline btn-sm outlbutmgr darkfont" href="<?php echo Yii::app()->createUrl($url,array('startdate'=>date('Y-m-d'),'mode'=>'total'))?>">Total</a><br />                            
                        </div>                                                                        
                        <div class="clearfix"></div>

                    </li>





                    <?php
                    $hasuser=false;
                    foreach($studenttotal AS $leader):
                        if($leader->TM_STP_Student_Id==Yii::app()->user->id):
                            $class='list-group-item active';
                            $hasuser=true;
                        else:
                            $class='list-group-item ';
                        endif;
                        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$leader->TM_STP_Student_Id));
                        $studentprofile=User::model()->findByPk($leader->TM_STP_Student_Id);
                        ?>
                    <li class="<?php echo $class;?>">
                        <!--<div class="col-xs-3 col-lg-2 col-sm-2 col-md-3 vcenter">
                            <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($parent->profile->image!=''?$parent->profile->image:"noimage.png").'?size=large&type=user&width=80&height=80';?>" alt="<?php echo ucfirst($student->TM_STU_First_Name).' '.ucfirst($student->TM_STU_Last_Name);?>" class="img-responsive img-circle imgbrd2">
                        </div>-->

                        <div class="col-xs-7 col-lg-10 col-sm-5 col-md-10 vcenter">
                            <span class="name"><?php echo ucfirst($studentprofile->username);?></span><br>
                            <?php if(!$school):?>
                            <span class="text-muted"><?php echo ($student->TM_STU_State!=''?ucfirst($student->TM_STU_State).', ':'').ucfirst(Country::model()->findByPk(array($student->TM_STU_Country_Id))->TM_CON_Name);?></span>
                            <?php endif;?>
                        </div>
                        <div class="col-xs-3 col-lg-1 col-sm-2 col-md-1 vcenter">
                            <span class="vcenter pull-right" style="font-size: 43px;"><?php echo $leader->totalpoints;?></span>
                        </div>
                    </li>
                    <?php endforeach;
                    if(!$hasuser):

                        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));
                        $studentprofile=User::model()->findByPk(Yii::app()->user->id);
                    ?>
                    <li class="list-group-item active">
                        <!--<div class="col-xs-3 col-lg-2 col-sm-2 col-md-3 vcenter">
                            <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($student->TM_STU_Image!=''?$student->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140';?>" alt="<?php echo ucfirst($student->TM_STU_First_Name).' '.ucfirst($student->TM_STU_Last_Name);?>" class="img-responsive img-circle imgbrd2">
                        </div>-->

                        <div class="col-xs-7 col-lg-10 col-sm-5 col-md-10 vcenter">
                            <span class="name"><?php echo ucfirst($student->TM_STU_First_Name);?></span><br>
                            <?php if(!$school):?>
                            <span class="text-muted"><?php
                            $country=Country::model()->find(array('condition'=>'TM_CON_Id='.$student->TM_STU_Country_Id));                            
                             echo ucfirst($student->TM_STU_State).', '.$country->TM_CON_Name;?></span>
                             <?php endif;?>
                        </div>
                        <div class="col-xs-3 col-lg-1 col-sm-2 col-md-1 vcenter">
                            <span class="vcenter pull-right" style="font-size: 43px;"><?php echo ($usertotal->totalpoints==''?'0':$usertotal->totalpoints);?></span>
                        </div>

                    </li>
                    <?php endif;
                    $currentweek=$this->GetWeek(date('Y-m-d'));/*
                    ?>
                    <li class="list-group-item">
                        <ul class="pager" style="margin: 0px;">
                            <li class="previous"><a href="<?php echo Yii::app()->createUrl($url,array('startdate'=>$dates[3],'mode'=>$mode))?>"><span aria-hidden="true">←</span> Previous</a></li>
                            <?php if($dates[1]!=$currentweek[1]):?>
                            <li class="next " ><a href="<?php echo Yii::app()->createUrl($url,array('startdate'=>$dates[4],'mode'=>$mode))?>" >Next<span aria-hidden="true">→</span></a></li>
                            <?php endif;?>
                        </ul>
                    </li>
                    <?php */?>



                </div>

            </div>
        </div>
    </div>


















</div>