	<b>Overall % completed</b>
                    	<div class="row"  style="margin-bottom: 40px;">
                    	  <div class="col-xs-6 col-sm-6">										
                    			<div class="pie_progress4" role="progressbar" data-goal="<?php echo $progressstatus['average']?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress4__number"><?php echo $progressstatus['average']?>%</span>							
                    			</div>
                    			<p>Me</p>
                    	  </div>
                    	  <div class="col-xs-6 col-sm-6">
                    		<div class="pie_progress4 " role="progressbar" data-goal="<?php echo $progressstatus['restaverage'];?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress4__number"><?php echo $progressstatus['restaverage'];?>%</span>							
                    		</div>
                    			<p>Rest of the crowd</p>
                    	  </div>
                    	</div>
                    	
                    	<strong>Overall : Correct vs Incorrect</strong><br>
                    	<small>(From attempted topics)</small>
                    	<div class="row">
                    	  <div class="col-xs-6 col-sm-6">
                    		<div class="pie_progress3" role="progressbar" data-goal="<?php echo $progressstatus['correct']?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress3__number"><?php echo $progressstatus['correct']?>%</span>							
                    		</div>
                            <p>Me</p>
                    	  </div>
                    	  <div class="col-xs-6 col-sm-6">
                    		<div class="pie_progress3" role="progressbar" data-goal="<?php echo $progressstatus['restcorrect'];?>" aria-valuemin="1" aria-valuemax="100">
                    				<span class="pie_progress3__number"><?php echo $progressstatus['restcorrect'];?>%</span>							
                    		</div>
                            <p>Rest of the crowd</p>
                    	  </div>
                    	</div>