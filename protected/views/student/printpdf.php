<!DOCTYPE html>
<html>
<head>
    <!-- This line adds MathJax to the page with default SVG output -->
    <!--<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->


<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>-->
<style>
    .MathJax span
    {
        font-size: 14px;
    }
    tr {
        min-height: 5px;
        height: 5px;
    }
    .navbar-fixed-top
    {
        display: none !important;
    }
    #wrapper
    {
        padding:0 !important;
    }
    .sidebar{
        display: none;
    }
    body{
        visibility: hidden;
        color: #333 !important;
    }
    .container-fluid
    {
        visibility: visible;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }
    .row
    {
        margin-right: auto;
        margin-left: auto;
    }
   html
    {
        margin:0 !important;
        background-color: white;
    }
    td, th{
         padding: 0 !important; 
         border-bottom: none !important; 
         text-align: none !important; 
    }
    .container-fluid
    {
        background-color: white;
    }
    @media print {
        @page {
            margin:0;
            margin-bottom:0.6cm;
            /*margin-top:0.6cm;*/
        }
        html {margin:0cm}
        body{
            margin-top:0 !important;
        }
        .nonPrintingContent{
            display:none;
        }
        #footer {
            display: block !important; 
            position: relative; 
            bottom: 0;
        }
    }
</style>
</head>
<body >
    <div class="nonPrintingContent">
        <a href='javascript::void(0)' style='float:right;margin-right:10px;' class='btn btn-warning btn-xs' id="printWork">
        <span style='padding-right: 10px;'>Print</span>
        <i class='fa fa-print fa-lg'></i>
        </a>
    </div>
     <table id="footer" width="100%" style="font-size: 12px;
    font-family: STIXGeneral;display: none"> 
      <tr> 
          <td width="100%"> 
            <p>© Copyright @ TabbieMe Ltd. All rights reserved .</p>
        </td>
        <td>
            <p>www.tabbiemath.com</p>
          </td>
      </tr>
    </table>
   <?php
	echo $html;
	?> 
     
    <!--<div id="mpdf-create">-->
    <!--    <form autocomplete="off" action="<?php echo Yii::app()->request->baseUrl; ?>/teachers/printAll" method="POST" id="pdfform" onSubmit="document.getElementById('bodydata').value=encodeURIComponent(document.body.innerHTML);">-->
    <!--    <input type="submit" value="PDF" name="submit"/>-->
    <!--    <input type="hidden" value="" id="bodydata" name="bodydata" />-->
    <!--    </form>-->
    <!--</div>-->
</body>
 <?php
// $documentTemplate = $html;
// foreach ($_POST as $key => $postVar)
// {
//     $documentTemplate = 
//     preg_replace ("/name=\"$key\"/", "value=\"$postVar\"", $documentTemplate);
// }
// file_put_contents ("out.html", $documentTemplate);
// shell_exec ("wkhtmltopdf out.html test.pdf");
 ?>
<script>
//  $( document ).ready(function() {
//      setTimeout(function(){
//     //     let doc = new jsPDF('p','pt','a4');
//     // doc.addHTML(document.body,function() {
//     //     doc.save('html.pdf');
//     // });
//     window.print();
//      }, 3000);
// });
$( document ).ready(function() {
    $('#printWork').click(function(){
        // window.scrollTo(0, 0);
        window.print();
    });
});
</script>
</html>