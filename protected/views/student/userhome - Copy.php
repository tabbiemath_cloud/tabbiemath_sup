<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
    <?php if(Yii::app()->session['quick'] || Yii::app()->session['difficulty']):?>
        <div class="bs-example">

            <div class="sky1">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky1.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <h1 class="h125">You have the following open self revisions</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->

                <div class="table-responsive-vertical shadow-z-1">
                    <!-- Table starts here -->


                    <div class="table-responsive shadow-z-1">
                        <table class="table table-hover table-mc-light-blue">
                            <thead>
                            <tr>
                                <th>Published On</th>

                                <th>Topic</th>
                                <th>Questions</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody class="font2">
                            <?php
                            if(count($studenttests)>0):
                            foreach ($studenttests AS $tests):
                                ?>
                                <tr id="<?php echo 'testrow' . $tests->TM_STU_TT_Id;?>">
                                    <td data-title="Date"> <?php echo Yii::app()->dateFormatter->formatDateTime(strtotime($tests->TM_STU_TT_CreateOn), 'long', false); ?></td>

                                    <td data-title="Division"><?php
                                        foreach ($tests->chapters AS $key => $chapter):
                                            echo ($key != '0' ? ' ,' : '') . $chapter->chapters->TM_TP_Name;
                                        endforeach;?></td>
                                    <td>
                                        <?php echo $this->GetQuestionCount($tests->TM_STU_TT_Id);?>
                                    </td>
                                    <td data-title="Link" colspan="3"><span class="pull-right">
                                        <?php
                                        if (Student::model()->GetTestStatus($tests->TM_STU_TT_Id) > 0):
                                            if ($tests->TM_STU_TT_Status == '2'):
                                                echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/showpaper', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Submit Paper</a>";
                                            else:
                                                echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/RevisionTest', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Continue</a>";
                                            endif;
                                        else:
                                            echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/starttest', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Start</a>";
                                        endif;
                                        ?>
                                    </span> </td>


                                    <td data-title="Delete">
                                       <span class="pull-right" data-toggle="tooltip" data-placement="top" title="Delete Revision"> <?php echo CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/trash.png', '', array('class' => 'hvr-buzz-out','id' => 'some-name-'.uniqid())),
                                               Yii::app()->createUrl('student/deletetest', array('id' => $tests->TM_STU_TT_Id)),
                                            array(
                                                'type' => 'POST',
                                                'success' => 'function(html){ if(html=="yes"){$("#testrow' . $tests->TM_STU_TT_Id . '").remove();} }'
                                            ),
                                            array('confirm' => 'Are you sure you want to delete this revision?')
                                        );?></span>

                                    </td>
                                </tr>
                            <?php endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="6" class="font2" style="text-align: center" align="center"><a href="<?php echo Yii::app()->createUrl('student/revision');?>" class="btn btn-warning">Add revision test</a> </td>
                            </tr>
                            <?php endif;?>
                            </tbody>
                        </table>
                    </div>


                </div>


            </div>
            <div
                class="sky3"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    <?php endif;?>

    <?php if(Yii::app()->session['challenge']):?>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">You have the following open challenges</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table table-hover table-mc-light-blue">
                        <thead>
                        <tr>
                            <th>Set By</th>
                            <th>Set On</th>
                            <th>Questions</th>
                            <th>Set For</th>

                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody class="font2">
                        <?php
                        if(count($challenges)>0):
                        foreach ($challenges AS $challenge):

                            ?>
                            <tr id="challengerow<?php echo $challenge->challenge->TM_CL_Id;?>">
                                <td data-title="Date"><?php echo($challenge->challenge->TM_CL_Chalanger_Id == Yii::app()->user->id ? 'Me' : $this->GetChallenger($challenge->challenge->TM_CL_Chalanger_Id));?></td>
                                <td data-title="Publisher"><?php echo date("d-m-Y", strtotime($challenge->challenge->TM_CL_Created_On)); ?></td>
                                <td>
                                    <?php echo $challenge->challenge->TM_CL_QuestionCount;?>
                                </td>


                                <td data-title="Division" id="buddies<?php echo $challenge->challenge->TM_CL_Id;?>"><?php echo $this->GetChallengeInvitees($challenge->challenge->TM_CL_Id, Yii::app()->user->id);?></td>


                                <?php if($challenge->challenge->TM_CL_Chalanger_Id==Yii::app()->user->id):?>
                                    <td>

                                        <span data-toggle="tooltip" data-placement="top" title="Invite Buddies">
                                        <a href="javascript:void(0)"  target="_blank" class="btn btn-info choosebuddies" data-id="<?php echo $challenge->challenge->TM_CL_Id;?>" data-toggle="modal" data-target="#invitModel" style="width: 40%;"><span class="fa fa-users" aria-hidden="true" ></span></a></span>

                                    </td>
                                <?php endif;?>
                                <?php echo $this->getChallengeLinks($challenge->challenge->TM_CL_Id, Yii::app()->user->id);?>

                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="6" class="font2" style="text-align: center" align="center"><a href="<?php echo Yii::app()->createUrl('student/challenge');?>" class="btn btn-warning">Add Challenge</a> </td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal fade " id="invitModel" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Invite Buddies</h4>
                            </div>
                            <form action="" method="post" id="inviteform">
                            <div class="modal-body" >
                                <input type="hidden" name="challengeid" id="challengeid" value="32">
                                <input type="text" id="usersuggest" name="invitebuddies[]">
                                <span class="alert alert-danger error" role="alert" style="display: none;"></span>
                            </div>
                            <div class="modal-footer">
                                <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                                <button type="button" class="btn btn-default" id="invitebuddies" >Invite</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>
    <?php endif;?>
    <?php if(Yii::app()->session['mock']):?>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">You have the following mocks</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table table-hover table-mc-light-blue">
                        <thead>
                        <tr>
                            <th>Mock</th>
                            <th>Description</th>
                            <th>Created On</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody class="font2">
                        <?php
                        if(count($studentmocks)>0):
                        foreach ($studentmocks AS $mock):
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type NOT IN (6,7)';
                            $totalonline=MockQuestions::model()->count($criteria);
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type IN (6,7)';
                            $totalpaper=MockQuestions::model()->count($criteria);
                            $totalquestions=$totalonline+$totalpaper;
                            ?>
                            <tr id="mockrow<?php echo $mock->TM_STMK_Id;?>">
                                <td data-title="Date"><?php echo $mock->mocks->TM_MK_Name;?></td>
                                <td data-title="Date"><?php echo $mock->mocks->TM_MK_Description;?></td>
                                <td data-title="Publisher" colspan="3"><?php echo date("d-m-Y", strtotime($mock->TM_STMK_Date)); ?></td>
                                <td ><span class="pull-right">
                                    <?php
                                        if($totalquestions==$totalpaper):
                                            echo "<a href='" .Yii::app()->createUrl('student/Showmockpaper', array('id' => $mock->TM_STMK_Id)). "' class='btn btn-warning'>Continue</a>";
                                        else:
                                            echo "<a href='" .Yii::app()->createUrl('student/mocktest', array('id' => $mock->TM_STMK_Id)). "' class='btn btn-warning'>Continue</a>";
                                        endif;
                                    ?></span></td>
                                <td ><span class="pull-right" data-toggle="tooltip" data-placement="top" title="Delete Mock"><?php echo CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/trash.png', '', array('class' => 'hvr-buzz-out')),
                                        Yii::app()->request->baseUrl.'/student/deletemock/' . $mock->TM_STMK_Id,
                                        array(
                                            'type' => 'POST',
                                            'success' => 'function(html){ if(html!="no"){$("#mockrow'.$mock->TM_STMK_Id. '").remove();} }'
                                        ),
                                        array('confirm' => 'Are you sure you want to delete this mock?')
                                    );?></span></td>
                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="6" class="font2" style="text-align: center" align="center"><a href="<?php echo Yii::app()->createUrl('student/mock');?>" class="btn btn-warning">Add mock test</a> </td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>
    <?php endif;?>
    </div>


</div>
<?php Yii::app()->clientScript->registerScript('sendmailreminder', "
 $(document).on('click','.reminder',function(){
  var recepientid= $(this).attr('data-id');

  $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('student/SendMailReminders')."',
                  data: { recepientid: recepientid}

                })
                .done  (function(data, textStatus, jqXHR){
                $('.rem'+recepientid).remove();
                $('.remsend'+recepientid).slideDown();
                setTimeout(function(){
                        $('.remsend'+recepientid).remove();
                    },6000)

                });

 });"); ?>
<script type="text/javascript">
    $(function(){
        
        function getparams()
        {            
            alert(document.getElementById('challengeid').value);
        }
        var suggest=$('#usersuggest').magicSuggest({
            allowFreeEntries: false,
            maxSelection: 10,             
            data:'<?php echo Yii::app()->request->baseUrl."/site/getBuddies";?>'                       
        });
        $('.choosebuddies').click(function()
        {
            var challengeid=$(this).attr('data-id');
            $('#challengeid').val(challengeid);
            suggest.clear(); 
            suggest.setDataUrlParams({id:challengeid});               
        });
        $('#invitebuddies').click(function(){
            var challengeid=$('#challengeid').val();
            /*if($('#usersuggest').val()!='')
            {*/
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Invitebuddies";?>',
                data: $('#inviteform').serialize(),
                success: function(data){
                    if(data)
                    {
                        $('.inviteinfo').show();
                        $('.inviteinfo').text('Invite sent successful.');
                        $('#inviteform')[0].reset();
                        setTimeout(function(){
                            $('.close').trigger('click');
                            $('.inviteinfo').hide();
                        },3000);
                        $('#buddies'+challengeid).html(data);
                    }
                    else
                    {
                        $('.inviteinfo').show();
                        $('.inviteinfo').text('Invite failed. Please try again later.');
                        setTimeout(function(){
                            $('.inviteinfo').hide();
                        },3000);
                    }
                }
            });
           /* }
            else
            {
                $('.inviteinfo').show();
                $('.inviteinfo').text('Please select users.');
            }*/

        });
    });
    $(document).on('click','.hover',function(){
        var id=$(this).attr('data-id');
        var tooltip=$(this).attr('data-tooltip');
        $('.tooltiptable').hide();
        $('#tooltip'+id).fadeIn();
    });
    $(document).on('click','#close',function(){
        $('.tooltiptable').hide();

    });
</script>
