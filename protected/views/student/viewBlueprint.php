<!-- scrollable div -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="contentarea">
		<div class="bs-example">
			<h1 class="h125"><?php echo $model['TM_BP_Name']?></h1>
			<div style="overflow-x: auto;" class="panel panel-default clearfix">
				<div class="tab-pane fade active in" >
					<?php
					if(count($chapters) > 0)
					{
					?>
					<table class="rwd-tables">
						<tr>
							<th style="width:250px;"></th>
							<?php
							if(count($marks) > 0){
								foreach ($marks as $mark)
								{
									?>
									<th style="width:100px;
					text-align: center;"><?= $mark->TM_AV_MK_Mark.'-Mark' ?></th>
									<?php
								}
							}
							?>
						</tr>
						<tr>
							<th style="width:250px;">
								Total no of questions: <span id="totalNoOfQuest"></span><br>
								Total marks: <span id="totalMark"></span>
							</th>
							<?php
							if(count($marks) > 0)
							{
								foreach ($marks as $mark)
								{
									?>
									<th style="text-align:center;width:100px;"><span id="QuestionsInRow"></span><br>
										<span id="markInRow"></span>
									</th>
									<?php
								}
							}
							?>
						</tr>
						<?php
						foreach($chapters AS $chapter)
						{
						?>
						<tr style="background-color:#f6f6f6;border-bottom: 2px solid #d7d7d7;"><td style="padding-left: 10px;width:250px;"><?= $chapter->TM_TP_Name ?></td>
							<?php
							foreach ($marks as $mark)
							{
								$value = blueprintTemplate::model()->find(array('condition'=>'TM_BPT_Blueprint_Id='.$model->TM_BP_Id.' AND TM_BPT_Chapter_Id='.$chapter->TM_TP_Id.' AND TM_BPT_Mark_type='.$mark->TM_AV_MK_Id));?>
								<td style="padding: 10px;width:100px;
							text-align: center;">
									<input type="hidden" class="allowedNumbers" data-valueid=<?= $value->TM_BPT_Id ?> value=<?= $value->TM_BPT_No_questions ?> data-chapter=<?= $chapter->TM_TP_Id ?> data-mark=<?= $mark->TM_AV_MK_Mark ?> data-id=<?= $mark->TM_AV_MK_Id ?> name="allowedNumbers">
									<?= $value->TM_BPT_No_questions ?>
								</td>
								<?php
							}
							?>
							<?php } ?>
						</tr>
					</table>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function()
{
	var totalNoOfQuest = 0;
    var totalMarks = 0;
    $(".allowedNumbers").each(function(){
      var val = parseFloat($(this).val());
      var mark = parseFloat($(this).attr('data-mark'));
      if(!isNaN(val)) 
      {
        totalNoOfQuest = totalNoOfQuest+val;
        totalMarks = totalMarks+(val*mark);
      }
    });
    console.log(totalNoOfQuest);
    // console.log(totalMarks);
    $('#totalNoOfQuest').html(totalNoOfQuest);
    $('#totalMark').html(totalMarks);
 });
</script>