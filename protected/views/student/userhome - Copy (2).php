<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
    <?php if(Yii::app()->session['quick'] || Yii::app()->session['difficulty']):?>
        <div class="bs-example">
            <h1 class="h125">You have the following open self revisions</h1>

            <div class="panel panel-default">
			
                <div class="tab-pane fade active in" id="revisions">

                        <table class="rwd-tables">
                           <tbody>						   
                            <tr>
                                <th><b>Published On</b></th>

                                <th><b>Topic</b></th>
                                <th><b>Questions</b></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            <?php
                            if(count($studenttests)>0):
                            foreach ($studenttests AS $tests):
                                ?>
                                <tr id="<?php echo 'testrow' . $tests->TM_STU_TT_Id;?>">
                                    <td ><span class="rwd-tables thead"><b>Published On</b></span><span class="rwd-tables tbody"><?php echo Yii::app()->dateFormatter->formatDateTime(strtotime($tests->TM_STU_TT_CreateOn), 'long', false); ?></span></td>

                                    <td ><span class="rwd-tables thead"><b>Topic</b></span><span class="rwd-tables tbody"><?php
                                        foreach ($tests->chapters AS $key => $chapter):
                                            echo ($key != '0' ? ' ,' : '') . $chapter->chapters->TM_TP_Name;
                                        endforeach;?></span></td>
                                    <td><span class="rwd-tables thead"><b>Questions</b></span><span class="rwd-tables tbody"><?php echo $this->GetQuestionCount($tests->TM_STU_TT_Id);?></span>
                                    </td>
                                    <td colspan="4" class="text-right">
                                        <?php                                       
                                        if ($tests->TM_STU_TT_Status > 0):
                                            if ($tests->TM_STU_TT_Status == '1'):
                                                echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/RevisionTest', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Continue</a>";
                                            else:
                                                echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/showpaper', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Continue</a>";
                                            endif;
                                        else:
                                            echo "<a href='" . ($tests->TM_STU_TT_Mode == '1' || $tests->TM_STU_TT_Mode == '0' ? Yii::app()->createUrl('student/starttest', array('id' => $tests->TM_STU_TT_Id)) : '') . "' class='btn btn-warning'>Start</a>";
                                        endif;
                                        ?>
                                    </td>


                                    <td class="text-right">
                                       <span  data-toggle="tooltip" data-placement="top" title="Delete Revision"> <?php echo CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/trash.png', '', array('class' => 'hvr-buzz-out','id' => 'some-name-'.uniqid())),
                                               Yii::app()->createUrl('student/deletetest', array('id' => $tests->TM_STU_TT_Id)),
                                            array(
                                                'type' => 'POST',
                                                'success' => 'function(html){ if(html=="yes"){$("#testrow' . $tests->TM_STU_TT_Id . '").remove();} }'
                                            ),
                                            array('confirm' => 'Are you sure you want to delete this revision?')
                                        );?></span>

                                    </td>
                                </tr>
                            <?php endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="8" class="font2" style="text-align: center" align="center"><a href="<?php echo Yii::app()->createUrl('student/revision');?>" class="btn btn-warning">Add revision test</a> </td>
                            </tr>
                            <?php endif;?>
                            </tbody>
                        </table>


                </div>


            </div>
        </div>
    <?php endif;?>

    <?php if(Yii::app()->session['challenge']):?>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">You have the following open challenges</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="tab-pane fade active in">
                    <table class="rwd-tables">
                        
                        <tbody >
                        <tr class="bro_top_none">
							<th><b>Set By</b></th>
							<th><b>Set On</b></th>
							<th><b>Questions</b></th>
							<th><b>Set For</b></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>

                        <?php
                        if(count($challenges)>0):
                        foreach ($challenges AS $challenge):

                            ?>
                            <tr id="challengerow<?php echo $challenge->challenge->TM_CL_Id;?>">
                                <td><span class="rwd-tables thead"><b>Set By</b></span><span class="rwd-tables tbody"><?php echo($challenge->challenge->TM_CL_Chalanger_Id == Yii::app()->user->id ? 'Me' : $this->GetChallenger($challenge->challenge->TM_CL_Chalanger_Id));?></span></td>
                                <td><span class="rwd-tables thead"><b>Set On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($challenge->challenge->TM_CL_Created_On)); ?></span></td>
                                <td>
                                    <span class="rwd-tables thead"><b>Questions</b></span><span class="rwd-tables tbody"><?php echo $challenge->challenge->TM_CL_QuestionCount;?></span>
                                </td>
								<td id="buddies<?php echo $challenge->challenge->TM_CL_Id;?>">
								<span class="rwd-tables thead"><b>Set For</b></span><span class="rwd-tables tbody"><?php echo $this->GetChallengeInvitees($challenge->challenge->TM_CL_Id, Yii::app()->user->id);?></span>
								</td>								
								<td class="text-right">
									<?php if($challenge->challenge->TM_CL_Chalanger_Id==Yii::app()->user->id & $this->GetBuddies(Yii::app()->user->id)):?>
										<span data-toggle="tooltip" data-placement="top" title="Invite Buddies">
										<a href="javascript:void(0)" target="_blank" class="btn btn-info choosebuddies" data-id="<?php echo $challenge->challenge->TM_CL_Id;?>" data-toggle="modal" data-target="#invitModel" style="width: 30px;"><span class="fa fa-users" aria-hidden="true" ></span></a></span>										
									<?php endif;?>
								</td>
                                
                                <?php echo $this->getChallengeLinks($challenge->challenge->TM_CL_Id, Yii::app()->user->id);?>

                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="7" class="font2" style="text-align: center" align="center"><a href="<?php echo Yii::app()->createUrl('student/challenge');?>" class="btn btn-warning">Add Challenge</a> </td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal fade " id="invitModel" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Invite Buddies</h4>
                            </div>
                            <form action="" method="post" id="inviteform">
                            <div class="modal-body" >
                                <input type="hidden" name="challengeid" id="challengeid" value="32">
                                <input type="text" id="usersuggest" name="invitebuddies[]">
                                <span class="alert alert-danger error" role="alert" style="display: none;"></span>
                            </div>
                            <div class="modal-footer">
                                <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                                <button type="button" class="btn btn-default" id="invitebuddies" >Invite</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
<div id="myinvite" class="modal fade" role="dialog" style="padding-top:20%;">
  <div class="modal-dialog modal-sm modal-sm_plus">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Challenge position</h4>
      </div>
      <div class="modal-body modal-body_padding">
      <div class="row">
        <ul class='popu-cart' role='menu' style='display: block !important;'>                  
             
            </ul>
            </div>
      </div>
    </div>

  </div>
</div>
        </div>
    <?php endif;?>
    <?php if(Yii::app()->session['mock']):?>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">You have the following mocks</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="tab-pane fade active in" >
                    <table class="rwd-tables">
                        <tbody >
                        <tr>
                            <th>Mock</th>
                            <th>Description</th>
                            <th>Created On</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>                        
                        <?php
                        if(count($studentmocks)>0):
                        foreach ($studentmocks AS $mock):
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type NOT IN (6,7)';
                            $totalonline=MockQuestions::model()->count($criteria);
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type IN (6,7)';
                            $totalpaper=MockQuestions::model()->count($criteria);
                            $totalquestions=$totalonline+$totalpaper;
                            ?>
                            <tr id="mockrow<?php echo $mock->TM_STMK_Id;?>">
                                <td ><span class="rwd-tables thead"><b>Mock</b></span><span class="rwd-tables tbody"><?php echo $mock->mocks->TM_MK_Name;?></span></td>
                                <td ><span class="rwd-tables thead"><b>Description</b></span><span class="rwd-tables tbody"><?php echo $mock->mocks->TM_MK_Description;?></span></td>
                                <td ><span class="rwd-tables thead"><b>Created On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($mock->TM_STMK_Date)); ?></span></td>
                                <td colspan="4" class="text-right"><span >
                                        <?php                                       
                                        if ($mock->TM_STMK_Status > 0):
                                            if ($mock->TM_STMK_Status == '1'):
                                                echo "<a href='" .Yii::app()->createUrl('student/mocktest', array('id' => $mock->TM_STMK_Id)). "' class='btn btn-warning'>Continue</a>";
                                            else:
                                                echo "<a href='" .Yii::app()->createUrl('student/Showmockpaper', array('id' => $mock->TM_STMK_Id)). "' class='btn btn-warning'>Continue</a>";
                                            endif;
                                        else:
                                            echo "<a href='" .Yii::app()->createUrl('student/mocktest', array('id' => $mock->TM_STMK_Id)). "' class='btn btn-warning'>Start</a>";
                                        endif;
                                        ?>
                                   </span></td>
                                <td class="text-right"><span data-toggle="tooltip" data-placement="top" title="Delete Mock"><?php echo CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/trash.png', '', array('class' => 'hvr-buzz-out')),
                                        Yii::app()->request->baseUrl.'/student/deletemock/' . $mock->TM_STMK_Id,
                                        array(
                                            'type' => 'POST',
                                            'success' => 'function(html){ if(html!="no"){$("#mockrow'.$mock->TM_STMK_Id. '").remove();} }'
                                        ),
                                        array('confirm' => 'Are you sure you want to delete this mock?')
                                    );?></span></td>
                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="8" class="font2" style="text-align: center" align="center"><a href="<?php echo Yii::app()->createUrl('student/mock');?>" class="btn btn-warning">Add mock test</a> </td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>
    <?php endif;?>
    </div>


</div>
<?php Yii::app()->clientScript->registerScript('sendmailreminder', "
 $(document).on('click','.reminder',function(){
    var recepientid= $(this).attr('data-id');
    var item=$(this);
    $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMailReminders')."',
              data: { recepientid: recepientid},
              beforeSend: function() {
                item.text('sending reminder..')
              }
    })
    .done  (function(data, textStatus, jqXHR){
        $('.rem'+recepientid).slideUp();
        $('.remsend'+recepientid).slideDown();        
        setTimeout(function(){
            item.text(' Send Reminder')
            $('.remsend'+recepientid).slideUp();
        },6000)

    });
 });"); ?>
<script type="text/javascript">
    $(function(){
        
        function getparams()
        {            
            alert(document.getElementById('challengeid').value);
        }
        var suggest=$('#usersuggest').magicSuggest({
            allowFreeEntries: false,
            maxSelection: 10,                       
            data:'<?php echo Yii::app()->request->baseUrl."/site/getBuddies";?>'                       
        });
        $('.choosebuddies').click(function()
        {
            $('.nobuddies').hide();
            var challengeid=$(this).attr('data-id');
            $('#challengeid').val(challengeid);
            suggest.clear(); 
            suggest.setDataUrlParams({id:challengeid});
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/site/getPlanBuddies";?>',
                data: {id:challengeid},
                success: function(data){
                    if(data=='0')
                    {
                        $('.nobuddies').html('You have no buddies for the selected plan').show();
                    }
                }
                });
                          
        });
        $('#invitebuddies').click(function(){
            var challengeid=$('#challengeid').val()                      
            if(suggest.getValue()!='')
            {
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Invitebuddies";?>',
                data: $('#inviteform').serialize(),
                success: function(data){
                    if(data!='no')
                    {
                        $('.inviteinfo').show();
                        $('.inviteinfo').text('Invite sent successful.');
                        $('#inviteform')[0].reset();
                        setTimeout(function(){
                            $('.close').trigger('click');
                            $('.inviteinfo').hide();
                        },3000);
                        $('#buddies'+challengeid).html(data);
                    }
                    else
                    {                       
                        $('.inviteinfo').html('Invite failed. Please try again later.');
                        $('.inviteinfo').show();
                        setTimeout(function(){
                            $('.inviteinfo').hide();
                        },3000);
                    }
                }
                });
            }
            else
            {
                $('.inviteinfo').show();
                $('.inviteinfo').text('Please select users.');
            }

        });
    });
    /*$(document).on('click','.hover',function(){
        var id=$(this).attr('data-id');
        var tooltip=$(this).attr('data-tooltip');
        $('.tooltiptable').hide();
        $('#tooltip'+id).fadeIn();
    });*/
    $(document).on('click','.hover',function(){
        $('.popu-cart').html(''); 
        var id=$(this).attr('data-id');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetChallengeBuddies";?>',
                data: {challenge:id},
                success: function(data){
                    if(data!='no')
                    {
                        $('.popu-cart').html(data);                        
                    }
                }
            });
    });
    $(document).on('click','#close',function(){
        $('.tooltiptable').hide();

    });
</script>
