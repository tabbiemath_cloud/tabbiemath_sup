<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="contentarea">
        <h1 class="h125"> </h1>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">Here is your Assessment result</h1>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div style="text-align: right;">
                        View Solution <img id="viewsolution" data-toggle="modal" data-target="#solutionModel" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" style="margin-left:10px;cursor: pointer">

                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table" style="background-color: #ffffff;">
                        <thead>

                        </thead>
                        <tbody class="font3 wirisfont">
                        <?php echo $testresult;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>