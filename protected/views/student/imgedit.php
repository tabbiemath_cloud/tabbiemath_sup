
<html> 
  <head> 
    <style type="text/css">
      .marker-text {
    position: absolute;
     top: 21px;
    width: 100%;
    margin: 0 0 0 0;
    z-index: 1;
    font-size: 12px;
    font-weight: bold;
    text-align: center;
    color: black;
}
p{
    color: black;
    font-size: 14px;
    font-family: monospace;
}


 .commentli {
    margin-right: 11px;
    background: #ec9d00;
    border-radius: 100%;
    color: black;
    width: 1.4em !important;
    min-width: 19px;
    text-align: center;
    display: inline-block;
    height: 20px;
 }
 .cke_top {
  display: none;
 }
    @media print {

        @page {
        }
        .sidebar{
            display:none;
        }
        .printinvoice
        {
          display: none;
        }

    #imgtag + .row {
        padding: 0px !important;
        min-height: initial !important;
        display: block !important;
      }

    }
#imgtag img { cursor: pointer !important;  }

.tagview{ cursor: pointer !important; }
  </style>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
  <title>Tabbiemath Image Comment</title>

   <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/tagging.css">
    <script>
        CKEDITOR.env.isCompatible = true;
    </script>
   <script type="text/javascript">
    
    $(document).on('click','.tagview',function(){
    CKEDITOR.instances['commentedit'].setData('Loading data...');
    jQuery.event.props.push('dataTransfer');
    var id = $(this).data('id');
    $('#comment_id').val(id);
    $('#imgid').val(id);
        $.ajax({
        type: "POST", 
        url: "getdata", 
        data: {id:id},
        success: function(data) {
            //$('.insertcomment').html(data);
            CKEDITOR.instances['commentedit'].setData(data);
           

        }
      });
     $('#server_msg_modal').modal('show');
  });
   </script>

  </head> 
  <body>
  </div>

  <div class="modal fade" id="server_msg_modal">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Comment</h4>
        </div>
        <div class="modal-body">
            <form method="post" id="editform" >
             <textarea  id="commentedit" name="commentedit" disabled class="form-control insertcomment">
             
            </textarea> 
            <div class="insertcomment"></div>
            <input type="hidden" value=""  id="comment_id" name="commentid">
            <div id="imgclass"></div>
            <div class="cl-md-6">
            </div>
        </div>
        <div class="modal-header">
            <button type="button" id="close"  data-dismiss="modal" class="btn btn-default">Cancel</button>
            <input type="hidden" value="" id="imgid" name="">
            <input type="hidden" name="update" value="1">
            <input type="hidden" id="delete" name="delete" value="">
            </form>
        </div>
    </div>
  </div>
</div>

  <div id="container">
    <button style="margin-top: 53px;" class="btn btn-warning printinvoice"> Print</button>
  <div id="imgtag"> 
    <img width="100%" id="my_image" src="<?php echo Yii::app()->request->baseUrl?>/homework/<?php echo $_GET['name'];  ?>" /> 
    <div id="tagbox">
     
     <?php $comments=Imagetag::model()->findAll(array('condition' => "assignment_id='".$_GET['assignment']."' AND pic_id LIKE'".$_GET['name']."'")); 
     if($comments){
      $comm=0;
    foreach ($comments as $key => $comment) {
      $comm++;
    ?>

      <div data-id="<?php echo $comment->id; ?>" class="tagview" style="opacity: 1.0; left:<?php echo $comment->pic_x; ?>px;top:<?php echo $comment->pic_y; ?>px; margin-top: -27px; margin-left: -17px;" id="view_<?php echo $comm;  ?>">
      <img src="<?php echo Yii::app()->getBaseUrl() ?>/images/bubble-icon.png">
      <p class="marker-text"><?php echo $comm; ?></p>
    </div>

     <?php } } ?>
    </div>
  </div> 

  <div class="row" style="background: #edf2f2; min-height: 200px; margin-left: 0px; width: 851px;">
    <div class="col-md-12" style="text-align:left; color:black; font-weight: bold; font-size: 20px;"><center><p>Tabbiemath - View Comments</p></center></div>
  <div  class="col-md-12 red"   style="text-align:left; color:black; font-weight: bold;"> <p style="font-size: 20px;">Teacher's Comments</p></div>
    <?php $comments=Imagetag::model()->findAll(array('condition' => "assignment_id='".$_GET['assignment']."' AND pic_id LIKE'".$_GET['name']."'")); 
     if($comments){
      $comm=0;
    foreach ($comments as $key => $comment) {
      $comm++;
      if($key==0){
    ?>
    <div class="col-md-6" style="margin-top: 15px; display: flex;">
    <?php } elseif($key==1) { ?>
    <div class="col-md-6" style="margin-top: 15px; display: flex;">
    <?php } else{  ?>
    <div class="col-md-6" style="display: flex;">
    <?php  } ?>
       <span class="commentli"><P style="font-size: 14px;"><?php echo $comm; ?></P></span>
       <span style="word-break: break-word; text-decoration: none;}">
      <?php echo $comment->comments; ?> </span> 
    </div>  
  
      <?php } } ?>
    </div> 
  </div>   
  </body>
</html>


<?php
    Yii::app()->clientScript->registerScript('editor', "
         $( '#commentedit' ).ckeditor();
    ");
?>

<script type="text/javascript">
  $(document).on('click','.printinvoice',function(){
window.print();
  });
                  
</script>