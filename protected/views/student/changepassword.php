<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">

        <div class="bs-example">
            <h1 class="h125" >Change Password</h1>
            <div class="col-md-6" style="padding: 0px;;">
            <div  class="my_student-panel panel panel-default formdiv" >
                <div class="panel-body">
                <?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
                    </div>
                <?php endif;?>
                
                <?php $form=$this->beginWidget('UActiveForm', array(
                	'id'=>'changepassword-form',
                	'enableAjaxValidation'=>true,
                )); ?>
                    <fieldset>
                        <div class="form-group">
                            <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Enter New Password')); ?>                           
                            <?php echo CHtml::error($model,'password'); ?>
                            <!--<p class="hint">
                        	   <?php //echo UserModule::t("Minimal password length 4 symbols."); ?>
                        	</p>-->                             
                        </div>
                        <div class="form-group">
                            <?php echo $form->passwordField($model,'verifyPassword',array('class'=>'form-control','placeholder'=>'Re-enter Password')); ?>                            
                            <?php echo CHtml::error($model,'verifyPassword'); ?>
                        </div>
                        <div class="col-md-5 pull-right">
                            <!-- Change this to a button or input when using this as a form -->
                            <?php echo CHtml::submitButton(UserModule::t("Change Password"),array('class'=>'btn btn-warning btn-block')); ?>
                        </div>

                    </fieldset>
               <?php $this->endWidget(); ?>
               
            </div>
            </div>  
            </div>      
        </div>
    </div>
</div>    

