<style type="text/css">
    .cls
    {
        position: absolute;
        margin-top: -21px;
        right: 5px;
        background-color: rgb(19, 19, 19);
        opacity: 10;
        color: white;
        font-size: 16px;
        /* padding: 10px; */
        height: 20px;
        line-height: 17px;
        width: 20px;
        text-align: center;
        border-radius: 33px;
    }

</style>
<script type="text/javascript">
  
    jQuery(document).ready(function($){
        $('.pie_progress2').asPieProgress({
            namespace: 'pie_progress2'
        });       
		$('.pie_progress2').asPieProgress('go');        
    });
</script>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">

        <div class="bs-example">
            <h1 class="h125" >My History</h1>


                <div class="modal-body">


                    <div role="tabpanel" >
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <?php if(Yii::app()->session['quick'] || Yii::app()->session['difficulty']):?>
                                <li role="presentation" class="active"><a href="#revisions" aria-controls="revisions" role="tab" data-toggle="tab">My revisions</a></li>
                            <?php endif;?>
                            <?php if(Yii::app()->session['challenge']):?>
                                <li role="presentation" ><a href="#challenges" aria-controls="challenges" role="tab" data-toggle="tab">My challenges</a></li>
                            <?php endif;?>
                            <?php if(Yii::app()->session['mock']):?>
                                <li role="presentation" ><a href="#mocks" aria-controls="mocks" role="tab" data-toggle="tab">My Worksheets</a></li>
                            <?php endif;?>
                            <?php if(Yii::app()->session['school']):?>
                                <li role="presentation" class="active" ><a href="#homework" aria-controls="homework" role="tab" data-toggle="tab">My Tasks</a></li>
                            <?php endif;?>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade <?php echo (!Yii::app()->session['quick'] & !Yii::app()->session['difficulty'] & Yii::app()->session['challenge']?'active':'');?>" id="challenges">
                                <div class="panel panel-default">
                                    <div class="tab-pane fade active in" id="challenges">
                                        <table class="rwd-tables table-mc-light-blue">
                                            <tbody>


                                            <tr>
                                                <th><b>Set By</b></th>
                                                <th><b>Set On</b></th>
                                                <th><b>Set For</b></th>
                                                <th><b>My Score</b></th>
                                                <th><b>Position</b></th>
                                                <th><b>Result</b></th>
                                            </tr>

                                            <?php
                                            if(count($challenges)>0):
                                            foreach ($challenges AS $key => $viewChallenge):
                                                $testId = $viewChallenge->TM_CE_RE_Chalange_Id;
                                                $challenge = Challenges::model()->findByPk($viewChallenge->TM_CE_RE_Chalange_Id);
                                                $users = User::model()->findByPk($challenge->TM_CL_Chalanger_Id);
                                                //$creatorname = $users->username;
                                                $student = Student::model()->find(array('condition' => "TM_STU_User_Id=" . $challenge->TM_CL_Chalanger_Id));
                                                $creatorname = $student->TM_STU_First_Name . " " . $student->TM_STU_Last_Name;
                                                $criteria = new CDbCriteria;
                                                $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $testId);
                                                $criteria->order = 'TM_CE_RE_ObtainedMarks DESC';
                                                $participants = Chalangerecepient::model()->findAll($criteria);
                                                $userids = array();
                                                foreach ($participants AS $party):
                                                    $userids[] = $party->TM_CE_RE_Recepient_Id;
                                                endforeach;


                                                $count = Chalangerecepient::model()->count(
                                                    array(
                                                        'condition' => 'TM_CE_RE_Chalange_Id=:flag',
                                                        'params' => array(':flag' => $testId,)
                                                    ));?>
                                                <tr>
                                                    <td><span class="rwd-tables thead"><b>Set By</b></span><span class="rwd-tables tbody"><img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.(Student::model()->find(array('condition' => "TM_STU_User_Id='" . $users->id . "'"))->TM_STU_Image!=''?Student::model()->find(array('condition' => "TM_STU_User_Id='" . $users->id . "'"))->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140';?>"  class="img-circle tab_img">
                                                            <?php if (Yii::app()->user->getId() ==$challenge->TM_CL_Chalanger_Id) {
                                                                echo "Me";
                                                            } else {
                                                                echo $creatorname;
                                                            }?></span></td>
                                                    <td><span class="rwd-tables thead"><b>Set On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($challenge->TM_CL_Created_On)); ?></span>
                                                    </td>
                                                    <td><span class="rwd-tables thead"><b>Set For</b></span><span class="rwd-tables tbody"><?php echo $this->GetChallengeInvitees($challenge->TM_CL_Id, Yii::app()->user->id);?></span></td>
                                                    <?php
                                                    $challengecount=$challenge->TM_CL_QuestionCount;
                                                    if($challengecount=='10'):
                                                        $examtotalmarks='79';
                                                    elseif($challengecount=='15'):
                                                        $examtotalmarks='139';
                                                    elseif($challengecount=='20'):
                                                        $examtotalmarks='199';
                                                    elseif($challengecount=='25'):
                                                        $examtotalmarks='199';
                                                    endif;?>
                                                    <td><span class="rwd-tables thead"><b>My Score</b></span><span class="rwd-tables tbody"><?php echo $viewChallenge->TM_CE_RE_ObtainedMarks.'/'.$this->GetChallengetotal($viewChallenge->TM_CE_RE_Chalange_Id);?></span>
                                                    </td>
                                                    <?php  $criteria = new CDbCriteria;
                                                    $criteria->addCondition('TM_CE_RE_Chalange_Id='.$testId);
                                                    $criteria->order='TM_CE_RE_ObtainedMarks DESC';
                                                    $participants = Chalangerecepient::model()->findAll($criteria);
                                                    $recepients=count($participants);
                                                    ?>
                                                    <td><span class="rwd-tables thead"><b>Position</b></span><span class="rwd-tables tbody"><?php echo((count($participants))==1 ? "NA" : $this->GetChallengePosition($userids, Yii::app()->user->id)) ;?><a href="#"></a></span></td>
                                                    <td class="text-right"><a href="<?php echo Yii::app()->request->baseUrl."/student/challengeresult/".$viewChallenge->TM_CE_RE_Chalange_Id; ?>" title="View Test Result" target="_blank">View Test Results</a></td>
                                                </tr>
                                            <?php endforeach;
                                            else:?>
                                                <tr>
                                                    <td colspan="6" class="font2" style="text-align: center" align="center">No challenges found</td>
                                                </tr>
                                            <?php endif;?>
                                            </tbody>


                                        </table>
                                    </div>
                                </div>
                            </div>


                            <div role="tabpanel" class="tab-pane fade in <?php echo (Yii::app()->session['quick'] || Yii::app()->session['difficulty']?'active':'');?>" id="revisions">
                                <div class="panel panel-default">
                                    <div class="tab-pane fade in" id="revisions">
                                        
                                           <table class="rwd-tables table-mc-light-blue">
                                                <tbody>
                                                <tr>
												<th><b>Topic</b></th>
												<th><b>Date</b></th>
												<th class="Score"><b>Score</b></th>
												<th></th>
												<th></th>
												<th></th>                                                   
												<th></th>
                                                    <!--<th> </th>-->
                                                </tr>
                                                <?php
                                                if(count($totaltests)>0):
                                                $count=0;
                                                $script1='';
                                                foreach ($totaltests AS $viewTotaltests):
                                                    $totalsquestions=$this->GetRevisionQUestionCount($viewTotaltests->TM_STU_TT_Id);
                                                    ?>

                                                    <tr>
                                                        <td>
															<span class="rwd-tables thead"><b>Topic</b> </span><span class="rwd-tables tbody">
															<?php
																foreach($viewTotaltests->chapters AS $key=>$chapter):
																echo ($key!='0'?' ,':'').$chapter->chapters->TM_TP_Name;
																endforeach;?>
															</span>
														</td>



                                                        <td><span class="rwd-tables thead"><b>Date</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($viewTotaltests->TM_STU_TT_CreateOn)); ?></span></td>
                                                        <td><span class="rwd-tables thead"><b>Score</b></span><span class="rwd-tables tbody">
														<!--<div class="pie_progress2 showprogress<?php echo $count;?>" role="progressbar" data-goal="<?php echo round($viewTotaltests->TM_STU_TT_Percentage);?>" aria-valuemin="0" data-step="2" aria-valuemax="100"><span class="pie_progress2__number"><?php echo round($viewTotaltests->TM_STU_TT_Percentage)."%"?></span>
                                                            </div>-->
                                                        <div id="<?php echo 'progrev'.$count;?>" class="containerdiv" style="margin: auto;">
                                                            <div class="jCProgress" style="opacity: 1;">
                                                                <div class="percent" style="display: none;"><?php echo round($viewTotaltests->TM_STU_TT_Percentage);?></div>
                                                                <canvas width="55" height="55"></canvas>
                                                            </div>
                                                        </div>
                                                        <!--<script>$(function(){<?php /*echo "var mypluginrev".$count.";mypluginrev".$count." = $('#progrev".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STU_TT_Percentage).",loop : false,showPercent : true});";*/?>});</script>-->
                                                        <?php if(Yii::app()->session['userlevel']==1):?>
                                                            <script>$(function(){<?php echo "var mypluginrev".$count.";mypluginrev".$count." = $('#progrev".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STU_TT_Percentage).",loop : false,showPercent : true});";?>});</script>
                                                        <?php else:?>
                                                            <script>$(function(){<?php echo "var mypluginrev".$count.";mypluginrev".$count." = $('#progrev".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STU_TT_Percentage).",loop : false,showPercent : true});";?>});</script>
                                                        <?php endif;?>
                                                        </span>
                                                        </td>
                                                        <td><span class="rwd-tables thead"><b></b></span><span class="rwd-tables tbody" data-toggle="tooltip" data-placement="top" title="View Solution"><img class="viewsolution" data-toggle="modal" data-target="#solutionModel" title="View Solution" class="clickable-row" data-ID="<?php echo $viewTotaltests->TM_STU_TT_Id?>" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" class="hvr-pop mCS_img_loaded"></span></td>
                                                        <td class="text-right"><a href="<?php echo Yii::app()->request->baseUrl."/student/TestComplete/".$viewTotaltests->TM_STU_TT_Id; ?>" title="View Test Result" target="_blank">View Test Results</a></td>
                                                        <!-- <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><a href="#"><span class="rwd-tables thead"></span><span class="glyphicon glyphicon-open hvr-pop" style="font-size: 20px;"></span></a></span></td>-->
                                                        <?php if($totalsquestions['total']==$totalsquestions['paper']):?>
                                                            <td></td>
                                                            <td></td>
                                                        <?php else:?>
                                                        <td class="text-right">
														   
														   <span class="rwd-tables tbody">
															<select class="form-control redochange" style="width: 180px;display: -webkit-inline-box !important;" data-item="<?php echo $viewTotaltests->TM_STU_TT_Id?>"  id="selectRedo<?php echo $viewTotaltests->TM_STU_TT_Id?>">
																<?php if($this->GetRedoCount($viewTotaltests->TM_STU_TT_Id)>0 ): echo ' <option value="flagged">Redo Flagged Only</option>'; endif;?>
																<option value="all" selected="selected">Redo All</option>
															</select>
															</span>
														</td>
                                                        <td class="text-right">
														
														<span class="rwd-tables tbody">
														<form action="<?php echo Yii::app()->createUrl('student/RevisionRedo',array('id'=>$viewTotaltests->TM_STU_TT_Id))?>" method="get"><!--<input type="hidden" name="testid" value="<?php /*echo $viewTotaltests->TM_STU_TT_Id*/?>"/>--><input type="hidden" name="redo" id="redohidden<?php echo $viewTotaltests->TM_STU_TT_Id;?>" style="width: 100%;" class="<?php echo $viewTotaltests->TM_STU_TT_Id?>" value="all"> <input type="submit" value="Go" class="redoButton btn btn-warning"  style="width: 50px;"/></form> </span></td>
                                                        <?php endif;?>
                                                    </tr>
                                                <?php
                                                    
                                                    $count++;
                                                endforeach;

                                                else:?>
                                                <tr>
                                                    <td colspan="7" class="font2" style="text-align: center" align="center">No revisions found </td>
                                                </tr>
                                                <?php endif;?>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in <?php echo (!Yii::app()->session['quick'] & !Yii::app()->session['difficulty'] & !Yii::app()->session['challenge'] & Yii::app()->session['mock']?'active':'');?>" id="mocks">
                                <div class="panel panel-default">
                                    <div class="tab-pane fade in" id="mocks">
                                        <table class="rwd-tables">
                                            <tbody style=" border-bottom: 1px solid #E0E0E0;">


                                            </tbody>
                                            <table class="rwd-tables">
                                                <tbody>
                                                <tr>
                                                    <th>Mock</th>
                                                    <th>Date</th>
                                                    <th class="scoreth">Score</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Result</th>
                                                    <!--<th> </th>
                                                    <th> </th>-->
                                                    <!--<th> </th>-->
                                                </tr>
                                                <?php
                                                if(count($totalmocks)>0):
                                                    $count=0;
                                                    $script2='';
                                                    foreach ($totalmocks AS $viewTotaltests):

                                                        ?>

                                                        <tr>
                                                            <td>
                                                                <span class="rwd-tables thead"><b>Topic</b></span><span class="rwd-tables tbody"><?php echo $viewTotaltests->mocks->TM_MK_Name;?></span>
                                                            </td>
                                                            <td ><span class="rwd-tables thead"><b>Date</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($viewTotaltests->TM_STMK_Date)); ?></span></td>
                                                            <td>
															<span class="rwd-tables thead"><b>Score</b></span><span class="rwd-tables tbody">
                                                                <!--<div class="pie_progress2 showprogress<?php echo $count;?>" role="progressbar<?php echo $count;?>" data-goal="<?php echo round($viewTotaltests->TM_STMK_Percentage);?>" aria-valuemin="0" data-step="2" aria-valuemax="100">
                                                                    <span class="pie_progress2__number"><?php echo $viewTotaltests->TM_STMK_Percentage."%"?></span>
                                                                </div>-->
                                                                <div id="<?php echo 'progmock'.$count;?>" class="containerdiv" style="margin: auto;">
                                                                    <div class="jCProgress" style="opacity: 1;">
                                                                        <div class="percent" style="display: none;"><?php echo round($viewTotaltests->TM_STMK_Percentage);?></div>
                                                                        <canvas width="55" height="55"></canvas>
                                                                    </div>
                                                                </div>
                                                                <!--<script>$(function(){<?php /*echo "var mypluginmock".$count.";mypluginmock".$count." = $('#progmock".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STMK_Percentage).",loop : false,showPercent : true});";*/?>});</script>-->
                                                                <?php if(Yii::app()->session['userlevel']==1):?>
                                                                    <script>$(function(){<?php echo "var mypluginmock".$count.";mypluginmock".$count." = $('#progmock".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STMK_Percentage).",loop : false,showPercent : true});";?>});</script>
                                                                <?php else:?>
                                                                    <script>$(function(){<?php echo "var mypluginmock".$count.";mypluginmock".$count." = $('#progmock".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STMK_Percentage).",loop : false,showPercent : true});";?>});</script>
                                                                <?php endif;?>
																</span>
                                                            </td>
                                                            <td class="text-right"><span class="rwd-tables thead"></span><span  class="rwd-tables tbody" data-toggle="tooltip" data-placement="top" title="View Solution"><img class="viewsolutionmock" data-toggle="modal" data-target="#solutionModel"  class="clickable-row" data-id="<?php echo $viewTotaltests->TM_STMK_Id?>" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" class="hvr-pop mCS_img_loaded pull-right"></span></td>
                                                            <td class="text-right">
                                                                <a href="<?php echo Yii::app()->request->baseUrl."/student/mockcomplete/".$viewTotaltests->TM_STMK_Id ?>" title="View Test Result" target="_blank">View Test Results</a>
                                                            </td>
                                                            <!-- <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><a href="#"><span class="rwd-tables thead"></span><span class="glyphicon glyphicon-open hvr-pop" style="font-size: 20px;"></span></a></span></td>-->
                                                            <!--<td><span class="rwd-tables thead"></span><span class="rwd-tables tbody">
												<select class="form-control redochange" style="width:107px" data-item="<?php /*echo $viewTotaltests->TM_STMK_Id*/?>"  id="selectRedo<?php /*echo $viewTotaltests->TM_STMK_Id*/?>">
                                                    <?php /*if($this->GetMockRedoCount($viewTotaltests->TM_STMK_Id)>0 ): echo ' <option value="flagged">Redo Flagged Only</option>'; endif;*/?>
                                                    <option value="all" selected="selected">Redo All</option>


                                                </select>
												</span></td>
                                                            <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><form action="<?php /*echo Yii::app()->createUrl('student/MockRedo',array('id'=>$viewTotaltests->TM_STMK_Id))*/?>" method="get"><input type="hidden" name="redo" id="redohidden<?php /*echo $viewTotaltests->TM_STMK_Id;*/?>" style="width: 100%;" class="<?php /*echo $viewTotaltests->TM_STMK_Id*/?>" value="all"> <input type="submit" value="Go" class="redoButton btn btn-warning"/></form> </span></td>-->
                                                        </tr>
                                                        <?php
                                                        
                                                        $count++;
                                                    endforeach;

                                                else:?>
                                                    <tr>
                                                        <td colspan="6" class="font2" style="text-align: center" align="center">No mocks found </td>
                                                    </tr>
                                                <?php endif;?>
                                                </tbody>
                                            </table>


                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in <?php echo (!Yii::app()->session['quick'] & !Yii::app()->session['difficulty'] & !Yii::app()->session['challenge'] & !Yii::app()->session['mock'] & Yii::app()->session['school']?'active':'');?>" id="homework">
                                <div class="panel panel-default">
                                    <div class="tab-pane fade in" id="homework">
                                        <table class="rwd-tables">
                                            <tbody style=" border-bottom: 1px solid #E0E0E0;">


                                            </tbody>
                                            <table class="rwd-tables">
                                                <tbody>
                                                <tr>
                                                    <th>Class</th>
                                                    <th>Task</th>
                                                    <th>Publish Date</th>
                                                    <th>Completed date</th>
                                                     <th> Assigned By</th>
                                                    <th>Solution</th>
                                                    <th>Result</th>
                                                    <th></th>
                                                    <th class="scoreth">Score</th>
                                                </tr>
                                                <?php
                                                if(count($totalhomeworks)>0):
                                                    $count=0;
                                                    $script='';
                                                    foreach ($totalhomeworks AS $viewTotaltests):

                                                        ?>

                                                        <tr>
                                                            <td>
                                                                <span class="rwd-tables thead"><b>Class</b></span><span class="rwd-tables tbody"><?php echo Standard::model()->findByPk($viewTotaltests->homework->TM_SCH_Master_Stand_Id)->TM_SD_Name;?></span>
                                                            </td>
                                                            <td>
                                                                <span class="rwd-tables thead"><b>Task</b></span><span class="rwd-tables tbody"><?php echo $viewTotaltests->homework->TM_SCH_Name;?></span>
                                                            </td>
                                                              <td ><span class="rwd-tables thead"><b>Date</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($viewTotaltests->TM_STHW_PublishDate)); ?></span></td>
                                                            <td ><span class="rwd-tables thead"><b>Date</b></span><span class="rwd-tables tbody"><?php  echo date("d-m-Y", strtotime($viewTotaltests->TM_STHW_Date)); ?></span></td>
                                                             <td><?php echo User::model()->findByPk($viewTotaltests->TM_STHW_Assigned_By)->profile->firstname; ?></td>

                                                            <td>
                                                                <?php if($viewTotaltests->TM_STHW_Status==5):?>
                                                                    <span class="rwd-tables thead"><b>Solution</b></span><span class="rwd-tables tbody" data-toggle="tooltip" data-placement="top" title="View Solution"><img class="hwviewsolution clickable-row" data-toggle="modal" data-target="#HWsolutionModel" title="View Solution" data-ID="<?php echo $viewTotaltests->TM_STHW_HomeWork_Id?>" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" class="hvr-pop mCS_img_loaded"/></span>
                                                                <?php else:
                                                                    if($viewTotaltests->TM_STHW_ShowSolution):?>
                                                                        <span class="rwd-tables thead"><b>Solution</b></span><span class="rwd-tables tbody" data-toggle="tooltip" data-placement="top" title="View Solution"><img class="hwviewsolution clickable-row" data-toggle="modal" data-target="#HWsolutionModel" title="View Solution" data-ID="<?php echo $viewTotaltests->TM_STHW_HomeWork_Id?>" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" class="hvr-pop mCS_img_loaded"/></span>
                                                                    <?php endif;?>
                                                                <?php endif;?>
                                                            <?php /*if($viewTotaltests->TM_STHW_ShowSolution):*/?><!--
                                                                <span class="rwd-tables thead"><b>Solution</b></span><span class="rwd-tables tbody" data-toggle="tooltip" data-placement="top" title="View Solution"><img class="hwviewsolution clickable-row" data-toggle="modal" data-target="#HWsolutionModel" title="View Solution" data-ID="<?php /*echo $viewTotaltests->TM_STHW_HomeWork_Id*/?>" src="<?php /*echo Yii::app()->request->baseUrl;*/?>/images/solutions.png" class="hvr-pop mCS_img_loaded"/></span>
                                                            --><?php /*endif;*/?>
                                                            </td>
                                                            <td>
                                                                <?php if($viewTotaltests->TM_STHW_Type==2):?>
                                                                    <a href="<?php echo Yii::app()->request->baseUrl."/student/homeworkcomplete/".$viewTotaltests->TM_STHW_HomeWork_Id ?>" title="View Test Result" target="_blank">View Test Results</a>

                                                                <?php endif;?>
                                                            <?php if($this->HasWorksheeturl($viewTotaltests->TM_STHW_HomeWork_Id)=='0'): ?>
                                                                 <a href="javascript:void(0)" class="homeworkurl" title="Insert Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $viewTotaltests->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-link"></span></a>


                                                               

                                                                 <?php else: ?>
                                                                    <a href="javascript:void(0)" class="homeworkurl" title="Open/Edit Link" data-toggle="modal" data-target="#urlmodal" data-value="<?php echo $viewTotaltests->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a> 
                                                                <?php endif;?>
                                                                 <?php if($this->HasAnswersheets($viewTotaltests->TM_STHW_HomeWork_Id)=='0'):?>
                                                                <a href="javascript:void(0)" class="homework" title="Manage Answer Sheets" data-toggle="modal" data-target="#myModal" data-value="<?php echo $viewTotaltests->TM_STHW_HomeWork_Id;?>"><span class="glyphicon glyphicon-cloud-upload"></span></a>
                                                                <?php else: ?>
                                                                  <a href="javascript:void(0)" class="homework" title="Manage Answer Sheets" data-toggle="modal" data-target="#myModal" data-value="<?php echo $viewTotaltests->TM_STHW_HomeWork_Id;?>"><span style=""  class="glyphicon glyphicon-cloud-upload"></span></a>
                                                                  <?php endif; ?>  
                                                            </td>
                                                            <td>
                                                                <?php if($this->CheckAnswersheetCount(Yii::app()->user->id,$viewTotaltests->TM_STHW_HomeWork_Id)):
                                                                    $hwanswers=$this->GetAnswersheetCount(Yii::app()->user->id,$viewTotaltests->TM_STHW_HomeWork_Id);
                                                                    foreach($hwanswers AS $hwanswer):
                                                                        $ext = pathinfo($hwanswer['TM_STHWAR_Filename'], PATHINFO_EXTENSION);
                                                                        ?>
                                                                        <?php if($ext=='one'):?>
                                                                        <a href="<?php echo Yii::app()->request->baseUrl."/student/download?file=".urlencode($hwanswer['TM_STHWAR_Filename']) ?>" title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
                                                                            <!--<span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-download-alt"></span>-->
                                                                            <?php if(Yii::app()->session['userlevel']==1):?>
                                                                                <span style="color:#800001;font-size: 17px" class="glyphicon glyphicon-download-alt"></span>
                                                                            <?php else:?>
                                                                                <span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-download-alt"></span>
        <?php endif;?>
        </a>
        <?php else:
  
        $completed=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id = '".$hwanswer->TM_STHWAR_HW_Id."' AND TM_STHW_Student_Id='".Yii::app()->user->id."'"));
        if($completed->TM_STHW_Status==5){
            $allowed = array('gif', 'png', 'jpg');
            $ext = pathinfo($hwanswer['TM_STHWAR_Filename'], PATHINFO_EXTENSION);
        if (in_array($ext, $allowed)) { ?>
        <a href="<?php echo Yii::app()->request->baseUrl."/student/viewanssheet?name=".$hwanswer['TM_STHWAR_Filename']."&assignment=".$hwanswer->TM_STHWAR_HW_Id ?>" title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
        <?php } else { ?> 
        <a href="<?php echo Yii::app()->request->baseUrl."/homework/".$hwanswer['TM_STHWAR_Filename'] ?>" title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
        <?php } } else { ?>
        <a href="<?php echo Yii::app()->request->baseUrl."/homework/".$hwanswer['TM_STHWAR_Filename'] ?>" title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
        <?php } ?>
        
            <!--<span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-download-alt"></span>-->
         <?php if(Yii::app()->session['userlevel']==1):?>
        <span style="color:#800001;font-size: 17px" class="glyphicon glyphicon-download-alt"></span>
        <?php else:?>
            <?php 
            
            $commentexist=Imagetag::model()->findAll(array('condition' => "pic_id LIKE '".$hwanswer['TM_STHWAR_Filename']."' AND assignment_id='".$hwanswer->TM_STHWAR_HW_Id."'"));
        
        $completed=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id = '".$hwanswer->TM_STHWAR_HW_Id."' AND TM_STHW_Student_Id='".Yii::app()->user->id."'"));
            $allowed = array('gif', 'png', 'jpg');
            $ext = pathinfo($hwanswer['TM_STHWAR_Filename'], PATHINFO_EXTENSION);
        if (in_array($ext, $allowed)) { 
             if($commentexist && $completed->TM_STHW_Status==5)
             { ?>
            <span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-download-alt"></span>
            <?php } else { ?>
            <span style="font-size: 17px" class="glyphicon glyphicon-download-alt"></span>
            <?php } }
             else { ?>
            <span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-download-alt"></span>
            <?php } ?>

        <?php endif;?>
        </a>
        <?php endif;?>
        <?php endforeach;endif;?>
        <?php if($this->CheckCommentCount(Yii::app()->user->id,$viewTotaltests->TM_STHW_HomeWork_Id)):
                                                                    $hwcomments=$this->GetCommentCount(Yii::app()->user->id,$viewTotaltests->TM_STHW_HomeWork_Id);
                                                                    foreach($hwcomments AS $hwcomment):
                                                                        $ext = pathinfo($hwcomment['TM_STHW_Comments_File'], PATHINFO_EXTENSION);
                                                                        ?>
                                                                        <?php if($ext=='one'):?>
                                                                        <a href="<?php echo Yii::app()->request->baseUrl."/student/download?file=".urlencode($hwcomment['TM_STHW_Comments_File']) ?>" title="" target="_blank" class="viewcomment" data-item="<?php echo $hwcomment->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $hwcomment->TM_STHW_Student_Id;?>">
                                                                            <!--<span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-cloud-download"></span>-->
                                                                            <?php if(Yii::app()->session['userlevel']==1):?>
                                                                                <span style="color:#800001;font-size: 17px" class="glyphicon glyphicon-cloud-download"></span>
                                                                            <?php else:?>
                                                                                <span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-cloud-download"></span>
                                                                            <?php endif;?>
                                                                        </a>
                                                                    <?php else:?>
                                                                        <a href="<?php echo Yii::app()->request->baseUrl."/homework/".$hwcomment['TM_STHW_Comments_File'] ?>" title="" target="_blank" class="viewcomment" data-item="<?php echo $hwcomment->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $hwcomment->TM_STHW_Student_Id;?>">
                                                                            <span style="color:#ffa900;font-size: 17px" class="glyphicon glyphicon-cloud-download"></span>
                                                                        </a>
                                                                    <?php endif;?>
                                                                    <?php endforeach;endif;?>
                                                            </td>
                                                            <?php if($viewTotaltests->TM_STHW_Percentage=='0' || $viewTotaltests->TM_STHW_Percentage=='' ):?>
                                                                <td style="text-align: center;">
                                                                    <?php echo "NA";?>
                                                                </td>                                                            
                                                            <?php
                                                            else:?>

                                                                <?php if($viewTotaltests->TM_STHW_Status==4):?>
                                                                    <td style="text-align: center;">
                                                                        <?php echo "NA";?>
                                                                    </td>
                                                                    <?php else:?>
                                                                    <td>
                                                                        <span class="rwd-tables thead"><b>Score</b></span>
                                                                        <span class="rwd-tables tbody">
                                                                        <!--<div class="pie_progress2 showprogress<?php echo $count;?>" role="progressbar<?php echo $count;?>" data-goal="<?php echo round($viewTotaltests->TM_STHW_Percentage);?>" aria-valuemin="0" data-step="2" aria-valuemax="100">
                                                                            <span class="pie_progress2__number"><?php echo $viewTotaltests->TM_STHW_Percentage."%"?></span>
                                                                        </div>-->
                                                                        <div id="<?php echo 'prog'.$count;?>" class="containerdiv" style="margin: auto;">
                                                                            <div class="jCProgress" style="opacity: 1;">
                                                                                <div class="percent" style="display: none;"><?php echo round($viewTotaltests->TM_STHW_Percentage);?></div>
                                                                                <canvas width="55" height="55"></canvas>
                                                                            </div>
                                                                        </div>
                                                                        <!--<script>$(function(){<?php /*echo "var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STHW_Percentage).",loop : false,showPercent : true});";*/?>});</script>-->
                                                                        <?php if(Yii::app()->session['userlevel']==1):?>
                                                                            <script>$(function(){<?php echo "var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STHW_Percentage).",loop : false,showPercent : true});";?>});</script>
                                                                        <?php else:?>
                                                                            <script>$(function(){<?php echo "var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".round($viewTotaltests->TM_STHW_Percentage).",loop : false,showPercent : true});";?>});</script>
                                                                        <?php endif;?>
    															        </span>
                                                                    </td>
                                                                    <?php endif;?>
                                                            <?php endif;?>
                                                            
                                                            <!--<td class="text-right"><span class="rwd-tables thead"></span><span  class="rwd-tables tbody" data-toggle="tooltip" data-placement="top" title="View Solution"><img class="viewsolutionmock" data-toggle="modal" data-target="#solutionModel"  class="clickable-row" data-id="<?php echo $viewTotaltests->TM_STHW_Id?>" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" class="hvr-pop mCS_img_loaded pull-right"></span></td>-->
                                                            <!-- <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><a href="#"><span class="rwd-tables thead"></span><span class="glyphicon glyphicon-open hvr-pop" style="font-size: 20px;"></span></a></span></td>-->
                                                            <!--<td><span class="rwd-tables thead"></span><span class="rwd-tables tbody">
												    <select class="form-control redochange" style="width:107px" data-item="<?php /*echo $viewTotaltests->TM_STMK_Id*/?>"  id="selectRedo<?php /*echo $viewTotaltests->TM_STMK_Id*/?>">
                                                    <?php /*if($this->GetMockRedoCount($viewTotaltests->TM_STMK_Id)>0 ): echo ' <option value="flagged">Redo Flagged Only</option>'; endif;*/?>
                                                    <option value="all" selected="selected">Redo All</option>


                                                </select>
												</span></td>
                                                            <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><form action="<?php /*echo Yii::app()->createUrl('student/MockRedo',array('id'=>$viewTotaltests->TM_STMK_Id))*/?>" method="get"><input type="hidden" name="redo" id="redohidden<?php /*echo $viewTotaltests->TM_STMK_Id;*/?>" style="width: 100%;" class="<?php /*echo $viewTotaltests->TM_STMK_Id*/?>" value="all"> <input type="submit" value="Go" class="redoButton btn btn-warning"/></form> </span></td>-->
                                                        </tr>
                                                        <?php
                                                        
                                                        $count++;
                                                    endforeach;

                                                else:?>
                                                    <tr>
                                                        <td colspan="6" class="font2" style="text-align: center" align="center">No tasks found </td>
                                                    </tr>
                                                <?php endif;?>
                                                </tbody>
                                            </table>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>


        </div>


    </div>
	
<div id="myinvite" class="modal fade" role="dialog" style="padding-top:20%;">
  <div class="modal-dialog modal-sm modal-sm_plus">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Challenge position</h4>
      </div>
      <div class="modal-body modal-body_padding">
      <div class="row">
        <ul class='popu-cart' role='menu' style='display: block !important;'>                  
            </ul>
            </div>
      </div>
    </div>

  </div>
</div>
<div class="modal fade " id="solutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>
            </div>
            <div class="panel-heading">
                <div style="text-align: right;">
<!--                    <span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" id="printanchorspan" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" id="printanchor" target="_blank"><i class="fa fa-print fa-lg"></i></a>-->

                </div>
            </div>
            <div class="modal-body" >
                <table class="table ">
                    <thead>

                    </thead>
                    <tbody class="font3 wirisfont" id="showsolution">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->


<div class="modal fade " id="HWsolutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>
            </div>
            <div class="panel-heading">
                <div style="text-align: right;">
<!--                    <span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" id="printanchorspan" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" id="printanchor" target="_blank"><i class="fa fa-print fa-lg"></i></a>-->

                </div>
            </div>
            <div class="modal-body" >
                <table class="table ">
                    <thead>

                    </thead>
                    <tbody class="font3 wirisfont" id="showsolutionhw">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

                 <div id="urlmodal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content"> 
                                <form action="<?php echo Yii::app()->request->baseUrl."/student/Uploadhistoryurl";?>" onsubmit="return Checkfiles();" id="answersheetform" enctype="multipart/form-data" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Enter Url</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'homework-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                                        <label class="control-label">Enter File</label>
                <?php 
                
                     echo $form->textField($model1, 'TM_STHWARU_Url',array('class'=>'form-control','id'=>'url'));
               
                 ?>
                                     <!--    <input id="input-1" type="file" class="file" name="Answersheet"> -->

                                        <input type="hidden" name="homeworkid" id="homeworkid">
                                        <div id="answersheetlist"></div>
                                      
                                    </div>
                                    <div class="modal-footer">
                                        <span class="alert alert-success pull-left uploadinfo" style="display: none;"> </span>
                                         <button type="button" class="btn btn-default" id="openurl">Open Url</button>
                                        <button type="submit" class="btn btn-default" id="">Ok</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <?php $this->endWidget(); ?>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>


                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <form action="<?php echo Yii::app()->request->baseUrl."/student/UploadAnswersheet";?>" onsubmit="return Checkfiles();" id="answersheetform" enctype="multipart/form-data" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Upload Answer sheet</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'homework-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
                                       
               <!--  <?php 
                echo $form->fileField($answers, 'TM_STHWAR_Filename[]',array('class'=>'file','id'=>'input-1', 'accept'=>'image/*', 'multiple'=>true));
                 ?> -->
                  <label class="control-label">Select Files</label>
                 <div class="col-md-12" style="display: flex;">
                    
                    <input style="width: 100%;" id="file" type="file" accept="image/x-png,image/gif,image/jpeg" class="file" name="file">

                    <img id="loader_process" src="<?php echo Yii::app()->request->baseUrl."/images/preloader.gif" ?>" style="width: 23px;margin-left: 0px; display: none;" >
                    <button type="button" class="btn btn-warning" id="uploadimg">Upload</button>
                    </div>
                                        <input type="hidden" name="homework_id" id="homework_id">
                                        <div style="margin-top: 33px;" id="answersheetlisting"></div>
                                      
                                        </div>
                                    <div class="modal-footer">
                                        <span class="alert alert-success pull-left uploadinfo" style="display: none;"> </span>
                                        <p class="imgnote" style="float: left;">*You can select multiple images and upload</p><br><br>
                                        <p  style="float: left; color: red; display: none;" id="imgonlymsg">Only image files are allowed.!</p>
                                       
                                        <button type="button" class="btn btn-warning refreshpage" data-dismiss="modal">Ok</button>
                                          <?php $this->endWidget(); ?>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
<script type="text/javascript">
    $(document).ready(function () {
        <?php 
        echo $script;
        echo $script1;
        echo $script2;
        
        ?>
        $(".rwd-tables table, .table.rwd-tables").rwdTables();
    });
</script>
<script type="text/javascript">

    $(document).on('click','.hover',function(){
        $('.popu-cart').html(''); 
        var id=$(this).attr('data-id');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetChallengeBuddies";?>',
                data: {challenge:id},
                success: function(data){
                    if(data!='no')
                    {
                        $('.popu-cart').html(data);                        
                    }
                }
            });
    });
    $(document).on('click','#close',function(){
        $('.tooltiptable').hide();

    });
</script>

<?php 

Yii::app()->clientScript->registerScript('viewhistoryredo', "

$('.hwviewsolution').click(function(){
    $('#showsolutionhw').html('');
	var testid=$(this).attr('data-ID');	
	$.ajax({
		type: 'POST',
		url: '".Yii::app()->createUrl('student/GethomeworkSolution')."',
		data: { testid: testid}
	})
	.done(function( html ) {
		$('#showsolutionhw').html( html );
        MathJax.Hub.Queue(['Typeset',MathJax.Hub, 'HWsolutionModel']);
	});
});

$('.redochange').change(function(){

    var item=$(this).attr('data-item');

    $('#redohidden'+item).val($(this).val());
});
$('.viewsolution').click(function(){
    $('#showsolution').html('');
var testid=$(this).attr('data-ID');
var urlpdf='".Yii::app()->request->baseUrl."/student/getpdfanswer/'+testid+'.html';
$('#printanchorspan').attr('href',urlpdf);
$('#printanchor').attr('href',urlpdf);
$.ajax({
type: 'POST',
url: '".Yii::app()->createUrl('student/GetSolution')."',
data: { testid: testid}
})
.done(function( html ) {
$('#showsolution').html( html );
MathJax.Hub.Queue(['Typeset',MathJax.Hub, 'solutionModel']);
});
});
$('.viewsolutionmock').click(function(){
    $('#showsolution').html('');
var testid=$(this).attr('data-id');
var urlpdf='".Yii::app()->request->baseUrl."/student/GetpdfMockAnswer/'+testid+'.html';
$('#printanchorspan').attr('href',urlpdf);
$('#printanchor').attr('href',urlpdf);
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/GetmockSolution')."',
          data: { testid: testid}
        })
          .done(function( html ) {
             $('#showsolution').html( html );
             MathJax.Hub.Queue(['Typeset',MathJax.Hub, 'solutionModel']);
          });
    });

");?>
<?php Yii::app()->clientScript->registerScript('sendmailreminder', "
 $(document).on('click','.reminder',function(){
    var recepientid= $(this).attr('data-id');
    var challengeid= $(this).attr('data-challenge');
  $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('student/SendMailReminders')."',
                  data: { recepientid: recepientid,challengeid:challengeid}

                })
                .done  (function(data, textStatus, jqXHR){
                $('.rem'+recepientid).remove();
                $('.remsend'+recepientid).slideDown();
                setTimeout(function(){
                        $('.remsend'+recepientid).remove();
                    },6000)

                });

 });"); ?>

<?php Yii::app()->clientScript->registerScript('sendmailtest', "
    $(document).on('click','.showSolutionItem',function(){
            var item= $(this).attr('data-reff');
            $('.sendmail').slideUp('fast');
            $('.suggestiontest'+item).slideDown('slow')
    });

    $(document).on('click','#mailSend',function(){
        var item= $(this).attr('data-id');
      
        var comments= $('#comment'+item).val();   
        comments=comments.trim();   
        if(comments!='')
        {
            var questionId=$(this).data('id');
            var questionReff=$(this).data('reff');
             $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('student/SendMail')."',
                  data: { questionId: questionId,questionReff: questionReff,comments: comments}

                })
                .done  (function(data, textStatus, jqXHR){
                    $('.mailSendComplete'+questionId).slideDown();
                    setTimeout(function(){
                        $('.mailSendComplete'+questionId).hide();
                        $('.sendmail').slideUp();
                        $('#comment'+questionId).val('')
                    },6000)
                });
        }
        else
        {   $('#comment'+item).val('');
            $('#comment'+item).css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
            $('#comment'+item).attr('placeholder', 'Please enter your comments before clicking send');
            setTimeout(function() {
                $('#comment'+item).css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
               $('#comment'+item).attr('placeholder', 'Enter your comments here');
            },5000)
        }
    });

");
?>
<script type="text/javascript">

               $(document).on("click", '.homeworkurl', function() { 
                var hwid=$(this).data('value');
                $('#homeworkid').val(hwid);
                $('#homework_id').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Getworksheeturl";?>',
                data: {homework:hwid},
                success: function(data){
                  $('#url').val(data);
                        //$('#url').html(data);                        
                    
                }
                });                 
            });

 $(document).on("click", '.homeworkurl', function() { 
                var hwid=$(this).data('value');
                $('#homeworkid').val(hwid);
                $('#homework_id').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Getworksheetcomplete";?>',
                data: {homework:hwid},
                success: function(data){
                if(data==2)
                {
                      $(":text").prop("disabled", true);
                        //$('#url').html(data);                        
                }
                else if(data==0)
                {
                    $(":text").prop("disabled", false);
                }
                    
                }
                });                 
                
            });

$(document).on('click','#openurl', function(){
    var url = $('#url').val();
    if( url.indexOf('https://') >= 0){
       window.open(url);
    }
    else
    {
        window.open("https://"+url);
    }
})
    $(document).ready(function(){
            $('.homework').click(function(){
                var hwid=$(this).data('value');
                $('#homeworkid').val(hwid);
                $('#homework_id').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Checkcomment";?>',
                data: {homework:hwid},
                success: function(data){
                    if(data!='no')
                    {
                      if(data==2)
                      {
                        $('.file').hide();

                      }

                                               
                    }
                }
                });                 
            });
});

  $(document).ready(function(){
            $('.homework').click(function(){
                var hwid=$(this).data('value');
                $('#homeworkid').val(hwid);
                $('#homework_id').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetWorksheetshistory";?>',
                data: {homework:hwid},
                success: function(data){
                    if(data!='no')
                    {
                        console.log(data);
                        $('#answersheetlisting').html(data);                        
                    }
                }
                });                 
            });
});
</script>

    <script type="text/javascript"> 
        $(document).ready(function() { 
        $("#file").change(function() { 
        var fileName = document.getElementById("file").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $("#imgonlymsg").hide();
            //TO DO
        }else{
           
            $("#imgonlymsg").show();
            return false;
        }
        });     
        $("#uploadimg").click(function() { 
        var fileName = document.getElementById("file").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
            //TO DO
        $("#imgonlymsg").hide();
        }else{
           
            $("#imgonlymsg").show();
            return false;
        }   
                $("#imgonlymsg").hide();
                $('#loader_process').show();
                var fd = new FormData(); 
                var files = $('#file')[0].files[0]; 
                var homework_id = $('#homework_id').val();
                fd.append('file', files); 
                fd.append('homework_id', homework_id); 
                fd.append('history', 1);

                console.log(fd);
                $.ajax({ 
                    url: '<?php echo Yii::app()->request->baseUrl."/student/uploadanswersheet";?>', 
                    type: 'post', 
                    data: fd, 
                    contentType: false, 
                    processData: false, 
                    success: function(response){ 
                        if(response != 0){ 

                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetWorksheetshistory";?>',
                data: {homework:homework_id},
                success: function(data){
                    if(data!='no')
                    {
                        $('#answersheetlisting').html(data);
                        $('#file').val(''); 
                        $('#loader_process').hide();                       
                    }
                }
                }); 
                        } 
                        else{ 
                            console.log('file not uploaded'); 
                        } 
                    }, 
                }); 
            }); 
      
        }); 


            $(document).on("click", '.deleteworksheet', function(event) { 
            var worksheet=$(this).data('value');
            
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/deleteWorksheet";?>',
                data: {worksheet:worksheet},
                success: function(data){
                    if(data!='no')
                    {
                        $('#file'+worksheet).remove();                        
                    }
                }
                })
            
        });

$(document).ready(function(){
            $('.homework').click(function(){
                var hwid=$(this).data('value');
                $('#homeworkid').val(hwid);
                $('#homework_id').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Completecheck";?>',
                data: {homework:hwid},
                success: function(data){
                    if(data==0)
                    {
                        $('#file').hide();
                        $('#uploadimg').hide();
                        $(".deleteworksheet").hide();
                        $('.imgnote').hide();
                    }
                    else
                    {
                         $('#file').show();
                         $('#uploadimg').show();
                         $(".deleteworksheet").show();
                          $('.imgnote').show();
                    }
                }
                });                 
            });
});

$(document).on('click','.refreshpage', function(){
    location.reload();
});
    </script> 