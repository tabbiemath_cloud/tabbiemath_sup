<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sweetalert.css">
    <!-- responsive-full-background-image.css stylesheet contains the code you want -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/responsive-full-background-image.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/frontendbootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/hover.css" rel="stylesheet" media="all">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/dashboard.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/slider.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/progress.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/prologin.css">
        <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script>
    
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php
    $cs=Yii::app()->clientScript;

    $cs->scriptMap=array(
        'jquery.js'=>false,
    );?>
    <style>

        .img_center{height: 400px;
            display: block;
            margin: auto;}

        .img_center_sm{height: 200px;
            display: block;
            margin: auto;}

        body {
            padding-top: 0px !important;
            background-image: none !important;
            background-color: #FFFFFF !important;   
                        
        }
        .panel
        {
            border-top:0px !important;
            border-left:0px !important;
            border-right:0px !important;            
        }

        blockquote {
            padding: 0px 10px;
            margin: 0 0 10px;
            font-size: 16px;
            border-left: 5px solid #FFA900;
        }
        .ptp25 p{
            margin-left:25px;
        }
        .ptp25 input{margin-right:10px}
        .media { margin-top: 4px;}
        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 0px 0;
            border-radius: 4px;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #23527c;
            background-color: #eee;
            border-color: #ddd;
        }
        .pagination>li:first-child>a, .pagination>li:first-child>span {
            margin-left: 0;
            border-top-left-radius: 30px;
            border-bottom-left-radius: 30px;
        }
        .pagination>li:last-child>a, .pagination>li:last-child>span {
            border-top-right-radius: 30px;
            border-bottom-right-radius: 30px;
        }

        .pagination>li>a, .pagination>li>span {
            position: relative;
            float: left;
            padding:4px;
            margin-left: -1px;
            margin: 3px;
            line-height: 1.42857143;
            color: #383838;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 30px;
            height: 30px;
            width: 30px;
            text-align: center;
            font-size: 14px;
        }

        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            z-index: 2;
            color: #fff;
            cursor: default;
            background-color: #FFA900;
            border-color: #FFA900;
        }
        .sanfont{  font-family: sans-serif;
            font-size: 16px;}

        .pager {
            padding-left: 0;
            margin: 0px 0;
            text-align: center;
            list-style: none;
        }

        .label, .glyphicon {
            margin-right: 0px;
        }
        .pagination>.skip>a{  background-color: rgb(181, 181, 181);  cursor: not-allowed;
            color: rgb(202, 202, 202);
            border-color: rgb(173, 173, 173);}
    </style>
</head>
<?php if(Yii::app()->session['userlevel']==1): ?>
<body class="professional hidebg">
<?php else: ?>
<body>
<?php endif; ?>
<?php
$correctoption=$this->OptionHint($question->TM_QN_Type_Id,$question->TM_QN_Id);
$count=$this->GetCountChallengeQuestion($testid);
$questioncount=($count<$challengecount?$count:$challengecount);
?>
<form id="submitanswer" method="post">
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12 main">

            <div class="contentarea">

                <div class="panel panel-default padng">
                    <!-- Default panel contents -->
                    <div class="row">
                        <?php if($question->TM_QN_Type_Id!='5'):?>
                        <div class="col-md-12 col-lg-12" style="margin-bottom:10px ">
                            <!-- artigo em destaque -->
                            <div class="featured-article">
                                <div class="col-md-8">
                                    <div class="block-title">
                                        <blockquote>
                                            <?php echo $question->TM_QN_Question;?>
                                        </blockquote>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <?php if($question->TM_QN_Image!=''):?>
                                        <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=large&type=question&width=400&height=400';?>" alt="">
                                    <?php endif;?>
                                </div></div>
                            <!-- /.featured-article -->
                        </div>

                        <br>
                        <div class="col-md-12 col-lg-12">

                            <?php
                            if($question->TM_QN_Type_Id=='1' || $question->TM_QN_Type_Id=='2' || $question->TM_QN_Type_Id=='3'):

                            ?>
                            <ul class="media-list main-list">
                            <?php
                            foreach($question->answers AS $answer):?>
                                <li class="media">

                                    <?php if($answer->TM_AR_Answer!='' & $answer->TM_AR_Image!=''):?>
                                        <span class="<?php echo ($answer->TM_AR_Answer!=''?'pull-right':'');?>" style="margin-right: 15px;">
                                            <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=200&height=200';?>" alt="">

                                    </span>
                                    <?php endif;?>
                                    <div class="media-body ptp25">
                                        <?php if($question->TM_QN_Type_Id=='1'):?>

                                            <input style="float:left" type="checkbox" class="answerrcheck" name="answer[]" value="<?php echo $answer->TM_AR_Id;?>"><?php echo $answer->TM_AR_Answer;?>

                                        <?php elseif($question->TM_QN_Type_Id=='2' || $question->TM_QN_Type_Id=='3'):?>
                                            <input style="float:left" type="radio" name="answer" class="answerradio"  value="<?php echo $answer->TM_AR_Id;?>"><?php echo $answer->TM_AR_Answer;?>
                                        <?php endif;?>
                                        <?php if($answer->TM_AR_Answer=='' & $answer->TM_AR_Image!=''):?>
                                            <span class="<?php echo ($answer->TM_AR_Answer!=''?'pull-right':'');?>">
                                                <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=200&height=200';?>" alt="">
                                            </span>
                                        <?php endif;?>
                                    </div>
                                </li>
                            <?php endforeach;?>
                                <ul>
                                <?php elseif($question->TM_QN_Type_Id=='4'):?>
                                <div class="row" >
                                    <div class="col-md-12 col-lg-3">
                                        <input type="text" placeholder="Enter your answer" value="" autocomplete="off" name="answer" class="form-control answer" style="height: 50px;margin: 10px 0px;">
                                    </div>
                                </div>
                                <?php endif;?>
                        </div>
                        <?php

                        elseif($question->TM_QN_Type_Id=='5'):
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            ?>
                            <div class="col-md-12 col-lg-12" style="margin-bottom:10px ">
                                <!-- artigo em destaque -->
                                <div class="featured-article">
                                    <div class="col-md-8">
                                        <div class="block-title">
                                            <blockquote>
                                                <?php echo $question->TM_QN_Question;?>
                                            </blockquote>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?php if($question->TM_QN_Image!=''):?>
                                            <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=large&type=question&width=400&height=400';?>" alt="">
                                        <?php endif;?>
                                    </div></div>
                                <!-- /.featured-articles -->
                            </div>
                            <?php
                            $countchild=1;
                            foreach ($questions AS $childquestion):?>
                                <div class="col-md-12 col-lg-12" style="margin-bottom:10px ">
                                    <!-- artigo em destaque -->
                                    <div class="featured-article">
                                        <!--<h5 style="text-decoration: underline;"> <?php /*echo "Part-".$countchild;*/?></h5>-->
                                        <div class="col-md-8">
                                            <div class="block-title">

                                                <blockquote>
                                                    <?php echo $childquestion->TM_QN_Question;?>
                                                </blockquote>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <?php if($childquestion->TM_QN_Image!=''):?>
                                                <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childquestion->TM_QN_Image.'?size=large&type=question&width=400&height=400';?>" alt="">
                                            <?php endif;
                                            $countchild++;
                                            ?>
                                        </div></div>
                                    <!-- /.featured-article -->
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <?php
                                    if($childquestion->TM_QN_Type_Id=='1' || $childquestion->TM_QN_Type_Id=='2' || $childquestion->TM_QN_Type_Id=='3'):
                                        ?>
                                        <ul class="media-list main-list parttest<?php echo $childquestion->TM_QN_Id;?>">
                                            <?php
                                            foreach($childquestion->answers AS $childanswer):
                                                ?>
                                                <li class="media">

                                                    <?php if($childanswer->TM_AR_Answer!='' & $childanswer->TM_AR_Image!=''):?>
                                                        <span class="<?php echo ($childanswer->TM_AR_Answer!=''?'pull-right':'');?>" style="margin-right: 15px;">
                                                    <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childanswer->TM_AR_Image.'?size=thumbs&type=answer&width=200&height=200';?>" alt="">
                                                </span>
                                                    <?php endif;?>
                                                    <div class="media-body ptp25">
                                                        <?php if($childquestion->TM_QN_Type_Id=='1'):?>
                                                            <input style="float:left" type="checkbox" class="answerrcheck<?php echo $childquestion->TM_QN_Id;?>"  name="answer<?php echo $childquestion->TM_QN_Id;?>[]" value="<?php echo $childanswer->TM_AR_Id;?>"><?php echo $childanswer->TM_AR_Answer;?>
                                                        <?php elseif($childquestion->TM_QN_Type_Id=='2' || $childquestion->TM_QN_Type_Id=='3'):?>
                                                            <input style="float:left" type="radio" name="answer<?php echo $childquestion->TM_QN_Id;?>" class="answerradio<?php echo $childquestion->TM_QN_Id;?>"   value="<?php echo $childanswer->TM_AR_Id;?>"><?php echo $childanswer->TM_AR_Answer;?>
                                                        <?php endif;?>
                                                        <?php if($childanswer->TM_AR_Answer=='' & $childanswer->TM_AR_Image!=''):?>
                                                            <span class="<?php echo ($childanswer->TM_AR_Answer!=''?'pull-right':'');?>">
                                                                <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childanswer->TM_AR_Image.'?size=thumbs&type=answer&width=200&height=200';?>" alt="">
                                                            </span>
                                                        <?php endif;?>
                                                    </div>
                                                </li>
                                            <?php
                                            endforeach;?>
                                        </ul>
                                    <?php
                                    elseif($childquestion->TM_QN_Type_Id=='4'):
                                        ?>
                                        <div class="row" >
                                            <div class="col-md-3 col-lg-3">
                                                <input type="text" placeholder="Enter your answer" name="answer<?php echo $childquestion->TM_QN_Id;?>" autocomplete="off"  class="form-control answer<?php echo $childquestion->TM_QN_Id;?>" style="height: 50px;margin: 10px 0px;" value="" >
                                            </div>
                                        </div>
                                    <?php endif;?>
                                </div>
                            <?php
                            endforeach;
                            ?>

                        <?php endif;?>
                    </div>


                    <ul class="pager">
                        <div class="row">
                            <input type="hidden" name="QuestionId" value="<?php echo $question->TM_QN_Id;?>">
                            <input type="hidden" name="QuestionType" value="<?php echo $question->TM_QN_Type_Id;?>">
                            <input type="hidden" name="QuestionNumber" value="<?php echo $challengequestion->TM_CEUQ_Iteration;?>">
                            <input type="hidden" name="ChallengeQuestionId" value="<?php echo $challengequestion->TM_CEUQ_Id;?>">
                            <input type="hidden" name="ChallengeId" value="<?php echo $testid;?>">
                            <!--                    <button type="button" class="pull-right btn btn-default btn-round btn-lg" style="margin-right:20px;">Next</button>-->
                            <?php if($question->TM_QN_Type_Id=='1'):?>
                            <div style="display: none;" class="pull-left col-md-12"><p class="pull-left font2">Please Select Any <?php echo $correctoption['correctoptions'];?> Options
                            </p></div>
                            <?php  endif;?>
                            <div class="col-md-6 col-xs-12 visible-sm visible-xs hidden-md hidden-lg">
                                <div class="rd22">
                                    <span>Question  <span class="label label-default badge"> <?php echo $challengequestion->TM_CEUQ_Iteration;?> </span> of <?php echo $questioncount;?> </span></div>
                                <div class="rd22">
                                    <span>Marks  <span class="label label-default badge">
                                                    <?php
                                                    if ($challengequestion->TM_CEUQ_Difficulty== '1'):
                                                        echo '3';
                                                    elseif ($challengequestion->TM_CEUQ_Difficulty == '2'):
                                                        echo  '5';
                                                    elseif ($challengequestion->TM_CEUQ_Difficulty == '3'):
                                                        echo '8';
                                                    endif;
                                                    ?>
                                                    </span>
                                    </span>
                                </div>
                            </div>                            
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-6 col-xs-12 col-md-push-1 hidden-sm hidden-xs visible-md visible-lg">
                                <div class="rd22">
                                    <span>Question  <span class="label label-default badge"> <?php echo $challengequestion->TM_CEUQ_Iteration;?> </span> of <?php echo $questioncount;?> </span></div>
                                <div class="rd22">
                                    <span>Marks  <span class="label label-default badge">
                                                    <?php
                                                    if ($challengequestion->TM_CEUQ_Difficulty== '1'):
                                                        echo '3';
                                                    elseif ($challengequestion->TM_CEUQ_Difficulty == '2'):
                                                        echo  '5';
                                                    elseif ($challengequestion->TM_CEUQ_Difficulty == '3'):
                                                        echo '8';
                                                    endif;
                                                    ?>
                                                    </span>
                                    </span>
                                </div>
                            </div>
                                                        
                    		  <div class="col-md-2 col-xs-12 col-sm-3 pull-right" style="margin-bottom: 9px;">                                    
                                    <?php if($challengequestion->TM_CEUQ_Iteration!=$questioncount):?>
                                        <button name="action[GoNext]" id="GoNext" type="submit" class="pull-left btn btn-warning sanfont btn-block" >Next</button>
                                    <?php else:?>
                                        <button name="action[GoNext]" id="GoNext" type="submit" class="pull-left btn btn-warning sanfont btn-block complete">Complete</button>
                                    <?php endif;?>                                                                        
                    		  </div>
                        </div>
                    </ul>

                </div>


                <div class="row padng">
                    <div class="col-md-2 pull-right"><a href="<?php echo Yii::app()->createUrl('home');?>" id="Quittest" ><button type="button" class="pull-left btn btn-default sanfont btn-block" >Quit Test</button></a></div>                    

                </div>




            </div>





        </div>


    </div>

</div>
</form>

<style>
    .btn-default {
        color: #FFA900;
        background-color: #fff;
        border-color: #ccc;
    }
    .btn-primary.active, .btn-primary.focus, .btn-primary:active, .btn-primary:focus, .btn-primary:hover, .open>.dropdown-toggle.btn-primary {
        color: #fff;
        background-color: #FFA900;
        border-color: #C7993F;
    }

    .padng{   padding: 20px;
        font-family: Lucida Sans !important;
        font-size: 16px !important;
        margin-bottom: 0;
        /*color: black;*/
        color: #333;
    }

    .label-default {
        background-color: #FFA900;
        padding-top: 5px;
        color: white;
        border: 1px solid #FFFFFF;
    }

    .rd22 {
        margin-top: 8px;
        margin-bottom: 10px;
        border: 1px solid #F5F5F5;
        /* padding: 8px; */
        background-color: white;
        /* margin-right: 15px; */
        margin-left: 15px;
        border-radius: 20px;
        padding-left: 13px;float: left;
    }

    .witbg{ background-color: white;}
    .table3 td{padding-left:5px !important}
    .table-hover>tbody>tr:hover {
        background-color: #F7F7F7;
    }
    .ptp25{  padding-top: 0; padding-left: 15px;}
</style>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery1_11.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie10-viewport-bug-workaround.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-slider.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-asPieProgress.js"></script>
<script>
    // With JQuery
    $('#ex1').slider({
        formatter: function(value) {
            return 'Current value: ' + value;
        }
    }).on('slideStop', function(ev){
        var newVal = $('#ex1').data('slider').getValue();
        $('#adv').val(newVal);
    });
    /*
     // Without JQuery
     var slider = new Slider('#ex1', {
     formatter: function(value) {
     return 'Current value: ' + value;
     }
     });*/

</script>
<script>
    $(document).ready(function () {
        $('#Quittest').on('click',function(e, data){

            if(!data){
                handleDelete(e, 1);
            }else{
                window.location = $(this).attr('href');
            }
        });
    });
    function handleDelete(e, stop){
        if(stop){
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You want to quit the test",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#FDBF43",
                    confirmButtonText: "Yes, Quit!",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#Quittest').trigger('click', {});
                    }
                });
        }
    };
</script>

<script type="text/javascript">
    <?php /*$dolatercount=$this->doLatercount($testid);
        if($dolatercount>0):
    ?>
        $('.complete').click(function(){
            if(confirm('You have marked some questions to do later. Are you sure you want to continue without attempting those?'))
            {
                return true;
            }
            else
            {
                return false;
            }
        })
    <?php endif;*/?>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $(document).ready(function(){

        /*$('#Quittest').click(function(){
            if(confirm('Are you sure you want to quit this test?'))
            {
                return true;
            }
            else
            {
                return false;
            }
        });*/

        /*        $('.showslide').click(function(){
         var identi=$(this).attr('data-uniq');
         $('#slide'+identi+'Slider').toggle()
         });*/
        $(".tip-top").tooltip({
            placement : 'top'
        });
        $(".tip-right").tooltip({
            placement : 'right'
        });
        $(".tip-bottom").tooltip({
            placement : 'bottom'
        });
        $(".tip-left").tooltip({
            placement : 'left'
        });
    });
</script>
<script type="text/javascript">
    $('#GoNext').click(function(){
        $('.alert').remove();
        var type='<?php echo $question->TM_QN_Type_Id;?>';
        if(type=='1')
        {
            if($('.answerrcheck:checked').length=='0')
            {
                $('.pager').before('<div class=\'alert alert-danger\' role=\'alert\'>Select an answer before proceeding.</div>');
                setTimeout(function() {
                    $('.alert').fadeOut(function(){
                        $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
/*            else if($('.answerrcheck:checked').length!='<?php echo $correctoption['correctoptions'];?>')
            {
                $('.pager').before('<div class=\'alert alert-danger\' role=\'alert\'>Select any <?php echo $correctoption['correctoptions'];?> options before proceeding.</div>');
                setTimeout(function() {
                    $('.alert').fadeOut(function(){
                        $('.alert').remove();
                    });
                }, 5000);
                return false;
            }*/
            else
            {
                return true;
            }
        }
        else if(type=='2' || type=='3')
        {
            if($('.answerradio:checked').length!='1')
            {
                $('.pager').before('<div class=\'alert alert-danger\' role=\'alert\'>Select an answer before proceeding.</div>');
                setTimeout(function() {
                    $('.alert').fadeOut(function(){
                        $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
        }
        else if(type=='4')
        {
            if($('.answer').val()=='')
            {
                $('.answer').css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
                $('.answer').attr('placeholder', 'Please enter your answer before clicking next');
                setTimeout(function() {
                    $('.answer').css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
                    $('.answer').attr('placeholder', 'Enter your answer here');
                }, 5000);

                return false;
            }
        }
        else if(type=='5')
        {
            return checkMultypart();
        }
        else
        {
            return false;
        }
    }); 
</script>
<?php if($question->TM_QN_Type_Id=='5'):
    $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));

    ?>
    <script type="text/javascript">
        function checkMultypart()
        {
            var error=0;
            <?php foreach ( $questions AS $child):
            $childcorrectoption=$this->OptionHint($child->TM_QN_Type_Id,$child->TM_QN_Id);
            if($child->TM_QN_Type_Id=='1'):?>
            if($('.answerrcheck<?php echo $child->TM_QN_Id;?>:checked').length=='0')
            {
                $('.<?php echo 'parttest'.$child->TM_QN_Id;?>').after('<div class="alert alert-danger" role="alert">Select an answer before proceeding.</div>');
                setTimeout(function() {
                    $('.alert').fadeOut(function(){
                        $('.alert').remove();
                    });
                }, 5000);
                error++;

            }
/*            else if($('.answerrcheck<?php echo $child->TM_QN_Id;?>:checked').length!='<?php echo $childcorrectoption['correctoptions'];?>')
            {

                $('.<?php echo 'parttest'.$child->TM_QN_Id;?>').after('<div class="alert alert-danger" role="alert">Select any <?php echo $childcorrectoption['correctoptions'];?> options before proceeding.</div>');
                setTimeout(function() {
                    $('.alert').fadeOut(function(){
                        $('.alert').remove();
                    });
                }, 5000);
                error++
            }*/
            <?php
            elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):?>
            if($('.<?php echo 'answerradio'.$child->TM_QN_Id;?>:checked').length!='1')
            {
                $('.<?php echo 'parttest'.$child->TM_QN_Id;?>').after('<div class="alert alert-danger" role="alert">Select an answer before proceeding.</div>');
                setTimeout(function() {
                    $('.alert').fadeOut(function(){
                        $('.alert').remove();
                    });
                }, 5000);
                error++;
            }
            <?php
            elseif($child->TM_QN_Type_Id=='4'):?>
            if($('.answer<?php echo $child->TM_QN_Id;?>').val()=='')
            {
                $('.answer<?php echo $child->TM_QN_Id;?>').css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
                $('.answer<?php echo $child->TM_QN_Id;?>').attr('placeholder', 'Please enter your answer before clicking next');
                setTimeout(function() {
                    $('.answer<?php echo $child->TM_QN_Id;?>').css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
                    $('.answer<?php echo $child->TM_QN_Id;?>').attr('placeholder', 'Enter your answer here');
                }, 5000);

                error++;

            }
            <?php
            endif;
            ?>
            <?php  endforeach;?>
            if(error==0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    </script>
<?php
endif;
?>

</body>
</html>