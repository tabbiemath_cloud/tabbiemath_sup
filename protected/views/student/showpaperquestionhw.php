<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" xmlns="http://www.w3.org/1999/html"
     xmlns="http://www.w3.org/1999/html">
<div class="contentarea">
    <h1 class="h125">You have the following paper based questions to complete.</h1>
    <div class="bs-example">



        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <div style="text-align: right;">
                    <span style="padding-right: 10px;"><?php echo "<a href='" .Yii::app()->createUrl('student/getpdfhomework', array('id' => $id)). "' target='_blank'>Download</a>";?></span>

                </div>
            </div>

            <!-- Table -->
            <div class="table-responsive shadow-z-1">
                <table class="table table-hover table-mc-light-blue wirisfont">
                    <thead>

                    </thead>
                    <tbody class="font3 wirisfont">
                    <?php foreach($paperquestions AS $key=>$paperquestion):

                        $question=Questions::model()->findByPk($paperquestion->TM_STHWQT_Question_Id)
                        ?>
                        <?php if($question->TM_QN_Type_Id!='7'):?>

                    <!-- -->
                    <tr>
                        <td>
                            <div class="col-md-4" >
                                <span style="padding-left: 12px">Question <?php echo $key+1; ?></span>
                                &nbsp; &nbsp; &nbsp;Mark:<?php echo $question->TM_QN_Totalmarks; ?>
                            </div>


                            <div class="col-md-3 pull-right">
                                Ref:<?php echo $question->TM_QN_QuestionReff ?>

                            </div>


                            <div class="col-md-12">
                                <?php if($question->TM_QN_Image!=''):?>

                                <div class="col-md-7">
                                    <?php echo $question->TM_QN_Question;?>
                                </div>
                                <div class="col-md-5">
                                                     <span class="pull-right">
                            
                                                   <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" alt="">
                            
                                            </span>
                                </div>

                                <?php else:?>

                                <div class="col-md-12">
                                    <?php echo $question->TM_QN_Question;?>

                                </div>

                                <?php endif ?>
                            </div>




                        </td>
                    </tr>

                    <!-- -->

                        <?php elseif($question->TM_QN_Type_Id=='7'):
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));?>
                        <?php $childquestionsmark = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $multimark=0;
                        foreach ($childquestionsmark AS $childquestion):
                            $multimark=$multimark+(int)$childquestion->TM_QN_Totalmarks;

                        endforeach;?>
                    <tr>
                        <td style="padding-left: 0px;">
                            <div class="col-md-4">
                                <span style="padding-left: 12px">Question <?php echo $key+1; ?></span>
                                &nbsp; &nbsp; &nbsp;Mark:<?php echo $multimark ?>
                            </div>


                            <div class="col-md-3 pull-right">
                                Ref:<?php echo $question->TM_QN_QuestionReff ?>

                            </div>
                            <div class="col-md-12">
                                <?php if($question->TM_QN_Image!=''):?>

                                <div class="col-md-7">
                                    <?php echo $question->TM_QN_Question;?>
                                </div>
                                <div class="col-md-5">
                                                 <span class="pull-right">

                                               <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" alt="">

                                        </span>
                                </div>

                                <?php else:?>

                                <div class="col-md-12">
                                    <?php echo $question->TM_QN_Question;?>

                                </div>

                                <?php endif ?>
                            </div>


                            <?php $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                            $count = 1?>
                            <?php foreach ($questions AS $childquestion): ?>


                            <div class="col-md-12">
                                <?php if($childquestion->TM_QN_Image!=''):?>

                                <div class="col-md-12">
                                    Part:<?php echo $count ;?>
                                </div>

                                <div class="col-md-7">
                                    <?php echo $childquestion->TM_QN_Question;?>
                                </div>
                                <div class="col-md-5">
                                                 <span class="pull-right">

                                               <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childquestion->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" alt="">

                                        </span>
                                </div>

                                <?php else:?>

                                <div class="col-md-12">
                                    Part:<?php echo $count ;?>
                                </div>
                                <div class="col-md-12">
                                    <?php echo $childquestion->TM_QN_Question;?>

                                </div>

                                <?php endif ?>
                            </div><?php $count++ ?>

                            <?php endforeach; ?>
                        </td>
                    </tr>


                        <?php endif; endforeach;?>
                    <tr><td><button type="button" class="pull-right btn btn-warning complete" id="complete" data-value="<?php echo $id;?>">Complete</button></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="modal fade " id="invitModel" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form action="<?php echo Yii::app()->createUrl('/student/HomeworkSubmitnow',array('id'=>$id));?>" enctype="multipart/form-data" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body" id="managesubmission">
                                <div class="row" style="margin-left: 10px;">
                                    <input type="radio" id="nofiles" value="0" name="files" >I do not have files to attach
                                    <input type="radio" id="files" value="1" name="files" checked="true">I have files to attach

                                    <div class="row" style="margin-left: 10px;" id="attachfiles">
                                         <div class="col-md-12">
                                        <label class="control-label">Select Files</label>
                                    </div>
                                        <div class="col-md-8" style="display: flex;">
                                        <input style="width: 100%" id="file" type="file" class="file" multiple="true" name="file" accept="image/*">
                                         <img id="loader_process" src="<?php echo Yii::app()->request->baseUrl."/images/preloader.gif" ?>" style="width: 23px;margin-left: 0px; display: none;" >
                                         <button type="button" class="btn btn-warning" id="uploadimg">Upload</button>
                                        </div>
                                        <input type="hidden" name="homeworkid" id="homeworkid">
                                    </div>
                                </div>
                                <div id="answersheetlist" style="margin-top: 12px;"></div>
                            </div>
                            <div class="modal-footer">
                                <p id="note" style="float: left;">*Files can uploaded after submitting also, from History</p>
                                <br><br>
                                        <p  style="float: left; color: red; display: none;" id="imgonlymsg">Only image files are allowed.!</p>
                                <div class="row" id="submitnow" >
                                    <div class="col-lg-2 pull-right">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
                                    </div>
                                    <div class="col-lg-3 pull-right" >
                                        <a href="<?php echo Yii::app()->createUrl('student/homeworksubmit', array('id'=>$id));?>" style="text-decoration: none;"><button 
                                            style="display: none;" type="button" class="btn btn-warning btn-block nofilesubmitnown">SUBMIT NOW</button></a>
                                        <!--<input type="submit" name="action[Complete]" value="SUBMIT NOW" class="btn btn-warning btn-block submitnow">-->
                                    </div>

                                </div>
                                <div class="row" id="submitlater" style="">
                                    <div class="col-lg-12">
                                        <span class="alert alert-success pull-left fileinfo" style="display: none;"> </span>
                                        <span class="alert alert-success pull-left uploadinfo" style="display: none;"> </span>
                                        <p class="afterupload" style="display: none;">Please go to homepage, attach files and then submit.</p>
                                    </div>
                                    <div class="col-lg-2 pull-right">
                                       <!--  <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button> -->
                                    </div>
                                    <div class="col-lg-3 pull-right" >
                                        <button style="display: none;" type="submit" class="btn btn-warning submitlater" name="submit" value="submitlater">SUBMIT LATER</button>
                                        <!--<a href="<?php /*echo Yii::app()->createUrl('student/HomeworkSubmitlater', array('id'=>$id));*/?>" style="text-decoration: none;"><button type="button" class="btn btn-warning btn-block">SUBMIT LATER</button></a>-->
                                        <!--<input type="submit" name="action[Complete]" value="SUBMIT OFFLINE" class="btn btn-warning btn-block col-lg-2">-->
                                    </div>
                                    <div class="col-lg-4 pull-right" >
                                          
                                          <button style="display: none;" name="action[Complete]" value="SUBMIT NOW" type="submit" class="btn btn-warning submittonbutton"  id="uploadimg">Submit Now</button>
                                       <!--  <button type="submit" class="btn btn-warning btn-block" name="submit" id="submitnowbutton" value="submitnow">SUBMIT NOW</button> -->
                                        <!--<input type="submit" name="action[Complete]" value="SUBMIT OFFLINE" class="btn btn-warning btn-block col-lg-2">-->
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>

            <!--<div class="panel-footer">
                <form action="" method="post">
                    <div class="media-body ptp25" style=" text-align: -webkit-left; float: left; width: 265px;">
                        <input type="checkbox" id="checkcomplete" name="checkcomplete" value="Check To Complete" style="float: left; margin-right: 10px;">I completed working out the paper part.<br>
                    </div>
                    <div style="text-align: right;position: static;">
                        <button name="coomplete" id="GoNext" type="submit" class="btn btn-warning sanfont cmplt" style="margin-right:20px;width: 155px;" disabled="">Complete</button>
                    </div>
                </form>


            </div>-->

        </div>
        <script>
            function Checkfiles()
            {
                var fup = document.getElementById('input-1');
                var fileName = fup.value;
                var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

                if(ext =="PDF" || ext=="pdf" || ext=="jpg" || ext=="gif" || ext=="png")
                {
                    return true;
                }
                else
                {
                    $('.uploadinfo').html('Upload PDF,JPEG,GIF,PNG Files only');
                    $('.uploadinfo').show();
                    setTimeout(function(){
                        $('.uploadinfo').hide();
                    },3000);

                    return false;
                }
            }
            $(document).ready(function () {
                $('#complete').click(function(){
                    var hwid=$(this).data('value');
                    $('#homeworkid').val(hwid);
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetWorksheets";?>',
                data: {homework:hwid},
                success: function(data){
                    if(data!='no')
                    {
                        $('#answersheetlist').html(data);                      
                    }
                }
                }); 
                    
                });
                $('#complete').on('click',function(){
                    $('#invitModel').modal('toggle');
                    $("#managesubmission input[name='files']").click(function(){
                        if($('input:radio[name=files]:checked').val() == "0"){
                            $('#submitnow').show();
                            $('#submitlater').hide();
                            $('#attachfiles').hide();
                            $('#note').hide();
                        }
                        else
                        {
                            $('#attachfiles').show();
                            $('#submitlater').show();
                            $('#submitnow').hide();
                            $('#note').show();
                        }
                    });
                });
                $('#submitnowbutton').on('click',function(){
                    var fup = document.getElementById('input-1');
                    var fileName = fup.value;
                    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
                    if(fileName=='')
                    {
                        $('.fileinfo').html('Please select file.');
                        $('.fileinfo').show();
                        setTimeout(function(){
                            $('.fileinfo').hide();
                        },3000);
                        return false;
                    }
                    if(ext =="PDF" || ext=="pdf" || ext=="jpg" || ext=="gif" || ext=="png")
                    {
                        return true;
                    }
                    else
                    {
                        $('.uploadinfo').html('Upload PDF,JPEG,GIF,PNG Files only');
                        $('.uploadinfo').show();
                        setTimeout(function(){
                            $('.uploadinfo').hide();
                        },3000);
                        return false;
                    }
                });
            });
        </script>











        <?php Yii::app()->clientScript->registerScript('showbtn', "
$('input:checkbox').change(function () {
    if ($('input[type=checkbox]:checked').length > 0) {
        $('#GoNext').attr('disabled',false);
    } else {
        $('#GoNext').attr('disabled',true);
    }
});");?>


<script type="text/javascript"> 
        $(document).ready(function() { 
        $("#file").change(function() { 
        var fileName = document.getElementById("file").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
        $("#imgonlymsg").hide();
            //TO DO
        }else{
           
            $("#imgonlymsg").show();
            return false;
        }
        });   
        $("#uploadimg").click(function() { 
        var fileName = document.getElementById("file").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
            //TO DO
        }else{
           
            $("#imgonlymsg").show();
            return false;
        } 
               $('#loader_process').show();
                $("#imgonlymsg").hide();
                var fd = new FormData(); 
                var files = $('#file')[0].files[0]; 
                var homework_id = $('#homeworkid').val();
                fd.append('file', files); 
                fd.append('homework_id', homework_id); 

                console.log(fd);
                $.ajax({ 
                    url: '<?php echo Yii::app()->request->baseUrl."/student/uploadanswersheet";?>', 
                    type: 'post', 
                    data: fd, 
                    contentType: false, 
                    processData: false, 
                    success: function(response){ 
                document.getElementById("file").value = "";
                $('.submitlater').show();
                $('.submittonbutton').show();
               if(response != 0){ 
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetWorksheets";?>',
                data: {homework:homework_id},
                success: function(data){
                    if(data!='no')
                    {
                        $('#answersheetlist').html(data);
                        $('#file').val(''); 
                        $('#loader_process').hide();                        
                    }
                }
                }); 
                        } 
                        else{ 
                            console.log('file not uploaded'); 
                        } 
                    }, 
                }); 
            }); 
    
        });

            $(document).on("click", '.deleteworksheet', function(event) { 
            var worksheet=$(this).data('value');
            
                $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/deleteWorksheet";?>',
                data: {worksheet:worksheet},
                success: function(data){
                    if(data!='no')
                    {
                        $('#file'+worksheet).remove();                        
                    }
                }
                })
            
        }); 

$(document).on('click','#nofiles', function(){

    if($("#nofiles").prop('checked') == true)
    {
       $('.nofilesubmitnown').show();
    }

});

$(document).on('click','#files', function(){

    if($("#files").prop('checked') == true)
    {
       $('.nofilesubmitnown').hide();
    }

});

    </script>