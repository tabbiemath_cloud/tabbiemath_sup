<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

<div class="contentarea">

<form action="" method="post" id="questionform">
<input type="hidden" name="publisher" value="<?php echo Yii::app()->session['publisher']; ?>">
<div class="bs-example">
<h1 class="h125">Start a revision by selecting questions</h1>
		<div class="panel panel-default nonmargin"> 
			<div class="row hidden-lg hidden-md hidden-xs" style="padding-top: 10px;">
				<div class="col-xs-6" style=" padding-top: 12px;">
					<?php if (Yii::app()->session['quick'] & !Yii::app()->session['difficulty']): ?>				
						<input type="radio" name="testtype" class="RevisionCheck margin_bottom_new" value="Quick" checked="checked">Revision
					<?php elseif (Yii::app()->session['difficulty'] & !Yii::app()->session['quick']): ?>
						<input type="radio" name="testtype" class="RevisionCheck margin_bottom_new" value="Difficulty" checked="checked">Revision by difficulty
					<?php elseif (Yii::app()->session['difficulty'] & Yii::app()->session['quick']): ?>
					<input type="radio" name="testtype" class="RevisionCheck margin_bottom_new" value="Quick" checked="checked">Revision<br>
					<input type="radio" name="testtype" class="RevisionCheck margin_bottom_new" value="Difficulty">Revision by difficulty
					<?php endif;?>					
				</div>
				<div class="col-xs-6 text-right" style=" padding-bottom: 10px;">					
					<button class="btn btn-warning revisionvalidation" name="startTest" style=" margin-bottom: 4px;">Start Test</button><br>
					<button class="btn btn-warning revisionvalidationtodo"name="addTest">Add to To-Do List</button>
				</div>
			</div>
			<table class="hidden-sm rwd2-tables table_bord_none" id="primarytable">
				<tbody>													
					<tr>
					<?php if (Yii::app()->session['quick'] & !Yii::app()->session['difficulty']): ?>
                        <td>
                            <input type="radio" name="testtype" class="RevisionCheck" value="Quick" checked="checked">
                            Quick Revision
                        </td>
                    <?php elseif (Yii::app()->session['difficulty'] & !Yii::app()->session['quick']): ?>
                        <td>
                            <!--<input type="hidden" name="publisher" value="3">-->
                            <input type="radio" name="testtype" class="RevisionCheck" value="Difficulty"
                                   checked="checked">
                            Revision by difficulty
                        </td>
                    <?php
                    elseif (Yii::app()->session['difficulty'] & Yii::app()->session['quick']): ?>
                        <td>
                            <input type="radio" name="testtype" class="RevisionCheck" value="Quick" checked="checked">
                            Quick Revision
                        </td>
                        <td>

                            <input type="radio" name="testtype" class="RevisionCheck" value="Difficulty">
                            Revision by difficulty
                        </td>
                    <?php endif; ?>																		
					<td class="text-right text_lft"><button class="btn btn-warning revisionvalidation"  name="startTest">Start Test</button></td>
					<td class="text-right text_lft"><button class="btn btn-warning revisionvalidationtodo" name="addTest">Add to To-Do List</button></td>
					</tr>																											
				</tbody>
			</table>  
		</div>
<?php if (Yii::app()->session['quick']): ?>
    
    <div class="panel panel-default clearfix" id="QuickChapters">
    <div class="panel-body padnone">
    <input type="hidden" id="Quickquestiontotal" value="0" />
    <div class="panel-heading">
        
        <b>Topics Chosen</b> : <span id="Quicktopicindication">No Topics Selected</span>

        <div class="pull-right  hidden-xs hidden-sm">
            Selected Questions : <span class="Quickquestiontotalinfo">0</span>
        </div>
        <div class="pull-right  hidden-xs hidden-sm">
            Maximum Questions : <span>25 &nbsp;</span>
        </div>
		<div class="visible-xs visible-sm margin">
            Selected Questions : 
                <span class="Quickquestiontotalinfo">0</span>
		</div>
		<div class="visible-xs visible-sm margin">
            Maximum Questions : 
                <span>25 &nbsp;</span>
		</div>
    </div>
    <?php foreach ($chapters AS $key => $chapter):
        if ($this->GetChapterTotal($chapter->TM_TP_Id, '1') > 0):
            ?>
            <div>
                <div class="col-xs-12 toggle-header">
                    <div class="col-xs-12 col-md-6 ">
                                            <span data-toggle="tooltip" data-placement="left"
                                                  title="Select topics within the chapter that you want to revise on">
                                                <button type="button" class="btn btn-default btn-circle"
                                                        id="quicktoggleheader<?php echo $key; ?>" data-toggle="collapse"
                                                        data-target="#<?php echo 'quickfeature-' . $key; ?>"
                                                        aria-expanded="true">
                                                    <i class="fa fa-chevron-down"></i>
                                                </button>
                                            <input type="hidden" id="quickchapter<?php echo $key; ?>"
                                                   class="quickselectedchapter" name="quickchapter[]" value="">
                                            <a href="#" data-toggle="collapse"
                                               data-target="#<?php echo 'quickfeature-' . $key; ?>"> <?php echo $chapter->TM_TP_Name; ?></a></span>
                    </div>
                    <div class="col-xs-12 col-md-4 text-left pull-right">
                        <input id="<?php echo 'quick' . $key; ?>"
                               name="<?php echo 'quick' . $chapter->TM_TP_Id; ?>" value="0" type="hidden">
                        <input id="<?php echo 'slidequick' . $key; ?>" class="selectslide" data-slider-id='<?php echo 'slidequick' . $key; ?>Slider' type="text"
                               data-parent="<?php echo $key; ?>" data-slider-min="0" data-slider-max="10"
                               data-slider-step="1" data-slider-value="0"/>
                        <span class="pull-left">0</span>
                        <span class="pull-right">10</span>
                        <input type="hidden" value="0" id="quickchaptertotal<?php echo $key; ?>"
                               name="quickchaptertotal<?php echo $chapter->TM_TP_Id; ?>"
                               class="quickchaptertotal">
                        <input type="hidden" id="<?php echo 'quickchaptertotaltitle' . $key; ?>" class="total" value="0">
                    </div>
                </div>


                <div id="<?php echo 'quickfeature-' . $key; ?>" class="collapse <?php //echo ($key=='0'?'in':'');?>" aria-expanded="true">


                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-xs " style="margin-left: 15px;" id="uncheckall"
                                    href="javascript:void(0)">
                                Clear all
                            </button>
                        </div>
                        <?php
                        foreach (array_chunk($chapter->topic, 4) as $topics):
                            ?>
                            <div class="col-xs-12 col-md-4">

                                <ul class="listnew">

                                    <?php foreach ($topics as $topic): ?>
                                        <li>
                                            <input type="checkbox" class="inputtopic<?php echo $key ?>"
                                                   name="<?php echo 'quicktopic' . $chapter->TM_TP_Id . '[]'; ?>"
                                                   checked="checked"
                                                   value="<?php echo $topic->TM_SN_Id; ?>"> <?php echo $topic->TM_SN_Name; ?>
                                        </li>
                                    <?php endforeach; ?>

                                </ul>


                            </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                </div>
            </div>
            <?php
            Yii::app()->clientScript->registerScript('quickslider' . $key, "

                        var x=  $('#slidequick" . $key . "').slider({

                                                value:'0',

                                                formatter: function(value) {
                                                    return 'Current value: ' + value;
                                                }
                                            });
                                            x.on('slide', function(ev){

                                             var atLeastOneIsChecked = false;
                                                  $('#quickfeature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;
                                                     // Stop .each from processing any more items
                                                     return false;
                                                     }
                                                     });
                                                if( !atLeastOneIsChecked ){

                                                     var value=0;
                                                     $('#slidequick" . $key . "').slider('setValue', value);
                                                     $('.alert').remove();
                                                      $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select at least one topic before selecting questions.</div>')
                                                       setTimeout(function() {
                                                           $('.alert').fadeOut(function(){
                                                        $('.alert').remove();
                                                       });
                                                     }, 5000);

                                                    }

                                                   else
                                                   {
                                                var totalquestions=($('#Quickquestiontotal').val()*1)+$('#slidequick" . $key . "').data('slider').getValue();
                                                        var newVal = $('#slidequick" . $key . "').data('slider').getValue();

                                                        $('#quick" . $key . "').val(newVal);
                                                        var chaptertotal=($('#quick" . $key . "').val()*1);
                                                        if(chaptertotal!='0')
                                                        {
                                                            $('#quickchapter" . $key . "').val('" . $chapter->TM_TP_Id . "');
                                                        }
                                                        else{
                                                        $('#quickchapter" . $key . "').val('');
                                                        }
                                                        $('#quickchaptertotal" . $key . "').val(chaptertotal);
                                                        $('#quickchaptertotaltitle" . $key . "').text(chaptertotal);
                                                        var questiontotal=0;
                                                        $('.quickchaptertotal').each(function(){
                                                                totalqst=($(this).val()*1);
                                                                questiontotal=questiontotal+totalqst
                                                        });
                                                        $('#Quickquestiontotal').val(questiontotal);
                                                        $('.Quickquestiontotalinfo').html(questiontotal);
                                                        GetChapterQuick()


                                                  }


                                            });
                      $('#quickfeature-" . $key . " #uncheckall').click(function () {


                             $('#quickfeature-" . $key . " input[type=checkbox]').each(function () {
                                $(this).prop('checked', false);

                          });
                          $('#quickchapter" . $key . "').val('');
                          GetChapterQuick()
                          var tot=$('#Quickquestiontotal').val();
                          var tot2=$('#quickchaptertotaltitle" . $key . "').html();
                          $('#Quickquestiontotal').val(tot-tot2);
                      var x=$('#slidequick" . $key . "').slider({
                                  value:'0',
                       formatter: function(value) {
                                                    return 'Current value: ' + value;
                                                }
                                            });
                                            var value=0;

            x.slider('setValue', value);
            $('#quickchaptertotaltitle" . $key . "').text(0);
             $('#quickchaptertotal" . $key . "').val('');
             


    });
    $('.inputtopic" . $key . "').click(function () {
                      var atLeastOneIsChecked = false;
                                                  $('#quickfeature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;
                                                     // Stop .each from processing any more items
                                                     return false;
                                                     }
                                                     });
                                                if( !atLeastOneIsChecked ){
                                                $('#quickchapter" . $key . "').val('');
                          GetChapterQuick()
                               var tot=$('#Quickquestiontotal').val();
                          var tot2=$('#quickchaptertotaltitle" . $key . "').html();
                          $('#Quickquestiontotal').val(tot-tot2);
                          $('#quickchaptertotaltitle" . $key . "').text(0);
                                                     var value=0;
                                                     $('#slidequick" . $key . "').slider('setValue', value);
                                                     $('.alert').remove();
                                                      $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select at least one topic before selecting questions.</div>')
                                                       setTimeout(function() {
                                                           $('.alert').fadeOut(function(){
                                                        $('.alert').remove();
                                                       });
                                                     }, 5000);
                                                       
                                                    }
													});


                            ");
        endif;
    endforeach;?>
    </div>
    </div>
<?php endif;
if (Yii::app()->session['difficulty']):?>
    <div class="panel panel-default clearfix" <?php echo(Yii::app()->session['difficulty'] & !Yii::app()->session['quick'] ? 'style="display:block"' : ''); ?>
        id="DifficultyChapters">
    <div class="panel-body padnone">
	<div class="panel-heading">
        <b>Topics Chosen</b> : <span id="topicindication">No Topics Selected</span>

        <div class="pull-right  hidden-xs hidden-sm">
            Selected Questions : <span id="questiontotal">0</span>
        </div>
        <div class="pull-right  hidden-xs hidden-sm">
            Maximum Questions : <span>25 &nbsp;</span>
        </div>
		<div class="visible-xs visible-sm margin">
            Selected Questions : 
                <span id="questiontotal">0</span>
		</div>
		<div class="visible-xs visible-sm margin">
            Maximum Questions : 
                <span>25 &nbsp;</span>
		</div>
    </div>

    <?php foreach ($chapters AS $key => $chapter):
        if ($this->GetChapterTotal($chapter->TM_TP_Id, '1') > 0):
            ?>
            
                <div class="col-xs-12 toggle-header">
                    <div class="col-xs-12 col-md-6">
                                        <span data-toggle="tooltip" data-placement="left"
                                              title="Select topics within the chapter that you want to revise on"><button
                                                type="button" class="btn btn-default btn-circle"
                                                id="toggleheader<?php echo $key; ?>" data-toggle="collapse"
                                                data-target="#<?php echo 'feature-' . $key; ?>" aria-expanded="true">
                                                <i class="fa fa-chevron-down"></i>
                                            </button>
                                        <input type="hidden" id="chapter<?php echo $key; ?>" class="selectedchapter"
                                               name="chapter[]" value="">
                                        <a href="#" data-toggle="collapse"
                                           data-target="#<?php echo 'feature-' . $key; ?>"> <?php echo $chapter->TM_TP_Name; ?></a></span>
                    </div>
                    <div class="col-xs-12 col-md-2 text-left">
                        <label class="control-label col-lg-12 lab2">Basic</label>
                        <input id="<?php echo 'basic' . $key; ?>" name="<?php echo 'basic' . $chapter->TM_TP_Id; ?>"
                               value="0" type="hidden">
                        <input id="<?php echo 'slidebasic' . $key; ?>" class="selectslide"
                               data-slider-id='<?php echo 'slidebasic' . $key; ?>Slider' type="text"
                               data-parent="<?php echo $key; ?>" data-slider-min="0" data-slider-max="5"
                               data-slider-step="1" data-slider-value="0"/>
                        <span class="pull-left">0</span>
                        <span class="pull-right">5</span>
                    </div>
                    <div class="col-xs-12 col-md-2 text-left">
                        <label class="control-label col-lg-12 lab2">Intermediate</label>
                        <input id="<?php echo 'inter' . $key; ?>" name="<?php echo 'inter' . $chapter->TM_TP_Id; ?>"
                               value="0" type="hidden">
                        <input id="<?php echo 'slideinter' . $key; ?>" class="selectslide"
                               data-slider-id='<?php echo 'slideinter' . $key; ?>Slider' type="text"
                               data-parent="<?php echo $key; ?>" data-slider-min="0" data-slider-max="5"
                               data-slider-step="1" data-slider-value="0"/>
                        <span class="pull-left">0</span>
                        <span class="pull-right">5</span>
                    </div>
                    <div class="col-xs-12 col-md-2 text-left">
                        <label class="control-label col-lg-12 lab2">Advanced</label>
                        <input id="<?php echo 'adv' . $key; ?>" name="<?php echo 'adv' . $chapter->TM_TP_Id; ?>"
                               value="0" type="hidden">
                        <input id="<?php echo 'slideadv' . $key; ?>" class="selectslide"
                               data-slider-id='<?php echo 'slideadv' . $key; ?>Slider' type="text" data-slider-min="0"
                               data-parent="<?php echo $key; ?>" data-slider-max="5" data-slider-step="1"
                               data-slider-value="0"/>
                        <span class="pull-left">0</span>
                        <span class="pull-right">5</span>
                    </div>
  
                </div>
                <div id="<?php echo 'feature-' . $key; ?>" class="collapse <?php //echo ($key=='0'?'in':'');?>" aria-expanded="true">

                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-xs " style="margin-left: 15px;" id="uncheckall"
                                    href="javascript:void(0)">
                                Clear all
                            </button>
                        </div>


                        <?php
                        foreach (array_chunk($chapter->topic, 4) as $topics):
                            ?>
                            <div class="col-xs-12 col-md-4">
                                <ul class="listnew">

                                    <?php foreach ($topics as $topic): ?>
                                        <li>
                                            <input type="checkbox" class="featureinputtopic<?php echo $key ?>"
                                                   name="<?php echo 'topic' . $chapter->TM_TP_Id . '[]'; ?>"
                                                   checked="checked"
                                                   value="<?php echo $topic->TM_SN_Id; ?>"> <?php echo $topic->TM_SN_Name; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php
                        endforeach;
                        ?>

                    </div>
                </div>
            
<?php
#$('#feature-".$key."').collapse('show');
            Yii::app()->clientScript->registerScript('slider' . $key, "
                                            $('#slidebasic" . $key . "').slider({

                                                value:'0',
                                                formatter: function(value) {
                                                    return 'Current value: ' + value;
                                                }
                                            }).on('slide', function(ev){
                                                 var atLeastOneIsChecked = false;
                                                  $('#feature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;

                                                     }
                                                     });
                                                if( !atLeastOneIsChecked ){

                                                     var value=0;
                                                     $('#slidebasic" . $key . "').slider('setValue', value);
                                                     $('.alert').remove();
                                                      $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select at least one topic before selecting questions.</div>')
                                                       setTimeout(function() {
                                                           $('.alert').fadeOut(function(){
                                                        $('.alert').remove();
                                                       });
                                                     }, 5000);
                                                    }
                                                    else
                                                    {

                                                var totalquestions=($('#questiontotal').html()*1)+$('#slidebasic" . $key . "').data('slider').getValue();


                                                    var newVal = $('#slidebasic" . $key . "').data('slider').getValue();
                                                    $('#basic" . $key . "').val(newVal);
                                                    var basictot=($('#basic" . $key . "').val()*1);
                                                    var intertot=($('#inter" . $key . "').val()*1);
                                                    var advtot=($('#adv" . $key . "').val()*1);
                                                    var chaptertotal=basictot+intertot+advtot;
                                                    if(chaptertotal!='0')
                                                    {
                                                        $('#chapter" . $key . "').val('" . $chapter->TM_TP_Id . "');
                                                    }
                                                    else{
                                                    $('#chapter" . $key . "').val('');
                                                    }
                                                    $('#chaptertotal" . $key . "').val(chaptertotal);
                                                    $('#chaptertotaltitle" . $key . "').text(chaptertotal);
                                                    var questiontotal=0;
                                                    $('.chaptertotal').each(function(){
                                                            totalqst=($(this).val()*1);
                                                            questiontotal=questiontotal+totalqst
                                                    });
                                                    $('#questiontotal').html(questiontotal);
                                                    GetChapter()


                                                }
                                            });


                                            $('#slideinter" . $key . "').slider({
                                                value:'0',
                                                formatter: function(value) {
                                                    return 'Current value: ' + value;
                                                }
                                            }).on('slide', function(ev){
                                             var atLeastOneIsChecked = false;
                                                  $('#feature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;

                                                     }
                                                     });
                                                if( !atLeastOneIsChecked ){

                                                     var value=0;
                                                     $('#slideinter" . $key . "').slider('setValue', value);
                                                     $('.alert').remove();
                                                      $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select at least one topic before selecting questions.</div>')
                                                       setTimeout(function() {
                                                           $('.alert').fadeOut(function(){
                                                        $('.alert').remove();
                                                       });
                                                     }, 5000);
                                                    }
                                                    else
                                                    {

                                                var totalquestions=($('#questiontotal').html()*1)+$('#slideinter" . $key . "').data('slider').getValue();

                                                    var newVal = $('#slideinter" . $key . "').data('slider').getValue();
                                                    $('#inter" . $key . "').val(newVal);
                                                    var basictot=($('#basic" . $key . "').val()*1);
                                                    var intertot=($('#inter" . $key . "').val()*1);
                                                    var advtot=($('#adv" . $key . "').val()*1);
                                                    var chaptertotal=basictot+intertot+advtot;
                                                    if(chaptertotal!='0')
                                                    {
                                                        $('#chapter" . $key . "').val('" . $chapter->TM_TP_Id . "');
                                                    }
                                                    else{
                                                    $('#chapter" . $key . "').val('');
                                                    }
                                                    $('#chaptertotal" . $key . "').val(chaptertotal);
                                                    $('#chaptertotaltitle" . $key . "').text(chaptertotal);
                                                    var questiontotal=0;
                                                    $('.chaptertotal').each(function(){
                                                            totalqst=($(this).val()*1);
                                                            questiontotal=questiontotal+totalqst
                                                    });
                                                    $('#questiontotal').html(questiontotal);
                                                    GetChapter()


                                                }
                                            });

                                            $('#slideadv" . $key . "').slider({
                                                value:'0',
                                                formatter: function(value) {
                                                    return 'Current value: ' + value;
                                                }
                                            }).on('slide', function(ev){

                                             var atLeastOneIsChecked = false;
                                                  $('#feature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;

                                                     }
                                                     });
                                                if( !atLeastOneIsChecked ){

                                                     var value=0;
                                                     $('#slideadv" . $key . "').slider('setValue', value);
                                                     $('.alert').remove();
                                                      $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select at least one topic before selecting questions.</div>')
                                                       setTimeout(function() {
                                                           $('.alert').fadeOut(function(){
                                                        $('.alert').remove();
                                                       });
                                                     }, 5000);
                                                    }
                                                    else
                                                    {


                                                var totalquestions=($('#questiontotal').html()*1)+$('#slideadv" . $key . "').data('slider').getValue();


                                                    var newVal = $('#slideadv" . $key . "').data('slider').getValue();
                                                    $('#adv" . $key . "').val(newVal);
                                                    var basictot=($('#basic" . $key . "').val()*1);
                                                    var intertot=($('#inter" . $key . "').val()*1);
                                                    var advtot=($('#adv" . $key . "').val()*1);
                                                    var chaptertotal=basictot+intertot+advtot;
                                                    if(chaptertotal!='0')
                                                    {
                                                        $('#chapter" . $key . "').val('" . $chapter->TM_TP_Id . "');
                                                    }
                                                    else{
                                                    $('#chapter" . $key . "').val('');
                                                    }
                                                    $('#chaptertotal" . $key . "').val(chaptertotal);
                                                    $('#chaptertotaltitle" . $key . "').text(chaptertotal);
                                                    var questiontotal=0;
                                                    $('.chaptertotal').each(function(){
                                                            totalqst=($(this).val()*1);
                                                            questiontotal=questiontotal+totalqst
                                                    });
                                                    $('#questiontotal').html(questiontotal);
                                                    GetChapter()
                                                }


                                            });



     $('#feature-" . $key . " #uncheckall').click(function () {


             $('#feature-" . $key . " input[type=checkbox]').each(function () {
                 $(this).prop('checked', false);
              });
               $('#chapter" . $key . "').val('');
                 GetChapter()
              var tot=$('#questiontotal').html();
                          var tot2=$('#chaptertotaltitle" . $key . "').html();
                          $('#questiontotal').html(tot-tot2);
                          $('#chaptertotaltitle" . $key . "').text(0);
                          $('#chaptertotal" . $key . "').val('');
            var x=$('#slidebasic" . $key . "').slider({
                    value:'0',
                    formatter: function(value) {
                    return 'Current value: ' + value;
                    }
                    });
                    var value=0;

                     x.slider('setValue', value);
            var y=$('#slideinter" . $key . "').slider({

                    value:'0',
                    formatter: function(value) {
                    return 'Current value: ' + value;
                    }
                    });
                    var value=0;

            y.slider('setValue', value);

            var z=$('#slideadv" . $key . "').slider({
                    value:'0',
                     formatter: function(value)
                      {
                    return 'Current value: ' + value;
                    }
                    });
                    var value=0;

            z.slider('setValue', value);



    });
    $('.featureinputtopic" . $key . "').click(function () {
                      var atLeastOneIsChecked = false;
                                                  $('#feature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;
                                                     // Stop .each from processing any more items
                                                     return false;
                                                     }
                                                     });
                                                if( !atLeastOneIsChecked ){
                                                $('#chapter" . $key . "').val('');
                                                      GetChapter()
              var tot=$('#questiontotal').html();
                          var tot2=$('#chaptertotaltitle" . $key . "').html();
                          $('#questiontotal').html(tot-tot2);
                          $('#chaptertotaltitle" . $key . "').text(0);
                          $('#chaptertotal" . $key . "').val('');
            var x=$('#slidebasic" . $key . "').slider({
                    value:'0',
                    formatter: function(value) {
                    return 'Current value: ' + value;
                    }
                    });
                    var value=0;

                     x.slider('setValue', value);
            var y=$('#slideinter" . $key . "').slider({

                    value:'0',
                    formatter: function(value) {
                    return 'Current value: ' + value;
                    }
                    });
                    var value=0;

            y.slider('setValue', value);

            var z=$('#slideadv" . $key . "').slider({
                    value:'0',
                     formatter: function(value)
                      {
                    return 'Current value: ' + value;
                    }
                    });
                    var value=0;

            z.slider('setValue', value);

            }
													});


                                            ");
        endif;
    endforeach;?>

    </div>

    </div>
<?php endif; ?>
</div>
</form>
</div>
</div>
<?php
Yii::app()->clientScript->registerScript('checkboxval', "

    $('.RevisionCheck').click(function(){
        if($(this).val()=='Quick')
        {
            $('#DifficultyChapters').fadeOut(function(){
                $('#QuickChapters').fadeIn();
            });
        }
        else
        {
            $('#QuickChapters').fadeOut(function(){
                $('#DifficultyChapters').fadeIn();
            });
        }


    });
    function GetChapter()
    {
        var count=0;
        $('.selectedchapter').each(function(){
                if(count=='0')
                {
                    topicids=$(this).val();
                }
                else
                {
                    topicids=topicids+','+$(this).val();
                }
                count++;
        });
        if(topicids!='')
        {
            $('#topicindication').text('')
            $.ajax({
                type: 'POST',
                url: '" . Yii::app()->createUrl('student/getTopics') . "',
                data: { topicids: topicids}
            })
              .done(function( data ) {
              $('#topicindication').text(data);
            });
        }
        else
        {
            $('#topicindication').text('No Topics Selected');
        }
    }
    function GetChapterQuick()
    {
        var count=0;
        $('.quickselectedchapter').each(function(){
                if(count=='0')
                {
                    topicids=$(this).val();

                }
                else
                {
                    topicids=topicids+','+$(this).val();
                }
                count++;
        });
        if(topicids!='')
        {
            $('#Quicktopicindication').text('')
            $.ajax({
                type: 'POST',
                url: '" . Yii::app()->createUrl('student/getTopics') . "',
                data: { topicids: topicids}
            })
              .done(function( data ) {
              $('#Quicktopicindication').text(data);
            });
        }
        else
        {
            $('#Quicktopicindication').text('No Topics Selected');

        }
    }
    $('#questionform').submit(function(){
        $('.alert').remove();
        if($('.RevisionCheck:checked').val()=='Quick')
        {
            if(($('#Quickquestiontotal').val()*1)=='0')
            {
                $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select chapters before proceeding.</div>')
                setTimeout(function() {
                     $('.alert').fadeOut(function(){
                         $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            if(($('#questiontotal').text()*1)=='0')
            {
                $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select chapters before proceeding.</div>')
                setTimeout(function() {
                     $('.alert').fadeOut(function(){
                         $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
            else
            {
                return true;
            }
        }


    });


");?>
<?php Yii::app()->clientScript->registerScript('revisionvalidationval', "
 $('.revisionvalidation').click(function(){
 var totquick=$('#Quickquestiontotal').val();
 
 var totfeature=$('#questiontotal').html();
 if($('.RevisionCheck:checked').val()=='Quick'){
 if(totquick>25 )
 {
 $('.alert').remove();
 $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Maximum 25 Questions are allowed.</div>')
 setTimeout(function() {
                     $('.alert').fadeOut(function(){
                         $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
            else
            {
                return true;
            }
 }
 else{
 $('.alert').remove();
  if(totfeature>25 ){
 $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Maximum 25 Questions are allowed.</div>')
 setTimeout(function() {
                     $('.alert').fadeOut(function(){
                         $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
            else
            {
                return true;
            }
 }


 });
 $('.revisionvalidationtodo').click(function(){
 var totquick=$('#Quickquestiontotal').val();
 alert(totquick); return false;
 var totfeature=$('#questiontotal').html();
 if($('.RevisionCheck:checked').val()=='Quick'){
 if(totquick>25 )
 {
 $('.alert').remove();
 $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Maximum 25 Questions are allowed.</div>')
 setTimeout(function() {
                     $('.alert').fadeOut(function(){
                         $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
            else
            {
                return true;
            }
 }
 else{
  if(totfeature>25 ){
  $('.alert').remove();
 $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Maximum 25 Questions are allowed.</div>')
 setTimeout(function() {
                     $('.alert').fadeOut(function(){
                         $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
            else
            {
                return true;
            }
 }


 });
");

?>
