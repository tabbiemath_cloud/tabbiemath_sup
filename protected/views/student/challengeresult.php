<style>
.poci {
    text-align: right;
     float: none !important; 
    padding: 10px;
}
</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="contentarea">
        <h1 class="h125"> </h1>
        	<div class="bs-example">
				<div class="panel panel-default">				
					<div class="row">
							<div class="col-md-12">							
								<div class="outer-div col-md-12 col-lg-12 col-xs-12 text12525">
									<h1 style="font-weight: 700;">You scored <?php echo$studenttotal.'/'.$totalmarks;?></h1>											
									<?php if(count($participants)!=1):?>
									<p class="yf">Your position in challenge is</p>
									<h1 style="font-weight: 700;  font-size: 100px;" class="poci"><?php echo $this->GetChallengePosition($userids, Yii::app()->user->id);?></h1>
                                    <?php endif;?>
										
								</div>

								<div class="middle-div col-md-12 col-lg-12 col-xs-12 pull-right text12525">	
                                <?php if(count($participants)!=1):                                
                                        if($completed==count($participants)):
                                            echo $this->GetChallengeInviteesForResults($testid, Yii::app()->user->id);
                                        else:
                                            echo ((count($participants)-1)==1 ? (count($participants)-$completed)." more buddy is yet to take the challenge". $this->GetChallengeInviteesForResults($testid, Yii::app()->user->id) :(count($participants)-$completed)." more buddies are yet to take the challenge". $this->GetChallengeInviteesForResults($testid, Yii::app()->user->id));
                                        endif;    
                                    endif;?>								
									
									<h4 style="font-weight: 700;margin-bottom: 20px;"><?php

                            $level=Levels::model()->findByPk($pointlevels['studentlevel']);
                            if($pointlevels['newlevel']):?>
                                <p class="yf">YOU ARE NOW A <?php echo strtoupper($level->TM_LV_Name);?> !!</p>
                            <?php endif;?>                            
                            <?php if($pointlevels['newlevel']):?>
                                <img class="bdg" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$level->TM_LV_icon.'?size=large&type=level&width=150&height=150';?>" alt="">
                                <h1 style="font-weight: 700;margin-bottom: 20px;">Congratulations !!</h1>
                            <?php endif;?>
                            <?php
                            if(Yii::app()->session['userlevel']==1):

                            else:
                                $level=Levels::model()->find(array('condition'=>'TM_LV_Minpoint>'.$pointlevels['total'],'order'=>'TM_LV_Minpoint ASC'));
                                if($level & !$pointlevels['newlevel']):
                                    $nextpoint=$level->TM_LV_Minpoint-$pointlevels['total'];
                                    echo '<h4 style="font-weight: 700;margin-bottom: 20px;">'.$nextpoint.' more points to be a '.strtoupper($level->TM_LV_Name).'. Go for it !!!</h4>';
                                endif;
                            endif;
                            ?>
                            </h4>
										
								</div>
							</div>
					</div>
				</div>
			</div>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">Here is your challenge result</h1>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div style="text-align: right;">
                        View Solution <img id="viewsolution" data-toggle="modal" data-target="#solutionModel" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" style="margin-left:10px;cursor: pointer">

                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table">
                        <tbody class="font3 wirisfont">
                        <?php echo $testresult;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myinvite" class="modal fade" role="dialog" style="padding-top:20%;">
  <div class="modal-dialog modal-sm modal-sm_plus">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Challenge position</h4>
      </div>
      <div class="modal-body modal-body_padding">
      <div class="row">
        <ul class='popu-cart' role='menu' style='display: block !important;'>                  
            </ul>
            </div>
      </div>
    </div>

  </div>
</div>
<div class="modal fade " id="solutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>
            </div>
            <div class="panel-heading">
                <div style="text-align: right;">
                    <!--<span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('student/GetchallengepdfAnswer',array('id'=>$_GET['id']));?>" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('student/getchallengepdfAnswer',array('id'=>$_GET['id']));?>" target="_blank"><i class="fa fa-print fa-lg"></i></a>-->

                </div>
            </div>
            <div class="modal-body" >
                <table class="table ">
                    <thead>

                    </thead>
                    <tbody class="font3" id="showsolution">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php Yii::app()->clientScript->registerScript('sendmailreminder', "
 $(document).on('click','.reminder',function(){
  var recepientid= $(this).attr('data-id');
  var challengeid= $(this).attr('data-challenge');
  $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('student/SendMailReminders')."',
                  data: { recepientid: recepientid,challengeid:challengeid}

                })
                .done  (function(data, textStatus, jqXHR){
                $('.rem'+recepientid).remove();
                $('.remsend'+recepientid).slideDown();
                setTimeout(function(){
                        $('.remsend'+recepientid).remove();
                    },6000)

                });

 });"); ?>
<?php
$percentage=($studenttotal/$totalmarks)*100;
Yii::app()->clientScript->registerScript('showmarks', "
$('#viewsolution').click(function(){
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/ChallengeGetSolution')."',
          data: { challengeid: '".$_GET['id']."'}
        })
          .done(function( html ) {
             $('#showsolution').html( html );
          });
    });
 $('.pie_progress').asPieProgress({
            namespace: 'pie_progress'
        });

        // Example with grater loading time - loads longer
        $('.pie_progress--slow').asPieProgress({
            namespace: 'pie_progress',
            goal: 1000,
            min: 0,
            max: 1000,
            speed: 200,
            easing: 'linear'
        });

        // Example with grater loading time - loads longer
        $('.pie_progress--countdown').asPieProgress({
            namespace: 'pie_progress',
            easing: 'linear',
            first: 120,
            max: 120,
            goal: 0,
            speed: 1200, // 120 s * 1000 ms per s / 100
            numberCallback: function(n){
                var minutes = Math.floor(this.now/60);
                var seconds = this.now % 60;
                if(seconds < 10) {
                    seconds = '0' + seconds;
                }
                return minutes + ': ' + seconds;
            }
        });
        $('#button_start').on('click', function(){
            $('.pie_progress').asPieProgress('start');
        });
        $('#button_finish').on('click', function(){
            $('.pie_progress').asPieProgress('finish');
        });
        $('#button_go').on('click', function(){
            $('.pie_progress').asPieProgress('go',80);
        });
        $('#button_go_percentage').on('click', function(){
            $('.pie_progress').asPieProgress('go','".floor($percentage)."%');
        });
        $('#button_stop').on('click', function(){
            $('.pie_progress').asPieProgress('stop');
        });
        $('#button_reset').on('click', function(){
            $('.pie_progress').asPieProgress('reset');
        });
        $( '#button_go_percentage' ).trigger( 'click' );
");?>
<?php Yii::app()->clientScript->registerScript('sendmail', "
    $(document).on('click','.showSolutionItem',function(){
            var item= $(this).attr('data-reff');
            $('.sendmail').slideUp('fast');
            $('.suggestion'+item).slideDown('slow');
    });

    $(document).on('click','#mailSend',function(){
var questionReff=$(this).data('reff');
var questionId=$(this).data('id');
var comments= $('#comment'+questionId).val();
comments=comments.trim();
if(comments.length>0){
    $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {
$('#mailSendComplete'+questionId).slideDown();
setTimeout(function(){
$('#mailSendComplete'+questionId).hide();
$('.sendmail').slideUp();
 $('.comment'+questionId).val('');
}, 6000);
 })
}
   else{
    $('.comment'+questionId).val('');
$('.comment'+questionId).css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
  $('.comment'+questionId).attr('placeholder', 'Please enter your comments before clicking send');
setTimeout(function() {
                    $('.comment'+questionId).css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
               $('.comment'+questionId).attr('placeholder', 'Enter your comments here');
                }, 5000);
                return false;

                return false;

}

    });
");?>


<?php Yii::app()->clientScript->registerScript('sendmailtest', "
    $(document).on('click','.showSolutionItemtest',function(){
            var item= $(this).attr('data-reff');
            $('.sendmailtest').slideUp('fast');
            $('.suggestiontest'+item).slideDown('slow')


    });

    $(document).on('click','#mailSendtest',function(){
    var questionId=$(this).data('id');
var comments= $('#comments'+questionId).val();
comments=comments.trim();
if(comments.length>0){

var count=0;

            var questionReff=$(this).data('reff');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {
$('#mailSendCompletetest'+questionId).slideDown();
setTimeout(function(){
$('#mailSendCompletetest'+questionId).hide();
$('.sendmailtest').slideUp();
var comments= $('.comments'+questionId).val('');
}, 6000);
 })
}
else{
    $('#comments'+questionId).val('')
$('.comments'+questionId).css({'background-color':'#f2dede', 'color': '#a94442','border-color': '#ebccd1','border': '1px solid'});
  $('.comments'+questionId).attr('placeholder', 'Please enter your comments before clicking send');
setTimeout(function() {
                    $('.comments'+questionId).css({'background-color':'#fff', 'color': '#555','border-color': '#ccc','border': '1px solid #ccc'});
               $('.comments'+questionId).attr('placeholder', 'Enter your comments here');
                }, 5000);
                return false;

}


    });
");
?>
<script type="text/javascript">
    $(document).on('click','.hover',function(){
        $('.popu-cart').html(''); 
        var id=$(this).attr('data-id');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/GetChallengeBuddies";?>',
                data: {challenge:id},
                success: function(data){
                    if(data!='no')
                    {
                        $('.popu-cart').html(data);                        
                    }
                }
            });
    });
$(document).on('click','.hover',function(){
var id=$(this).attr('data-id');
var tooltip=$(this).attr('data-tooltip');
$('.tooltiptable').hide();
$('#tooltip'+id).fadeIn();
});
$(document).on('click','#close',function(){
$('.tooltiptable').hide();

});
    </script>