
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="contentarea">
        <h1 class="h125"> </h1>
        <div class="bs-example">
            <div class="panel panel-default">


                <div class="row">
                    <div class="col-md-4 col-lg-3 col-xs-9 col-lg-offset-0 col-md-offset-0 col-xs-offset-1">

                    </div>

                    <div class="col-md-12 col-lg-12 col-xs-12 text12525">
                        <h1 style="font-weight: 700;">
                            You are about to begin your challenge.
                        </h1>
                    </div>

                    <div class="col-md-12" style="text-align: center;">
                        <h5 style="margin: 20px;"> It is a computer adaptive test, so the questions will increase or decrease in difficulty based on how you answer them. </h5>
                        <h5 style="margin: 20px;">Please keep your pen & paper ready. Work it out on paper & mark the answers online. </h5>

                    </div>
                    <div class="col-md-4 col-md-offset-5"  style="margin-bottom: 20px;">
                            <a href="<?php echo Yii::app()->createUrl('student/ChallengeTest',array('id'=>$test->TM_CL_Id));?>">
                                <button class="btn btn-warning" name="startTest" >Proceed</button>
                            </a>

                    </div>
                </div>


            </div>
        </div>
    </div>
</div>