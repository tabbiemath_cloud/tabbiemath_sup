<?php //print_r($notstartedstudents);exit;?>
      <div class="row">
                <div class="col-md-12">
                    
                    <h1 class="page-header2 pull-left">Mark - <?php echo $homework->TM_SCH_Name;?></h1>
                    <?php if(Yii::app()->user->isTeacher()):?>
                    <a href="<?php echo Yii::app()->createUrl('teachers/completeallmarking',array('id'=>$homework->TM_SCH_Id));?>" class="btn btn-warning pull-right" id="completeallmarking" style="margin-top: 20px; margin-bottom: 10px;">Completed All Marking</a>
                    <?php endif;?>
                    <a href="<?php echo Yii::app()->createUrl('home');?>" class="btn btn-warning pull-right" style="margin-top: 20px; margin-bottom: 10px;margin-right:10px;">Exit</a>
                </div>              
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
					<div class="row">
					
					</div>
				<div class="panel with-nav-tabs panel-default">
             
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade" id="tab1default">Default 1</div>
                        <div class="tab-pane fade active in" id="tab2default">
							<div class="row" style="display:none">
								<div class="col-md-12">
								<button type="button" class="btn btn-warning btn-md pull-right">Completed All Marking</button>	
									<form class="centermy">
										<label class="radio-inline" style="margin-left: 10px;">
										  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked> All
										</label>
										<label class="radio-inline">
										  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> View Incorrect 
										</label>
										<label class="radio-inline">
										  <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> View to be Marked
										</label>
									</form>
																	
								</div>
							</div>
<?php if(count($startedstudents)>0):?>                            
							<div class="row">
                                <div class="col-md-4">
                                    <h4>Attempted</h4>
                                </div>
                                <div class="col-md-8">
                                    <div class="alert alert-warning fade in" style="display: none;" id="markinfo">
                                    </div>                                    
                                </div>
                            </div>
							<ul class="list-group">	
<?php 
        foreach($startedstudents AS $started):
            if($started->TM_STHW_Status=='4'):
            $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$started->TM_STHW_Student_Id))            
            ?>							 
					<li class="list-group-item">
                        <div class="row">
                            <div class="col-md-2 col-xs-6">
                            	<div class="doc" id="doc<?php echo $started->TM_STHW_Student_Id;?>">
                            		<div class="active item">
                            			<div class="carousel-info">
                            				<div class="pull-left">
                            					<span class="doc-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>
                            					<div class="namebottom">
                            						<ul>
                            							<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Solution">
                            								<a title="" class="drop viewsolution" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-lightbulb-o fa-2x"></i>
                            								</a>
                            							</li>
                            							<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Answer">
                            								<a title="" class="drop2 viewanswer" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-eye fa-2x"></i>
                            								</a>
                            							</li>
                                                        <?php if($this->CheckAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id)):
                                                            $hwanswers=$this->GetAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id);
                                                            foreach($hwanswers AS $hwanswer):
                                                        ?>
                                                                <li data-toggle="tooltip" data-placement="bottom" data-original-title="Download">
                                                                    <?php //$answersheet=Studenthomeworkanswers::model()->find(array('condition'=>"TM_STHWAR_HW_Id='".$started->TM_STHW_HomeWork_Id."' AND TM_STHWAR_Student_Id='".$started->TM_STHW_Student_Id."' "));?>
                                                                    <a href="<?php echo Yii::app()->request->baseUrl."/homework/".$hwanswer['TM_STHWAR_Filename'] ?>" title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
                                                                        <i class="fa fa-download fa-2x"></i>
                                                                    </a>
                                                                </li>
                                                        <?php 
                                                            endforeach;
                                                        endif;?>                                                       
                                                        <!--code by amal-->
														<?php 
														$criteria=new CDbCriteria;
														$criteria->condition='TM_HQ_Homework_Id='.$started->TM_STHW_HomeWork_Id.' AND TM_HQ_Type IN (6,7)';
														$totalpaper=HomeworkQuestions::model()->count($criteria);
														if($totalpaper>0):														
														?>
                                                        <!--<li data-toggle="tooltip" data-placement="bottom" data-original-title="Download">
                                                            <?php /*$answersheet=Studenthomeworkanswers::model()->find(array('condition'=>"TM_STHWAR_HW_Id='".$started->TM_STHW_HomeWork_Id."' AND TM_STHWAR_Student_Id='".$started->TM_STHW_Student_Id."' "));*/?>
                                                            <a href="<?php /*echo Yii::app()->request->baseUrl."/homework/".$answersheet['TM_STHWAR_Filename'] */?>" title="" target="_blank" class="viewanswersheet" data-item="<?php /*echo $started->TM_STHW_HomeWork_Id;*/?>" data-student="<?php /*echo $started->TM_STHW_Student_Id;*/?>">
                                                                <i class="fa fa-download fa-2x"></i>
                                                            </a>
                                                        </li>-->
														<?php endif;?>
                                                        <!--ends-->
                            							<li>
                            								<a title="" class="saveline" data-toggle="tooltip" data-placement="bottom" data-original-title="Save that line" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-floppy-o fa-2x"></i>
                            								</a>
                            							</li>
                            							<li>
                                                            <a title="" class="markcomplete" data-toggle="tooltip" data-placement="bottom" data-original-title="Mark it complete" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-check fa-2x"></i>
                            								</a>                                                                                    								
                            							</li>
                            						</ul>
                            					</div>
                            				</div>
                            			</div>
                            		</div>
                            	</div>
                                <span class="alert alert-success alert" id="alert<?php echo $started->TM_STHW_Student_Id;?>" style="display: none;"> Marked Complete</span>
                            </div>
                                <!--<div class="col-md-1 col-xs-6">
                                    <div class="btmy">                                                                        
                                    <div id="pbar" class="progress-pie-chart gt-50" data-percent="0">
                                    <div class="ppc-progress">
                                    <div class="ppc-progress-fill" style="transform: rotate(316.8deg);"></div>
                                    </div>
                                    <div class="ppc-percents">
                                    <div class="pcc-percents-wrapper">
                                    <span>88%</span>
                                    </div>
                                    </div>
                                    </div>                                    
                                    <progress style="display: none" id="progress_bar3" value="100" max="100"></progress>                                            							                                            								
                                    </div>
                                </div>-->                            
                            <div class="col-xs-12 col-md-9 col-lg-9">							
                            	<div class="table-responsive">
                                    <form id="markingform<?php echo $started->TM_STHW_Student_Id;?>" method="post">
                                        <input type="hidden" name="student" id="student<?php echo $started->TM_STHW_Student_Id;?>" value="<?php echo $started->TM_STHW_Student_Id;?>"/>                                        
                                        <input type="hidden" name="homework" id="homework<?php echo $started->TM_STHW_Student_Id;?>" value="<?php echo $started->TM_STHW_HomeWork_Id;?>"/>
                                		<table class="table tdnone">								
                                			<tbody>
                                				<tr>
                                                    <?php echo $this->GetQuestionColumns($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id); ?>
                                				</tr>
                                			</tbody>
                                		</table>
                                    </form>
                            	</div>
                            </div>                                                     							                                  					
                        </div>
						<div class="row">
								<div class="my_wrapper drop_row">
									<ul class="lst_stl_my solution<?php echo $started->TM_STHW_Student_Id;?>" style="height: 650px;">

									</ul>               
								</div>
						</div>
						<div class="row">
								<div class="my_wrapper drop_row2">
									<ul class="lst_stl_my answer<?php echo $started->TM_STHW_Student_Id;?>">

										</ul>	            
								</div>
						</div>
						
                    </li>
<?php
            elseif($started->TM_STHW_Status=='5'):            
                    $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$started->TM_STHW_Student_Id));  
?>
        <li class="list-group-item">
                                <div class="row">
        							<div class="col-md-2 col-xs-6">
        								<div class="doc">
        									<div class="active item">
        									  
        									  <div class="carousel-info">
        										<div class="pull-left">
        										  <span class="doc-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>
        											<div class="namebottom">
        												<ul>
        													<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Solution"><a title="" class="drop viewsolution" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>"><i class="fa fa-lightbulb-o fa-2x"></i></a></li>
        													<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Answer"><a title="" class="drop2 viewanswer" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>"><i class="fa fa-eye fa-2x"></i></a></li>
                                                            <li>
                                                            <a title="" class="unmarkcomplete" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit Marking" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-check fa-2x"></i>
                            								</a>                                                                                    								
                            							</li>        													        													
        												</ul>
        											</div>
        										</div>
        									  </div>
        									</div>
        								</div>
        							</div>
                                   
        						
        						
                               <!--<div class="col-md-1 col-xs-6">
                                    <div class="btmy">                                                                        
                                    <div id="pbar" class="progress-pie-chart gt-50" data-percent="0">
                                    <div class="ppc-progress">
                                    <div class="ppc-progress-fill" style="transform: rotate(316.8deg);"></div>
                                    </div>
                                    <div class="ppc-percents">
                                    <div class="pcc-percents-wrapper">
                                    <span>88%</span>
                                    </div>
                                    </div>
                                    </div>                                    
                                    <progress style="display: none" id="progress_bar3" value="100" max="100"></progress>                                            							                                            								
                                    </div>
                                </div> -->       						        						        						
        						<div class="col-xs-12 col-md-9 col-lg-9">							
        							<div class="table-responsive">
        								<table class="table tdnone">								
        									<tbody>
        										<tr>
                                                    <?php echo $this->GetQuestionColumnsCompleted($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id); ?>
        											
        										</tr>
        									</tbody>
        								</table>
        							</div>        												        						        							          							          							          							    
        						</div>
        						
        						
        						
        					
        						<!--<div class="pull-right btmy">
        							<button type="button" class="btn btn-warning btn-md">Save</button>
        						</div>-->
        						<div class="pull-right btmy" style="display:none">
        						
        								
        								<div id="pbar" class="progress-pie-chart" data-percent="0">
        								<div class="ppc-progress">
        								<div class="ppc-progress-fill"></div>
        								</div>
        								<div class="ppc-percents">
        								<div class="pcc-percents-wrapper">
        								<span>%</span>
        								</div>
        								</div>
        								</div>
        
        								<progress style="display: none" id="progress_bar" value="0" max="88"></progress>
        							
        								
        						</div>
                                </div>
        						
							<div class="row">
									<div class="my_wrapper drop_row">
										<ul class="lst_stl_my solution<?php echo $started->TM_STHW_Student_Id;?>" style="height: 650px;">

										</ul>               
									</div>
							</div>
							<div class="row">
									<div class="my_wrapper drop_row2">
										<ul class="lst_stl_my answer<?php echo $started->TM_STHW_Student_Id;?>">

											</ul>	            
									</div>
							</div>
                            </li>
<?php                    
            endif;
        endforeach;        
?>

                  
                </ul>
<?php endif;?>				
				
					<?php if(count($notstartedstudents)>0):?>
                       <h4 class="notatt">NOT ATTEMPTED <span class="badge bdg1 badge-warning"><?php echo count($notstartedstudents);?></span></h4>
					   <div class="row">
                        <?php foreach($notstartedstudents AS $notstarted):
                                $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$notstarted->TM_STHW_Student_Id))
                        ?>
        					<div class="col-md-4 col-sm-6">
        						<div class="not_attmpt">
        							<div class="active item">							  
        							  <div class="carousel-info">        								
        								<div class="pull-left">
        								  <span class="not_attmpt-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>
        								  <button type="button" id="sendreminder<?php echo $notstarted->TM_STHW_Student_Id;?>" data-item="<?php echo $notstarted->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $notstarted->TM_STHW_Student_Id;?>" class="btn btn-default btn-xs sendreminder"><i class="fa fa-bell"></i> Send Reminder</button>
        								</div>
        							  </div>
        							</div>
        						</div>
        					</div>
                        <?php endforeach;?>                       
                       </div>                        
                    <?php endif;?>																										
						</div>
                       
                    </div>
                </div>
            </div>

<script>
$(function(){
    $('.viewsolution').click(function(){
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('teachers/solution');?>',
            data: {student:student,homework:homework}
        })
            .done  (function(data, textStatus, jqXHR){
            $('.solution'+student).html(data);

        })
    });
    $('.viewanswer').click(function(){
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('teachers/answer');?>',
            data: {student:student,homework:homework}
        })
            .done  (function(data, textStatus, jqXHR){
            $('.answer'+student).html(data);

        })
    });
    $('.saveline').click(function(){
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        var error=$('.error'+student).length;
        $('.setmark'+student).each(function(){
            var stud=$(this).attr('data-student');
            var questionid=$(this).attr('data-id');
            if($('#error'+stud+questionid).val()==0)
            {
                error--;
            }
            else
            {
                error++;
            }
        });
        $('#completeall').val(error);

        if(error==0)
        {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('teachers/savemarks');?>',
                data: $('#markingform'+student).serialize()
            })
                .done  (function(data, textStatus, jqXHR){
                $('#doc'+student).after('<span class="alert alert-success alert'+student+'"> Line Saved</span>');
                setTimeout(function(){
                    $('.alert'+student).remove();
                }, 2000);
            })
        }
        else
        {
            $('#markinfo').show();
            $('#markinfo').html("Given marks cannot be less than 0 or greater than assigned marks.");
            setTimeout(function(){
                $('#markinfo').hide();
            }, 5000);
        }

    });

    $('.markcomplete').click(function(){
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        var item=$(this);
        var error=$('.error'+student).length;
        $('.setmark'+student).each(function(){
            var stud=$(this).attr('data-student');
            var questionid=$(this).attr('data-id');
            if($('#error'+stud+questionid).val()==0)
            {
                error--;
            }
            else
            {
                error++;
            }
        });
        if(error==0)
        {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('teachers/markcomplete');?>',
                data: $('#markingform'+student).serialize()
            })
                .done  (function(data, textStatus, jqXHR){
                $('.setmark'+student).attr('readonly',true);
                $('#doc'+student).after('<span class="alert alert-success alert'+student+'"> Marked Complete</span>');
                setTimeout(function(){
                    $('.alert'+student).remove();
                    item.removeClass('markcomplete');
                    item.addClass('unmarkcomplete');
                }, 2000);
            });
        }
        else
        {
            $('#markinfo').show();
            $('#markinfo').html("Given marks cannot be less than 0 or greater than assigned marks.");
            setTimeout(function(){
                $('#markinfo').hide();
            }, 5000);
        }

    });
    $(document).on('click', '.unmarkcomplete', function(){
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');

        $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('teachers/editmarkcomplete');?>',
          data: {student:student,homework:homework}
        })
        .done  (function(data, textStatus, jqXHR){
            location.reload();
         });
    });       
    $('.sendreminder').click(function(){
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        var elementitem=$(this);  
        $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('teachers/sendreminder');?>',
          data: {student:student,homework:homework}    
        })
        .done  (function(data, textStatus, jqXHR){
            elementitem.html('<i class="fa fa-bell"></i> Reminder Sent');
            setTimeout(function(){
                        elementitem.fadeOut() 
            }, 2000);              
        
         });        
    }); 
   //code by amal
    $(document).on( "change",".mark", function(){
        var questid=$(this).attr('data-id');
        var student=$(this).attr('data-student');
        var totalmark=$('#totmarks'+questid).val();
        var marks=$(this).val();
        if(parseFloat(marks) > totalmark || parseFloat(marks) < 0)
        {
            $('#markinfo').show();
            $('#markinfo').html("Given marks cannot be less than 0 or greater than assigned marks.");
            setTimeout(function(){
                $('#markinfo').hide();
            }, 5000);
            $('#error'+student+questid).val('1');
        }
        else
        {
            $('#error'+student+questid).val('0');
        }
    });
    //ends
})
</script>				
				
				
				
				
				
                   <!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Action</a>
                                        </li>
                                        <li><a href="#">Another action</a>
                                        </li>
                                        <li><a href="#">Something else here</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>-->	
                        
			                