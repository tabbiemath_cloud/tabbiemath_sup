
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">

        <form action="" method="post" id="challengeform">
            <div class="bs-example">
                <h1 class="h125" >Start a challenge</h1>


                <div class="bs-example">
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <!-- Table -->
                        <div class="table-responsive">											
                            <table class="rwd2-tables">
                                <tbody>	
                                <tr>
                                    <td><label class="filter-col" style="margin-right: 25px;" for="pref-perpage">Set Challenge For </label></td>
                                    <td>
                                        <input type="radio" name="Challenge[myself]" id="challengefor" value="1" checked="">
                                        Me

                                    </td>
                                    <td class="text-right hidden-xs hidden-sm">
                                        <?php                                         
                                        if($this->GetBuddies(Yii::app()->user->id)!=''):?>
                                            <a href="javascript:void(0)" target="_blank" class="btn btn-info choosebuddies" data-backdrop="static" data-toggle="modal" data-target="#invitModel"  style="width: 180px;" ><span class="fa fa-users" aria-hidden="true" style="margin-right: 9px;"></span>Choose Buddies</a>

                                        <?php endif;?>										
                                    </td>
									<td class="text-right visible-xs visible-sm">
                                        <?php                                         
                                        if($this->GetBuddies(Yii::app()->user->id)!=''):?>
															<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose Buddies">
															<a href="javascript:void(0)" class="choosebuddies btn btn-info"  data-backdrop="static" data-id="60" data-toggle="modal" data-target="#invitModel" style="width: 30px;">
															<span class="fa fa-users" aria-hidden="true"></span></a></span>

                                        <?php endif;?>                                                            

									</td>
                                    <td class="text-right" style="display: table-cell;">
                                        <button name="Challenge[SetChalange]" type="submit"  id="SetChalange" class="btn btn-warning" style="width: 180px;">Set Challenge</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="filter-col" style="margin-right: 25px;" for="pref-perpage">Number of questions</label>


                                    </td>
                                    <td class="text-right">
                                        <select id="pref-perpage" name="Challenge[questions]" class="form-control" style="width: 180px;">
                                            <option selected="selected" value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="25">25</option>
                                        </select></td>
                                    <td colspan="2"><?php                                         
                                        if($this->GetBuddies(Yii::app()->user->id)!=''):?><b>Chosen Buddies : </b>  <span id="buddyindication">No Buddies Selected</span>
                                            <?php else:?>
                                            <div >Want to invite buddies to the challenge? Add them first using the <span class="fa fa-users size2 brd_r"></span> icon above.
                                            </div>
                                        <?php endif;?></td>

                                </tr>
                                <tr>
                                <td colspan="4"><div class="alert2 alert-danger" id="challengeinfo" role="alert" style="display: none;"></div></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default clearfix">
                    <div class="panel-body padnone">
                        <div class="panel-heading">
                            <b>Chosen Topics : </b>  <span id="topicindication">No Topics Selected</span>


                        </div>



                        <div>
                            <?php foreach ($chapters AS $key=>$chapter):
                            if($this->GetChapterTotal($chapter->TM_TP_Id,'3')>10):
                                ?>
							<div class="col-xs-12 toggle-header">
									<div class="col-xs-10 col-md-4">
										<span data-toggle="tooltip" data-placement="right" title="" data-original-title="Select topics within the chapter that you want to revise on">
										<button type="button" class="btn btn-default btn-circle collapsed" data-toggle="collapse" data-target="#feature-<?php echo $key;?>" aria-expanded="true">
											<i class="fa fa-chevron-down"></i>
										</button>
										<a href="#" data-toggle="collapse" data-target="#feature-<?php echo $key;?>"> <?php echo $chapter->TM_TP_Name;?></a></span>
									</div>
									<div class="col-md-1 pull-right aline_r">
										<input type="checkbox" id="chapter<?php echo $key;?>" data-id="<?php echo $key;?>" class="chapters" name="chapters[]" value="<?php echo $chapter->TM_TP_Id;?>">
									</div>
							</div>
							<div id="feature-<?php echo $key;?>" class="collapse" aria-expanded="true" style="height: 0px;">
									<div class="row">
											<div class="col-md-12">
                                                <button type="button" class="btn btn-xs " style="margin-left: 15px;" id="uncheckall" href="javascript:void(0)">
                                                    Clear all
                                                </button>

                                                <button type="button" class="btn btn-xs " style="margin-left: 15px;" id="checkall" href="javascript:void(0)">
                                                    Select all
                                                </button>
											</div>
										<?php
										foreach(array_chunk($chapter->topic, 4) as $topics ):
											?>											
											<div class="col-xs-12 col-md-4">
												<ul class="listnew">
													<?php foreach($topics as $topic): ?>
														<li><input type="checkbox" name="<?php echo 'topic'.$chapter->TM_TP_Id.'[]';?>" checked="checked" value="<?php echo $topic->TM_SN_Id;?>" class="topiccheck-<?php echo $key;?>" data-id="<?php echo $key;?>"> <?php echo $topic->TM_SN_Name; ?></li>
													<?php endforeach;?>													
												</ul>
											</div>
										<?php
										endforeach;
										?>											
									</div>
							</div>							
                            
                            <?php
Yii::app()->clientScript->registerScript('challengeclear'.$key, "
$('#feature-".$key." #checkall').click(function () {


            $('#feature-".$key." input[type=checkbox]').each(function () {
                $(this).prop('checked', true);
            });
            });
$('#feature-".$key." #uncheckall').click(function () {


            $('#feature-".$key." input[type=checkbox]').each(function () {
                $(this).prop('checked', false);
            });
            $('#chapter".$key." ').attr('checked', false);
            var count=0;
            var topicids='';
            $('.chapters').each(function(index, element){

                if(element.checked){
                    if(count=='0')
                    {
                        topicids=$(this).val();

                    }
                    else
                    {
                        topicids=topicids+','+$(this).val();
                    }
                    count++;
                }
                if(topicids!='')
            {
                $('#topicindication').text('')
                $.ajax({
                    type: 'POST',
                    url: '".Yii::app()->createUrl('student/getTopics')."',
                data: { topicids: topicids}
            })
            .done(function( data ) {
                $('#topicindication').text(data);
            });
            }
            else
            {
                $('#topicindication').text('No Topics Selected');
            }

            });


      $('.topiccheck-".$key."').click(function(){
            var chapterid=$(this).data('id');
            var atLeastOneIsChecked = false;
          
            $('#feature-".$key." input[type=checkbox]').each(function ()
            {
                if ($(this).is(':checked'))
                {
                    atLeastOneIsChecked = true;

                }
            });
            if( !atLeastOneIsChecked ){
                var value=0;
                $('#chapter".$key."').prop('checked', false);
                $('#challengeinfo').show();
                $('#challengeinfo').text('Please select at least one topic.');
                setTimeout(function(){
                    $('#challengeinfo').hide();
                },5000);
            }

        });
    });
    ");
                            endif;
                            endforeach;?>

                        </div>


                        <div>



                        </div>

                    </div>

                </div>


            </div>
            <div class="modal fade " id="invitModel" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Choose Buddies</h4>
                        </div>
                        <div class="modal-body" >
                            <input type="text" id="usersuggest" name="invitebuddies[]">
                        </div>
                        <div class="modal-footer">
                            <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                            <span class="alert alert-warning pull-left nobuddies" style="display: none;"> </span>
                            <button type="button" class="btn btn-default" id="invitechallange">Invite</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </form>
    </div>
</div>

<script type="text/javascript">

    $(function(){

        $('#usersuggest').magicSuggest({
            maxSelection: 10,
            allowFreeEntries: false,
            data: [<?php echo $this->GetBuddies(Yii::app()->user->id);?>]
        });
        var names=$('#usersuggest').magicSuggest({
            maxSelection: 10,
            allowFreeEntries: false,
            data: [<?php echo $this->GetBuddies(Yii::app()->user->id);?>]
        });
        $('.choosebuddies').click(function(){                    
             $('.nobuddies').hide();
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/site/getPlanBuddies";?>',
                data: {id:'challengeid'},
                success: function(data){
                    if(data=='0')
                    {
                        $('.nobuddies').html('You have no buddies for the selected plan').show();
                    }
                }
            });
        })
        $('#invitechallange').click(function() {
            $('.inviteinfo').hide();
            var nameids=names.getValue();
            if((nameids.length)!=0){
                $('#buddyindication').text('')
                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->createUrl('student/getChallengernames');?>',
                    data: {nameids: nameids}
                })
                .done(function (data) {
                    $('#buddyindication').text(data);
                });
                //$('#inviteclose').trigger('click');
                $('#invitModel').modal('toggle')
            }
            else{
                
                $('.inviteinfo').text('Please select users.');
                $('.inviteinfo').show();
                $('#buddyindication').text('No Buddies Selected');
                

            }

        });
        $('#inviteclose').click(function(){
            var nameids=names.getValue();
            names.clear();
            if((nameids.length)!=0){
              $('#buddyindication').text('No Buddies Selected');  
            }
        });
        $('.close').click(function(){
            var nameids=names.getValue();
            names.clear();
            if((nameids.length)!=0){
              $('#buddyindication').text('No Buddies Selected');  
            }
        });                
        $('.chapters').click(function(){
            var chapterid=$(this).data('id');

            var atLeastOneIsChecked = false;
            $('#feature-'+chapterid+' input[type=checkbox]').each(function ()
            {
               if ($(this).is(':checked'))
                {
                    atLeastOneIsChecked = true;

                }
            });
            if( !atLeastOneIsChecked ){
                var value=0;
                $('#chapter'+chapterid).prop('checked', false);
                $('#challengeinfo').show();
                $('#challengeinfo').text('Please select at least one topic.');
                setTimeout(function(){
                    $('#challengeinfo').hide();
                },5000);
            }else{
                var count=0;
                var topicids='';
                $('.chapters').each(function(index, element){
                    if(element.checked){
                        if(count=='0')
                        {
                            topicids=$(this).val();
                        }
                        else
                        {
                            topicids=topicids+','+$(this).val();
                        }
                        count++;
                    }

                });
                if(topicids!='')
                {
                    $('#topicindication').text('')
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('student/getTopics');?>',
                        data: { topicids: topicids}
                    })
                        .done(function( data ) {
                            $('#topicindication').text(data);
                        });
                }
                else
                {
                    $('#topicindication').text('No Topics Selected');
                }
            }

        });
        $('#SetChalange').click(function(){

            var length=$('.chapters:checked').length;
            if(length==0)
            {
                $('#challengeinfo').show();
                $('#challengeinfo').text('Please select chapters.');
                setTimeout(function(){
                    $('#challengeinfo').hide();
                },5000);
                return false;
            }
            else
            {
                return true;
            }


        });


    });
</script>