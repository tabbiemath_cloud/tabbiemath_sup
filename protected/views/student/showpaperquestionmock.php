<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" xmlns="http://www.w3.org/1999/html"
     xmlns="http://www.w3.org/1999/html">
    <div class="contentarea">
        <h1 class="h125">You have the following paper based questions to complete.</h1>
        <div class="bs-example">



            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div style="text-align: right;">
                        <span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('student/getpdfmock',array('id'=>$id));?>" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('student/getpdfmock',array('id'=>$id));?>" target="_blank"><i class="fa fa-print fa-lg"></i></a>

                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table table-hover table-mc-light-blue wirisfont">
                        <thead>

                        </thead>
                        <tbody class="font3 wirisfont">
                        <?php foreach($paperquestions AS $key=>$paperquestion):

                            $question=Questions::model()->findByPk($paperquestion->TM_STMKQT_Question_Id)
                            ?>
                            <?php if($question->TM_QN_Type_Id!='7'):?>

<!-- -->
                            <tr>
                                <td>
                                    <div class="col-md-4" >
                                        <span style="padding-left: 12px">Question <?php echo $key+1; ?></span>
                                        &nbsp; &nbsp; &nbsp;Mark:<?php echo $question->TM_QN_Totalmarks; ?>
                                    </div>
                            
                            
                                    <div class="col-md-3 pull-right">
                                        Ref:<?php echo $question->TM_QN_QuestionReff ?>
                            
                                    </div>
                            
                            
                                    <div class="col-md-12">
                                        <?php if($question->TM_QN_Image!=''):?>
                            
                                                <div class="col-md-7">
                                                <?php echo $question->TM_QN_Question;?>
                                                </div>
                                                <div class="col-md-5">
                                                     <span class="pull-right">
                            
                                                   <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" alt="">
                            
                                            </span>
                                                    </div>
                            
                                        <?php else:?>
                            
                                                    <div class="col-md-12">
                                                        <?php echo $question->TM_QN_Question;?>
                            
                                                    </div>
                            
                                        <?php endif ?>
                                    </div>
                            
                            
                            
                            
                                </td>
                            </tr>

<!-- -->                            
                            
                        <?php elseif($question->TM_QN_Type_Id=='7'):
                             $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));?>
                        <?php $childquestionsmark = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                                $multimark=0;
                         foreach ($childquestionsmark AS $childquestion):
                             $multimark=$multimark+(int)$childquestion->TM_QN_Totalmarks;

                             endforeach;?>
                    <tr>
                        <td style="padding-left: 0px;">
                            <div class="col-md-4">
                                <span style="padding-left: 12px">Question <?php echo $key+1; ?></span>
                                &nbsp; &nbsp; &nbsp;Mark:<?php echo $multimark ?>
                            </div>


                            <div class="col-md-3 pull-right">
                                Ref:<?php echo $question->TM_QN_QuestionReff ?>

                            </div>
                            <div class="col-md-12">
                                <?php if($question->TM_QN_Image!=''):?>

                                            <div class="col-md-7">
                                                <?php echo $question->TM_QN_Question;?>
                                            </div>
                                            <div class="col-md-5">
                                                 <span class="pull-right">

                                               <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" alt="">

                                        </span>
                                            </div>

                                <?php else:?>

                                            <div class="col-md-12">
                                                <?php echo $question->TM_QN_Question;?>

                                            </div>

                                <?php endif ?>
                            </div>


                            <?php $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                            $count = 1?>
                            <?php foreach ($questions AS $childquestion): ?>


                                <div class="col-md-12">
                                <?php if($childquestion->TM_QN_Image!=''):?>

                                            <div class="col-md-12">
                                                Part:<?php echo $count ;?>
                                            </div>

                                            <div class="col-md-7">
                                                <?php echo $childquestion->TM_QN_Question;?>
                                            </div>
                                            <div class="col-md-5">
                                                 <span class="pull-right">

                                               <img class="img-responsive img_right" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childquestion->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" alt="">

                                        </span>
                                            </div>

                                <?php else:?>

                                            <div class="col-md-12">
                                                Part:<?php echo $count ;?>
                                            </div>
                                            <div class="col-md-12">
                                                <?php echo $childquestion->TM_QN_Question;?>

                                            </div>

                                <?php endif ?>
                                </div><?php $count++ ?>

                            <?php endforeach; ?>
                        </td>
                    </tr>


                        <?php endif; endforeach;?></tbody>
                    </table>
                </div>

                <div class="panel-footer">
                    <form action="" method="post">
                        <div class="media-body ptp25" style=" text-align: -webkit-left; float: left; width: 265px;">
                            <input type="checkbox" id="checkcomplete" name="checkcomplete" value="Check To Complete" style="float: left; margin-right: 10px;">I completed working out the paper part.<br>
                        </div>
                        <div style="text-align: right;position: static;">
                            <button name="coomplete" id="GoNext" type="submit" class="btn btn-warning sanfont cmplt" style="margin-right:20px;width: 155px;" disabled="">Complete</button>
                        </div>
                    </form>


                </div>

            </div>











            <?php Yii::app()->clientScript->registerScript('showbtn', "
$('input:checkbox').change(function () {
    if ($('input[type=checkbox]:checked').length > 0) {
        $('#GoNext').attr('disabled',false);
    } else {
        $('#GoNext').attr('disabled',true);
    }
});");?>


