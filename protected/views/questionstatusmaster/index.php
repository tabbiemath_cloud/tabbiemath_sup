<?php
/* @var $this QuestionstatusmasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Questionstatusmasters',
);

$this->menu=array(
	array('label'=>'Create Questionstatusmaster', 'url'=>array('create')),
	array('label'=>'Manage Questionstatusmaster', 'url'=>array('admin')),
);
?>

<h1>Questionstatusmasters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
