<?php
/* @var $this QuestionstatusmasterController */
/* @var $model Questionstatusmaster */

$this->breadcrumbs=array(
	'Status'=>array('admin'),
	$model->TM_QSM_Id,
);

$this->menu=array(
    array('label'=>'Manage Status', 'class'=>'nav-header'),
	array('label'=>'List Status', 'url'=>array('admin')),
	array('label'=>'Create Status', 'url'=>array('create')),
	array('label'=>'Update Status', 'url'=>array('update', 'id'=>$model->TM_QSM_Id)),
	array('label'=>'Delete Status', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_QSM_Id),'confirm'=>'Are you sure you want to delete this item?')),	
);
?>

<h3>View Status #<?php echo $model->TM_QSM_Name; ?></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_QSM_Id',
		'TM_QSM_Name',
	),
)); ?>
