<?php
/* @var $this QuestionstatusmasterController */
/* @var $model Questionstatusmaster */

$this->breadcrumbs=array(
	'Status'=>array('admin'),
	$model->TM_QSM_Id=>array('view','id'=>$model->TM_QSM_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Status', 'class'=>'nav-header'),
	array('label'=>'List Status', 'url'=>array('admin')),
	array('label'=>'Create Status', 'url'=>array('create')),
	array('label'=>'View Status', 'url'=>array('view', 'id'=>$model->TM_QSM_Id)),	
);
?>

<h3>Update Status <?php echo $model->TM_QSM_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>