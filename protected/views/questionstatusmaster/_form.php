<?php
/* @var $this CouponsController */
/* @var $model Coupons */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('questionstatusmaster/admin')."';
    });
");
?>

<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'questionstatusmaster-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_QSM_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_QSM_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_QSM_Name'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-3 pull-right">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="reset" value="Reset">Cancel</button>
                </div>
            </div>
        </div>        
</div>	

<?php $this->endWidget(); ?>

</div><!-- form -->