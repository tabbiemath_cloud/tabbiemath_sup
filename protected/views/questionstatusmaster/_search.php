<?php
/* @var $this QuestionstatusmasterController */
/* @var $model Questionstatusmaster */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_QSM_Id'); ?>
		<?php echo $form->textField($model,'TM_QSM_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QSM_Name'); ?>
		<?php echo $form->textField($model,'TM_QSM_Name',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->