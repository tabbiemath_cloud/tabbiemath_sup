<?php
/* @var $this QuestionstatusmasterController */
/* @var $data Questionstatusmaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QSM_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_QSM_Id), array('view', 'id'=>$data->TM_QSM_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QSM_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QSM_Name); ?>
	<br />


</div>