<?php
/* @var $this QuestionstatusmasterController */
/* @var $model Questionstatusmaster */

$this->breadcrumbs=array(
	'Status'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Status', 'class'=>'nav-header'),
	array('label'=>'List Status', 'url'=>array('admin')),	
);
?>

<h3>Create Status</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>