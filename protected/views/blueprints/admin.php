
<style>
@media screen and (max-width: 992px) {
 .side-nav
 {
 	display: none;
 }
 .manage-mobile{
 	display: block !important;
 }
}
</style>

<?php
/* @var $this BlueprintsController */
/* @var $model Blueprints */

$this->breadcrumbs=array(
	'Blueprints'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Manage Blueprints', 'class'=>'nav-header'),
	array('label'=>'List Blueprints', 'url'=>array('admin')),
	array('label'=>'Create Blueprints', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#blueprints-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div style="margin-top:15px;display: inline-block;">                
    <a href="<?php echo Yii::app()->createUrl('blueprints/admin') ?>" style="display:none;width:150px;float:left;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >List Blueprint</button></a>

    <a href="<?php echo Yii::app()->createUrl('blueprints/create') ?>" style="display:none;width:150px;float:right;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >Create Blueprint</button></a>
</div>

<h3>Manage Blueprints</h3>
<div class="row brd1" style="overflow-x: auto;">
	<div class="col-lg-12">
		<!-- <p>
		You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
		or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
		</p> -->

		<!-- <?php // echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
		<div class="search-form" style="display:none">
		<?php // $this->renderPartial('_search',array(
			//'model'=>$model,
		// )); ?>
		</div> -->
		<!-- search-formm -->
		<?php if(Yii::app()->user->isAdmin()){?>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'blueprints-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
		//'TM_BP_Id',
		'TM_BP_Name',
		'TM_BP_Description',
		array(
			'name'=>'TM_BP_Syllabus_Id',
			'value'=>'Syllabus::model()->findByPk($data->TM_BP_Syllabus_Id)->TM_SB_Name',
			'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),

		),
		// 'TM_BP_Syllabus_Id',
		array(
			'name'=>'TM_BP_Standard_Id',
			'value'=>'Standard::model()->findByPk($data->TM_BP_Standard_Id)->TM_SD_Name',
			'filter'=>CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),

		),
		array(
				'name' => 'TM_BP_Availability',
				'filter'=>false,
				'value' => 'Blueprints::GetAvailablity($data->TM_BP_Availability)',
		),

		array(
            'header'=>'Created By',
            'type'=>'raw',
            'value'=>'Schoolhomework::GetData(Blueprints::GetCreatedBy($data->TM_BP_CreatedBy),"")',
        ),
		array(
				'name'=>'TM_BP_Status',
				'value'=>'Blueprints::itemAlias("BlueprintStatus",$data->TM_BP_Status)',
				'filter'=>Blueprints::itemAlias("BlueprintStatus"),

		),
		//'TM_BP_Standard_Id',
		// 'TM_BP_Totalmarks',
		/*
		'TM_BP_Status',
		'TM_BP_CreatedOn',
		'TM_BP_CreatedBy',
		*/
		array(
                'class'=>'CButtonColumn',
                'template'=>'{view}{update}{delete}{duplicate}',
                'deleteButtonImageUrl'=>false,
                'updateButtonImageUrl'=>false,
                'viewButtonImageUrl'=>false,
                'buttons'=>array
                (                  
                    'view'=>array(
                        'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                        'options'=>array(
                            'title'=>'View'
                        )),
                    'update'=>array(
                        'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                        'options'=>array(
                            'title'=>'Update'
                        )),
                    'delete'=>array(
                        'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                        'options'=>array(
    
                            'title'=>'Delete',
                        ),
                    ),
	                'duplicate'=>array(
                        'label'=>'<span class="glyphicon glyphicon-duplicate"></span>',
                        'options'=>array(
    
                            'title'=>'Duplicate',
	                        'onclick' => 'return confirm("Are you sure want to duplicate this blueprint?");',
                        ),
	                   'url'=>'Yii::app()->createUrl("blueprints/duplicate",array("id"=>$data->TM_BP_Id))'
                    )
                ),
            ),
		),'itemsCssClass'=>"table table-bordered table-hover table-striped"
		)); ?>
		<?php } else if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()){?>

		<?php	
		$uid = Yii::app()->user->id;
		$user=User::model()->find(array('condition'=>'id="'.$uid.'"'));

		$userSchool = $user->school_id;
		/// $status = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id=19 AND TM_BPS_School_Id='.$userSchool))->TM_BPS_Status

 		?>

		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'blueprints-grid',
		'dataProvider'=>$model->schoolsearch($id),
		'filter'=>$model,
		'columns'=>array(
		//'TM_BP_Id',
		'TM_BP_Name',
		'TM_BP_Description',
		array(
			'name'=>'TM_BP_Syllabus_Id',
			'value'=>'Syllabus::model()->findByPk($data->TM_BP_Syllabus_Id)->TM_SB_Name',
			'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),

		),
		// 'TM_BP_Syllabus_Id',
		array(
			'name'=>'TM_BP_Standard_Id',
			'value'=>'Standard::model()->findByPk($data->TM_BP_Standard_Id)->TM_SD_Name',
			'filter'=>CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),

		),

		// array(
  //                   'name'=>'TM_BP_CreatedBy',
  //                   'value'=>'User::model()->findByPk($data->TM_BP_CreatedBy)->profile->firstname',
  //               ),
		array(
            'header'=>'Created By',
            'type'=>'raw',
            'value'=>'Schoolhomework::GetData(Blueprints::GetCreatedByForSchool($data->TM_BP_CreatedBy),"")',
        ),

		array(
			'name'=>'TM_BP_Status',
            'type'=>'raw',
            'value'=>'Blueprints::GetStatusUpdateLink($data->TM_BP_Id)',
			'filter'=>Blueprints::itemAlias("BlueprintActivate"),
        ),
		// array(
		// 	'name'=>'TM_BP_Status',
		// 	'value'=>'blueprints::model()->findByPk($data->TM_BP_Standard_Id)->TM_SD_Name',
		// 	'filter'=>CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),

		// ),
		//'TM_BP_Standard_Id',
		// 'TM_BP_Totalmarks',
		// BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='.$data->TM_BP_Id.' AND TM_BPS_School_Id='.$userSchool))->TM_BPS_Status
		// 'TM_BP_Status',
		
		 // array(
   //              'name'=>'TM_BP_Status',
   //              'value'=>BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id="'.$data->TM_BP_Id.'" AND TM_BPS_School_Id="'.$userSchool.'"'))->TM_BPS_Status,
   //          ),

		  // array(
    //             'name'=>'TM_BP_Status',
    //             'value'=>"Blueprints::itemAlias('BlueprintStatus',BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='19' AND TM_BPS_School_Id=3'))->TM_BPS_Status)",
    //         ),
		// 'TM_BP_CreatedOn',
		// 'TM_BP_CreatedBy',
		
		array(
                'class'=>'CButtonColumn',
                'template'=>'{view}{update}{delete}{duplicate}',
                'deleteButtonImageUrl'=>false,
                'updateButtonImageUrl'=>false,
                'viewButtonImageUrl'=>false,
                'buttons'=>array
                (                  
                    'view'=>array(
                        'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                        'options'=>array(
                            'title'=>'View'
                        )),
                    'update'=>array(
                        'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                        'options'=>array(
                            'title'=>'Update'
                        ),
	                   'visible'=>'$data->TM_BP_CreatedBy == Yii::app()->user->id',
                       ),
                    'delete'=>array(
                        'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                        'options'=>array(
    
                            'title'=>'Delete',
                        ),
	                   'visible'=>'$data->TM_BP_CreatedBy == Yii::app()->user->id',
                    ),
	                // 'active'=>array(
	                //     'label'=>'Active/Inactive',
	                //     'options'=>array(

	                //         'title'=>'Active/Inactive',
	                //     ),
	                //    'visible'=>'$data->TM_BP_CreatedBy != Yii::app()->user->id',
	                //    'url'=>'Yii::app()->createUrl("blueprints/changeStatus",array("id"=>$data->TM_BP_Id))'
                 //    ),
	                'duplicate'=>array(
                        'label'=>'<span class="glyphicon glyphicon-duplicate"></span>',
                        'options'=>array(
    
                            'title'=>'Duplicate',
	                        'onclick' => 'return confirm("Are you sure want to duplicate this blueprint?");',
                        ),
	                   'url'=>'Yii::app()->createUrl("blueprints/duplicate",array("id"=>$data->TM_BP_Id))'
                    )
                ), 
            ),
		),'itemsCssClass'=>"table table-bordered table-hover table-striped"
		)); ?>
		<?php } ?>
	</div>
</div>
