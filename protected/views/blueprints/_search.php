<?php
/* @var $this BlueprintsController */
/* @var $model Blueprints */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_Id'); ?>
		<?php echo $form->textField($model,'TM_BP_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_Name'); ?>
		<?php echo $form->textField($model,'TM_BP_Name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_Description'); ?>
		<?php echo $form->textField($model,'TM_BP_Description',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_Syllabus_Id'); ?>
		<?php echo $form->textField($model,'TM_BP_Syllabus_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_Standard_Id'); ?>
		<?php echo $form->textField($model,'TM_BP_Standard_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_Totalmarks'); ?>
		<?php echo $form->textField($model,'TM_BP_Totalmarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_Status'); ?>
		<?php echo $form->textField($model,'TM_BP_Status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_BP_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_BP_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_BP_CreatedBy'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->