
<style>
  
   
.btn-circle:focus, .btn-circle:hover {
    color: #FFF;
    background-color: grey;
    border-color: grey;
  }

  @media screen and (max-width: 992px) {
 .side-nav
 {
  display: none;
 }
 .manage-mobile{
  display: block !important;
 }
}
</style>
<?php
/* @var $this BlueprintsController */
/* @var $model Blueprints */

// $this->breadcrumbs=array(
//  'Blueprints'=>array('index'),
//  'Create',
// );


$this->menu=array(
    array('label'=>'Manage Blueprints', 'class'=>'nav-header'),
  array('label'=>'List Blueprints', 'url'=>array('admin')),
  // array('label'=>'Manage Blueprints', 'url'=>array('admin')),
);
?>

<div style="margin-top:15px;display: inline-block;">                
    <a href="<?php echo Yii::app()->createUrl('blueprints/admin') ?>" style="display:none;width:150px;float:left;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >List Blueprint</button></a>

    <a href="<?php echo Yii::app()->createUrl('blueprints/create') ?>" style="display:none;width:150px;float:right;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >Create Blueprint</button></a>
</div>

<h3>Create Blueprints</h3>

<?php // $this->renderPartial('_form', array('model'=>$model)); ?>

<div class="row brd1" style="background-color: #d7d7d7;">
  <form id ="blueprintForm">
    <div class="panel-body form-horizontal payment-form">
      <div class="part first-part">
      
        <div class="form-group">
          <label class="col-sm-3 control-label" for="Name">Name:</label>
          <div class="col-sm-9">
            <input type="text" name="name" class="form-control" id="name">
      <span id="error-name" style="color:red;"> </span>
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">Description:</label>
          <div class="col-sm-9">
            <input type="textarea" name="description" class="form-control" id="description">
      <span id="error-description" style="color:red;"> </span>
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">syllabus:</label>
          <div class="col-sm-9">
            <select class="form-control syllabus" name="syllabus" id="syllabus"><option value="">Please select a syllabus</option>
            <?php 
            $syllabuses = Syllabus::model()->findAll();
            foreach($syllabuses AS $syllabus)
            { ?>
              <option value="<?= $syllabus->TM_SB_Id?>"><?= $syllabus->TM_SB_Name ?></option>
            <?php }
            ?>
            </select>
           <span id="error-syllabus" style="color:red;"> </span>
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">standard:</label>
          <div class="col-sm-9">
            <select class="form-control standards" name="standard" id="standard"><option value="">Please select a standard</option></select>
      <span id="error-standard" style="color:red;"> </span>
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">status:</label>
          <div class="col-sm-9">
            <select class="form-control" name="status" id="status"><option value="">Please select a status</option>
              <option value=0>Active</option>
              <option value=1>Inactive</option>    
            </select>
      <span id="error-status" style="color:red;"> </span>
          </div>
        </div>
  <?php 
  if(Yii::app()->user->isAdmin()){ ?>


      <div class="form-group">
          <label class="col-sm-3 control-label" for="availability">Availability</label>
          <div class="col-sm-9">
              <select class="form-control" name="availability" id="blueprint_Availability">
                  <option value="">Select Availability</option>
                  <option value="0">Student</option>
                  <option value="1">School - All</option>
                  <option value="3">School - Specific</option>
                  <option value="4">Both - Specific Schools</option>
                  <option value="2">Both</option>
              </select>
          </div>
      </div>

      <div class="form-group" style="display:none;" id="school_list">
          <label for="description" class="col-sm-3 control-label">Schools:</label>
          <div class="col-sm-9">
              <?php echo CHtml::dropDownList('schools',$selected,CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name'),array('class'=>'form-control', 'multiple' => 'multiple')); ?>
              <span id="error-schools" style="color:red;"> </span>
          </div>
      </div>

  <?php } else if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()){
      
      $id = Yii::app()->user->id;
          $user=User::model()->find(array('condition'=>'id="'.$id.'"'));
      $school = School::model()->find(array('condition'=>'TM_SCL_Id="'.$user->school_id.'"'));
      ?>
        <input type="hidden" name="availability" value=3>
      <div class="form-group">
          <label for="description" class="col-sm-3 control-label">published schools:</label>
          <div class="col-sm-9">
              <?php echo CHtml::dropDownList('schools',$selected,CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name'),array('class'=>'form-control','disabled' => 'disabled','options'=>array($user->school_id=>array('selected'=>true)))); ?>
              <span id="error-schools" style="color:red;"> </span>
              <input type="hidden" name="school_id" value="<?php echo $user->school_id;?>">
          </div>
      </div>
  <?php } ?>
        <!-- <div class="checkbox">
          <label><input type="checkbox"> Remember me</label>
        </div> -->
        <div class="form-group">
          <div class="col-sm-12" id="preloaderrow" style="display: none">
            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader">
          </div>
        </div>
        <div class="form-group">
          <!--<div class="row">-->
            <div class="col-sm-3 pull-right">
              <button type="button" class="btn btn-warning btn-lg btn-block firstpart-submit">Continue</button>
            </div>
          <!--</div>-->
        </div>
      </div>
      <div class="part second-part" style="display:none;">
        <h3 style="text-align:center;margin-bottom: 30px;margin-top: 0;">Choose number of questions from each chapter</h3>
        <div class="row">
          <div class="col-sm-3 pull-left" style="">
              <button type="button" class="btn btn-warning btn-lg btn-block submitAll">Save</button>
            </div>
        </div>
      <div class="second-part-table" style="overflow-x: auto;"></div>
        <div class="form-group">
          <div class="col-sm-12" id="preloaderrow1" style="display: none">
            <img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader">
          </div>
        </div>
        <div class="form-group">
          <!--<div class="row">-->
            <div class="col-sm-3 pull-left">
              <button type="button" class="btn btn-warning btn-lg btn-block submitAll">Save</button>
            </div>
          <!--</div>-->
        </div>
      </div>
    </div>
  </form>
  </div>
  <input type="hidden" id="bluePrintId">

  <script type="text/javascript">
 $('body').on('keyup', '.allowedNumbers', function() {
    // alert('success');
  var totalNoOfQuest = 0;
  var totalMarks = 0;
    $(".allowedNumbers").each(function(){
      var val = parseFloat($(this).val());
      var mark = parseFloat($(this).attr('data-mark'));
      if(!isNaN(val)) 
      {
        totalNoOfQuest = totalNoOfQuest+val;
        totalMarks = totalMarks+(val*mark);
      }
    });
    console.log(totalNoOfQuest);
    // console.log(totalMarks);
    $('#totalNoOfQuest').html(totalNoOfQuest);
    $('#totalMark').html(totalMarks);
  });


$('body').on('change', '.allowedNumbers', function() {
    // alert('success');
  var totalNoOfQuest = 0;
  var totalMarks = 0;
    $(".allowedNumbers").each(function(){
      var val = parseFloat($(this).val());
      var mark = parseFloat($(this).attr('data-mark'));
      if(!isNaN(val)) 
      {
        totalNoOfQuest = totalNoOfQuest+val;
        totalMarks = totalMarks+(val*mark);
      }
    });
    console.log(totalNoOfQuest);
    // console.log(totalMarks);
    $('#totalNoOfQuest').html(totalNoOfQuest);
    $('#totalMark').html(totalMarks);
  });


$(document).ready(function()
{

   $(document).on('click', '.topic-toggable', function()
  {
    

    var target = $(this).attr('data-target');
    if(!$(target).is(":visible")){
      $('.topics-toggle-list').hide();
    }
    $(target).slideToggle();

  });
 // console.log('test');
   $(document).on('click', '.submitAll', function()
  {
    // console.log("starts now")
    // $('#preloaderrow1').show();
    $(this).attr('disabled','disabled');
    var name = $('#name').val();
    var description = $('#description').val();
    var syllabus = $('#syllabus').val();
    var standard = $('.standards').val();
    var status = $('#status').val();
    var availability = $('#blueprint_Availability').val();
    var schools = $('#schools').val();
    $.ajax({
      type: 'POST',
      url: '../blueprints/save',
      // async:false,
      beforeSend : function(){
            console.log("starts now")
            $('#preloaderrow1').show()
          },
      dataType: "json",
      // data: {name: name,description: description,syllabus: syllabus, standard: standard, status: status, schools: schools, availability: availability},
      data:$('#blueprintForm').serialize(),
      success: function(data){
          if(data.error=='No')
          {
          //  alert(data.bluePrintId);
             // $('#bluePrintId').val(data.bluePrintId);
         //window.location.replace('../blueprints/admin.html');
            // var bluePrintId = data.bluePrintId;
            // console.log('Yes Blue Print Exists');
        
          window.location.replace('../blueprints/admin.html');
        }
      }
    });
   
  }); 
    
 
  $(document).on('click', '.firstpart-submit', function()
  {
  var error=0;
    var standard = $('.standards').val();
  if($('#name').val()=='')
  {
    $("#name").next("span").html('Please enter the name').show('slow');
    error=1;
  }
  // if($('#description').val()==''){
  //     $("#description").next("span").html('Please enter the description').show('slow');
  //  error=1;
  // }
  if($('#syllabus').val()==''){
      $("#syllabus").next("span").html('Please select a syllabus').show('slow');
    error=1;
  }
  if($('#standard').val()==''){
      $("#standard").next("span").html('Please select a standard').show('slow');
    error=1;
  }
  if($('#status').val()==''){
      $("#status").next("span").html('Please select a status').show('slow');
    error=1;
  }
  
  if(error==0){
    $.ajax({
      type: 'POST',
      url: '../blueprints/getSecondPart',
          beforeSend : function(){
            $('#preloaderrow').show();
          },
      dataType: "json",
      data: {standard: standard},
      success: function(data){
        if(data.error=='No')
        {
          $('.second-part').show();
          $('.second-part-table').html(data.result);
          $('.first-part').hide();
        }
      }
    });   
  }
    
  });
  $(document).on('change', '.syllabus', function()
  { 
  var syllabus=$(this).val();
    $.ajax({
        type: 'POST',
        async:false,
        url: '../blueprints/getStandard',
        dataType: "json",
        data: {syllabus: syllabus},
        success: function(data){
            if(data.error=='No')
            {
                $('.standards').html(data.result);
            }
        }
    });
  });

    $(document).on('change', '#blueprint_Availability', function()
    {
        // alert($(this).val());
        if($(this).val() == 0)
            $('#school_list').hide();
        else{
            $('#school_list').show();
            $('#school_list option').prop('selected', true);
        }

    });

   $(document).on('click', '.checkall', function()
  { 
    var mark=$(this).data('id');
    var chapter=$(this).data('chapter');
    $('.inputtopic-'+mark+'-'+chapter).prop('checked', true);
  });
  $(document).on('click', '.uncheckall', function()
  { 
    var mark=$(this).data('id');
    var chapter=$(this).data('chapter');
    $('.inputtopic-'+mark+'-'+chapter).prop('checked', false);
  });
  
  
  $(document).on('change', '.form-control', function()
  { 
   if($('#name').val()!='')
  {
    $("#error-name").css("display", "none");
  }
   if($('#description').val()!=''){
      $("#error-description").css("display", "none");
  }
  if($('#syllabus').val()!=''){
      $("#error-syllabus").css("display", "none");
  }
  if($('#standard').val()!=''){
      $("#error-standard").css("display", "none");
  }
  if($('#status').val()!=''){
      $("#error-status").css("display", "none");
  }
  if($('#schools').val()!=null){
      $("#error-schools").css("display", "none");
  }
  });
  
});

</script>