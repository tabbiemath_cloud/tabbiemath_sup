<?php
/* @var $this BlueprintsController */
/* @var $data Blueprints */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_BP_Id), array('view', 'id'=>$data->TM_BP_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_Description')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_Description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_Syllabus_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_Syllabus_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_Standard_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_Standard_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_Totalmarks')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_Totalmarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_Status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_BP_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_BP_CreatedBy); ?>
	<br />

	*/ ?>

</div>