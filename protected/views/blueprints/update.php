<style type="text/css">
  
  @media screen and (max-width: 992px) {
 .side-nav
 {
  display: none;
 }
 .manage-mobile{
  display: block !important;
 }
}
</style>
<?php
/* @var $this BlueprintsController */
/* @var $model Blueprints */

$this->breadcrumbs=array(
  'Blueprints'=>array('index'),
  $model->TM_BP_Id=>array('view','id'=>$model->TM_BP_Id),
  'Update',
);

$this->menu=array(
    array('label'=>'Manage Blueprints', 'class'=>'nav-header'),
  array('label'=>'List Blueprints', 'url'=>array('admin')),
  array('label'=>'Create Blueprints', 'url'=>array('create')),
  array('label'=>'View Blueprints', 'url'=>array('view', 'id'=>$model->TM_BP_Id)),
  //array('label'=>'Manage Blueprints', 'url'=>array('admin')),
    array('label'=>'Delete Blueprints', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_BP_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div style="margin-top:15px;display: inline-block;">                
      <a href="<?php echo Yii::app()->createUrl('blueprints/admin') ?>" style="display:none;width:150px;float:left;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >List Blueprint</button></a>

      <a href="<?php echo Yii::app()->createUrl('blueprints/create') ?>" style="display:none;width:150px;float:right;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >Create Blueprint</button></a>

      
  </div>
  <div style="margin-top:15px;display: inline-block;">
    <a href="<?php echo Yii::app()->createUrl('blueprints/view',array('id'=>$model->TM_BP_Id))?>" style="display:none;width:150px;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >View Blueprint</button></a>
  </div>

<h1>Update <?php echo $model->TM_BP_Name; ?></h1>

<div class="row brd1" style="background-color: #d7d7d7;">
  <form id ="blueprintForm">
    <div class="panel-body form-horizontal payment-form">
      <div class="part first-part">
      
        <div class="form-group">
          <label class="col-sm-3 control-label" for="Name">Name:</label>
          <div class="col-sm-9">
            <input type="text" name="name" value="<?= $model->TM_BP_Name ?>" class="form-control" id="name">
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">Description:</label>
          <div class="col-sm-9">
            <input type="textarea" name="description" value="<?= $model->TM_BP_Description ?>" class="form-control" id="description">
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">syllabus:</label>
          <div class="col-sm-9">
            <select class="form-control syllabus" name="syllabus" id="syllabus"><option>Please select a syllabus</option>
            <?php 
            $syllabuses = Syllabus::model()->findAll();
      foreach($syllabuses AS $syllabus)
      {
      $selected = '';
      if($model->TM_BP_Syllabus_Id == $syllabus->TM_SB_Id)
        $selected= 'selected';
             ?>
              <option value="<?= $syllabus->TM_SB_Id?>" <?= $selected ?>><?= $syllabus->TM_SB_Name ?></option>
            <?php }
            ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">standard:</label>
          <div class="col-sm-9">
            <select class="form-control standards" name="standard" id="standard"><option>Please select a standard</option></select>
          </div>
        </div>
        <div class="form-group">
          <label for="description" class="col-sm-3 control-label">status:</label>
          <div class="col-sm-9">
            <select class="form-control" name="status" id="status"><option>Please select a status</option>
            <option value=0 <?php if($model->TM_BP_Status == 0) {?>selected<?php } ?>>Active</option>
            <option value=1 <?php if($model->TM_BP_Status == 1) {?>selected<?php } ?>>Inactive</option>    
            </select>
          </div>
        </div>
     <?php if(Yii::app()->user->isAdmin()){?>
         <div class="form-group">
             <label class="col-sm-3 control-label" for="availability">Availability</label>
             <div class="col-sm-9">
                 <select class="form-control" name="availability" id="blueprint_Availability">
                     <option value="">Select Availability</option>
                     <option value="0" <?php if($model->TM_BP_Availability == 0) {?>selected<?php } ?>>Student</option>
                     <option value="1" <?php if($model->TM_BP_Availability == 1) {?>selected<?php } ?>>School - All</option>
                     <option value="3" <?php if($model->TM_BP_Availability == 3) {?>selected<?php } ?>>School - Specific</option>
                     <option value="4" <?php if($model->TM_BP_Availability == 4) {?>selected<?php } ?>>Both - Specific Schools</option>
                     <option value="2" <?php if($model->TM_BP_Availability == 2) {?>selected<?php } ?>>Both</option>
                 </select>
             </div>
         </div>

         <div class="form-group" <?php if($model->TM_BP_Availability == 0){ ?>style="display:none;" <?php } ?> id="school_list">
             <label for="description" class="col-sm-3 control-label">published schools:</label>
             <div class="col-sm-9">

                 <?php

                 $selected=array();
                 foreach($schools AS $school):
                     $selected[]=$school->TM_BPS_School_Id;
                 endforeach;

                 echo CHtml::dropDownList('schools',$selected,CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name'),array('class'=>'form-control', 'multiple' => 'multiple')); ?>
             </div>
         </div>

    <?php } else if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()){
      
      $id = Yii::app()->user->id;
          $user=User::model()->find(array('condition'=>'id="'.$id.'"'));
      $school = School::model()->find(array('condition'=>'TM_SCL_Id="'.$user->school_id.'"'));
      ?>
        <input type="hidden" name="availability" value=3>
         <div class="form-group">
             <label for="description" class="col-sm-3 control-label">published schools:</label>
             <div class="col-sm-9">

                 <?php
                 if($id==$model->TM_BP_CreatedBy){
                     echo CHtml::dropDownList('schools',$selected,CHtml::listData(School::model()->findAll(array('condition'=>'TM_SCL_Id="'.$user->school_id.'"','order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name'),array('class'=>'form-control','disabled' => 'disabled','multiple' => 'multiple','options'=>array($user->school_id=>array('selected'=>true))));
                 }
                 else{
                     echo CHtml::dropDownList('schools',$selected,CHtml::listData(School::model()->findAll(array('condition'=>'TM_SCL_Name="'.$school->TM_SCL_Name.'"')),'TM_SCL_Id','TM_SCL_Name'),array('prompt' => 'Select School','class'=>'form-control','multiple' => 'multiple','options'=>array($user->school_id=>array('selected'=>true))));
                 }
                 ?>
             </div>
         </div>
    <?php } ?>
        <!-- <div class="checkbox">
          <label><input type="checkbox"> Remember me</label>
        </div> -->
          <div class="form-group">
              <div class="col-sm-12" id="preloaderrow" style="display: none">
                  <img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader">
              </div>
          </div>
          <div class="form-group">
              <!--<div class="row">-->
                  <div class="col-sm-3 pull-right">
                      <button type="button" class="btn btn-warning btn-lg btn-block firstpart-submit">Continue</button>
                  </div>
              <!--</div>-->
          </div>
      </div>
      <div class="part second-part" style="display:none;">
        <h3 style="text-align:center;margin-bottom: 30px;margin-top: 0;">Choose number of questions from each chapter</h3>
         <div class="row">
          <div class="col-sm-3 pull-left" style="">
              <button type="button" class="btn btn-warning btn-lg btn-block submitAll">Save</button>
            </div>
        </div>
      <div class="second-part-table" style="overflow-x: auto;"></div>
          <div class="form-group">
              <div class="col-sm-12" id="preloaderrow1" style="display: none">
                  <img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader">
              </div>
          </div>
          <div class="form-group">
              <!--<div class="row">-->
                  <div class="col-sm-3 pull-left">
                      <button type="button" class="btn btn-warning btn-lg btn-block submitAll">Save</button>
                  </div>
              <!--</div>-->
          </div>
      </div>
    </div>
  </form>
  </div>
  <input type="hidden" id="bluePrintId">

  <script type="text/javascript">
    $('body').on('keyup', '.allowedNumbers', function() {
    // alert('success');
  var totalNoOfQuest = 0;
  var totalMarks = 0;
    $(".allowedNumbers").each(function(){
      var val = parseFloat($(this).val());
      var mark = parseFloat($(this).attr('data-mark'));
      if(!isNaN(val)) 
      {
        totalNoOfQuest = totalNoOfQuest+val;
        totalMarks = totalMarks+(val*mark);
      }
    });
    console.log(totalNoOfQuest);
    // console.log(totalMarks);
    $('#totalNoOfQuest').html(totalNoOfQuest);
    $('#totalMark').html(totalMarks);
  });


  $('body').on('change', '.allowedNumbers', function() {
    // alert('success');
  var totalNoOfQuest = 0;
  var totalMarks = 0;
    $(".allowedNumbers").each(function(){
      var val = parseFloat($(this).val());
      var mark = parseFloat($(this).attr('data-mark'));
      if(!isNaN(val)) 
      {
        totalNoOfQuest = totalNoOfQuest+val;
        totalMarks = totalMarks+(val*mark);
      }
    });
    console.log(totalNoOfQuest);
    // console.log(totalMarks);
    $('#totalNoOfQuest').html(totalNoOfQuest);
    $('#totalMark').html(totalMarks);
  });
  

$(document).ready(function()
{

  $(document).on('click', '.topic-toggable', function()
  {
    

    var target = $(this).attr('data-target');
    if(!$(target).is(":visible")){
      $('.topics-toggle-list').hide();
    }
    $(target).slideToggle();

  });
  
  $.ajax({
      type: 'POST',
      url: '../getStandard',
      dataType: "json",
      data: {syllabus: $('#syllabus').val(), selecteStandard: <?= $model->TM_BP_Standard_Id ?>},
      success: function(data){
          if(data.error=='No')
          {
              $('.standards').html(data.result);
          }
      }
  });
  // console.log('test');
  
  $(document).on('click', '.submitAll', function()
  {
    $(this).attr('disabled','disabled');
    $('#preloaderrow1').show();
    var name = $('#name').val();
    var description = $('#description').val();
    var syllabus = $('#syllabus').val();
    var standard = $('.standards').val();
    var status = $('#status').val();
    var schools = $('#schools').val();
    $.ajax({
      type: 'POST',
      url: '../UpdateBluePrint',
      //async:false,
      dataType: "json",
      data: $('#blueprintForm').serialize(),
      success: function(data){
          if(data.error=='No')
          {
            // alert('success');
              // var bluePrintId = <?= $model->TM_BP_Id ?>;
              window.location.replace('../admin.html');
          }
      }
    });

  });
  $(document).on('click', '.firstpart-submit', function()
  {
    var standard = $('.standards').val();
    $.ajax({
      type: 'POST',
      url: '../getSecondPartEdit',
        beforeSend : function(){
            $('#preloaderrow').show();
        },
      dataType: "json",
      data: {standard: standard, bluePrintId: <?= $model->TM_BP_Id ?>},
      success: function(data){
          if(data.error=='No')
          {
            $('.second-part').show();
            $('.second-part-table').html(data.result);
            $('.first-part').hide();
              // total mark calculation
            var totalNoOfQuest = 0;
            var totalMarks = 0;
            $(".allowedNumbers").each(function(){
              var val = parseFloat($(this).val());
              var mark = parseFloat($(this).attr('data-mark'));
              if(!isNaN(val)) 
              {
                totalNoOfQuest = totalNoOfQuest+val;
                totalMarks = totalMarks+(val*mark);
              }
            });
            console.log(totalNoOfQuest);
            // console.log(totalMarks);
            $('#totalNoOfQuest').html(totalNoOfQuest);
            $('#totalMark').html(totalMarks);
          }
      }
    });
  });
    $(document).on('change', '#blueprint_Availability', function()
    {
        // alert($(this).val());
        if($(this).val() == 0)
            $('#school_list').hide();
        else{
            $('#school_list').show();
            $('#school_list option').prop('selected', true);
        }

    });

    $(document).on('click', '.checkall', function()
    {
        var mark=$(this).data('id');
        var chapter=$(this).data('chapter');
        $('.inputtopic-'+mark+'-'+chapter).prop('checked', true);
    });
    $(document).on('click', '.uncheckall', function()
    {
        var mark=$(this).data('id');
        var chapter=$(this).data('chapter');
        $('.inputtopic-'+mark+'-'+chapter).prop('checked', false);
    });
  $(document).on('click', '.syllabus', function()
  { 
  /// alert('test');
  var syllabus=$(this).val();
  // // if($('#schedule_item_second').val()=='' )
  // //     $('#schedule_item_second').val(scheduleItem);
  $.ajax({
      type: 'POST',
      url: '../getStandard',
      dataType: "json",
      data: {syllabus: syllabus, selecteStandard: <?= $model->TM_BP_Standard_Id ?>},
      success: function(data){
          if(data.error=='No')
          {
              $('.standards').html(data.result);
          }
      }
  });
  });
});

</script>