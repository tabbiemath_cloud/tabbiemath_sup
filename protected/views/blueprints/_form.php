<?php
/* @var $this BlueprintsController */
/* @var $model Blueprints */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'blueprints-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_Name'); ?>
		<?php echo $form->textField($model,'TM_BP_Name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'TM_BP_Name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_Description'); ?>
		<?php echo $form->textField($model,'TM_BP_Description',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'TM_BP_Description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_Syllabus_Id'); ?>
		<?php echo $form->textField($model,'TM_BP_Syllabus_Id'); ?>
		<?php echo $form->error($model,'TM_BP_Syllabus_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_Standard_Id'); ?>
		<?php echo $form->textField($model,'TM_BP_Standard_Id'); ?>
		<?php echo $form->error($model,'TM_BP_Standard_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_Totalmarks'); ?>
		<?php echo $form->textField($model,'TM_BP_Totalmarks'); ?>
		<?php echo $form->error($model,'TM_BP_Totalmarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_Status'); ?>
		<?php echo $form->textField($model,'TM_BP_Status'); ?>
		<?php echo $form->error($model,'TM_BP_Status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_BP_CreatedOn'); ?>
		<?php echo $form->error($model,'TM_BP_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_BP_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_BP_CreatedBy'); ?>
		<?php echo $form->error($model,'TM_BP_CreatedBy'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->