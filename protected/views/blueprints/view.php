
<style type="text/css">
	
  @media screen and (max-width: 992px) {
 .side-nav
 {
  display: none;
 }
 .manage-mobile{
  display: block !important;
 }
}
</style>
<?php
/* @var $this BlueprintsController */
/* @var $model Blueprints */

$this->breadcrumbs=array(
	'Blueprints'=>array('index'),
	$model->TM_BP_Id,
);

if($model->TM_BP_CreatedBy == Yii::app()->user->id || Yii::app()->user->isAdmin())
{
	$this->menu=array(
		array('label'=>'Manage Blueprints', 'class'=>'nav-header'),
		array('label'=>'List Blueprints', 'url'=>array('admin')),
		array('label'=>'Create Blueprints', 'url'=>array('create')),
		array('label'=>'Update Blueprints', 'url'=>array('update', 'id'=>$model->TM_BP_Id)),
		//array('label'=>'Manage Blueprints', 'url'=>array('admin')),
		array('label'=>'Delete Blueprints', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_BP_Id),'confirm'=>'Are you sure you want to delete this item?')),
	);
	?>
	<div style="margin-top:15px;display: inline-block;">                
	    <a href="<?php echo Yii::app()->createUrl('blueprints/admin') ?>" style="display:none;width:150px;float:left;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >List Blueprint</button></a>

	    <a href="<?php echo Yii::app()->createUrl('blueprints/create') ?>" style="display:none;width:150px;float:right;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >Create Blueprint</button></a>

	    
	</div>
	<div style="margin-top:15px;display: inline-block;">
		<a href="<?php echo Yii::app()->createUrl('blueprints/update',array('id'=>$model->TM_BP_Id))?>" style="display:none;width:150px;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >Update</button></a>
	</div>
	<?php
}
else
{

	$this->menu=array(
	array('label'=>'Manage Blueprints', 'class'=>'nav-header'),
	array('label'=>'List Blueprints', 'url'=>array('admin')),
	array('label'=>'Create Blueprints', 'url'=>array('create')),
	// array('label'=>'Update Blueprints', 'url'=>array('update', 'id'=>$model->TM_BP_Id)),
	//array('label'=>'Manage Blueprints', 'url'=>array('admin')),
	// array('label'=>'Delete Blueprints', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_BP_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div style="margin-top:15px;display: inline-block;">                
    <a href="<?php echo Yii::app()->createUrl('blueprints/admin') ?>" style="display:none;width:150px;float:left;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >List Blueprint</button></a>

    <a href="<?php echo Yii::app()->createUrl('blueprints/create') ?>" style="display:none;width:150px;float:right;" class="choosegroups manage-mobile"><button type="button" class="btn btn-warning btn-block" >Create Blueprint</button></a>
</div>
<?php
}
?>



<h3>View <?php echo $model->TM_BP_Name; ?></h3>
<div class="row brd1">
	<div class="col-lg-12">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'TM_BP_Id',
		'TM_BP_Name',
		'TM_BP_Description',
	    /* array(
			'name'=>'TM_BP_Description',
			'label'=>'Blue Print Description',
		 ), */
	     array(
			'name'=>'TM_BP_Syllabus_Id',
			'value'=>Syllabus::model()->findByPk($model->TM_BP_Syllabus_Id)->TM_SB_Name,
		 ),
	    array(
			'name'=>'TM_BP_Standard_Id',
			'value'=>Standard::model()->findByPk($model->TM_BP_Standard_Id)->TM_SD_Name,
		 ),
		array(
				'name' => 'TM_BP_Availability',
				'value' => Blueprints::GetAvailablity($model->TM_BP_Availability),
		),
		///'TM_BP_Totalmarks',
		//'TM_BP_Status',
	     array(
			'name'=>'TM_BP_Status',
	        'value'=>Blueprints::itemAlias("BlueprintStatus",$model->TM_BP_Status),
		 ),
		//'TM_BP_CreatedOn',
	     /*array(
			'name'=>'TM_BP_CreatedBy',
			'value'=>User::model()->findByPk($model->TM_BP_CreatedBy)->username,
		 ),*/
	),'htmlOptions' => array('class' => 'table table-bordered table-hover')
)); ?>
	</div>
</div>


<div style="overflow-x: auto;">
<?php
if(count($chapters) > 0)
		{
			?>
			<table class="table table-bordered" style="width:100%;box-sizing: border-box;table-layout: fixed;background-color: white;">
    	  		<tr>
		    	<th style="width:250px;"></th>
			
		    <?php

			if(count($marks) > 0)
			{
				foreach ($marks as $mark)
				{
					?>
                    <!-- <td><?= $mark->TM_AV_MK_Mark ?></td> -->
                   <th style="width:100px;
    text-align: center;"><?= $mark->TM_AV_MK_Mark.'-Mark' ?></th>
				<?php
				}
			}
			?>

		</tr>
		<tr>
			<th style="width:250px;">
			Total no of questions: <span id="totalNoOfQuest"></span><br>
			Total marks: <span id="totalMark"></span>
			</th>
			<?php
			if(count($marks) > 0)
			{
					foreach ($marks as $mark)
					{
						?>
	               
	                   <th style="text-align:center;width:100px;"><span id="QuestionsInRow"></span><br>
	                   <span id="markInRow"></span>
	                   </th>
             <?php      
				}
			}
			?>
		</tr>

		<?php
		foreach($chapters AS $chapter)
			{
				?>
				<tr style="background-color:#f6f6f6;border-bottom: 2px solid #d7d7d7;"><td style="padding-left: 10px;width:250px;"><?= $chapter->TM_TP_Name ?></td>

			<?php
			foreach ($marks as $mark)
			{

			$value = blueprintTemplate::model()->find(array('condition'=>'TM_BPT_Blueprint_Id='.$model->TM_BP_Id.' AND TM_BPT_Chapter_Id='.$chapter->TM_TP_Id.' AND TM_BPT_Mark_type='.$mark->TM_AV_MK_Id));  


						?>
						<td style="padding: 10px;width:100px;
    text-align: center;">
    <input type="hidden" class="allowedNumbers" data-valueid=<?= $value->TM_BPT_Id ?> value=<?= $value->TM_BPT_No_questions ?> data-chapter=<?= $chapter->TM_TP_Id ?> data-mark=<?= $mark->TM_AV_MK_Mark ?> data-id=<?= $mark->TM_AV_MK_Id ?> name="allowedNumbers">
    <?= $value->TM_BPT_No_questions ?>
	                   </td>


			<?php 
			}
			?>

	</tr>

	
		<?php } ?>






	</table>
			<?php

	}
?>
</div>
<script>
$(document).ready(function()
{
	var totalNoOfQuest = 0;
    var totalMarks = 0;
    $(".allowedNumbers").each(function(){
      var val = parseFloat($(this).val());
      var mark = parseFloat($(this).attr('data-mark'));
      if(!isNaN(val)) 
      {
        totalNoOfQuest = totalNoOfQuest+val;
        totalMarks = totalMarks+(val*mark);
      }
    });
    console.log(totalNoOfQuest);
    // console.log(totalMarks);
    $('#totalNoOfQuest').html(totalNoOfQuest);
    $('#totalMark').html(totalMarks);
 });
</script>