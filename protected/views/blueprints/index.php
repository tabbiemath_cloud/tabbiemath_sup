<?php
/* @var $this BlueprintsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Blueprints',
);

$this->menu=array(
	array('label'=>'Create Blueprints', 'url'=>array('create')),
	array('label'=>'Manage Blueprints', 'url'=>array('admin')),
);
?>

<h1>Blueprints</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
