<?php
/* @var $this CountryController */
/* @var $model Country */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'country-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_CON_Name'); ?>
		<?php echo $form->textField($model,'TM_CON_Name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'TM_CON_Name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_CON_Code'); ?>
		<?php echo $form->textField($model,'TM_CON_Code',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'TM_CON_Code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_CON_Status'); ?>
		<?php echo $form->textField($model,'TM_CON_Status'); ?>
		<?php echo $form->error($model,'TM_CON_Status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_CON_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_CON_CreatedBy'); ?>
		<?php echo $form->error($model,'TM_CON_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_CON_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_CON_CreatedOn'); ?>
		<?php echo $form->error($model,'TM_CON_CreatedOn'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->