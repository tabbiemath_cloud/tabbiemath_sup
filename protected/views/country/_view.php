<?php
/* @var $this CountryController */
/* @var $data Country */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CON_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_CON_Id), array('view', 'id'=>$data->TM_CON_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CON_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CON_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CON_Code')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CON_Code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CON_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CON_Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CON_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CON_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CON_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CON_CreatedOn); ?>
	<br />


</div>