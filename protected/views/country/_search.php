<?php
/* @var $this CountryController */
/* @var $model Country */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_CON_Id'); ?>
		<?php echo $form->textField($model,'TM_CON_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CON_Name'); ?>
		<?php echo $form->textField($model,'TM_CON_Name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CON_Code'); ?>
		<?php echo $form->textField($model,'TM_CON_Code',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CON_Status'); ?>
		<?php echo $form->textField($model,'TM_CON_Status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CON_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_CON_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CON_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_CON_CreatedOn'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->