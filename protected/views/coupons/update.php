<?php
/* @var $this CouponsController */
/* @var $model Coupons */

$this->breadcrumbs=array(
	'Coupons'=>array('admin'),
	$model->TM_CP_Id=>array('view','id'=>$model->TM_CP_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Coupons', 'class'=>'nav-header'),
	array('label'=>'List Coupons', 'url'=>array('admin')),
	array('label'=>'Create Coupons', 'url'=>array('create')),
	array('label'=>'View Coupons', 'url'=>array('view', 'id'=>$model->TM_CP_Id)),
);
?>

<h3>Update <?php echo $model->TM_CP_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>