<?php
/* @var $this CouponsController */
/* @var $model Coupons */

$this->breadcrumbs=array(
	'Coupons'=>array('admin'),
	$model->TM_CP_Id,
);

$this->menu=array(
    array('label'=>'Manage Coupons', 'class'=>'nav-header'),
	array('label'=>'List Coupons', 'url'=>array('admin')),
	array('label'=>'Create Coupons', 'url'=>array('create')),
	array('label'=>'Update Coupons', 'url'=>array('update', 'id'=>$model->TM_CP_Id)),
	array('label'=>'Delete Coupons', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_CP_Id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View <?php echo $model->TM_CP_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $id=$model->TM_CP_Type;
        $allocid=$model->TM_CP_Allocation;
        $userid=$model->TM_CP_createdBy;
        $status=$model->TM_CP_Status;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_CP_Id',
                'TM_CP_Name',
                'TM_CP_Code',
                array(
                    'label'=>'Type',
                    'type'=>'raw',
                    'value'=>Coupons::model()->GetType($id),
                ),
                'TM_CP_value',
                'TM_CP_startdate',
                'TM_CP_enddate',
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Coupons::itemAlias("CouponsStatus",$status),
                ),
                array(
                    'name'=>'TM_CP_Currency_Id',
                    'type'=>'raw',
                    'value'=>Currency::model()->findByPk($model->TM_CP_Currency_Id)->TM_CR_Name,
                ),
                'TM_CP_createdOn',
                array(
                    'label'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
