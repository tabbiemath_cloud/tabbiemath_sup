<?php
/* @var $this CouponsController */
/* @var $model Coupons */

$this->breadcrumbs=array(
	'Coupons'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Coupons', 'class'=>'nav-header'),
	array('label'=>'List Coupons', 'url'=>array('admin')),
	array('label'=>'Create Coupons', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#coupons-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>Manage Coupons</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'coupons-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                'TM_CP_Name',
                'TM_CP_Code',
                array(
                    'name'=>'TM_CP_Type',
                    'value'=>'Coupons::itemAlias("Coupontype",$data->TM_CP_Type)',
                ),
                'TM_CP_value',

                'TM_CP_startdate',
                'TM_CP_enddate',
                array(
                    'name'=>'TM_CP_Status',
                    'value'=>'Coupons::itemAlias("CouponsStatus",$data->TM_CP_Status)',
                    'filter'=>Coupons::itemAlias("CouponsStatus"),
                ),
                /*'TM_CP_Allocation',

                'TM_CP_createdOn',
                'TM_CP_createdBy',
                */
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            )),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
