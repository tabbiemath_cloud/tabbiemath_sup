<?php
/* @var $this CouponsController */
/* @var $model Coupons */

$this->breadcrumbs=array(
	'Coupons'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Coupons', 'class'=>'nav-header'),
	array('label'=>'List Coupons', 'url'=>array('admin')),
);
?>

<h3>Create Coupons</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>