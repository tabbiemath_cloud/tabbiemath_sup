<?php
/* @var $this CouponsController */
/* @var $model Coupons */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_Id'); ?>
		<?php echo $form->textField($model,'TM_CP_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_Name'); ?>
		<?php echo $form->textField($model,'TM_CP_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_Code'); ?>
		<?php echo $form->textField($model,'TM_CP_Code',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_Type'); ?>
		<?php echo $form->textField($model,'TM_CP_Type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_value'); ?>
		<?php echo $form->textField($model,'TM_CP_value',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_duration'); ?>
		<?php echo $form->textField($model,'TM_CP_duration',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_startdate'); ?>
		<?php echo $form->textField($model,'TM_CP_startdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_enddate'); ?>
		<?php echo $form->textField($model,'TM_CP_enddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_Allocation'); ?>
		<?php echo $form->textField($model,'TM_CP_Allocation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_Status'); ?>
		<?php echo $form->textField($model,'TM_CP_Status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_createdOn'); ?>
		<?php echo $form->textField($model,'TM_CP_createdOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CP_createdBy'); ?>
		<?php echo $form->textField($model,'TM_CP_createdBy'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->