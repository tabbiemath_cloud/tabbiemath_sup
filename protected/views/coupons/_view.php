<?php
/* @var $this CouponsController */
/* @var $data Coupons */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_CP_Id), array('view', 'id'=>$data->TM_CP_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_Code')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_Code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_Type')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_Type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_value')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_duration')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_startdate')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_startdate); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_enddate')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_enddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_Allocation')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_Allocation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_createdOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_createdOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CP_createdBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CP_createdBy); ?>
	<br />

	*/ ?>

</div>