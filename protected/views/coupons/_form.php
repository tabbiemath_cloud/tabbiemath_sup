<?php
/* @var $this CouponsController */
/* @var $model Coupons */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('coupons/admin')."';
    });
");
?>

<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'coupons-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_CP_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_Name'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_Code',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_CP_Code',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_Code'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_Type',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_CP_Type',Coupons::itemAlias('Coupontype'),array('empty'=>'Select Type','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_Type'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_value',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_CP_value',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_value'); ?>
            </div>
        </div><!--
        <div class="form-group">
            <?php /*echo $form->labelEx($model,'TM_CP_duration',array('class'=>'col-sm-3 control-label')); */?>
            <div class="col-sm-9">
                <?php /*echo $form->textField($model,'TM_CP_duration',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); */?>
                <?php /*echo $form->error($model,'TM_CP_duration'); */?>
            </div>
        </div>-->
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_startdate',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dateField($model,'TM_CP_startdate',array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_startdate'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_enddate',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dateField($model,'TM_CP_enddate',array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_enddate'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_Currency_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_CP_Currency_Id',CHtml::listData(Currency::model()->findAll(array('order' => 'TM_CR_Name')),'TM_CR_Id','TM_CR_Name'),array('empty'=>'Select Currency','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_Allocation'); ?>
            </div>
        </div>
        <!--
        <div class="form-group">
            <?php /*echo $form->labelEx($model,'TM_CP_Allocation',array('class'=>'col-sm-3 control-label')); */?>
            <div class="col-sm-9">
                <?php /*echo $form->dropDownList($model,'TM_CP_Allocation',array('0'=>'All','1'=>'Student','2'=>'School'),array('empty'=>'Select Allocation','class'=>'form-control')); */?>
                <?php /*echo $form->error($model,'TM_CP_Allocation'); */?>
            </div>
        </div>-->

        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_CP_Status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->hiddenField($model,'TM_CP_createdBy',array('value'=>Yii::app()->user->id)); ?>
                <?php echo $form->dropDownList($model,'TM_CP_Status',Coupons::itemAlias('CouponsStatus'),array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_CP_Status'); ?>

            </div>
        </div>

        <!--<div class="row">
            <?php /*echo $form->labelEx($model,'TM_CP_createdOn'); */?>
            <?php /*echo $form->textField($model,'TM_CP_createdOn'); */?>
            <?php /*echo $form->error($model,'TM_CP_createdOn'); */?>
        </div>

        <div class="row">
            <?php /*echo $form->labelEx($model,'TM_CP_createdBy'); */?>
            <?php /*echo $form->textField($model,'TM_CP_createdBy'); */?>
            <?php /*echo $form->error($model,'TM_CP_createdBy'); */?>
        </div>-->

        <div class="form-group">
            <div class="row">
            <div class="col-sm-3 pull-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="reset" value="Reset">Cancel</button>
                </div>
                </div>
        </div>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->