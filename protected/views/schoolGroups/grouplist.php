<?php
$this->breadcrumbs=array(
    UserModule::t('Groups')=>array('grouplist'),
    UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Groups', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),
    //array('label' => 'List Groups', 'url' => array('manageGroups','id'=>$id)),
    array('label'=>'Add Groups', 'url'=>array('AddGroups','id'=>$id)),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#school-groups-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
#school-groups-grid_c1
{
    text-align: right;
}
</style>
<?php /*echo $this->renderPartial('_menu', array(
    'list'=> array(
        CHtml::link(UserModule::t('Create User'),array('create')),
    ),
));
*/
if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
<h3><?php echo UserModule::t("Manage Groups"); ?></h3>
<?php elseif(Yii::app()->user->isTeacher()):?>
<div class="row">
    <div class="col-lg-12">
        <h3 class="h125 pull-left"><?php echo UserModule::t("Manage Groups"); ?></h3>
        <a href="<?php echo Yii::app()->createUrl('schoolGroups/AddGroups',array('id'=>$id))?>"visible='<?php UserModule::isAllowedSchool();?>'>
            <button class="btn pull-right btn-warning quickhwvalidation" type="button" style="margin-top: 18px; margin-bottom: 10px;">Create New Group</button>
        </a>
    </div>
</div>
<?php endif;?>
<?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
    <div class="row brd1">
        <div class="col-lg-12">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'school-groups-grid',
        'dataProvider'=>$model->search($id),
        'filter'=>$model,
        'columns'=>array(
            array(
                'name'=>'TM_SCL_GRP_Name',
                'type'=>'raw',
                'value'=>'$data->TM_SCL_GRP_Name',
            ),
            //'TM_SCL_GRP_Name',
            /* array(
                'name'=>'TM_SCL_SD_Id',
                'type'=>'raw',
                'value'=>'SchoolGroups::StandardName($data->TM_SCL_SD_Id)',
                'filter'=>false,
            ),
            array(
                'name'=>'TM_SCL_Id',
                'type'=>'raw',
                'value'=>'SchoolGroups::SchoolName($data->TM_SCL_Id)',
                'filter'=>false,
            ),
            array(
                'name'=>'TM_SCL_PN_Id',
                'type'=>'raw',
                'value'=>'SchoolGroups::PlanName($data->TM_SCL_PN_Id)',
                'filter'=>false,
            ),*/
            array(
                'header'=>'Manage',
                'type'=>'raw',
                'value'=>'SchoolGroups::GetMangeLink($data->TM_SCL_Id,$data->TM_SCL_GRP_Id)',
            ),
            array(
                'class'=>'CButtonColumn',
                'template'=>'{update}{delete}',
                'deleteButtonImageUrl'=>false,
                'updateButtonImageUrl'=>false,
                'viewButtonImageUrl'=>false,
                'buttons'=>array
                (
                    'addstudents'=>array(
                        'label'=>'Manage',
                        'options'=>array(
                            'title'=>'Add Students',
                            'class'=>'btn btn-warning btn-block'
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/addstudents",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                    'view'=>array(
                        'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                        'options'=>array(
                            'title'=>'View'
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/Viewgroup",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                    'update'=>array(
                        'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                        'options'=>array(
                            'title'=>'Update'
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/Editgroup",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                    'delete'=>array(
                        'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                        'options'=>array(

                            'title'=>'Delete',
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/deletegroup",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                ),
            ),
        ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
    )); ?>
<?php else:?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'school-groups-grid',
        'dataProvider'=>$model->search($id),
        'filter'=>$model,
        'columns'=>array(
            array(
                'name'=>'TM_SCL_GRP_Name',
                'type'=>'raw',
                'value'=>'$data->TM_SCL_GRP_Name',
            ),
            //'TM_SCL_GRP_Name',
            /* array(
                'name'=>'TM_SCL_SD_Id',
                'type'=>'raw',
                'value'=>'SchoolGroups::StandardName($data->TM_SCL_SD_Id)',
                'filter'=>false,
            ),
            array(
                'name'=>'TM_SCL_Id',
                'type'=>'raw',
                'value'=>'SchoolGroups::SchoolName($data->TM_SCL_Id)',
                'filter'=>false,
            ),
            array(
                'name'=>'TM_SCL_PN_Id',
                'type'=>'raw',
                'value'=>'SchoolGroups::PlanName($data->TM_SCL_PN_Id)',
                'filter'=>false,
            ),*/
            array(
                'header'=>'Manage',
                'type'=>'raw',
                'value'=>'SchoolGroups::GetMangeLink($data->TM_SCL_Id,$data->TM_SCL_GRP_Id)',
            ),
            array(
                'class'=>'CButtonColumn',
                'template'=>'{update}{delete}',
                'deleteButtonImageUrl'=>false,
                'updateButtonImageUrl'=>false,
                'viewButtonImageUrl'=>false,
                'buttons'=>array
                (
                    'addstudents'=>array(
                        'label'=>'Manage',
                        'options'=>array(
                            'title'=>'Add Students',
                            'class'=>'btn btn-warning btn-block'
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/addstudents",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                    'view'=>array(
                        'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                        'options'=>array(
                            'title'=>'View'
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/Viewgroup",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                    'update'=>array(
                        'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                        'options'=>array(
                            'title'=>'Update'
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/Editgroup",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                    'delete'=>array(
                        'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                        'options'=>array(

                            'title'=>'Delete',
                        ),
                        'url'=>'Yii::app()->createUrl("SchoolGroups/deletegroup",array("id"=>$data->TM_SCL_Id,"group"=>$data->TM_SCL_GRP_Id))'),
                ),
            ),
        ),'itemsCssClass'=>"rwd-tables"
    )); ?>
<?php endif;?>

        </div>
    </div>
</div>
