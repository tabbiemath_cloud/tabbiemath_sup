<?php
/* @var $this SchoolGroupsController */
/* @var $model SchoolGroups */

$this->breadcrumbs=array(
	'School Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SchoolGroups', 'url'=>array('index')),
	array('label'=>'Manage SchoolGroups', 'url'=>array('admin')),
);
?>

<h1>Create SchoolGroups</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>