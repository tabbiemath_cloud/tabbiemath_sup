<?php
/* @var $this SchoolGroupsController */
/* @var $model SchoolGroups */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_GRP_Id'); ?>
		<?php echo $form->textField($model,'TM_SCL_GRP_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_GRP_Name'); ?>
		<?php echo $form->textField($model,'TM_SCL_GRP_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_SD_Id'); ?>
		<?php echo $form->textField($model,'TM_SCL_SD_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Id'); ?>
		<?php echo $form->textField($model,'TM_SCL_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_PN_Id'); ?>
		<?php echo $form->textField($model,'TM_SCL_PN_Id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->