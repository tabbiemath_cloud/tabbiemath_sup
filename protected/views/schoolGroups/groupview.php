<?php
$this->breadcrumbs=array(
    'Schools'=>array('admin'),
    'Manage',
);
$this->menu=array(
    array('label'=>'Manage Admin', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$school), 'visible'=>UserModule::isAllowedSchool()),
    array('label' => 'List Groups', 'url' => array('manageGroups','id'=>$school)),
    array('label'=>'Add Groups', 'url'=>array('AddGroups','id'=>$school)),
    array('label'=>'Update Groups', 'url'=>array('Editgroup', 'id'=>$school,'group'=>$schoolgroup->TM_SCL_GRP_Id)),
    array('label'=>'Delete Groups', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$school,'group'=>$schoolgroup->TM_SCL_GRP_Id),'confirm'=>'Are you sure you want to delete this Group?')),
);
if(Yii::app()->user->isSchoolAdmin()):?>
<h3>View Group <?php echo $schoolgroup->TM_SCL_GRP_Name; ?></h3>
        <?php 
        elseif(Yii::app()->user->isTeacher()):?>
<div class="row">
    <div class="col-lg-12">
        <h3>View Group <?php echo $schoolgroup->TM_SCL_GRP_Name; ?></h3>        
    </div>
   
    <!-- /.col-lg-12 -->
</div>
<?php 
        endif;
?>

<div class="row brd1">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover" id="yw0">
            <tr>
                <th>Group Name</th>
                <td><?php echo $schoolgroup->TM_SCL_GRP_Name;?></td>
            </tr>
            <tr>
                <th>Standard</th>
                <td><?php echo Standard::model()->findByPk($schoolgroup->TM_SCL_SD_Id)->TM_SD_Name;?></td>
            </tr>
            <tr>
                <th>School Name</th>
                <td><?php echo School::model()->findByPk($schoolgroup->TM_SCL_Id)->TM_SCL_Name;?></td>
            </tr>
            <tr>
                <th>Plan</th>
                <td><?php echo Plan::model()->findByPk($schoolgroup->TM_SCL_PN_Id)->TM_PN_Name;?></td>
            </tr>
        </table>
    </div>
</div>