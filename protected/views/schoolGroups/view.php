<?php
/* @var $this SchoolGroupsController */
/* @var $model SchoolGroups */

$this->breadcrumbs=array(
	'School Groups'=>array('index'),
	$model->TM_SCL_GRP_Id,
);

$this->menu=array(
	array('label'=>'List SchoolGroups', 'url'=>array('index')),
	array('label'=>'Create SchoolGroups', 'url'=>array('create')),
	array('label'=>'Update SchoolGroups', 'url'=>array('update', 'id'=>$model->TM_SCL_GRP_Id)),
	array('label'=>'Delete SchoolGroups', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_SCL_GRP_Id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SchoolGroups', 'url'=>array('admin')),
);
?>

<h1>View SchoolGroups #<?php echo $model->TM_SCL_GRP_Id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_SCL_GRP_Id',
		'TM_SCL_GRP_Name',
		'TM_SCL_SD_Id',
		'TM_SCL_Id',
		'TM_SCL_PN_Id',
	),
)); ?>
