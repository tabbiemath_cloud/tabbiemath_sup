<?php
/* @var $this SchoolGroupsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'School Groups',
);

$this->menu=array(
	array('label'=>'Create SchoolGroups', 'url'=>array('create')),
	array('label'=>'Manage SchoolGroups', 'url'=>array('admin')),
);
?>

<h1>School Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
