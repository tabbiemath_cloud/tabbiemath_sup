<!--code by amal-->
<?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
    <div class="row brd1">
<?php else:?>
    <div class="row">
        <div class="col-lg-8 ">
            <div class="panel panel-default">
<?php endif;?>
<!--ends-->
                <?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>
                <div class="panel-body form-horizontal payment-form">
                    <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
                    <div class="form-group">
                        <?php echo CHtml::activeLabelEx($model,'TM_SCL_GRP_Name',array('class'=>'col-sm-3 control-label')); ?>
                        <div class="col-sm-9">
                            <?php echo CHtml::activeTextField($model,'TM_SCL_GRP_Name',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
                            <?php echo CHtml::error($model,'TM_SCL_GRP_Name'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label required" for="TM_SCL_SD_Id">Select Standard</label>
                        <div class="col-sm-9">
                            <?php
                            $c = new CDbCriteria;
                            $c->select = 't.TM_SD_Id, t.TM_SD_Name,tm_school_plan.TM_SPN_PlanId';
                            $c->join = "INNER JOIN tm_school_plan tm_school_plan ON t.TM_SD_Id = tm_school_plan.TM_SPN_StandardId";
                            $c->compare("tm_school_plan.TM_SPN_SchoolId", $id);
                            $result = Standard::model()->findAll($c);
                            $listdata = CHtml::listData($result, 'TM_SD_Id', 'TM_SD_Name');
                            $selected = array();

                            $selected[] = $model['TM_SCL_SD_Id'];
                            echo CHtml::dropDownList('Standard', $selected, $listdata, array('class' => 'form-control', 'empty' => 'Select Standard'));
                            ?>
                            <?php echo CHtml::error($model,'TM_SCL_SD_Id'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <!--code by amal-->
                            <div class="col-sm-3 pull-right">
                                <?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):
                                    echo CHtml::submitButton($model->isNewRecord ? 'Continue' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block'));
                                else:
                                    echo CHtml::submitButton($model->isNewRecord ? 'Continue' : 'Save',array('class'=>'btn btn-warning btn-block'));
                                endif; ?>
                            </div>
                            <div class="col-lg-3 pull-right">
                                <?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
                                <button class="btn btn-warning btn-lg btn-block" type="button" value="Reset" onclick="goBack()">Cancel</button>
                                <?php else:?>
                                <button class="btn btn-warning btn-block" type="button" value="Reset" onclick="goBack()">Cancel</button>
                                <?php endif;?>
                            </div>
                            <!--ends-->
                        </div>
                    </div>
                </div>
                <?php echo CHtml::endForm(); ?>
            </div>
        </div>
    </div>
<script>
    function goBack()
    {
        window.history.back()
    }
</script>