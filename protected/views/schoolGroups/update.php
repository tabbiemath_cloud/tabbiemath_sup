<?php
/* @var $this SchoolGroupsController */
/* @var $model SchoolGroups */

$this->breadcrumbs=array(
	'School Groups'=>array('index'),
	$model->TM_SCL_GRP_Id=>array('view','id'=>$model->TM_SCL_GRP_Id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SchoolGroups', 'url'=>array('index')),
	array('label'=>'Create SchoolGroups', 'url'=>array('create')),
	array('label'=>'View SchoolGroups', 'url'=>array('view', 'id'=>$model->TM_SCL_GRP_Id)),
	array('label'=>'Manage SchoolGroups', 'url'=>array('admin')),
);
?>

<h1>Update SchoolGroups <?php echo $model->TM_SCL_GRP_Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>