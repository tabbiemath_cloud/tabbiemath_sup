<?php
$this->breadcrumbs=array(
    'Schools'=>array('admin'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Admin', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$school), 'visible'=>UserModule::isAllowedSchool()),
    array('label' => 'List Groups', 'url' => array('manageGroups','id'=>$school)),
    array('label'=>'Add Groups', 'url'=>array('AddGroups','id'=>$school)),
    array('label'=>'Delete Groups', 'url'=>'#', 'linkOptions'=>array('submit'=>array('deletegroup','id'=>$school,'group'=>$model->TM_SCL_GRP_Id),'confirm'=>'Are you sure you want to delete this Group?')),
);
    if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
        <h3>Update Group <?php echo $model->TM_SCL_GRP_Name; ?></h3>
        <?php elseif(Yii::app()->user->isTeacher()):?>
            <div class="row">
                <div class="col-lg-12">
                    <h3>Update Group <?php echo $model->TM_SCL_GRP_Name; ?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
    <?php endif;?>
<?php 	echo $this->renderPartial('_form', array('model'=>$model)); ?>