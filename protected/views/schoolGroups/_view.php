<?php
/* @var $this SchoolGroupsController */
/* @var $data SchoolGroups */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_GRP_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_SCL_GRP_Id), array('view', 'id'=>$data->TM_SCL_GRP_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_GRP_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_GRP_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_SD_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_SD_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_PN_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_PN_Id); ?>
	<br />


</div>