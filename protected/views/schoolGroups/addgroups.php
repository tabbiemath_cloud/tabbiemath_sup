<?php

$this->menu = array(
    array('label' => 'Manage Students', 'class' => 'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),
    array('label' => 'List Groups', 'url' => array('manageGroups','id'=>$id)),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
    if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
        <h1><?php echo UserModule::t("Create School Group"); ?></h1>
    <?php elseif(Yii::app()->user->isTeacher()):?>
        <div class="row">
            <div class="col-lg-12">
                <h3><?php echo UserModule::t("Create School Group"); ?></h3>
            </div>
    <!-- /.col-lg-12 -->
        </div>
<?php endif;?>

<?php
/*echo $this->renderPartial('_menu',array(
        'list'=> array(),
    ));*/
echo $this->renderPartial('_form', array('id'=>$id,'model'=>$model));
?>