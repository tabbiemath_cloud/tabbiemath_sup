<?php
/* @var $this SchoolGroupsController */
/* @var $model SchoolGroups */

$this->breadcrumbs=array(
	'School Groups'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SchoolGroups', 'url'=>array('index')),
	array('label'=>'Create SchoolGroups', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#school-groups-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage School Groups</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'school-groups-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'TM_SCL_GRP_Id',
		'TM_SCL_GRP_Name',
		'TM_SCL_SD_Id',
		'TM_SCL_Id',
		'TM_SCL_PN_Id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
