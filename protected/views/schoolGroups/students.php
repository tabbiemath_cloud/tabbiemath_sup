<script type="text/javascript">
    $(function(){
        $('#allstud').hide();
        $('body').on('click touchstart', '.students', function() {
            $(this).toggleClass('selected');
        });
        $('body').on('click touchstart', '.grpstudents', function() {
            $(this).toggleClass('selected');
        });
        /*$('#addstudents').click(function() {
            $('.list1').append($('.list2 .selected').removeClass('selected'));
        });*/
    $(document).on('click touchstart','#addstudents',function(){		        
            $('#nostud').hide();
            $('#groupstud').hide();
            $('.list2').append($('.list1 .selected').removeClass('selected'));
            if ($('.list1 li').length == 1)
            {
                $('#allstud').show();
            }
        });
    $(document).on('click touchstart','#removestudents',function(){		        
            $('#allstud').hide();
            $('#allstudents').hide();
            $('.list1').append($('.list2 .selected').removeClass('selected'));
            if ($('.list2 li').length == 1)
            {
                $('#groupstud').show();
            }
        });
    });
</script>
    <style type="text/css">
        ul {
            list-style-type:none;
        }
        .selected {
                background-color: #efefef;
    padding: 0px 5px 0px 9px;
    border-radius: 4px;
        }
    </style>
<?php

$this->menu = array(
    array('label' => 'Manage Students', 'class' => 'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$model->TM_SCL_Id), 'visible'=>UserModule::isAllowedSchool()),
    array('label' => 'List Groups', 'url' => array('manageGroups','id'=>$model->TM_SCL_Id)),
    array('label'=>'Add Groups', 'url'=>array('AddGroups','id'=>$model->TM_SCL_Id)),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
    if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
        <h3>Group: <?php echo $model->TM_SCL_GRP_Name; ?></h3>
    <?php elseif(Yii::app()->user->isTeacher()):?>
        <div class="row">
            <div class="col-lg-12">
                <h3 class="h125 pull-left">Group: <?php echo $model->TM_SCL_GRP_Name; ?></h3>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    <?php endif;?>

<!--code by amal-->
<?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
    <div class="row brd1">
<?php else:?>
    <div class="row">
        <div class="col-lg-8 ">
            <div class="panel panel-default">
<?php endif;?>
<!--ends-->
        <?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>
        <div class="panel-body form-horizontal payment-form">
            <div class="form-group">
                <div class="col-lg-5">
                    <table class="table table-bordered" id="yw0">
                        <?php if(count($students)>0):?>
                            <tr><td>
                                <ul class='list1'>
                                    <li id="allstud">No more Students to add</li>
                                    <?php foreach($students AS $student):?>
                                    <li value="<?php echo $student['TM_STU_Id'];?>" class="students"><?php echo $student['TM_STU_First_Name'].' '.$student['TM_STU_Last_Name'];?></li>
                                    <?php endforeach;?>
                                    <input type="hidden" id="grpstudents" name="grpstudents" value="">
                                </ul>
                            </td></tr>
                        <?php else:?>
                        <tr><td>
                            <ul class="list1">
                                <li id="allstudents">No Students to Add</li>
                                <input type="hidden" id="grpstudents" name="grpstudents" value="">
                            </ul>
                        </td></tr>
                        <?php endif;?>
                    </table>
                </div>
                <div class="col-lg-2 col-md-2 text-center">
                    <button type="button" class="btn btn-warning btn-md" name="Remove" id="removestudents" value="Remove"><span class="glyphicon glyphicon-arrow-left"></span> </button>
                    <button type="button" class="btn btn-warning btn-md" name="Allocate" id="addstudents" value="Allocate"  style=" display: block; margin: auto; margin-top: 7px;"><span class="glyphicon glyphicon-arrow-right"></span> </button>
                    <!--<input type="button" name="Allocate" id="addstudents" value="Allocate" class="btn btn-warning btn-md">
                    <input type="button" name="Remove" id="removestudents" value="Remove" class="btn btn-warning btn-md" style="margin-top: 5px;">-->
                </div>
                <div class="col-lg-5 pull-right">
                    <table class="table table-bordered" id="yw01">
                        <tbody>
                            <?php if(count($grpstudents)>0):?>
                            <tr><td>
                                <ul class='list2'>
                                    <li id="groupstud" style="display: none;">No more Students to remove</li>
                                <?php foreach($grpstudents AS $grpstudent):?>
                                <li value="<?php echo $grpstudent['TM_GRP_STUId'];?>" class="grpstudents"><?php echo $grpstudent['TM_GRP_STUName'];?></li>
                                <?php endforeach;?>
                                </ul>
                                <input type="hidden" id="students" name="students" value="">
                            </td></tr>
                            <?php else:?>
                            <tr><td>
                                <ul class="list2">
                                    <li id="nostud">No Students</li>
                                </ul>
                                <input type="hidden" id="students" name="students" value="">
                            </td></tr>
                            <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <!--code by amal-->
                <div class="col-sm-2 pull-right">
                    <?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):
                    echo CHtml::submitButton('Submit',array('class'=>'btn btn-warning btn-lg btn-block','onclick'=>'getstud()'));
                    else:
                    echo CHtml::submitButton('Submit',array('class'=>'btn btn-warning  btn-block','onclick'=>'getstud()'));
                    endif; ?>
                </div>
                <div class="col-lg-2 pull-right">
                    <?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin() || Yii::app()->session['mode']== "TeacherAdmin"):?>
                    <button class="btn btn-warning btn-lg btn-block" type="button" value="Reset" onclick="goBack()">Cancel</button>
                    <?php else:?>
                    <button class="btn btn-warning btn-block" type="button" value="Reset" onclick="goBack()">Cancel</button>
                    <?php endif;?>
                </div>
                <!--ends-->
            </div>
        </div>
    </div>
</div>
<?php echo CHtml::endForm(); ?>
<script type="text/javascript">
    function getstud(){
        $('.list2 .students').each(function(){
            var students=$(this).val();
            var oldValue = $("#students").val();
            var arr = oldValue === "" ? [] : oldValue.split(',');
            arr.push(students);
            var newValue = arr.join(',');
            $("#students").val(newValue);
        });
        $('.list1 .grpstudents').each(function(){
            var students=$(this).val();
            var oldValue = $("#grpstudents").val();
            var arr = oldValue === "" ? [] : oldValue.split(',');
            arr.push(students);
            var newValue = arr.join(',');
            $("#grpstudents").val(newValue);
        });
    }
</script>
<script>
    function goBack()
    {
        window.history.back()
    }
</script>



