<?php
/* @var $this MockQuestionsController */
/* @var $model MockQuestions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_MQ_Id'); ?>
		<?php echo $form->textField($model,'TM_MQ_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MQ_Mock_Id'); ?>
		<?php echo $form->textField($model,'TM_MQ_Mock_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MQ_Question_Id'); ?>
		<?php echo $form->textField($model,'TM_MQ_Question_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MQ_Type'); ?>
		<?php echo $form->textField($model,'TM_MQ_Type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MQ_Order'); ?>
		<?php echo $form->textField($model,'TM_MQ_Order'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->