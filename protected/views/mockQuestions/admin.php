<?php
/* @var $this MockQuestionsController */
/* @var $model MockQuestions */

$this->breadcrumbs=array(
	'Mock Questions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MockQuestions', 'url'=>array('index')),
	array('label'=>'Create MockQuestions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mock-questions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Mock Questions</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mock-questions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'TM_MQ_Id',
		'TM_MQ_Mock_Id',
		'TM_MQ_Question_Id',
		'TM_MQ_Type',
		'TM_MQ_Order',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
