<?php
/* @var $this SectionsController */
/* @var $model Sections */

$this->breadcrumbs=array(
	'Sections'=>array('index'),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Topic', 'class'=>'nav-header'),
	array('label'=>'List Topic', 'url'=>array('admin')),
	array('label'=>'Create Topic', 'url'=>array('create')),
	array('label'=>'View Topic', 'url'=>array('view', 'id'=>$model->TM_SN_Id)),
);
?>

<h1>Update Topic <?php echo $model->TM_SN_Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>