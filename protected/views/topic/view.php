<?php
/* @var $this SectionsController */
/* @var $model Sections */

$this->breadcrumbs=array(
	'Topic'=>array('index'),
	$model->TM_SN_Id,
);

$this->menu=array(
    array('label'=>'Manage Topic', 'class'=>'nav-header'),
    array('label'=>'List Topic', 'url'=>array('admin')),
	array('label'=>'Create Topic', 'url'=>array('create')),
	array('label'=>'Update Topic', 'url'=>array('update', 'id'=>$model->TM_SN_Id)),
	array('label'=>'Delete Topic', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_SN_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View <?php echo $model->TM_SN_Name; ?></h1>
<div class="row brd1">
    <div class="col-lg-12">
<?php
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_SN_Id',
        array(
            'name'=>'TM_SN_Syllabus_Id',
            'type'=>'raw',
            'value'=>Syllabus::model()->findByPk($model->TM_SN_Syllabus_Id)->TM_SB_Name,
        ),
        array(
            'name'=>'TM_SN_Standard_Id',
            'type'=>'raw',
            'value'=>Standard::model()->findByPk($model->TM_SN_Standard_Id)->TM_SD_Name,
        ),
        array(
            'name'=>'TM_SN_Topic_Id',
            'type'=>'raw',
            'value'=>Chapter::model()->findByPk($model->TM_SN_Topic_Id)->TM_TP_Name,
        ),
		'TM_SN_Name',

		'TM_SN_CreatedOn',
        array(
            'label'=>'Created By',
            'type'=>'raw',
            'value'=>User::model()->GetUser($model->TM_SN_CreatedBy),
        ),
        array(
            'label'=>'Status',
            'type'=>'raw',
            'value'=>Topic::itemAlias("TopicStatus",$model->TM_SN_Status),
        ),
	),'htmlOptions' => array('class' => 'table table-bordered table-hover')
)); ?>
    </div>
</div>
