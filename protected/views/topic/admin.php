<?php
/* @var $this SectionsController */
/* @var $model Sections */

$this->breadcrumbs=array(
	'Topic'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Topic', 'class'=>'nav-header'),
	array('label'=>'List Topic', 'url'=>array('admin')),
	array('label'=>'Create Topic', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sections-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Topic</h1>


<div class="row brd1">
    <div class="col-lg-12">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'topic-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'TM_SN_Name',
        array(
            'name'=>'TM_SN_Syllabus_Id',
            'value'=>'Syllabus::model()->findByPk($data->TM_SN_Syllabus_Id)->TM_SB_Name',
            'filter'=>CHtml::listData(Syllabus::model()->findAll(array('condition' => "TM_SB_Status='0'",'order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),
        ),
        array(
            'name'=>'TM_SN_Standard_Id',
            'value'=>'Standard::model()->findByPk($data->TM_SN_Standard_Id)->TM_SD_Name',
            'filter' =>
                CHtml::listData(
                    is_numeric($model->TM_SN_Syllabus_Id) ? Standard::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_SD_Syllabus_Id = :syllabusid AND TM_SD_Status='0'",
                        'params' => array(':syllabusid' => $model->TM_SN_Syllabus_Id),
                        'order' => 'TM_SD_Id'
                    ))) : Standard::model()->findAll(array('condition' => "TM_SD_Status='0'",'order' => 'TM_SD_Id')),'TM_SD_Id','TM_SD_Name'),
        ),
        array(
            'name'=>'TM_SN_Topic_Id',
            'value'=>'Chapter::model()->findByPk($data->TM_SN_Topic_Id)->TM_TP_Name',
            'filter' =>
                CHtml::listData(
                    is_numeric($model->TM_SN_Standard_Id) ? Chapter::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_TP_Standard_Id = :standard AND TM_TP_Status='0'",
                        'params' => array(':standard' => $model->TM_SN_Standard_Id),
                        'order' => 'TM_TP_Id'
                    ))) : Chapter::model()->findAll(array('condition' => "TM_TP_Status='0'",'order' => 'TM_TP_Id')),'TM_TP_Id','TM_TP_Name'),
        ),
        array(
            'name'=>'TM_SN_Status',
            'value'=>'Topic::itemAlias("TopicStatus",$data->TM_SN_Status)',
            'filter'=>Topic::itemAlias('TopicStatus'),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'deleteButtonImageUrl'=>false,
            'updateButtonImageUrl'=>false,
            'viewButtonImageUrl'=>false,
            'buttons'=>array
            (
                'view'=>array(
                    'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                    'options'=>array(
                        'title'=>'View'
                    )),
                'update'=>array(
                    'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                    'options'=>array(
                        'title'=>'Update'
                    )),
                'delete'=>array(
                    'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                    'options'=>array(

                        'title'=>'Delete',
                    ),
                )
            ),
        ),
        array(
            'header' => 'Order',
            'name' => 'TM_SN_order',

            'class' => 'ext.OrderColumn.OrderColumnTopic',
        ),
    ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
)); ?>
    </div>
</div>
