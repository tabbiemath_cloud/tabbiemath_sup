<?php
/* @var $this SectionsController */
/* @var $model Sections */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_SN_Id'); ?>
		<?php echo $form->textField($model,'TM_SN_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SN_Name'); ?>
		<?php echo $form->textField($model,'TM_SN_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SN_Topic_Id'); ?>
		<?php echo $form->textField($model,'TM_SN_Topic_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SN_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_SN_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SN_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_SN_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SN_Status'); ?>
		<?php echo $form->textField($model,'TM_SN_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->