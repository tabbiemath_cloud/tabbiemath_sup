<?php
/* @var $this SectionsController */
/* @var $data Sections */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SN_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_SN_Id), array('view', 'id'=>$data->TM_SN_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SN_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SN_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SN_Topic_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SN_Topic_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SN_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SN_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SN_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SN_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SN_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SN_Status); ?>
	<br />


</div>