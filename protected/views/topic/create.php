<?php
/* @var $this SectionsController */
/* @var $model Sections */

$this->breadcrumbs=array(
	'Topic'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Topic', 'class'=>'nav-header'),
	array('label'=>'List Topic', 'url'=>array('admin')),
);
?>

<h1>Create Topic</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>