<?php
/* @var $this QuestionsController */
/* @var $model Questions */

$this->breadcrumbs=array(
    'Questions'=>array('index'),
    $model->TM_QN_Id,
);
$showoption=0;
$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
    array('label'=>'List Questions', 'url'=>array('admin')),
    array('label'=>'Create Question', 'url'=>array('create')),
    array('label'=>'Update Question', 'url'=>array('update', 'id'=>$model->TM_QN_Id)),
    array('label'=>'Copy Question', 'url'=>array('copy', 'id'=>$model->TM_QN_Id)),    
    array('label'=>'Delete Question', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_QN_Id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>
<?php  if($model->TM_QN_Type_Id=='1'||$model->TM_QN_Type_Id=='2'):
$showoption=1;
?>
    <div class="panel  panel-yellow">
        <div class="panel-heading">
            <h3 class="panel-title">
                View Question
            </h3>
        </div>
        <div class="panel-body panelnopadding">
            <div class="row questborder">
                <div class="col-md-8">
                    <?php echo $model->TM_QN_Question; ?>
                </div>
                <?php if($model->TM_QN_Image!=''):?>
                <div class="media col-md-4">
                    <figure class="pull-left">                        
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$model->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >
                    </figure>
                </div>
                <?php endif;?>
            </div>
            <h3 class="list-group-item-heading"> Answer Options </h3>
            <?php foreach($model->answers AS $answer):?>
            <div class="row answborder">
                <div class="col-md-10">
                    <?php echo $answer->TM_AR_Answer; ?>
                </div>
                <?php if($answer->TM_AR_Marks!='0'):?>
                <div class="col-md-2 text-center">
                    <label class="mark">Marks</label>
                    <h2> <?php echo CHtml::encode($answer->TM_AR_Marks); ?></h2>
                </div>
                <?php endif;?>
                <?php if($answer->TM_AR_Image!=''):?>
                <div class="media col-md-4 pull-right">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=300&height=300';?>" >                        
                    </figure>
                </div>
                <?php endif;?>
            </div>

            <?php endforeach;?>
        </div>
    </div>
<?php elseif($model->TM_QN_Type_Id=='3'):
$showoption=1;
?>
    <div class="panel  panel-yellow">
        <div class="panel-heading">
            <h3 class="panel-title">
                View Question
            </h3>
        </div>
        <div class="panel-body panelnopadding">
            <div class="row questborder">
                <div class="col-md-8">
                    <?php echo $model->TM_QN_Question; ?>
                </div>
                <?php if($model->TM_QN_Image!=''):?>
                <div class="media col-md-4">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$model->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >                        
                    </figure>
                </div>
                <?php endif;?>
            </div>
            <h3 class="list-group-item-heading"> Answer Options </h3>
            <?php foreach($model->answers AS $answer):?>
            <div class="row answborder">
                <div class="col-md-10">
                    <?php echo $answer->TM_AR_Answer; ?>
                </div>
                <?php if($answer->TM_AR_Marks!='0'):?>
                <div class="col-md-2 text-center">
                    <label class="mark">Marks</label>
                    <h2> <?php echo CHtml::encode($answer->TM_AR_Marks); ?></h2>
                </div>
                <?php endif;?>
            </div>
            <?php endforeach;?>
        </div>
    </div>
<?php elseif($model->TM_QN_Type_Id=='4'):
$showoption=0;
?>
    <div class="panel  panel-yellow">
        <div class="panel-heading">
            <h3 class="panel-title">
                View Question
            </h3>
        </div>
        <div class="panel-body panelnopadding">
            <div class="row questborder">
                <div class="col-md-8">
                    <?php echo $model->TM_QN_Question; ?>
                </div>
                <?php if($model->TM_QN_Image!=''):?>
                <div class="media col-md-4">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$model->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >
                    </figure>
                </div>
                <?php endif;?>
            </div>
            <h3 class="list-group-item-heading"> Probable Answers </h3>
            <?php foreach($model->answers AS $answer):?>
            <div class="row answborder">
                <div class="col-md-10">
                    <?php echo $answer->TM_AR_Answer; ?>
                </div>
                <?php if($answer->TM_AR_Marks!='0'):?>
                <div class="col-md-2 text-center">
                    <label class="mark">Marks</label>
                    <h2> <?php echo CHtml::encode($answer->TM_AR_Marks); ?></h2>
                </div>
                <?php endif;?>
            </div>
            <?php endforeach;?>
        </div>
    </div>
<?php elseif($model->TM_QN_Type_Id=='5'):
$showoption=1;
    $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$model->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
    ?>
    <div class="panel  panel-yellow">
        <div class="panel-heading">
            <h3 class="panel-title">
                View Question
            </h3>
        </div>
        <div class="panel-body panelnopadding">
        <?php if($model->TM_QN_Question!=''):?>
        <div class="row questborder">
            <div class="col-md-8">
                <?php echo $model->TM_QN_Question; ?>
            </div>
            <?php if($model->TM_QN_Image!=''):?>
                <div class="media col-md-4">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$model->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >                        
                    </figure>
                </div>
            <?php endif;?>

        </div>
        <?php endif;
            foreach($questions AS $key=>$question):
                $number=$key+1;
                ?>
                <div class="row questborder">
                    <div class="col-md-8">
                        <h3 class="list-group-item-heading"> <?php echo "Question ".$number;?> </h3>
                        <?php echo $question->TM_QN_Question; ?>
                    </div>
                    <?php if($question->TM_QN_Image!=''):?>
                    <div class="media col-md-4">
                        <figure class="pull-left">
                            <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >                            
                        </figure>
                    </div>
                    <?php endif;?>
                </div>
                <?php foreach($question->answers AS $answer):?>
                <div class="row answborder">
                    <div class="col-md-10">
                        <?php echo $answer->TM_AR_Answer; ?>
                    </div>
                    <?php if($answer->TM_AR_Marks!='0'):?>
                    <div class="col-md-2 text-center">
                        <label class="mark">Marks</label>
                        <h2> <?php echo CHtml::encode($answer->TM_AR_Marks); ?></h2>
                    </div>
                    <?php endif;?>
                    <?php if($answer->TM_AR_Image!=''):?>
                    <div class="media col-md-4  pull-right">
                        <figure class="pull-left">
                            <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=300&height=300';?>" >                            
                        </figure>
                    </div>
                    <?php endif;?>
                </div>
                <?php endforeach;?>
        <?php endforeach;?>
        </div>
    </div>
<?php elseif($model->TM_QN_Type_Id=='6'):
$showoption=0;?>
    <div class="panel  panel-yellow">
        <div class="panel-heading">
            <h3 class="panel-title">
                View Question
            </h3>
        </div>
        <div class="panel-body panelnopadding">
            <div class="row questborder">
                <div class="col-md-8">
                    <?php echo $model->TM_QN_Question; ?>
                </div>
                <?php if($model->TM_QN_Image!=''):?>
                <div class="media col-md-4">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$model->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >                        
                    </figure>
                </div>
                <?php endif;?>
            </div>
            <div class="row answborder">
                <div class="col-md-2 text-center pull-right">
                    <label class="mark">Marks</label>
                    <h2> <?php echo CHtml::encode($model->TM_QN_Totalmarks); ?></h2>
                </div>
            </div>
        </div>
    </div>
<?php elseif($model->TM_QN_Type_Id=='7'):
$showoption=0;
    $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$model->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
    ?>
    <div class="panel  panel-yellow">
        <div class="panel-heading">
            <h3 class="panel-title">
                View Question
            </h3>
        </div>
        <div class="panel-body panelnopadding">
            <?php if($model->TM_QN_Question!=''):?>
                <div class="row questborder">
                    <div class="col-md-12">
                        <?php echo $model->TM_QN_Question; ?>
                    </div>
                </div>
            <?php endif;?>
            <?php if($model->TM_QN_Image!=''):?>
                <div class="media col-md-4">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$model->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >                        
                    </figure>
                </div>
            <?php endif;?>
            <?php foreach($questions AS $key=>$question):
            $number=$key+1;
            ?>
            <div class="row questborder">
                <div class="col-md-8">
                    <h3 class="list-group-item-heading"> <?php echo "Question ".$number;?> </h3>
                    <?php echo $question->TM_QN_Question; ?>
                </div>
                <?php if($question->TM_QN_Image!=''):?>
                <div class="media col-md-4">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=thumbs&type=question&width=300&height=300';?>" >                        
                    </figure>
                </div>
                <?php endif;?>
            </div>
            <div class="row answborder">
                <div class="col-md-2 text-center pull-right">
                    <label class="mark">Marks</label>
                    <h2> <?php echo CHtml::encode($question->TM_QN_Totalmarks); ?></h2>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
<?php endif;
if($model->TM_QN_Solutions!=''):
?>
<div class="panel  panel-yellow">
    <div class="panel-heading">
        <h3 class="panel-title">
            Solution
        </h3>
    </div>
    <div class="panel-body panelnopadding">
        <div class="row answborder">
            <div class="col-md-12">
                <?php echo $model->TM_QN_Solutions; ?>
            </div>
            <?php if($model->TM_QN_Solution_Image!=''):?>
                <div class="media col-md-4 pull-right">
                    <figure class="pull-left">
                        <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$model->TM_QN_Solution_Image.'?size=thumbs&type=solution&width=300&height=300';?>" >                        
                    </figure>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php endif;?>
    <div class="panel panel-yellow">
        <div class="panel-heading">
            <h3 class="panel-title">Question Options </h3>
        </div>
        <table class="table">
            <tr>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Publisher_Id')); ?></label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Publisher_Id!='0'?$model->publisher->TM_PR_Name:'')); ?></h4>
                </td>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Syllabus_Id')); ?> </label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Syllabus_Id!='0'?$model->syllabus->TM_SB_Name:'')); ?></h4>
                </td>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Standard_Id')); ?> </label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Standard_Id!='0'?$model->standard->TM_SD_Name:'')); ?></h4>
                </td>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Topic_Id')); ?> </label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Topic_Id!='0'?$model->chapter->TM_TP_Name:'')); ?></h4>
                </td>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Section_Id')); ?> </label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Section_Id!='0'?$model->topic->TM_SN_Name:'')); ?></h4>
                </td>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Dificulty_Id')); ?> </label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Dificulty_Id!='0'?$model->dificulty->TM_DF_Name:'')); ?></h4>
                </td>
            </tr>
            <tr>

                <td>
                    <label class="font18">Availabilty</label>
                    <?php foreach($availability AS $avail):?>
                    <h4><?php echo $avail['Name'];?></h4>
                    <?php endforeach;?>
                </td>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Teacher_Id')); ?> </label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Teacher_Id!='0'?$model->teachers->TM_TH_Name:'')); ?></h4>
                </td>
                <td>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Status')); ?> </label>
                    <h4> <?php echo CHtml::encode(Questions::itemAlias("QuestionStatus",$model->TM_QN_Status)); ?></h4>
                </td>
                <td><label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_QuestionReff')); ?> </label>
                    <h4> <?php echo CHtml::encode($model->TM_QN_QuestionReff); ?></h4>
                </td>
                <td>
                    <?php if($model->TM_QN_Pattern!=''):?>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Pattern')); ?> </label>
                    <h4> <?php echo CHtml::encode($model->TM_QN_Pattern); ?></h4>
                    <?php endif;?>
                </td>
                <td>
                    <?php if($model->TM_QN_Type_Id=='4'):?>
                        <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_Editor')); ?> </label>
                        <h4> <?php echo ($model->TM_QN_Editor?'Yes':'No'); ?></h4>
                    <?php endif;?></td>
            </tr>
            <tr>
                <?php if($showoption=1):?>                
                <td >
                    <label class="font18"><?php echo CHtml::encode('Print Options'); ?> </label>
                    <h4> <?php echo CHtml::encode(Questions::OptionAlias("OptionStatus",$model->TM_QN_Show_Option)); ?></h4>
                </td>                
                <?php endif;?>
                <td >
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_CreatedBy')); ?> </label>
                    <h4> <?php
                        $user=User::model()->findByPk($model->TM_QN_CreatedBy)->profile;
                        echo $user->firstname." ".$user->lastname;
                        ?></h4>

                </td>

                                
                <td >
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_CreatedOn')); ?> </label>
                    <h4> <?php echo CHtml::encode(date('d-m-Y',strtotime($model->TM_QN_CreatedOn))); ?></h4>
                </td>
                <td >
                    <?php if($model->TM_QN_UpdatedOn!='0000-00-00'):?>
                        <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_UpdatedOn')); ?> </label>
                        <h4> <?php echo CHtml::encode(date('d-m-Y',strtotime($model->TM_QN_UpdatedOn))); ?></h4>
                    <?php endif;?>
                </td>

                <td >
                    <?php if($model->TM_QN_UpdatedBy!='0'):?>
                    <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_QN_UpdatedBy')); ?> </label>
                    <h4> <?php
                        $user=User::model()->findByPk($model->TM_QN_UpdatedBy)->profile;
                        echo $user->firstname." ".$user->lastname;
                        ?></h4>
                    <?php endif;?>
                </td>
                <?php if($showoption=0):?>                
                <td > 
                </td>                
                <?php endif;?>                 
                <td >
                    <!--<form action="<?php /*echo Yii::app()->createUrl('questions/copy')*/?>" method="post">
                        <input type="hidden" name="questionid" value="<?php /*echo $model->TM_QN_Id;*/?>">
                    <button class="btn btn-warning btn-lg btn-block">Copy Question</button>
                    </form>-->
                </td>
            </tr>
            <tr>
                <td>
                    <label class="font18">Skill</label>
                    <?php if($model->TM_QN_Skill!=''){ ?>
                    <h4> <?php $skill_ids=explode(",",$model->TM_QN_Skill);
                    $skill_names=array();
                    foreach ($skill_ids as $key => $value) {
                    $skill_name=TmSkill::model()->findByPk($value)->tm_skill_name;
                    array_push($skill_names,$skill_name);
                    }
                     echo implode(",",$skill_names); ?></h4>
                    <?php } ?>
                </td>
                <td>
                    <label class="font18">Additional Info</label>
                    <?php if($model->TM_QN_Question_type!=''){ ?>
                    <h4><?php $type_ids=explode(",",$model->TM_QN_Question_type);
                    $qtypes=array();
                    foreach ($type_ids as $key => $value) {
                    $qtypename=QuestionType::model()->findByPk($value)->TM_Question_Type;
                    array_push($qtypes,$qtypename);
                    }
                     echo implode(",",$qtypes); 
                       ?></h4>
                <?php } ?>
                </td>

                 <td>
                    <label class="font18">Question Info</label>
                    <h4> <?php echo CHtml::encode(($model->TM_QN_Info!=''?$model->TM_QN_Info:'')); ?></h4>
                </td>
                <td colspan="3"></td>
            </tr>
        </table>
    </div>