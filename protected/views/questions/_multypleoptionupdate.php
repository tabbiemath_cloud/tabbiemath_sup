<div class="col-lg-12 mater_bg_border">

    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2'));
        ?>

        <textarea rows="6" class="form-control" name="Questions[TM_QN_Questionchoice]" id="Questions_TM_QN_Questionchoice">
            <?php echo ($model->TM_QN_Type_Id=='1'|| $model->TM_QN_Type_Id=='2'?CHtml::encode($model->TM_QN_Question):''); ?>
        </textarea>
        <?php
        echo $form->error($model,'TM_QN_Question');
        ?>
    </div>
    <div class="form-group">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="Imagechoice" id="Questions_TM_QN_Image" data-type="question" data-section="main" data-name="Imagechoice"  data-main="questionimagediv<?php echo $model->TM_QN_Id;?>" class="imageselect" type="file">
                     </span>

            <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>

    <div class="row">
        <?php if($model->TM_QN_Image!=''):?>
        <div class="media col-md-3" id="questionimagediv<?php echo $model->TM_QN_Id;?>">
            <figure class="pull-left imagespan">
                <div class="deletespan" data-id="<?php echo $model->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $model->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
            </figure>
        </div>
        <?php endif;?>
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImageDiv">
            <figure class="pull-left previmagespan">
                <div class="previewdeletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageDiv" data-file="Questions_TM_QN_Image" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePreview" src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
            </figure>
        </div>
        <div class="col-lg-3 options">
            <label for="Questions_TM_QN_Option_Countchoice">No. Of Options<span class="required">*</span></label>
            <select class="form-control" name="Questions[TM_QN_Option_Countchoice]" id="Questions_TM_QN_Option_Countchoice">
                <option value="2" <?php echo ($model->TM_QN_Option_Count=='2'?'selected="selected"':'');?>>2</option>
                <option value="3" <?php echo ($model->TM_QN_Option_Count=='3'?'selected="selected"':'');?>>3</option>
                <option value="4" <?php echo ($model->TM_QN_Option_Count=='4'?'selected="selected"':'');?>>4</option>
                <option value="5" <?php echo ($model->TM_QN_Option_Count=='5'?'selected="selected"':'');?>>5</option>
            </select>
            <?php echo $form->error($model,'TM_QN_Option_Count'); ?>
            <input type="hidden" class="selectedcount" value="<?php echo $model->TM_QN_Option_Count;?>">
        </div>

    </div>

</div>
<div class="col-lg-12 mater_bg_border">
    <h3>Answer Options</h3>
    <div id="answeroptions">
    <?php foreach($model->answers AS $key=>$answer):
        $identifier=$key+1;
        ?>
        <div class="row row_with_brd option<?php echo $identifier;?>">
            <div class="col-lg-10">
                <label >Option <?php echo $identifier;?></label>
                <textarea rows="4" class="form-control answer marbott " id="choiceanswer<?php echo $identifier;?>" name="answer[]" >
                  <?php echo CHtml::encode($answer->TM_AR_Answer);?>
                </textarea>
            </div>
            <div class="col-lg-2 topmarg">
                <label class="mark">marks</label>
                <input type="text" name="marks<?php echo $identifier;?>" id="choicemark<?php echo $identifier;?>" value="<?php echo ($answer->TM_AR_Correct?$answer->TM_AR_Marks:'');?>" class="form-control marks markdisply choicemark <?php echo ($answer->TM_AR_Correct?'correctmark':'');?>">
            </div>
            <div class="col-lg-12">
                <div class="col-lg-5">
                     <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input type="file" name="anserimage<?php echo $identifier;?>" data-type="answer" data-section="<?php echo $identifier;?>" data-name="anserimage<?php echo $identifier;?>"  data-main="answerimagediv<?php echo $answer->TM_AR_Id;?>" id="anserimage<?php echo $identifier;?>" class="answerimage imageselect">

                     </span>
                </div>
                <div class="col-lg-3">
                    <input type="<?php echo ($model->TM_QN_Type_Id=='1'?'checkbox':'radio');?>" <?php echo ($answer->TM_AR_Correct?'checked':'');?> name="<?php echo ($model->TM_QN_Type_Id=='1'?'correctanswer'.$identifier:'correctanswer');?>" value="<?php echo ($model->TM_QN_Type_Id=='1'?'1':$identifier);?>" id="correctacswerchoice<?php echo $identifier;?>" class="correctacswer correctacswerchoice">
                    <input type="hidden" name="answerid<?php echo $identifier;?>" value="<?php echo $answer->TM_AR_Id;?>">
                    <input type="hidden" id="answerimagehidden<?php echo $identifier;?>" value="<?php echo $answer->TM_AR_Image;?>">
                    <input type="hidden" id="checkanswer<?php echo $identifier;?>" value="<?php echo ($answer->TM_AR_Image!=''?'true':'false');?>">
                    <label>Tick for correct answer</label>
                </div>
            </div>
            <div class="col-lg-12">
                <?php if($answer->TM_AR_Image!=''):?>
                    <div class="media col-md-3" id="answerimagediv<?php echo $answer->TM_AR_Id;?>">
                        <figure class="pull-left imagespan">
                            <div class="deletespan" data-type="answer" data-section="<?php echo $identifier;?>" data-id="<?php echo $answer->TM_AR_Id;?>" data-parent="answerimagediv<?php echo $answer->TM_AR_Id;?>" data-type="Answer" ><span class="glyphicon glyphicon-trash"> </span></div>
                            <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->baseUrl.'/images/answerimages/thumbs/'.$answer->TM_AR_Image;?>" alt="placehold.it/350x250">
                        </figure>
                    </div>
                <?php endif;?>
                <div class="media col-md-3 imagepreview" id="anserimage<?php echo $identifier;?>Div">
                    <figure class="pull-left previmagespan">
                        <div class="previewdeletespan" data-type="answer" data-section="<?php echo $identifier;?>"  data-parent="anserimage<?php echo $identifier;?>Div" data-file="anserimage<?php echo $identifier;?>" ><span class="glyphicon glyphicon-trash"> </span></div>
                        <img class="media-object img-rounded img-responsive" id="anserimage<?php echo $identifier;?>Preview" src="" alt="placehold.it/350x250">
                    </figure>
                </div>
            </div>
        </div>
    <?php endforeach;?>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript('answeroptions', "
        $('#Questions_TM_QN_Option_Countchoice').change(function(){
            var count=($(this).val()*1);
            var prevcount=($('.selectedcount').val()*1);
            var newcount=0;
            if(count>prevcount)
            {
                newcount=count-prevcount;
                for(i=1;i<=newcount;i++)
                {
                    optioncount=prevcount+i
                    var answerhtml=getanswerhtmlmultychoice(optioncount)
                    $('#answeroptions').append(answerhtml);
                    $( '#choiceanswer'+optioncount ).ckeditor();
                }
            }
            else if(prevcount>count)
            {

                newcount=prevcount-count;

                for(i=prevcount;i>count;i--)
                {
                      $('.option'+i).remove();
                }
            }
            $('.selectedcount').val(count)
        })
    ");
?>
<script type="text/javascript">
    function getanswerhtmlmultychoice(count)
    {
        var type=$('.typeselect:checked').val();

        var fieldtype='';
        var fieldname='';
        var fieldvalue='';
        if(type=='1'){
            fieldtype='checkbox';
            fieldname='correctanswer'+count;
            fieldvalue='1'

        }
        else if(type=='2')
        {
            fieldtype='radio'
            fieldname='correctanswer';
            fieldvalue=count;
        }
        var answertemp='<div class="row row_with_brd option'+count+'">' +
            '<div class="col-lg-10">' +
            '<label >Option '+count+'</label>' +
            '<textarea rows="4" class="form-control answer marbott choiceanswer" id="choiceanswer'+count+'" name="answer[]" ></textarea>' +
            '</div>' +
            '<div class="col-lg-2 topmarg">' +
            '<label class="mark">marks</label>' +
            '<input type="text" disabled="disabled" name="marks'+count+'" id="choicemark'+count+'" class="form-control marks markdisply choicemark">' +
            '</div>' +
            '<div class="col-lg-12">' +
                '<div class="col-lg-8">' +
                '<span class="btn btn-default btn-file">' +
                '<i class="fa fa-picture-o fa-lg yellow"></i>' +
                'Upload Image <input type="file" name="anserimage'+count+'" data-type="answer" data-section="'+count+'" data-name="anserimage'+count+'"  id="anserimage'+count+'" class="answerimage imageselect"></span>' +
                '<input type="hidden" id="checkanswer'+count+'" value="false">'+
                '</div>' +
                '<div class="col-lg-3">' +
                '<input type="'+fieldtype+'"  name="'+fieldname+'" value="'+fieldvalue+'" id="correctacswerchoice'+count+'" class="correctacswer correctacswerchoice">' +
                '<label>Tick for correct answer</label>' +
                '</div>' +
            '</div>' +
            '<div class="col-lg-12">' +
                '<div class="media col-md-3 imagepreview" id="anserimage'+count+'Div">' +
                '<figure class="pull-left previmagespan">' +
                '<div class="previewdeletespan" data-type="answer" data-section="'+count+'" data-parent="anserimage'+count+'Div" data-file="anserimage'+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>' +
                '<img class="media-object img-rounded img-responsive" id="anserimage'+count+'Preview" src="http://placehold.it/100x100" alt="placehold.it/350x250">' +
                '</figure>' +
                '</div>'+
            '</div>' +
            '</div>';
        return answertemp;
    }

</script>



