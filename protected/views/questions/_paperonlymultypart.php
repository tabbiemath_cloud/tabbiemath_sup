<div class="col-lg-12 mater_bg_border">
    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_QuestionMPPaperOnly]" id="Questions_TM_QN_QuestionMPPaperOnly"></textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="form-group">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImagePaperPart" data-type="question" data-section="main" data-name="TM_QN_ImagePaperPart" class="imageselect" id="Questions_TM_QN_ImagePaperPart" type="file">
                     </span>

        <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePaperPartDiv">
            <figure class="pull-left imagespan">
                <div class="deletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImagePaperPartDiv" data-file="Questions_TM_QN_ImagePaperPart" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePaperPartPreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
        <div class="col-lg-3 options">
            <label>No. Of Parts</label>
            <select class="form-control" id="PoMPqstnum" name="PoMPqstnum">
                <option selected="selected" value="0">Select</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
            <input type="hidden" class="selectePoMPqstnum" value="0">
        </div>

    </div>
</div>
<div id="papermulpartqstarea">

</div>
<?php
    Yii::app()->clientScript->registerScript('papermultypart', "
        $('#PoMPqstnum').change(function(){
                var count=($(this).val()*1);
                var prevcount=($('.selectePoMPqstnum').val()*1);
                var newcount=0;
                if(count!='0')
                {

                    if(count>prevcount)
                    {

                        newcount=count-prevcount;
                        for(i=1;i<=newcount;i++)
                        {
                            optioncount=prevcount+i
                            var questionhtml=getQuestionpaper(optioncount)
                            $('#papermulpartqstarea').append(questionhtml);
                            $('#PapermultypartQuestions'+i).ckeditor();
                        }
                    }
                    else if(prevcount>count)
                    {

                        newcount=prevcount-count;

                        for(i=prevcount;i>count;i--)
                        {
                              $('.paperquestioncount'+i).remove();
                        }
                    }
                }
                else
                {
                   $('#papermulpartqstarea').html('')
                }
                 $('.selectePoMPqstnum').val(count)

         });
    ");
?>
<script type="text/javascript">
    function getQuestionpaper(count)
    {
        var questionhtml='<div class="col-lg-12 mater_bg_border paperquestioncount'+count+'">' +
            '<div class="form-group">' +
            '<label for="Questions_Enter_Question" class="font2">Enter  Question '+count+'</label>' +
            '<textarea rows="6" class="form-control PapermultypartQuestions" name="PapermultypartQuestions[]" id="PapermultypartQuestions'+count+'" ></textarea>' +
            '</div>'+
            '<div class="row">' +
                '<div class="col-lg-2"><span class="btn btn-default btn-file">' +
                '<i class="fa fa-picture-o fa-lg yellow"></i>Upload Image' +
                '<input name="PapermultypartQuestionsImage'+count+'" data-type="question" data-section="'+count+'" data-name="PapermultypartQuestionsImage'+count+'" id="Questions_TM_QN_ImagePaperPart'+count+'" class="imageselect" type="file">' +
                '</span>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label class="mark">marks</label>' +
                '<input type="text" name="PaperMPmarks[]"  id="PaperMPmarks'+count+'"   class="form-control marks PaperMPmarks'+count+'">' +
                '</div>' +
                '<div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePaperPart'+count+'Div">'+
                '<figure class="pull-left imagespan">' +
                '<div class="deletespan" data-type="question" data-section="'+count+'" data-parent="Questions_TM_QN_ImagePaperPart'+count+'Div" data-file="Questions_TM_QN_ImagePaperPart'+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>' +
                '<img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePaperPart'+count+'Preview" src="" alt="placehold.it/350x250"></figure>' +
                '</div>'+
            '</div>' +
            '</div>' +
            '</div>';
        return questionhtml
    }
</script>