<div class="col-lg-12 mater_bg_border">
    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_QuestionTF]" id="Questions_TM_QN_QuestionTF">
            <?php echo ($model->TM_QN_Type_Id=='3'?CHtml::encode($model->TM_QN_Question):''); ?>
        </textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="form-group">
                    <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImageTF" data-type="question" data-section="main" data-name="TM_QN_ImageTF" data-main="questionimagediv<?php echo $model->TM_QN_Id;?>" id="Questions_TM_QN_Image"  class="imageselect"  type="file">
                     </span>

        <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImageDiv">
            <figure class="pull-left previmagespan">
                <div class="previewdeletespan" data-type="question" data-section="main"  data-parent="Questions_TM_QN_ImageDiv" data-file="Questions_TM_QN_ImageImage" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
        <?php if($model->TM_QN_Image!=''):?>
            <div class="media col-md-3" id="questionimagediv<?php echo $model->TM_QN_Id;?>">
                <figure class="pull-left imagespan">
                    <div class="deletespan" data-type="question" data-section="main"  data-id="<?php echo $model->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $model->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                    <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
                </figure>
            </div>
        <?php endif;?>
    </div>
</div>
<div class="col-lg-12 mater_bg_border">
    <h3>Answer Options</h3>
    <div class="row row_with_brd option1" id="TFoption">
        <?php foreach($model->answers AS $key=>$answer):
        $identifier=$key+1;
        if($answer->TM_AR_Correct):
            $marks=$answer->TM_AR_Marks;
        endif;
        ?>
        <div class="col-lg-2 marginfive">
            <input type="radio" name="correctanswerTF" class="correctanswerTF" value="<?php echo ($key=='0'?'True':'False');?>" <?php echo ($answer->TM_AR_Correct?'checked':'');?>><label><?php echo $answer->TM_AR_Answer;?></label>
            <input type="hidden" name="answeridTF<?php echo $identifier;?>" value="<?php echo $answer->TM_AR_Id;?>">
        </div>
        <?php endforeach;?>
        <div class="col-lg-2 topmarg">
            <label  class="mark">marks</label>
            <input type="text" name="marksTF" id="marksTF" value="<?php echo $marks;?>"  class="form-control marks">
        </div>
        <div class="col-lg-8">
            <label>Please select correct answer for True Or False</label>
        </div>

    </div>
</div>