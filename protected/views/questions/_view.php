<?php
/* @var $this QuestionsController */
/* @var $data Questions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_QN_Id), array('view', 'id'=>$data->TM_QN_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Parent_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Parent_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Publisher_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Publisher_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Syllabus_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Syllabus_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Standard_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Standard_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Subject_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Subject_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Topic_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Topic_Id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Section_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Section_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Dificulty_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Dificulty_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Type_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Type_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Question')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Question); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Image')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Answer_Type')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Answer_Type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Option_Count')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Option_Count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Editor')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Editor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Totalmarks')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Totalmarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_QN_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_QN_Status); ?>
	<br />

	*/ ?>

</div>