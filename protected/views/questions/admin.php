<style>
/*.math span
{
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
    font-style: normal !important;
    font-size: 15px;
}*/
#questions-grid
{
    font-style: normal !important;
    font-family: STIXGeneral normal !important;
    font-size: 16px;
    /*color: #333333e0;*/
}
</style>
<?php
/* @var $this QuestionsController */
/* @var $model Questions */

$this->breadcrumbs=array(
    'Questions'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
    array('label'=>'List Questions', 'url'=>array('admin')),
    array('label'=>'Build Questions', 'url'=>array('Importcsv')),
    array('label'=>'Import Questions', 'url'=>array('importtosyllabus')),
    array('label'=>'Export Questions', 'url'=>array('exporttosyllabus')),
    array('label'=>'Create Question', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#questions-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
$standards=$model->GetAdminStandards();

?>

<h3>Manage Questions</h3>
<div class="row brd1" style="width: 250%">
    <div class="col-lg-12">
        <div class="col-lg-3" style="margin-bottom: 10px;">
            <div class="col-sm-4">
                <?php echo CHtml::dropDownList('changestatus', 0,Questions::itemAlias('QuestionStatus'),array('prompt'=>'select status','class'=>'form-control' ));?>
            </div>
            <div class="col-sm-4">
                <?php
                echo CHtml::ajaxLink("Change Status", $this->createUrl('questions/ChangeStatus'), array(
                    "type" => "post",
                    "data" => 'js:{theIds : $.fn.yiiGridView.getChecked("questions-grid","question").toString(),"status":$("#changestatus").val()}',
                    "success" => 'js:function(data){ $.fn.yiiGridView.update("questions-grid")  }' ),array(
                        'class' => 'btn btn-warning btn-block'
                    )
                );
                ?>
            </div>
            <?php /*
            <div class="col-sm-4">
                <?php
                echo CHtml::ajaxLink("Export Questions", $this->createUrl('questions/exportquestions'), array(
                    "type" => "post",
                    "data" => 'js:{theIds : $.fn.yiiGridView.getChecked("questions-grid","question").toString()}',
                    "success" => 'js:function(data){ window.location = data }' ),array(
                        'class' => 'btn btn-warning btn-block'
                    )
                );
                ?>
            </div>*/?>
        </div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'questions-grid',
    'dataProvider'=>$model->search(),
        'pager' => array('maxButtonCount' => 5),
    'filter'=>$model,
    'selectableRows'=>2,
    'columns'=>array(
        array(
            'class'=>'CCheckBoxColumn',
            'name'=>'question',
            'value'=>'$data->TM_QN_Id',
            'id'=>'question'
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{copy}{view}{update}{delete}',
            //'template'=>'{view}{delete}',
            'deleteButtonImageUrl'=>false,
            'updateButtonImageUrl'=>false,
            'viewButtonImageUrl'=>false,
            'buttons'=>array
            (
                'copy'=>array(
                    'label'=>'<span class="glyphicon glyphicon-copy"></span>',
                    'url'=>'Yii::app()->createUrl("questions/copy", array("id"=>$data->TM_QN_Id))',
                    'options'=>array(
                        'title'=>'Copy',
                    )),
                'view'=>array(
                    'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                    'options'=>array(
                        'title'=>'View',
                    )),
                'update'=>array(
                    'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                    'options'=>array(
                        'title'=>'Update'
                    )),
                'delete'=>array(
                    'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                    'options'=>array(
                        'title'=>'Delete',
                    ),
                )
            ),
        ),
        'TM_QN_Id',
        'TM_QN_QuestionReff',
        'TM_QN_Pattern',
        array(
            'name'=>'TM_QN_Question',
            'type'=>'raw',
            'value'=>'Questions::model()->GetQuestion($data->TM_QN_Id)',
        ),
        array(
            'name'=>'TM_QN_Status',
            'value'=>'Questions::itemAlias("QuestionStatus",$data->TM_QN_Status)',
            'filter'=>Questions::itemAlias('QuestionStatus'),
        ),
        array(
            'name'=>'TM_QN_Show_Option',
            'value'=>'Questions::OptionAlias("OptionStatus",$data->TM_QN_Show_Option)',
            'filter'=>Questions::OptionAlias('OptionStatus'),
        ),        
        array(
            'name'=>'TM_QN_Syllabus_Id',
            'type' => 'raw',
            'value'=>'($data->TM_QN_Syllabus_Id!="0")?Syllabus::model()->findByPk($data->TM_QN_Syllabus_Id)->TM_SB_Name:""',
            'filter'=>CHtml::listData(Syllabus::model()->findAll(array('condition' => "TM_SB_Status='0'",'order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),

        ),
        array(
            'name'=>'TM_QN_Standard_Id',
            'value'=>'($data->TM_QN_Standard_Id!="0")?Standard::model()->findByPk($data->TM_QN_Standard_Id)->TM_SD_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_Syllabus_Id) ? Standard::model()->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))->findAll(new CDbCriteria(array(
                        'condition' => "TM_SD_Syllabus_Id = :syllabusid AND TM_SD_Status='0'",
                        'params' => array(':syllabusid' => $model->TM_QN_Syllabus_Id),
                        'order' => 'TM_SD_Id'
                    ))) : Standard::model()->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))->findAll(array('condition' => "TM_SD_Status='0'",'order' => 'TM_SD_Id')),'TM_SD_Id','TM_SD_Name'),

        ),
        array(
            'name'=>'TM_QN_Topic_Id',
            'value'=>'($data->TM_QN_Topic_Id!="0")?Chapter::model()->findByPk($data->TM_QN_Topic_Id)->TM_TP_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_Standard_Id) ? Chapter::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_TP_Standard_Id = :standard AND TM_TP_Status='0'",
                        'params' => array(':standard' => $model->TM_QN_Standard_Id),
                        'order' => 'TM_TP_order ASC '
                    ))) : Chapter::model()->findAll(array('condition' => "TM_TP_Status='0'  AND TM_TP_Standard_Id IN(".$standards.")",'order' => ' TM_TP_order ASC')),'TM_TP_Id','TM_TP_Name'),
        ),
        array(
            'name'=>'TM_QN_Section_Id',
            'value'=>'($data->TM_QN_Section_Id!="0")?Topic::model()->findByPk($data->TM_QN_Section_Id)->TM_SN_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_Topic_Id) ? Topic::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_SN_Topic_Id = :chapter AND TM_SN_Status='0'",
                        'params' => array(':chapter' => $model->TM_QN_Topic_Id),
                        'order' => 'TM_SN_order ASC'
                    ))) : Topic::model()->findAll(array('condition' => "TM_SN_Status='0' AND TM_SN_Standard_Id IN (".$standards.")",'order' => 'TM_SN_order ASC')),'TM_SN_Id','TM_SN_Name'),
        ),


        array(
            'name'=>'TM_QN_Dificulty_Id',
            'value'=>'($data->TM_QN_Dificulty_Id!="0")?Difficulty::model()->findByPk($data->TM_QN_Dificulty_Id)->TM_DF_Name:""',
            'filter'=>CHtml::listData(Difficulty::model()->findAll(array('order' => 'TM_DF_Name')),'TM_DF_Id','TM_DF_Name'),

        ),
        array(
            'name'=>'TM_QN_Type_Id',
            'value'=>'($data->TM_QN_Type_Id!="0")?Types::model()->findByPk($data->TM_QN_Type_Id)->TM_TP_Name:""',
            'filter'=>CHtml::listData(Types::model()->findAll(array('order' => 'TM_TP_Name')),'TM_TP_Id','TM_TP_Name'),

        ),

        array(
            'name'=>'TM_QN_School_Id',
            'value'=>'($data->TM_QN_School_Id!="0")?School::model()->findByPk($data->TM_QN_School_Id)->TM_SCL_Name:"System"',
            'filter'=>array('0'=>'All','1'=>'System') + CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Id')),'TM_SCL_Id','TM_SCL_Name'),

        ),



         array(
            'name'=>'TM_QN_Teacher_Id',
            'value'=>'($data->TM_QN_Teacher_Id!="0")?Teachers::model()->findByPk($data->TM_QN_Teacher_Id)->TM_TH_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_School_Id) ? Teachers::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_TH_SchoolId = :teacher ",
                        'params' => array(':teacher' => $model->TM_QN_School_Id),
                        'order' => 'TM_TH_Name ASC'
                    ))) : Teachers::model()->findAll(array('condition' => "TM_TH_UserType='1'")),'TM_TH_Id','TM_TH_Name'),
        ),
         
        array(
            'name'=>'TM_QN_CreatedBy',
            'value'=>'Questions::model()->GetUser($data->TM_QN_CreatedBy)',
            'filter'=> CHtml::dropDownList('Questions[TM_QN_CreatedBy]', $model->TM_QN_CreatedBy ? $model->TM_QN_CreatedBy:'0',Questions::model()->GetquestionCreatorList($model->TM_QN_School_Id),array('prompt'=>'')),
        ),
        array(
            'name'=>'TM_QN_CreatedOn',
            'type'=>'raw',
            'value'=>'Questions::model()->GetCretedate($data->TM_QN_CreatedOn)',
            'filter'=>false,
        ),
        array(
            'name'=>'TM_QN_UpdatedBy',
           'value'=>'($data->TM_QN_UpdatedBy!="0")?Questions::model()->GetUser($data->TM_QN_UpdatedBy):""',
            'filter'=> CHtml::dropDownList('Questions[TM_QN_UpdatedBy]', $model->TM_QN_UpdatedBy ? $model->TM_QN_UpdatedBy:'0',Questions::model()->GetquestionUpdatorList($model->TM_QN_School_Id),array('prompt'=>'')),
        ),
        array(
            'name'=>'TM_QN_UpdatedOn',
            'type'=>'raw',
            'value'=>'Questions::model()->GetCretedate($data->TM_QN_UpdatedOn)',
            //'value'=>'($data->TM_QN_UpdatedOn!="0000-00-00")?Yii::app()->dateFormatter->format("d MMM y",strtotime($data->TM_QN_UpdatedOn)):""',
            'filter'=>false,
        ),
        array(
            'name'=>'TM_QN_Publisher_Id',
            'type'=>'raw',
            'value'=>'($data->TM_QN_Publisher_Id!="0")?Publishers::model()->findByPk($data->TM_QN_Publisher_Id)->TM_PR_Name:""',
            'filter'=>CHtml::listData(Publishers::model()->findAll(array('order' => 'TM_PR_Id')),'TM_PR_Id','TM_PR_Name'),
        ),

            array(
            'name'=>'TM_QN_Skill',
            'type'=>'raw',
            'value'=>function($model)
             {
                $skill_ids=explode(",",$model->TM_QN_Skill);
                $skill_names=array();
                foreach($skill_ids as $skill_id)
                {
                    $skillname=TmSkill::model()->findByPk($skill_id)->tm_skill_name;
                    array_push($skill_names,$skillname);
                }
                $skill_name=implode(", ",$skill_names);

                return $skill_name; 
                
             },
            'filter'=>CHtml::listData(TmSkill::model()->findAll(array(  
             'condition' => "tm_standard_id = :standard",
                        'params' => array(':standard' => $model->TM_QN_Standard_Id),'order' => 'id')),'id','tm_skill_name'),
        ),
        array(
            'name'=>'TM_QN_Question_type',
            'type'=>'raw',
           //'value'=>'($data->TM_QN_Question_type!="0")?QuestionType::model()->findByPk($data->TM_QN_Question_type)->TM_Question_Type:""',
            'value'=>function($model)
             {
                $type_ids=explode(",",$model->TM_QN_Question_type);
                $type_names=array();
                foreach($type_ids as $type_id)
                {
                    $typename=QuestionType::model()->findByPk($type_id)->TM_Question_Type;
                    array_push($type_names,$typename);
                }
                $type_name=implode(", ",$type_names);

                return $type_name; 
                
             },
            'filter'=>CHtml::listData(QuestionType::model()->findAll(array( 
                'condition' => "tm_standard_id = :standard",
                        'params' => array(':standard' => $model->TM_QN_Standard_Id),'order' => 'id')),'id','TM_Question_Type'),
        ),
        'TM_QN_Info',
        /*
        // for standard admin filter removed code
        ->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))
                    
        // filter ends here
        array(
            'name'=>'TM_QN_Question',
            'value'=>'strip_tags($data->TM_QN_Question)',

        ),*/
/*        array(
            'name'=>'TM_QN_Subject_Id',
            'value'=>'Subject::model()->findByPk($data->TM_QN_Subject_Id)->TM_ST_Name',
            'filter'=>CHtml::listData(Subject::model()->findAll(array('order' => 'TM_ST_Name')),'TM_ST_Id','TM_ST_Name'),
        ),
        'TM_QN_Topic_Id',
        'TM_QN_Section_Id',
        'TM_QN_Dificulty_Id',
        'TM_QN_Type_Id',
        'TM_QN_Question',
        'TM_QN_Image',
        'TM_QN_Answer_Type',
        'TM_QN_Option_Count',
        'TM_QN_Editor',
        'TM_QN_Totalmarks',
        'TM_QN_CreatedOn',
        'TM_QN_CreatedBy',
        'TM_QN_Status',
        */

    ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
)); ?>

    </div>

</div>
    <?php 
    Yii::app()->clientScript->registerScript('popup', "
    $(document).on('click','.showpopup',function(){     
        var id=$(this).attr('data-id');
        
        if($('#preview'+id).is(':visible'))
        {
                $('.popup').hide();
        }
        else
        {
                $('.popup').hide();
                $('#preview'+id).fadeIn();
        }
        
        return false;
    })
    ");
    ?>
<script type="text/javascript">
    $( document ).ajaxComplete(function() { 
      MathJax.Hub.Queue(["Typeset",MathJax.Hub, "questions-grid"]);
    });
</script>