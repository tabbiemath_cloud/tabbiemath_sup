<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
    'Questions'=>array('admin'),
    'Create',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
    array('label'=>'List Questions', 'url'=>array('admin')),
    array('label'=>'Create Question', 'url'=>array('create')),
    array('label'=>'View Question', 'url'=>array('view', 'id'=>$model->TM_QN_Id)),
    array('label'=>'Copy Question', 'url'=>array('copy', 'id'=>$model->TM_QN_Id)),    
    //array('label'=>'Copy Question', 'url'=>'#', 'linkOptions'=>array('submit'=>array('copy','id'=>$model->TM_QN_Id))),
    array('label'=>'Delete Question', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_QN_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3>Update Question <?php echo $model->TM_QN_Id; ?></h3>

<?php $this->renderPartial('_formupdate', array('model'=>$model,'availability'=>$availability)); ?>