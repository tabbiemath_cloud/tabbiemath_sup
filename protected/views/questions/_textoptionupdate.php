<div class="col-lg-12 mater_bg_border">
    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_QuestionTEXT]" id="Questions_TM_QN_QuestionTEXT">
            <?php echo ($model->TM_QN_Type_Id=='4'?CHtml::encode($model->TM_QN_Question):''); ?>
        </textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>

    <div class="form-group">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImageTEXT"  data-type="question" data-section="main" data-name="TM_QN_ImageTEXT" class="imageselect"  data-main="questionimagediv<?php echo $model->TM_QN_Id;?>" id="Questions_TM_QN_ImageTEXT" type="file">
                     </span>

        <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImageTEXTDiv">
            <figure class="pull-left previmagespan">
                <div class="previewdeletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageTEXTDiv" data-file="Questions_TM_QN_ImageTEXT" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImageTEXTPreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
        <?php if($model->TM_QN_Image!=''):?>
            <div class="media col-md-3" id="questionimagediv<?php echo $model->TM_QN_Id;?>">
                <figure class="pull-left imagespan">
                    <div class="deletespan" data-type="question" data-section="main" data-id="<?php echo $model->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $model->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                    <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
                </figure>
            </div>
        <?php endif;
       $answer=$model->answers[0];
        ?>
    </div>
</div>
<div class="col-lg-12 mater_bg_border">
    <h3>Answer Options</h3>
    <div class="row row_with_brd option1">
        <div class="col-lg-10">
            <label >Please separate all probable solutions by '<b>;</b>'</label>
            <textarea rows="4" class="form-control answer marbott" name="answeroptions" id="answeroptionsTEXT" >
                <?php echo CHtml::encode($answer->TM_AR_Answer);?>
            </textarea>
        </div>
        <div class="col-lg-2 topmarg">
            <label class="mark">marks</label>
            <input type="text" name="marksTEXT" value="<?php echo $answer->TM_AR_Marks;?>" id="marksTEXT" class="form-control marks markdisply">
            <input type="hidden" name="answeridTEXT" value="<?php echo $answer->TM_AR_Id;?>">
        </div>
        <!--<div class="col-lg-4 pull-right">
            <?php /*echo $form->checkBox($model,'TM_QN_Editor',array('size'=>60,'maxlength'=>250)); */?>
            <?php /*echo $form->labelEx($model,'Click To enable editor for student answering'); */?>
        </div>-->
    </div>

</div>