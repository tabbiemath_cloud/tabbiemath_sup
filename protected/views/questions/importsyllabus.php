<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
    'Questions'=>array('admin'),
    'Create',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
    array('label'=>'List Questions', 'url'=>array('admin')),
    array('label'=>'Create Question', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('questions/admin')."';
    });
");
?>

<h3>Import Syllabus Questions</h3>
<div class="row brd1">
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body form-horizontal payment-form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select CSV <span class="required">*</span></label>
                <div class="col-sm-4">
                    <input type="file" name="importcsv" class="form-control" id="importcsv">
                    <span class="info">Maximum number of rows in a csv is 500</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 pull-left"> </div>
                <div class="col-sm-3 pull-left">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                <div class="col-sm-3 pull-left">
                    <?php echo CHtml::submitButton('Upload',array('class'=>'btn btn-warning btn-lg btn-block','name'=>'submit')); ?>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
    if(count($importlog)>0):
    ?>
<div class="panel panel-yellow addedqstable">
        <div class="panel-heading">
            <h3 class="panel-title">Import Status</h3>
        </div>
        <div class="scrol-body">
            <table class="table table-striped table-bordered ">
                <thead>
                    <th>Row #</th>
                    <th>Import Status</th>
                    <th>Notes</th>
                </thead>
                <tbody>
                <?php for($i=1;$i<=count($importlog);$i++):?>
                    <tr><td><?php echo $i;?></td><td><?php echo ($importlog[$i]['question']=='0'?'Question Import Failed': 'Question Import Successful');?></td><td><?php echo $importlog[$i]['status'];?></td></tr>
                <?php endfor;?>
                </tbody>
            </table>
        </div>
</div>
<?php endif;?>