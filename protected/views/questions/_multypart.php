<div class="col-lg-12 mater_bg_border">

    <div class="form-group">
        <?php echo $form->labelEx($model,'Question Title',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control Questions_TM_QN_QuestionMP" name="Questions[TM_QN_Questionpart]" id="Questions_TM_QN_Question"></textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>

    <div class="form-group">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImagePart" data-type="question" data-section="main" data-name="TM_QN_ImagePart"  id="Questions_TM_QN_ImagePart" class="imageselect"  type="file">
                     </span>

        <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePartDiv">
            <figure class="pull-left imagespan">
                <div class="deletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImagePartDiv" data-file="Questions_TM_QN_ImagePart" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePartPreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
        <div class="col-lg-3 options">
            <label>No. Of Parts</label>
            <select class="form-control" id="multipleqstnum" name="multipleqstnum">
                <option selected="selected" value="0">Select</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
            <input type="hidden" class="selecteqstdcount" value="0">
        </div>

    </div>
</div>

<div id="mulpartqstarea">

</div>
<?php
Yii::app()->clientScript->registerScript('multypart', "
         $('#multipleqstnum').change(function(){
                var count=($(this).val()*1);
                var prevcount=($('.selecteqstdcount').val()*1);
                var newcount=0;
                if(count!='0')
                {

                    if(count>prevcount)
                    {

                        newcount=count-prevcount;
                        for(i=1;i<=newcount;i++)
                        {
                            optioncount=prevcount+i
                            var editorname='multypartQuestions'+i
                            var randomclass='rand'+Math.floor((Math.random() * 1000) + 1);
                            var questionhtml=getQuestion(optioncount,randomclass);
                            $('#mulpartqstarea').append(questionhtml);
                            $('#multypartQuestions'+randomclass).ckeditor();
                        }

                    }
                    else if(prevcount>count)
                    {

                        newcount=prevcount-count;

                        for(i=prevcount;i>count;i--)
                        {
                              $('.questioncount'+i).remove();
                              $('.answerpart'+i).remove();
                        }
                    }
                }
                else
                {
                   $('#mulpartqstarea').html('')
                }
                 $('.selecteqstdcount').val(count)

         });

         $(document).on('change','.multypartType',function(){
                var selectedval=$(this).val();
                var identifier=$(this).attr('data-count');
                if(selectedval=='1' | selectedval=='2')
                {
                     $('#multypartOptions'+identifier).attr('disabled',false);
                     $('.answerpart'+identifier).html('');
                     $('#multypartselectedcount'+identifier).val('0');
                }
                else if(selectedval=='3')
                {
                    $('#multypartOptions'+identifier).attr('disabled',true)
                    $('#multypartOptions'+identifier+' option[value=0]').attr('selected','selected');
                    var trufalseanswer=GetTrufalseanswer(identifier)
                    $('.answerpart'+identifier).html(trufalseanswer);
                }
                else if(selectedval=='4')
                {
                    $('#multypartOptions'+identifier).attr('disabled',true)
                    $('#multypartOptions'+identifier+' option[value=0]').attr('selected','selected');
                    var trufalseanswer=Gettextanswer(identifier)
                    $('.answerpart'+identifier).html(trufalseanswer);
                    $('#MPansweroptions'+identifier).ckeditor();

                }
                else
                {
                     $('#multypartOptions'+identifier).attr('disabled',true)
                     $('#multypartOptions'+identifier+' option[value=0]').attr('selected','selected');
                     $('.answerpart'+identifier).html('');
                }
                var show=0;
                $('.multypartType').each(function(){
                    if($(this).val()=='1')
                    {
                        show=1;    
                    }
                    else if($(this).val()=='2')
                    {
                        show=1;    
                    }
                    else if($(this).val()=='3')
                    {
                        show=1;    
                    }                     
                });
                if(show==1)
                {
                 $('#showoptiondiv').show();   
                }
                else
                {
                    $('#showoptiondiv').hide();
                }
         })
         $(document).on('change','.multypartOptions',function(){
                var identifier=$(this).attr('data-count');
                var questiontype=$('#multypartType'+identifier).val();
                var count=($(this).val()*1);
                var prevcount=($('#multypartselectedcount'+identifier).val()*1);
                var newcount=0;

                if(count>prevcount)
                {
                    newcount=count-prevcount;
                    for(i=1;i<=newcount;i++)
                    {
                        optioncount=prevcount+i
                        var randomclass='rand'+Math.floor((Math.random() * 1000) + 1);
                        var answerhtml=getanswerhtml(identifier,optioncount,questiontype,randomclass)
                        $('.answerpart'+identifier).append(answerhtml);
                        CKEDITOR.replace( 'MPCanswer'+randomclass);
                        //$('.answer').ckeditor();
                    }
                }
                else if(prevcount>count)
                {

                    newcount=prevcount-count;

                    for(i=prevcount;i>count;i--)
                    {
                          $('.option'+identifier+i).remove();

                    }
                }
                $('#multypartselectedcount'+identifier).val(count)
         })
    ");
?>
<script type="text/javascript">
    function getQuestion(count,randomclass)
    {
        var questionhtml='<div class="col-lg-12 mater_bg_border questioncount'+count+'">' +
            '<div class="form-group">' +
            '<label for="Questions_Enter_Question" class="font2">Enter  Question '+count+'</label>' +
            '<textarea rows="6" class="form-control multypartQuestions'+count+'" name="multypartQuestions[]" id="multypartQuestions'+randomclass+'" ></textarea>' +
            '</div>'+
            '<div class="form-group"><span class="btn btn-default btn-file">' +
            '<i class="fa fa-picture-o fa-lg yellow"></i>Upload Image' +
            '<input name="multypartQuestionsImage'+count+'"  data-type="question" data-section="'+count+'" data-name="multypartQuestionsImage'+count+'"  id="Questions_TM_QN_ImagePart'+count+'" class="imageselect"  type="file">' +
            '</span>' +
            '</div>' +
            '<div class="row">' +
            '<div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePart'+count+'Div">' +
            '<figure class="pull-left imagespan">' +
            '<div class="deletespan" data-type="question" data-section="'+count+'" data-parent="Questions_TM_QN_ImagePart'+count+'Div" data-file="Questions_TM_QN_ImagePart'+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>' +
            '<img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePart'+count+'Preview" src="" alt="placehold.it/350x250">' +
            '</figure></div>'+
            '<div class="col-lg-3 options">' +
            '<label >Question Type </label>' +
            '<select class="form-control multypartType" id="multypartType'+count+'" data-count="'+count+'" name="multypartType[]" >' +
            '<option value="0" selected="selected">Selected</option><option value="1" >Multiple Option</option><option value="2">Single Option</option><option value="3">True/false</option><option value="4">Text Entry</option></select>' +
            '</div>' +
            '<div class="col-lg-3 options">' +
            '<label >No. Of Options </label>' +
            '<select class="form-control multypartOptions" disabled id="multypartOptions'+count+'" data-count="'+count+'" name="multypartOptions'+count+'" >' +
            '<option value="0" selected>Select</option><option value="2" >2</option><option value="3">3</option><option value="4">4</option></select>' +
            '<input type="hidden" class="multypartselectedcount" id="multypartselectedcount'+count+'" value="0">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="answerpart'+count+'"></div>';
        return questionhtml
    }
    function getanswerhtml(identifier,count,type,randomclass)
    {
        if(type=='1')
        {
            var qtype='checkbox';
            var correctname='correctanswer'+identifier+count;
        }
        else if(type='2')
        {
            var qtype='radio';
            var correctname='correctanswer'+identifier;
        }

        var answertemp='<div class="col-lg-12 mater_bg_border option'+identifier+count+'">' +
            '<div class="col-lg-10">' +
            '<label >Option '+count+'</label>' +
            '<textarea rows="4" class="form-control MPCanswer'+identifier+count+'" id="MPCanswer'+randomclass+'" name="answer'+identifier+'[]" ></textarea>' +
            '</div>' +
            '<div class="col-lg-2 topmarg">' +
            '<label class="mark">marks</label>' +
            '<input type="text" name="marks'+identifier+count+'"  id="MPCmarks'+identifier+count+'"  disabled="true" class="form-control marks MPCmarks'+identifier+'">' +
            '</div>' +
            '<div class="col-lg-12"><div class="col-lg-8">' +
            '<span class="btn btn-default btn-file">' +
            '<i class="fa fa-picture-o fa-lg yellow"></i>' +
            'Upload Image <input type="file" name="anserimage'+identifier+count+'" data-type="answer" data-section="'+identifier+count+'" data-name="anserimage'+identifier+count+'" id="anserimage'+identifier+count+'" class="answerimage imageselect">' +
            '<input type="hidden" id="checkanswer'+identifier+count+'" value="false">'+
            '</span>' +
            '</div>' +
            '<div class="col-lg-3">' +
            '<input type="'+qtype+'" name="'+correctname+'" value="'+count+'" id="MPCcorrectanswer'+identifier+count+'"   class="correctacswer MPCcorrectanswer'+identifier+' MPCcorrectanswer">' +
            '<label>Tick for correct answer</label>' +
            '</div></div>' +
            '<div class="col-lg-12">' +
            '<div class="media col-md-3 imagepreview" id="anserimage'+identifier+count+'Div">' +
            '<figure class="pull-left imagespan">' +
            '<div class="deletespan" data-type="answer" data-section="'+identifier+count+'" data-parent="anserimage'+identifier+count+'Div" data-file="anserimage'+identifier+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>'+
            '<img class="media-object img-rounded img-responsive" id="anserimage'+identifier+count+'Preview" src="" alt="placehold.it/350x250"></figure></div>'+
            '</div>'+
            '</div>';
        return answertemp;
    }
    function GetTrufalseanswer(identifier)
    {
        var answertemp='<div class="col-lg-12 mater_bg_border option'+identifier+'">' +
            '<h3>Answer Options</h3>' +
            '<div class="row row_with_brd " id="MPTFoption'+identifier+'">' +
            '<div class="col-lg-2 marginfive">' +
            '<input type="radio" name="correctanswer'+identifier+'" class="correctanswerTF'+identifier+'" value="True"><label>True</label>' +
            '</div>' +
            '<div class="col-lg-2 marginfive">' +
            '<input type="radio" name="correctanswer'+identifier+'" class="correctanswerTF'+identifier+'" value="False"><label>False</label>' +
            '</div>' +
            '<div class="col-lg-2 topmarg">' +
            '<label class="mark">marks</label>' +
            '<input type="text" name="marks'+identifier+'" id="MPmarksTF'+identifier+'" class="form-control marks">' +
            '</div>' +
            '<div class="col-lg-8"><label>Please select correct answer for True Or False</label>' +
            '</div>'+
            '</div>' +
            '</div>';
        return answertemp
    }
    function Gettextanswer(identifier)
    {
        var answertemp='<div class="col-lg-12 mater_bg_border">' +
            '<h3>Answer Options</h3>' +
            '<div class="row row_with_brd">' +
            '<div class="col-lg-10">' +
            '<label >Please provide probable solition seperated by \'<b>;</b>\'</label>'+
            '<textarea rows="4" class="form-control answer" name="answeroptions'+identifier+'" id="MPansweroptions'+identifier+'"></textarea>' +
            '</div>'+
            '<div class="col-lg-2 topmarg">' +
            '<label class="mark">marks</label>' +
            '<input type="text" name="marks'+identifier+'" id="MPmarksTEXT'+identifier+'" class="form-control marks">' +
            '</div>' +
            '<div class="col-lg-4 pull-right">' +
            '<input size="60" maxlength="250" name="Editor'+identifier+'"  value="1" type="checkbox"> ' +
            '<label for="Questions_Click_To_enable_editor_for_student_answering">Click  To Enable Editor For Student Answering</label>' +
            '</div>' +
            '</div>' +
            '</div>';
        return answertemp
    }
    function GetMulpartCont()
    {

    }
</script>