<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
    'Questions'=>array('admin'),
    'Create',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
    array('label'=>'List Questions', 'url'=>array('admin')),
);
?>

<h3>Create Question</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>