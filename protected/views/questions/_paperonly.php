<div class="col-lg-12 mater_bg_border">
    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_QuestionPaperOnly]" id="Questions_TM_QN_QuestionPaperOnly"></textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImagePaperOnly" id="Questions_TM_QN_ImagePaperOnly" data-type="question" data-section="main" data-name="TM_QN_ImagePaperOnly" class="imageselect" type="file">
                     </span>

            <?php echo $form->error($model,'TM_QN_Image'); ?>
        </div>
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePaperOnlyDiv">
            <figure class="pull-left imagespan">
                <div class="deletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImagePaperOnlyDiv" data-file="Questions_TM_QN_ImagePaperOnly" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePaperOnlyPreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
        <div class="col-lg-2 pull-right">
            <label class="mark">marks</label>
            <input type="text" name="Papermarks"  id="Papermarks"   class="form-control marks Papermarks">
        </div>
    </div>

</div>