<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Parent_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Parent_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Publisher_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Publisher_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Syllabus_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Syllabus_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Standard_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Standard_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Subject_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Subject_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Topic_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Topic_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Section_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Section_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Dificulty_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Dificulty_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Type_Id'); ?>
		<?php echo $form->textField($model,'TM_QN_Type_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Question'); ?>
		<?php echo $form->textArea($model,'TM_QN_Question',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Image'); ?>
		<?php echo $form->textField($model,'TM_QN_Image',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Answer_Type'); ?>
		<?php echo $form->textField($model,'TM_QN_Answer_Type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Option_Count'); ?>
		<?php echo $form->textField($model,'TM_QN_Option_Count'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Editor'); ?>
		<?php echo $form->textField($model,'TM_QN_Editor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Totalmarks'); ?>
		<?php echo $form->textField($model,'TM_QN_Totalmarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_QN_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_QN_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_QN_Status'); ?>
		<?php echo $form->textField($model,'TM_QN_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->