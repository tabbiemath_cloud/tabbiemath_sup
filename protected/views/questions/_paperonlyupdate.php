<div class="col-lg-12 mater_bg_border">
    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_QuestionPaperOnly]" id="Questions_TM_QN_QuestionPaperOnly">
            <?php echo ($model->TM_QN_Type_Id=='6'?CHtml::encode($model->TM_QN_Question):''); ?>
        </textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImagePaperOnly" data-type="question" data-section="main" data-name="TM_QN_ImagePaperOnly"   data-main="questionimagediv<?php echo $model->TM_QN_Id;?>" id="Questions_TM_QN_ImagePaperOnly"  class="imageselect" type="file">
                     </span>

            <?php echo $form->error($model,'TM_QN_Image'); ?>
        </div>
        <div class="col-lg-2">
            <label class="mark">marks</label>
            <input type="text" name="Papermarks"  id="Papermarks"  value="<?php echo $model->TM_QN_Totalmarks;?>"  class="form-control marks markdisply Papermarks">
        </div>
    </div>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePaperOnlyDiv">
            <figure class="pull-left previmagespan">
                <div class="previewdeletespan" data-type="question" data-section="<?php echo $identifier;?>" data-parent="Questions_TM_QN_ImagePaperOnlyDiv" data-file="Questions_TM_QN_ImagePaperOnlyImage" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePaperOnlyPreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
        <?php if($model->TM_QN_Image!=''):?>
        <div class="media col-md-3" id="questionimagediv<?php echo $model->TM_QN_Id;?>">
            <figure class="pull-left imagespan">
                <div class="deletespan" data-type="question" data-section="<?php echo $identifier;?>" data-id="<?php echo $model->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $model->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
            </figure>
        </div>
        <?php endif;?>
    </div>
</div>