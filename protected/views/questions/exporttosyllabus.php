<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
    'Questions'=>array('admin'),
    'Create',
);
$this->pageTitle='Export Syllabus Questions';
$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
    array('label'=>'List Questions', 'url'=>array('admin')),
    array('label'=>'Create Question', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('questions/admin')."';
    });
");
?>

<h3>Export Syllabus Questions</h3>
<div class="row brd1">
    <form method="post" action="<?php echo CController::createUrl('questions/exportsyllabuscsv');?>" enctype="multipart/form-data" id="exportform">
        <div class="panel-body form-horizontal payment-form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select Syllabus <span class="required">*</span></label>
                <div class="col-sm-4">                                
                    <select name="syllabus" id="syllabus" class="form-control">
                    <option value="">Select Syllabus</option>
                    <?php foreach($syllabus AS $syllab):?>
                            <option value="<?=$syllab->TM_SB_Id;?>"><?=$syllab->TM_SB_Name;?></option>
                    <?php endforeach;?>  
                     
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select Standard <span class="required">*</span></label>
                <div class="col-sm-4">                                
                    <select name="standard" id="standard" class="form-control">
                    <option value="">Select Standard</option>                  
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select Chapter </label>
                <div class="col-sm-4">                                
                    <select name="chapter" id="chapter" class="form-control">
                    <option value="">Select Chapter</option>                  
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select Topic</label>
                <div class="col-sm-4">                                
                    <select name="topic" id="topic" class="form-control">
                    <option value="">Select Topic</option>                  
                    </select>
                </div>
            </div>            
            <div class="form-group">
                <div class="col-sm-3 pull-left"> </div>
                <div class="col-sm-3 pull-left">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                <div class="col-sm-3 pull-left">
                    <?php echo CHtml::submitButton('Export',array('class'=>'btn btn-warning btn-lg btn-block','name'=>'submit')); ?>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $('#syllabus').change(function(){
        $("#standard").empty().append('<option value="">Select Standard</option>');
        $("#chapter").empty().append('<option value="">Select Chapter</option>');
        $("#topic").empty().append('<option value="">Select Topic</option>');
       if($(this).val()!=''){
            var id=$(this).val();
            $.ajax({
                method: 'POST',
                url: '<?php echo CController::createUrl('chapter/GetstandardQuestion');?>',                
                data: {id:id},
                success: function(data){                        
                    $("#standard").empty().append(data);                    
                }
            });             
       } 
    });
    $('#standard').change(function(){    
        $("#chapter").empty().append('<option value="">Select Chapter</option>');
        $("#topic").empty().append('<option value="">Select Topic</option>');
       if($(this).val()!=''){
            var id=$(this).val();
            $.ajax({
                method: 'POST',
                url: '<?php echo CController::createUrl('topic/GetChapterList');?>',                
                data: {id:id},
                success: function(data){                        
                    $("#chapter").empty().append(data);                    
                }
            });             
       } 
    });

    $('#chapter').change(function(){            
        $("#topic").empty().append('<option value="">Select Topic</option>');
       if($(this).val()!=''){
            var id=$(this).val();
            $.ajax({
                method: 'POST',
                url: '<?php echo CController::createUrl('questions/GetTopicList');?>',                
                data: {id:id},
                success: function(data){                        
                    $("#topic").empty().append(data);                    
                }
            });             
       } 
    });  
    $('#exportform').submit(function(event){ 
        $('.errorMessage').remove();
        var syllabus=$('#syllabus').val();
        var standard=$('#standard').val();          
        if(syllabus!='' & standard!='' )
        {
            return true;
        }
        else
        {
            if(standard=='')
            {
                $('#syllabus').after('<div class="errorMessage">Select Syllabus</div>');   
            }   
            if(standard=='')
            {
                $('#standard').after('<div class="errorMessage">Select Standard</div>');
            }
            return false;
        }
        event.preventDefault(); 
    })      
</script>