<?php
/* @var $this PlanController */
/* @var $model Plan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_PN_Id'); ?>
		<?php echo $form->textField($model,'TM_PN_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PN_Name'); ?>
		<?php echo $form->textField($model,'TM_PN_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PN_Rate'); ?>
		<?php echo $form->textField($model,'TM_PN_Rate',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PN_Duration'); ?>
		<?php echo $form->textField($model,'TM_PN_Duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PN_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_PN_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PN_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_PN_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PN_Status'); ?>
		<?php echo $form->textField($model,'TM_PN_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->