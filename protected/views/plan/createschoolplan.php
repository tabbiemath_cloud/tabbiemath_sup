<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'School Plans'=>array('manageschoolplan'),
	'Create',
);
$this->menu=array(
    array('label'=>'Manage School Plan', 'class'=>'nav-header'),
	array('label'=>'List School Plan', 'url'=>array('manageschoolplan')),	
);
?>

<h3>Create Plan</h3>

<?php $this->renderPartial('_schoolplanform', array('model'=>$model)); ?>