<?php
/* @var $this PlanController */
/* @var $model Plan */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('plan/admin')."';
    });
");
?>
<div class="row brd1">
    <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'plan-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <?php echo $form->errorSummary($model); ?>

        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_PN_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_PN_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_PN_Name'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_PN_Publisher_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_PN_Publisher_Id',CHtml::listData(Publishers::model()->findAll(array('order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),array('empty'=>'Select Publisher','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_PN_Publisher_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_PN_Syllabus_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_PN_Syllabus_Id',CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty'=>'Select Syllabus','class'=>'form-control','ajax' =>
            array(
                'type'=>'POST', //request type
                'url'=>CController::createUrl('chapter/Getstandard'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                //Style: CController::createUrl('currentController/methodToCall')
                'update'=>'#Plan_TM_PN_Standard_Id', //selector to update
                'data'=>'js:{id:$(this).val()}'
                //leave out the data key to pass all form values through
            ))); ?>
                <?php echo $form->error($model,'TM_PN_Syllabus_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_PN_Standard_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php
                if($model->isNewRecord):
                    echo $form->dropDownList($model,'TM_PN_Standard_Id',array(''=>'Select Standard'),array('class'=>'form-control'));
                else:
                    echo $form->dropDownList($model,'TM_PN_Standard_Id',CHtml::listData(Standard::model()->findAll(array('condition'=>"TM_SD_Syllabus_Id =  $model->TM_PN_Syllabus_Id",'order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),array('empty'=>'Select Standard','class'=>'form-control'));
                endif;
                    ?>
                <?php echo $form->error($model,'TM_PN_Standard_Id'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_PN_Status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->hiddenField($model,'TM_PN_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
                <?php echo $form->dropDownList($model,'TM_PN_Status',Plan::itemAlias('PlanStatus'),array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_PN_Status'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
            <div class="col-sm-3 pull-right">
                <?php echo $form->hiddenField($model,'TM_PN_Type',array('value'=>1)); ?>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?php
if(!$model->isNewRecord):?>
    <script type="text/javascript">
        $(function(){
            $.ajax({
                type:'POST',
                url:'<?php echo CController::createUrl('chapter/Getstandard');?>',
                update:'#Plan_TM_PN_Standard_Id',
                data:{id:<?php echo $model->TM_PN_Standard_Id;?>}
            });
        })
    </script>
<?php 
endif;
?>