<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'School Plans'=>array('manageschoolplan'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage School Plan', 'class'=>'nav-header'),
	array('label'=>'List School Plan', 'url'=>array('manageschoolplan')),
	array('label'=>'Create School Plan', 'url'=>array('createschoolplan')),
);
?>
<h3>Manage School Plans</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'plan-grid',
            'dataProvider'=>$model->searchschool(),
            'filter'=>$model,
            'columns'=>array(
                'TM_PN_Name',            
                array(
                    'name'=>'TM_PN_Publisher_Id',
                    'value'=>'Publishers::model()->findByPk($data->TM_PN_Publisher_Id)->TM_PR_Name',
                    'filter'=>CHtml::listData(Publishers::model()->findAll(array('order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),
                ),
                array(
                    'name'=>'TM_PN_Syllabus_Id',
                    'value'=>'Syllabus::model()->findByPk($data->TM_PN_Syllabus_Id)->TM_SB_Name',
                    'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),
                ),
                array(
                    'name'=>'TM_PN_Standard_Id',
                    'value'=>'Standard::model()->findByPk($data->TM_PN_Standard_Id)->TM_SD_Name',
                    'filter'=>CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),
                ),
                array(
                    'name'=>'TM_PN_Status',
                    'value'=>'Plan::itemAlias("PlanStatus",$data->TM_PN_Status)',
                    'filter'=>Plan::itemAlias('PlanStatus'),
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            ),
                            'url'=>'Yii::app()->createUrl("plan/viewschoolplan",array("id"=>$data->TM_PN_Id))',
                            ),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            ),
                            'url'=>'Yii::app()->createUrl("plan/updateschoolplan",array("id"=>$data->TM_PN_Id))',                            
                            ),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                            'url'=>'Yii::app()->createUrl("plan/deleteschoolplan",array("id"=>$data->TM_PN_Id))',                            
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
