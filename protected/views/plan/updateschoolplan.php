<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'Plans'=>array('admin'),
	$model->TM_PN_Id=>array('view','id'=>$model->TM_PN_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage School Plan', 'class'=>'nav-header'),
	array('label'=>'List School Plan', 'url'=>array('manageschoolplan')),
	array('label'=>'Create School Plan', 'url'=>array('createschoolplan')),
	array('label'=>'View School Plan', 'url'=>array('viewschoolplan', 'id'=>$model->TM_PN_Id)),
);
?>

<h3>Update <?php echo $model->TM_PN_Name; ?></h3>

<?php $this->renderPartial('_schoolplanform', array('model'=>$model)); ?>