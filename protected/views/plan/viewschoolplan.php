<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'Plans'=>array('admin'),
	$model->TM_PN_Id,
);

$this->menu=array(
    array('label'=>'Manage School Plan', 'class'=>'nav-header'),
	array('label'=>'List School Plan', 'url'=>array('manageschoolplan')),
	array('label'=>'Create School Plan', 'url'=>array('createschoolplan')),
	array('label'=>'Update School Plan', 'url'=>array('updateschoolplan', 'id'=>$model->TM_PN_Id)),
	array('label'=>'Delete School Plan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('deleteschoolplan','id'=>$model->TM_PN_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3>View <?php echo $model->TM_PN_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $id=$model->TM_PN_Status;
        $userid=$model->TM_PN_CreatedBy;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_PN_Id',
                'TM_PN_Name',                              
                array(
                    'name'=>'TM_PN_Publisher_Id',
                    'type'=>'raw',
                    'value'=>Publishers::model()->findByPk($model->TM_PN_Publisher_Id)->TM_PR_Name,
                ),
                array(
                    'name'=>'TM_PN_Syllabus_Id',
                    'type'=>'raw',
                    'value'=>Syllabus::model()->findByPk($model->TM_PN_Syllabus_Id)->TM_SB_Name,
                ),
                array(
                    'name'=>'TM_PN_Standard_Id',
                    'type'=>'raw',
                    'value'=>Standard::model()->findByPk($model->TM_PN_Standard_Id)->TM_SD_Name,
                ),
                'TM_PN_CreatedOn',
                array(
                    'label'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Plan::itemAlias("PlanStatus",$id),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
