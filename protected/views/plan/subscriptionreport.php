<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
    'Plans'=>array('admin'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Plan', 'class'=>'nav-header'),
    /*array('label'=>'List Plan', 'url'=>array('admin')),
    array('label'=>'Create Plan', 'url'=>array('create')),*/
);

?>
    <h3>Subscription Report</h3>
        <div class="row brd1">
            <div class="col-lg-12">
                <form method="post" id="searchform">
                    <table class="table table-bordered table-hover table-striped admintable">
                        <tr><th colspan="6"><h5><?php echo UserModule::t("Search"); ?></h5></th></tr>
                        <tr>
                            <th>School</th>
                            <th>Voucher Code</th>
                            <th>Plan</th>
                            <th>Subscription Status</th>
                        </tr>
                        <tr>
                            <td>
                                <select name="school" id="school" class="form-control">                                    
                                    <?php 
                                        $data=CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');
                                        
                                        echo CHtml::tag('option',
                                                       array('value'=>''),'Select School',true);
                                        foreach($data as $value=>$name)
                                        {
                                            if($_POST['school']==$value):
                                                $selected=array('value'=>$value,'selected'=>'selected');              
                                            else:
                                                $selected=array('value'=>$value);
                                            endif;
                                            echo CHtml::tag('option',
                                                       array('value'=>$value),CHtml::encode($name),true);
                                        }
                                        echo CHtml::tag('option',array('value'=>'0'),'Other',true);                                    
                                    ?>
                                </select>
                                <input type="text" name="schooltext" id="schooltext" class="form-control" />
                                </td>
                            <td><input type="text" name="vouchercode" id="vouchercode" class="form-control" /></td>
                            <td>
                                <select name="plan[]" id="plan" class="form-control" multiple="multiple">
                                    <option value="0">Select Plan</option>
                                    <?php
                                    $plans=Plan::model()->findAll(array('condition'=>'TM_PN_Status=0'));

                                    foreach($plans AS $plan):
                                        if($_GET['plan']==$plan->TM_PN_Id):
                                            $selected='selected';
                                        else:
                                            $selected='';
                                        endif;;
                                        echo "<option value='".$plan->TM_PN_Id."' ".$selected.">".$plan->TM_PN_Name."</option>";
                                    endforeach;
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select name="status" class="form-control">
                                    <option value="all">Select Status</option>
                                    <option value="0">Active</option>
                                    <option value="1">Inactive</option>
                                    <option value="both">Both</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" class="date" name="date" value="before" id="before">Before a date
                                <input type="radio" class="date" name="date" value="after" id="after">After a date
                                <input type="radio" class="dates" name="date" value="between" id="days" checked="checked">Between dates
                            </td>
                            <td><input type="date" class="form-control" id="fromdate" name="fromdate" ></td>
                            <td><input type="date" class="form-control" id="todate" name="todate" ></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td ><?php if(count($formvals)>0):?><a href="<?php echo Yii::app()->createUrl('plan/export',array('school'=>$formvals['school'],'schooltext'=>$formvals['schooltext'],'vouchercode'=>$formvals['vouchercode'],'plan'=>$formvals['plan'],'status'=>$formvals['status'],'date'=>$formvals['date'],'fromdate'=>$formvals['fromdate'],'todate'=>$formvals['todate']))?>"><input type='button' class='btn btn-warning btn-md' id='export' value='Export'></a><?php endif;?></td>
                            <!--<td><?php /*if(count($formvals)>0):*/?><a href="<?php /*echo Yii::app()->request->baseUrl*/?>/plan/export?"<?php /*echo http_build_query($formvals);*/?>><input type='button' class='btn btn-warning btn-md' id='export' value='Export'></a><?php /*endif;*/?></td>-->
                            <td><button class="btn btn-warning btn-lg btn-block" id="search" name="search" value="search" type="submit">Search</button></td>
                            <td><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="row brd1" style="width: 250%;">
            <div class="col-lg-12">
                <div class="grid-view" id="yw0">
                    <table class="table table-bordered table-hover table-striped admintable" id="reporttable">
                        <thead>
                        <tr>
                            <th>Student Username</th>
                            <th>Student Name</th>
                            <th>School Name</th>
                            <th>Student Status</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>Parent Name</th>
                            <th>Parent Email</th>
                            <th>Parent Phone Number</th>
                            <th>Plan</th>
                            <th>Syllabus</th>
                            <th>Standard</th>
                            <th>Transaction Code</th>
                            <th>Gateway</th>
                            <th>Activated On</th>
                            <th>Expired On</th>
                            <th>Cancelled On</th>
                            <th>Subscription Status</th>
                            <th>Coupon Code</th>
                            <th>Currency</th>
                            <th>Price of Subscription</th>
                            <th>Final Price Paid</th>
                        </tr>
                        </thead>
                        <tbody id="reportitems">
                        <?php echo $dataitems;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    <script type="text/javascript">
        $(function(){
            $('#schooltext').hide();
            $('#clear').click(function(){
                $('#student ').val('');
                $('#parent ').val('');
                $('#school ').val('');
                $('#plan ').val('0');
                return true;
            });
            $('.date').click(function(){
                $('#fromdate').show();
                $('#todate').hide();
            })
            $('.dates').click(function(){
                $('#fromdate').show();
                $('#todate').show();
            })            
            $('#school').change(function(){
                if($(this).val()=='0')
                {
                    $('#schooltext').show();
                }   
                else
                {
                    $('#schooltext').hide();
                }
            });              
        });
        /*$(document).on('click', '#export', function(){
            $.ajax({
                type: 'POST',
                url: '../plan/Export',
                data: $("#searchform" ).serialize()
            });
        });*/
    </script>