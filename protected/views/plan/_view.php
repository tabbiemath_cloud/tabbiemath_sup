<?php
/* @var $this PlanController */
/* @var $data Plan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PN_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_PN_Id), array('view', 'id'=>$data->TM_PN_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PN_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PN_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PN_Rate')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PN_Rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PN_Duration')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PN_Duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PN_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PN_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PN_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PN_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PN_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PN_Status); ?>
	<br />


</div>