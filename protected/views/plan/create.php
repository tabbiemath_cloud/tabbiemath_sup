<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'Plans'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Plan', 'class'=>'nav-header'),
	array('label'=>'List Plan', 'url'=>array('admin')),
);
?>

<h3>Create Plan</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>