<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'Plans'=>array('admin'),
	$model->TM_PN_Id=>array('view','id'=>$model->TM_PN_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Plan', 'class'=>'nav-header'),
	array('label'=>'List Plan', 'url'=>array('admin')),
	array('label'=>'Create Plan', 'url'=>array('create')),
	array('label'=>'View Plan', 'url'=>array('view', 'id'=>$model->TM_PN_Id)),
);
?>

<h3>Update <?php echo $model->TM_PN_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model,'modules'=>$modules)); ?>