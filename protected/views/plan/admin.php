<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
	'Plans'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Plan', 'class'=>'nav-header'),
	array('label'=>'List Plan', 'url'=>array('admin')),
	array('label'=>'Create Plan', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#plan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>Manage Revision Plans</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'plan-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                'TM_PN_Name',
                'TM_PN_Rate',
                'TM_PN_Additional_Rate',                                           
                array(
                    'name'=>'TM_PN_Duration',
                    'value'=>'$data->TM_PN_Duration." Days"',                    
                ),                
                array(
                    'name'=>'TM_PN_Publisher_Id',
                    'value'=>'Publishers::model()->findByPk($data->TM_PN_Publisher_Id)->TM_PR_Name',
                    'filter'=>CHtml::listData(Publishers::model()->findAll(array('order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),
                ),
                array(
                    'name'=>'TM_PN_Syllabus_Id',
                    'value'=>'Syllabus::model()->findByPk($data->TM_PN_Syllabus_Id)->TM_SB_Name',
                    'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),
                ),
                array(
                    'name'=>'TM_PN_Standard_Id',
                    'value'=>'Standard::model()->findByPk($data->TM_PN_Standard_Id)->TM_SD_Name',
                    'filter'=>CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),
                ),
                array(
                    'name'=>'TM_PN_Currency_Id',
                    'value'=>'Currency::model()->findByPk($data->TM_PN_Currency_Id)->TM_CR_Code',
                    'filter'=>CHtml::listData(Currency::model()->findAll(array('order' => 'TM_CR_Name')),'TM_CR_Id','TM_CR_Name'),
                ), 
                array(
                    'name'=>'TM_PN_Additional_Currency_Id',
                    'value'=>'Plan::model()->getCurrency($data->TM_PN_Additional_Currency_Id)',                    
                    'filter'=>CHtml::listData(Currency::model()->findAll(array('order' => 'TM_CR_Name')),'TM_CR_Id','TM_CR_Name'),
                ),                                                  
                array(
                    'name'=>'TM_PN_Visibility',
                    'value'=>'Plan::itemAlias("PlanVisibility",$data->TM_PN_Visibility)',
                ),
                array(
                    'name'=>'TM_PN_Status',
                    'value'=>'Plan::itemAlias("PlanStatus",$data->TM_PN_Status)',
                    'filter'=>Plan::itemAlias('PlanStatus'),
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            )),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
