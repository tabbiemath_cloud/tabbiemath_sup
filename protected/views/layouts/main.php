<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if lt IE 9]>
    <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- responsive-full-background-image.css stylesheet contains the code you want -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/responsive-full-background-image.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/frontendbootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/common.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/hover.css" rel="stylesheet" media="all">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/dashboard.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/slider.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/progress.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magicsuggest-min.css">    
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/rwd-tableslandscape.css">
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sweetalert.css">    
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/stylesheet.css" type="text/css" charset="utf-8">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/prologin.css">
    <?php
    $cs=Yii::app()->clientScript;

    $cs->scriptMap=array(
        'jquery.js'=>false,
        'jquery.min.js'=>false,		
    );?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery1_11.js"></script>
    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-slider.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr.custom.93665.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-asPieProgress.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/magicsuggest-min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.rwd-tables.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jCProgress-1.0.3.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/icheck.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.form.js"></script>
    <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script>
   <!--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script> -->
    <script>
        // With JQuery
        $('#ex1').slider({
            formatter: function(value) {
                return 'Current value: ' + value;
            }
        }).on('slideStop', function(ev){
            var newVal = $('#ex1').data('slider').getValue();
            $('#adv').val(newVal);
        });
        /*
         // Without JQuery
         var slider = new Slider('#ex1', {
         formatter: function(value) {
         return 'Current value: ' + value;
         }
         });*/

    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            /*        $('.showslide').click(function(){
             var identi=$(this).attr('data-uniq');
             $('#slide'+identi+'Slider').toggle()
             });*/
           
        var is_touch_device = 'ontouchstart' in document.documentElement;
        
        if (!is_touch_device) {
         $('[data-toggle="tooltip"]').tooltip();
          $(".tip-top").tooltip({
                placement : 'top'
            });
            $(".tip-right").tooltip({
                placement : 'right'
            });
            $(".tip-bottom").tooltip({
                placement : 'bottom'
            });
            $(".tip-left").tooltip({
                placement : 'left'
            });
        }
        else
        {
            $('.selectslide').attr('data-slider-tooltip','hide')
        } 

        });
    </script>
</head>
<?php if(Yii::app()->session['userlevel']==1):?>
<body class="professional hidebg">
<?php else:?>
<body class="regular">
<?php endif;?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid pro-content">
        <?php if(Yii::app()->session['userlevel']==1):?>
            <div class="pro-logo-right"><span>Powered by TabbieMe Ltd</span><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" /></div>
        <?php endif;?>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php if(Yii::app()->user->isGuest):?>
                <a class="navbar-brand" href="<?php echo Yii::app()->request->baseUrl;?>"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png'); ?></a>
            <?php else:?>
                <a class="navbar-brand" href="<?php echo Yii::app()->createUrl((Yii::app()->controller->GetParent()?'parenthome':'home'))?>">
                    <?php if(Yii::app()->session['userlevel']==1):
                        echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo_pro.png');
                    else:
                        echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png');
                    endif;
                    ?>
                    <?php /*echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png'); */?>
                </a>
                <!--<a class="navbar-brand" href="<?php /*echo Yii::app()->createUrl((Yii::app()->controller->GetParent()?'parenthome':'home'))*/?>"><?php /*echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png'); */?></a>-->
            <?php endif;?>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php if(!Yii::app()->controller->GetParent() & !Yii::app()->user->isGuest):?>
            <ul class="nav navbar-nav navbar-right">
                <?php if(Yii::app()->session['quick'] || Yii::app()->session['difficulty'] || Yii::app()->session['challenge'] || Yii::app()->session['mock']): ?>     
                <?php /*echo Yii::app()->controller->getLevels();*/?>
                    <?php
                    if(Yii::app()->session['userlevel']==1){
                    }
                    else{
                        echo Yii::app()->controller->getLevels();
                    }
                    ?>
                <li data-toggle="tooltip" data-placement="bottom" title="Progress Report">
                    <a href="<?php echo Yii::app()->createUrl('student/ProgressReport')?>" >
                        <span class="fa fa-line-chart size2 brd_r"></span>
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="bottom" title="Leader Board">
                    <a href="<?php echo Yii::app()->createUrl('student/Leaderboard')?>" >
                        <span class="fa fa-trophy size2 brd_r"></span>
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="bottom" title="Buddies">
                    <a href="<?php echo Yii::app()->createUrl('buddies/Buddylist')?>">
                        <span class="fa fa-users size2 brd_r"></span>
                    </a>
                </li>
                <?php endif;?>
                <?php if(Yii::app()->session['school']):?>
                <li data-toggle="tooltip" data-placement="bottom" title="Progress Report">
                    <a href="<?php echo Yii::app()->createUrl('student/schoolprogressReport')?>" >
                        <span class="fa fa-line-chart size2 brd_r"></span>
                    </a>
                </li>                
                <li data-toggle="tooltip" data-placement="bottom" title="Leader Board">
                    <a href="<?php echo Yii::app()->createUrl('student/SchoolLeaderboard')?>" >
                        <span class="fa fa-trophy size2 brd_r"></span>
                    </a>
                </li>
                <?php endif;?>
                <!--<li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-comments size2 brd_r"></span>
                    <span class="label label-primary">5</span>
                    </a>
                </li>-->


            <li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Notifications">
                <a href="#" class="dropdown-toggle notificationread" data-toggle="dropdown">
                    <span class="fa fa-bell fa-2x brd_r"></span>
                    <?php if(Yii::app()->controller->getNotificationCount()>0):?>
                        <span class="label label-primary listnotifications"><?php echo Yii::app()->controller->getNotificationCount();?></span>
                    <?php endif;?>
                </a>
                <ul class="dropdown-menu showsroll">
                        <?php echo Yii::app()->controller->getNotifications();?>

                </ul>

            </li>
            <?php if(Yii::app()->user->id!=45408){ ?>
            <li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Personal Set-Up">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-cog size2 brd_r"></span>
                </a>
                <ul class="dropdown-menu showsroll">
                        <li ><a href="<?php echo Yii::app()->createUrl('student/passwordchange')?>">Change Password</a></li>
                        <li ><a href="#"  data-toggle="modal" data-target="#myModalimagechange" data-backdrop="static" class="deletespan">Change profile image</a></li>

                </ul>

            </li>
            <?php } ?>
            <li data-toggle="tooltip" data-placement="bottom" title="Help">
                <a href="http://www.tabbiemath.com/demo-faq/" target="_blank" >
                    <span class="fa fa-question-circle size2 brd_r"></span>
                </a>
            </li> 				
            <li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Logout">
                <?php if(Yii::app()->session['userlevel']==1):?>
                    <a href="<?php echo Yii::app()->createUrl('professional/logout')?>">
                        <span class="glyphicon glyphicon-off size2 brd_r"></span>
                    </a>
                <?php else:?>
                    <a href="<?php echo Yii::app()->createUrl('user/logout')?>">
                        <span class="glyphicon glyphicon-off size2 brd_r"></span>
                    </a>
                <?php endif;?>
                <!--<a href="<?php /*echo Yii::app()->createUrl('user/logout')*/?>">
                    <span class="glyphicon glyphicon-off size2 brd_r"></span>
                </a>-->
            </li>			
                <div id="myModalimagechange" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close closepreofpicstudent" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Change Profile Pic</h4>
                            </div>
                            <div class="modal-body">
                                <form   id="imageupdatestudent">
                                    <input type="file"  accept="image/*" name="photoimgstud" data-type="profpic" data-section="main" data-name="profpic"  id="photoimgstud" class="studentimageselect profpic">
                                </form>
                                <div class="studentpreviewmain" style="display: none;">
                                    <ul class="upimg">
                                        <form   id="studentimagesave">
                                            <input type="hidden" id="studentnameimg" name="studentnameimg"  value="">
                                            <li class="savespan " data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageTEXTDiv" data-file="uestions_TM_QN_ImageTEXT"  > </a></li>
                                            <img  class="img-circle tab_img studentnewpic" id="studentnewimg" style="margin-right: 26px;" src="">

                                        </form>
                                    </ul>

                                </div>
                            </div>

                            <div class="modal-footer">
                                <div class="pull-left errorupload"> Maximum allowed size is 2Mb </div>
                                
                                <button type="button" class="btn btn-warning  studentsavespan" data-dismiss="modal" style="display: none">Save changes</button>
								<button class="btn btn-default closemodal">Cancel</button>
                            </div>
                        </div>

                    </div>
                </div>

        </ul>
            <?php endif;?>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <?php $this->widget('LeftPanelWidget');?>
        <?php echo $content; ?>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
<?php Yii::app()->clientScript->registerScript('guestscript', "
$(document).on('click','.readnotification',function(){
        var item=$(this).data('id');
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('site/changestatus')."',
              data: { item: item}
            })
        .done(function(data, textStatus, jqXHR){
            return true
        })
});
$(document).on('click','.notificationread',function(){
    var item='all'
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('site/changestatus')."',
              data: { item: item}
            })
        .done(function(data, textStatus, jqXHR){
            $('.listnotifications').html('0')
            return true
        })
});
$('.loadmore').hover(function(){
        var offset=$('#lastitem').val();
        if(offset!='none')
        {
            $.ajax({
                    type: 'POST',
                    url: '".Yii::app()->createUrl('site/getNotifications')."',
                    dataType: 'json',
                    data: {offset: offset},
                    beforeSend : function(){
                        $('#lastitem').val('none');
                    },
                    success: function(data){
                     $('.loadmore').before(data.html);
                     $('#lastitem').val(data.lastitem);
                    }
                });
        }
});
 $('.studentsavespan').click(function(){
            var name=$('#studentnameimg').val();
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('Student/uploadImageSave')."',
              data: { name: name}

            })
.done  (function(data, textStatus, jqXHR)        {

                    $('.oldpicimg').attr('src','". Yii::app()->request->baseUrl."/site/Imagerender/id/'+data+'?size=large&type=user&width=140&height=140');
                    $('#leftpanelimgstud').attr('src','". Yii::app()->request->baseUrl."/site/Imagerender/id/'+data+'?size=large&type=user&width=140&height=140');

                    $('.oldpic').show();
                    $('.studentpreviewmain').hide();
                    $('.studentsavespan').hide();

 })

 });


 $(document).on('change','.studentimageselect',function(){
            $('.studentsavespan').hide();
            var filesize=this.files[0].size;
            filesize=filesize/1024;
            if(filesize<=2000)
            {
                $('#imagetype').val($(this).attr('data-type'));
                $('#imagesection').val($(this).attr('data-section'));
                var section=$(this).attr('data-section');
                var type=$(this).attr('data-type');
                $('#name').val($(this).attr('name'));

                var dataid=$(this).attr('id');
                var form = $('#imageupdatestudent')[0]; // You need to use standart javascript object here
                var formData = new FormData(form);

                $.ajax({
                        url: '".Yii::app()->createUrl('Student/uploadImage')."', // Url to which the request is send
                        type: 'POST',             // Type of request to be send, called as method
                        data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                        contentType: false,       // The content type used when sending data to the server.
                        cache: false,             // To unable request pages to be cached
                        processData:false,        // To send DOMDocument or non processed data file it is set to false
                        beforeSend: function() {
                                                $('#studentnewimg').attr('src','".Yii::app()->request->baseUrl."/images/imageloader.gif');
                                                $('.studentpreviewmain').show();
                          },
                        success: function(data)   // A function to be called if request succeeds
                        {
                        var retstr='failed';
                        var retstr1='Image file size max 2MB';
                        var retstr2='Invalid file format..';
                        var retstr3='Please select image..!';
                        if(data===retstr){
                        $('.studentsavespan').hide();
                        return false;
                        }
                       else if(data===retstr1)
                       {
                        $('.studentsavespan').hide();
                        $('.errorupload').addClass('alert alert-danger');
                            setTimeout(function(){
                            $('.errorupload').removeClass('alert alert-danger');
                            }, 6000);
                        $('#studentnewimg').attr('src','');
                        $('.studentpreviewmain').hide();
                        return false;
                       }
                       else if(data===retstr2){
                       $('.studentsavespan').hide();
                       return false;
                       }
                       else if(data===retstr3){
                       $('.studentsavespan').hide();
                       return false;
                       }
                       else{
                        $('#studentnewimg').attr('src','').hide();                                             
                        //$('.oldpic').hide();
                        $('#studentnewimg').attr('src','". Yii::app()->request->baseUrl."/site/Imagerender/id/'+data+'?size=large&type=user&width=140&height=140').ready(function() {
                            	$('#studentnewimg').show(function(){
                            		$('.studentsavespan').show();
                            	});                                  
                            });                        
                        $('#studentnameimg').attr('value',data);
                   
                        $('.studentpreviewmain').show();

                            $('#photoimgstud').val('');
                        }

                        }
                });
            }
            else
            {
                $('#studentnewimg').attr('src','');
                $('.studentpreviewmain').hide();
                $('.studentsavespan').hide();
                $('.errorupload').addClass('alert alert-danger');
                setTimeout(function(){
                    $('.errorupload').removeClass('alert alert-danger');
                }, 6000);
            }
            //readURL(this)
        });

         $('.closemodal').click(function(){

                 $('.closepreofpicstudent').trigger('click');

         });
          $('.closepreofpicstudent').click(function(){

                  $('.studentpreviewmain').hide();
                  $('#photoimgstud').val('');
                  $('.studentsavespan').hide();
          });

");?>
<script type="text/javascript">

    $(function () {
      
    });
</script>