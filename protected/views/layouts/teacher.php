<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/teacherbootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/common.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin-teacher.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">    
    <!--[if lt IE 9]>
    
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/teacherprologin.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/rwd-tableslandscape.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/slider.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magicsuggest-min.css">
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sweetalert.min.js"></script>
 
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sweetalert.css">    
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/stylesheet.css" type="text/css" charset="utf-8">
        <?php
    $cs=Yii::app()->clientScript;

    $cs->scriptMap=array(
        'jquery.js'=>false,
        'jquery.min.js'=>false,
    );?> 
    
    

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
<title><?php echo CHtml::encode($this->pageTitle); ?></title> 
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>
    <!--<script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js"></script>-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/adapters/jquery.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/metisMenu.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.rwd-tables.js"></script>
     <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-slider.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/magicsuggest-min.js"></script> 
     <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jCProgress-1.0.3.js"></script>
        <!--<script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js?viewer=image"></script>-->
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js"></script>
  <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
      
       MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true } },
          SVG: { linebreaks: { automatic: true } }
        });
  </script>
  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script> -->
</head>
<?php if(Yii::app()->session['userlevel']==1):?>
<body class="professional hidebg">
<?php else:?>
<body class="regular">
<?php endif;?>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php if(Yii::app()->session['userlevel']==1):?>
            <div class="pro-logo-right"><span>Powered by TabbieMe Ltd</span><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" /></div>
            <?php endif;?>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php if(Yii::app()->user->isTeacher()):?>
                    <?php if(Yii::app()->session['userlevel']==1):?>
                        <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('teacherhome')?>"><img style="width: 135px" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo_pro.png"/></a>
                    <?php else:?>
                        <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('teacherhome')?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a>
                    <?php endif;?>
                <?php else:?>
                <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('staffhome')?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a>
                <?php endif;?>
            </div>

                <ul class="nav navbar-top-links navbar-right">
                    <li data-toggle="tooltip" data-placement="bottom" title="Manage groups">
                        <?php $user=User::model()->findByPk(Yii::app()->user->id);?>
                        <a href="<?php echo Yii::app()->createUrl('SchoolGroups/managegroups',array('id'=>$user->school_id))?>">
                            <span class="fa fa-users size2 brd_r"></span>
                        </a>
                    </li>
                    <?php if(Yii::app()->user->isTeacherManageQUestion()):?>
                        <li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Manage questions">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-list-alt size2 brd_r"></span>
                            </a>
                            <ul class="dropdown-menu showsroll">
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('SchoolQuestions/admin',array('id'=>$user->school_id))?>">List Questions</a>
                                </li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('SchoolQuestions/create',array('id'=>$user->school_id))?>">Create Questions</a>
                                </li>
                            </ul>
                        </li>
                    <?php endif;?>
                    <li class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Settings">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="fa fa-cog size2 brd_r"></span>
                        </a>
                        <ul class="dropdown-menu showsroll">
                            <li ><a href="<?php echo Yii::app()->createUrl('teachers/passwordchange')?>">Change Password</a></li>
                            <?php if(Yii::app()->user->isTeacherAdmin()):?>
                            <li ><a href="<?php echo Yii::app()->createUrl('schoolhome')?>" >Switch to admin</a></li>
                            <?php endif;?>
                        </ul>
                    </li>
                    <li data-toggle="tooltip" data-placement="bottom" title="Logout">
                        <?php if(Yii::app()->session['userlevel']==1):?>
                        <a href="<?php echo Yii::app()->createUrl('professional/logout')?>">
                            <?php else:?>
                            <a href="<?php echo Yii::app()->createUrl('user/logout')?>">
                                <?php endif;?>
                            <span class="glyphicon glyphicon-off size2 brd_r"></span>
                        </a>
                    </li>
                </ul>

            <!-- /.navbar-header

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages 
                </li>-->
              
                <!-- /.dropdown 
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts
                </li> -->
                <!-- /.dropdown 
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user 
                </li>-->
                <!-- /.dropdown 
            </ul>-->
            <!-- /.navbar-top-links -->
        <?php $this->widget('TeacherLeftPanelWidget');?>
            
            <!-- /.navbar-static-side -->
        </nav>
    </div>

<?php if(Yii::app()->session['userlevel']==1):?>
<div id="page-wrapper-pro" style="min-height: 678px;">
    <?php else:?>
    <div id="page-wrapper" style="min-height: 678px;">
        <?php endif;?>
    <?php echo $content; ?>
    </div>
</body>
</html>