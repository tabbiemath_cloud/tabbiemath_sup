<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon" />
    <meta name="google" content="notranslate">
	<meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
    <?php if(!Yii::app()->user->isAdmin()):?>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/responsive-full-background-image.css" rel="stylesheet">
    <?php endif;?>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/common.css" rel="stylesheet">
	<!-- radio button css-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/yellow.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css">
    <!-- Morris Charts CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery1_11.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php
    $cs=Yii::app()->clientScript;

    $cs->scriptMap=array(
        'jquery.js'=>false,
        'jquery.min.js'=>false,		
    );?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/icheck.js"></script>
    <!-- <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML"></script> -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.form.js"></script>


      <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js"></script>
    <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        
         MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true } },
          SVG: { linebreaks: { automatic: true } }
        });
    </script>
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script> -->
    
    
    
    <!-- Morris Charts JavaScript -->
    <!--<script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris-data.js"></script>-->

</head>
<style type="text/css">
    #page-wrapper
    {
        background-color: transparent;
    }
</style>
<body>
<?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):?>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo Yii::app()->createUrl('schoolhome')?>" class="navbar-brand" >
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/><br />

            </a>
            <p class="navbar-text" style="padding-top:8%">
                        <?php             
            $masterschool=UserModule::GetSchoolDetails();
            echo '<b>'.ucfirst(User::model()->findByPk(Yii::app()->user->id)->profile->firstname).'</b><br>'.$masterschool->TM_SCL_Name;
            ?> </p>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
<?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
        $this->widget('zii.widgets.CMenu',array(
        'items'=>array(
            array('label'=>'Home', 'url'=>array('/schoolhome')),
            array('label'=>'Subscriptions', 'url'=>array('/school/subscriptions','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),            
            array('label'=>'Admin', 'url'=>array('/school/manageStaff','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffSSupAdmin($masterschool->TM_SCL_Id)),
            array('label'=>'Teachers', 'url'=>array('Teachers/ManageTeachers','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
            array('label'=>'Students', 'url'=>array('/school/manageStudents','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
            array('label'=>'Groups', 'url'=>array('/schoolGroups/managegroups','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
            array('label'=>'Questions', 'url'=>array('/schoolQuestions/admin','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
            //5.html          
            //array('label'=>'Questions', 'url'=>array('/questions/admin'), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),            
            array('label'=>'Settings','url'=>'javascript:void(0)',
                    'linkOptions'=>array('class'=>'dropdown-toggle js-activated','data-toggle'=>'dropdown','aria-expanded'=>'true'),
                    'items'=>array(
                                   // array('label'=>'Profile', 'url'=>array('/publishers/admin')),
                                    array('label'=>'School Profile', 'url'=>array('/school/Schoolprofile','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                                    array('label'=>'Switch to Teacher','url'=>array('/teacherhome'),'visible'=>UserModule::isTeacherAdmin($masterschool->TM_SCL_Id)),                                    
                                    //array('label'=>'Change Password', 'url'=>array('/syllabus/admin')),
//                                    array('label'=>'Question Types', 'url'=>array('/types/admin')),
                    )
                    ,'itemOptions'=>array('class'=>'dropdown')),
            array('label'=>'Logout', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
        ),
        'htmlOptions'=>array('class'=>'nav navbar-right top-nav'),
        'submenuHtmlOptions' => array(
            'class' => 'dropdown-menu',
        ),
    ));
    endif;?>
        </div>
        <?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->widget('zii.widgets.CMenu', array(
                'items'=>$this->menu,
                'firstItemCssClass'=>'nav-header',
                'activeCssClass'=>'active',
                'htmlOptions'=>array('class'=>'nav navbar-nav side-nav'),
            ));
        endif;
        ?>
    </nav>

    <div id="page-wrapper" >
        <div class="container-fluid">
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div>
<?php else:?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
        </div>
<?php endif;?>
</body>
</html>

<!--<script>
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_flat-yellow',
    radioClass: 'iradio_flat-yellow'
  });
});
</script>-->


