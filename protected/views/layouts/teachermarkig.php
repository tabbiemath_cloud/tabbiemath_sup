<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/teacherbootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin-teachermarking.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/teacherprologin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,700,800,900,500,300' rel='stylesheet' type='text/css'>
	
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/stylesheet.css" type="text/css" charset="utf-8">
    	
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/metisMenu.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sb-admin-2.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.bootpag.min.js"></script>    
    <script type="<?php echo Yii::app()->request->baseUrl; ?>/text/javascript" src="js/enscroll-0.4.2.min.js"></script> 
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jCProgress-1.0.3.js"></script>
      <!-- <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
      
       MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true } },
          SVG: { linebreaks: { automatic: true } }
        });
  </script> -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.tdnone {    width: 100%;
    max-width: 100%;
   margin-bottom: 0px !important; }
.tdnone td{    padding: 3px !important;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: none !important;}
	
		.my_wrapper {
   background-color: #F3F3F3;
    color: #5D5D5D;
    padding: 30px;
    display: none;
    border-bottom: 1px solid #ECECEC;
    border-top: 1px solid #ECECEC;
	}
	.lst_stl_my{list-style-type: decimal;    padding-left: 18px;}
    #preloader{
        display: none;
    }
</style>
</head>
<?php if(Yii::app()->session['userlevel']==1):?>
<body class="professional hidebg">
<?php else:?>
<body class="regular">
<?php endif;?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php if(Yii::app()->user->isTeacher()):?>
                    <?php if(Yii::app()->session['userlevel']==1):?>
                        <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('teacherhome')?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo_pro.png"/></a>
                    <?php else:?>
                        <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('teacherhome')?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a>
                    <?php endif;?>
                <?php elseif(Yii::app()->user->isStudent()):?>
                <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('home')?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a>
                <?php else:?>
                <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('staffhome')?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a>
                <?php endif;?>
            </div>
            <!-- /.navbar-header

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages 
                </li>-->
              
                <!-- /.dropdown 
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts
                </li> -->
                <!-- /.dropdown 
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user 
                </li>-->
                <!-- /.dropdown 
            </ul>-->            
        </nav>
    </div>

        <div id="page-wrapper" style="min-height: 678px;">
         <?php echo $content; ?>
        </div>

	<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip(); 
	});

	</script>
	<script>
			$('.demo4_bottom').bootpag({
			total: 5,
			page: 1,
			maxVisible: 5,
			leaps: true,
			firstLastUse: true,
			first: '?',
			last: '?',
			wrapClass: 'pagination',
			activeClass: 'active',
			disabledClass: 'disabled',
			nextClass: 'next',
			prevClass: 'prev',
			lastClass: 'last',
			firstClass: 'first'
		}).on("page", function(event, num){
			$(".content4").html("Page " + num); // or some ajax content loading...
		}); 
	</script>
	
	<script>
		$(document).ready(function() {
		var progressbar = $('.pieprogress');
		max = progressbar.attr('max');
		time = (50 / max) * 5;
		value = progressbar.val();

		var loading = function() {
		value += 1;
		addValue = progressbar.val(value);

		$('.progress-value').html(value + '%');
		var $ppc = $('.progress-pie-chart'),
		deg = 360 * value / 100;
		if (value > 50) {
		$ppc.addClass('gt-50');
		}

		$('.ppc-progress-fill').css('transform', 'rotate(' + deg + 'deg)');
		$('.ppc-percents span').html(value + '%');

		if (value == max) {
		clearInterval(animate);
		}
		};

		var animate = setInterval(function() {
		loading();
		}, time);
		});
	</script>
	
	

    
<!-- -------------------------------------Slide toogle -------------------------------------------------------------------->
	<script type="text/javascript">	
		$(document).ready(function(){
		  $(document).on('click', '.drop', function(){ 			
				$(this).closest( ".list-group-item"  ).find(".drop_row2").slideUp("fast");
				$(this).closest( ".list-group-item"  ).find(".drop_row").slideToggle("slow");
			});
            
        $(document).on('click', '.drop2', function(){             			
				$(this).closest( ".list-group-item"  ).find(".drop_row").slideUp("fast");
				$(this).closest( ".list-group-item"  ).find(".drop_row2").slideToggle("slow");
			});
		});
		
		$(document).ready(function(){
			$("#flip").click(function(){
				$("#panel").slideToggle("slow");
			});
		});

	</script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('.horizontal').enscroll({
                horizontalTrackClass: 'horizontal-track2',
                horizontalHandleClass: 'horizontal-handle2',
                verticalScrolling: false,
                horizontalScrolling: true,
                addPaddingToPane: true
            });
            
        });

        
    </script>

</body>

</html>
