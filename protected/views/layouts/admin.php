<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon" />
    <meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/common.css" rel="stylesheet">
    <?php if(!Yii::app()->user->isAdmin()):?>
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/responsive-full-background-image.css" rel="stylesheet">
    <?php endif;?>

    <!-- radio button css-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/yellow.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css">
    <!-- Morris Charts CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/prologin.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery1_11.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php
    $cs=Yii::app()->clientScript;

    $cs->scriptMap=array(
        'jquery.js'=>false,
        'jquery.min.js'=>false,     
    );?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/plugins/ckeditor_wiris/integration/WIRISplugins.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/icheck.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.form.js"></script>
     <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script> 
     <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML">
        MathJax.Hub.Config({
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true }},
          SVG: { linebreaks: { automatic: true } }
        });
    </script> -->

    

    
    
    
    
    <!-- Morris Charts JavaScript -->
    <!--<script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris-data.js"></script>-->

</head>

<?php if(Yii::app()->session['userlevel']==1):?>
<body class="professional hidebg">
<?php else:?>
<body class="preload">
<?php endif;?>
<?php if(Yii::app()->user->isAdmin()):?>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo Yii::app()->createUrl('administration')?>" class="navbar-brand" >
           <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/>
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
<?php if(Yii::app()->user->isAdmin()):
        $session=new CHttpSession;
        $session->open();                      
        $this->widget('zii.widgets.CMenu',array(
        'items'=>array(
            array('label'=>'Home', 'url'=>array('/administration')),
            array('label'=>'Students', 'url'=>array('/user/admin/studentlist'.($session['urlstring']!=''?'?'.$session['urlstring']:'')), 'visible'=>UserModule::isAllowedStudent()),
            array('label'=>'Parents', 'url'=>array('/user/admin/parentlist'), 'visible'=>UserModule::isAllowedStudent()),
            array('label'=>'Admins', 'url'=>array('/user/admin/adminlist'), 'visible'=>UserModule::isSuperAdmin()),
            array('label'=>'School', 'url'=>array('/school/admin'), 'visible'=>UserModule::isAllowedSchool()),

            array('label'=>'Coupons', 'url'=>array('/coupons/admin'), 'visible'=>UserModule::isSuperAdmin()),
            array('label'=>'Questions', 'url'=>array('/questions/admin'), 'visible'=>UserModule::isAllowedQuestion()),
            array('label'=>'Worksheet', 'url'=>array('/mock/admin'), 'visible'=>UserModule::isAllowedQuestion()),
            array('label'=>'Blueprint', 'url'=>array('/blueprints/admin'), 'visible'=>UserModule::isAllowedQuestion()),
            array('label'=>'Masters','url'=>'javascript:void(0)',
                    'linkOptions'=>array('class'=>'dropdown-toggle js-activated','data-toggle'=>'dropdown','aria-expanded'=>'true'),
                    'items'=>array(
                                    array('label'=>'Publishers', 'url'=>array('/publishers/admin')),
                                    array('label'=>'Syllabus', 'url'=>array('/syllabus/admin')),
                                    array('label'=>'Standard', 'url'=>array('/standard/admin')),
//                                    array('label'=>'Subject', 'url'=>array('/subject/admin')),
                                    array('label'=>'School Plan', 'url'=>array('/plan/manageschoolplan'), 'visible'=>UserModule::isSuperAdmin()),
                                    array('label'=>'Revision Plan', 'url'=>array('/plan/admin'), 'visible'=>UserModule::isSuperAdmin()),
                                    array('label'=>'Chapter', 'url'=>array('/chapter/admin')),
                                    array('label'=>'Topic', 'url'=>array('/topic/admin')),
                                    array('label'=>'Teachers', 'url'=>array('/teachers/admin')),
                                    array('label'=>'Levels', 'url'=>array('/levels/admin')),
                                    array('label'=>'Question Status', 'url'=>array('/questionstatusmaster/admin')),
                                    array('label'=>'Worksheet Tags', 'url'=>array('/mock/tags')),
                                    array('label'=>'Skill', 'url'=>array('/tmSkill/admin')),
                                    array('label'=>'Additional Info', 'url'=>array('/questionType/admin')),

//                                    array('label'=>'Question Types', 'url'=>array('/types/admin')),
                    )
                    ,'itemOptions'=>array('class'=>'dropdown'), 'visible'=>UserModule::isSuperAdmin()),
                    array('label'=>'Report','url'=>'javascript:void(0)',
                    'linkOptions'=>array('class'=>'dropdown-toggle js-activated','data-toggle'=>'dropdown','aria-expanded'=>'true'),
                    'items'=>array(
                                    array('label'=>'Subscription', 'url'=>array('/plan/report')),
                                    array('label'=>'Progress', 'url'=>array('/student/Studentprogressreport')),
                                    array('label'=>'Summary', 'url'=>array('/student/Studentsummaryreport')),
                                    array('label'=>'Teachers Assessment', 'url'=>array('/teachers/assessmentreport')),
                                    array('label'=>'Worksheet Print Report', 'url'=>array('/teachers/WorksheetPrintReport')),
                                    array('label'=>'Login Report', 'url'=>array('/teachers/LoginReport')),
                    )
                    ,'itemOptions'=>array('class'=>'dropdown'), 'visible'=>UserModule::isSuperAdmin()),                    
                    array('label'=>'Logout', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
        ),
        'htmlOptions'=>array('class'=>'nav navbar-right top-nav'),
        'submenuHtmlOptions' => array(
            'class' => 'dropdown-menu',
        ),
    ));
    endif;?>
        </div>
        <?php if(Yii::app()->user->isAdmin()):
            $this->widget('zii.widgets.CMenu', array(
                'items'=>$this->menu,
                'firstItemCssClass'=>'nav-header',
                'activeCssClass'=>'active',
                'htmlOptions'=>array('class'=>'nav navbar-nav side-nav'),
            ));
        endif;
        ?>
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div>
<?php else:?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
        </div>
<?php endif;?>
<?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):?>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo Yii::app()->createUrl('schoolhome')?>" class="navbar-brand" >
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/><br />

                </a>
                <p class="navbar-text" style="padding-top:8%">
                    <?php
                    $masterschool=UserModule::GetSchoolDetails();
                    echo '<b>'.ucfirst(User::model()->findByPk(Yii::app()->user->id)->profile->firstname).'</b><br>'.$masterschool->TM_SCL_Name;
                    ?> </p>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
                    $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array('label'=>'Home', 'url'=>array('/schoolhome')),
                            array('label'=>'Subscriptions', 'url'=>array('/school/subscriptions','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                            array('label'=>'Admin', 'url'=>array('/school/manageStaff','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffSSupAdmin($masterschool->TM_SCL_Id)),
                            array('label'=>'Teachers', 'url'=>array('Teachers/ManageTeachers','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                            array('label'=>'Students', 'url'=>array('/school/manageStudents','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                            array('label'=>'Groups', 'url'=>array('/schoolGroups/managegroups','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                            array('label'=>'Questions', 'url'=>array('/schoolQuestions/admin','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                            // array('label'=>'Blue Print', 'url'=>array('/blueprints/admin'), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                            //5.html
                            //array('label'=>'Questions', 'url'=>array('/questions/admin'), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                            array('label'=>'Settings','url'=>'javascript:void(0)',
                                'linkOptions'=>array('class'=>'dropdown-toggle js-activated','data-toggle'=>'dropdown','aria-expanded'=>'true'),
                                'items'=>array(
                                    // array('label'=>'Profile', 'url'=>array('/publishers/admin')),
                                    array('label'=>'School Profile', 'url'=>array('/school/Schoolprofile','id'=>$masterschool->TM_SCL_Id), 'visible'=>UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)),
                                    array('label'=>'Switch to Teacher','url'=>array('/teacherhome'),'visible'=>UserModule::isTeacherAdmin($masterschool->TM_SCL_Id)),
                                    //array('label'=>'Change Password', 'url'=>array('/syllabus/admin')),
//                                    array('label'=>'Question Types', 'url'=>array('/types/admin')),
                                )
                            ,'itemOptions'=>array('class'=>'dropdown')),
                            array('label'=>'Logout', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
                        ),
                        'htmlOptions'=>array('class'=>'nav navbar-right top-nav'),
                        'submenuHtmlOptions' => array(
                            'class' => 'dropdown-menu',
                        ),
                    ));
                endif;?>
            </div>
            <?php if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'firstItemCssClass'=>'nav-header',
                    'activeCssClass'=>'active',
                    'htmlOptions'=>array('class'=>'nav navbar-nav side-nav'),
                ));
            endif;
            ?>
        </nav>
    </div>

<?php endif;?>
</body>
</html>

<!--<script>
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_flat-yellow',
    radioClass: 'iradio_flat-yellow'
  });
});
</script>-->


