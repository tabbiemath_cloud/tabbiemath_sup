<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if lt IE 9]>
    <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- responsive-full-background-image.css stylesheet contains the code you want -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/responsive-full-background-image.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/frontendbootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/hover.css" rel="stylesheet" media="all">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/dashboard.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/slider.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/progress.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magicsuggest-min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/rwd-tables.css">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php
    $cs=Yii::app()->clientScript;

    $cs->scriptMap=array(
        'jquery.js'=>false,
        'jquery.min.js'=>false,
    );?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery1_11.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-slider.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-asPieProgress.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/magicsuggest-min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.rwd-tables.js"></script>
    <script>
        // With JQuery
        $('#ex1').slider({
            formatter: function(value) {
                return 'Current value: ' + value;
            }
        }).on('slideStop', function(ev){
            var newVal = $('#ex1').data('slider').getValue();
            $('#adv').val(newVal);
        });
        /*
         // Without JQuery
         var slider = new Slider('#ex1', {
         formatter: function(value) {
         return 'Current value: ' + value;
         }
         });*/

    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            /*        $('.showslide').click(function(){
             var identi=$(this).attr('data-uniq');
             $('#slide'+identi+'Slider').toggle()
             });*/
            $(".tip-top").tooltip({
                placement : 'top'
            });
            $(".tip-right").tooltip({
                placement : 'right'
            });
            $(".tip-bottom").tooltip({
                placement : 'bottom'
            });
            $(".tip-left").tooltip({
                placement : 'left'
            });


        });
    </script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('home')?>"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png'); ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!--<ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-home size2 brd_r"></span>
                    </a>
                </li>

                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-trophy size2 brd_r"></span>
                    </a>
                </li>

                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-cog size2 brd_r"></span>
                    </a>
                </li>
                <!--<li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-comments size2 brd_r"></span>
                    <span class="label label-primary">5</span>
                    </a>
                </li>-->


            <!--<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-bell fa-2x brd_r"></span>
                    <span class="label label-primary">42</span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#"><span class="label label-warning">7:00 AM</span>Hi :)</a></li>
                    <li><a href="#"><span class="label label-warning">8:00 AM</span>How are you?</a></li>
                    <li><a href="#"><span class="label label-warning">9:00 AM</span>What are you doing?</a></li>
                    <li class="divider"></li>
                    <li><a href="#" class="text-center">View All</a></li>
                </ul>
            </li>
        </ul>-->
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <?php $this->widget('LeftPanelWidget');?>
        <?php echo $content; ?>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>