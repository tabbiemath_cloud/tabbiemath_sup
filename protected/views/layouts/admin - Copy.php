<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->
    <!--<script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris.min.js"></script>
    <script src="<?php /*echo Yii::app()->request->baseUrl; */?>/js/plugins/morris/morris-data.js"></script>-->

</head>

<body>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" ><?php echo CHtml::encode(Yii::app()->name); ?></a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
<?php if(Yii::app()->user->isAdmin()):
        $this->widget('zii.widgets.CMenu',array(
        'items'=>array(
            array('label'=>'Home', 'url'=>array('/administration')),
            array('label'=>'Users', 'url'=>array('/user/admin')),
            array('label'=>'Publishers', 'url'=>array('/publishers/admin')),
            array('label'=>'Syllabus', 'url'=>array('/syllabus/admin')),
            array('label'=>'Standard', 'url'=>array('/standard/admin')),
            array('label'=>'Subject', 'url'=>array('/subject/admin')),
            array('label'=>'Topic', 'url'=>array('/topic/admin')),
            array('label'=>'Section', 'url'=>array('/sections/admin')),
            array('label'=>'Plan', 'url'=>array('/plan/admin')),
            array('label'=>'Question Types', 'url'=>array('/types/admin')),
            array('label'=>'Coupons', 'url'=>array('/coupons/admin')),
            array('label'=>'Logout', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
        ),
        'htmlOptions'=>array('class'=>'nav navbar-right top-nav'),
    ));
    endif;?>
        </div>
        <?php
        $this->widget('zii.widgets.CMenu', array(
            'items'=>$this->menu,
            'firstItemCssClass'=>'nav-header',
            'activeCssClass'=>'active',
            'htmlOptions'=>array('class'=>'nav navbar-nav side-nav'),
        ));

        ?>
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
