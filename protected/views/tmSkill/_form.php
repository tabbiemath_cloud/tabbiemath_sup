<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('tmSkill/admin')."';
    });
");
?>

<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'syllabus-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="panel-body form-horizontal payment-form">
    <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>




        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_sk_Syllabus_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_sk_Syllabus_Id',CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty'=>'Select Syllabus','class'=>'form-control','ajax' =>
                array(
                    'type'=>'POST', //request type
                    'url'=>CController::createUrl('questions/standardlist'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
    //Style: CController::createUrl('currentController/methodToCall')
                    'update'=>'#TmSkill_tm_standard_id', //selector to update
                'data'=>'js:{id:$(this).val()}'
                //leave out the data key to pass all form values through
                ))); ?>
                <?php echo $form->error($model,'TM_sk_Syllabus_Id'); ?>
            </div>
        </div>
        
        <div class="form-group">
            <?php echo $form->labelEx($model,'tm_standard_id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php
                if($model->isNewRecord):
                    echo $form->dropDownList($model,'tm_standard_id',array(''=>'Select Standard'),array('class'=>'form-control'));
                else:
                    echo $form->dropDownList($model,'tm_standard_id',CHtml::listData(Standard::model()->findAll(array('condition'=>"TM_SD_Syllabus_Id =  $model->TM_sk_Syllabus_Id",'order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),array('empty'=>'Select Standard','class'=>'form-control'));
                endif;
                ?>
                <?php echo $form->error($model,'tm_standard_id'); ?>
            </div>
        </div>

<!--         <div class="form-group"> 
            <?php echo $form->labelEx($model,'tm_standard_id',array('class'=>'col-sm-3 control-label'));?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'tm_standard_id',CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),array('empty'=>'Select','class'=>'form-control','ajax' =>
            array(
                'type'=>'POST', //request type
                'url'=>CController::createUrl('questions/standardlist'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                //Style: CController::createUrl('currentController/methodToCall')
                'update'=>'#syllabus', //selector to update
                'data'=>'js:{id:$(this).val()}'
                //leave out the data key to pass all form values through
            )
            )); ?>
                <?php echo $form->error($model,'tm_standard_id'); ?>
            </div>
        </div> -->
        
        <div class="form-group">

		</div>

	    <div class="form-group">
			<?php echo $form->labelEx($model,'tm_skill_name',array('class'=>'col-sm-3 control-label')); ?>
	        <div class="col-sm-9">
	            <?php echo $form->textField($model,'tm_skill_name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
	            <?php echo $form->error($model,'tm_skill_name'); ?>
	        </div>
		</div>



	<!--<div class="row">
		<?php /*echo $form->labelEx($model,'TM_SB_CreatedOn'); */?>
		<?php /*echo $form->textField($model,'TM_SB_CreatedOn'); */?>
		<?php /*echo $form->error($model,'TM_SB_CreatedOn'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->labelEx($model,'TM_SB_CreatedBy'); */?>
		<?php /*echo $form->textField($model,'TM_SB_CreatedBy'); */?>
		<?php /*echo $form->error($model,'TM_SB_CreatedBy'); */?>
	</div>-->

    <div class="form-group">
        <div class="raw">
        <div class="col-sm-3 pull-right">
		    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
        </div>
            <div class="col-lg-3 pull-right">
                <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
            </div>
        </div>
	</div>

<?php $this->endWidget(); ?>
</div>
</div><!-- form -->

<script type="text/javascript">
	$(document).on('change','#TmSkill_tm_standard_id', function(){
		var standard = $(this).val();

	});
</script>