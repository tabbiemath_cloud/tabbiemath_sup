<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */

$this->breadcrumbs=array(
	'Syllabuses'=>array('admin'),
	$model->id,
);

$this->menu=array(
    array('label'=>'Manage Skill', 'class'=>'nav-header'),
	array('label'=>'List Skill', 'url'=>array('admin')),
	array('label'=>'Create Skill', 'url'=>array('create')),
	array('label'=>'Update Skill', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Skill', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View <?php echo $model->tm_skill_name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'id',
                'tm_skill_name',
                array(
                        'name'=>'tm_standard_id',
                        'type'=>'raw',
                        'value'=>function($model)
                         {
                            $sdname=Standard::model()->findByPk($model->tm_standard_id)->TM_SD_Name;
                            return $sdname; 
                            
                        },
                    ),

                    array(
                        'name'=>'Syllabus',
                        'type'=>'raw',
                        'header'=>'Syllabus',
                        'value'=>function($model)
                         {
                             $syllabus_name=Syllabus::model()->findByPk($model->TM_sk_Syllabus_Id)->TM_SB_Name;
                            return $syllabus_name; 
                            
                        },
                     ),

            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
