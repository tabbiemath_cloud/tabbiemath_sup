<?php
/* @var $this TmSkillController */
/* @var $model TmSkill */

$this->breadcrumbs=array(
	'Skills'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Skill', 'url'=>array('admin')),
	//array('label'=>'Manage Skill', 'url'=>array('admin')),
);
?>

<h1>Create Skill</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>