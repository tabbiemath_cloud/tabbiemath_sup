<?php
/* @var $this TmSkillController */
/* @var $model TmSkill */

$this->breadcrumbs=array(
	'Tm Skills'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Skill', 'url'=>array('admin')),
	array('label'=>'Create Skill', 'url'=>array('create')),
	array('label'=>'View Skill', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Skill', 'url'=>array('admin')),
);
?>

<h1>Update <?php echo $model->tm_skill_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>