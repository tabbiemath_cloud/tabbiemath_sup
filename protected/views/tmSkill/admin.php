<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */

$this->breadcrumbs=array(
	'Skills'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Skill', 'class'=>'nav-header'),
	array('label'=>'List Skill', 'url'=>array('admin')),
	array('label'=>'Create Skill', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#syllabus-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>Manage Skills</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'syllabus-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                'tm_skill_name',

                array(
                        'name'=>'TM_sk_Syllabus_Id',
                        'type'=>'raw',
                        'header'=>'Syllabus',
                        'value'=>function($model)
                         {
                           
                            $syllabus_name=Syllabus::model()->findByPk($model->TM_sk_Syllabus_Id)->TM_SB_Name;
                            return $syllabus_name; 
                            
                        },
                         'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),
                    ),

                array(
                    'name'=>'tm_standard_id',
                    'value'=>'Standard::model()->findByPk($data->tm_standard_id)->TM_SD_Name',
                    'filter'=>CHtml::listData(Standard::model()->findAll(array('condition' => "TM_SD_Syllabus_Id = :syllabus",
                        'params' => array(':syllabus' => $model->TM_sk_Syllabus_Id),
                        'order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),
                    ),

                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            )),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
