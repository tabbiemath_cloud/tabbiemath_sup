<?php
/* @var $this TmSkillController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tm Skills',
);

$this->menu=array(
	array('label'=>'Create TmSkill', 'url'=>array('create')),
	//array('label'=>'Manage TmSkill', 'url'=>array('admin')),
);
?>

<h1>Tm Skills</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
