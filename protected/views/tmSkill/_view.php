<?php
/* @var $this TmSkillController */
/* @var $data TmSkill */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tm_skill_name')); ?>:</b>
	<?php echo CHtml::encode($data->tm_skill_name); ?>
	<br />


</div>