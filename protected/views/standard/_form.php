<?php
/* @var $this StandardController */
/* @var $model Standard */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('standard/admin')."';
    });
");
?>

<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'standard-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SD_Syllabus_Id',array('class'=>'col-sm-3 control-label'));?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_SD_Syllabus_Id',CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty'=>'Select','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_SD_Syllabus_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SD_Name',array('class'=>'col-sm-3 control-label'));?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SD_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_SD_Name'); ?>
            </div>
        </div>

        <!--<div class="row">
            <?php /*echo $form->labelEx($model,'TM_SD_CreatedOn'); */?>
            <?php /*echo $form->textField($model,'TM_SD_CreatedOn'); */?>
            <?php /*echo $form->error($model,'TM_SD_CreatedOn'); */?>
        </div>

        <div class="row">
            <?php /*echo $form->labelEx($model,'TM_SD_CreatedBy'); */?>
            <?php /*echo $form->textField($model,'TM_SD_CreatedBy'); */?>
            <?php /*echo $form->error($model,'TM_SD_CreatedBy'); */?>
        </div>-->

        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SD_Status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->hiddenField($model,'TM_SD_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
                <?php echo $form->dropDownList($model,'TM_SD_Status',Standard::itemAlias('StandardStatus'),array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_SD_Status'); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="raw">


            <div class="col-sm-3 pull-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->