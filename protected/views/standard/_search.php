<?php
/* @var $this StandardController */
/* @var $model Standard */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_SD_Id'); ?>
		<?php echo $form->textField($model,'TM_SD_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SD_Name'); ?>
		<?php echo $form->textField($model,'TM_SD_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SD_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_SD_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SD_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_SD_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SD_Status'); ?>
		<?php echo $form->textField($model,'TM_SD_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->