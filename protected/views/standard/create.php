<?php
/* @var $this StandardController */
/* @var $model Standard */

$this->breadcrumbs=array(
	'Standards'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Standard', 'class'=>'nav-header'),
	array('label'=>'List Standard', 'url'=>array('admin')),

);
?>

<h3>Create Standard</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>