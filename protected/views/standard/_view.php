<?php
/* @var $this StandardController */
/* @var $data Standard */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SD_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_SD_Id), array('view', 'id'=>$data->TM_SD_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SD_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SD_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SD_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SD_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SD_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SD_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SD_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SD_Status); ?>
	<br />


</div>