<?php
/* @var $this StandardController */
/* @var $model Standard */

$this->breadcrumbs=array(
	'Standards'=>array('admin'),
	$model->TM_SD_Id=>array('view','id'=>$model->TM_SD_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Standard', 'class'=>'nav-header'),
	array('label'=>'List Standard', 'url'=>array('admin')),
	array('label'=>'Create Standard', 'url'=>array('create')),
	array('label'=>'View Standard', 'url'=>array('view', 'id'=>$model->TM_SD_Id)),

);
?>

<h3>Update <?php echo $model->TM_SD_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>