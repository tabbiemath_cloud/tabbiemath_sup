<?php
/* @var $this StandardController */
/* @var $model Standard */

$this->breadcrumbs=array(
	'Standards'=>array('admin'),
	$model->TM_SD_Id,
);

$this->menu=array(
    array('label'=>'Manage Standard', 'class'=>'nav-header'),
	array('label'=>'List Standard', 'url'=>array('admin')),
	array('label'=>'Create Standard', 'url'=>array('create')),
	array('label'=>'Update Standard', 'url'=>array('update', 'id'=>$model->TM_SD_Id)),
	array('label'=>'Delete Standard', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_SD_Id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View <?php echo $model->TM_SD_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $id=$model->TM_SD_Status;
        $userid=$model->TM_SD_CreatedBy;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_SD_Id',
                array(
                    'name'=>'TM_SD_Syllabus_Id',
                    'type'=>'raw',
                    'value'=>Syllabus::model()->findByPk($model->TM_SD_Syllabus_Id)->TM_SB_Name,
                ),
                'TM_SD_Name',
                'TM_SD_CreatedOn',
                array(
                    'label'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Standard::itemAlias("StandardStatus",$id),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
