<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
        <div class="bs-example">
            <h1 class="h125">Buddies</h1>

            <div class="panel panel-default clearfix" id="QuickChapters">
                <div class="panel-body padnone">

                    <div class="panel-heading">

                        <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                            <div id="custom-search-input">
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control input-sm" placeholder="Search & invite buddies"
                                           id="buddyname">
										<span class="input-group-btn">
											<button class="btn btn-warning btn-sm" type="button" id="buddysearch">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
										</span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 col-sm-6 col-xs-12" style="
    display: initialboth;
    margin-top: 10px;
    margin-right: 2px;
">

                                <a class="btn btn-danger btn-xs align-right" id="pendingbuddies" style="margin-right: 3px; <?php if(count($buddiespending)==0) echo 'display: none;' ?>" >Pending Requests
                                    <span class="glyphicon glyphicon-user" ></span>
                                </a>


                            <a class="btn btn-warning btn-xs align-right"id="mybuddies">My Buddies
                                <span class="fa fa-users"></span></a>
                        </div>
                    </div>

                </div>


                <div class="col-md-12" style="margin-top: 20px">

                    <ul class="list-group lst_grp_nonmrg" id="buddylist">
                    
                        <?php
                        if(count($buddies)>0):
                            foreach ($buddies AS $buddyname):
                                $buddyuser=User::model()->findbyPk($buddyname->TM_BD_Student_Id);
                                $studentimage=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$buddyuser->id)); 
                                ?>
                                <li href="#" class="list-group-item col-md-4 mrgbotm1 brdno text-left buddy<?php echo $buddyname->TM_BD_Id;?>">
                                        <img class="img-thumbnail img-circle" src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($studentimage->TM_STU_Image!=''?$studentimage->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140';?>">
                                         <label class="fno">
                                            <?php echo $buddyuser->profile->firstname.' '.$buddyuser->profile->lastname; ?><br>
                                            <?php echo $buddyuser->username ?>         
                                         </label>
                                		<div class="bottom_button">
                                            <?php if($buddyname->TM_BD_Status=='0'):?>
                                                    <a class="btn btn-warning btn-outline btn-xs removebuddy mybutton1" data-parent="buddy<?php echo $buddyname->TM_BD_Id;?>" data-id="<?php echo $buddyname->TM_BD_Id;?>">Remove</a>                
                                            <?php else:?>                
                                                    <a class="btn btn-warning btn-outline btn-xs rejectbuddy mybutton1" data-parent="buddy<?php echo $buddyname->TM_BD_Id;?>" data-id="<?php echo $buddyname->TM_BD_Id;?>">Cancel</a>                
                                                    <span class="label label-success labelposition">Request sent</span>
                                            <?php endif;?>  
                                		</div>
                                </li>
                                

                        <?php endforeach;
                        else:
                        ?>
                            <li href="#" class="list-group-item col-md-4 brdno text-left">
                                <!--<img class="img-thumbnail img-circle" src="http://bootdey.com/img/Content/user_1.jpg">-->
                                <label class="fno">No buddies found</label>
                            </li>
                        <?php endif;?>
                    </ul>
                </div>


                <!------- add Buddies-->




            </div>

        </div>

    </div>


</div>

<script>
    $(document).ready(function () {
        $('.table-responsive').scrollLeft(9999);
    });
</script>
<?php
if($_GET['type']=='pending'):
    Yii::app()->clientScript->registerScript('buddylist', "
        $(document).ready(function () {
            $('#pendingbuddies').trigger('click');
        });
    ");
endif;
Yii::app()->clientScript->registerScript('buddyserach', "
$(document).on('click','#pendingbuddies',function(){

var buddyname=$('#buddyname').val();
              $.post('" . Yii::app()->createUrl('buddies/GetBuddyRequests') . "',  function(data){
                  $('#buddylist').html(data);
              },'html');
});
$(document).on('click','#mybuddies',function(){

var buddyname=$('#buddyname').val();
              $.post('" . Yii::app()->createUrl('buddies/GetMyBuddies') . "',  function(data){
                  $('#buddylist').html(data);
              },'html');
});
$(document).on('click','#buddysearch',function(){

var buddyname=$('#buddyname').val();
            if(buddyname!='')
            {
              $.post('" . Yii::app()->createUrl('buddies/BuddySearch') . "', {buddyname: buddyname},  function(data){
                  $('#buddylist').html(data);
              },'html');

              }

});


    $(document).on('click','.confirmbuddy',function(){
        var parent=$(this).data('parent');
        var buddyid=$(this).data('id');
        var item=$(this);
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('Buddies/addBuddy')."',
              data: { buddyid: buddyid}
            })
            .done  (function(data){
                if(data=='yes')
                {
                    item.parent('.bottom_button').append('<span class=\"label label-success labelposition message\">Request approved</span>');
                    setTimeout(function(){
                    $('.message').remove();
                    $('.'+parent).remove();
                    }, 3000);
                    var numItems = $('.budddysent').length
                    if(numItems>0){
                    $('#pendingbuddies').show();
                    }
                }

            })
    });
    $(document).on('click','.rejectbuddy',function()
    {

        if(confirm('Are you sure you want to cancel buddy request?'))
        {
            var parent=$(this).data('parent');
            var buddyid=$(this).data('id');
            var item=$(this);
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('Buddies/rejectBuddy')."',
              data: { buddyid: buddyid}

            })
            .done  (function(data){
                if(data=='yes')
                {
                    item.parent('.bottom_button').append('<span class=\"label label-success labelposition message\">Request rejected</span>');
                    setTimeout(function(){
                    var numItems = $('.rejectbuddy').length
                    if(numItems>0){
                    $('#pendingbuddies').hide();
                    }
                    $('.message').remove();
                    $('.'+parent).remove();
                    }, 3000);

                }
            });
        }

    });
    $(document).on('click','.removebuddy',function()
    {
        if(confirm('Are you sure you want to remove buddy?'))
        {
            var parent=$(this).data('parent');
            var buddyid=$(this).data('id');
            var item=$(this);
                $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('Buddies/removeBuddy')."',
                  data: { buddyid: buddyid}
                })
                .done  (function(data){
                    if(data=='yes')
                    {
                        item.hide()
                        item.parent('.bottom_button').append('<span class=\"label label-success labelposition message\">Buddy removed</span>');
                        setTimeout(function(){
                            $('.message').remove();
                            $('.'+parent).remove();
                            var numItems = $('.budddysent').length
                    if(numItems==0){
                    $('#pendingbuddies').hide();
                    }
                        }, 3000);


                    }

                })
        }

    });
     $(document).on('click','.addnew',function(){
        var parent=$(this).data('parent');
        var buddyid=$(this).data('id');
        var item=$(this);
            $.ajax({
                      type: 'POST',
                      url: '".Yii::app()->createUrl('Buddies/addNew')."',
                      data: { buddyid: buddyid}

                    })
        .done  (function(data){            
            if(data!='')
            {
                item.hide()                
                item.parent('.bottom_button').html('<span class=\"label label-success labelposition message budddysent\">Request sent</span><a class=\"btn btn-warning btn-outline btn-xs rejectbuddy\" data-parent=\"'+parent+'\" data-id=\"'+data+'\">Cancel</a>');
                /*setTimeout(function(){
                    $('.message').remove();
                }, 3000);*/
                    var numItems = $('.budddysent').length
                    if(numItems>0){
                    $('#pendingbuddies').show();
                    }
            }
            //$('.'+parent).remove();
         })


    });
    $('#buddyname').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
    $('#buddysearch').click();
    return false;
  }
});  ");?>
<style>
.bottom_button {position: relative; width: 100%; bottom: 0; height: 25px;}
.mrgbotm1{margin-bottom: 5px;}
.mybutton1{height: 18px;font-size: 10px;line-height: 11px;margin-top: 4px;}
</style>