<div class="row">
    <div class="col-lg-12">
        <h3><?php echo $homework->TM_SCH_Name;?> Question Status</h3>        
    </div>    
</div>
<div class="row brd1">
    <div class="col-lg-12">
        <div class="panel panel-default">
                <div class="table-responsive shadow-z-1">
                    <table class="table" style="background-color: #ffffff;">
                        <thead>

                        </thead>
                        <tbody class="font3 wirisfont">
                        <?php
                            $script='';
                            $count=1;                        
                         foreach($hwquestions AS $hwquestion):
                                $question=Questions::model()->findByPk($hwquestion['TM_HQ_Question_Id']);
                        ?>
                            <tr>
                            	<td>
                                    <?php
                                    if ($question->TM_QN_Type_Id != '5'):
                                    ?>
                                        <div class="col-md-4">
                                            <span class="clrr">Question <?php echo /*$hwquestion['TM_HQ_Order'];*/ $count;?></span>
                                        </div>
                            		    <div class="col-md-3 pull-right"></div>
                            		    <div class="<?php echo ($question->TM_QN_Image != '' ? 'col-md-9' : 'col-md-12');?>"><?php echo $question->TM_QN_Question;?></div>
                                        <?php if($question->TM_QN_Image != ''):?>
                                            <div class="col-md-3"><img class="img-responsive img_right pull-right" src="<?php echo Yii::app()->request->baseUrl;?>/site/Imagerender/id/<?php echo $question->TM_QN_Image;?>?size=thumbs&type=question&width=200&height=200" alt=""></div>
                                        <?php endif;?>
                                        <div class="col-md-12">
                                           <div class="col-md-1 col-xs-6">
                                                <!--<div class="btmyy">
                                                <div id="pbar" class="progress-pie-chart" data-percent="0">
                                                <div class="ppc-progress">
                                                <div class="ppc-progress-fill" ></div>
                                                </div>
                                                <div class="ppc-percents">
                                                <div class="pcc-percents-wrapper">
                                                <span><?php echo $this->GetIndividualquestionstatusnew($hwquestion->TM_HQ_Question_Id,$hwquestion->TM_HQ_Homework_Id,$attstds) ;?>%</span>
                                                </div>
                                                </div>
                                                </div>
                                                <progress style="display: none" id="progress_bar" class="pieprogress"  value="0" max="<?php echo $this->GetIndividualquestionstatusnew($hwquestion->TM_HQ_Question_Id,$hwquestion->TM_HQ_Homework_Id,$attstds) ;?>"></progress>
                                                </div>-->
                                               <?php if ($question->TM_QN_Type_Id == '6' || $question->TM_QN_Type_Id == '7'):?>
                                                <div id="<?php echo 'prog'.$count;?>" class="containerdiv">
                                                    <div class="jCProgress" style="opacity: 1;">
                                                        <div class="percent" style="display: none;"><?php echo $this->GetIndividualquestionstatusnew($hwquestion['TM_HQ_Question_Id'],$hwquestion['TM_HQ_Homework_Id'],$attstds);?></div>
                                                        <canvas width="55" height="55"></canvas>
                                                    </div>
                                                </div>
                                               <?php /*$script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($hwquestion['TM_HQ_Question_Id'],$hwquestion['TM_HQ_Homework_Id']).",loop : false,showPercent : true});"; */?>
                                                   <?php if(Yii::app()->session['userlevel']==1):?>
                                                       <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($hwquestion['TM_HQ_Question_Id'],$hwquestion['TM_HQ_Homework_Id'],$attstds).",loop : false,showPercent : true});"; ?>
                                                   <?php else:?>
                                                       <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($hwquestion['TM_HQ_Question_Id'],$hwquestion['TM_HQ_Homework_Id'],$attstds).",loop : false,showPercent : true});"; ?>
                                                   <?php endif;?>
                                               <?php else:?>
                                                   <div id="<?php echo 'prog'.$count;?>" class="containerdiv">
                                                       <div class="jCProgress" style="opacity: 1;">
                                                           <div class="percent" style="display: none;"><?php echo $this->GetIndividualquestionstatusnew($hwquestion['TM_HQ_Question_Id'],$hwquestion['TM_HQ_Homework_Id'],$attstds);?></div>
                                                           <canvas width="55" height="55"></canvas>
                                                       </div>
                                                   </div>
                                                   <?php if(Yii::app()->session['userlevel']==1):?>
                                                       <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($hwquestion['TM_HQ_Question_Id'],$hwquestion['TM_HQ_Homework_Id'],$attstds).",loop : false,showPercent : true});"; ?>
                                                   <?php else:?>
                                                       <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($hwquestion['TM_HQ_Question_Id'],$hwquestion['TM_HQ_Homework_Id'],$attstds).",loop : false,showPercent : true});"; ?>
                                                   <?php endif;?>
                                               <?php endif;?>
                                                <?php /*$script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($hwquestion->TM_HQ_Question_Id,$hwquestion->TM_HQ_Homework_Id).",loop : false,showPercent : true});"; */?>
                                            </div>
                                            <div class="col-md-2 col-xs-6">
                                                <span style="float: left;margin-top: 20px;font-weight: bold;">Got this correct</span>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-4">
                                            <span class="clrr">Question <?php echo /*$hwquestion['TM_HQ_Order'];*/ $count;?></span>
                                        </div>
                                        <div class="col-md-3 pull-right"></div>
                                        <div class="<?php echo ($question->TM_QN_Image != '' ? 'col-md-9' : 'col-md-12');?>"><?php echo $question->TM_QN_Question;?></div>
                                        <?php if($question->TM_QN_Image != ''):?>
                                            <div class="col-md-3"><img class="img-responsive img_right pull-right" src="<?php echo Yii::app()->request->baseUrl;?>/site/Imagerender/id/<?php echo $childquestion->TM_QN_Image;?>?size=thumbs&type=question&width=200&height=200" alt=""></div>
                                        <?php endif; ?>
                                        <?php
                                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                                        $multicount = 1;
                                        foreach ($questions AS $childquestion):
                                            // //$multipercent = HomeworkQuestions::model()->findByAttributes(array('TM_HQ_Question_Id'=>$childquestion->TM_QN_Id));
                                            // echo $hwquestion->TM_HQ_Homework_Id; exit;
                                            ?>
                                            <div class="col-md-12"><p>Part <?php echo $multicount; ?> </p><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10"> <?php echo $childquestion->TM_QN_Question; ?></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-1 col-xs-6">
                                                    <!--<div class="btmyy">
                                            <div id="pbar" class="progress-pie-chart" data-percent="0">
                                            <div class="ppc-progress">
                                            <div class="ppc-progress-fill" ></div>
                                            </div>
                                            <div class="ppc-percents">
                                            <div class="pcc-percents-wrapper">
                                            <span><?php echo $this->GetIndividualquestionstatusnew($childquestion->TM_QN_Id,$hwquestion['TM_HQ_Homework_Id'],$attstds) ;?>%</span>
                                            </div>
                                            </div>
                                            </div>
                                            <progress style="display: none" id="progress_bar" class="pieprogress"  value="0" max="<?php echo $this->GetIndividualquestionstatusnew($childquestion->TM_QN_Id,$hwquestion['TM_HQ_Homework_Id'],$attstds) ;?>"></progress>
                                            </div>-->
                                                    <div id="<?php echo 'prog'.$count;?>" class="containerdiv">
                                                        <div class="jCProgress" style="opacity: 1;">
                                                            <div class="percent" style="display: none;"><?php echo $this->GetIndividualquestionstatusnew($childquestion->TM_QN_Id,$hwquestion['TM_HQ_Homework_Id'],$attstds);?></div>
                                                            <canvas width="55" height="55"></canvas>
                                                        </div>
                                                    </div>
                                                    <?php if(Yii::app()->session['userlevel']==1):?>
                                                        <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($childquestion->TM_QN_Id,$hwquestion->TM_HQ_Homework_Id,$attstds).",loop : false,showPercent : true});"; ?>
                                                    <?php else:?>
                                                        <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetIndividualquestionstatusnew($childquestion->TM_QN_Id,$hwquestion->TM_HQ_Homework_Id,$attstds).",loop : false,showPercent : true});"; ?>
                                                    <?php endif;?>
                                                </div>
                                                <div class="col-md-2 col-xs-6">
                                                    <span style="float: left;margin-top: 20px;font-weight: bold;">Got this correct</span>
                                                </div>
                                            </div>
                                            <?php
                                            $multicount++;
                                            $count++;
                                        endforeach;
                                    endif;?>
                            	<td>
                            <tr>
                        <?php 
                        $count++;
                        endforeach;?>
                        </tbody>
                    </table>
                </div>            
        </div>
    </div>
</div>
<script>
		$(document).ready(function() {
		  
        <?php echo $script;?>          
		var progressbar = $('.pieprogress');
		max = progressbar.attr('max');        
		time = (50 / max) * 5;
		value = progressbar.val();        
		var loading = function() {
		  value += 1;
		  addValue = progressbar.val(value);

    		$('.progress-value').html(value + '%');
    		var $ppc = $('.progress-pie-chart'),
    		deg = 360 * value / 100;
    		if (value > 50) {
    		$ppc.addClass('gt-50');
    		}

    		$('.ppc-progress-fill').css('transform', 'rotate(' + deg + 'deg)');
    		$('.ppc-percents span').html(value + '%');

    		if (value == max) {
    		clearInterval(animate);
    		}
    		};

    		var animate = setInterval(function() {
    		loading();
    		}, time);
    		});            
	</script>