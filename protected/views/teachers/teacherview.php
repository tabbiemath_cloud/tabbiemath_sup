<?php
$this->breadcrumbs=array(
    'Schools'=>array('admin'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Admin', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$school), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'List Teachers', 'url'=>array('manageTeachers', 'id'=>$school)),
    array('label'=>'Add Teacher', 'url'=>array('AddTeacher', 'id'=>$school)),

    array('label'=>'Change Password', 'url'=>array('TeacherPassword','id'=>$school,'master'=>$model->id)),
    array('label'=>'Update Teacher', 'url'=>array('EditTeachers','id'=>$school,'master'=>$model->id)),
    array('label'=>'Manage Standards', 'url'=>array('AddStandards', 'id'=>$school,'master'=>$model->id)),
    array('label'=>'Delete Teacher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('DeleteTeachers', 'id'=>$school,'master'=>$model->id),'confirm'=>'Are you sure you want to delete this Teacher?')),
);
?>
<h3>View Teacher <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover" id="yw0">
            <tr>
                <th>Username</th>
                <td><?php echo $model->username;?></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><?php echo $model->profile->firstname;?></td>
            </tr>

            <tr>
                <th>Last Name</th>
                <td><?php echo $model->profile->lastname;?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $model->email;?></td>
            </tr>

            <tr>
                <th>Phone Number</th>
                <td><?php echo $model->profile->phonenumber;?></td>
            </tr>
            <tr>
                <th>School</th>
                <td><?php echo School::model()->findByPk($model->school_id)->TM_SCL_Name;?></td>
            </tr>
            <tr>
                <th>Standards</th>
                <td><?php echo $this->getstandards($model->id);?></td>
            </tr>
            <tr>
                <th>Manage Questions</th>
                <td><?php echo ($schoolstaff->TM_TH_Manage_Questions?'Yes':'No');?></td>
            </tr>
            <tr>
                <th>User Type</th>
                <td><?php echo ($schoolstaff->TM_TH_UserType?'Professional':'Regular');?></td>
            </tr>
        </table>
    </div>
</div>
