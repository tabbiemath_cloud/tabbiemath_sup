<?php
/* @var $this TeachersController */
/* @var $model Teachers */

$this->breadcrumbs=array(
	'Teachers'=>array('admin'),
	$model->TM_TH_Id,
);

$this->menu=array(
    array('label'=>'Manage Teachers', 'class'=>'nav-header'),
	array('label'=>'List Teachers', 'url'=>array('admin')),
	array('label'=>'Create Teacher', 'url'=>array('create')),
	array('label'=>'Update Teacher', 'url'=>array('update', 'id'=>$model->TM_TH_Id)),
	array('label'=>'Delete Teacher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_TH_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3>View <?php echo $model->TM_TH_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_TH_Id',
                'TM_TH_Name',
                'TM_TH_CreatedOn',
                array(
                    'name'=>'TM_TH_CreatedBy',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($model->TM_TH_CreatedBy),
                ),
                array(
                    'name'=>'TM_TH_Status',
                    'type'=>'raw',
                    'value'=>Teachers::itemAlias("TeachersStatus",$model->TM_TH_Status),
                ),
                array(
                    'name'=>'TM_TH_UserType',
                    'type'=>'raw',
                    'value'=>Teachers::itemAlias("UserLevel",$model->TM_TH_UserType),
                )
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
