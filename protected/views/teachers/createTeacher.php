<?php
$this->breadcrumbs=array(
    'Schools'=>array('admin'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Teacher', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'List Teachers', 'url'=>array('manageTeachers', 'id'=>$id)),
);
?>
<h1><?php echo UserModule::t("Create School Teacher"); ?></h1>

<?php
/*echo $this->renderPartial('_menu',array(
        'list'=> array(),
    ));*/
//
echo $this->renderPartial('_staffform', array('id'=>$id,'model'=>$model,'profile'=>$profile,'staffschool'=>$staffschool));
?>
