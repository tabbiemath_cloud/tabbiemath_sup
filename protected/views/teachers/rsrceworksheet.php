<style type="text/css">
    @media only screen and (max-width: 1300px) {
        #page-wrapper .col-md-3 {
            padding-left: 5px;
            padding-right: 5px;
        }
    }
    @media only screen and (max-width: 1200px) {
        #page-wrapper .col-md-3 {
            width:33.33%;
            float: left;
            text-align: center;
        }
    }
    @media only screen and (max-width: 998px) {
        #page-wrapper .col-md-3 {
            width: 50%;
        }
    }
    @media only screen and (max-width: 767px) {
        #page-wrapper .col-md-3 {
            width: 50%;
        }
        #page-wrapper {
            position: inherit;
            height: 100%;
            border-left: 1px solid #e7e7e7;
            background-color: #DFF1FB;
            background-image: url(../images/background-photo.png);
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }
    }
    @media only screen and (max-width: 500px) {
        #page-wrapper .col-md-3 {
            width: 100%;
            text-align:center;
        }
    }

    #showworksheet td, #showworksheet th{
        padding: 0 !important;
        border-bottom: none;
        
        

    }
    .math span {
        /*font-family: Arial !important;*/
        font-size: 13px;
    }
    .math
    {
        width: 50.93em !important;
    }
    @media print {
        @page { margin: 0.6cm; }
        body {
             /* margin: 1.6cm;  */
             }
    
        body * {
            visibility: hidden;
            /* overflow:hidden; */
            margin-top:0 !important;
            padding-top:0 !important;
            /* position:'relative'; */
            /* top:0; */
        }
        #showworksheet, #showworksheet * {
            visibility: visible;
        }
        #showworksheet {
            /* position: fixed; */
            /* left: 0; */
            /* top: 0 !important;  */
        }
        .nonPrintingContent, .thumbnail, .row{
            display:none;
        }
    }

</style> 
<div class="nonPrintingContent">
<div class="row">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>'',
            'method'=>'post',
        )); ?>
        <div class="col-lg-12">
            <div class="col-lg-3">
                <h3 class="h125 pull-left">Work Sheets </h3>
                <input type="hidden" id="schoolid" value="<?php echo Yii::app()->session['school'];?>">
            </div>
            <div class="col-lg-4">
                <a href="<?php echo Yii::app()->createUrl('teachers/createworksheet')?>"
                <button class="btn pull-left btn-warning quickhwvalidation" type="button" style="margin-top: 18px; margin-bottom: 10px;">Upload Worksheet</button></a>
            </div>
             <div class="col-lg-5">
                <div class="row" >
                    <div class="col-sm-9" style="margin-top: 18px; margin-bottom: 10px;">
                        <?php echo $form->textField($model,'TM_MK_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control','placeholder'=>'Search topics')); ?>
                       
                    </div>
                    <div class="col-sm-3" style="margin-top: 18px; margin-bottom: 10px;">
                        <?php echo $form->hiddenField($model,'schoolsearch',array('value'=>'true')); ?>
                        <!--<input value="<?php /*echo $_GET['tags'];*/?>" name="selectedtags" type="hidden">-->
                        <?php echo CHtml::submitButton('Search',array('class'=>'btn btn-warning')); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <?php
            foreach($mockheaders AS $mockheader):
                $headers=explode(',', $mockheader['TM_MT_Tags']);
                foreach($headers AS $header):
                    ?>
                    <button type="submit" class="btn btn-warning" style="border: none;background: none;text-decoration: underline;color: blue;font-weight: bold" name="tags" value="<?php echo $header;?>"><?php echo $header;?></button> <span>|</span>
                <?php endforeach;endforeach;?>
        </div>
        <?php $this->endWidget(); ?>
    </div>

    <div class="row" style="padding-top: 10px">
        <?php foreach($mocks AS $mock):
            if($mock['TM_MK_Worksheet']==''):
                // $worksheet="#Worksheetmodal";
                $worksheet='';
                $style="";
                $link="";
                $target="";
            else:
                $worksheet="";
                $style="style='cursor: not-allowed;'";
                $link=Yii::app()->createUrl('teachers/printmock',array('id'=>$mock['TM_MK_Id']));
                $target="target='_blank'";
            endif;
            if($mock['TM_MK_Privacy']==1):
                if($mock->TM_MK_CreatedBy!=Yii::app()->user->id):
                    $privacy="style='display:none;'";
                else:
                    $privacy="";
                endif;
            else:
                $privacy="";
            endif;
            $name = strlen($mock['TM_MK_Name']) > 50 ? substr($mock['TM_MK_Name'],0,50)."..." : $mock['TM_MK_Name'];
            $desc = strlen($mock['TM_MK_Description']) > 50 ? substr($mock['TM_MK_Description'],0,50)."..." : $mock['TM_MK_Description'];
            $createdby=Mock::model()->GetCreator($mock->TM_MK_CreatedBy);
            ?>
            <div class="col-md-3" <?php echo $privacy;?>>
                <div class="thumbnail" style="height: 400px;display: block">
                    <div class="caption" style="min-height: 55px">
                        <b <?php echo $namestyle;?> ><?php echo $name;?></b>
                        <?php if($createdby!='System'):?>

                            <img src="<?php echo Yii::app()->request->baseUrl."/images/noimage2.png"?>" class="img-circle pull-right" style="width: 32px;height: 32px;border: 2px solid #ffa900;">

                        <?php endif;?>
                    </div>
                    <a class="viewworksheet" <?php echo $style;?> title="View Worksheet" data-backdrop="static" data-toggle="modal" data-target="<?php echo $worksheet;?>" data-value="<?php echo $mock['TM_MK_Id'];?>" href="">
                        <?php if($mock['TM_MK_Thumbnail']!=''):?>
                            <img src="<?php echo Yii::app()->request->baseUrl."/worksheets/".$mock['TM_MK_Thumbnail']?>" style="height: 250px;" alt="<?php echo $mock['TM_MK_Thumbnail'];?>">
                        <?php else:?>
                            <img src="<?php echo Yii::app()->request->baseUrl."/images/worksheet.jpg"?>" style="height: 250px;" alt="">
                        <?php endif;?>
                    </a>
                    <div class="caption">
                        <p style="min-height: 38px;"><?php echo $desc;?></p>
                        <?php if($mock['TM_MK_Link_Resource']==1){  ?>
                        <a style="width: 1px; margin-left: -5px;" title="Print Worksheet" filter="" class="btn btn-warning btn-xs viewworksheet" data-backdrop="static" data-toggle="modal" data-target="<?php echo $worksheet;?>" data-value="<?php echo $mock['TM_MK_Id'];?>" href="<?php echo $link;?>" <?php echo $target;?> ><span style="margin-left: -5px;" class="glyphicon glyphicon-print"></span></a>
                    <?php } else { ?>
                        <a title="Print Worksheet" filter="" class="btn btn-warning btn-xs viewworksheet" data-backdrop="static" data-toggle="modal" data-target="<?php echo $worksheet;?>" data-value="<?php echo $mock['TM_MK_Id'];?>" href="<?php echo $link;?>" <?php echo $target;?> ><span class="glyphicon glyphicon-print"></span></a>
                    <?php } ?>

                       <?php if($mock['TM_MK_Link_Resource']==1){  ?>
                        <a style="width: 1px;"  title="Print Worksheet Solution" data-backdrop="static" data-value="<?php echo $mock['TM_MK_Id'];?>" filter="" style="visibility:visible" class="btn btn-warning btn-xs printsolut" href=""><span style="margin-left: -6px;" class="glyphicon glyphicon-list-alt"></span></a>
                        <?php } else { ?>
                        <a title="Print Worksheet Solution" data-backdrop="static" data-value="<?php echo $mock['TM_MK_Id'];?>" filter="" style="visibility:visible" class="btn btn-warning btn-xs printsolut" href=""><span class="glyphicon glyphicon-list-alt"></span></a>

                        <?php } ?>
                        <?php if($mock['TM_MK_Link_Resource']==1){ ?>

                        <a style="width: 1px" title="Resource" filter="" class="btn btn-warning btn-xs" data-backdrop="static" data-value="<?php echo $mock['TM_MK_Id'];?>" target="_blank" href="linkresources?resource=<?php echo $mock['TM_MK_Id']; ?>"><span style="margin-left: -6px;" class="glyphicon glyphicon-facetime-video"></span></a>

                        <?php } ?> 
                        <?php if($mock['TM_MK_Link_Resource']==1){ ?>
                        <a style="width: 68px;" title="publish" filter="" class="choosegroups btn btn-warning btn-xs" data-backdrop="static" data-toggle="modal" data-target="#SelectGroup" data-value="<?php echo $mock['TM_MK_Id'];?>" href="javascript:void(0)"><span style="margin-left: -7px">Publish</span></a>
                        <?php } else { ?>
                         <a title="publish" filter="" class="choosegroups btn btn-warning btn-xs" data-backdrop="static" data-toggle="modal" data-target="#SelectGroup" data-value="<?php echo $mock['TM_MK_Id'];?>" href="javascript:void(0)">Publish</a>
                        <?php } ?>
                        <?php if($mock['TM_MK_CreatedBy']==Yii::app()->user->id):
                            echo "<a href='" .Yii::app()->createUrl('teachers/updateworksheet', array('id' => $mock->TM_MK_Id)). "' title='Update'><span class='glyphicon glyphicon-pencil'></span></a>";
                            echo "<a href='" .Yii::app()->createUrl('teachers/deleteworksheet', array('id' => $mock->TM_MK_Id)). "' title='Delete'><span class='glyphicon glyphicon-trash'></span></a>";
                        endif;?>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <div class="modal fade " id="SelectGroup" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Publish Practice</h4>
                </div>
                <div class="modal-body" >
                    <div class="panel-body form-horizontal payment-form">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Class</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="standard" id="publishto">
                                        <option value="0">Select Class</option>
                                        <?php
                                        $teacherstandards=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));
                                        if(count($teacherstandards)>0):
                                            foreach($teacherstandards AS $teacherstandard):
                                                if($teacherstandard->standard->TM_SD_Id==Yii::app()->session['standard']):
                                                    $selected='selected';
                                                else:
                                                    $selected='';
                                                endif;
                                                echo "<option value='".$teacherstandard->standard->TM_SD_Id."' ".$selected.">".$teacherstandard->standard->TM_SD_Name."</option>";
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                    <input type="hidden" id="homeworkid">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish To</label>
                                <div class="col-sm-7" id="publishstdinfo">
                                    <input type="text" id="usersuggest" name="assigngroups[]">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish On <span class="glyphicon glyphicon-calendar"></span></label>
                                <div class="col-sm-7">
                                    <input type="date" id="publishdate" class="form-control" value="<?php echo date('Y-m-d');?>" name="publishdate">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Due On <span class="glyphicon glyphicon-calendar"></span></label>
                                <div class="col-sm-7">
                                    <input type="date" id="duedate" class="form-control" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d') .' +1 day'));?>" name="duedate">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Submission Type</label>
                                <div class="col-sm-8" id="type"><label class="radio-inline"><input type="radio" class="showsolutio" name="type" value="3" checked="checked">Worksheet</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Solution</label>
                                <div class="col-sm-7">
                                    <input type="radio" class="showsolution" name="solution" value="1" checked="checked" /><span>Yes</span>
                                    <input type="radio" class="showsolution" name="solution" value="0" /><span>No</span>
                                </div>
                            </div>
                        </div>

                    <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Marks</label>
                                <div class="col-sm-7">
                                    <input type="radio" class="showmarks" name="marks" value="0" checked="checked" /><span>Yes</span>
                                    <input type="radio" class="showmarks" name="marks" value="1" /><span>No</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Comments</label>
                                <div class="col-sm-7">
                                    <textarea name="comments" class="form-control" id="comments"></textarea>
                                </div>
                            </div>
                        </div>                   <!--code by amal-->




                        <!--ends-->

                    </div>
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-success pull-left dateinfo" style="display: none;">Due date cannot be earlier than Publish date</span>
                    <span class="alert alert-success pull-left datedelay" style="display: none;">Due date cannot be earlier than Today</span>
                    <span class="alert alert-success pull-left datedelaypub" style="display: none;">Publish date cannot be earlier than Today</span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <button type="button" class="btn btn-warning" id="invitequickhw">Publish</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<div class="modal fade " id="Worksheetmodal" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header nonPrintingContent">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <a href='javascript::void(0)' style='float:right;margin-right:10px;' class='btn btn-warning btn-xs' id="printWork">
                    <span style='padding-right: 10px;'>Print</span>
                    <i class='fa fa-print fa-lg'></i>
                </a>
                <h4 class="modal-title">View Worksheet</h4>
            </div>
            <div class="panel-heading nonPrintingContent">
                <div style="text-align: right;" id="printbtn">

                </div>
            </div>
             <div id="preloaderrow">
                        <div class='col-md-12'><img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader"></div>
                    </div>
            <div class="modal-body" id="showworksheet">
                <!-- <table class="table ">
                    <thead>
                    <tr id="preloaderrow">
                        <th>
                            <div class='col-md-12'><img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader"></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="font3 wirisfont" id="showworksheet">

                    </tbody>
                </table> -->
            </div>
            <div class="modal-footer nonPrintingContent">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script type="text/javascript">
        $(function(){
            $('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            var names=$('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            $('#publishto').change(function(){
                var std=$(this).val();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/Groups";?>',
                    data: {standard:std},
                    success: function(data){
                        ///console.log(data)
                        $('#publishstdinfo').html('<input type="text" id="usersuggest" name="assigngroups[]">');
                        $('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                        names=$('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                    }
                });

            });
            $('.choosegroups').click(function(){
                $('.nogroups').hide();
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).data('value');
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/GetGroupsWorksheet";?>',
                    data: {id:schoolid,homeworkid:homeworkid},
                    success: function(data){
                       if(data.count=='0')
                       {
                           $('.nogroups').html('You have no groups for the school').show();
                       }
                       $('#type').html(data.result);
                   }
                });
            });
            $('#invitequickhw').click(function() {
                var type=$("input[name=type]:checked").val();
                $('.inviteinfo').hide();
                var nameids=names.getValue();
                if($('#homeworkid').val()=='' || $('#duedate').val()=='' || $('#publishdate').val()==''){
                    $('.inviteinfo').text('Please enter all fields.');
                    $('.inviteinfo').show();
                    $("#invitequickhw").prop("disabled", false);
                    return false;
                }
                else{
                    //$('#buddyindication').text('')
                    var from = $('#publishdate').val().split("-");
                    var publish = new Date(from[0], from[1] - 1, from[2]);
                    var to = $('#duedate').val().split("-");
                    var due = new Date(to[0], to[1] - 1, to[2]);
                    var dateObj = new Date();
                    var month = dateObj.getMonth(); //months from 1-12
                    var day = dateObj.getDate();
                    var year = dateObj.getFullYear();
                    var d = new Date(year,month,day);

                    if (publish > due) {
                        $('.dateinfo').show();
                        $('.dateinfo').show();
                        setTimeout(function () {
                            $('.dateinfo').hide();
                        }, 5000);
                        return false;
                    }
                    else if(publish < d ){
                        $('.datedelaypub').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else if( due < d ){
                        $('.datedelay').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->createUrl('teachers/publishworksheet');?>',
                            beforeSend : function(){
                                $("#invitequickhw").prop("disabled", true)
                            },                            
                            data: {nameids: nameids,homeworkid:$('#homeworkid').val(),dudate:$('#duedate').val(),comments:$('#comments').val(),publishdate:$('#publishdate').val(),type:$("input[name=type]:checked").val(),solution:$('.showsolution:checked').val(),publishto:$('#publishto').val(),showmark:$('.showmarks:checked').val()}
                        })
                            .done(function (data) {
                                    $('.addgroupsinfo').text('Practice assigned to groups.');
                                    $('.addgroupsinfo').show();
                                    //$('#inviteclose').trigger('click');
                                    setTimeout(function() {
                                        $('#SelectGroup').modal('toggle');
                                        window.location='<?php echo Yii::app()->createUrl('teacherhome');?>';
                                    }, 2000);
                            });

                    }
                }
            });
            // $('.viewworksheet').click(function(){
            //     var worksheetid = $(this).data('value');
            //     $('#showworksheet').html('');
            //     $.ajax({
            //         type: 'POST',
            //         //dataType:'json',
            //         // async: false,
            //         url: '<?php echo Yii::app()->request->baseUrl."/teachers/Viewworksheet";?>',
            //         beforeSend : function(){
            //             $('#preloaderrow').show();
            //             console.log('before send');
            //         },
            //         data: {worksheetid:worksheetid},
            //         success: function(data){
            //             $('#preloaderrow').hide();
            //             // $('#printbtn').html(data.print);
            //             console.log('success');
            //             // $('#showworksheet').html(data.result);
            //             var html = data.split('<div class="first-div">');
            //             $('#showworksheet').html(html[1]);
            //             MathJax.Hub.Queue(["Typeset",MathJax.Hub, "modal-body"]);
            //         }
            //     });
            // });
            
            $('.viewworksheet').click(function(){
                var worksheetid = $(this).data('value');
                // $('#showworksheet').html('');
                window.open('printmock?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
               
            });

            $('.printsolut').click(function(){
                var worksheetid = $(this).data('value');
                // $('#showworksheet').html('');
                window.open('printmocksolution?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
               
            });
            $('#printWork').click(function(){
                window.scrollTo(0, 0);
                window.print();
            });
        });


    </script>
