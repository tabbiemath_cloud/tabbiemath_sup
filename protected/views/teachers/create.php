<?php
/* @var $this TeachersController */
/* @var $model Teachers */

$this->breadcrumbs=array(
	'Teachers'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Teachers', 'class'=>'nav-header'),
	array('label'=>'List Teachers', 'url'=>array('admin')),
);
?>

<h3>Create Teacher</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>