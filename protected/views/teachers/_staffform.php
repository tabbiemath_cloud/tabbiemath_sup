<div class="row brd1">

    <?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>
    <div class="panel-body form-horizontal payment-form">
    		<?php 
  echo CHtml::errorSummary($model);
echo CHtml::errorSummary($profile);  
echo CHtml::errorSummary($staffschool);
 ?>
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
        <?php /*
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'username',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextField($model,'username',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'username'); ?>
            </div>
        </div> */?>

        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'email',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'email'); ?>
            </div>
        </div>

        <?php if($model->password==''):?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'password',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activePasswordField($model,'password',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'password'); ?>
            </div>
        </div>
        <?php endif;?>

        <?php /*if($model->status==1):?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'status',User::itemAlias('UserStatus'),array('class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'status'); ?>
            </div>
        </div>
        <?php endif;*/?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'status',User::itemAlias('UserStatus'),array('class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'status'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($staffschool,'TM_TH_UserType',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($staffschool,'TM_TH_UserType',User::itemAlias('UserLevel'),array('class'=>'form-control')); ?>
                <?php echo CHtml::error($staffschool,'TM_TH_UserType'); ?>
            </div>
        </div>
        <!--code by amal-->
        <?php
        if($model->id!=Yii::app()->user->id):?>
            <div class="form-group">
                <label class="col-sm-3 control-label required" for="User_type">Userlevel<span class="required">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="User[type]" id="User_type">
                        <option value="9" <?php echo ($model['usertype']=='9'?'selected':'');?> >Teacher</option>
                        <option value="12" <?php echo ($model['usertype']=='12'?'selected':'');?> >Teacher admin</option>
                    </select>
                </div>
            </div>
        <?php endif;?>
        <!--ends-->
        <?php
        $profileFields=$profile->getFields();
        if ($profileFields) {
            foreach($profileFields as $field) {
                ?>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($profile,$field->varname,array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php
                        if ($field->widgetEdit($profile)) {
                            echo $field->widgetEdit($profile);
                        } elseif ($field->range) {
                            echo CHtml::activeDropDownList($profile,$field->varname,Profile::range($field->range),array('class'=>'form-control'));
                        } elseif ($field->field_type=="TEXT") {
                            echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50,'class'=>'form-control'));
                        } else {
                            echo CHtml::activeTextField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255),'class'=>'form-control'));
                        }
                        ?>

                        <?php echo CHtml::error($profile,$field->varname); ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <div class="form-group">
            <label class="col-sm-3 control-label required" for="User_type">Manage Questions</label>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($staffschool,'TM_TH_Manage_Questions',array('1'=>'Yes','0'=>'No'),array('class'=>'form-control'));?>
            </div>
        </div>        
        <div class="form-group">
            <div class="row">

                <div class="col-sm-3 pull-right">
                    <?php echo CHtml::activeHiddenField($model,'superuser',array('value'=>'0')); ?>
                    <?php echo CHtml::activeHiddenField($model,'usertype',array('value'=>'9')); ?>
                    <?php echo CHtml::activeHiddenField($staffschool,'TM_TH_SchoolId',array('value'=>$id)); ?>
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo CHtml::endForm(); ?>

</div><!-- form -->
<?php 
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('Teachers/manageTeachers',array('id'=>$id))."';
    });
");
?>

