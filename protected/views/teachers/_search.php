<?php
/* @var $this TeachersController */
/* @var $model Teachers */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_TH_Id'); ?>
		<?php echo $form->textField($model,'TM_TH_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TH_Name'); ?>
		<?php echo $form->textField($model,'TM_TH_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TH_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_TH_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TH_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_TH_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TH_Status'); ?>
		<?php echo $form->textField($model,'TM_TH_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->