<!--<div class="row">
    <div class="col-lg-12">
        <h3>Worksheet Print Report</h3>
    </div>
</div>-->
<?php
/* @var $this PlanController */
/* @var $model Plan */
$this->menu=array(
    array('label'=>'Manage Plan', 'class'=>'nav-header'),
);
?>
<h3>Worksheet Print Report</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <form method="get">
            <table class="table table-bordered table-hover table-striped admintable">
                <tr><th colspan="4"><h3><?php echo UserModule::t("Search"); ?></h3></th></tr>
                <tr>
                    <th>School</th>
                    <th>Standard</th>
                    <th colspan="2">
                        <input type="radio" class="date" <?php echo ($formvals['date']=='before'?'checked="checked"':'');?> name="date" value="before" id="before">Before a date
                        <input type="radio" class="date" <?php echo ($formvals['date']=='after'?'checked="checked"':'');?> name="date" value="after" id="after">After a date
                        <input type="radio" class="dates" <?php echo ($formvals['date']=='between' || $formvals['date']=='' ?'checked="checked"':'');?> name="date" value="between" id="days">Between dates
                    </th>
                </tr>
                <tr>
                    <td>
                        <select name="school" id="school" class="form-control">
                            <?php
                            $data=CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');

                            echo CHtml::tag('option',
                                array('value'=>''),'Select School',true);
                            foreach($data as $value=>$name)
                            {
                                if($formvals['school']==$value):
                                    $selected=array('value'=>$value,'selected'=>'selected');
                                else:
                                    $selected=array('value'=>$value);
                                endif;
                                echo CHtml::tag('option',
                                    $selected,CHtml::encode($name),true);
                            }
                            if($formvals['school']=='0'):
                                $selectedother=array('value'=>0,'selected'=>'selected');
                            else:
                                $selectedother=array('value'=>0);
                            endif;
                            echo CHtml::tag('option',$selectedother,'Other',true);
                            ?>
                        </select>
                        <input type="text" name="schooltext" id="schooltext" class="form-control" value="<?php echo ($formvals['schooltext']? $formvals['schooltext']:'');?>" />
                        <!--<input type="text" name="schoolname" id="schoolname" placeholder="School name" class="form-control" value="<?php /*echo ($formvals['schoolname']? $formvals['schoolname']:'');*/?>" /></td>-->
                    </td>
                    <td >
                        <select name="plan" id="plan" class="form-control" >
                            <option value="">Select Standard</option>
                            <?php
                            $plans=Standard::model()->findAll(array('condition'=>'TM_SD_Status=0'));

                            foreach($plans AS $plan):
                                if($formvals['plan']==$plan->TM_SD_Id):
                                    $selected='selected';
                                else:
                                    $selected='';
                                endif;;
                                echo "<option value='".$plan->TM_SD_Id."' ".$selected.">".$plan->TM_SD_Name."</option>";
                            endforeach;
                            ?>
                        </select>
                        <span style="color:red;display: none;;" class="error errordiv">Select A Plan</span>
                    </td>
                    <td><input type="date" class="form-control" value="<?php echo ($formvals['fromdate']!=''?$formvals['fromdate']:'');?>" id="fromdate" name="fromdate" >
                        <span style="color:red;display: none;;" class="error errordivdate">Select A Date</span></td>
                    <td><input type="date" class="form-control" value="<?php echo ($formvals['todate']!=''?$formvals['todate']:'');?>" id="todate" name="todate" ></td>
                </tr>
                <tr>
                    <?php if(count($formvals)>0):?>
                        <td></td>
                        <td><a href="<?php echo Yii::app()->createUrl('teachers/Downloadworksheet',array('plan'=>$formvals['plan'],'school'=>$formvals['school'],'date'=>$formvals['date'],'fromdate'=>$formvals['fromdate'],'todate'=>$formvals['todate']))?>" class="btn btn-warning btn-md">Export</a></td>
                    <?php else:?>
                        <td colspan="2"></td>
                    <?php endif;?>
                    <td><button class="btn btn-warning btn-lg btn-block" name="search" id="searchform" value="search" type="submit">Search</button></td>
                    <td><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
                </tr>
            </table>
        </form>
    </div>
</div>

<div class="row brd1">
    <div class="col-lg-12">
        <div id="worksheetprint-grid" class="grid-view">
            <table class="table table-bordered table-hover table-striped admintable">
                <thead>
                    <tr>
                        <th>Date & Time</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Standard</th>
                        <th>School</th>
                        <th>Teacher/Student</th>
                        <th>Location</th>
                        <th>Worksheet name</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if(count($dataval)>0):
                    foreach($dataval AS $data):
                        if($data['TM_WPR_Type']==0):
                            $type="Teacher";
                        else:
                            $type="Student";
                        endif;
                        $standard=Standard::model()->findByPk($data['TM_WPR_Standard'])->TM_SD_Name;
                ?>
                    <tr>
                        <td><?php echo date("d-m-Y h:i A", strtotime($data['TM_WPR_Date']));?></td>
                        <td><?php echo $data['TM_WPR_Username'];?></td>
                        <td><?php echo $data['TM_WPR_FirstName'];?></td>
                        <td><?php echo $data['TM_WPR_LastName'];?></td>
                        <td><?php echo $standard;?></td>
                        <td><?php echo $data['TM_WPR_School'];?></td>
                        <td><?php echo $type;?></td>
                        <td><?php echo $data['TM_WPR_Location'];?></td>
                        <td><?php echo $data['TM_WPR_Worksheetname'];?></td>
                    </tr>
                <?php endforeach;else:?>
                <tr>
                    <td colspan="9">No results found</td>
                </tr>
                <?php endif;?>
                </tbody>
            </table>
        </div>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination" >
            <?php echo $nopages;?>
        </ul>
    </nav>
</div>

<script type="text/javascript">
    $('.date').click(function(){
        $('#fromdate').attr('disabled',false);
        $('#todate').attr('disabled',true);
    });
    $('.dates').click(function(){
        $('#fromdate').attr('disabled',false);
        $('#todate').attr('disabled',false);
    });
    <?php
                if($formvals['date']=='before'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',true);";
                elseif($formvals['date']=='after'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',true);";
                elseif($formvals['date']=='between'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',false);";
                endif;
            ?>
    $('#searchform').click(function(){
        var error=0;
        $('.error').hide();
        if($('#fromdate').val()=='')
        {
            $('.errordivdate').show();
            error=1;
        }
        else
        {
            $('.errordivdate').hide();
        }
        if(error==1)
        {
            return false;
        }
        else
        {
            return true;
        }
    });
</script>
<script type="text/javascript">
    $('#clear').click(function(){
        $('#schoolname ').val('');
        $('#fromdate ').val('');
        $('#todate ').val('');
        return true;
    });
</script>
<script type="text/javascript">
    $(function(){
        $('#schooltext').hide();
        if ($('#schooltext').val()!=''){
            $('#schooltext').show();
        }
        $('#school').change(function(){
            if($(this).val()=='0')
            {
                $('#schooltext').show();
            }
            else
            {
                $('#schooltext').hide();
            }
        });
    });
</script>
