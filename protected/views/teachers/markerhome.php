<div class="row">
    <div class="col-lg-12">
        <h3>My Stuff</h3>
        <input type="hidden" id="schoolid" value="<?php echo Yii::app()->session['school'];?>">
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'homework-grid',
                    'dataProvider'=>$model->search(Yii::app()->user->id,0),
                    //'filter'=>$model,
                    'columns'=>array(
                        array(
                            'name'=>'Name',
                            'type'=>'raw',
                            'value'=>'$data->homework->TM_SCH_Name',
                        ), 
                        array(
                            'name'=>'TM_HWM_Assigned_On',
                            'type'=>'raw',
                            //'value'=>'$data->TM_HWM_Assigned_On',
                            'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_HWM_Assigned_On))',
                        ),
                        array(
                            'type'=>'raw',
                            'name'=>'TM_HWM_Assigned_By',
                            'value' => 'Homeworkmarking::getAssignee($data->TM_HWM_Assigned_By)',
                        ),                        
                       
                        /*array(
                            'name'=>'Assigned Groups',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetAssignees($data->TM_SCH_Id)',
                        ), 
                                                
                        array(
                            'type'=>'raw',
                            'name'=>'Publish',
                            'value' => 'CHtml::link("Publish","javascript:void(0)",array(
                            "title"=>"publish",
                            "class"=>"choosegroups btn btn-warning btn-xs",
                            "style"=>Schoolhomework::checkQuestions($data->TM_SCH_Id),
                            "data-backdrop"=>"static",
                            "data-toggle"=>"modal",
                            "data-target"=>"#SelectGroup",
                            "data-value"=>"$data->TM_SCH_Id"
                            ))',
                        ), */                                                 
                        array(
                            'class'=>'CButtonColumn',
                            'template'=>'{marking}',
                            'deleteButtonImageUrl'=>false,
                            'updateButtonImageUrl'=>false,
                            'viewButtonImageUrl'=>false,
                            'buttons'=>array
                            (
                                
                                'marking'=>array(
                                    'label'=>'Mark',
                                    'url'=>'Yii::app()->createUrl("teachers/markhomework",array("id"=>$data->TM_HWM_Homework_Id))',
                                    'options'=>array(
                                        'title'=>'Mark Practice',
                                        'class'=>'publishtest btn btn-warning btn-xs',
                                    )),
                            ),
                        ),
                    ),'itemsCssClass'=>"rwd-tables"
                )); ?>        
        </div>
    </div>    
</div>
