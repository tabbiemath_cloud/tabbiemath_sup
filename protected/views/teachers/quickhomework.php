<div class="row"  id="primarytable">
    <div class="col-lg-12">
        <h3 class="h125 pull-left">Set Practice<br>
            <p style="font-size: 14px;padding-top: 10px">Create revision worksheets by selecting topics from the list below. Publish to students to be completed online or on paper.</p>
        </h3>
        <button class="btn btn-warning quickhwvalidation pull-right" type="button" name="startquickhw" style="margin-top: 18px; margin-bottom: 10px;">
            Create</button>
    </div>


</div>
<div class="row">
    <div class="col-lg-12">
    <form action="" method="post" id="homeworkform">
    <div class="modal fade " id="SelectGroup" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
               
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Publish Practice</h4>
                </div>
                <div class="modal-body" >
                <div class="panel-body form-horizontal payment-form">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-7">
                            <input type="text" id="name" class="form-control" name="name">                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Class</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="standard" id="publishto">
                                <option value="0">Select Class</option>
                                <?php
                                $teacherstandards=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));
                                if(count($teacherstandards)>0):
                                    foreach($teacherstandards AS $teacherstandard):
                                        if($teacherstandard->standard->TM_SD_Id==Yii::app()->session['standard']):
                                            $selected='selected';
                                        else:
                                            $selected='';
                                        endif;
                                        echo "<option value='".$teacherstandard->standard->TM_SD_Id."' ".$selected.">".$teacherstandard->standard->TM_SD_Name."</option>";
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Publish To</label>
                        <div class="col-sm-7" id="publishstdinfo">
                            <input type="text" id="usersuggest" name="assigngroups[]">                            
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publish On <span class="glyphicon glyphicon-calendar"></span></label>
                            <div class="col-sm-7">
                                <input type="date" id="publishdate" class="form-control" value="<?php echo date('Y-m-d');?>" name="publishdate">
                            </div>
                        </div>
                    </div>
               <div class="row"> 
                <div class="form-group">
                    <label class="col-sm-3 control-label">Due On <span class="glyphicon glyphicon-calendar"></span></label>
                    <div class="col-sm-7">
                        <input type="date" id="duedate" class="form-control" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d') .' +1 day'));?>" name="duedate">
                    </div>
                </div>
                </div> 

                        <!--code by amal-->
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">To be submitted as</label>
                            <div class="col-sm-8">
                                <label class="radio-inline"><input type="radio" class="showsolution" name="type" value="2" checked="checked">Online</label>
                                <label class="radio-inline" ><input type="radio" class="showsolution" name="type" value="3" >Worksheet</label>
                                <label class="radio-inline"><input type="radio" class="showsolution" name="type" value="1">Worksheet (with options)</label>                                
                                

                                <!-- <select class="form-control" id="type" name="type"><option value="1">Worksheet Only</option><option value="2">Online &amp; Worksheet</option></select>-->
                            </div>
                        </div>
                    </div>
                        <!--<div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Submission Type</label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="type" name="type"><option value="1">Worksheet Only</option><option value="2">Online &amp; Worksheet</option></select>
                                </div>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Solution</label>
                                <div class="col-sm-7">
                                    <input type="radio" class="showsolution" name="solution" value="1" checked="checked" /><span>Yes</span>
                                    <input type="radio" class="showsolution" name="solution" value="0" /><span>No</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Mark</label>
                                <div class="col-sm-7">
                                    <input type="radio" class="showmarks" name="mark" value="0" checked="checked" /><span>Yes</span>
                                    <input type="radio" class="showmarks" name="mark" value="1" /><span>No</span>
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comments</label>
                            <div class="col-sm-7">
                                <textarea name="comments" class="form-control" id="comments"></textarea>
                            </div>
                        </div>
                    </div>
                        <!--ends-->                         
                </div> 
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-success pull-left dateinfo" style="display: none;">Due date cannot be earlier than Publish date</span>
                    <span class="alert alert-success pull-left datedelay" style="display: none;">Due date cannot be earlier than Today</span>
                    <span class="alert alert-success pull-left datedelaypub" style="display: none;">Publish date cannot be earlier than Today</span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <button type="submit" class="btn btn-warning" name="invitequickhw" id="invitequickhw">Publish</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>            

        <input type="hidden"  id="Quickquestiontotal" value="0" />
        <div class="panel panel-default clearfix" id="QuickChapters">
            <div class="panel-body padnone">
                <div class="panel-heading">
                    <b>Topics Chosen</b> : <span id="Quicktopicindication">No Topics Selected</span>

                    <div class="pull-right  hidden-xs hidden-sm">
                        Selected Questions : <span class="quicktotalspan">0</span>
                    </div>
                    <div class="pull-right  hidden-xs hidden-sm">
                        Maximum Questions : <span>25 &nbsp;</span>
                    </div>
                    <div class="visible-xs visible-sm margin">
                        Selected Questions :
                        <span class="quicktotalspan">0</span>
                    </div>
                    <div class="visible-xs visible-sm margin">
                        Maximum Questions :
                        <span>25 &nbsp;</span>
                    </div>
                </div>
                <?php foreach ($chapters AS $key => $chapter):
                    //if ($this->GetChapterTotal($chapter->TM_TP_Id, '1') > 0):
                        ?>
                <div>
                    <div class="col-xs-12 toggle-header">
                        <div class="col-xs-12 col-md-6 ">
                            <span data-toggle="tooltip" data-placement="left" title="Select topics within the chapter that you want to revise on">
                                <button type="button" class="btn btn-default btn-circle"
                                        id="quicktoggleheader<?php echo $key; ?>" data-toggle="collapse"
                                        data-target="#<?php echo 'quickfeature-' . $key; ?>"
                                        aria-expanded="true">
                                    <i class="fa fa-chevron-down"></i>
                                </button>
                                <input type="hidden" id="quickchapter<?php echo $key; ?>"
                                       class="quickselectedchapter" name="quickchapter[]" value="">
                                <a href="#" data-toggle="collapse"
                                   data-target="#<?php echo 'quickfeature-' . $key; ?>"> <?php echo $chapter->TM_TP_Name; ?></a>
                            </span>
                        </div>
                        <div class="col-xs-12 col-md-4 text-left pull-right">
                            <input id="<?php echo 'quick' . $key; ?>"
                                   name="<?php echo 'quick' . $chapter->TM_TP_Id; ?>" value="0" type="hidden">
                            <input id="<?php echo 'slidequick' . $key; ?>" class="selectslide"
                                   data-slider-id='<?php echo 'slidequick' . $key; ?>Slider' type="text"
                                   data-parent="<?php echo $key; ?>" data-slider-min="0" data-slider-max="10"
                                   data-slider-step="1" data-slider-value="0"/>
                            <span class="pull-left">0</span>
                            <span class="pull-right">10</span>
                        </div>
                        <div class="col-xs-12 col-md-1 text-left pull-right martop" style="display:none;">
                            <input type="hidden" value="0" id="quickchaptertotal<?php echo $key; ?>"
                                   name="quickchaptertotal<?php echo $chapter->TM_TP_Id; ?>"
                                   class="quickchaptertotal">
                            <span id="<?php echo 'quickchaptertotaltitle' . $key; ?>" class="total" style="display:none;">0</span>
                        </div>
                    </div>
                    <div id="<?php echo 'quickfeature-' . $key; ?>"
                         class="collapse <?php //echo ($key=='0'?'in':'');?>" aria-expanded="true">
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 17px;">
                                <button type="button" class="btn btn-xs " style="margin-left: 15px;" id="uncheckall"
                                        href="javascript:void(0)">
                                    Clear all
                                </button>
                                <button type="button" class="btn btn-xs " style="margin-left: 15px;" id="checkall"
                                        href="javascript:void(0)">
                                    Select all
                                </button>
                            </div>
                            <?php foreach (array_chunk($chapter->topic, 4) as $topics):?>
                                <div class="col-xs-12 col-md-4">
                                    <ul class="listnew">
                                        <?php
                                            foreach ($topics as $topic):
                                                $questions=Questions::model()->count(
                                                    array('condition'=>
                                                    'TM_QN_Syllabus_Id='.$topic->TM_SN_Syllabus_Id.' AND
                                                    TM_QN_Standard_Id='.$topic->TM_SN_Standard_Id.' AND
                                                    TM_QN_Topic_Id='.$topic->TM_SN_Topic_Id.' AND
                                                    TM_QN_Section_Id='.$topic->TM_SN_Id.' AND TM_QN_Status IN (0,2)'));
                                                    if($questions>0):
                                                ?>
                                        <li>
                                            <input type="checkbox" class="inputtopic<?php echo $key ?>"
                                                   name="<?php echo 'quicktopic' . $chapter->TM_TP_Id . '[]'; ?>"
                                                   checked="checked"
                                                   value="<?php echo $topic->TM_SN_Id; ?>">
                                            <?php echo $topic->TM_SN_Name; ?>
                                        </li>
                                        <?php endif;endforeach; ?>
                                    </ul>
                                </div>
                                <?php endforeach;?>
                        </div>
                    </div>
                </div>
                    <?php
                    Yii::app()->clientScript->registerScript('quickslider' . $key, "



                        var x=  $('#slidequick" . $key . "').slider({

                                                value:'0',

                                                formatter: function(value) {
                                                    return 'Current value: ' + value;
                                                }
                                            });

                                            x.on('slide', function(ev){

                                             var atLeastOneIsChecked = false;
                                                  $('#quickfeature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;
                                                     // Stop .each from processing any more items
                                                     return false;
                                                     }
                                                     });

                                                if( !atLeastOneIsChecked ){

                                                     var value=0;
                                                     $('#slidequick" . $key . "').slider('setValue', value);
                                                     $('.alert').remove();
                                                      $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select at least one topic before selecting questions.</div>')
                                                       setTimeout(function() {
                                                           $('.alert').fadeOut(function(){
                                                        $('.alert').remove();
                                                       });
                                                     }, 5000);

                                                    }

                                                   else
                                                   {
                                                var totalquestions=($('#Quickquestiontotal').val()*1)+$('#slidequick" . $key . "').data('slider').getValue();
                                                        var newVal = $('#slidequick" . $key . "').data('slider').getValue();

                                                        $('#quick" . $key . "').val(newVal);
                                                        var chaptertotal=($('#quick" . $key . "').val()*1);
                                                        if(chaptertotal!='0')
                                                        {
                                                            $('#quickchapter" . $key . "').val('" . $chapter->TM_TP_Id . "');
                                                        }
                                                        else{
                                                        $('#quickchapter" . $key . "').val('');
                                                        }
                                                        $('#quickchaptertotal" . $key . "').val(chaptertotal);
                                                        $('#quickchaptertotaltitle" . $key . "').text(chaptertotal);
                                                        var questiontotal=0;
                                                        $('.quickchaptertotal').each(function(){
                                                                totalqst=($(this).val()*1);
                                                                questiontotal=questiontotal+totalqst
                                                        });
                                                        $('#Quickquestiontotal').val(questiontotal);
                                                        $('.quicktotalspan').html(questiontotal);
                                                        GetChapterQuick()


                                                  }


                                            });
                                            $('#quickfeature-" . $key . " #checkall').click(function () {


                             $('#quickfeature-" . $key . " input[type=checkbox]').each(function () {
                                $(this).prop('checked', true);

                          });
                          });
                      $('#quickfeature-" . $key . " #uncheckall').click(function () {


                             $('#quickfeature-" . $key . " input[type=checkbox]').each(function () {
                                $(this).prop('checked', false);

                          });
                          $('#quickchapter" . $key . "').val('');
                          GetChapterQuick()
                          var tot=$('#Quickquestiontotal').val();
                          var tot2=$('#quickchaptertotaltitle" . $key . "').html();
                          $('#Quickquestiontotal').val(tot-tot2);
                      var x=$('#slidequick" . $key . "').slider({
                                  value:'0',
                       formatter: function(value) {
                                                    return 'Current value: ' + value;
                                                }
                                            });
                                            var value=0;

            x.slider('setValue', value);
            $('#quickchaptertotaltitle" . $key . "').text(0);
             $('#quickchaptertotal" . $key . "').val('');



    });
    $('.inputtopic" . $key . "').click(function () {
                      var atLeastOneIsChecked = false;
                                                  $('#quickfeature-" . $key . " input[type=checkbox]').each(function ()
                                                   {
                                                    if ($(this).is(':checked'))
                                                    {
                                                     atLeastOneIsChecked = true;
                                                     // Stop .each from processing any more items
                                                     return false;
                                                     }
                                                     });
                                                if( !atLeastOneIsChecked ){
                                                $('#quickchapter" . $key . "').val('');
                          GetChapterQuick()
                               var tot=$('#Quickquestiontotal').val();
                          var tot2=$('#quickchaptertotaltitle" . $key . "').html();
                          $('#Quickquestiontotal').val(tot-tot2);
                          $('#quickchaptertotaltitle" . $key . "').text(0);
                                                     var value=0;
                                                     $('#slidequick" . $key . "').slider('setValue', value);
                                                     $('.alert').remove();
                                                      $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select at least one topic before selecting questions.</div>')
                                                       setTimeout(function() {
                                                           $('.alert').fadeOut(function(){
                                                        $('.alert').remove();
                                                       });
                                                     }, 5000);

                                                    }
													});


                            ");
                //endif;
                endforeach;?>
            </div>
        </div>
    </form>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript('checkboxval', "

    function GetChapterQuick()
    {
        var count=0;
        $('.quickselectedchapter').each(function(){
                if(count=='0')
                {
                    topicids=$(this).val();

                }
                else
                {
                    topicids=topicids+','+$(this).val();
                }
                count++;
        });
        if(topicids!='')
        {
            $('#Quicktopicindication').text('')
            $.ajax({
                type: 'POST',
                url: '" . Yii::app()->createUrl('teachers/getTopics') . "',
                data: { topicids: topicids}
            })
              .done(function( data ) {
              $('#Quicktopicindication').text(data);
            });
        }
        else
        {
            $('#Quicktopicindication').text('No Topics Selected');

        }
    }
    $('#homeworkform').submit(function(){
        $('.alert').remove();

            if(($('#Quickquestiontotal').val()*1)=='0')
            {
                $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Select chapters before proceeding.</div>')
                setTimeout(function() {
                     $('.alert').fadeOut(function(){
                         $('.alert').remove();
                    });
                }, 5000);
                return false;
            }
            else
            {
                return true;
            }



    });



");?>
<?php Yii::app()->clientScript->registerScript('quickhwvalidation', "
$('.quickhwvalidation').click(function(){
 var totquick=$('#Quickquestiontotal').val();
 var totfeature=$('#questiontotal').val();

 if(totquick>25 )
 {
     $('.alert').remove();
     $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Maximum 25 Questions are allowed.</div>')
     setTimeout(function() {
                         $('.alert').fadeOut(function(){
                             $('.alert').remove();
                        });
                    }, 5000);
                    return false;
    }    
    else if(totquick==0)
    {
        $('.alert').remove();
        $('#primarytable').after('<div class=\'alert alert-danger\' role=\'alert\'>Please select questions.</div>')
        setTimeout(function() {
             $('.alert').fadeOut(function(){
                 $('.alert').remove();
            });
        }, 5000);
        return false;
        
    }
    else
    {
        $('#SelectGroup').modal('toggle');
    }

 });

");

?>
<script type="text/javascript">
        $(function(){
            $('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            var names=$('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            $('#publishto').change(function(){
                var std=$(this).val();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/Groups";?>',
                    data: {standard:std},
                    success: function(data){
                        ///console.log(data)
                        $('#publishstdinfo').html('<input type="text" id="usersuggest" name="assigngroups[]">');
                        $('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                        names=$('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                    }
                });
            });
            $('.choosegroups').click(function(){
                $('.nogroups').hide();
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).data('value');
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/getGroups";?>',
                    data: {id:schoolid},
                    success: function(data){
                       if(data.count=='0')
                       {
                           $('.nogroups').html('You have no groups for the school').show();
                       }
                       $('#type').html(data.result);
                    }
                });
            });
            $('#invitequickhw').click(function() {
                $("#invitequickhw").prop("disabled", true);
                $('.inviteinfo').hide();
                var nameids=names.getValue();
                if( $('#name').val()=='' || $('#duedate').val()=='' || $('#publishdate').val()==''){
                    $('.inviteinfo').text('Please enter all fields.');
                    $('.inviteinfo').show();
                    $("#invitequickhw").prop("disabled", false);
                    return false;
                }
                else{
                    var from = $('#publishdate').val().split("-");
                    var publish = new Date(from[0], from[1] - 1, from[2]);
                    var to = $('#duedate').val().split("-");
                    var due = new Date(to[0], to[1] - 1, to[2]);
                    var dateObj = new Date();
                    var month = dateObj.getMonth(); //months from 1-12
                    var day = dateObj.getDate();
                    var year = dateObj.getFullYear();
                    var d = new Date(year,month,day);

                    if (publish > due) {
                        $('.dateinfo').show();
                        $('.dateinfo').show();
                        setTimeout(function () {
                            $('.dateinfo').hide();
                        }, 5000);
                        $("#invitequickhw").prop("disabled", false);
                        return false;
                    }
                    else if(publish < d ){
                        $('.datedelaypub').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        $("#invitequickhw").prop("disabled", false);
                        return false;
                    }
                    else if( due < d ){
                        $('.datedelay').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        $("#invitequickhw").prop("disabled", false);
                        return false;
                    }
                    else {
                        $('#homeworkform')[0].submit();
                        return true;
                    }
                    //$('#buddyindication').text('No Buddies Selected');
                }

            });
        });
    </script>