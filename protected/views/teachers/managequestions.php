 <input type="hidden" id="schoolid" value="<?php echo Yii::app()->session['school'];?>">
<style>
.scrol-body
{
    font-size: 16px !important;
}
.button-column {
    width: 120px !important;
    text-align: center;
    padding: 5px 15PX !important;
}

@media screen and (max-width: 992px) {

    .publicBT
    {
        display: block !important;
    }

    #publishraw
    {
        display: none;
    }

}
.regular .textcolor{
    color: #f0ad4e;
}
.professional .textcolor{
    color: #800000;
}
.switch {
    position: relative;
    display: inline-block;
    width: 44px;
    height: 24px;
}

.switch input {
    opacity: 0;
    width: 0;
    height: 0;
}

.slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
}

.slider:before {
    position: absolute;
    content: "";
    height: 20px;
    width: 20px;
    left: 1px;
    bottom: 2px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;

}

.regular input:checked + .slider {
    background-color: #f68b01;
}

.professional input:checked + .slider {
    background-color: #800000;
}

input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
    -webkit-transform: translateX(21px);
    -ms-transform: translateX(21px);
    transform: translateX(21px);
}

/* Rounded sliders */
.slider.rounded {
    border-radius: 34px;
}

.slider.rounded:before {
    border-radius: 50%;
}
/* radio button */
.addedqstable .switch {
    width: 15px;
    height: 15px;
}

.regular .addedqstable input:checked + .slider:before {
    -webkit-transform: translateX(1px);
    -ms-transform: translateX(1px);
    transform: translateX(1px);
    border: 2px solid white;
    background: #f68b01;
    width: 11px;
    height: 11px;
}
.professional .addedqstable input:checked + .slider:before {
    -webkit-transform: translateX(1px);
    -ms-transform: translateX(1px);
    transform: translateX(1px);
    border: 2px solid white;
    background: #800000;
    width: 11px;
    height: 11px;
}
.addedqstable input + .slider:before {
    -webkit-transform: translateX(1px);
    -ms-transform: translateX(1px);
    transform: translateX(1px);
    border: 2px solid white;
    background: #dadada;
    width: 11px;
    height: 11px;
}
    #question_type
    {
        width: 189px;
    }
    #skill
    {
        width: 130px;

    }

</style>
<div class="row">
    <div class="col-lg-12">
        <h3 > </h3>
    </div>
    <!-- /.col-lg-12 -->

    <div class="col-lg-12">
<div class="panel panel-yellow" style="overflow-x: auto;">
    <table class="table">
        <tr>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_SCT_Name')); ?></label>
                <h4> <?php echo CHtml::encode($model->TM_SCT_Name); ?></h4>
            </td>
            <td style="visibility: hidden;">
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_SCT_Description')); ?> </label>
                <h4> <?php echo CHtml::encode($model->TM_SCT_Description); ?></h4>
            </td>
            <td style="visibility: hidden;">
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_SCT_Standard_Id')); ?> </label>
                <h4> <?php echo CHtml::encode($model->standard->TM_SD_Name); ?></h4>
            </td>
            <td style="visibility: hidden;">
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_SCT_Status')); ?> </label>
                <h4> <?php echo CHtml::encode(Schoolhomework::itemAlias("HomeworkStatus",$model->TM_SCT_Status)); ?></h4>
            </td>
            <td >
                <label class="font18">Total Questions</label>
                <h4 id="totalquestions"> <?php echo Schoolhomework::checkQuestionsCount($model->TM_SCT_Id);?></h4>
                <!--<a href="<?php echo Yii::app()->createUrl('teachers/savestatus',array('id'=>$model->TM_SCT_Id))?>"><button type="button" class="btn btn-warning btn-block" ><?php echo ($model->TM_SCT_Status==0?'Deactivate':'Activate');?></button></a>-->
            </td>
            <td >
                <label class="font18">Total Marks</label>
                <h4 id="totalmarks"> </h4>                
            </td>                        
            <td id="publishraw" <?php echo (Schoolhomework::checkQuestions($model->TM_SCT_Id)=='visibility:visible'?'style="visibility:visible"':'style="visibility:hidden"');?>>                
                <a href="javascript:void(0)" class="choosegroups" data-backdrop="static" data-toggle="modal" data-target="#SelectGroup" data-value="<?php echo $model->TM_SCT_Id;?>" ><button type="button" class="btn btn-warning btn-block" >Publish</button></a>
            </td>            
        </tr>


    </table>

   

</div>
 <div <?php echo (Schoolhomework::checkQuestions($model->TM_SCT_Id)=='visibility:visible'?'style="visibility:visible"':'style="visibility:hidden"');?>>                
    <a href="javascript:void(0)" style="display:none;width:150px;" class="choosegroups publicBT" data-backdrop="static" data-toggle="modal" data-target="#SelectGroup" data-value="<?php echo $model->TM_SCT_Id;?>" ><button type="button" class="btn btn-warning btn-block" >Publish</button></a>
</div> 
<div class="modal fade " id="SelectGroup" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
               
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Publish Assessment</h4>
                </div>
                <div class="modal-body" >
                    <div class="panel-body form-horizontal payment-form">
                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-7">
                                    <input class="form-control" value="<?= $model->TM_SCT_Name ?>" type="text" id="practiceName" name="practiceName">
                                    <!-- <input type="hidden" id="homeworkid"> -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Class</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="standard" id="publishto">
                                        <option value="0">Select Class</option>
                                        <?php
                                        $teacherstandards=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));
                                        if(count($teacherstandards)>0):
                                            foreach($teacherstandards AS $teacherstandard):
                                                if($teacherstandard->standard->TM_SD_Id==Yii::app()->session['standard']):
                                                    $selected='selected';
                                                else:
                                                    $selected='';
                                                endif;
                                                echo "<option value='".$teacherstandard->standard->TM_SD_Id."' ".$selected.">".$teacherstandard->standard->TM_SD_Name."</option>";
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                    <input type="hidden" id="homeworkid">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish To</label>
                                <div class="col-sm-7" id="publishstdinfo">
                                    <input type="text" id="usersuggest" name="assigngroups[]">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish On <span class="glyphicon glyphicon-calendar"></span></label>
                                <div class="col-sm-7">
                                    <input type="date" id="publishdate" class="form-control" value="<?php echo date('Y-m-d');?>" name="publishdate">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Due On <span class="glyphicon glyphicon-calendar"></span></label>
                                <div class="col-sm-7">
                                    <input type="date" id="duedate" class="form-control" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d') .' +1 day'));?>" name="duedate">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Submission Type</label>
                                <div class="col-sm-8" id="type">
                                    <!--<select class="form-control" id="type" name="type">

                                    </select>-->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Solution</label>
                                <div class="col-sm-7">
                                    <input type="radio" class="showsolution" name="marks" value="0" checked="checked" /><span>Yes</span>
                                    <input type="radio" class="showsolution" name="marks" value="1" /><span>No</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Mark</label>
                                <div class="col-sm-7">
                                    <input type="radio" class="showmarks" name="mark" value="0" checked="checked" /><span>Yes</span>
                                    <input type="radio" class="showmarks" name="mark" value="1" /><span>No</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Comments</label>
                                <div class="col-sm-7">
                                    <textarea name="comments" class="form-control" id="comments"></textarea>
                                </div>
                            </div>
                        </div>                   <!--code by amal-->




                        <!--ends-->

                    </div>
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-success pull-left dateinfo" style="display: none;">Due date cannot be earlier than Publish date</span>
                    <span class="alert alert-success pull-left datedelay" style="display: none;">Due date cannot be earlier than Today</span>
                    <span class="alert alert-success pull-left datedelaypub" style="display: none;">Publish date cannot be earlier than Today</span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <button type="button" class="btn btn-warning" id="invitequickhw">Assign</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    </div>
    <div class="col-lg-12">
        <h3>Questions Added
        <a href="<?php echo Yii::app()->createUrl('SchoolQuestions/create',array("id"=>Yii::app()->session['school'],"assignment"=>$model->TM_SCT_Id,"action"=>"copy"))?>">
                <button class="btn pull-right btn-warning" type="button">Create</button>
            </a>
        </h3>
    </div>
    <!-- /.col-lg-12 --> 
<div class="col-lg-12">
<div class="panel panel-yellow addedqstable">    
    <div class="scrol-body" style="overflow-x: auto;">
        <table class="table table-striped table-bordered " style="min-width: 1300px">
            <thead>
            <tr>
<!--                <th>Chapter</th>
                <th>Topic</th>-->
                <th>Order</th>
                <th class="td-actions"></th>
                <th>Question</th>
                <th><a class="sort-mark" data-order="ASC" data-id=<?= $model->TM_SCT_Id ?> href='javascript::void(0)'>Mark</a></th>

                <!-- <th>Type</th> -->
                 <th>Chapter</th>
                <th>Topic</th>
                <th>Skill</th>
                <th>Info</th>
                <th>Reference</th>
                <th>Pattern</th>

                
            </tr>
            </thead>
            <tbody id="removequestionlist" class="wirisfont">
            <?php

                if($addedQuestion!=''):
                    foreach($addedQuestion AS $mockquestion):
                    $question=Questions::model()->findByPk($mockquestion->TM_CTQ_Question_Id);
                        if($question['TM_QN_School_Id']!=0):
                            $editbtn='<a class="editquestion" style="color: #848484 !important;" title="Edit Question" href="'.Yii::app()->createUrl('schoolQuestions/update',array("id"=>Yii::app()->session['school'],"question"=>$question->TM_QN_Id,"assignment"=>$model->TM_SCT_Id)).'"><span class="glyphicon glyphicon-pencil"></span></a>';
                        else:
                            $editbtn='<a class="editquestion" style="color: #848484 !important;" title="Edit Question" href="'.Yii::app()->createUrl('schoolQuestions/copyquestion',array("id"=>Yii::app()->session['school'],"assignment"=>$model->TM_SCT_Id,"question"=>$question->TM_QN_Id)).'"><span class="glyphicon glyphicon-pencil"></span></a>';
                        endif;
            ?>
                        <tr id="removequestion<?php echo $question->TM_QN_Id;?>" class="order<?php echo $mockquestion->TM_CTQ_Order?>qn" data-rawid="<?php echo $question->TM_QN_Id;?>" data-reorder="<?php echo $mockquestion->TM_CTQ_Order?>">
<!--                            <td><?php /*echo Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name;*/?></td>
                            <td><?php /*echo Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name;*/?></td>-->
                            <td>
                                <INPUT TYPE="image" id="orderlimkup<?php echo $question->TM_QN_Id;?>" class="order_link" data-info="up" SRC="<?php echo Yii::app()->request->baseUrl.'/images/up.gif'?>" ALT="SUBMIT" data-id="<?php echo $mockquestion->TM_CTQ_Custom_Id?>" data-order="<?php echo $mockquestion->TM_CTQ_Order?>" data-qnId="<?php echo $question->TM_QN_Id;?>">
                                <INPUT TYPE="image" id="orderlimkdown<?php echo $question->TM_QN_Id;?>" class="order_link" data-info="down" SRC="<?php echo Yii::app()->request->baseUrl.'/images/down.gif'?>" ALT="SUBMIT" data-id="<?php echo $mockquestion->TM_CTQ_Custom_Id?>" data-order="<?php echo $mockquestion->TM_CTQ_Order?>" data-qnId="<?php echo $question->TM_QN_Id;?>">
                            </td>
                            <td class="td-actions">

                                <a style="color: #848484 !important;" class="removequestion" title="Remove Question" data-id="<?php echo base64_encode($question->TM_QN_Id);?>" data-element="removequestion<?php echo $question->TM_QN_Id;?>"><span class="glyphicon glyphicon-remove"></span></a>
				<?php if($mockquestion->TM_CTQ_Header!=''){ ?>
                                    <a href="javascript:void(0)" style="color: #848484 !important;" class="sectionheader delete" data-backdrop="static" data-toggle="modal" data-target="#SelectHeader" data-id="<?php echo $model->TM_SCT_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>" ><span style="font-size: 13px;"  class="glyphicon glyphicon-header textcolor"></span></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" style="color: #848484 !important;" class="sectionheader" data-backdrop="static" data-toggle="modal" data-target="#SelectHeader" data-id="<?php echo $model->TM_SCT_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>" ><span style="font-size: 13px;" class="glyphicon glyphicon-header"></span></a>
                                <?php } ?>
                                <?php
                                   $questionoptions=CustomtemplateQuestions::model()->find(array('condition'=>"TM_CTQ_Custom_Id='".$model->TM_SCT_Id."' AND TM_CTQ_Question_Id='".$question->TM_QN_Id."' "));
                                   if($questionoptions['TM_CTQ_Show_Options']==0)
                                    {
                                ?>
                                    <label class="switch" title="MCQ">
                                    <input  class="save" id="saveoptions" type="checkbox" data-id="<?php echo $model->TM_SCT_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>" name="option" >
                                    <span class="slider rounded"></span>
                                    </label>
                                <?php } elseif($questionoptions['TM_CTQ_Show_Options']==1) { ?>
                                    <label class="switch" title="MCQ">
                                    <input  class="save" id="saveoptions" type="checkbox" checked="checked" data-id="<?php echo $model->TM_SCT_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>"  name="option">
                                    <span class="slider rounded"></span>
                                    </label>
                                <?php } ?>
				<?= $editbtn;?>
                            </td>
                            <td><?php echo $question->TM_QN_Question; ?></td>
                            <td class="marks"><?php echo $question->TM_QN_Totalmarks;?></td>
                         


                            <!-- <td><?php echo Types::model()->findByPk($question->TM_QN_Type_Id)->TM_TP_Name;?></td> -->
                            <td><?php echo Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name;?></td>
                            <td><?php echo Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name;?></td>
                            <td><?php
                                $skill_ids=explode(",",$question->TM_QN_Skill);
                                $skill_names=array();
                               foreach ($skill_ids as $key => $skill_id) {
                                $skill_name = TmSkill::model()->findByPk($skill_id)->tm_skill_name;
                                array_push($skill_names,$skill_name);

                               }
                               $skills=implode(", ",$skill_names);
                             echo $skills; ?></td>

                            <td><?php
                            $type_ids=explode(",",$question->TM_QN_Question_type);
                            $type_names=array();
                            foreach ($type_ids as $key => $type_id) {
                             $type_name = QuestionType::model()->findByPk($type_id)->TM_Question_Type; 
                             array_push($type_names,$type_name);

                            }
                            $types=implode(",",$type_names);

                            echo $types; ?></td>
                            <td><?php echo $question->TM_QN_QuestionReff;?></td>
                            <td><?php echo $question->TM_QN_Pattern;?></td>


                            
                        </tr>
            <?php   endforeach;
                else:
            ?>
                    <tr id="noquestions">
                        <td colspan="6">No Questions added yet</td>
                    </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>
</div>

    <div class="col-lg-12">
        <h3 class="nomargin">Select Question To Add</h3>
    </div>
<div class="col-lg-12">
<div class="panel panel-yellow" style="overflow-x: auto;">    
    <form id="Filterform">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>      
            <th>Chapter</th>
            <th>Topic</th>
            <th>Mark</th>
            <th>Skill</th>
            <th>Difficulty</th>
            <th>Notes</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
         
            <td>
                <?php
                $standards = CHtml::listData(Chapter::model()->findAll(array('condition' => "TM_TP_Standard_Id =  $model->TM_SCT_Standard_Id AND TM_TP_Status = '0'", 'order' => 'TM_TP_order ASC')), 'TM_TP_Id', 'TM_TP_Name');
                $selectarray = array('empty' => 'Select Chapter', 'class' => 'form-control','ajax' =>
                    array(
                        'type'=>'POST', //request type
                        'url'=>CController::createUrl('SchoolQuestions/gettopiclist'), //url to call.
    //                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                        //Style: CController::createUrl('currentController/methodToCall')
                        'update'=>'#topic', //selector to update
                        'data'=>'js:{id:$(this).val()}'
                        //leave out the data key to pass all form values through
                    ));
                echo CHtml::dropDownList('chapter', '',$standards,$selectarray);
                ?>
            </td>
            <td>
                <?php
                echo CHtml::dropDownList('topic', '',array(''=>'Select Topic'),array('class'=>'form-control'));
                ?>
            </td>          
            <td>
             <input type="text" class="form-control" name="mark" id="mark">
            </td> 

            <td>
                 <?php 
                echo CHtml::dropDownList('skill', '',CHtml::listData(TmSkill::model()->findAll(array("condition"=>"tm_standard_id = '".Yii::app()->session['standard']."'",'order'=>'id')),'id','tm_skill_name'),array('empty'=>'Select Skill','class'=>'form-control'));
            ?>
            </td> 

            <td>
                <?php
                echo CHtml::dropDownList('difficulty', '',CHtml::listData(Difficulty::model()->findAll(array("condition"=>"TM_DF_Status = '0'",'order'=>'TM_DF_Id')),'TM_DF_Id','TM_DF_Name'),array('empty'=>'Select Difficulty','class'=>'form-control'));
            ?></td> 

            <td>                                   

                <input type="text" class="form-control" name="question_info" id="question_info">
                    
            </td>
        </tr>
        <tr>
            <th>Additional Info</th>
            <th>Pattern</th>
            <th>Reference</th>
            <th>Source</th>
            <th>Teacher</th>
            <th>Search</th>
        </tr>
        <tr>

            <td>
            <?php
                echo CHtml::dropDownList('question_type', '',CHtml::listData(QuestionType::model()->findAll(array("condition"=>"tm_standard_id = '".Yii::app()->session['standard']."'",'order'=>'id')),'id','TM_Question_Type'),array('empty'=>'Select One','class'=>'form-control','multiple'=>true));
            ?>
            </td>

            <td><input type="text" class="form-control" name="pattern" id="pattern"></td>
            <td>                                   
        <?php
                ///$refs=Questions::model()->getReffs();
            ?>
                <input type="text" class="form-control" name="reference" id="reference">
                    
            </td>
            <td>
                <?php
                echo CHtml::dropDownList('publisher', '',array(''=>'Select Source','1'=>'System Questions','0'=>'School Questions'),array('class'=>'form-control'));
                ?>
            </td> 
            <td>
                <?php
                echo CHtml::dropDownList('teacher', '',CHtml::listData(Teachers::model()->findAll(array("condition"=>"TM_TH_SchoolId = '".Yii::app()->session['school']."'",'order'=>'TM_TH_Id')),'TM_TH_Id','TM_TH_Name'),array('empty'=>'Select Teacher','class'=>'form-control'));
            ?></td>
            <td class="td-actions">
                <!--<input type="hidden" name="publisher" value="<?php echo $model->TM_SCT_Publisher_Id;?>">-->
                <input type="hidden" name="syllabus" value="<?php echo $model->TM_SCT_Syllabus_Id;?>">
                <input type="hidden" name="standard" value="<?php echo $model->TM_SCT_Standard_Id;?>">
                <input type="hidden" name="mock" value="<?php echo $model->TM_SCT_Id;?>">
                <button type="button" class="btn btn-warning btn-block" id="fliterquestion">Search</button>
            </td> 
        </tr>
        </tbody>
    </table>
    </form>
</div>
</div>

    <!-- /.col-lg-12 -->

<div class="col-lg-12">
<div class="panel panel-yellow listqstable" style="overflow-x: scroll;">
    <div class="scrol-body" style="width: 250%;">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
<!--                <th>Chapter</th>
                <th>Topic</th>-->
                <th class="button-column"></th>
                <th>Mark</th> 
                <th>Difficulty</th>                 
                <th>Question</th>
                <th>Skill</th>
                <th>Info</th>
                <th>Reference</th>
                <th>Pattern</th>                
                
            </tr>
            </thead>
            <tbody id="addqiestionlist" class="wirisfont">
               <td colspan="6">Search questions to add</td>
            </tbody>
        </table>
    </div>
</div>
    </div>
    <!-- /.col-lg-12 -->
</div>


<!--code by amal-->
<div class="modal fade" id="Preview">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Question</h4>
            </div>
            <div class="modal-body" id="previewqstns">

                <input type="hidden" id="homeworkid">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
 <div class="modal fade" id="SelectHeader">
     <div class="modal-dialog modal-md">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title">Add Section Header</h4>
             </div>
             <div class="modal-body">
                 <div class="panel-body form-horizontal payment-form">
                     <div class="row">
                         <div class="form-group">
                             <label class="col-sm-3 control-label">Header</label>
                             <div class="col-sm-7">
                                 <input class="form-control" type="text" id="practiceheader" name="Headername">
                             </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="form-group">
                             <label class="col-sm-3 control-label">Instructions</label>
                             <div class="col-sm-7">
                                 <textarea name="instructions" class="form-control" id="instructions"></textarea>
                             </div>
                         </div>
                     </div>
                 </div>
                 <input type="hidden" id="headquestionid">
                 <input type="hidden" id="practiceid">
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-warning" id="addsectionhead">Add</button>
                 <button type="button" class="btn btn-warning" data-dismiss="modal" id="inviteclose">Close</button>


                 <button style="display: none;" type="button" class="btn btn-warning"  id="deleteheader">Delete</button>
             </div>
         </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
 </div>
<!--ends-->
<script type="text/javascript">
    function gettotal()
    {
        var totalmark=0;
        $('.marks').each(function(){
            totalmark=totalmark+($(this).html()*1);
        });
        $('#totalmarks').html(totalmark)        
    }
    $(function(){
        gettotal();  
        $('#fliterquestion').click(function(){
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl;?>/teachers/Getquestions',
                data: $('#Filterform').serialize(),
                beforeSend: function(){
                    $('#addqiestionlist').html('<tr><td colspan="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader"></td></tr>');
                },
                success: function(data){
                    $('#addqiestionlist').html(data);
                    MathJax.Hub.Queue(["Typeset",MathJax.Hub, "page-wrapper"]);

                }
            });
        });
    });


    $(document).on('click touchstart','.sort-mark',function(){
        // $('#Preview').modal('show');
        var practiceId=$(this).attr('data-id');

        /*var order = $(this).attr('data-order');
        if(order == 'ASC')
            $(this).attr("data-order","DESC");
        else
            $(this).attr("data-order","ASC");*/

        var order  = "<?php echo $_GET['sort'] ?>";
       
        if(order=='')
        {
            order='DESC'
           
        }
        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/teachers/ChangeSort',
            dataType: "json",
            data: {practiceId:practiceId, order:order},
            success: function(data){


            }
        });
       
        var sort_order = "<?php echo $_GET['sort']; ?>";
        if(sort_order=='ASC')
        {
             location.reload();
         window.location = "?sort=DESC";
        }
        else
        {
             location.reload();
            window.location = "?sort=ASC";
        }
    });
    /*code by amal*/
    $(document).on('click touchstart','.previewquestion',function(){
        $('#Preview').modal('show');
        var questionid=$(this).attr('data-id');
        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/teachers/Previewquestion',
            dataType: "json",
            data: {question:questionid},
            success: function(data){
                if(data.success=='yes')
                {
                    $('#previewqstns').html(data.result);
                    MathJax.Hub.Queue(["Typeset",MathJax.Hub, "modal-body"]);

                }
            }
        });
    });
    /*ends*/    
    $(document).on('click touchstart','.addquestion',function(){
        var question=$(this).attr('data-id');
        var questionrow=$(this).attr('data-element');
        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/teachers/addquestion',
            dataType: "json",
            data: {mock:'<?php echo base64_encode($model->TM_SCT_Id);?>',question:question},
            success: function(data){
                if(data.success=='yes')
                {
                    $('#'+questionrow).remove();
                    $('#noquestions').remove();
                    $('#totalquestions').html(data.count);
                    if(data.count=='0')
                    {
                        $('#removequestionlist').html(data.insertrow);
                    }
                    else
                    {
                        $('#removequestionlist').append(data.insertrow);
                        $('#publishraw').css("visibility", "visible");   
                    }
                    gettotal();
                    MathJax.Hub.Queue(["Typeset",MathJax.Hub, "page-wrapper"]);

                }
            }
        });
    });
    $(document).on('click touchstart','.removequestion',function(){
        var question=$(this).attr('data-id');
        var questionrow=$(this).attr('data-element');
        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/teachers/removequestion',
            dataType: "json",
            data: {mock:'<?php echo base64_encode($model->TM_SCT_Id);?>',question:question},
            success: function(data){
                if(data.success=='yes')
                {
                    $('#'+questionrow).remove();
                    //$('#fliterquestion').trigger('click');
                    $('#totalquestions').html(data.count);
                    if(data.count=='0')
                    {
                        $('#publishraw').css("visibility", "hidden");  
                    }
                    else
                    {
                        $('#publishraw').css("visibility", "visible");
                    }
                    gettotal();
                }
            }
        });
    });
    $(document).on('click touchstart','.order_link',function(){
        var id=$(this).attr('data-id');
        var info=$(this).attr('data-info');

        var order=($(this).attr('data-order')*1);
        var qnId=$(this).attr('data-qnId');
        var orderprev=order-1;
        var ordernext=order+1;

        var prevquestionId=$('.order'+orderprev+'qn').attr('data-rawid');
        var nextquestionId=$('.order'+ordernext+'qn').attr('data-rawid');


        var orderclass=$('.order'+orderprev+'qn');

        var orderclassprev=$('.order'+order+'qn');

        var orderclassnext=$('.order'+ordernext+'qn');


        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/teachers/MockOrder',
            dataType: "text",
            data: {id:id,info:info,order:order},
            success: function(data){
                if(data=='yes')
                {
                    if(info=='up'){

                        orderclass.insertAfter(orderclassprev);

                        $('#orderlimkup'+qnId).attr('data-order',order-1);
                        $('#orderlimkdown'+qnId).attr('data-order',order-1);
                        orderclassprev.toggleClass('order'+order+'qn order'+orderprev+'qn');
                        orderclass.toggleClass('order'+orderprev+'qn order'+order+'qn');
                        $('#orderlimkup'+prevquestionId).attr('data-order',order);
                        $('#orderlimkdown'+prevquestionId).attr('data-order',order);

                    }
                    else{

                        orderclassprev.insertAfter(orderclassnext);

                        $('#orderlimkup'+qnId).attr('data-order',order+1);
                        $('#orderlimkdown'+qnId).attr('data-order',order+1);

                        $('.order'+ordernext+'qn').toggleClass('order'+ordernext+'qn  order'+order+'qn');
                        ordernext=order+1;
                        orderclassprev.toggleClass('order'+order+'qn  order'+ordernext+'qn');
                        $('#orderlimkup'+nextquestionId).attr('data-order',order);
                        $('#orderlimkdown'+nextquestionId).attr('data-order',order);





                    }

                }
            }
        });
    });

    $(".listqstable .scrol-body").mCustomScrollbar({
        setHeight:500
    });
</script>
<script type="text/javascript">

        $(function(){
            $('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            var names=$('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            $('#publishto').change(function(){
                var std=$(this).val();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/Groups";?>',
                    data: {standard:std},
                    success: function(data){
                        //console.log(data)
                        $('#publishstdinfo').html('<input type="text" id="usersuggest" name="assigngroups[]">');
                        $('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                        names=$('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                        //names.removeFromSelection(names.getSelection(), true);
                        //names.setValue(data);
                    }
                });
                //names.setValue([{"id":"782","name":"Standard X-A"}]);
            });
            $(document).on('click touchstart','.sectionheader',function(){
                var practquestid = $(this).data('value');
                var hwid=$(this).attr('data-id');
                $('#headquestionid').val(practquestid);
                $('#practiceid').val(hwid);
                $('#deleteheader').hide();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/GetHeaderCustom";?>',
                    data: {questionid:practquestid,homeworkid:hwid},
                    success: function(data){
                        $('#practiceheader').val(data.header);
                        $('#instructions').val(data.section);
                    }
                });
            });
            $(document).on('click','.delete',function(){
                $('#deleteheader').show();
            });
            $('#addsectionhead').click(function(){
                $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('teachers/AddSection');?>',
                        beforeSend : function(){
                            $("#addsectionhead").prop("disabled", true);
                        },
                        data: {qnids: $('#headquestionid').val(),practiceid:$('#practiceid').val(),header:$('#practiceheader').val(),section:$('#instructions').val()}
                    })
                    .done(function (data) {
                        $("#addsectionhead").prop("disabled", false);
                        setTimeout(function() {
                            $('#SelectHeader').modal('toggle');
                            ///window.location.reload();
                        }, 1000);
                        location.reload();
                    });
            });
            $(document).on('click','#deleteheader',function(){
                var questionid = $('#headquestionid').val();
                var homeworkid = $('#practiceid').val();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/deleteheader";?>',
                    data: {questionid:questionid,homeworkid:homeworkid},
                    success: function(data){
                        location.reload();

                    }
                });

            });
	    $(document).on('change','#saveoptions',function(){

                if($(this).is(':checked'))
                {
                    var option = 1;
                }
                else
                {
                    var option =0;
                }
                var qnids = $(this).data('value');
                var hwid  =  $(this).attr('data-id');
                $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('teachers/qstnoptions');?>',
                        data: {qnids:qnids,practiceid:hwid,option:option}
                    })
                    .done(function (data) {

                    });
            });
            $('.choosegroups').click(function(){
                
                $('.nogroups').hide();
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).data('value');
                
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/GetGroupsCustom";?>',
                    data: {id:schoolid,homeworkid:homeworkid},
                    success: function(data){
                       if(data.count=='0')
                       {
                           $('.nogroups').html('You have no groups for the school').show();
                       }
                       $('#type').html(data.result);
                   }
                });
            });
            $('#invitequickhw').click(function() {
                $('.inviteinfo').hide();
                var nameids=names.getValue();
                if($('#homeworkid').val()=='' || $('#duedate').val()=='' || $('#publishdate').val()==''){
                    $('.inviteinfo').text('Please enter all fields.');
                    $('.inviteinfo').show();
                    $("#invitequickhw").prop("disabled", false);
                    return false;
                }
                else{                
                    //$('#buddyindication').text('')
                    var from = $('#publishdate').val().split("-");
                    var publish = new Date(from[0], from[1] - 1, from[2]);
                    var to = $('#duedate').val().split("-");
                    var due = new Date(to[0], to[1] - 1, to[2]);
                    var dateObj = new Date();
                    var month = dateObj.getMonth(); //months from 1-12
                    var day = dateObj.getDate();
                    var year = dateObj.getFullYear();
                    var d = new Date(year,month,day);
                    var practiceName=$('#practiceName').val();


                    if (publish > due) {
                        $('.dateinfo').show();
                        $('.dateinfo').show();
                        setTimeout(function () {
                            $('.dateinfo').hide();
                        }, 5000);
                        return false;
                    }
                    else if(publish < d ){
                        $('.datedelaypub').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else if( due < d ){
                        $('.datedelay').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                else {                                      
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->createUrl('teachers/PublishCustomHomework');?>',
                            beforeSend : function(){
                                $("#invitequickhw").prop("disabled", true);
                            },                            
                            data: {nameids: nameids,homeworkid:$('#homeworkid').val(),dudate:$('#duedate').val(),comments:$('#comments').val(),publishdate:$('#publishdate').val(),type:$("input[name=type]:checked").val(),solution:$('.showsolution:checked').val(), practiceName: practiceName,publishto:$('#publishto').val(),showmark:$('.showmarks:checked').val()}
                        })
                            .done(function (data) {
                                $('.addgroupsinfo').text('Assessment assigned to groups.');
                                $('.addgroupsinfo').show();
                                //$('#inviteclose').trigger('click');
                                setTimeout(function() {
                                    $('#SelectGroup').modal('toggle');
                                    window.location.reload();
                                }, 2000);                                
                            });

                    }
                }
            });
        });
    </script>
