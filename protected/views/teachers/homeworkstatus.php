<div class="row">
    <div class="col-md-12">
            <h3><?php echo $homework->TM_SCH_Name;?> Status</h3>                     
    </div>              
</div>
<div class="row brd1">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="tab-content">
  
                <?php if(count($startedstudents)>0):?>
                    <div class="tab-pane fade" id="tab1default">Default 1</div>
                        <h4 class="notatt">ATTEMPTED <span class="badge bdg1 badge-warning"><?php echo count($startedstudents);?></span></h4>
                        <div class="row">                    
                            <div class="tab-pane fade active in" id="tab2default">              
                                <?php
                                $script='';
                                $count=1;                                
                                 foreach($startedstudents AS $notstarted):
                                        $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$notstarted->TM_STHW_Student_Id))
                                ?>
                                <div class="col-md-4 col-sm-6">
                                	<div class="not_attmpt" style="border: 0px;">
                                		<div class="active item">							  
                                		  <div class="carousel-info">        								
                                			<div class="pull-left">
                                			  <span class="not_attmpt-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>                                                                      			  
                                			</div>
                                		  </div>
                                		</div>
                                	</div>
                                </div> 
                                <div class="col-md-2 col-sm-6">
                                    <?php //if($this->GetAverage($notstarted->TM_STHW_Student_Id,$notstarted->TM_STHW_HomeWork_Id)!=0):?>
                                   <!--<div class="btmy">                                                                        
                                    <div id="pbar" class="progress-pie-chart" >
                                    <div class="ppc-progress">
                                    <div class="ppc-progress-fill" ></div>
                                    </div>
                                    <div class="ppc-percents">
                                    <div class="pcc-percents-wrapper">
                                    <span><?php echo $this->GetAverage($notstarted->TM_STHW_Student_Id,$notstarted->TM_STHW_HomeWork_Id);?>%</span>
                                    </div>
                                    </div>
                                    </div>                                    
                                    <progress style="display: none" id="progress_bar" class="pieprogress"  value="0" max="<?php echo $this->GetAverage($notstarted->TM_STHW_Student_Id,$notstarted->TM_STHW_HomeWork_Id);?>"></progress>                                            							                                            								
                                    </div>-->
                                    <?php //endif;?>
                                    <div id="<?php echo 'prog'.$count;?>" class="containerdiv">
                                    	<div class="jCProgress" style="opacity: 1;">
                                    		<div class="percent" style="display: none;"><?php echo $this->GetAverage($notstarted->TM_STHW_Student_Id,$notstarted->TM_STHW_HomeWork_Id);?></div>
                                    		<canvas width="55" height="55"></canvas>
                                    	</div>
                                    </div>
                                    <?php
                                    $average=($this->GetAverage($notstarted->TM_STHW_Student_Id,$notstarted->TM_STHW_HomeWork_Id)=='0'?'0':$this->GetAverage($notstarted->TM_STHW_Student_Id,$notstarted->TM_STHW_HomeWork_Id));
                                    if(Yii::app()->session['userlevel']==1):
                                        $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".$average.",loop : false,showPercent : true});";
                                    else:
                                        $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$average.",loop : false,showPercent : true});";
                                    endif;
                                    ?>
                                </div>
                                 <?php $count++;endforeach;?>
                            </div>
                            </div>
                    <?php endif;?>               
                <?php if(count($notstartedstudents)>0):?>
                    <div class="tab-pane fade" id="tab1default">Default 1</div>
                        <h4 class="notatt">NOT ATTEMPTED <span class="badge bdg1 badge-warning"><?php echo count($notstartedstudents);?></span></h4>                    
                         <div class="row">  
                            <div class="tab-pane fade active in" id="tab2default">              
                                <?php foreach($notstartedstudents AS $notstarted):
                                        $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$notstarted->TM_STHW_Student_Id))
                                ?>
                                <div class="col-md-4 col-sm-6">
                                	<div class="not_attmpt" style="border: 0px;;">
                                		<div class="active item">							  
                                		  <div class="carousel-info">        								
                                			<div class="pull-left">
                                			  <span class="not_attmpt-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>                        			  
                                			</div>
                                		  </div>
                                		</div>
                                	</div>
                                </div>                        
                                <?php endforeach;?>                        
                            </div>
                         </div> 
                    <?php endif;?>                    
                    </div>                                         
            </div>
        </div>
    </div>
</div>    

<script>
		$(document).ready(function() {
		<?php echo $script;?>    
		var progressbar = $('.pieprogress');
		max = progressbar.attr('max');
		time = (50 / max) * 5;
		value = progressbar.val();

		var loading = function() {
		value += 1;
		addValue = progressbar.val(value);

		$('.progress-value').html(value + '%');
		var $ppc = $('.progress-pie-chart'),
		deg = 360 * value / 100;
		if (value > 50) {
		$ppc.addClass('gt-50');
		}

		$('.ppc-progress-fill').css('transform', 'rotate(' + deg + 'deg)');
		$('.ppc-percents span').html(value + '%');

		if (value == max) {
		clearInterval(animate);
		}
		};

		var animate = setInterval(function() {
		loading();
		}, time);
		});
	</script>