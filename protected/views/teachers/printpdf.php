<!DOCTYPE html>
<html>
<head>
    <!-- This line adds MathJax to the page with default SVG output -->
    <!--<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->


<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>-->
<style>
    .navbar-fixed-top
    {
        display: none !important;
    }
    .MathJax span
    {
        /*font-size: 14px !important;*/
        font-size: 14px;
    }

    tr {
        min-height: 5px;
        height: 5px;
    }
   html
    {
        margin:0 !important;
        background-color: white;
    }
    body{
        visibility: hidden;
        margin-top: 50px !important;
        font-size: 14px !important;
    }
    .container-fluid
    {
        visibility: visible;
        background-color: white;
    }
    @media print {
        @page {
            margin-bottom:0.6cm;
            margin-top: 0.8cm;
            /*margin-top:0.6cm;*/
            /*padding-top: 50px;
            padding-bottom: 50px;*/
            /*padding: 2cm;
            margin: 3cm;*/ 


        }

        @page:first
        {
            margin-top: 0.2cm;
        }

        html {
            margin:0cm
        }
        body{
            margin-top:0 !important;

        }
        .nonPrintingContent{
            display:none;
        }

        #footer {
            /*z-index: :999;*/
            display: block !important; 
            position: relative; 
            bottom: 0;
            padding:8px;
        }
    }
</style>
</head>
<body >
    <div class="nonPrintingContent">
        <a href='javascript::void(0)' style='float:right;margin-right:10px;' class='btn btn-warning btn-xs' id="printWork">
        <span style='padding-right: 10px;'>Print</span>
        <i class='fa fa-print fa-lg'></i>
        </a>
    </div>
  
   <?php
	echo $html;
	?> 

    <!--<div id="mpdf-create">-->
    <!--    <form autocomplete="off" action="<?php echo Yii::app()->request->baseUrl; ?>/teachers/printAll" method="POST" id="pdfform" onSubmit="document.getElementById('bodydata').value=encodeURIComponent(document.body.innerHTML);">-->
    <!--    <input type="submit" value="PDF" name="submit"/>-->
    <!--    <input type="hidden" value="" id="bodydata" name="bodydata" />-->
    <!--    </form>-->
    <!--</div>-->
</body>
 <?php
// $documentTemplate = $html;
// foreach ($_POST as $key => $postVar)
// {
//     $documentTemplate = 
//     preg_replace ("/name=\"$key\"/", "value=\"$postVar\"", $documentTemplate);
// }
// file_put_contents ("out.html", $documentTemplate);
// shell_exec ("wkhtmltopdf out.html test.pdf");
 ?>
<script>
//  $( document ).ready(function() {
//      setTimeout(function(){
//     //     let doc = new jsPDF('p','pt','a4');
//     // doc.addHTML(document.body,function() {
//     //     doc.save('html.pdf');
//     // });
//     window.print();
//      }, 3000);
// });
$( document ).ready(function() {
    $('#printWork').click(function(){
        // window.scrollTo(0, 0);
        window.print();
    });
});
</script>
</html>