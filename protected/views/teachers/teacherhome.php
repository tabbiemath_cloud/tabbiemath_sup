<style>
.grid-view .button-column
{
    width: 250px !important;
    text-align: right;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <h3 class="h125">Dashboard</h3>
        <input type="hidden" id="schoolid" value="<?php echo Yii::app()->session['school'];?>">
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'homework-grid',
                    'dataProvider'=>$model->search(Yii::app()->user->id,Yii::app()->session['standard'],'All','1'),
                    //'filter'=>$model,
                    'columns'=>array(
                        array(
                            'name'=>'TM_SCH_Master_Stand_Id',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetMasterStd($data->TM_SCH_Master_Stand_Id,"Class")',
                        ),
                        array(
                            'name'=>'TM_SCH_Name',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetData($data->TM_SCH_Name,"Name")',
                        ), 
                        /*array(
                            'name'=>'Assessment Type',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetData($data->TM_SCH_Type,"Homework Type")',
                        ),                        
                        array(
                            'name'=>'TM_SCH_Standard_Id',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetData(Standard::model()->findByPk($data->TM_SCH_Standard_Id)->TM_SD_Name,"Standard")',
                        ),    */                                            
                        array(
                            'name'=>'TM_SCH_PublishDate',                        
                            'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_SCH_PublishDate))',
                        ),
                        array(
                            'name'=>'TM_SCH_DueDate',                        
                            'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_SCH_DueDate))',
                        ),
                        array(
                            'type'=>'raw',
                            'name'=>'TM_SCH_PublishType',
                            'value'=>'Schoolhomework::GetPublish($data->TM_SCH_PublishType,$data->TM_SCH_Type)',
                        ),
                        /*array(
                            'type'=>'raw',
                            'name'=>'Assign Marking',
                            'value' => 'Homeworkmarking::checkAssignes($data->TM_SCH_Id)',
                        ), 
                        array(
                            'type'=>'raw',
                            'name'=>'TM_SCH_PublishType',
                            'value'=>'Schoolhomework::GetData($data->TM_SCH_PublishType,"Submission Type")',
                        ),*/
                                                 
                        array(
                            'type'=>'raw',
                            'name'=>'Status',
                            'value' => 'Homeworkmarking::checkAssignesStatus($data->TM_SCH_Id)',
                        ),                                                
                        /*array(
                            'name'=>'Assigned Groups',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetAssignees($data->TM_SCH_Id)',
                        ), 
                                                
                        array(
                            'type'=>'raw',
                            'name'=>'Publish',
                            'value' => 'CHtml::link("Publish","javascript:void(0)",array(
                            "title"=>"publish",
                            "class"=>"choosegroups btn btn-warning btn-xs",
                            "style"=>Schoolhomework::checkQuestions($data->TM_SCH_Id),
                            "data-backdrop"=>"static",
                            "data-toggle"=>"modal",
                            "data-target"=>"#SelectGroup",
                            "data-value"=>"$data->TM_SCH_Id"
                            ))',
                        ), */                                                 
                        array(
                        'class' => 'CButtonColumn',
                        'template' => '{print}{printwork}{solution}{assign}{assigncolor}{edit}{move}{delete}{marking}',
                        'deleteButtonImageUrl' => false,
                        'updateButtonImageUrl' => false,
                        'viewButtonImageUrl' => false,

                        'buttons' => array
                        (
                            'print' => array(
                                'label' => '<span class="glyphicon glyphicon-print"></span>',
                                'url' => '$data->TM_SCH_Id',
                                'options' => array(
                                    'class' => 'printworksh',
                                    'title' => 'Print Worksheet',
                                     'data-backdrop' => 'static',
                                    'data-toggle' => 'modal',
                                    'data-value' => '$data->TM_SCH_Id',
                                ),
                                'visible' => 'Schoolhomework::checkWorksheet($data->TM_SCH_Worksheet)',
                                ),
                            'printwork' => array(
                                'label' => '<span class="glyphicon glyphicon-print"></span>',
                                'url' => '$data->TM_SCH_Id',
                                'options' => array(
                                    'class' => 'printworksh',
                                    'title' => 'Print Worksheet',
                                     'data-backdrop' => 'static',
                                    'data-toggle' => 'modal',
                                    'data-value' => '$data->TM_SCH_Id',
                                ),
                                'visible' => 'Schoolhomework::checkWorksheetStatus($data->TM_SCH_Worksheet)',
                                ),                                
                            'solution' => array(
                                'label' => '<span class="glyphicon glyphicon-list-alt"></span>',
                                'url' => '$data->TM_SCH_Id',
                                'options' => array(
                                    'title' => 'Print Solution',
                                    'class' => 'printworksol',
                                    'data-backdrop' => 'static',
                                    'data-toggle' => 'modal',
                                ),
                                //'visible' => 'Homeworkmarking::checkCompleteStatus($data->TM_SCH_Id)',
                                ),
                            'assign' => array(
                                'label' => '<span class="glyphicon glyphicon-user"></span>',
                                'url' => '$data->TM_SCH_Id',
                                'visible'=>'!Schoolhomework::model()->GetMarkingStatus($data->TM_SCH_Id)',
                                'options' => array(
                                    'title' => 'Assign',
                                    'class' => 'choosegroups',
                                    'data-backdrop' => 'static',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#SelectGroup',
                                    'data-value' => '$data->TM_SCH_Id',
                                )),
                            'assigncolor' => array(
                                'label' => '<span class="glyphicon glyphicon-user"></span>',
                                'url' => '$data->TM_SCH_Id',
                                'visible'=>'Schoolhomework::model()->GetMarkingStatus($data->TM_SCH_Id)',
                                'options' => array(
                                    'title' => 'Assign',
                                    'class' => 'choosegroups active',
                                    'data-backdrop' => 'static',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#SelectGroup',
                                    'data-value' => '$data->TM_SCH_Id',
                                )),
//Changes done for edit button by vigil
                            'edit' => array(
                                'label' => '<span class="glyphicon glyphicon-edit"></span>',
                                //other params
                                'visible' => 'Homeworkmarking::checkCompleteStatus($data->TM_SCH_Id)',
                                'url' => '$data->TM_SCH_Id',
                                'options' => array(
                                    'title' => 'Edit',
                                    'class' => 'edithome',
                                    'data-backdrop' => 'static',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#SelectGroupedit',
                                    'data-value' => '$data->TM_SCH_Id',

                                )),
                            'marking' => array(
                                'label' => 'Mark',
                                'url' => 'Yii::app()->createUrl("teachers/markhomework",array("id"=>$data->TM_SCH_Id))',
                                'options' => array(
                                    'title' => 'Mark Practice',
                                    'class' => 'publishtest btn btn-warning btn-xs',
                                )),
                            'move' => array(
                                'label' => '<span class="glyphicon glyphicon-ok"></span>',
                                'url' => 'Yii::app()->createUrl("teachers/Movetohistory",array("id"=>$data->TM_SCH_Id))',
                                'options' => array(
                                    'title' => 'Move to History',
                                    'class'=>'movehistory'

                                ),
                            ),
                            'delete' => array(
                                'label' => '<span class="glyphicon glyphicon-trash"></span>',
                                'url' => 'Yii::app()->createUrl("teachers/Deleteassignedhomework",array("id"=>$data->TM_SCH_Id))',
                                'options' => array(
                                    'title' => 'Delete',

                                ),

                                'visible' => 'Homeworkmarking::checkCompleteStatus($data->TM_SCH_Id)',

                            ),
                        ),
                    ),                        
                    ),'itemsCssClass'=>"rwd-tables"
                )); ?>        
        </div>
    </div>
    <div class="modal fade " id="SelectGroupedit" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title h125">Update Publish Information</h4>
                </div>
                <div class="modal-body" >
                    <div class="panel-body form-horizontal payment-form">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Class</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="standard" id="publishto">
                                        <option value="0">Select Class</option>
                                        <?php
                                        $teacherstandards=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));
                                        if(count($teacherstandards)>0):
                                            foreach($teacherstandards AS $teacherstandard):
                                                if($teacherstandard->standard->TM_SD_Id==Yii::app()->session['standard']):
                                                    $selected='selected';
                                                else:
                                                    $selected='';
                                                endif;
                                                echo "<option value='".$teacherstandard->standard->TM_SD_Id."' ".$selected.">".$teacherstandard->standard->TM_SD_Name."</option>";
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish To</label>
                                <div class="col-sm-7" id="publishstdinfo">
                                    <input type="text" id="editusersuggest" name="editassigngroups[]">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="homeworkedit">
                        </div>                                    
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <span class="alert alert-success pull-left dateinfo" style="display: none;">Due date cannot be earlier than Publish date</span>
                    <span class="alert alert-success pull-left datedelay" style="display: none;">Due date cannot be earlier than Today</span>
                    <span class="alert alert-success pull-left datedelaypub" style="display: none;">Publish date cannot be earlier than Today</span>
                    <button type="button" class="btn btn-default" id="changehw">Change</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade " id="SelectGroup" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title ">Choose Users for Marking</h4>
                </div>
                <div class="modal-body" >
                    <input type="text" id="usersuggest" name="assigngroups[]">
                    <input type="hidden" id="homeworkid">                    
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <button type="button" class="btn btn-default" id="invitequickhw">Assign</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>    
</div>
<?php 
//echo Homeworkmarking::model()->GetCount();
if(Homeworkmarking::model()->GetCount()>0):?>
<div class="row">
    <div class="col-lg-12">
        <h3>Assigned for marking</h3>    
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
                    //'id'=>'homework-grid',
                    'dataProvider'=>$modelmarking->search(Yii::app()->user->id,0),
                    //'filter'=>$model,
                    'columns'=>array(
                        array(
                            'name'=>'Name',
                            'type'=>'raw',
                            'value'=>'$data->homework->TM_SCH_Name',
                        ), 
                        array(
                            'name'=>'TM_HWM_Assigned_On',
                            'type'=>'raw',
                            //'value'=>'$data->TM_HWM_Assigned_On',
                            'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_HWM_Assigned_On))',
                        ), 
                        array(
                            'type'=>'raw',
                            'name'=>'TM_HWM_Assigned_By',
                            'value' => 'Homeworkmarking::getAssignee($data->TM_HWM_Assigned_By)',
                        ),                        
                       
                        /*array(
                            'name'=>'Assigned Groups',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetAssignees($data->TM_SCH_Id)',
                        ), 
                                                
                        array(
                            'type'=>'raw',
                            'name'=>'Publish',
                            'value' => 'CHtml::link("Publish","javascript:void(0)",array(
                            "title"=>"publish",
                            "class"=>"choosegroups btn btn-warning btn-xs",
                            "style"=>Schoolhomework::checkQuestions($data->TM_SCH_Id),
                            "data-backdrop"=>"static",
                            "data-toggle"=>"modal",
                            "data-target"=>"#SelectGroup",
                            "data-value"=>"$data->TM_SCH_Id"
                            ))',
                        ), */                                                 
                        array(
                            'class'=>'CButtonColumn',
                            'template'=>'{marking}',
                            'deleteButtonImageUrl'=>false,
                            'updateButtonImageUrl'=>false,
                            'viewButtonImageUrl'=>false,
                            'buttons'=>array
                            (
                                
                                'marking'=>array(
                                    'label'=>'Mark',
                                    'url'=>'Yii::app()->createUrl("teachers/markhomework",array("id"=>$data->TM_HWM_Homework_Id))',
                                    'options'=>array(
                                        'title'=>'Mark Practice',
                                        'class'=>'publishtest btn btn-warning btn-xs',
                                    )),
                            ),
                        ),
                    ),'itemsCssClass'=>"rwd-tables"
                )); ?>        
        </div>
    </div>    
</div>
<?php endif;?>
<script type="text/javascript">
        $(function(){
            $('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Markers(Yii::app()->session['school']);?>]
            });
            var names=$('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Markers(Yii::app()->session['school']);?>]
            });
            $('#editusersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            var namesassign=$('#editusersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            $('#publishto').change(function(){
                var std=$(this).val();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/Groups";?>',
                    data: {standard:std},
                    success: function(data){
                        //console.log(data)
                        $('#publishstdinfo').html('<input type="text" id="editusersuggest" name="editassigngroups[]">');
                        $('#editusersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                        namesassign=$('#editusersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                    }
                });

            });
            
            $('.choosegroups').click(function(){
                $('.nogroups').hide();
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).attr('href');                
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/getmarkers";?>',
                    data: {id:schoolid,homework:homeworkid},
                    success: function(data){
                        if(data.count=='0')
                        {
                            $('.nogroups').html('User/group not found.').show();
                        }
                        if(data.users!="")
                        {
                            $('.nogroups').html(data.users).show();
                        }
                    }
                });
            });
            
            $('#invitequickhw').click(function() {
                $('.inviteinfo').hide();
                var nameids=names.getValue();
                if((nameids.length)!=0){
                    //$('#buddyindication').text('')
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('teachers/assignmarking');?>',
                        beforeSend : function(){
                            $("#invitequickhw").prop("disabled", true);
                        },                        
                        data: {nameids: nameids,homeworkid:$('#homeworkid').val()}
                    })
                        .done(function (data) {
                            //$('#buddyindication').text(data);
                        });
                    $('.addgroupsinfo').text('User Assigned for marking.');
                    $('.addgroupsinfo').show();
                    //$('#inviteclose').trigger('click');
                    setTimeout(function() {
                        $('#SelectGroup').modal('toggle');
                        window.location.reload();
                    }, 5000);
                }
                else{

                    $('.inviteinfo').text('User/group not found.');
                    $('.inviteinfo').show();
                    //$('#buddyindication').text('No Buddies Selected');


                }

            });
            $('.edithome').click(function(){
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).attr('href');
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/EditHomework";?>',
                    data: {id:homeworkid},
                    success: function(data){
                        $('.homeworkedit').html(data.result);
                        namesassign.setValue(data.grpid);
                    }

                });
            });
            $('#changehw').click(function() {
                $('.inviteinfo').hide();
                var namesassignd=namesassign.getValue();
                if($('#duedate').val()=='' || $('#publishdate').val()==''){
                    $('.inviteinfo').text('Please enter all fields.');
                    $('.inviteinfo').show();
                    $("#invitequickhw").prop("disabled", false);
                    return false;
                }
                else{                
                    var from = $('#publishdate').val().split("-");
                    var publish = new Date(from[0], from[1] - 1, from[2]);
                    var to = $('#duedate').val().split("-");
                    var due = new Date(to[0], to[1] - 1, to[2]);
                    var dateObj = new Date();
                    var month = dateObj.getMonth(); //months from 1-12
                    var day = dateObj.getDate();
                    var year = dateObj.getFullYear();
                    var d = new Date(year,month,day);

                    if (publish > due) {
                        $('.dateinfo').show();
                        $('.dateinfo').show();
                        setTimeout(function () {
                            $('.dateinfo').hide();
                        }, 5000);
                        return false;
                    }
                    else if(publish < d ){
                        $('.datedelaypub').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else if( due < d ){
                        $('.datedelay').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->createUrl('teachers/changehomeworkdetails');?>',
                            data: {id:$('#homeworkid').val(),nameids: namesassignd,dudate:$('#duedate').val(),comments:$('#comments').val(),publishdate:$('#publishdate').val(),type:$('.type:checked').val(),solution:$('.showsolution:checked').val(),publishto:$('#publishto').val(),showmark:$('.showmarks:checked').val()}
                        })
                            .done(function (data) {
                                if(data=='yes')
                                {
                                    window.location.reload();
                                }                                
                                //$('#buddyindication').text(data);
                            });
                        $('.addgroupsinfo').text('Changed Details.');
                        $('.addgroupsinfo').show();

                        //$('#inviteclose').trigger('click');
                        setTimeout(function() {
                            $('#SelectGroupedit').modal('toggle');                            
                            $('.addgroupsinfo').hide();
                        }, 2000);}

                    }
                }


            );
        });
    </script>

<script>
    $(document).ready(function () {
        $('.movehistory').on('click',function(e, data){

            if(!data){
                handleDelete(e, 1,$(this));
            }else{
                window.location = $(this).attr('href');
            }
        });

        // $('.printworksh').click(function(){
        //     var worksheetid = $(this).attr('href');
        //     // $('#showworksheet').html('');
        //     window.open('teachers/printworksheet?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
           
        // });


    });

    $( 'body' ).on( 'click', '.printworksh', function () {
         var worksheetid = $(this).attr('href');
        // $('#showworksheet').html('');
        window.open('/teachers/printworksheet?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
    });

    $( 'body' ).on( 'click', '.printworksol', function () {
         var worksheetid = $(this).attr('href');
        // $('#showworksheet').html('');
        window.open('/teachers/Printsolution?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
    });

    function handleDelete(e, stop,item){
        if(stop){
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You want to move this to history?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText:"No",
                    <?php if(Yii::app()->session['userlevel']==1):?>
                        confirmButtonColor: "#4d0000",
                    <?php else:?>
                        confirmButtonColor: "#FDBF43",
                    <?php endif;?>
                    confirmButtonText: "Yes, Move!",
                    closeOnConfirm: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        item.trigger('click', {});
                    }
                });
        }
    };
</script>    