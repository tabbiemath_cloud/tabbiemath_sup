<?php
/* @var $this TeachersController */
/* @var $data Teachers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TH_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_TH_Id), array('view', 'id'=>$data->TM_TH_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TH_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_TH_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TH_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_TH_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TH_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_TH_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TH_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_TH_Status); ?>
	<br />


</div>