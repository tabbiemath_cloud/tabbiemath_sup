<?php
/* @var $this TeachersController */
/* @var $model Teachers */

$this->breadcrumbs=array(
	'Teachers'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Teachers', 'class'=>'nav-header'),
    array('label'=>'Add Teacher', 'url'=>array('create')),
);

?>
<h3>Manage Teachers</h3>
<div class="row brd1">
    <div class="col-lg-12">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'teachers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'TM_TH_Name',
/*		'TM_TH_CreatedOn',
		'TM_TH_CreatedBy',*/
        array(
            'name'=>'TM_TH_Status',
            'value'=>'Teachers::itemAlias("TeachersStatus",$data->TM_TH_Status)',
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'deleteButtonImageUrl'=>false,
            'updateButtonImageUrl'=>false,
            'viewButtonImageUrl'=>false,
            'buttons'=>array
            (
                'view'=>array(
                    'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                    'options'=>array(
                        'title'=>'View'
                    )),
                'update'=>array(
                    'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                    'options'=>array(
                        'title'=>'Update'
                    )),
                'delete'=>array(
                    'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                    'options'=>array(

                        'title'=>'Delete',
                    ),
                )
            ),
        ),
	),'itemsCssClass'=>"table table-bordered table-hover table-striped"
)); ?>
