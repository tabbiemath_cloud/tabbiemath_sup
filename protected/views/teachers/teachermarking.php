    <style>
        .MathJax span
        {
            font-size: 14px;
        }
        span.mi {
            font-style: normal !important;
            font-family: STIXGeneral normal !important;
        }
    </style>
    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sweetalert.css">
      <div class="row">
                <div class="col-md-12">
                    
                    <h1 class="page-header2 pull-left">Mark - <?php echo $homework->TM_SCH_Name;?></h1>
                    <a href="<?php echo Yii::app()->createUrl('teachers/completeallmarking',array('id'=>$homework->TM_SCH_Id));?>" class="btn btn-warning pull-right" id="completeallmarking" style="margin-top: 20px; margin-bottom: 10px;">COMPLETE ALL MARKING</a>                    
                    <?php if(Yii::app()->user->isTeacher()):?>
                    <a href="<?php echo Yii::app()->createUrl('teacherhome');?>" class="btn btn-warning pull-right" style="margin-top: 20px; margin-bottom: 10px;margin-right:10px;">Exit</a>
                    <?php elseif(Yii::app()->user->isMarker()):?>
                    <a href="<?php echo Yii::app()->createUrl('staffhome');?>" class="btn btn-warning pull-right" style="margin-top: 20px; margin-bottom: 10px;margin-right:10px;">Exit</a>
                    <?php endif;?>
                </div>              
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
					<div class="row">
					
					</div>
				<div class="panel with-nav-tabs panel-default">
             
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade" id="tab1default">Default 1</div>
                        <div class="tab-pane fade active in" id="tab2default">
							<div class="row" style="display:none">
								<div class="col-md-12">
								<button type="button" class="btn btn-warning btn-md pull-right">Completed All Marking</button>	
									<form class="centermy">
										<label class="radio-inline" style="margin-left: 10px;">
										  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked> All
										</label>
										<label class="radio-inline">
										  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> View Incorrect 
										</label>
										<label class="radio-inline">
										  <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> View to be Marked
										</label>
									</form>
																	
								</div>
							</div>
<?php if(count($startedstudents)>0):?>
    <input type="hidden" id="start" value="0">
    <input type="hidden" id="numrec" value="5">
    <input type="hidden" id="total" value="<?php echo $total;?>">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4>Attempted</h4>
                                    </div>
                                    <div class="col-md-8">
                                            <!-- position: fixed;
                                            left: auto;
                                            z-index: 999;
                                            margin: auto;
                                            display: block; -->
                                        <div class="alert alert-warning fade in" style="display: none; position: fixed;left: auto; z-index: 999;margin: auto;" id="markinfo">
                                        </div>
                                        <input type="hidden" id="completeall" value="0">
                                    </div>
                                </div>
                            <div style="max-height: 500px;overflow: auto;" id="scrolldiv">
							<ul class="list-group" id="markinglistul">	
<?php 
        $script='';
        $count=1;
        foreach($startedstudents AS $started):
            if($started->TM_STHW_Status=='4'):
            $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$started->TM_STHW_Student_Id))            
            ?>							 
					<li class="list-group-item">
                        <div class="row">
                            <div class="col-md-2 col-xs-6">
                            	<div class="doc" id="doc<?php echo $started->TM_STHW_Student_Id;?>">
                            		<div class="active item">
                            			<div class="carousel-info">
                            				<div class="pull-left">
                            					<span class="doc-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>
                            					<div class="namebottom">
                            						<ul>
<!--                             							<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Solution">
                            								<a title="" class="drop viewsolution" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-lightbulb-o fa-2x"></i>
                            								</a>
                            							</li> -->

                                                        <li data-toggle="tooltip" data-placement="bottom" data-original-title="Reopen">
                                                            <a title="" class="reopen" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                                                                <i style="font-size: 18px;" class="glyphicon glyphicon-remove"></i>
                                                            </a>
                                                        </li>

                            							<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Answer">
                            								<a title="" class="drop2 viewanswer" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-eye fa-2x"></i>
                            								</a>
                            							</li>
                                                                   
                                                        <!--code by amalll-->
														<?php 
														// $criteria=new CDbCriteria;
														// $criteria->condition='TM_HQ_Homework_Id='.$started->TM_STHW_HomeWork_Id.' AND TM_HQ_Type IN (6,7)';
														// $totalpaper=HomeworkQuestions::model()->count($criteria);
														if($totalpaper>0):														
														?>
                                                        <!--<li data-toggle="tooltip" data-placement="bottom" data-original-title="Download">
                                                            <?php /*$answersheet=Studenthomeworkanswers::model()->find(array('condition'=>"TM_STHWAR_HW_Id='".$started->TM_STHW_HomeWork_Id."' AND TM_STHWAR_Student_Id='".$started->TM_STHW_Student_Id."' "));*/?>
                                                            <a href="<?php /*echo Yii::app()->request->baseUrl."/homework/".$answersheet['TM_STHWAR_Filename'] */?>" title="" target="_blank" class="viewanswersheet" data-item="<?php /*echo $started->TM_STHW_HomeWork_Id;*/?>" data-student="<?php /*echo $started->TM_STHW_Student_Id;*/?>">
                                                                <i class="fa fa-download fa-2x"></i>
                                                            </a>
                                                        </li>-->
														<?php endif;?>
                                                        <!--ends-->
                                                        <li data-toggle="tooltip" data-placement="bottom" data-original-title="Comments">
                                                            <?php if($this->HasComments($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id)):?>
                                                                <a href="javascript:void(0)" class="comments" title="Upload Comments" data-toggle="modal" data-target="#myModal" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                                                                    <i class="fa fa-cloud-upload fa-2x"></i>
                                                                </a>
                                                            <?php else: ?>
                                                                <a href="javascript:void(0)" class="comments" title="Upload Comments" data-toggle="modal" data-target="#myModal" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                                                                    <i style="color: #777777" class="fa fa-cloud-upload fa-2x"></i>
                                                                </a>
                                                            <?php endif;?>
                                                        </li>
                                                        <li>
                                                        <?php if($this->HasWorksheeturl($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id)!='0'):
                                                        $homework_url=Studenthomeworkanswersurl::model()->find(array('condition'=>'TM_STHWARU_HW_Id='.$started->TM_STHW_HomeWork_Id.' AND TM_STHWARU_Student_Id='.$started->TM_STHW_Student_Id));
                                                        $homework_url->TM_STHWARU_Url;
                                                        $url=''; 
                                                        $search='https';
                                                        if(preg_match("/{$search}/i", $homework_url->TM_STHWARU_Url))
                                                        {
                                                           $url=$homework_url->TM_STHWARU_Url;
                                                        
                                                        }
                                                        else
                                                        {
                                                             $url='https://'.$homework_url->TM_STHWARU_Url;
                                                        }
                                                         ?>
                                                        <a href="<?php echo $url; ?>" target="_blank" class="homeworkurl" title="Open Link"  data-value="<?php echo $started->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                                            
                                                            
                                                         <?php endif; ?>
                                                        </li>
                            							<li>
                            								<a title="" class="saveline" data-toggle="tooltip" data-placement="bottom" data-original-title="Save that line" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-floppy-o fa-2x"></i>
                            								</a>
                            							</li>

                            							<li>
                                                            <a title="" class="markcomplete" data-toggle="tooltip" data-placement="bottom" data-original-title="Mark it complete" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-check fa-2x"></i>
                            								</a>               			
                            							</li>
                                                        <br>
                <?php if($this->CheckAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id)):
                $hwanswers=$this->GetAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id);
                foreach($hwanswers AS $hwanswer):
                $ext = pathinfo($hwanswer['TM_STHWAR_Filename'], PATHINFO_EXTENSION);
                ?>
                <li data-toggle="tooltip" data-placement="bottom" data-original-title="View File">
                                                                    <?php //$answersheet=Studenthomeworkanswers::model()->find(array('condition'=>"TM_STHWAR_HW_Id='".$started->TM_STHW_HomeWork_Id."' AND TM_STHWAR_Student_Id='".$started->TM_STHW_Student_Id."' "));?>
    <?php if($ext=='one'):?>
     <a href="<?php echo Yii::app()->request->baseUrl."/teachers/download?file=".urlencode($hwanswer['TM_STHWAR_Filename']) ?>" title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
        <?php else:?>
            <!-- href="<?php echo Yii::app()->request->baseUrl."/homework/".$hwanswer['TM_STHWAR_Filename'] ?>" -->
    <?php
    $allowed = array('gif', 'png', 'jpg');
    $ext = pathinfo($hwanswer['TM_STHWAR_Filename'], PATHINFO_EXTENSION);
    if (in_array($ext, $allowed)) { ?>
    
     <a href="<?php echo Yii::app()->request->baseUrl ?>/teachers/editscreen?name=<?php echo $hwanswer['TM_STHWAR_Filename'].'&assignment='.$_GET['id']; ?>" class="imagedit" data-value="<?php echo $hwanswer['TM_STHWAR_Filename']; ?>"  title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
     <?php } else { ?>
    <a href="<?php echo Yii::app()->request->baseUrl ?>/homework/<?php echo $hwanswer['TM_STHWAR_Filename']; ?>" class="imagedit" data-value="<?php echo $hwanswer['TM_STHWAR_Filename']; ?>"  title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">

    <?php } endif; ?>

        <?php 
        $commentexist=Imagetag::model()->findAll(array('condition' => "pic_id LIKE '".$hwanswer['TM_STHWAR_Filename']."' AND assignment_id='".$hwanswer->TM_STHWAR_HW_Id."'"));
        ?>
    <?php if($commentexist){ ?>  
    <i class="fa fa-download fa-2x"></i>
    <?php } else { ?>
    <i style="color: #7a7e81 !important; " class="fa fa-download fa-2x"></i>
    <?php } ?>

    </a>
    </li>
    <?php 
    endforeach;
    endif;?>
                            						</ul>
                            					</div>
                            				</div>
                            			</div>
                            		</div>
                            	</div>
                            </div>
                            <div class="col-md-1 col-xs-6">
                            </div>
                                <!--<div class="col-md-1 col-xs-6">
                                    <div class="btmy">                                                                        
                                    <div id="pbar" class="progress-pie-chart gt-50" data-percent="0">
                                    <div class="ppc-progress">
                                    <div class="ppc-progress-fill" style="transform: rotate(316.8deg);"></div>
                                    </div>
                                    <div class="ppc-percents">
                                    <div class="pcc-percents-wrapper">
                                    <span>88%</span>
                                    </div>
                                    </div>
                                    </div>                                    
                                    <progress style="display: none" id="progress_bar3" value="100" max="100"></progress>                                            							                                            								
                                    </div>
                                </div>-->                            
                            <div class="col-xs-12 col-md-9 col-lg-9">							
                            	<div class="table-responsive">
                                    <form id="markingform<?php echo $started->TM_STHW_Student_Id;?>" method="post">
                                        <input type="hidden" name="student" id="student<?php echo $started->TM_STHW_Student_Id;?>" value="<?php echo $started->TM_STHW_Student_Id;?>"/>                                        
                                        <input type="hidden" name="homework" id="homework<?php echo $started->TM_STHW_Student_Id;?>" value="<?php echo $started->TM_STHW_HomeWork_Id;?>"/>
                                		<table class="table tdnone">								
                                			<tbody>
                                				<tr>
                                                    <?php echo $this->GetQuestionColumns($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id); ?>									
                                				</tr>
                                			</tbody>
                                		</table>
                                    </form>
                            	</div>
                            </div>                                                     							                                  					
                        </div>
						<div class="row">
								<!-- <div class="my_wrapper drop_row solution<?php echo $started->TM_STHW_Student_Id;?>" style="height: 650px;">
								</div> -->
						</div>
						<div class="row">
								<div class="my_wrapper drop_row2 answer<?php echo $started->TM_STHW_Student_Id;?>">
	            
								</div>
						</div>
						
                    </li>
<?php
            elseif($started->TM_STHW_Status=='5'):            
                    $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$started->TM_STHW_Student_Id));  
?>
        <li class="list-group-item">
                                <div class="row">
        							<div class="col-md-2 col-xs-6">
        								<div class="doc">
        									<div class="active item">
        									  
        									  <div class="carousel-info">
        										<div class="pull-left">
        										  <span class="doc-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>
        											<div class="namebottom">
        												<ul>
        												<!-- 	<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Solution"><a title="" class="drop viewsolution" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>"><i class="fa fa-lightbulb-o fa-2x"></i></a></li> -->
                                                                                                                <li>
                                                        <?php if($this->HasWorksheeturl($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id)!='0'):
                                                        $homework_url=Studenthomeworkanswersurl::model()->find(array('condition'=>'TM_STHWARU_HW_Id='.$started->TM_STHW_HomeWork_Id.' AND TM_STHWARU_Student_Id='.$started->TM_STHW_Student_Id)); 

                                                         ?>
                                                            <a href="<?php if (strpos($homework_url->TM_STHWARU_Url, 'https://')==0)
                                                            {
                                                                echo 'https://'.$homework_url->TM_STHWARU_Url;
                                                            }
                                                            else{
                                                             echo $homework_url->TM_STHWARU_Url; } ?>" target="_blank" class="homeworkurl" title="Open Link"  data-value="<?php echo $started->TM_STHW_HomeWork_Id;?>"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a>
                                                         <?php endif; ?>
                                                        </li>
                                                        <li data-toggle="tooltip" data-placement="bottom" data-original-title="Reopen">
                                                            <a title="" class="reopen" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                                                                <i style="font-size: 18px;" class="glyphicon glyphicon-remove"></i>
                                                            </a>
                                                        </li>
        													<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Answer"><a title="" class="drop2 viewanswer" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>"><i class="fa fa-eye fa-2x"></i></a></li>
                                                            
                                                            <li>
                                                            <a title="" class="unmarkcomplete" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit Marking" data-item="<?php echo $started->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $started->TM_STHW_Student_Id;?>">
                            									<i class="fa fa-check fa-2x"></i>
                            								</a>                                 
                            							</li>        			<br>
                <?php if($this->CheckAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id)):
                $hwanswers=$this->GetAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id);
                foreach($hwanswers AS $hwanswer):
                $ext = pathinfo($hwanswer['TM_STHWAR_Filename'], PATHINFO_EXTENSION);
                ?>
                <li data-toggle="tooltip" data-placement="bottom" data-original-title="View File">
                                                                    <?php //$answersheet=Studenthomeworkanswers::model()->find(array('condition'=>"TM_STHWAR_HW_Id='".$started->TM_STHW_HomeWork_Id."' AND TM_STHWAR_Student_Id='".$started->TM_STHW_Student_Id."' "));?>
    <?php if($ext=='one'):?>
     <a href="<?php echo Yii::app()->request->baseUrl."/teachers/download?file=".urlencode($hwanswer['TM_STHWAR_Filename']) ?>" title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
        <?php else:?>
            <!-- href="<?php echo Yii::app()->request->baseUrl."/homework/".$hwanswer['TM_STHWAR_Filename'] ?>" -->

    <a href="<?php echo Yii::app()->request->baseUrl ?>/teachers/editscreen?name=<?php echo $hwanswer['TM_STHWAR_Filename'].'&assignment='.$_GET['id']; ?>" class="imagedit" data-value="<?php echo $hwanswer['TM_STHWAR_Filename']; ?>"  title="" target="_blank" class="viewanswersheet" data-item="<?php echo $hwanswer->TM_STHWAR_HW_Id;?>" data-student="<?php echo $hwanswer->TM_STHWAR_Student_Id;?>">
    <?php endif;?>
        <?php 
        $commentexist=Imagetag::model()->findAll(array('condition' => "pic_id LIKE '".$hwanswer['TM_STHWAR_Filename']."' AND assignment_id='".$hwanswer->TM_STHWAR_HW_Id."'"));
        ?>
    <?php if($commentexist){ ?>  
    <i class="fa fa-download fa-2x"></i>
    <?php } else { ?>
    <i style="color: #7a7e81 !important; " class="fa fa-download fa-2x"></i>
    <?php } ?>

    </a>
    </li>
    <?php 
    endforeach;
    endif;?>										        													
        												</ul>
        											</div>
        										</div>
        									  </div>
        									</div>
        								</div>
        							</div>
                                   
        						
        						
                               <div class="col-md-1 col-xs-6">                                                              
                                    <!--<div class="btmy">                                                                        
                                    <div id="pbar" class="progress-pie-chart" data-percent="0">
                                    <div class="ppc-progress">
                                    <div class="ppc-progress-fill" ></div>
                                    </div>
                                    <div class="ppc-percents">
                                    <div class="pcc-percents-wrapper">
                                    <span>%</span>
                                    </div>
                                    </div>
                                    </div>                                    
                                    <progress style="display: none" id="progress_bar" class="pieprogress"  value="0" max="<?php echo $this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id);?>"></progress>                                            							                                            								
                                    </div>-->
                                    <div id="<?php echo 'prog'.$count;?>" class="containerdiv">
                                    	<div class="jCProgress" style="opacity: 1;">
                                    		<div class="percent" style="display: none;"><?php echo $this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id);?></div>
                                    		<canvas width="55" height="55"></canvas>
                                    	</div>
                                    </div>
                                   <?php if(Yii::app()->session['userlevel']==1):?>
                                    <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).",loop : false,showPercent : true});"; ?>
                                    <?php else:?>
                                    <?php $script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).",loop : false,showPercent : true});"; ?>
                                    <?php endif;?>
                                                                        
                                </div>  
                                     						        						        						
        						<div class="col-xs-12 col-md-9 col-lg-9">							
        							<div class="table-responsive">
        								<table class="table tdnone">								
        									<tbody>
        										<tr>
                                                    <?php echo $this->GetQuestionColumnsCompleted($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id); ?>
        											
        										</tr>
        									</tbody>
        								</table>
        							</div>        												        						        							          							          							          							    
        						</div>
                                </div>
        						
							<div class="row">
									<!-- <div class="my_wrapper drop_row">
										<ul class="lst_stl_my solution<?php echo $started->TM_STHW_Student_Id;?>" style="height: 650px;">

										</ul>               
									</div> -->
							</div>
							<div class="row">
									<div class="my_wrapper drop_row2">
										<ul class="lst_stl_my answer<?php echo $started->TM_STHW_Student_Id;?>">

											</ul>	            
									</div>
							</div>
                            </li>
<?php                    
            endif;
            $count++;
        endforeach;        
?>

                  
                </ul>
                <div id="preloader">
                Loading 
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/imageloader.gif" alt=""></div>
                </div>
<?php endif;?>				
				
					<?php if(count($notstartedstudents)>0):?>
                       <h4 class="notatt">NOT ATTEMPTED <span class="badge bdg1 badge-warning"><?php echo count($notstartedstudents);?></span></h4>
                       <a style="display: none;" href="<?php echo Yii::app()->createUrl('teachers/submitall',array('id'=>$homework->TM_SCH_Id));?>" class="btn btn-warning pull-right" >Submit All</a>
                        <button type="button" id="sendreminderall" data-item="<?php echo $homework->TM_SCH_Id;?>" class="btn btn-warning pull-right" style="margin-right: 10px;">Reminder All</button>
					   <div class="row">
                        <?php foreach($notstartedstudents AS $notstarted):
                                $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$notstarted->TM_STHW_Student_Id))
                        ?>
        					<div class="col-md-4 col-sm-6">
        						<div class="not_attmpt">
        							<div class="active item">							  
        							  <div class="carousel-info">        								
        								<div class="pull-left">
        								  <span class="not_attmpt-name"><?php echo $studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name;?></span>
        								  <button type="button" id="sendreminder<?php echo $notstarted->TM_STHW_Student_Id;?>" data-item="<?php echo $notstarted->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $notstarted->TM_STHW_Student_Id;?>" class="btn btn-default btn-xs sendreminder"><i class="fa fa-bell"></i> Send Reminder</button>                                          
                                          <button type="button" id="submit<?php echo $notstarted->TM_STHW_Student_Id;?>" data-item="<?php echo $notstarted->TM_STHW_HomeWork_Id;?>" data-student="<?php echo $notstarted->TM_STHW_Student_Id;?>" class="btn btn-default btn-xs submitstudent"><i class="fa fa-arrow-circle-up"></i>Enter Marks</button>
        								</div>
        							  </div>
        							</div>
        						</div>
        					</div>
                        <?php endforeach;?>                       
                       </div>                        
                    <?php endif;?>																										
						</div>
                       
                    </div>
                </div>
            </div>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <form action="<?php echo Yii::app()->request->baseUrl."/teachers/UploadComments";?>" onsubmit="return Checkfiles();" id="commentsform" enctype="multipart/form-data" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Upload Comments</h4>
                                    </div>
                                    <div class="modal-body">
                                        <label class="control-label">Select File</label>
                                        <input id="input-1" type="file" class="file" name="comments">
                                        <input type="hidden" name="homeworkid" id="homeworkid">
                                        <input type="hidden" name="studentid" id="studentid">
                                        <div id="commentslist"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="alert alert-success pull-left uploadinfo" style="display: none;"> </span>
                                        <button type="submit" class="btn btn-default" id="">Submit</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
<input type="hidden" value="" id="student_id">
<input type="hidden" value="" id="homewor_id">
<script>

    $(document).ready(function () {
        <?php echo $script;?>
        
         $('#completeallmarking').on('click',function(e, data){

            if(!data){
                handleDelete(e, 1);
            }else{
                completeprocess()
                //
            }
        });
    });
    function completeprocess()
    {
        $('#markinfo').show();
        $('#markinfo').html("Please wait.. Saving marks of the student.");
        var count=$('.saveline').length;
        var errorcount=0;
        $('.saveline').each(function(){
            $(this).trigger('click');
            var errorvalue=$('#completeall').val();
            if(errorvalue!=0)
            {
                errorcount++;
            }
            ///each input event one by one... will be blured
            count--;
        });
        if(count==0 && errorcount==0)
        {
            setTimeout(function(){
                $('#markinfo').html("");
                $('#markinfo').hide(function(){
                    setTimeout(function(){
                        window.location = $('#completeallmarking').attr('href');
                    }, 2000);
                });
            }, 5000);
        }
        else
        {
            $('#markinfo').show();
            $('#markinfo').html("Given marks cannot be less than 0 or greater than assigned marks.");
            setTimeout(function(){
                $('#markinfo').hide();
            }, 5000);
        }
    }
    function handleDelete(e, stop){
        if(stop){
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "Are you sure to mark all homework complete?",
                    type: "warning",
                    showCancelButton: true,
                    <?php if(Yii::app()->session['userlevel']==1):?>
                        confirmButtonColor: "#4d0000",
                    <?php else:?>
                        confirmButtonColor: "#FDBF43",
                    <?php endif;?>
                    confirmButtonText: "Yes, Mark complete!",
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#completeallmarking').trigger('click', {});
                    }
                });
        }
    };
        $(document).on('click','.reopen', function(e, data){
            $("#student_id").val($(this).attr('data-student'));
            $("#homewor_id").val($(this).attr('data-item'));
            if(!data){
                handleremove(e, 1);
           
                //
            }
        });

        function handleremove(e, stop){
        if(stop){
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "This will cancel submission and reopen the assignment for student. Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    <?php if(Yii::app()->session['userlevel']==1):?>
                        confirmButtonColor: "#4d0000",
                    <?php else:?>
                        confirmButtonColor: "#FDBF43",
                    <?php endif;?>
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        var studentid = $("#student_id").val();
                        var homeworid = $("#homewor_id").val();
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->createUrl('teachers/studentreopen');?>',
                            data: {homeworid:homeworid,studentid:studentid}
                        })
                            
                            .done  (function(data, textStatus, jqXHR){
                                location.reload();
                           /* $('#doc'+student).after('<span class="alert alert-success alert'+student+'"> Line Saved</span>');
                            setTimeout(function(){
                                $('.alert'+student).remove();
                            }, 2000);*/
                        })
                                  
                    }
                });
        }
    };
</script>
<script>
$(function(){
    $(document).on('click', '.viewsolution', function(){        
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        // $.ajax({
        //     type: 'POST',
        //     url: '<?php echo Yii::app()->createUrl('teachers/solution');?>',
        //     beforeSend: function() {
        //         $('.solution'+student).html('<img src="<?php echo Yii::app()->request->baseUrl."/images/imageloader.gif";?>" style="margin:auto; display: block;">');         
        //     },             
        //     data: {student:student,homework:homework}
        // })
        //     .done  (function(data, textStatus, jqXHR){
        //     /// $('.solution'+student).html(data);

        //     MathJax.Hub.Queue(["Typeset",MathJax.Hub, "page-wrapper"]);

        // })

        window.open('../solutionnew?student='+student+'&homework='+homework, 'myWin', 'width = 700px, height = 500px');

    });
    $(document).on('click', '.viewanswer', function(){        
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('teachers/answer');?>',
            data: {student:student,homework:homework}
        })
            .done  (function(data, textStatus, jqXHR){
            $('.answer'+student).html(data);
            MathJax.Hub.Queue(["Typeset",MathJax.Hub, "page-wrapper"]);

        })
    });    
    $(document).on('click', '.saveline', function(){        
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        var error=$('.error'+student).length;
        $('.setmark'+student).each(function(){
            var stud=$(this).attr('data-student');
            var questionid=$(this).attr('data-id');
            if($('#error'+stud+questionid).val()==0)
            {
                error--;
            }
            else
            {
                error++;
            }
        });
        $('#completeall').val(error);

        if(error==0)
        {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('teachers/savemarks');?>',
                data: $('#markingform'+student).serialize()
            })
                .done  (function(data, textStatus, jqXHR){
                $('#doc'+student).after('<span class="alert alert-success alert'+student+'"> Line Saved</span>');
                setTimeout(function(){
                    $('.alert'+student).remove();
                }, 2000);
            })
        }
        else
        {
            $('#markinfo').show();
            $('#markinfo').html("Given marks cannot be less than 0 or greater than assigned marks.");
            setTimeout(function(){
                $('#markinfo').hide();
            }, 5000);
        }

    });
    $(document).on('click', '.markcomplete', function(){    
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        var item=$(this);
        var error=$('.error'+student).length;
        $('.setmark'+student).each(function(){
            var stud=$(this).attr('data-student');
            var questionid=$(this).attr('data-id');
            if($('#error'+stud+questionid).val()==0)
            {
                error--;
            }
            else
            {
                error++;
            }
        });
        if(error==0)
        {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('teachers/markcomplete');?>',
                data: $('#markingform'+student).serialize()
            })
                .done  (function(data, textStatus, jqXHR){
                $('.setmark'+student).attr('readonly',true);
                $('#doc'+student).after('<span class="alert alert-success alert'+student+'"> Marked Complete</span>');
                setTimeout(function(){
                    $('.alert'+student).remove();
                    item.removeClass('markcomplete');
                    item.addClass('unmarkcomplete');
                }, 2000);
            });
        }
        else
        {
            $('#markinfo').show();
            $('#markinfo').html("Given marks cannot be less than 0 or greater than assigned marks.");
            setTimeout(function(){
                $('#markinfo').hide();
            }, 5000);
        }

    });
    $(document).on('click', '.unmarkcomplete', function(){
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');

        $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('teachers/editmarkcomplete');?>',
          data: {student:student,homework:homework}
        })
        .done  (function(data, textStatus, jqXHR){
            location.reload();
         });
    });
    
    $(document).on('click', '.sendreminder', function(){        
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        var elementitem=$(this);  
        $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('teachers/sendreminder');?>',
          data: {student:student,homework:homework}
        })
        .done  (function(data, textStatus, jqXHR){
            if(data=='yes'){
                elementitem.html('<i class="fa fa-bell"></i> Reminder Sent');
                setTimeout(function(){
                    elementitem.fadeOut()
                }, 2000);
            }
            else {
                elementitem.html('<i class="fa fa-exclamation-circle"></i> Invalid email');
                setTimeout(function(){
                    elementitem.fadeOut()
                }, 2000);
            }
        
         });        
    });
    $(document).on('click', '#sendreminderall', function(){
        var homework=$(this).attr('data-item');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('teachers/remindall');?>',
                dataType: 'json',
                data: {homework:homework}
            })
            .done  (function(data, textStatus, jqXHR){
                var students=data.students;
                var invalidstudents=data.invalidstudents;
                for (var i = 0; i < students.length; i++) {
                    $('#sendreminder'+students[i]).html('<i class="fa fa-bell"></i> Reminder Sent');
                }
                for (var j = 0; j < invalidstudents.length; j++) {
                    $('#sendreminder'+invalidstudents[j]).html('<i class="fa fa-exclamation-circle"></i> Invalid email');
                }
                ///$('.sendreminder').html('<i class="fa fa-bell"></i> Reminder Sent');
            });
    });
    $(document).on('click', '.submitstudent', function(){           
        var student=$(this).attr('data-student');
        var homework=$(this).attr('data-item');
        var elementitem=$(this);  
        $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('teachers/submitstudent');?>',
          data: {student:student,homework:homework}    
        })
        .done  (function(data, textStatus, jqXHR){
            //elementitem.html('<i class="fa fa-bell"></i> Submited');
            setTimeout(function(){
                        location.reload();   
            }, 1000);              
        
         });        
    });
    $(document).on( "change",".mark", function(){
        var questid=$(this).attr('data-id');
        var student=$(this).attr('data-student');
        var totalmark=$('#totmarks'+questid).val();
        var marks=$(this).val();
        if(parseFloat(marks) > totalmark || parseFloat(marks) < 0)
        {
            $('#markinfo').show();
            $('#markinfo').html("Given marks cannot be less than 0 or greater than assigned marks.");
            setTimeout(function(){
                $('#markinfo').hide();
            }, 5000);
            $('#error'+student+questid).val('1');
        }
        else
        {
            $('#error'+student+questid).val('0');
        }
    });
    $('.comments').click(function(){
        var hwid=$(this).data('item');
        var studentid=$(this).data('student');
        $('#homeworkid').val(hwid);
        $('#studentid').val(studentid);
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl."/teachers/GetComments";?>',
            data: {homework:hwid,student:studentid},
            success: function(data){
                if(data!='no')
                {
                    $('#commentslist').html(data);
                }
            }
        });
    });
    $(document).on("click", '.deletecomment', function(event) {
        var worksheet=$(this).data('value');
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl."/teachers/deleteComment";?>',
            data: {worksheet:worksheet},
            success: function(data){
                if(data!='no')
                {
                    $('#file'+worksheet).remove();
                }
            }
        })

    });
})
</script>
<script type="text/javascript">
    function Checkfiles()
    {
        var fup = document.getElementById('input-1');
        var fileName = fup.value;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

        if(ext =="PDF" || ext=="pdf" || ext=="jpg" || ext=="gif" || ext=="png" || ext=="one")
        {
            return true;
        }
        else
        {
            $('.uploadinfo').html('Upload PDF,JPEG,GIF,PNG,Onenote Files only');
            $('.uploadinfo').show();
            setTimeout(function(){
                $('.uploadinfo').hide();
            },3000);

            return false;
        }
    }
</script>
				
				
				
				
				
                   <!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Action</a>
                                        </li>
                                        <li><a href="#">Another action</a>
                                        </li>
                                        <li><a href="#">Something else here</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>-->	
                        
<script>
var request_pending = false;
function onScroll(event) {
    if (request_pending) {
        return;
    }      
    // Check if we're within 100 pixels of the bottom edge of the broser window.
    var winHeight = $( "#scrolldiv").height()*1; // iphone fi  
    var scroltp=$( "#scrolldiv").scrollTop()*1; 
    var innerheight= $( "#scrolldiv").innerHeight()*1;
    var scrolheight= $( "#scrolldiv")[0].scrollHeight*1;    
    var closeToBottom = (scroltp + innerheight >= scrolheight);
          
    if (closeToBottom) {
        // Get the first then items from the grid, clone them, and add them to the bottom of the grid.        
                var start=($('#start').val()*1)+5;
                var limit=($('#numrec').val()*1);
                var total=($('#total').val()*1);
                //var current=start*limit;
       // $('#tiles').append(firstTen.clone());
        if(start<=total){
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->request->baseUrl; ?>/teachers/markinglist",
                dataType: "json",
                beforeSend: function(xhr){
                    $('#preloader').fadeIn();  /// #info must be defined somehwere
                    request_pending = true;
                },
                data: { pagecount: start,limit:limit,homework:'<?php echo $id;?>' }
            }).done(function( data ) {
                        $('#markinglistul').append(data.html);
                        $('#preloader').fadeOut();
                        $('#start').val(start);
                        eval(data.percentage);
                        request_pending = false;
                    });
        }        
    }
};
$(function(){
$( "#scrolldiv").unbind('scroll').bind('scroll', onScroll);    
})

   $(document).on('click', '.imagedit', function() {
    var imgname=$(this).data('value');
    $.ajax({
            type: "POST", 
            url: "<?php echo Yii::app()->request->baseUrl; ?>/teachers/editscreen", 
            data: {imgname:imgname},
            success: function(data) {

            }
          });
});
</script>			                