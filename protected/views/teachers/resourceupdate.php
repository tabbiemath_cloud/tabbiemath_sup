
<div class="row">
    <div class="col-lg-12">
        <h3>Update Resource</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
/* @var $this MockController */
/* @var $model Mock */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('teachers/home')."';
    });
");
?>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'homework-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation'=>false,
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            )); ?>
            <div class="panel-body form-horizontal payment-form">
                <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
                <p class="note">Fields with <span class="required">*</span> are required.</p>
                <?php echo $form->errorSummary($model); ?>
                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Name',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->textField($model,'TM_MK_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                        <?php echo $form->error($model,'TM_MK_Name'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Description',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->textArea($model,'TM_MK_Description',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
                        <?php echo $form->error($model,'TM_MK_Description'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Tags',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->textArea($model,'TM_MK_Tags',array('class'=>'form-control','placeholder'=>'Enter search tags separated by comma')); ?>
                        <?php echo $form->error($model,'TM_MK_Tags'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Status',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php
                        if($model->isNewRecord):
                            echo $form->dropDownList($model,'TM_MK_Status',Mock::itemAlias('MockStatus'),array('class'=>'form-control','options' => array('1'=>array('selected'=>true))));
                        else:
                            echo $form->dropDownList($model,'TM_MK_Status',Mock::itemAlias('MockStatus'),array('class'=>'form-control'));
                        endif;
                        ?>

                        <?php echo $form->error($model,'TM_MK_Status'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Link_Resource',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php
                        if($model->isNewRecord):
                            echo $form->dropDownList($model,'TM_MK_Link_Resource',Mock::itemAlias('Linkresource'),array('class'=>'form-control','options' => array('1'=>array('selected'=>true))));
                        else:
                            echo $form->dropDownList($model,'TM_MK_Link_Resource',Mock::itemAlias('Linkresource'),array('class'=>'form-control'));
                        endif;
                        ?>

                        <?php echo $form->error($model,'TM_MK_Link_Resource'); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Privacy',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php
                        if($model->isNewRecord):
                            echo $form->dropDownList($model,'TM_MK_Privacy',Mock::itemAlias('MockPrivacy'),array('class'=>'form-control','options' => array('1'=>array('selected'=>true))));
                        else:
                            echo $form->dropDownList($model,'TM_MK_Privacy',Mock::itemAlias('MockPrivacy'),array('class'=>'form-control'));
                        endif;
                        ?>

                        <?php echo $form->error($model,'TM_MK_Privacy'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Video_Url',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->textField($model,'TM_MK_Video_Url',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                        <?php echo $form->error($model,'TM_MK_Video_Url'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_MK_Thumbnail',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php
                        echo $form->fileField($model, 'TM_MK_Thumbnail',array('class'=>'form-control'));
                        ?>

                        <?php echo $form->error($model,'TM_MK_Thumbnail'); ?>
                    </div>
                </div>
                <div class="form-group" id="thumbnaildiv">
                    <label class="col-sm-3 control-label">Uploaded Thumbnail File</label>
                    <div class="col-sm-9">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <?php
                                if($model->TM_MK_Thumbnail==''):
                                    echo "No file uploaded";
                                else:
                                    echo $model->TM_MK_Thumbnail;
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;<a id=\"deletethumbnail\" data-id=\"".$model->TM_MK_Id."\" href=\"javascript:void(0);\" style=\"float:right;\" title=\"Delete File\"><span class=\"glyphicon  glyphicon-trash\"></span></a>";
                                endif;
                                ?></li>
                        </ul>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 pull-right">
                        <?php echo $form->hiddenField($model,'TM_MK_Standard_Id',array('value'=>Yii::app()->session['standard']));?>
                        <?php echo $form->hiddenField($model,'schools',array('value'=>User::model()->findByPk(Yii::app()->user->id)->school_id));?>
                        <?php echo $form->hiddenField($model,'TM_MK_Type',array('value'=>1));?>
                        <?php echo $form->hiddenField($model,'TM_MK_Publisher_Id',array('value'=>1));?>
                        <?php echo $form->hiddenField($model,'TM_MK_Syllabus_Id',array('value'=>1));?>
                        <?php echo $form->hiddenField($model,'TM_MK_Availability',array('value'=>1));?>

                        <?php echo $form->hiddenField($model,'TM_MK_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Continue' : 'Save',array('class'=>'btn btn-warning btn-block')); ?>
                    </div>
                    <div class="col-lg-2 pull-right">
                        <button class="btn btn-warning btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                    </div>
                </div>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>

</div><!-- form -->
<script type="text/javascript">
    $(function(){
        $('#deletethumbnail').click(function(){
            if(confirm('Are you sure you want to delete this thumbnail?'))
            {
                var mock=$('#deletethumbnail').attr('data-id');
                $.ajax({
                    method:'POST',
                    url:'<?php echo Yii::app()->request->baseUrl;?>/mock/deletefile',
                    data: {mock:mock,type:'thumbnail'},
                    success: function(data){
                        if(data=='yes')
                        {
                            $('#thumbnaildiv').remove();
                        }
                    }
                });
            }
        });

    });
</script>
