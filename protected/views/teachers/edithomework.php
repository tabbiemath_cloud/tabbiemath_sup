<?php
/* @var $this MockController */
/* @var $model Mock */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('teachers/home')."';
    });
");
?>

<form method="post" id="edithomeworkform">

        <div class="modal-content">


            <div class="modal-body">
                <div class="panel-body form-horizontal payment-form">

                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Publish Date <span
                                    class="glyphicon glyphicon-calendar"></span></label>

                            <div class="col-sm-8">
                                <input type="date" id="publishdate" class="form-control" name="publishdate"
                                       value="<?php $datetime = new DateTime($homework->TM_SCH_DueDate);
                                       echo $datetime->format('Y-m-d'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Due On <span
                                    class="glyphicon glyphicon-calendar"></span></label>

                            <div class="col-sm-8">
                                <input type="date" id="duedate" class="form-control" name="duedate"
                                       value="<?php $datetime = new DateTime($homework->TM_SCH_PublishDate);
                                       echo $datetime->format('Y-m-d'); ?>">
                            </div>
                        </div>
                    </div>
                    <!--code by amal-->

                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">To be submitted</label>

                            <div class="col-sm-8">
                                <!--<select class="form-control" id="type" name="type">

                                </select>-->
                                <?php if($homework->TM_SCH_PublishType==2):?>
                                <input type="radio" class="showsolution" name="type"
                                       value="1"/><span>Worksheet Only</span>
                                <input type="radio" class="showsolution" name="type" value="2"
                                       checked="checked"/><span>Online &amp; Worksheet</span>
                                <?php elseif($homework->TM_SCH_PublishType==2):?>
                                    <input type="radio" class="showsolution" name="type"
                                           checked="checked" value="1"/><span>Worksheet Only</span>
                                    <input type="radio" class="showsolution" name="type" value="2"
                                           /><span>Online &amp; Worksheet</span>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Show Solution</label>

                            <div class="col-sm-8">
                                <?php if($homework->TM_SCH_ShowSolution==1):?>
                                <input type="radio" class="showsolution" name="solution" value="1"
                                       checked="checked"/><span>Yes</span>
                                <input type="radio" class="showsolution" name="solution" value="0"/><span>No</span>
                                <?php elseif($homework->TM_SCH_ShowSolution==0):?>
                                    <input type="radio" class="showsolution" name="solution" value="1"
                                           checked="checked"/><span>Yes</span>
                                    <input type="radio" class="showsolution" name="solution" value="0"/><span>No</span>

                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Comments</label>

                            <div class="col-sm-8">
                                <textarea name="comments" class="form-control" id="comments"><?php echo $homework->TM_SCH_Comments?></textarea>
                            </div>
                        </div>
                    </div>

                    <!--ends-->

                </div>
            </div>
            <div class="modal-footer">
                <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                <span class="alert alert-success pull-left dateinfo" style="display: none;">Due date cannot be earlier than Publish date</span>
                <span class="alert alert-success pull-left datedelay" style="display: none;">Due date cannot be earlier than Today</span>
                <span class="alert alert-success pull-left datedelaypub" style="display: none;">Publish date cannot be earlier than Today</span>
                <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                <button type="submit" class="btn btn-default" id="invitequickhw" name="invitequickhw">Save</button>
                <button type="button" class="btn btn-default cancelbtn" id="cancel">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
</form>
<script type="text/javascript">

        $('#invitequickhw').click(function () {
            $('.inviteinfo').hide();

            //var nameids = names.getValue();

            if ( $('#homeworkid').val() == '' || $('#duedate').val() == '' || $('#comments').val() == '' || $('#publishdate').val() == '' || $('#type').val() == '') {
                $('.inviteinfo').text('Please enter all fields.');
                $('.inviteinfo').show();


                return false;
            }
            else {
                //Date validation begins
                var from = $('#publishdate').val().split("-");
                var publish = new Date(from[2], from[1] - 1, from[0]);
                var to = $('#duedate').val().split("-");
                var due = new Date(to[2], to[1] - 1, to[0]);
                var d = new Date();
                if (publish > due) {
                    $('.dateinfo').show();
                    $('.dateinfo').show();
                    setTimeout(function () {
                        $('.dateinfo').hide();
                    }, 5000);
                    return false;
                }
                else if(publish < d ){
                    $('.datedelaypub').show();
                    setTimeout(function () {
                        $('.datedelaypub').hide();
                    }, 5000);
                    return false;
                }
                else if( due < d ){

                    $('.datedelay').show();
                    setTimeout(function () {
                        $('.datedelaypub').hide();
                    }, 5000);
                    return false;
                }
                else {

                    return true;
                }
                //date validation ends

                //$('#buddyindication').text('No Buddies Selected');
            }


        });
    });
</script>