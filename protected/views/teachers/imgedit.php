
<html> 
  <head> 
    <style type="text/css">
  .marker-text {
    position: absolute;
    top: 21px;
    width: 100%;
    margin: 0 0 0 0;
    z-index: 1;
    font-size: 12px;
    font-weight: bold;
    text-align: center;
    color: #fff;
}
.navbar-inverse{
  display: none;
}
    </style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	<title>Tabbiemath Image Comment</title>

   <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/tagging.css">
    <script>
        CKEDITOR.env.isCompatible = true;
    </script>
<script type="text/javascript">
  $(document).ready(function(){
    var counter = 0;
    var mouseX = 0;
    var mouseY = 0;
    
    $("#imgtag img").click(function(e) { // make sure the image is click
      //var imgtag = $(this).parent(); // get the div to append the tagging list
      var offset = $(this).offset();
      mouseX = ( e.pageX - offset.left ); // x and y axis
      mouseY = ( e.pageY - offset.top );
      $("#imagexval").val(mouseX);
      $("#imageyval").val(mouseY)
      $( '#tagit' ).remove( ); // remove any tagit div first
      $( imgtag ).append( '<div id="tagit"></div>' );
      $( '#tagit' ).css({ top:mouseY, left:mouseX });
      $('#msg_modal').modal('show');
      $('#tagname').focus();
    });

	// Save button click - save tags
/*    $( document ).on( 'click',  '#save', function(){
   // var comment = $('#imgcommenting').val();
    var comment = $( '#imgcommenting' ).val();
		var img = $('#imgtag').find( 'img' );
		var id = $( img ).attr( 'id' );
    var assignment = "<?php echo $_GET['assignment']; ?>";
    var imgname = "<?php echo $_GET['name']; ?>";
      $.ajax({
        type: "POST", 
        url: "savetag", 
        data: "pic_id=" + id + "&assignment=" + assignment + "&imgname=" + imgname + "&comment=" + comment + "&pic_x=" + mouseX + "&pic_y=" + mouseY + "&type=insert",
        cache: true, 
        success: function(data){
          viewtag( id );
          $('#tagit').fadeOut();
          //$('#my_image').attr('src','minion.jpg');
        }
      });
      
    });*/
    
	// Cancel the tag box.
    $( document ).on( 'click', '#tagit #btncancel', function() {
      $('#tagit').fadeOut();
    });
    
	// mouseover the taglist 
	$('#taglist').on( 'mouseover', 'li', function( ) {
      id = $(this).attr("id");
      $('#view_' + id).css({ opacity: 1.0 });
    }).on( 'mouseout', 'li', function( ) {
        $('#view_' + id).css({ opacity: 0.0 });
    });
	

    $( '#taglist' ).on('click', '.remove', function() {
      id = $(this).parent().attr("id");
      // Remove the tag
	  $.ajax({
        type: "POST", 
        url: "savetag.php", 
        data: "tag_id=" + id + "&type=remove",
        success: function(data) {
			var img = $('#imgtag').find( 'img' );
			var id = $( img ).attr( 'id' );
			//get tags if present
			viewtag( id );
        }
      });
    });
	
	// load the tags for the image when page loads.
    var img = $('#imgtag').find( 'img' );
	var id = $( img ).attr( 'id' );
	
	viewtag( id ); // view all tags available on page load
    
    function viewtag( pic_id )
    {
      // get the tag list with action remove and tag boxes and place it on the image.
	  $.post( "taglist.php" ,  "pic_id=" + pic_id, function( data ) {
	  	$('#taglist ol').html(data.lists);
		 $('#tagbox').html(data.boxes);
	  }, "json");
	
    }
  });

    $(document).on('click','.tagview',function(){
    CKEDITOR.instances['commentedit'].setData('Loading data...');
    jQuery.event.props.push('dataTransfer');
    var id = $(this).data('id');
    $('#comment_id').val(id);
    $('#imgid').val(id);
        $.ajax({
        type: "POST", 
        url: "getdata", 
        data: {id:id},
        success: function(data) {
            $('.cke_editable').html('<p>my new content</p>');
            CKEDITOR.instances['commentedit'].setData(data);
           

        }
      });
     $('#server_msg_modal').modal('show');
  });


/*    $(document).on('click','#editsave',function(){
    var id = $('#comment_id').val();
    var commentt = $('#commentedit').val();

      $.ajax({
        type: "POST", 
        url: "edittag", 
        data: "id=" + id + "&commentt=" + commentt,
        cache: true, 
        success: function(data){
          
        $("#close").trigger("click");
        }
      });
  });*/


/*$(document).on('click','#Deletecomment', function(){
var val = confirm("Are your Sure.!");
if(val==true)
{
  $("#delete").val(1);
   $('#editform').submit();
}

});*/
</script> 
  </head> 
  <body>

  <div class="modal fade" id="msg_modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">  
        <h4 class="modal-title">Comment</h4>
        </div>
        <form method="post" id="createtag" >
        <div class="modal-body">
          <textarea rows="6" class="form-control" name="comment" id="imgcommenting">
          </textarea>
        </div>
         
            <input type="hidden" name="imgid" value="">
          

          <div class="modal-header">
              <button type="submit" id="save" class="btn btn-default">Save</button>
              
              <button type="button" id="closemodal"  data-dismiss="modal" class="btn btn-default">Cancel</button>
              <input type="hidden" value="" id="imgid" name="">
              <input type="hidden" id="imagexval" name="imagex" value="">
              <input type="hidden" id="imageyval" name="imagey" value="">
              <input type="hidden" name="create" value="1">
              </form>
          </div>
        </div>
    </div>
  </div>

<div class="modal fade" id="server_msg_modal">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Comment</h4>
        </div>
        <div class="modal-body">
            <form method="post" id="editform" >
            <textarea id="commentedit" name="commentedited" class="form-control">
             
            </textarea>
            <input type="hidden" value=""  id="comment_id" name="commentid">
            <div id="imgclass"></div>
            <div class="cl-md-6">
            </div>
        </div>
        <div class="modal-header">
          <button type="submit"  id="editsave" class="btn btn-default">Save</button>
        
         
            <button type="button" id="Deletecomment" class="btn btn-default">Delete</button>
            <button type="button" id="close"  data-dismiss="modal" class="btn btn-default">Cancel</button>
            <input type="hidden" value="" id="imgid" name="">
            <input type="hidden" name="update" value="1">
            <input type="hidden" id="delete" name="delete" value="">
            </form>
        </div>
    </div>
  </div>
</div>



  <div id="container">
  <div id="imgtag"> 
    <img width="845" id="my_image" src="<?php echo Yii::app()->request->baseUrl?>/homework/<?php echo $_GET['name'];  ?>" /> 
    <div id="tagbox">
     
     <?php $comments=Imagetag::model()->findAll(array('condition' => "assignment_id='".$_GET['assignment']."' AND pic_id LIKE'".$_GET['name']."'")); 
     if($comments){
      $comm=0;
      foreach ($comments as $key => $comment) {
        $comm++;
      ?>

      <div data-id="<?php echo $comment->id; ?>" class="tagview" style="opacity: 1.0; left:<?php echo $comment->pic_x; ?>px;top:<?php echo $comment->pic_y; ?>px; margin-top: -27px; margin-left: -17px;" id="view_<?php echo $comm;  ?>">
      <img style="z-index: 1;" class="editcomment" src="<?php echo Yii::app()->getBaseUrl() ?>/images/bubble-icon.png">
      <p class="marker-text"><?php echo $comm; ?></p>
    </div>

     <?php } } ?>
    </div>
  </div> 
  <div id="taglist"> 
    <ol> 
    </ol> 
  </div> 
  </div>
  </body>
</html>

<?php
    Yii::app()->clientScript->registerScript('editor', "
        $( '#imgcommenting' ).ckeditor();
         $( '#commentedit' ).ckeditor();
    ");
?>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/sweetalert.min.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sweetalert.css">
<script type="text/javascript">
  

  $('#Deletecomment').on('click',function(e, data){
            if(!data){
                handleremove(e, 1);
           
                //
            }
        });

        function handleremove(e, stop){
        if(stop){
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "This will delete the comment. Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    <?php if(Yii::app()->session['userlevel']==1):?>
                        confirmButtonColor: "#4d0000",
                    <?php else:?>
                        confirmButtonColor: "#FDBF43",
                    <?php endif;?>
                    confirmButtonText: "Yes",
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                       $("#delete").val(1);
                        $('#editform').submit();

                                  
                    }
                });
        }
    };
</script>