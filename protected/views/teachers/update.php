<?php
/* @var $this TeachersController */
/* @var $model Teachers */

$this->breadcrumbs=array(
	'Teachers'=>array('admin'),
	$model->TM_TH_Id=>array('view','id'=>$model->TM_TH_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Teachers', 'class'=>'nav-header'),
	array('label'=>'List Teachers', 'url'=>array('admin')),
	array('label'=>'Create Teacher', 'url'=>array('create')),
	array('label'=>'View Teacher', 'url'=>array('view', 'id'=>$model->TM_TH_Id)),
);
?>

<h3>Update  <?php echo $model->TM_TH_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>