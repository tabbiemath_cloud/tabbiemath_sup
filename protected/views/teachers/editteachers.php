<?php
$this->breadcrumbs=array(
    'Schools'=>array('admin'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Admin', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$school), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'List Teachers', 'url'=>array('manageTeachers', 'id'=>$school)),
    array('label'=>'Add Teacher', 'url'=>array('AddTeacher', 'id'=>$school)),

    array('label'=>'View Teacher', 'url'=>array('ViewTeachers','id'=>$school,'master'=>$model->id)),
    array('label'=>'Change Password', 'url'=>array('TeacherPassword','id'=>$school,'master'=>$model->id)),
    array('label'=>'Add Standards', 'url'=>array('AddStandards', 'id'=>$school,'master'=>$model->id)),
    array('label'=>'Delete Teacher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('DeleteTeachers','id'=>$school,'master'=>$model->id),'confirm'=>'Are you sure you want to delete this Teacher?')),
);

?>

<h3>Update <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>


<?php 	echo $this->renderPartial('_staffform', array('model'=>$model,'profile'=>$profile,'staffschool'=>$schoolstaff,'id'=>$school)); ?>