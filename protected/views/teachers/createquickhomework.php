
<div class="row">
    <div class="col-lg-12">
        <h3>Create Practice</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
/* @var $this MockController */
/* @var $model Mock */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('teachers/home')."';
    });
");
?>


<div class="row">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'homework-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
	    <p class="note">Fields with <span class="required">*</span> are required.</p>
        <?php echo $form->errorSummary($model); ?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCH_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCH_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_SCH_Name'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCH_Description',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textArea($model,'TM_SCH_Description',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_SCH_Description'); ?>
                <?php 
                echo $form->hiddenField($model,'TM_SCH_CreatedBy',array('value'=>Yii::app()->user->id));
                echo $form->hiddenField($model,'TM_SCH_Status',array('value'=>'0'));
                
                 ?>                
            </div>
        </div>                
        <div class="form-group">
            <div class="row">
            <div class="col-sm-3 pull-right">
                <?php echo $form->hiddenField($model,'TM_SCH_Standard_Id',array('value'=>Yii::app()->session['standard']));?>
                <?php echo $form->hiddenField($model,'TM_SCH_School_Id',array('value'=>User::model()->findByPk(Yii::app()->user->id)->school_id));?>
                <?php echo $form->hiddenField($model,'TM_SCH_Type',array('value'=>'1'));?>                
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Continue' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                </div>

        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
