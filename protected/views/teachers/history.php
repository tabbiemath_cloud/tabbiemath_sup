<div class="row">
    <div class="col-lg-12">
        <h3>History</h3>
        <input type="hidden" id="schoolid" value="<?php echo Yii::app()->session['school'];?>">
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'homework-grid',
           
            'dataProvider'=>$model->search(Yii::app()->user->id,Yii::app()->session['standard'],'All','2',true,true),
            //'filter'=>$model,
                'pager'=>array(                    
                    'pageSize' => 1,                            
                ),             
            'columns'=>array( 
                array(
                    'name'=>'TM_SCH_Master_Stand_Id',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetMasterStd($data->TM_SCH_Master_Stand_Id,"Class")',
                ),
                array(
                    'name'=>'TM_SCH_Name',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData($data->TM_SCH_Name,"Name")',
                ),
                array(
                    'name'=>'TM_SCH_PublishDate',                        
                    'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_SCH_PublishDate))',
                    ),
                array(
                    'name'=>'TM_SCH_CompletedOn',                        
                    'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_SCH_CompletedOn))',
                ),
                /*array(
                    'name'=>'TM_SCH_Standard_Id',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData(Standard::model()->findByPk($data->TM_SCH_Standard_Id)->TM_SD_Name,"Standard")',
                ),*/
                array(
                            'name'=>'Practice Type',
                            'type'=>'raw',
                            'value'=>'Schoolhomework::GetData($data->TM_SCH_Type,"Homework Type")',
                        ), 
                /*array(
                    'name'=>'TM_SCH_Standard_Id',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData(Standard::model()->findByPk($data->TM_SCH_Standard_Id)->TM_SD_Name,"Standard")',
                ),
                array(
                    'name'=>'TM_SCH_CreatedOn',                        
                    'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_SCH_CreatedOn))',
                ),
                array(
                   'name'=>'Status',
                   'type'=>'raw',
                   'value'=>'Completed',
               ),*/
                array(
                    'type'=>'raw',
                    'name'=>'Re-publish',                    
                    'value' => 'CHtml::link("Re-publish","javascript:void(0)",array(
                    "title"=>"Re-publish",
                    "class"=>"choosegroups btn btn-warning btn-xs",                    
                    "data-backdrop"=>"static",
                    "data-toggle"=>"modal",
                    "data-target"=>"#SelectGroup",
                    "data-value"=>"$data->TM_SCH_Id"
                    ))',
                ), 

                /*array(
                    'type'=>'raw',
                    'name'=>'Question Status',                    
                    'value' => 'CHtml::link("<span class=\"fa fa-area-chart\"></span>",array("teachers/questionstatus","id"=>"$data->TM_SCH_Id"),array(
                    "title"=>"Question Status",
                    "class"=>"btn btn-warning",                                        
                    ))',
                ),
                array(
                    'type'=>'raw',
                    'name'=>'Assessment Status',                    
                    'value' => 'CHtml::link("Assessment Status",array("teachers/homeworkstatus","id"=>"$data->TM_SCH_Id"),array(
                    "title"=>"Assessment Status",
                    "class"=>"btn btn-warning btn-xs",                                        
                    ))',
                ),*/

                    array(
                        'class'=>'CButtonColumn',
                        'template'=>'{question}{assessment}{assessmentreport}',
                        'deleteButtonImageUrl'=>false,
                        'updateButtonImageUrl'=>false,
                        'viewButtonImageUrl'=>false,
                        'buttons'=>array
                        (
                            'question'=>array(
                                'label'=>'<span class="fa fa-area-chart"></span>',
                                'url' => 'Yii::app()->createUrl("teachers/questionstatus",array("id"=>$data->TM_SCH_Id))',
                                'options'=>array(
                                    'title'=>'Question Status'
                                )),
                            'assessment'=>array(
                                'label' => '<span class="fa fa-bar-chart"></span>',
                                'url' => 'Yii::app()->createUrl("teachers/homeworkstatus",array("id"=>$data->TM_SCH_Id))',
                                'options' => array(
                                    'title' => 'Practice Status',
                                )),
                            'assessmentreport'=>array(
                                'label' => '<span class="glyphicon glyphicon-print"></span>',
                                'url' => 'Yii::app()->createUrl("teachers/assessmentreportpdf",array("id"=>$data->TM_SCH_Id))',
                                'options' => array(
                                    'title' => 'Assessment Report',
                                    'target'=>'_blank',
                                )),
                        ),
                    ),
                array(
                    'type'=>'raw',
                    'name'=>'RE-OPEN',
                    'value' => 'CHtml::link("RE-OPEN",array("teachers/reopen","id"=>"$data->TM_SCH_Id"),array(
                    "title"=>"Re-open",
                    ))',

                ),

            ),'itemsCssClass'=>"rwd-tables"
        )); ?>
        </div>
    </div>
<div class="modal fade " id="SelectGroup" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Publish Practice</h4>
                </div>
                <div class="modal-body" >
                    <div class="panel-body form-horizontal payment-form">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-7">
                                    <input type="text" id="name" class="form-control" name="name">                            
                                </div>
                            </div>  
                        </div>                                                  
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish To</label>
                                <div class="col-sm-7">
                                    <input type="text" id="usersuggest" name="assigngroups[]">
                                    <input type="hidden" id="homeworkid">
                                </div>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish On <span class="glyphicon glyphicon-calendar"></span></label>
                                <div class="col-sm-7">
                                    <input type="date" id="publishdate" class="form-control" value="" name="publishdate">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Due On <span class="glyphicon glyphicon-calendar"></span></label>
                                <div class="col-sm-7">
                                    <input type="date" id="duedate" class="form-control" value="" name="duedate">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Submission Type</label>
                                <div class="col-sm-8" id="type">
                                    <!--<select class="form-control" id="type" name="type">

                                    </select>-->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Solution</label>
                                <div class="col-sm-7" id="solution">
                                    <!--<input type="radio" class="showsolution" name="solution" value="1" checked="checked" /><span>Yes</span>
                                    <input type="radio" class="showsolution" name="solution" value="0" /><span>No</span>-->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Comments</label>
                                <div class="col-sm-7">
                                    <textarea name="comments" class="form-control" id="comments"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-success pull-left dateinfo" style="display: none;">Due date cannot be earlier than Publish date</span>
                    <span class="alert alert-success pull-left datedelay" style="display: none;">Due date cannot be earlier than Today</span>
                    <span class="alert alert-success pull-left datedelaypub" style="display: none;">Publish date cannot be earlier than Today</span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <button type="button" class="btn btn-warning" id="invitequickhw">Re-Publish</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>     
</div>
<script type="text/javascript">
        $(function(){
            $('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            var names=$('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            $('.choosegroups').click(function(){
                $('.nogroups').hide();
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).data('value');
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/getGroups";?>',
                    data: {id:schoolid,homeworkid:homeworkid},
                    success: function(data){
                       if(data.count=='0')
                       {
                           $('.nogroups').html('You have no groups for the school').show();
                       }
                        $('#name').val(data.name);
                        $('#publishdate').val(data.ondate);
                        $('#duedate').val(data.duedate);
                        $('#comments').val(data.comments);
                       $('#type').html(data.result);
                       $('#solution').html(data.solution);
                    }
                });
            });
            $('#invitequickhw').click(function() {
                $('.inviteinfo').hide();
                var nameids=names.getValue();
                if($('#homeworkid').val()=='' || $('#name').val()=='' || $('#duedate').val()=='' || $('#publishdate').val()==''){
                    $('.inviteinfo').text('Please enter all fields.');
                    $('.inviteinfo').show();
                    $("#invitequickhw").prop("disabled", false);
                    return false;
                }
                else{
                                
                    var from = $('#publishdate').val().split("-");
                    var publish = new Date(from[0], from[1] - 1, from[2]);
                    var to = $('#duedate').val().split("-");
                    var due = new Date(to[0], to[1] - 1, to[2]);
                    var dateObj = new Date();
                    var month = dateObj.getMonth(); //months from 1-12
                    var day = dateObj.getDate();
                    var year = dateObj.getFullYear();
                    var d = new Date(year,month,day);

                    if (publish > due) {
                        $('.dateinfo').show();
                        $('.dateinfo').show();
                        setTimeout(function () {
                            $('.dateinfo').hide();
                        }, 5000);
                        return false;
                    }
                    else if(publish < d ){
                        $('.datedelaypub').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else if( due < d ){
                        $('.datedelay').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else
                    {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->createUrl('teachers/Republishhomework');?>',
                            beforeSend : function(){
                                $("#invitequickhw").prop("disabled", true);
                            },                            
                            data: {name:$('#name').val(),nameids: nameids,homeworkid:$('#homeworkid').val(),dudate:$('#duedate').val(),comments:$('#comments').val(),publishdate:$('#publishdate').val(),type:$("input[name=type]:checked").val(),solution:$("input[name=solution]:checked").val()}
                        })
                            .done(function (data) {
                                $('.addgroupsinfo').text('Practice assigned to groups.');
                                $('.addgroupsinfo').show();
                                //$('#inviteclose').trigger('click');
                                setTimeout(function() {
                                    $('#SelectGroup').modal('toggle');
                                    window.location.reload();
                                }, 2000);                                
                            });


                    }
                }

            });
        });
    </script>