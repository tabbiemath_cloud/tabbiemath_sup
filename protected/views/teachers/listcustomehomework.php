<style>
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    cursor: default;
    text-decoration: none;
    background-color: #FFA900;
    color: #FFF;
}
.nav-tabs>li>a {
    background-color: #fff;
    padding-right: 15px;
    padding-left: 15px;
    line-height: 1.42857143;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
}

@media screen and (max-width: 992px) {
    .nav-tabs li {
        max-width: 30%;
    }

    .grid-view .button-column
    {
        width: 100% !important;
        text-align: right;
    }
  
}

/*@media screen and (max-width: 500px) {
    .nav-tabs li {
        max-width: 120px;
    }

}*/
</style>

<div class="row">

    <div class="col-lg-12">
        <h3 class="h125 pull-left">Custom Practice<br>
            <p style="font-size: 14px;padding-top: 10px">Create revision worksheets by choosing questions by chapter,topic or difficulty level. Publish to students to be completed online or on paper.</p>
        </h3>
        <input type="hidden" id="schoolid" value="<?php echo Yii::app()->session['school'];?>">
        <!--<a href="<?php /*echo Yii::app()->createUrl('teachers/createcustomehomework')*/?>">
            <button class="btn pull-right btn-warning quickhwvalidation" type="button" style="margin-top: 18px; margin-bottom: 10px;">Create New</button>
        </a>-->
    </div>
    <div class="col-lg-12">
      <!--   <a href="<?php echo Yii::app()->createUrl('teachers/createcustomehomework')?>">
            <button class="btn pull-right btn-warning" type="button" style="margin-top: 18px; margin-bottom: 10px;margin-left:10px;">Create New</button>
        </a>
        <button class="btn pull-right btn-warning existinglist" type="button" style="margin-top: 18px; margin-bottom: 10px;margin-left:10px;">Use existing</button>
        <button class="btn pull-right btn-warning blueprintlist" type="button" style="margin-top: 18px; margin-bottom: 10px;margin-left:10px;">Use Blueprints</button> -->


        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#revisions" class="blueprintlist" aria-controls="revisions" role="tab" data-toggle="tab" aria-expanded="true">Create New (Using Blueprint)</a></li>
             <li role="presentation" class=""><a href="<?php echo Yii::app()->createUrl('teachers/createcustomehomework')?>" aria-controls="mocks" aria-expanded="false">Create New (without blueprint)</a></li>
            <li role="presentation" class=""><a href="#mocks" class="existinglist" aria-controls="mocks" role="tab" data-toggle="tab" aria-expanded="false">Existing Templates</a></li>
           
        </ul>

    </div>
</div>

<div class="row" id="default-list">
    <div class="col-lg-12">
    <div class="panel panel-default">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'homework-grid',
        'dataProvider'=>$model->search(Yii::app()->user->id,Yii::app()->session['standard'],'0'),
        //'filter'=>$model,
        'columns'=>array(
            array(
                'name'=>'TM_SCT_Name',
                'type'=>'raw',
                'value'=>'Schoolhomework::GetData($data->TM_SCT_Name,"Name")',
            ), 
            /*array(
                'name'=>'TM_SCT_Status',
                'type'=>'raw',
                'value'=>'Schoolhomework::GetData($data->itemAlias("HomeworkStatus",$data->TM_SCT_Status),"Status")',
            ),
            array(
                'name'=>'TM_SCT_Standard_Id',
                'type'=>'raw',
                'value'=>'Schoolhomework::GetData(Standard::model()->findByPk($data->TM_SCT_Standard_Id)->TM_SD_Name,"Standard")',
            ),*/           
            array(
                'name'=>'TM_SCT_CreatedBy',                        
                'value'=>'User::model()->findByPk($data->TM_SCT_CreatedBy)->profile->firstname',
            ),  
            array(
                'name'=>'TM_SCT_CreatedOn',                        
                'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_SCT_CreatedOn))',
            ),
             array(
                'name'=>'Type',
                'value'=>'Customtemplate::getBlueprintName($data->TM_SCT_Blueprint_Id,$data->TM_SCT_Type)',
            ),                                  
            array(
                'header'=>'Manage Questions',
                'type'=>'raw',
                'visible'=>'$data->TM_SCT_CreatedBy ==Yii::app()->user->id ',
                'value'=>'Schoolhomework::GetData(Schoolhomework::GetMangeLink($data->TM_SCT_Id),"Manage Questions")',
            ),   
            array(
                'type'=>'raw',
                'name'=>'Publish',
                'value' => 'CHtml::link("Publish","javascript:void(0)",array(
                "title"=>"publish",
                "class"=>"choosegroups btn btn-warning btn-xs",
                "style"=>Schoolhomework::checkQuestions($data->TM_SCT_Id),
                "data-backdrop"=>"static",
                "data-toggle"=>"modal",
                "data-target"=>"#SelectGroup",
                "data-value"=>"$data->TM_SCT_Id"
                ))',
            ),                                                  
            array(
                'class'=>'CButtonColumn',
                'template'=>'{update}{delete}',
                'deleteButtonImageUrl'=>false,
                'updateButtonImageUrl'=>false,
                'viewButtonImageUrl'=>false,
                'buttons'=>array
                (
                    
                    'publish'=>array(
                        'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                        'url'=>'Yii::app()->createUrl("teachers/PublishHomework",array("id"=>$data->TM_SCT_Id))',
                        'options'=>array(
                            'title'=>'publish',
                            'class'=>'publishtest',
                        )),
                    'update'=>array(
                        'label'=>'<span class="glyphicon glyphicon-pencil"></span>',                        
                        'url'=>'Yii::app()->createUrl("teachers/updateCustomeHomework",array("id"=>$data->TM_SCT_Id))',
                        'options'=>array(
                            'title'=>'Update'
                        ),
                        'visible'=>'$data->TM_SCT_CreatedBy ==Yii::app()->user->id ',
                        ),
                    'delete'=>array(
                        'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                        'url'=>'Yii::app()->createUrl("teachers/deleteHomework",array("id"=>$data->TM_SCT_Id))',
                        'options'=>array(

                            'title'=>'Delete',
                        ),
                        'visible'=>'$data->TM_SCT_CreatedBy ==Yii::app()->user->id ',
                                               
                    )
                ),
            ),
        ),'itemsCssClass'=>"rwd-tables"
    )); ?>
    </div>
    </div>
    <div class="modal fade " id="SelectGroup" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
               
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Publish Practice</h4>
                </div>
                <div class="modal-body" >
                <div class="panel-body form-horizontal payment-form">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Class</label>
                            <div class="col-sm-7">
                                <select class="form-control" name="standard" id="publishto">
                                    <option value="0">Select Class</option>
                                    <?php
                                    $teacherstandards=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));
                                    if(count($teacherstandards)>0):
                                        foreach($teacherstandards AS $teacherstandard):
                                            if($teacherstandard->standard->TM_SD_Id==Yii::app()->session['standard']):
                                                $selected='selected';
                                            else:
                                                $selected='';
                                            endif;
                                            echo "<option value='".$teacherstandard->standard->TM_SD_Id."' ".$selected.">".$teacherstandard->standard->TM_SD_Name."</option>";
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                                <input type="hidden" id="homeworkid">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publish To</label>
                            <div class="col-sm-7" id="publishstdinfo">
                                <input type="text" id="usersuggest" name="assigngroups[]">
                            </div>
                        </div>
                   </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publish On <span class="glyphicon glyphicon-calendar"></span></label>
                            <div class="col-sm-7">
                                <input type="date" id="publishdate" class="form-control" value="<?php echo date('Y-m-d');?>" name="publishdate">
                            </div>
                        </div>
                    </div>
                   <div class="row"> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Due On <span class="glyphicon glyphicon-calendar"></span></label>
                        <div class="col-sm-7">
                            <input type="date" id="duedate" class="form-control" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d') .' +1 day'));?>" name="duedate">
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Submission Type</label>
                            <div class="col-sm-8" id="type">
                                <!--<select class="form-control" id="type" name="type">

                                </select>-->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Show Solution</label>
                            <div class="col-sm-7">
                                <input type="radio" class="showsolution" name="solution" value="1" checked="checked" /><span>Yes</span>
                                <input type="radio" class="showsolution" name="solution" value="0" /><span>No</span>
                            </div>
                        </div>
                    </div>
                 <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Show Mark</label>
                            <div class="col-sm-7">
                                <input type="radio" class="showmarks" name="marks" value="0" checked="checked" /><span>Yes</span>
                                <input type="radio" class="showmarks" name="marks" value="1" /><span>No</span>
                            </div>
                        </div>
                    </div>
                   <div class="row">                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Comments</label>
                        <div class="col-sm-7">
                            <textarea name="comments" class="form-control" id="comments"></textarea>                                                        
                        </div>
                    </div>
                    </div>                   <!--code by amal-->


                        

                        <!--ends-->                                                                               
                  
                    </div> 
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-success pull-left dateinfo" style="display: none;">Due date cannot be earlier than Publish date</span>
                    <span class="alert alert-success pull-left datedelay" style="display: none;">Due date cannot be earlier than Today</span>
                    <span class="alert alert-success pull-left datedelaypub" style="display: none;">Publish date cannot be earlier than Today</span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <button type="button" class="btn btn-warning" id="invitequickhw">Publish</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>    
</div>
<div class="row" id="blueprintlist">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'blueprints-grid',
            'dataProvider'=>$blueprints->schoolsearch($id),
            //'filter'=>$blueprints,
            'columns'=>array(
                //'TM_BP_Id',
                'TM_BP_Name',
                'TM_BP_Description',
  
                 array(
                    'header'=>'Created By',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData(Blueprints::GetCreatedByForSchool($data->TM_BP_CreatedBy),"")',
                ),
               
                array(
                    'name'=>'TM_BP_CreatedOn',
                    'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_BP_CreatedOn))',
                ),
                // 'TM_BP_Syllabus_Id',
                /*array(
                    'name'=>'TM_BP_Standard_Id',
                    'value'=>'Standard::model()->findByPk($data->TM_BP_Standard_Id)->TM_SD_Name',
                    'filter'=>CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),

                ),*/
                //'TM_BP_Standard_Id',
                // 'TM_BP_Totalmarks',
                /*
                'TM_BP_Status',
                'TM_BP_CreatedOn',
                'TM_BP_CreatedBy',


                */
                array(
                    'header'=>'View',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData(Blueprints::GetMangeLink($data->TM_BP_Id),"View")',
                ), 
                array(
                        'class'=>'CButtonColumn',
                        'template'=>'{autogenerate}',
                        'deleteButtonImageUrl'=>false,
                        'updateButtonImageUrl'=>false,
                        'viewButtonImageUrl'=>false,
                        'buttons'=>array
                        (   
                                           
                            'autogenerate'=>array(
                                'label'=>'<span class="btn btn-warning btn-xs">Auto Generate</span>',
                                'url'=>'Yii::app()->createUrl("teachers/generateblueprint",array("id"=>$data->TM_BP_Id))',
                                'options'=>array(
                                    'title'=>'BP'
                                )),
                            // 'details'=>array(
                            //     'label'=>'<span class="btn btn-warning btn-xs">View</span>',
                            //     'url'=>'Yii::app()->createUrl("teachers/generateblueprint",array("id"=>$data->TM_BP_Id))',
                            //     'options'=>array(
                            //         'title'=>'BP'
                            //     ))
                        ), 
                    ),
            ),'itemsCssClass'=>"rwd-tables"
        )); ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function()
    {
        $('#default-list').hide();
        $('#blueprintlist').show();
    });
        $(function(){
            $('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            var names=$('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            $('#publishto').change(function(){
                var std=$(this).val();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/Groups";?>',
                    data: {standard:std},
                    success: function(data){
                        //console.log(data)
                        $('#publishstdinfo').html('<input type="text" id="usersuggest" name="assigngroups[]">');
                        $('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                        names=$('#usersuggest').magicSuggest({
                            maxSelection: 10,
                            allowFreeEntries: false,
                            data: data
                        });
                        //names.removeFromSelection(names.getSelection(), true);
                        //names.setValue(data);
                    }
                });
                //names.setValue([{"id":"782","name":"Standard X-A"}]);
            });
            $('.choosegroups').click(function(){
                $('.nogroups').hide();
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).data('value');
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/GetGroupsCustom";?>',
                    data: {id:schoolid,homeworkid:homeworkid},
                    success: function(data){
                       if(data.count=='0')
                       {
                           $('.nogroups').html('You have no groups for the school').show();
                       }
                       $('#type').html(data.result);
                   }
                });
            });
            $('#invitequickhw').click(function() {
                var type=$("input[name=type]:checked").val();
                $('.inviteinfo').hide();
                var nameids=names.getValue();
                if($('#homeworkid').val()=='' || $('#duedate').val()=='' || $('#publishdate').val()==''){
                    $('.inviteinfo').text('Please enter all fields.');
                    $('.inviteinfo').show();
                    $("#invitequickhw").prop("disabled", false);
                    return false;
                }
                else{
                    //$('#buddyindication').text('')
                    var from = $('#publishdate').val().split("-");
                    var publish = new Date(from[0], from[1] - 1, from[2]);
                    var to = $('#duedate').val().split("-");
                    var due = new Date(to[0], to[1] - 1, to[2]);
                    var dateObj = new Date();
                    var month = dateObj.getMonth(); //months from 1-12
                    var day = dateObj.getDate();
                    var year = dateObj.getFullYear();
                    var d = new Date(year,month,day);

                    if (publish > due) {
                        $('.dateinfo').show();
                        $('.dateinfo').show();
                        setTimeout(function () {
                            $('.dateinfo').hide();
                        }, 5000);
                        return false;
                    }
                    else if(publish < d ){
                        $('.datedelaypub').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else if( due < d ){
                        $('.datedelay').show();
                        setTimeout(function () {
                            $('.datedelaypub').hide();
                        }, 5000);
                        return false;
                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo Yii::app()->createUrl('teachers/PublishCustomHomework');?>',
                            beforeSend : function(){
                                $("#invitequickhw").prop("disabled", true)
                            },                            
                            data: {nameids: nameids,homeworkid:$('#homeworkid').val(),dudate:$('#duedate').val(),comments:$('#comments').val(),publishdate:$('#publishdate').val(),type:$("input[name=type]:checked").val(),solution:$('.showsolution:checked').val(),publishto:$('#publishto').val(),showmark:$('.showmarks:checked').val()}
                        })
                            .done(function (data) {
                                    $('.addgroupsinfo').text('Practice assigned to groups.');
                                    $('.addgroupsinfo').show();
                                    //$('#inviteclose').trigger('click');
                                    setTimeout(function() {
                                        $('#SelectGroup').modal('toggle');
                                        window.location='<?php echo Yii::app()->createUrl('teacherhome');?>';
                                    }, 2000);                               
                            });

                    }
                }
            });
        });
    
        $('.blueprintlist').click(function(){
            $('#default-list').hide();
            $('#blueprintlist').show();
        });
    
        $('.existinglist').click(function(){
            $('#default-list').show();
            $('#blueprintlist').hide();
        });
    </script>
