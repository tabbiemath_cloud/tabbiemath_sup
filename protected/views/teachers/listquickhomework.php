<div class="row">
    <div class="col-lg-12">
        <h3>Manage Quick Practice</h3>
        <input type="hidden" id="schoolid" value="<?php echo Yii::app()->session['school'];?>">
    </div>
    <div class="col-lg-3 pull-left" style="margin-bottom: 10px;padding-left: 0px;;">
    <a href="<?php echo Yii::app()->createUrl('teachers/createquickhomework')?>" class="btn btn-warning btn-block">Create Quick Practice</a>
    </div>
    
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'homework-grid',
            'dataProvider'=>$model->search(Yii::app()->user->id,Yii::app()->session['standard'],'0','0'),
            //'filter'=>$model,
            'columns'=>array(
                array(
                    'name'=>'TM_SCH_Name',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData($data->TM_SCH_Name,"Name")',
                ),
                array(
                    'name'=>'TM_SCH_Status',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData($data->itemAlias("HomeworkStatus",$data->TM_SCH_Status),"Status")',
                ),
                array(
                    'name'=>'TM_SCH_Standard_Id',
                    'type'=>'raw',
                    'value'=>'Schoolhomework::GetData(Standard::model()->findByPk($data->TM_SCH_Standard_Id)->TM_SD_Name,"Standard")',
                ),
                array(
                    'name'=>'TM_SCH_CreatedOn',                        
                    'value'=>'Yii::app()->dateFormatter->format("dd-MM-y",strtotime($data->TM_SCH_CreatedOn))',
                ),                
                array(
                    'type'=>'raw',
                    'name'=>'Publish',                    
                    'value' => 'CHtml::link("Publish","javascript:void(0)",array(
                    "title"=>"publish",
                    "class"=>"choosegroups btn btn-warning btn-xs",
                    "style"=>Schoolhomework::checkQuestions($data->TM_SCH_Id),
                    "data-backdrop"=>"static",
                    "data-toggle"=>"modal",
                    "data-target"=>"#SelectGroup",
                    "data-value"=>"$data->TM_SCH_Id"
                    ))',
                ),

                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'url'=>'Yii::app()->createUrl("teachers/updateCustomeHomework",array("id"=>$data->TM_SCH_Id))',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'url'=>'Yii::app()->createUrl("teachers/deleteHomework",array("id"=>$data->TM_SCH_Id))',
                            'options'=>array(

                                'title'=>'Delete',
                            ),

                        )
                    ),
                ),
            ),'itemsCssClass'=>"rwd-tables"
        )); ?>
        </div>
    </div>

    <div class="modal fade " id="SelectGroup" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Publish Practice</h4>
                </div>
                <div class="modal-body" >
                <div class="panel-body form-horizontal payment-form">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publish To</label>
                            <div class="col-sm-9">
                                <input type="text" id="usersuggest" name="assigngroups[]">
                                <input type="hidden" id="homeworkid">
                            </div>
                        </div>
                       </div> 
                       <div class="row"> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Due On</label>
                            <div class="col-sm-9">
                                <input type="date" id="duedate" class="form-control" name="duedate">                            
                            </div>
                        </div>
                        </div> 
                       <div class="row">                    
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comments</label>
                            <div class="col-sm-9">                        
                                <textarea name="comments" class="form-control" id="comments"></textarea>                                                        
                            </div>
                        </div>
                    <!--code by amal-->
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish Date</label>
                                <div class="col-sm-9">
                                    <input type="date" id="publishdate" class="form-control" name="publishdate">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Submission Type</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="type" name="type">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Show Solution</label>
                                <div class="col-sm-9">
                                    <input type="radio" class="showsolution" name="solution" value="1" /><span>Yes</span>
                                    <input type="radio" class="showsolution" name="solution" value="0" /><span>No</span>
                                </div>
                            </div>
                        </div>                        
                        <!--ends-->                                                                                    
                    </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                    <span class="alert alert-warning pull-left nogroups" style="display: none;"> </span>
                    <span class="alert alert-success pull-left addgroupsinfo" style="display: none;"> </span>
                    <button type="button" class="btn btn-default" id="invitequickhw">Assign</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            var names=$('#usersuggest').magicSuggest({
                maxSelection: 10,
                allowFreeEntries: false,
                data: [<?php echo $this->Groups(Yii::app()->session['school']);?>]
            });
            $('.choosegroups').click(function(){
                $('.nogroups').hide();
                var schoolid=$('#schoolid').val();
                var homeworkid = $(this).data('value');
                $('#homeworkid').val(homeworkid);
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/teachers/getGroups";?>',
                    data: {id:schoolid},
                    success: function(data){
                       if(data.count=='0')
                       {
                           $('.nogroups').html('You have no groups for the school').show();
                       }
                       $('#type').html(data.result);
                    }
                });
            });
            $('#invitequickhw').click(function() {
                $('.inviteinfo').hide();
                var nameids=names.getValue();
                if((nameids.length)!=0){
                    //$('#buddyindication').text('')
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('teachers/PublishHomework');?>',
                        data: {nameids: nameids,homeworkid:$('#homeworkid').val(),dudate:$('#duedate').val(),comments:$('#comments').val(),publishdate:$('#publishdate').val(),type:$('#type').val(),solution:$('.showsolution:checked').val()}
                    })
                        .done(function (data) {
                            //$('#buddyindication').text(data);
                        });
                    $('.addgroupsinfo').text('Practice assigned to groups.');
                    $('.addgroupsinfo').show();
                    //$('#inviteclose').trigger('click');
                    setTimeout(function() {
                        $('#SelectGroup').modal('toggle');
                        window.location.reload();
                    }, 2000);

                }
                else{
                    $('.inviteinfo').text('Please select groups.');
                    $('.inviteinfo').show();                    
                    //$('#buddyindication').text('No Buddies Selected');
                }

            });
        });
    </script>


