<div class="row">
    <div class="col-lg-12">
        <h3 class="h125 pull-left">Change Password</h3>
    </div>
</div>
<?php
/* @var $this MockController */
/* @var $model Mock */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('teachers/home')."';
    });
");
?>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
                </div>
                <?php endif;?>
                <?php $form=$this->beginWidget('UActiveForm', array(
                'id'=>'changepassword-form',
                'enableAjaxValidation'=>true,
            )); ?>
                <div class="panel-body form-horizontal payment-form">
                    <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
                    <div class="form-group">
                        <?php echo CHtml::activeLabelEx($model,'password',array('class'=>'col-sm-3 control-label')); ?>
                        <div class="col-sm-9">
                            <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Enter New Password')); ?>
                            <?php echo CHtml::error($model,'password'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo CHtml::activeLabelEx($model,'verifyPassword',array('class'=>'col-sm-3 control-label')); ?>
                        <div class="col-sm-9">
                            <?php echo $form->passwordField($model,'verifyPassword',array('class'=>'form-control','placeholder'=>'Re-enter Password')); ?>
                            <?php echo CHtml::error($model,'verifyPassword'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 pull-right">
                            <?php echo CHtml::submitButton(UserModule::t("Change Password"),array('class'=>'btn btn-warning btn-block')); ?>
                        </div>
                        <div class="col-lg-2 pull-right">
                            <button class="btn btn-warning btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>