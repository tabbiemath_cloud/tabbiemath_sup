<?php
$this->breadcrumbs=array(
    'Schools'=>array('admin'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Admin', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'),'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$school),'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'List Teachers', 'url'=>array('manageTeachers', 'id'=>$school)),
    array('label'=>'Add Teacher', 'url'=>array('AddTeacher', 'id'=>$school)),

    array('label'=>'Update Teacher', 'url'=>array('EditTeachers','id'=>$school,'master'=>$model->id)),
    array('label'=>'Add Standards', 'url'=>array('AddStandards', 'id'=>$school,'master'=>$model->id)),
    array('label'=>'Delete Teacher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('DeleteTeachers','id'=>$model->id),'confirm'=>'Are you sure you want to delete this Teacher?')),
);
?>
<h3>Add Standards To <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>
<div class="row brd1">
<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data','id'=>'everything')); ?>
    <div class="panel-body form-horizontal payment-form">
    <div class="col-lg-12">
        <div class="form-group">
            <label class="col-sm-3 control-label required" for="User_username">Select Standards</label>  
            <div class="col-sm-9">
                <?php
                $c = new CDbCriteria;
                $c->select ='t.TM_SD_Id, t.TM_SD_Name';
                $c->join = "INNER JOIN tm_school_plan tm_school_plan ON t.TM_SD_Id = tm_school_plan.TM_SPN_StandardId";
                $c->compare("tm_school_plan.TM_SPN_SchoolId", $school);

                $result=Standard::model()->findAll($c);
                $listdata = CHtml::listData($result, 'TM_SD_Id', 'TM_SD_Name');
                if(count($techerprevdata)!=0):
                    $selected=array();
                    foreach($techerprevdata AS $techerprevdat):
                        $selected[]=$techerprevdat['TM_TE_ST_Standard_Id'];

                        endforeach;
                    endif;
                echo CHtml::dropDownList('standards',$selected,$listdata,array('class'=>'form-control', 'multiple' => 'multiple','empty' => 'Select Standard')); ?>

            </div>
        </div>
    <div class="form-group">
         <div class="row">
            <div class="row">

                <div class="col-sm-3 pull-right"> 
                
                <?php echo CHtml::activeHiddenField($schoolstaff,'TM_TH_Id',array('value'=>$schoolstaff->TM_TH_Id)); ?>
                <?php echo CHtml::activeHiddenField($model,'id',array('value'=>$model->id)); ?>                   
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block','id'=>'addstandardsave')); ?>
                </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block" type="button" value="Reset">Cancel</button>
                </div>
            </div>         
        </div>
    
    </div>        
    </div>
</div>


<?php echo CHtml::endForm(); ?>
</div>
<?php Yii::app()->clientScript->registerScript('addstandard', "
");?>