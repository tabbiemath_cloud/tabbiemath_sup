<?php
/* @var $this PlanController */
/* @var $model Plan */
$this->menu=array(
    array('label'=>'Manage Plan', 'class'=>'nav-header'),
);
?>
<style>
    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {display: inline;}

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    ul.pagination li a.active {
        background-color: #4CAF50;
        color: white;
    }

    ul.pagination li a:hover:not(.active) {background-color: #ddd;}
</style>
<h3>Teachers Assessment Report</h3>
<div class="row brd1">

        <div class="col-lg-12">
            <form method="get" id="searchform">
                <table class="table table-bordered table-hover table-striped admintable">
                    <tr><th colspan="4"><h3><?php echo UserModule::t("Search"); ?></h3></th></tr>
                    <tr>
                        <th>School</th>
                        <th >Teacher</th>                         
                        <th colspan="2">
                            <input type="radio" class="date" <?php echo ($formvals['date']=='before'?'checked="checked"':'');?> name="date" value="before" id="before">Before a date
                            <input type="radio" class="date" <?php echo ($formvals['date']=='after'?'checked="checked"':'');?> name="date" value="after" id="after">After a date
                            <input type="radio" class="dates" <?php echo ($formvals['date']=='between' || $formvals['date']=='' ?'checked="checked"':'');?> name="date" value="between" id="days">Between dates
                        </th>                   
                    </tr>
                    <tr>
                        <td>
                            <select name="school" id="school" class="form-control">                                    
                            <?php 
                                $data=CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');
                                
                                echo CHtml::tag('option',
                                               array('value'=>''),'Select School',true);
                                foreach($data as $value=>$name)
                                {
                                    if($formvals['school']==$value):
                                        $selected=array('value'=>$value,'selected'=>'selected');              
                                    else:
                                        $selected=array('value'=>$value);
                                    endif;
                                    echo CHtml::tag('option',
                                               $selected,CHtml::encode($name),true);
                                }
                                if($formvals['school']=='0'):
                                    $selectedother=array('value'=>0,'selected'=>'selected');              
                                else:
                                    $selectedother=array('value'=>0);
                                endif;                                        
                                //echo CHtml::tag('option',$selectedother,'Other',true);                                    
                            ?>
                            </select>
                            <!--<input type="text" name="schooltext" id="schooltext" class="form-control" value="<?php echo ($formvals['schooltext']? $formvals['schooltext']:'');?>" />-->
                        </td>
                        <td><input type="text" name="teachername" id="teachername" class="form-control" value="<?php echo ($formvals['teachername']? $formvals['teachername']:'');?>" /></td>                                                                                               
                       
                        <td><input type="date" class="form-control" value="<?php echo ($formvals['fromdate']!=''?$formvals['fromdate']:'');?>" id="fromdate" name="fromdate" >
                        <span style="color:red;display: none;;" class="error errordivdate">Select A Date</span></td>
                        <td><input type="date" class="form-control" value="<?php echo ($formvals['todate']!=''?$formvals['todate']:'');?>" id="todate" name="todate" ></td>                       
                    </tr>                    
                    <tr>
                        <?php if(count($formvals)>0):?>
                            <td ></td>                        
                            <td ><a href="<?php echo Yii::app()->createUrl('teachers/Downloadassessment',array('teachername'=>$formvals['teachername'],'school'=>$formvals['school'],'date'=>$formvals['date'],'fromdate'=>$formvals['fromdate'],'todate'=>$formvals['todate']))?>" class="btn btn-warning btn-md">Export</a></td>
                        
                        <?php else:?>
                            <td colspan="2"></td>                            
                        <?php endif;?>                                                                        
                        <td><button class="btn btn-warning btn-lg btn-block" name="search" value="search" type="submit">Search</button></td>
                        <td ><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
    <div class="row brd1" style="width: 250%;">
        <div class="col-lg-12">
            <div class="grid-view" id="yw0">
                <table class="table table-bordered table-hover table-striped admintable">
                    <thead>
                    <tr >
                        <th >Created On</th>
                        <th >Teacher</th>                        
                        <th >Email</th>
                        <th >School</th>
                        <th >Type</th>
                        <th >Standard</th>
                        <th >Submission</th>
                        <th >Published On</th>
                        <th >Completed by Students</th>
                        <th >Pending</th>
                        <th >Status</th>
                        <th >Groups</th>
                        <th >Questions</th>                        
                    </tr>
                    </thead>
                    <tbody id="reportitems">
                        <?php 
                        if(count($dataval)>0):
                            foreach($dataval AS $key=>$data):
                                switch ($data['TM_SCH_PublishType']) {
                                    case 1:
                                        $submission='Worksheet (with options)';
                                        break;
                                    case 2:
                                        $submission='Online';
                                        break;
                                    case 3:
                                        $submission='Worksheet';
                                        break;                                    
                                } 
                                $completed=$this->GetCompletedAssessments($data['TM_SCH_Id']);
                                $pending=$this->GetPendingAssessments($data['TM_SCH_Id']);
                                                           
                        ?>
                            <tr id="assesreportrows<?=$data['TM_SCH_Id']?>">
                                <td ><?=date("d-m-Y", strtotime($data['TM_SCH_CreatedOn']))?></td>
                                <td ><?=$data['firstname'].' '.$data['lastname'];?></td>                        
                                <td ><?=$data['email']?></td>
                                <td ><?=$data['TM_SCL_Name']?></td>
                                <td ><?php 
                                    if($data['TM_SCH_Type']=='1'):
                                        echo 'Custom Practice';
                                    elseif($data['TM_SCH_Type']=='3'):
                                        echo 'Worksheet';
                                    elseif($data['TM_SCH_Type']=='33'):
                                        echo 'Resource';
                                    elseif($data['TM_SCH_Type']=='22'):
                                        echo 'Blueprint';
                                    else:
                                        echo 'Quick Practice';
                                    endif;                                    
                                    ?></td>
                                <td ><?=$this->GetStandard($data['TM_SCH_Standard_Id'])?></td>
                                <td ><?=$submission;?></td>
                                <td ><?=date("d-m-Y", strtotime($data['TM_SCH_PublishDate']))?></td>
                                <td ><?=$completed;?></td>
                                <td ><?=$pending;?></td>
                                <td ><?=($data['TM_SCH_Status']=='2'?'Completed':'Open');?></td>
                                <td ><?=$this->GetAssignedGroups($data['TM_SCH_Id']);?></td> 
                                <td ><?=$this->GetQuestions($data['TM_SCH_Id']);?></td>
                            </tr>                        
                        <?php 
                            endforeach;
                        ?>
                        <?php else:?>
                            <tr>
                                <th colspan="11">No results found</th>
                            </tr>    
                        <?php endif;?>  
                    </tbody>
                </table>            
            </div>
        </div>
        <ul class="pagination" >
            <li><a href="#">Previous</a></li>
            <?php echo $nopages;?>
        </ul>
    </div>  
                            
    <script type="text/javascript">
        $(function(){
            $('#clear').click(function(){
                $('#student ').val('');
                $('#parent ').val('');
                $('#school ').val('');
                $('#plan ').val('');
                return true;
            });
            $('#searchform').submit(function(){
                var error=0
               $('.error').hide()               
               if($('#plan').val()=='')
               {
                    $('.errordiv').show();
                    error=1;
                    
               } 
               else
               {
                    $('.errordiv').hide();                                        
               }               
               if($('#fromdate').val()=='')
               {
                    $('.errordivdate').show();
                    error=1;                
               }
               else
               {
                    $('.errordivdate').hide();
               }
                if(error==1)
                {
                    return false;
                }                
                else
                {
                    return true;
                }              
            });
            $('.date').click(function(){
                $('#fromdate').attr('disabled',false);
                $('#todate').attr('disabled',true);
            })
            $('.dates').click(function(){
                $('#fromdate').attr('disabled',false);
                $('#todate').attr('disabled',false);
            })
            <?php
                if($formvals['date']=='before'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',true);";
                elseif($formvals['date']=='after'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',true);";                
                elseif($formvals['date']=='between'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',false);";                
                endif; 
            ?>
        }); 
    </script>
<script type="text/javascript">
        $(function(){
            $('#schooltext').hide();                        
            $('#school').change(function(){                
                if($(this).val()=='0')
                {
                    $('#schooltext').show();
                }   
                else
                {
                    $('#schooltext').hide();
                }
            });              
        });
        /*$(document).on('click', '#export', function(){
            $.ajax({
                type: 'POST',
                url: '../plan/Export',
                data: $("#searchform" ).serialize()
            });
        });*/
    </script>