<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */

$this->breadcrumbs=array(
	'Syllabuses'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Syllabus', 'class'=>'nav-header'),
	array('label'=>'List Syllabus', 'url'=>array('admin')),
);
?>

<h3>Create Syllabus</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>