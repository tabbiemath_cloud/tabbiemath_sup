<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('syllabus/admin')."';
    });
");
?>

<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'syllabus-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="panel-body form-horizontal payment-form">
    <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
    <div class="form-group">
		<?php echo $form->labelEx($model,'TM_SB_Name',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?php echo $form->textField($model,'TM_SB_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
            <?php echo $form->error($model,'TM_SB_Name'); ?>
        </div>
	</div>

	<!--<div class="row">
		<?php /*echo $form->labelEx($model,'TM_SB_CreatedOn'); */?>
		<?php /*echo $form->textField($model,'TM_SB_CreatedOn'); */?>
		<?php /*echo $form->error($model,'TM_SB_CreatedOn'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->labelEx($model,'TM_SB_CreatedBy'); */?>
		<?php /*echo $form->textField($model,'TM_SB_CreatedBy'); */?>
		<?php /*echo $form->error($model,'TM_SB_CreatedBy'); */?>
	</div>-->

    <div class="form-group">
		<?php echo $form->labelEx($model,'TM_SB_Status',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?php echo $form->hiddenField($model,'TM_SB_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
            <?php echo $form->dropDownList($model,'TM_SB_Status',Syllabus::itemAlias('SyllabusStatus'),array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'TM_SB_Status'); ?>
        </div>
	</div>
    <div class="form-group">
        <div class="raw">
        <div class="col-sm-3 pull-right">
		    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
        </div>
            <div class="col-lg-3 pull-right">
                <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
            </div>
        </div>
	</div>

<?php $this->endWidget(); ?>
</div>
</div><!-- form -->