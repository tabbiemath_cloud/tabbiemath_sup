<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_SB_Id'); ?>
		<?php echo $form->textField($model,'TM_SB_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SB_Name'); ?>
		<?php echo $form->textField($model,'TM_SB_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SB_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_SB_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SB_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_SB_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SB_Status'); ?>
		<?php echo $form->textField($model,'TM_SB_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->