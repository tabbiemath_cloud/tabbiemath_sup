<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */

$this->breadcrumbs=array(
	'Syllabuses'=>array('admin'),
	$model->TM_SB_Id,
);

$this->menu=array(
    array('label'=>'Manage Syllabus', 'class'=>'nav-header'),
	array('label'=>'List Syllabus', 'url'=>array('admin')),
	array('label'=>'Create Syllabus', 'url'=>array('create')),
	array('label'=>'Update Syllabus', 'url'=>array('update', 'id'=>$model->TM_SB_Id)),
	array('label'=>'Delete Syllabus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_SB_Id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View <?php echo $model->TM_SB_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $id=$model->TM_SB_Status;
        $userid=$model->TM_SB_CreatedBy;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_SB_Id',
                'TM_SB_Name',
                'TM_SB_CreatedOn',
                array(
                    'label'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Syllabus::itemAlias("SyllabusStatus",$id),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
