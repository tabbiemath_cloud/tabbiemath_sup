<?php
/* @var $this SyllabusController */
/* @var $data Syllabus */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SB_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_SB_Id), array('view', 'id'=>$data->TM_SB_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SB_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SB_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SB_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SB_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SB_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SB_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SB_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SB_Status); ?>
	<br />


</div>