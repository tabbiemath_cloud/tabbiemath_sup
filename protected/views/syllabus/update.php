<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */

$this->breadcrumbs=array(
	'Syllabuses'=>array('admin'),
	$model->TM_SB_Id=>array('view','id'=>$model->TM_SB_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Syllabus', 'class'=>'nav-header'),
	array('label'=>'List Syllabus', 'url'=>array('admin')),
	array('label'=>'Create Syllabus', 'url'=>array('create')),
	array('label'=>'View Syllabus', 'url'=>array('view', 'id'=>$model->TM_SB_Id)),

);
?>

<h3>Update <?php echo $model->TM_SB_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>