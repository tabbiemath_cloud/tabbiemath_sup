<?php
/* @var $this PatternsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Patterns',
);

$this->menu=array(
	array('label'=>'Create Patterns', 'url'=>array('create')),
	array('label'=>'Manage Patterns', 'url'=>array('admin')),
);
?>

<h1>Patterns</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
