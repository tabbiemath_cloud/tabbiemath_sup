<?php
/* @var $this PatternsController */
/* @var $model Patterns */

$this->breadcrumbs=array(
	'Patterns'=>array('admin'),
	$model->TM_PT_Id,
);

$this->menu=array(
    array('label'=>'Manage Patterns', 'class'=>'nav-header'),
	array('label'=>'List Patterns', 'url'=>array('admin')),
	array('label'=>'Create Pattern', 'url'=>array('create')),
	array('label'=>'Update Pattern', 'url'=>array('update', 'id'=>$model->TM_PT_Id)),
	array('label'=>'Delete Pattern', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_PT_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3>View Pattern <?php echo $model->TM_PT_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_PT_Id',
                'TM_PT_Name',
                'TM_PT_CreatedOn',
                array(
                    'name'=>'TM_PT_CreatedBy',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($model->TM_PT_CreatedBy),
                ),
                array(
                    'name'=>'TM_PT_Status',
                    'type'=>'raw',
                    'value'=>Patterns::itemAlias("PatternsStatus",$model->TM_PT_Status),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
