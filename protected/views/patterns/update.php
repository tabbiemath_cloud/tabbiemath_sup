<?php
/* @var $this PatternsController */
/* @var $model Patterns */

$this->breadcrumbs=array(
	'Patterns'=>array('admin'),
	$model->TM_PT_Id=>array('view','id'=>$model->TM_PT_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Patterns', 'class'=>'nav-header'),
	array('label'=>'List Patterns', 'url'=>array('admin')),
	array('label'=>'Create Pattern', 'url'=>array('create')),
	array('label'=>'View Pattern', 'url'=>array('view', 'id'=>$model->TM_PT_Id)),
);
?>

<h3>Update Pattern <?php echo $model->TM_PT_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>