<?php
/* @var $this PatternsController */
/* @var $model Patterns */

$this->breadcrumbs=array(
	'Patterns'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Patterns', 'class'=>'nav-header'),
	array('label'=>'List Patterns', 'url'=>array('admin')),
);
?>

<h3>Create Pattern</h3>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>