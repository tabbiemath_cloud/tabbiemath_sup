<?php
/* @var $this PatternsController */
/* @var $data Patterns */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PT_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_PT_Id), array('view', 'id'=>$data->TM_PT_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PT_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PT_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PT_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PT_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PT_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PT_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PT_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PT_Status); ?>
	<br />


</div>