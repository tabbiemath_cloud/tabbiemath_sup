<?php
/* @var $this PatternsController */
/* @var $model Patterns */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_PT_Id'); ?>
		<?php echo $form->textField($model,'TM_PT_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PT_Name'); ?>
		<?php echo $form->textField($model,'TM_PT_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PT_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_PT_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PT_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_PT_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PT_Status'); ?>
		<?php echo $form->textField($model,'TM_PT_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->