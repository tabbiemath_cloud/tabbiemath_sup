<?php //$progressstatus=$this->GetCompleteStatus($student->id,$plan->TM_PN_Id);?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
    <!-- new design -->
        <div class="bs-example">        
			<h1 class="h125"><?php echo ucfirst($student->profile->firstname);?> : Progress for <?php echo ucfirst($plan->TM_PN_Name);?></h1>
            <div class="panel panel-default">
                    <table class="rwd-tables">                    
                        <tr>
                            <th></th>
                            <th>This Week</th>
                            <th>Total</th>                            
                        </tr>
                        <?php  if ($planmodules['revision']):?>
                        <tr>
                            <td><b>Revisions attempted</b></td>
                            <td><span class="rwd-tables thead"><b>This Week</b></span><span class="rwd-tables tbody"><?php echo $revisionweekstatus;?></span></td>
                            <td><span class="rwd-tables thead"><b>Total</b></span><span class="rwd-tables tbody"><?php echo $revisiontotal;?></span></td>                                                        
                        </tr>
                        <?php 
                        endif;
                        if ($planmodules['challenge']):
                        ?>
                        <tr>
                            <td><b>Challenges attempted</b></td>
                            <td><span class="rwd-tables thead"><b>This Week</b></span><span class="rwd-tables tbody"><?php echo $challengeweekstatus;?></span></td>
                            <td><span class="rwd-tables thead"><b>Total</b></span><span class="rwd-tables tbody"><?php echo $challengetotal;?></span></td>                                                                                    
                        </tr>
                        <?php 
                        endif;
                        if ($planmodules['mock']):
                        ?>                        
                        <tr>
                            <td><b>Mocks attempted</b></td>
                            <td><span class="rwd-tables thead"><b>This Week</b></span><span class="rwd-tables tbody"><?php echo $mockweekstatus;?></span></td>
                            <td><span class="rwd-tables thead"><b>Total</b></span><span class="rwd-tables tbody"><?php echo $mocktotal;?></span></td>                                                                                    
                        </tr> 
                        <?php endif;?>
                        <tr>
                            <td><b>Points Collected</b></td>
                            <td><span class="rwd-tables thead"><b>This Week</b></span><span class="rwd-tables tbody"><?php echo ($weeklypoints->totalpoints==''?'0':$weeklypoints->totalpoints);?></span></td>
                            <td><span class="rwd-tables thead"><b>Total</b></span><span class="rwd-tables tbody"><?php echo ($totalpoints->totalpoints==''?'0':$totalpoints->totalpoints);?></span></td>
                        </tr>                        
                        <tr>
                        <td ><b>Last login</b></td>
                        <td colspan="2"><b><?php echo $lastlogin;?></b></td>
                        </tr>                                               
                    </table>               
            </div>            
        </div>
        <div class="bs-example">
            <h1 class="h125">Overall Performance: %Completed vs % Correct</h1>
            <div class="panel panel-default clearfix">
                <div class="col-md-12" style="text-align: center;" >
            	<div class="row" style="margin-top: 15px;margin-bottom: 15px;">
                    					<div class="col-md-12">
                    						<div class="col-md-4">
                    							<span class="attmt"> </span>% attempted
                    						</div>
                    						<div class="col-md-4">
                    							<span class="coattmt"> </span>% correct from attempted
                    						</div>
                    						<div class="col-md-4">
                    							<span class="noattmt"> </span>% not attempted
                    						</div>
                    					</div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 padtop20" id="overallstatus" style="text-align: center;" >                                      
                        <img src="<?php echo Yii::app()->request->baseUrl.'/images/reportloader.gif';?>" style="margin-bottom: 20px;margin-top: 20px;"/>
                        <h6>Fetching data..</h6>                   
        		     </div>
                    <div class="col-sm-4 pull-right text-center padtop20" id="reststatus">
	                   <b>Overall % completed</b>
                    	<div class="row"  style="margin-bottom: 40px;">
                    	  <div class="col-xs-6 col-sm-6" id="completedme">
                                <img src="<?php echo Yii::app()->request->baseUrl.'/images/reportloader.gif';?>" style="margin-bottom: 20px;margin-top: 20px;"/>
                                <h6>Fetching data..</h6>                           										                    			
                    	  </div>
                    	  <div class="col-xs-6 col-sm-6"  id="completedrest">
                                <img src="<?php echo Yii::app()->request->baseUrl.'/images/reportloader.gif';?>" style="margin-bottom: 20px;margin-top: 20px;"/>
                                <h6>Fetching data..</h6>                                              		
                    	  </div>
                    	</div>
                    	
                    	<strong>Overall : Correct vs Incorrect</strong><br>
                    	<small>(From attempted topics)</small>
                    	<div class="row">
                    	  <div class="col-xs-6 col-sm-6" id="overallme">
                            <img src="<?php echo Yii::app()->request->baseUrl.'/images/reportloader.gif';?>" style="margin-bottom: 20px;margin-top: 20px;"/>
                                <h6>Fetching data..</h6>
                    		
                    	  </div>
                    	  <div class="col-xs-6 col-sm-6" id="overallrest">
                                <img src="<?php echo Yii::app()->request->baseUrl.'/images/reportloader.gif';?>" style="margin-bottom: 20px;margin-top: 20px;"/>
                                <h6>Fetching data..</h6>                          
                    		
                    	  </div>
                    	</div>                                         
                    </div>                                   
                </div>              
            </div>
        </div>                    
    <!-- new design ends-->                           
        <div class="bs-example">
            <h1 class="h125">Suggested topics to revise on based on past performance</h1>
            <div class="panel panel-default clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 padtop20" id="topicstatus" style="text-align: center;">
                    <img src="<?php echo Yii::app()->request->baseUrl.'/images/reportloader.gif';?>" style="margin-bottom: 20px;margin-top: 20px;"/>
                    <h6>Fetching data..</h6>                                  
                </div>              
            </div>
        </div>                                            
    </div>
</div>    <?php Yii::app()->clientScript->registerScript('showmarks', "
    $('#viewsolution').click(function(){
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('student/GetSolution')."',
          data: { testid: '".$test->TM_STU_TT_Id."'}
        })
          .done(function( html ) {
             $('#showsolution').html( html );
          });
    });
               

");?>
<script>
$(document).ready(function() {
    $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('parent/GetChapterstatus');?>',
          data: { student: '<?php echo $student->id;?>',plan: '<?php echo $plan->TM_PN_Id;?>'}          
        })
          .done(function( html ) {
             $('#overallstatus').html( html );
             TopicStatus()
    });
});
function TopicStatus()
{    
    $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('parent/GetTopicStatus');?>',
          data: { student: '<?php echo $student->id;?>',plan: '<?php echo $plan->TM_PN_Id;?>'}                     
        })
          .done(function( html ) {
             $('#topicstatus').html( html );
             RestStatus()
    });  
    
}
function RestStatus()
{
    $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('parent/GetRestStatus');?>',
          dataType: 'json',
          data: { student: '<?php echo $student->id;?>',plan: '<?php echo $plan->TM_PN_Id;?>'}                     
        })
          .done(function( data ) {
             $('#completedrest').html('<div class="pie_progress4 restaverage" role="progressbar" data-goal="'+data.restaverage+'" aria-valuemin="1" aria-valuemax="100"><span class="pie_progress4__number">'+data.restaverage+'%</span></div><p>Rest of the crowd</p>')
             $('#overallrest').html('<div class="pie_progress3 restcorrect" role="progressbar" data-goal="'+data.restcorrect+'" aria-valuemin="1" aria-valuemax="100"><span class="pie_progress3__number">'+data.restcorrect+'%</span></div><p>Rest of the crowd</p>')
             MyStatus()
		$('.restcorrect').asPieProgress({
            namespace: 'pie_progress'
        });
		$('.restcorrect').asPieProgress('go');
        
		$('.restaverage').asPieProgress({
            namespace: 'pie_progress'
        });
		$('.restaverage').asPieProgress('go'); 
                  
    });       
}
function MyStatus()
{
    $.ajax({
          type: 'POST',
          url: '<?php echo Yii::app()->createUrl('parent/GetMyStatus');?>',
          dataType: 'json',                    
          data: { student: '<?php echo $student->id;?>',plan: '<?php echo $plan->TM_PN_Id;?>'}                     
        })
          .done(function( data ) {
            $('#completedme').html('<div class="pie_progress4 meaverage" role="progressbar" data-goal="'+data.average+'" aria-valuemin="1" aria-valuemax="100"><span class="pie_progress4__number">'+data.average+'%</span></div><p>Me</p>')
            $('#overallme').html('<div class="pie_progress3 mecorrect" role="progressbar" data-goal="'+data.correct+'" aria-valuemin="1" aria-valuemax="100"><span class="pie_progress3__number">'+data.correct+'%</span></div><p>Me</p>')                          
		$('.meaverage').asPieProgress({
            namespace: 'pie_progress'
        });
		$('.meaverage').asPieProgress('go');
        
		$('.mecorrect').asPieProgress({
            namespace: 'pie_progress'
        });
		$('.mecorrect').asPieProgress('go'); 
                  
    });    
}
       
</script>