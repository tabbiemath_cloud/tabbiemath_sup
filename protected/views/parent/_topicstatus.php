
                <?php if(count($topicstatus)>0): ?>
                <div class="row">
                <div class="col-xs-12 col-md-3 text-left">
				    <b>Topic</b>
				</div>
                <div class="col-xs-12 col-md-3 text-left">
					<b>Chapter</b>
				</div>
                </div>
                
                <?php  
                              
                    foreach($topicstatus AS $topic):
                    
                    $chapter=Chapter::model()->findByPk($topic['TM_SN_Topic_Id']);  
                    if($topic['completed']>0):
                        $average=($topic['completed']/$topic['total'])*100;                
                        if($topic['attempted']>0):
                            $correct=round(($topic['attempted']/$topic['completed'])*100);
                        else:
                            $correct=0;
                        endif;
                        $average=round($average);
                    else:
                        $average=0;
                        $correct=0;
                    endif;                                           
                    ?>
                    <div class="row" style=" margin-bottom: 20px;">                     	
    						<div class="col-xs-12 col-md-3 text-left">
    							<h6 style="margin-top: 7px;margin-bottom: 16px;"><?php echo $topic['TM_SN_Name'];?></h6>
    						</div>
                            <div class="col-xs-12 col-md-3 text-left">
    							<h6 style="margin-top: 7px;margin-bottom: 16px;"><?php echo $chapter->TM_TP_Name;?></h6>
    						</div>                        
                        	<div class="col-xs-12 col-md-6">
                        		<div class="progress" >                               
                            			<div class="progress-bar progress-bar-danger progrescompleted"  style="width: 100%; text-align: right;">
                            				<span class="attempted"> &nbsp;</span>
                                            
                            			<div class="progress-bar progress-bar-success progresssuccess" style="width: <?php echo  $correct;?>%;">
                            				<span class="correct" ><?php echo  $correct;?>%</span>
                            			</div>
                            			</div>                                 
                        		</div>  
                        	</div>
                        <!-- end LP -->
                        
                        </div>                   
                    <?php
                    endforeach;
                else:                
                ?>
                    <div class="row" >
                        <div class="col-xs-12 col-md-12" style="text-align: center;">
                            <h6>No Topics found</h6>
                        </div>
                    </div>
                <?php endif;?>