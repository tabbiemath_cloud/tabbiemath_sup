
                    <?php  
                    if(count($chapters)>0):                       
                        foreach($chapters AS $key=>$chapter):                                                                                                               
                            $getquestions=$this->GetChapterCount($chapter['TM_QN_Topic_Id'],$student,$chapter['attemptedcount'],$chapter['correctcount']);                              
                    ?>   
                        <div class="row" style=" margin-bottom: 20px;"> 
                        	<div class="col-xs-12 col-md-4 text-left">
                        		<h6 style="margin-top: 7px;margin-bottom: 16px;"><?php echo $chapter['TM_TP_Name'];?></h6>
                        	</div>
                        	<div class="col-xs-12 col-md-8">
                        		<div class="progress" style="background-color:#a3a3a3;">
                                    <?php if($getquestions['correct']>0):?>
                            			<div class="progress-bar progress-bar-danger progrescompleted"  style="width: <?php echo $getquestions['average'];?>%; text-align: right;">
                            				<span class="attempted"> <?php echo $getquestions['average'];?>% </span>
                                            <div class="progress-bar progress-bar-success progresssuccess" style="width: <?php echo $getquestions['correct'];?>%;">
                                				<span class="correct" ><?php echo $getquestions['correct'];?>%</span>
                                			</div>                                        
                            			</div>
                            			
                                    <?php else:?>
                                        <div class="progress-bar progress-bar-danger"  style="width: <?php echo $getquestions['average'];?>%; text-align: right;">
                            				<span class="attempted"> <?php echo $getquestions['average'];?>% </span>
                            			</div>
                                    <?php endif;?> 
                        		</div>  
                        	</div>
                        <!-- end LP -->
                        
                        </div>                                         				
                     <?php                     
                       endforeach;
                       else:
                       ?> 
                       <div class="col-xs-12 col-md-12 text-left">
                        		<h6 style="margin-top: 7px;margin-bottom: 16px;">No questions attempted</h6>
                        	</div>
                       <?php endif;?>