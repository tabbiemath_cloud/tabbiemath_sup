<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
    <?php if($planmodules['revision']):?>
        <div class="bs-example">

            <div class="sky1">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky1.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <h1 class="h125"><?php echo ucfirst($student->profile->firstname);?>: Revisions for plan <?php echo ucfirst($plan->TM_PN_Name);?></h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="table-responsive-vertical shadow-z-1">
                    <!-- Table starts here -->


                    <div class="table-responsive shadow-z-1">
                        <table class="table table-hover table-mc-light-blue">
                            <thead>
                            <tr>
                                <th>Published On</th>
                                <th>Publisher</th>
                                <th>Topic</th>
                                <th>Questions</th>
                            </tr>
                            </thead>
                            <tbody class="font2">
                            <?php
                            if(count($studenttests)>0):
                            foreach ($studenttests AS $tests):
                                ?>
                                <tr id="<?php echo 'testrow' . $tests->TM_STU_TT_Id;?>">
                                    <td data-title="Date"> <?php echo Yii::app()->dateFormatter->formatDateTime(strtotime($tests->TM_STU_TT_CreateOn), 'long', false); ?></td>
                                    <td data-title="Publisher"><?php echo $tests->publisher->TM_PR_Name;?></td>
                                    <td data-title="Division"><?php
                                        foreach ($tests->chapters AS $key => $chapter):
                                            echo ($key != '0' ? ' ,' : '') . $chapter->chapters->TM_TP_Name;
                                        endforeach;?></td>
                                    <td>
                                        <?php echo $this->GetQuestionCount($tests->TM_STU_TT_Id);?>
                                    </td>
                                </tr>
                            <?php endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="5" class="font2" style="text-align: center" align="center">No open revisions found</td>
                            </tr>
                            <?php endif;?>
                            </tbody>
                        </table>
                    </div>


                </div>

            </div>
            <div
                class="sky3"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    <?php endif;?>

    <?php if($planmodules['challenge']):?>
        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;"><?php echo ucfirst($student->profile->firstname);?>: Challenges for plan <?php echo ucfirst($plan->TM_PN_Name);?></h1>            

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table table-hover table-mc-light-blue">
                        <thead>
                        <tr>
                            <th>Set By</th>
                            <th>Set On</th>
                            <th>Set For</th>
                        </tr>
                        </thead>
                        <tbody class="font2">
                        <?php
                        if(count($challenges)>0):
                        foreach ($challenges AS $challenge):

                            ?>
                            <tr id="challengerow<?php echo $challenge->challenge->TM_CL_Id;?>">
                                <td data-title="Date"><?php echo $this->GetChallenger($challenge->challenge->TM_CL_Chalanger_Id);?></td>
                                <td data-title="Publisher"><?php echo date("d-m-Y", strtotime($challenge->challenge->TM_CL_Created_On)); ?></td>
                                <td data-title="Division" id="buddies<?php echo $challenge->challenge->TM_CL_Id;?>"><?php echo $this->GetChallengeInvitees($challenge->challenge->TM_CL_Id, $student->id);?></td
                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="5" class="font2" style="text-align: center" align="center">No open challenges found</td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal fade " id="invitModel" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Invite Buddies</h4>
                            </div>
                            <form action="" method="post" id="inviteform">
                            <div class="modal-body" >
                                <input type="hidden" name="challengeid" id="challengeid">
                                <input type="text" id="usersuggest" name="invitebuddies[]">
                                <span class="alert alert-danger error" role="alert" style="display: none;"></span>
                            </div>
                            <div class="modal-footer">
                                <span class="alert alert-success pull-left inviteinfo" style="display: none;"> </span>
                                <button type="button" class="btn btn-default" id="invitebuddies" >Invite</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>
    <?php endif;?>
    <?php if($planmodules['mock']):?>
        <div class="bs-example">

            
            <h1 class="h125" style="padding-top: 0;"><?php echo ucfirst($student->profile->firstname);?>: Mocks for plan <?php echo ucfirst($plan->TM_PN_Name);?></h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table table-hover table-mc-light-blue">
                        <thead>
                        <tr>
                            <th>Mock</th>
                            <th>Description</th>
                            <th>Created On</th>
                        </tr>
                        </thead>
                        <tbody class="font2">
                        <?php
                        if(count($studentmocks)>0):
                        foreach ($studentmocks AS $mock):
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type NOT IN (6,7)';
                            $totalonline=MockQuestions::model()->count($criteria);
                            $criteria=new CDbCriteria;
                            $criteria->condition='TM_MQ_Mock_Id='.$mock->mocks->TM_MK_Id.' AND TM_MQ_Type IN (6,7)';
                            $totalpaper=MockQuestions::model()->count($criteria);
                            $totalquestions=$totalonline+$totalpaper;
                            ?>
                            <tr id="mockrow<?php echo $mock->TM_STMK_Id;?>">
                                <td data-title="Date"><?php echo $mock->mocks->TM_MK_Name;?></td>
                                <td data-title="Date"><?php echo $mock->mocks->TM_MK_Description;?></td>
                                <td data-title="Publisher"><?php echo date("d-m-Y", strtotime($mock->mocks->TM_MK_CreatedOn)); ?></td>
                            </tr>
                        <?php endforeach;
                        else:?>
                        <tr>
                            <td colspan="5" class="font2" style="text-align: center" align="center">No open mocks found</td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="sky2">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky2.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <div class="sky3">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?>
            </div>
        </div>
    <?php endif;?>
    </div>

<div id="myinvite" class="modal fade" role="dialog" style="padding-top:20%;">
  <div class="modal-dialog modal-sm modal-sm_plus">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Challenge position</h4>
      </div>
      <div class="modal-body modal-body_padding">
      <div class="row">
        <ul class='popu-cart' role='menu' style='display: block !important;'>                  
            </ul>
            </div>
      </div>
    </div>

  </div>
</div>
</div>
<script type="text/javascript">
    $(function(){
    $(document).on('click','.hover',function(){
        $('.popu-cart').html(''); 
        var id=$(this).attr('data-id');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/parent/GetChallengeBuddies";?>',
                data: {challenge:id},
                success: function(data){
                    if(data!='no')
                    {
                        $('.popu-cart').html(data);                        
                    }
                }
            });
    });        
        $('#usersuggest').magicSuggest({
            maxSelection: 10,
            allowFreeEntries: false,
            data: [<?php echo $this->GetBuddies(Yii::app()->user->id);?>]
        });
        $('.choosebuddies').click(function()
        {
            var challengeid=$(this).attr('data-id');
            $('#challengeid').val(challengeid);

        });
        $('#invitebuddies').click(function(){
            var challengeid=$('#challengeid').val();
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/student/Invitebuddies";?>',
                data: $('#inviteform').serialize(),
                success: function(data){
                    if(data)
                    {
                        $('.inviteinfo').show();
                        $('.inviteinfo').text('Invite sent successful.');
                        $('#inviteform')[0].reset();
                        setTimeout(function(){
                            $('.close').trigger('click');
                            $('.inviteinfo').hide();
                        },3000);
                        $('#buddies'+challengeid).html(data);
                    }
                    else
                    {
                        $('.inviteinfo').show();
                        $('.inviteinfo').text('Invite failed. Please try again later.');
                        setTimeout(function(){
                            $('.inviteinfo').hide();
                        },3000);
                    }
                }
            });

        });
    });
    $(document).on('click','.hover',function(){
        var id=$(this).attr('data-id');
        var tooltip=$(this).attr('data-tooltip');
        $('.tooltiptable').hide();
        $('#tooltip'+id).fadeIn();
    });
    $(document).on('click','#close',function(){
        $('.tooltiptable').hide();

    });
</script>
