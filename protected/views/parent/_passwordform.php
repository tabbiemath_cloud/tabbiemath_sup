<div  class="my_student-panel panel panel-default formdiv" >
<div class="panel-heading" style="background-color: rgb(154, 163, 182);color: white;">
    <div class="row">
        <div class="col-md-6 col-xs-6 ">
            <h5><b><?php echo ($type=='student'?'Change student password':'Change password');?></b></h5>
        </div>
        <button class="pull-right btn btn-default btn-xs closeform" style="margin-right: 10px;">X</button>
    </div>
</div>
<div class="panel-body">
    <?php $form=$this->beginWidget('UActiveForm', array(
        'id'=>'changepassword-form',
        'enableAjaxValidation'=>true,
        'action'=>Yii::app()->request->baseUrl.'/parent/ChangePassword'
    )); ?>
    <div class="form-group">
        <div class="col-md-4 col-sm-6">

            <dl>
                <dt><?php echo $form->labelEx($model,'password'); ?></dt>
                <dd>
                    <?php echo $form->passwordField($model,'password',array('class'=>'form-control passwordfield','data-item'=>'Password')); ?>
                    <input id="userid" type="hidden" name="userid" value="<?php echo $student;?>">
                    <input id="userid" type="hidden" name="usertype" value="<?php echo $type;?>">
                </dd>
            </dl>

        </div>


        <div class="col-md-4 col-sm-6">
            <dl>
                <dt><?php echo $form->labelEx($model,'verifyPassword'); ?></dt>
                <dd>
                    <?php echo $form->passwordField($model,'verifyPassword',array('class'=>'form-control passwordfield verify','data-item'=>'Retype password')); ?>
                </dd>
            </dl>
        </div>
        <div class="col-md-4 col-sm-12" style="padding-top: 20px;">
        <button type="button" class="btn btn-warning closeform pull-right">Cancel</button>
        <?php echo CHtml::submitButton(UserModule::t("Save"),array('class'=>'btn btn-warning pull-right','id'=>'passwordsubmit','style'=>'margin-right:20px;')); ?>                   
        </div>


                
    </div>
    <?php $this->endWidget(); ?>
</div>
</div>
