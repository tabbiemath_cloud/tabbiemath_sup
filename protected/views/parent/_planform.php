<div  class="my_student-panel panel panel-default formdiv" >
    <div class="panel-heading" style="background-color: rgb(154, 163, 182);color: white;">
        <div class="row">
            <div class="col-md-6 col-xs-6 ">
                <h5><b><?php echo ($mode=='subscription'?'Step 2 - ':'');?> Add new subscription</b></h5>
            </div>
            <button class="pull-right btn btn-default btn-xs closeform" style="margin-right: 10px;">X</button>
        </div>
    </div>
    <div class="panel-body">
        <?php if($parent->location=='0'):?>
            <form id="planform" method="post" action="<?php echo Yii::app()->request->baseUrl.'/processpayment.php';?>">
        <?php else:?>
            <form id="planform" method="post" action="<?php echo Yii::app()->request->baseUrl.'/payment/process.php';?>">
        <?php endif;?>
            <div class="form-group">
                <div class="row">
                    <?php if(Publishers::model()->with('plan')->count(array('condition' => 'TM_PR_Status=0 AND TM_PN_Visibility=0'))>1):?>
                        <div class="col-md-2 col-sm-6">

                            <dl>
                                <dt>Publisher</dt>
                                <dd><?php echo CHtml::dropDownList('publisher','',CHtml::listData(Publishers::model()->with('plan')->findAll(array('condition' => 'TM_PR_Status=0 AND TM_PN_Visibility=0','order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),array('empty'=>'Select','class'=>'form-control subscripvalid','data-item'=>'Publisher'));?></dd>
                            </dl>

                        </div>
                    <?php
                    else:?>
                        <input id="publisher" type="hidden" name="publisher" value="<?php echo Publishers::model()->with('plan')->find(array('condition' => 'TM_PR_Status=0 AND TM_PN_Visibility=0'))->TM_PR_Id;?>">
                    <?php endif;?>
                    <div class="col-md-3 col-sm-6">
                        <dl>
                            <dt>Syllabus</dt>
                            <dd><?php echo CHtml::dropDownList('syllabus','',CHtml::listData(Syllabus::model()->with('plan')->findAll(array('condition' => 'TM_PN_Visibility=0','order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty'=>'Select','class'=>'form-control subscripvalid','data-item'=>'Syllabus'));?></dd>
                        </dl>
                    </div>
                    <div class="col-md-3 col-sm-6">


                        <dl>
                            <dt>Standard</dt>
                            <dd>
                                <select id="standard" name="standard" class="form-control subscripvalid" data-item="Standard">
                                    <option value="">Select</option>
                                </select>
                            </dd>
                        </dl>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <dl>
                            <dt>Subscription</dt>
                            <dd>
                                <select id="plan" name="plan" class="form-control subscripvalid"  data-item="Plan">
                                    <option value="">Select</option>
                                </select>
                            </dd>
                        </dl>
                    </div>
                    
                    <div class="col-md-2 col-sm-6" >
                        <dl>
                            <dt>Amount</dt>                            
                            <dd id="amount"></dd>
                        </dl>
                    </div>                   
                </div>
                <div class="row">
                <div class="col-xs-12 col-sm-5 col-lg-3" style="margin-bottom:5px;">
                                                   
                                <button type="button"  data-toggle="modal"  data-target="#addvoucher" class="btn btn-warning ">Redeem Voucher</button>
                        
                </div>
                 <div class="col-xs-12 col-sm-4 col-lg-6" style="margin-bottom:5px;">
                <?php if($parent->location=='0'):?>
                    <input type="hidden" id="planexist" value=0/>
                    <input type="hidden" id="idst" name="idst" value="<?php echo base64_encode($student);?>">
                    <input type="hidden" name="username" value="<?php echo $studentdetails->username;?>">
                    <input type="hidden" name="voucherid" id="voucheridhidden" value="">
                    <input type="hidden" name="amount" id="amounthidden" value="">
                    <input type="hidden" name="source" value="parent">
                    <input type="hidden" class="form-control subscripvalid" name="phone"   data-item="Phone Number"   value="<?php echo ($parent->profile->phonenumber==''?'000000000':$parent->profile->phonenumber);?>">
                    <input type="hidden" class="form-control subscripvalid" name="firstname" data-item="First Name" value="<?php echo $parent->profile->firstname;?>">
                    <input type="hidden" class="form-control subscripvalid" name="lastname"  data-item="Last Name" value="<?php echo $parent->profile->lastname;?>">
                    <input type="hidden" class="form-control subscripvalid" name="email"  data-item="Email"  value="<?php echo $parent->email;?>">                                    
                <?php else:?>
                    <input type="hidden" id="planexist" value=0/>
					<input type="hidden" name="no_shipping" value="1" />
                    <input type="hidden" name="cmd" value="_xclick" />
					<input type="hidden" name="no_note" value="1" />
                    <input type="hidden" name="voucherid" id="voucheridhidden" value="">
					<input type="hidden" name="currency_code" id="currency_code" value="" />
					<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
					<input type="hidden" name="name" value="<?php echo $parent->profile->firstname.' '.$parent->profile->lastname;?>"  />					
					<input type="hidden" name="email" value="<?php echo $parent->email;?>"  />					
					<input type="hidden" name="item_name" id="item_name"  value="" / >
					<input type="hidden" name="amount" id="amounthidden" value="">  
                    <input type="hidden" id="idst" name="idst" value="<?php echo base64_encode($student);?>">
                    <input type="hidden" id="studentid" name="studentid" value="<?php echo $student;?>">
                     <input type="hidden" name="username"  id="username" value="<?php echo $studentdetails->username;?>">                                  
					<input type="hidden" name="custom"  id="custom" value="<?php echo base64_encode($id);?>" />
                    <input type="hidden" name="source" value="parent" />
					<input type="hidden" name="stripeBillingName" value="<?php echo $parent->profile->firstname.' '.$parent->profile->lastname;?>"  />
					<input type="hidden" name="stripeEmail" value="<?php echo $parent->email;?>"  />
                    <button id="stripe-button" type="button" class="btn btn-warning  subscripsave" ><span class="glyphicon glyphicon-floppy-save"></span>Pay with Credit/Debit Card</button>
                <?php endif;?>                                                       
                    <button  type="submit"  class="btn btn-warning  subscripsave" ><span class="glyphicon glyphicon-floppy-save"></span><?php echo ($parent->location=='0'?'Subscribe':'Pay with PayPal');?></button> 
                                                                                  
                    </div>
 
                    <div class="col-xs-12 col-sm-3 col-lg-3" style="margin-bottom:5px;">
                                                     
                                <button type="button" class="btn btn-warning  closeform">Cancel</button>
                         
                    </div>
                    
           
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12" style="padding: 0px;padding-left: 15px; margin-top: 15px;">                                                                                              
                            <p class="buttoninfo">Subscribe through secure payment gateway</p>
                            <p class="buttoninfo">Adding a subscription will give you unlimited access to all the Tabbie revision modules enabled for that subscription, for the duration specified.</p>    
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
</div>
<script>
        $('#stripe-button').click(function(){            
            if($('#plan').val()!='' & ($('#amounthidden').val()=='0 INR' || $('#amounthidden').val()=='0 GBP'))
            {                                
                $('.overlay').show();
                $('#loader').show();                                                   
                $('#planform').attr('action', "<?php echo Yii::app()->request->baseUrl.'/parent/PaymentComplete';?>");
                $('#planform').submit();                
            }
            else
            {
                    var handler = StripeCheckout.configure({
                    key: 'pk_live_wcSYngDrinVh1yamt56OA5vB',            
                    locale: 'auto',
                    token: function(token) {
                        
                        var $id = $('<input type=hidden name=stripeToken />').val(token.id);
        
                        $('#planform').attr('action', "<?php echo Yii::app()->request->baseUrl.'/stripe/charge.php';?>");
                        $('.overlay').show();
                        $('#loader').show();                                                                
                        $('#planform').append($id).submit();
                    }
                  });            
                  var amount = ($('#amounthidden').val()*100);
                  amount=amount.toFixed(0);
                  var currency=$('#currency_code').val();
                  handler.open({            
                    amount:amount,            
                    email:'<?php echo $parent->email;?>',
                    name:        'Tabbieme Subscription',
                    //image:       '{% static "img/marketplace.png" %}',
                    //description: 'Purchase Products',
                    panelLabel:  'Subscribe',
                    allowRememberMe:false,
                    currency:currency, 
                    billingAddress:true,           
                  });
        
                  return false;
            }            
        
        });
</script>
    