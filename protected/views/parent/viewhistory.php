<style type="text/css">
    .cls
    {
        position: absolute;
        margin-top: -21px;
        right: 5px;
        background-color: rgb(19, 19, 19);
        opacity: 10;
        color: white;
        font-size: 16px;
        /* padding: 10px; */
        height: 20px;
        line-height: 17px;
        width: 20px;
        text-align: center;
        border-radius: 33px;
    }
</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
        <div class="bs-example">


            <div class="" id="loginModal">
                <div class="modal-header">

                    <h3><?php echo ucfirst($student->profile->firstname);?>: History for plan <?php echo ucfirst($plan->TM_PN_Name);?></h3>
                </div>



</div>
                <div class="modal-body">


                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <?php
                            if($planmodules['revision']):?>
                                <li role="presentation" class="active"><a href="#revisions" aria-controls="revisions" role="tab" data-toggle="tab">Revisions</a></li>
                            <?php
                            endif;
                            if($planmodules['challenge']):?>
                                <li role="presentation" <?php echo (!$planmodules['revision']?'class="active"':'');?>><a href="#challenges" aria-controls="challenges" role="tab" data-toggle="tab">Challenges</a></li>
                            <?php
                            endif;
                            if($planmodules['mock']):?>
                                <li role="presentation" <?php echo (!$planmodules['challenge'] & !$planmodules['revision']?'class="active"':'');?>><a href="#mocks" aria-controls="mocks" role="tab" data-toggle="tab">Mocks</a></li>
                            <?php
                            endif;
                            ?>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <?php
                            if($planmodules['revision']):?>
                            <div role="tabpanel" class="tab-pane fade in active" id="revisions">
                                <div class="panel panel-default">
                                    <div class="tab-pane fade in" id="revisions">
                                        <table class="rwd-tables">
                                            <tbody style=" border-bottom: 1px solid #E0E0E0;">


                                            </tbody>
                                            <table class="rwd-tables">
                                                <tbody>
                                                <tr>
                                                    <th>Topic</th>
                                                    <th>Date</th>
                                                    <th class="scoreth">Score</th>
                                                    <!--<th> </th>
                                                    <th> </th>
                                                    <th> </th>-->
                                                    <!--<th> </th>-->
                                                </tr>
                                                <?php
                                                if(count($totaltests)>0):
                                                    $count=0;
                                                    foreach ($totaltests AS $viewTotaltests):

                                                        ?>

                                                        <tr>
                                                            <td>
                                                                <span class="rwd-tables thead">Topic </span>
															<span class="rwd-tables tbody"><?php
                                                                foreach($viewTotaltests->chapters AS $key=>$chapter):
                                                                    echo ($key!='0'?' ,':'').$chapter->chapters->TM_TP_Name;
                                                                endforeach;?>
															</span>
                                                            </td>



                                                            <td style="  width: 125px;"><span class="rwd-tables thead">Date</span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($viewTotaltests->TM_STU_TT_CreateOn)); ?></span></td>
                                                            <td>
                                                                <div class="pie_progress2 showprogress<?php echo $count;?>" role="progressbar<?php echo $count;?>" data-goal="100"  aria-valuemax="100">
                                                                    <span class="pie_progress2__number"><?php echo $viewTotaltests->TM_STU_TT_Percentage."%"?></span>
                                                                </div>
                                                            </td>
                                                            <!--<td colspan="3"><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><img id="viewsolution" data-toggle="modal" data-target="#solutionModel" class="clickable-row viewsolution" data-ID="<?php /*echo $viewTotaltests->TM_STU_TT_Id*/?>" src="<?php /*echo Yii::app()->request->baseUrl;*/?>/images/solutions.png" class="hvr-pop mCS_img_loaded"></span></td>-->
                                                        </tr>
                                                        <?php
                                                        Yii::app()->clientScript->registerScript('progress'.$count, "
                                                    jQuery(document).ready(function($){
                                                     $('.showprogress".$count."').asPieProgress('go','".round($viewTotaltests->TM_STU_TT_Percentage)."%');

                                                        });
                                                    ");
                                                        $count++;
                                                    endforeach;

                                                else:?>
                                                    <tr>
                                                        <td colspan="5" class="font2" style="text-align: center" align="center">No revisions found </td>
                                                    </tr>
                                                <?php endif;?>
                                                </tbody>
                                            </table>


                                    </div>
                                </div>
                            </div>
                            <?php
                            endif;
                            if($planmodules['challenge']):
                                ?>
                            <div role="tabpanel" class="tab-pane fade <?php echo (!$planmodules['revision']?'in active':'');?>" id="challenges">
                                <div class="panel panel-default">
                                    <div class="tab-pane fade in <?php echo (!$planmodules['revision']?'active':'');?>" id="challenges">
                                        <table class="rwd-tables table-hover table-mc-light-blue">
                                            <tbody>


                                            <tr>
                                                <th><b>Set By</b></th>
                                                <th><b>Set On</b></th>
                                                <th><b>Set For</b></th>
                                                <th><b>My Score</b></th>
                                                <th><b>Position</b></th>
                                            </tr>

                                            <?php
                                            if(count($challenges)>0):
                                            foreach ($challenges AS $key => $viewChallenge):
                                                $testId = $viewChallenge->TM_CE_RE_Chalange_Id;
                                                $challenge = Challenges::model()->findByPk($viewChallenge->TM_CE_RE_Chalange_Id);
                                                $users = User::model()->findByPk($challenge->TM_CL_Chalanger_Id);
                                                $creatorname = $users->username;
                                                $studdetails = Student::model()->find(array('condition' => "TM_STU_User_Id='" . $challenge->TM_CL_Chalanger_Id. "'"));
                                                $firstname =$studdetails->TM_STU_First_Name;

                                                $criteria = new CDbCriteria;
                                                $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $testId);
                                                $criteria->order = 'TM_CE_RE_ObtainedMarks DESC';
                                                $participants = Chalangerecepient::model()->findAll($criteria);
                                                $userids = array();
                                                foreach ($participants AS $party):
                                                    $userids[] = $party->TM_CE_RE_Recepient_Id;
                                                endforeach;


                                                $count = Chalangerecepient::model()->count(
                                                    array(
                                                        'condition' => 'TM_CE_RE_Chalange_Id=:flag',
                                                        'params' => array(':flag' => $testId,)
                                                    ));?>
                                                <tr>
                                                    <td><span class="rwd-tables thead"><b>Set By</b></span><span class="rwd-tables tbody"><img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($studdetails->TM_STU_Image!=''?$studdetails->TM_STU_Image:"noimage.png").'?size=large&type=user&width=60&height=60';?>" class="img-circle tab_img ">
                                                            <?php if ($student->id==$challenge->TM_CL_Chalanger_Id)
                                                            {
                                                                echo $firstname;
                                                                /*echo $student->profile->firstname;*/
                                                            } else {
                                                                echo $firstname;
                                                            }?></span></td>
                                                    <td><span class="rwd-tables thead"><b>Set On</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($challenge->TM_CL_Created_On)); ?></span>
                                                    </td>
                                                    <td><span class="rwd-tables thead"><b>Set For</b></span><span class="rwd-tables tbody"><?php echo $this->GetChallengeInvitees($challenge->TM_CL_Id, $student->id);?></span></td>
                                                    <td><span class="rwd-tables thead"><b>Score</b></span><span class="rwd-tables tbody"><?php echo $viewChallenge->TM_CE_RE_ObtainedMarks?></span>
                                                    </td>
                                                    <td><span class="rwd-tables thead"><b></b></span><span class="rwd-tables tbody">
                                                    <?php  $criteria = new CDbCriteria;
                                                    $criteria->addCondition('TM_CE_RE_Chalange_Id='.$testId);
                                                    $criteria->order='TM_CE_RE_ObtainedMarks DESC';
                                                    $participants = Chalangerecepient::model()->findAll($criteria);
                                                    $recepients=count($participants);
                                                    ?>
                                                    <?php echo((count($participants))==1 ? "NA" : $this->GetChallengePosition($userids, $student->id)) ;?><a href="#"><span class="rwd-tables thead"><b>Position</b></span></a></span></td>
                                                </tr>
                                            <?php endforeach;
                                            else:?>
                                                <tr>
                                                    <td colspan="5" class="font2" style="text-align: center" align="center">No challenges found</td>
                                                </tr>
                                            <?php endif;?>
                                            </tbody>


                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php
                            endif;
                            if($planmodules['mock']):?>
                                <div role="tabpanel" class="tab-pane fade <?php echo (!$planmodules['revision'] & !$planmodules['challenge']?'in active':'');?>" id="mocks">
                                    <div class="panel panel-default">
                                        <div class="tab-pane fade in" id="mocks">
                                            <table class="rwd-tables">
                                                <tbody style=" border-bottom: 1px solid #E0E0E0;">


                                                </tbody>
                                                <table class="rwd-tables">
                                                    <tbody>
                                                    <tr>
                                                        <th>Mock</th>
                                                        <th>Date</th>
                                                        <th class="scoreth">Score</th>

                                                        <!--<th> </th>-->
                                                    </tr>
                                                    <?php
                                                    if(count($totalmocks)>0):
                                                        $count=0;
                                                        foreach ($totalmocks AS $viewTotaltests):

                                                            ?>

                                                            <tr>
                                                                <td>
                                                                    <span class="rwd-tables thead">Mock </span>
															<span class="rwd-tables tbody"><?php echo $viewTotaltests->mocks->TM_MK_Name;?>
															</span>
                                                                </td>



                                                                <td style="  width: 125px;"><span class="rwd-tables thead">Date</span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($viewTotaltests->TM_STMK_Date)); ?></span></td>
                                                               <td>
                                                                    <div class="pie_progress2 showprogress<?php echo $count;?>" role="progressbar<?php echo $count;?>" data-goal="100"  aria-valuemax="100">
                                                                        <span class="pie_progress2__number"><?php echo $viewTotaltests->TM_STMK_Percentage."%"?></span>
                                                                    </div>
                                                                </td>
                                                                <!--<td colspan="3"><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><img id="viewsolution" data-toggle="modal" data-target="#solutionModel" class="clickable-row viewsolution" data-ID="<?php /*echo $viewTotaltests->TM_STMK_Id*/?>" src="<?php /*echo Yii::app()->request->baseUrl;*/?>/images/solutions.png" class="hvr-pop mCS_img_loaded"></span></td>-->
                                                            </tr>
                                                            <?php
                                                            Yii::app()->clientScript->registerScript('progress'.$count, "
                                                    jQuery(document).ready(function($){
                                                     $('.showprogress".$count."').asPieProgress('go','".round($viewTotaltests->TM_STMK_Percentage)."%');

                                                        });
                                                    ");
                                                            $count++;
                                                        endforeach;

                                                    else:?>
                                                        <tr>
                                                            <td colspan="5" class="font2" style="text-align: center" align="center">No mocks found </td>
                                                        </tr>
                                                    <?php endif;?>
                                                    </tbody>
                                                </table>


                                        </div>
                                    </div>
                                </div>
                            <?php
                            endif;
                            ?>




                        </div>

                    </div>


                </div>
            </div>


        </div>


    </div>
<div id="myinvite" class="modal fade" role="dialog" style="padding-top:20%;">
  <div class="modal-dialog modal-sm modal-sm_plus">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Challenge position</h4>
      </div>
      <div class="modal-body modal-body_padding">
      <div class="row">
        <ul class='popu-cart' role='menu' style='display: block !important;'>                  
            </ul>
            </div>
      </div>
    </div>

  </div>
</div>

<div class="modal fade " id="solutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>
            </div>
            <div class="panel-heading">
                <div style="text-align: right;">
                    <span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('student/getpdfanswer',array('id'=>$test->TM_STU_TT_Id));?>" target="_blank"><i class="fa fa-print fa-lg"></i></a>

                </div>
            </div>
            <div class="modal-body" >
                <table class="table ">
                    <thead>

                    </thead>
                    <tbody class="font3 wirisfont" id="showsolution">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(document).on('click','.hover',function(){
        $('.popu-cart').html(''); 
        var id=$(this).attr('data-id');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/parent/GetChallengeBuddies";?>',
                data: {challenge:id},
                success: function(data){
                    if(data!='no')
                    {
                        $('.popu-cart').html(data);                        
                    }
                }
            });
    });
    $(document).ready(function () {
        $(".rwd-tables table, .table.rwd-tables").rwdTables();
    });
</script>
<script type="text/javascript">
    $(document).on('click','.hover',function(){
        var id=$(this).attr('data-id');
        var tooltip=$(this).attr('data-tooltip');
        $('.tooltiptable').hide();
        $('#tooltip'+id).fadeIn();
    });
    $(document).on('click','#close',function(){
        $('.tooltiptable').hide();

    });
    jQuery(document).ready(function($){
        $('.pie_progress2').asPieProgress({
            namespace: 'pie_progress2'
        });

    });
</script>

<?php Yii::app()->clientScript->registerScript('viewhistoryredo', "
$('.redochange').change(function(){

    var item=$(this).attr('data-item');

    $('#redohidden'+item).val($(this).val());
});
$('.viewsolution').click(function(){
var testid=$(this).attr('data-ID');
$.ajax({
type: 'POST',
url: '".Yii::app()->createUrl('parent/GetSolution')."',
data: { testid: testid,student:".$student->id."}
})
.done(function( html ) {
$('#showsolution').html( html );
});
});

");?>
<?php Yii::app()->clientScript->registerScript('sendmail', "
    $(document).on('click','.showSolutionItem',function(){
            $('.sendmail').slideUp('fast');
            $(this).next('.sendmail').slideDown('slow')
    });

    $(document).on('click','#mailSend',function(){
var comments= $('#comments').val();

var questionId=$(this).data('id');

            var questionReff=$(this).data('reff');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {  alert('Message Sens');$('.sendmail').slideUp('fast'); })

    });
");?>
<?php Yii::app()->clientScript->registerScript('sendmailtest', "
    $(document).on('click','.showSolutionItemtest',function(){
            var item= $(this).attr('data-reff');
            $('.sendmailtest').slideUp('fast');
            $('.suggestion'+item).slideDown('slow')
    });

    $(document).on('click','#mailSendtest',function(){
var comments= $('#comments').val();

var questionId=$(this).data('id');

            var questionReff=$(this).data('reff');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        { alert('Message Sens'); $('.sendmailtest').slideUp('fast'); })

    });
");
?>