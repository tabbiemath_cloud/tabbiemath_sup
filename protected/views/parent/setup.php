<script src="https://checkout.stripe.com/checkout.js"></script> 
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
        <?php if(Yii::app()->user->hasFlash('subscriptionsuccess')): ?>
            <div class="alert alert-success" role="alert" style="margin-top: 52px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
            <?php echo Yii::app()->user->getFlash('subscriptionsuccess'); ?></div>
        <?php endif;?>
        <?php if(Yii::app()->user->hasFlash('subscriptionfail')): ?>
            <div class="alert alert-danger" role="alert" style="margin-top: 52px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
            <?php echo Yii::app()->user->getFlash('subscriptionfail'); ?></div>
        <?php endif;?>
        <h1 class="h125">My Details</h1>
        <a href="javascript:void(0);" class="pull-right editr" id="edit" ><span class="glyphicon glyphicon-pencil"></span>Edit</a>
        <a href="javascript:void(0);" class="pull-right editr" id="save" style="display: none;" ><span class="glyphicon glyphicon-floppy-save"></span>Save</a>
        <a href="javascript:void(0);" class="pull-right editr" id="canceledit" style="display: none;margin-right:10px;" ><span class="glyphicon glyphicon-remove"></span>Cancel</a>
        <div class="bs-example"  id="parentdiv">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">

                        <div class="col-md-1 col-sm-12" style="padding-left: 0;">                            
                              <div class="oldpic imagespan"">
                            <ul class="upimg">


                                    <li class="deletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageTEXTDiv" data-file="Questions_TM_QN_ImageTEXT" hidden ><button type="button" data-toggle="modal" data-target="#myModal" data-backdrop="static" class="btn btn-warning btn-circle deletespan"><i class="fa fa-camera"></i></button></li>


                            </ul>
                            <?php if($parent->profile->image!=''): ?>
                                <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($parent->profile->image!=''?$parent->profile->image:"noimage.png").'?size=large&type=user&width=140&height=140';?>" class="img-circle tab_img oldpicimg" style="margin-right: 26px;">
                                <img class="loadpic" src="" hidden>
                            <?php else: ?>
                                <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($parent->profile->image!=''?$parent->profile->image:"noimage.png").'?size=large&type=user&width=140&height=140';?>" class="img-circle tab_img " style="margin-right: 26px;">

                            <?php endif ?>
                        </div>
                        </div>
						<div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close closepreofpic" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Change Profile Pic</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form   id="imageupdate" enctype="multipart/form-data">

                                        <input type="file"  name="photoimg" accept="image/*"  data-type="profpic" data-section="main" data-name="profpic"  id="photoimg" class="imageselect profpic">

                                            </form>
                                        <div class="preview " style="display: none;">
                                            <ul class="upimg">
                                                <form   id="imagesave">
                                                    <input type="hidden" id="nameimg" name="nameimg"  value="" hidden>
                                                    <li class="savespan " data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageTEXTDiv" data-file="uestions_TM_QN_ImageTEXT"  > </a></li>
                                                    <img  class="img-circle tab_img newpic" id="newimg" style="margin-right: 26px;" src="">

                                                </form>
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        
										<div class="pull-left errorupload"> Maximum allowed size is 2Mb </div>
                                        
                                        <button type="button" class="btn btn-warning  savespan" data-dismiss="modal" style="display: none">Save changes</button>
										<button class="btn btn-default closemodalparent">Cancel</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6" style="/* margin-top: 31px; */">
                            <dl class="mbtm">
                                <dd ><b>First Name</b></dd>
                                <dt><h5 id="firstname"><?php echo $parent->profile->firstname;?></h5></dt>
                                <input type="text" class="form-control" id="editfirstname" name="firstname" maxlength="20" value="<?php echo $parent->profile->firstname;?>" style="display: none;">
                            </dl>
                        </div>

                        <div class="col-md-2 col-sm-6" style=" /* margin-top: 31px; */">
                            <dl class="mbtm">

                                <dd ><b>Last Name</b></dd>
                                <h5 id="lastname"><?php echo $parent->profile->lastname;?></h5>
                                <input type="text" class="form-control" id="editlastname" name="lastname" maxlength="20" value="<?php echo $parent->profile->lastname;?>" style="display: none;">
                            </dl>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <dl class="mbtm">
                                <dd id="lastname"><b>Email</b></dd>
                                <dt><h5><?php echo $parent->email;?></h5></dt>
                            </dl>
                        </div>

                        <div class="col-md-2 col-sm-6">
                            <dl class="mbtm">
                                <dd id="lastname"><b>Password</b></dd>
                                <dt>
                                    <a href="javascript:void(0);" data-id="<?php echo $parent->id;?>" data-type="parent" data-parent="parentdiv" class="changepassword"><h5  class="showtooltip" data-toggle="tooltip" data-placement="left" title="" data-original-title="Click to change your password." >xxxxxxxx</h5></a>
                                    </dt>
                                <input type="text" class="form-control" id="editfirstname" name="firstname" maxlength="20" value="Parent" style="display: none;">
                            </dl>
                        </div>

                        <!--<div class="col-md-3 col-sm-6">
                            <dl class="mbtm">
                                <dd id="lastname"><b>Email</b></dd>
                                <dt><h5><?php //echo $parent->email;?></h5></dt>
                            </dl>
                        </div>-->
                    </div>



                </div>

            </div>
        </div>
        <div class="bs-example padbtm">
            <div class="row">
                <div class="col-md-4 col-xs-6 ">
                    <h5 class="hedld">My Student Details</h5>
                </div>
                <div class="col-md-3 margin2">
                    <a href="javascript:void(0);" class="addnwe" data-id="<?php echo $studentdetails->profile->user_id;?>"  id="addstudent">Add new student</a>
                </div>
            </div>
            <div class="bs-example">
                <div  class="my_student-panel panel panel-default hidemealways" id="newstudent"></div>
                <div  class="my_student-panel panel panel-default hidemealways" id="newplan"></div>
                <?php foreach($students AS $key=>$student):
                $studentdetails=User::model()->findByPk($student->TM_STU_User_Id);                
                $subscriptioncount=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId=:student','params'=>array(':student'=>$student->TM_STU_User_Id)));
                $activesubscriptions=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId=:student AND TM_SPN_Status=0','params'=>array(':student'=>$student->TM_STU_User_Id)));
                $inactivesubscriptions=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId=:student AND TM_SPN_Status=1','params'=>array(':student'=>$student->TM_STU_User_Id)));
                ?>
                <div class="my_student-panel panel panel-default" id="studentraw<?php echo $student->TM_STU_User_Id;?>">
                    <div class="panel-heading headdetail">
                        <div class="row">
                            <div class="col-md-6 col-xs-5 ">
                                <h5 class="hedld"><b id="studenthead<?php echo $student->TM_STU_User_Id;?>"><?php echo $studentdetails->profile->firstname.' '.$studentdetails->profile->lastname;?></b></h5>
                            </div>

                            <div class="col-md-6 col-xs-7 ">
                                <div class="btn-group pull-right" style=" margin-top: 7px;"></div>
                                <?php if(count($activesubscriptions)==0):?>
                                <a href="javascript:void(0);" class="pull-right lineu removestud" data-id="<?php echo $studentdetails->profile->user_id;?>" id="removestud<?php echo $studentdetails->profile->user_id;?>"  style="margin-top: 13px;">Remove</a>
                                <?php endif;?>                                                                
                                <a href="javascript:void(0);" class="pull-right lineu studentsave" data-id="<?php echo $studentdetails->profile->user_id;?>"  id="studentsave<?php echo $studentdetails->profile->user_id;?>" style="margin-right: 25px; margin-top: 13px;display: none;"><span class="glyphicon glyphicon-floppy-save"></span>Save</a>                                 
                                <a href="javascript:void(0);" class="pull-right lineu studentcancel" data-id="<?php echo $studentdetails->profile->user_id;?>"  id="studentcancel<?php echo $studentdetails->profile->user_id;?>" style="margin-right: 15px; margin-top: 13px;display: none;"><span class="glyphicon glyphicon-remove"></span>Cancel</a>                                                               
                                <a href="javascript:void(0);" class="pull-right lineu studentedit" data-id="<?php echo $studentdetails->profile->user_id;?>" id="studentedit<?php echo $studentdetails->profile->user_id;?>" style="margin-right: 25px; margin-top: 13px;"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
                                <a href="javascript:void(0);" class="pull-right lineu changepassword" data-id="<?php echo $studentdetails->id;?>" data-type="student" data-parent="studentraw<?php echo $student->TM_STU_User_Id;?>" id="hidepassword<?php echo $studentdetails->profile->user_id;?>"  style="margin-right: 25px; margin-right: 15px;margin-top: 13px;">Change password</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-1 col-sm-12" style="padding-left: 0;">

                                <div class="imagespanchild oldpicchild"">
                                <ul class="upimg">


                                    <li class="deletespanchild" data-id="<?php echo $studentdetails->profile->user_id;?>"  hidden ><button type="button"   class="btn btn-warning btn-circle deletespanchild" data-backdrop="static" data-toggle="modal" data-target="#myModalChild<?php echo $studentdetails->profile->user_id;?>"><i class="fa fa-camera"></i></button></li>


                                </ul>
                                <?php if($parent->profile->image!=''): ?>
                                    <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($student->TM_STU_Image!=''?$student->TM_STU_Image:"noimage.png").'?size=large&type=user&width=60&height=60';?>" class="img-circle tab_img oldpicimgchild<?php echo $studentdetails->profile->user_id;?>">
                                <?php else: ?>
                                    <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($student->TM_STU_Image!=''?$student->TM_STU_Image:"noimage.png").'?size=large&type=user&width=60&height=60';?>" class="img-circle tab_img oldpicimgchild<?php echo $studentdetails->profile->user_id;?>">

                                <?php endif ?>
                            </div>

                            </div>
							 <div id="myModalChild<?php echo $studentdetails->profile->user_id;?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close closepreofpicchild" data-id="<?php echo $studentdetails->profile->user_id;?>" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Change Profile Pic</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form   id="imageupdatechild<?php echo $studentdetails->profile->user_id;?>">
                                                <input type="hidden" name="studId" value="<?php echo $studentdetails->profile->user_id;?>">
                                                <input type="file" accept="image/*"  name="photoimgchild"  data-name="<?php echo $studentdetails->profile->user_id;?>"  data-id="<?php echo $studentdetails->profile->user_id;?>" id="photoimgchild<?php echo $studentdetails->profile->user_id;?>" class="childimg">
                                            </form>
                                            <div class="previewchild<?php echo $studentdetails->profile->user_id;?>" hidden>
                                                <ul class="upimg">
                                                    <form   id="imagesavechild<?php echo $studentdetails->profile->user_id;?>">
                                                        <input type="hidden" id="nameimgchild<?php echo $studentdetails->profile->user_id;?>" name="nameimgchild"  value="" hidden>

                                                        <img  class="img-circle tab_img newpic" id="newimgchild<?php echo $studentdetails->profile->user_id;?>" style="margin-right: 26px;" src="">

                                                    </form>
                                                </ul>

                                            </div>
                                        </div>
                                        <div class="modal-footer">
										<div class="pull-left errorupload"> Maximum allowed size is 2Mb </div>
                                            
                                            <button type="button" class="btn btn-warning  savespanchild" data-id="<?php echo $studentdetails->profile->user_id;?>" data-dismiss="modal" style="display:none;">Save changes</button>
											<button class="btn btn-default closemodalchild">Cancel</button>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="col-md-2 col-sm-6">
                                <dl>
                                    <dt>First Name</dt>
                                    <dd id="studfirstname<?php echo $studentdetails->profile->user_id;?>"><?php echo $studentdetails->profile->firstname;?></dd>
                                    <input type="hidden" id="studentid<?php echo $studentdetails->profile->user_id;?>" value="<?php echo $studentdetails->profile->user_id;?>">
                                    <input type="text" class="form-control"id="editstudfirstname<?php echo $studentdetails->profile->user_id;?>" maxlength="15" name="studfirstname" value="<?php echo $studentdetails->profile->firstname;?>" style="display: none;">
                                </dl>

                            </div>
                            <div class="col-md-2 col-sm-6">
                                <dl>


                                    <dt>Last Name</dt>
                                    <dd id="studlastname<?php echo $studentdetails->profile->user_id;?>"><?php echo $studentdetails->profile->lastname;?></dd>
                                    <input type="text" class="form-control" id="editstudlastname<?php echo $studentdetails->profile->user_id;?>" maxlength="15" name="studlastname" value="<?php echo $studentdetails->profile->lastname;?>" style="display: none;">

                                </dl>

                            </div>
                            <div class="col-md-2 col-sm-6">
                                <dl>
                                    <dt>User Name</dt>
                                    <dd><?php echo $studentdetails->username;?></dd>
                                </dl>
                            </div>

                            <div class="col-md-2 col-sm-6">
                                <dl>
                                    <dt>Password</dt>
                                    <dd>
                                        <a href="javascript:void(0);" class="showtooltip showpassword" id="showpassword<?php echo $studentdetails->profile->user_id;?>" data-student="<?php echo $student->TM_STU_Id;?>" data-toggle="tooltip" data-placement="bottom" title="Show/Hide password">
                                            <span id="maskpassword<?php echo $student->TM_STU_Id;?>">xxxxxxxx</span>
                                            <span class="unmaskpassword" id="unmaskpassword<?php echo $student->TM_STU_Id;?>"><?php echo $student->TM_STU_Password;?></span>
                                        </a>
                                        <a href="javascript:void(0);" style="display: none;"  data-id="<?php echo $studentdetails->id;?>" data-type="student" data-parent="studentraw<?php echo $student->TM_STU_User_Id;?>" class="changepassword" id="hidepassword<?php echo $studentdetails->profile->user_id;?>" > Change password</a>
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <dl>


                                    <dt>School</dt>
                                    <dd id="studentschool<?php echo $studentdetails->profile->user_id;?>"><?php echo ($student->TM_STU_School=='0'?$student->TM_STU_SchoolCustomname:School::model()->findByPk($student->TM_STU_School)->TM_SCL_Name) ;?></dd>
                                    <select name="studschool" style="display: none;" onchange="Shoschooltext(<?php echo $studentdetails->profile->user_id;?>)" id="editstudentschool<?php echo $studentdetails->profile->user_id;?>" class="form-control" data-item="School">
                                        <?php 
                                            $data=CHtml::listData(School::model()->findAll(array('condition' => 'TM_SCL_Country_Id='.$student->TM_STU_Country_Id,'order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');
                                            
                                            echo CHtml::tag('option',
                                                           array('value'=>''),'Select School',true);
                                            foreach($data as $value=>$name)
                                            {
                                                if($student->TM_STU_School==$value):
                                                    $selected=array('value'=>$value,'selected'=>'selected');              
                                                else:
                                                    $selected=array('value'=>$value);
                                                endif;
                                                echo CHtml::tag('option',
                                                           $selected,CHtml::encode($name),true);
                                            }
                                            if($student->TM_STU_School=='0'):
                                                $optionval=array('value'=>'0','selected'=>'selected');
                                            else:
                                                $optionval=array('value'=>'0');
                                            endif;        
                                            echo CHtml::tag('option',
                                               $optionval,'Other',true);                                        
                                        ?>                                                               
                                    </select>                                    
                                    <input type="text" class="form-control" id="editstudentschoolother<?php echo $studentdetails->profile->user_id;?>" maxlength="15" name="studschoolother" value="<?php echo $student->TM_STU_SchoolCustomname;?>" style="display: none;">

                                </dl>

                            </div>                            
                        </div>
                    </div>

                    <div class="panel-body panalb_padno" style="padding: 0;">
                        <div class="panel-heading" style=" background-color: rgb(242, 242, 242);/* border-top: 1px solid #E8E8E8; */ border-radius: 0; padding-bottom: 0; padding-top: 0;">
                            <div class="row">
                                <div class="col-md-3 col-xs-6 ">
                                    <h5>
                                        <b>Active Subscription</b>
                                    </h5>
                                </div>
                                <div class="col-md-3" style="padding-left: 0;padding-top: 0px;">
                                    <a href="javascript:void(0)" id="addnwe<?php echo $student->TM_STU_User_Id;?>"  data-student="<?php echo $student->TM_STU_User_Id;?>"  class="addplan addnwe">Add new subscription</a>
                                </div>
                            </div>                            

                        </div>

                        <table class="rwd-tables padlft25">
                            <tbody class="font2">
                            <?php if(count($activesubscriptions)>0):
                                foreach($activesubscriptions AS $activesubscription):?>
                                <tr style="background-color: none !important;" id="active<?php echo $studentdetails->profile->user_id;?>">
                                    <td ><?php echo $activesubscription->syllabus->TM_SB_Name;?></td>
                                    <td ><?php echo $activesubscription->standard->TM_SD_Name;?></td>
                                    <td ><?php echo $activesubscription->plan->TM_PN_Name;?></td>
                                    <td >Expires : <?php echo date("d-m-Y",mktime(0, 0, 0, date("m",strtotime($activesubscription->TM_SPN_ExpieryDate)),date("d",strtotime($activesubscription->TM_SPN_ExpieryDate)),date("Y",strtotime($activesubscription->TM_SPN_ExpieryDate)))); ?></td>
                                    <td >
                                        <?php if($activesubscription->plan->TM_PN_Type=='0'):?>
                                        <ul class="navb">
                                            <li><a href="<?php echo Yii::app()->createUrl('parent/studentprogress',array('id'=>$studentdetails->profile->user_id,'plan'=>$activesubscription->plan->TM_PN_Id));?>"><span class="glyphicon glyphicon-eye-open"></span>View Progress</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('parent/studentactivity',array('id'=>$studentdetails->profile->user_id,'plan'=>$activesubscription->plan->TM_PN_Id));?>"><span class="glyphicon glyphicon-copy"></span>View Open Tasks</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('parent/studenthistory',array('id'=>$studentdetails->profile->user_id,'plan'=>$activesubscription->plan->TM_PN_Id));?>"><span class="glyphicon glyphicon-list-alt"></span>View Completed</a></li>
                                        </ul>
                                        <?php elseif($activesubscription->plan->TM_PN_Type=='1'):?>
                                        <ul class="navb">
                                            <li><a href="<?php echo Yii::app()->createUrl('parent/Studentschoolprogress',array('id'=>$studentdetails->profile->user_id,'plan'=>$activesubscription->plan->TM_PN_Id));?>"><span class="glyphicon glyphicon-eye-open"></span>View Progress</a></li>
                                            <!--<li><a href="<?php echo Yii::app()->createUrl('parent/studentactivity',array('id'=>$studentdetails->profile->user_id,'plan'=>$activesubscription->plan->TM_PN_Id));?>"><span class="glyphicon glyphicon-copy"></span>View Open Homeworks</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('parent/studenthistory',array('id'=>$studentdetails->profile->user_id,'plan'=>$activesubscription->plan->TM_PN_Id));?>"><span class="glyphicon glyphicon-list-alt"></span>View Completed Homeworks</a></li>-->
                                        </ul>
                                        <?php endif;?>                                        
                                    </td>
                                    <td data-title="Link" style="text-align: right;">
                                    <?php if($activesubscription->plan->TM_PN_Type=='0'):
                                            $difference=$this->GetDateDifference($date1 = date("Y-m-d",mktime(0, 0, 0, date("Y-m-d",strtotime($activesubscription->TM_SPN_CreatedOn)))),$date2 = date("Y-m-d"));
                                    ?>
                                        <a style="margin-top: 13px;font-size:13px;" id="save" class="unsubscribe" href="javascript:void(0);" data-toggle="modal" data-difference="<?php echo $difference;?>" data-id="<?php echo $activesubscription->TM_SPN_Id;?>" data-target="#removesubsc">Cancel Subscription</a>
                                    <?php endif;?>
                                    </td>



                                </tr>
                            <?php
                                endforeach;
                            else:?>
                                <tr  style="background-color: none !important;"><td>
								<?php
                                $subcount=count($activesubscriptions);
                                if($subcount==0):?>
                                <div class="col-md-12" style="padding-left: 0;text-align:center;">
                                <span class="label label-danger"> User will be activated when you add a subscription</span><br>No subscriptions found. 
                                </div>
                                <?php endif;?> 
								</td></tr>
                            <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                    <?php if(count($inactivesubscriptions)>0):?>
                    <div class="panel-body panalb_padno">
                        <div class="panel-heading" style=" background-color: rgb(242, 242, 242);/* border-top: 1px solid #E8E8E8; */ border-radius: 0; padding-bottom: 0; padding-top: 0;">
                            <div class="row">
                                <div class="col-md-3 col-xs-6 ">
                                    <h5><b>Inactive Subscription</b></h5>
                                </div>


                                <div class="col-md-3" style="padding-left: 0;">
                                    <!--<a href="#" target="_blank" class="addnwe">Add new subscription</a>-->
                                </div>

                            </div>
                        </div>

                        <table class="table" id="table">
                            <thead>

                            </thead>
                            <tbody class="font2">
                        <?php foreach($inactivesubscriptions AS $inactivesubscription):?>
                            <tr id="inactive<?php echo $studentdetails->profile->user_id;?>" style=" background-color: none !important;">
                                <td><?php echo $inactivesubscription->syllabus->TM_SB_Name;?></td>
                                <td><?php echo $inactivesubscription->standard->TM_SD_Name;?></td>
                                <td><?php echo $inactivesubscription->plan->TM_PN_Name;?></td>
                                <td>
                                <?php 
                                    $expirydate=date("d-m-Y",mktime(0, 0, 0, date("m",strtotime($inactivesubscription->TM_SPN_ExpieryDate)),   date("d",strtotime($inactivesubscription->TM_SPN_ExpieryDate)),   date("Y",strtotime($inactivesubscription->TM_SPN_ExpieryDate))));
                                    $canceleddate=date("d-m-Y",mktime(0, 0, 0, date("m",strtotime($inactivesubscription->TM_SPN_CancelDate)),   date("d",strtotime($inactivesubscription->TM_SPN_CancelDate)),   date("Y",strtotime($inactivesubscription->TM_SPN_CancelDate))));
                                    if($inactivesubscription->TM_SPN_CancelDate!='0000-00-00'):
                                        echo 'Cancelled : '.$canceleddate;
                                    else:
                                        echo 'Expired :'.$expirydate;
                                    endif;
                                    ?>                                
                                <td>
                                    <ul class="navb">
                                        <li><a href="<?php echo Yii::app()->createUrl('parent/studenthistory',array('id'=>$studentdetails->profile->user_id,'plan'=>$inactivesubscription->plan->TM_PN_Id));?>" class="disabled"><span class="glyphicon glyphicon-list-alt"></span>View Completed</a></li>
                                    </ul>
                                </td>
								<td data-title="Link" style="text-align: right;">
<?php 
        $difference=$this->GetDateDifference($date1 = date("Y-m-d",mktime(0, 0, 0, date("Y-m-d",strtotime($activesubscription->TM_SPN_CreatedOn)))),$date2 = date("Y-m-d"));
?>
                                        <a style="margin-top: 13px;visibility:hidden;	" id="save" class="unsubscribe" href="javascript:void(0);" data-toggle="modal" data-difference="<?php echo $difference;?>" data-id="<?php echo $activesubscription->TM_SPN_Id;?>" data-target="#removesubsc">
                                            Cancel Subscription</a>

								</td>
                            </tr>

                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    <?php endif;?>
                </div>
                <?php endforeach;?>
            </div>
        </div>



    </div>


    <div class="modal fade bs-example-modal-lg" id="removesubsc" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="mySmallModalLabel">Cancel Subscription</h4>
                </div>
                <div class="modal-body">
                    We will be sorry to see you leave.<br>
                    Please refer to our cancellation policy to check if you will be eligible for a refund. <br>
                    If you want to cancel, please press CONTINUE.<br>
                    click here for <label class="sub_line"> <a style="color: #ffa900" href="http://www.tabbiemath.com/cancellation-refunds-terms-conditions/" target="_blank">Cancellation policy</a></label>
                </div>
                <div class="modal-footer">
                    <form action="<?php echo Yii::app()->request->baseUrl.'/parent/cancelsubscription';?>" method="post">
                        <input type="hidden" name="subscription" id="cancelstudent" value="<?php echo ''?>">
                        <button type="submit" class="btn btn-warning">CONTINUE</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Do not Cancel</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->

        </div>

    </div>
    <div class="modal fade bs-example-modal-lg" id="addvoucher" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="mySmallModalLabel">Redeem Voucher</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <dl>
                                <dt>Enter voucher code</dt>   
                                <dd>                                
                                    <input id="voucher" name="voucher" class="form-control voucher"  data-item="Voucher"/>
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-4 col-sm-6 vouchererror">
                        
                        </div>
                    </div>
                </div>
                <div class="modal-footer">                                            
                        <button type="button" class="btn btn-warning redeem">Redeem</button>
                        <button type="button" class="btn btn-warning vouchercancel" data-dismiss="modal">Cancel</button>                    
                </div>
            </div><!-- /.modal-content -->

        </div>

    </div>

</div>


<script type="text/javascript">
    $('#edit').click(function(){
        $('#firstname').toggle();
        $('#editfirstname').toggle();
        $('#lastname').toggle();
        $('#editlastname').toggle();
        $('#save').toggle();
        $('#canceledit').toggle();        
        $('#edit').toggle();
    });
    $('#canceledit').click(function(){
        $('#firstname').toggle();
        $('#editfirstname').val($('#firstname').text()).toggle();
        $('#lastname').toggle();
        $('#editlastname').val($('#lastname').text()).toggle();
        $('#save').toggle();
        $('#canceledit').toggle();        
        $('#edit').toggle();
        $('.error').remove();
        $('#editfirstname').removeClass('errorborder');
        $('#editlastname').removeClass('errorborder');
    })
    $(document).on('click','.unsubscribe',function(){
        $('#cancelstudent').val($(this).attr('data-id'));
        var difference=$(this).attr('data-difference');


    });
    $('#save').click(function(){
        var firstname=$('#editfirstname').val();
        var lastname=$('#editlastname').val();
        var error=0;        
        var regex = /^[a-zA-Z�`']+$/;        
        $('.error').remove();
        if(firstname=='')
        {
            $('#editfirstname').addClass('errorborder');
            $('#editfirstname').after( '<div class=\"error alert alert-danger\">Enter First Name</div>' );
            error=1;
        }
        else if(!regex.test(firstname))
        {
            $('#editfirstname').addClass('errorborder');
            $('#editfirstname').after( '<div class=\"error alert alert-danger\">Special characters not allowed</div>' );
            error=1;
        }
        else
        {
            $('#editfirstname').removeClass('errorborder');
        }
        if(lastname=='')
        {
            $('#editlastname').addClass('errorborder');
            $('#editlastname').after( '<div class=\"error alert alert-danger\">Enter Last Name</div>' );
            error=1;
        }
        else if(!regex.test(lastname))
        {
            $('#editlastname').addClass('errorborder');
            $('#editlastname').after( '<div class=\"error alert alert-danger\">Special characters not allowed</div>' );
            error=1;
        }
        else
        {
            $('#editlastname').removeClass('errorborder');
        }
        if(error==0)
        {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl."/parent/update";?>',
            dataType:'json',
            data: { firstname:firstname,lastname:lastname},
            success: function(data){
                if(data.error=='No')
                {
                    $('#firstname').text(data.firstname);
                    $('#lastname').text(data.lastname);
                    $('#firstname').toggle();
                    $('#editfirstname').toggle();
                    $('#lastname').toggle();
                    $('#editlastname').toggle();
                    $('#save').toggle();
                    $('#edit').toggle();
                    $('#canceledit').toggle();
                    $('.outter h1').text(data.firstname);
                }
                else
                {
                    alert(data.errortext);
                }
            }
        });
        }

    });
    $('.studentedit').click(function(){
        var studid=$(this).attr('data-id');
        $('#studfirstname'+studid).toggle();
        $('#editstudfirstname'+studid).toggle();
        $('#studlastname'+studid).toggle();
        $('#editstudlastname'+studid).toggle();
        $('#studentsave'+studid).toggle();
        $('#studentcancel'+studid).toggle();        
        $('#studentedit'+studid).toggle();
        $('#hidepassword'+studid).toggle();
        $('#studentschool'+studid).toggle();
        $('#editstudentschool'+studid).toggle();
        if($('#editstudentschool'+studid).val()=='0')
        {
            $('#editstudentschoolother'+studid).show();            
        }                        
        $('.formdiv').remove();        
    });    
    $('.studentcancel').click(function(){
        var studid=$(this).attr('data-id');
        $('#studfirstname'+studid).toggle();
        $('#editstudfirstname'+studid).val($('#studfirstname'+studid).text()).toggle();
        $('#studlastname'+studid).toggle();
        $('#editstudlastname'+studid).val($('#studlastname'+studid).text()).toggle();
        $('#studentsave'+studid).toggle();
        $('#studentcancel'+studid).toggle();        
        $('#studentedit'+studid).toggle();  
        $('#editstudfirstname'+studid).removeClass('errorborder');
        $('#editstudlastname'+studid).removeClass('errorborder');
        $('#hidepassword'+studid).toggle();
        $('#studentschool'+studid).toggle();
        $('#editstudentschool'+studid).toggle();
        if($('#editstudentschool'+studid).val()=='0')
        {
            $('#editstudentschoolother'+studid).hide();            
        }        
        $('.error').remove();
    });
    function Shoschooltext(student)
    {        
        if($('#editstudentschool'+student).val()=='0')
        {
            $('#editstudentschoolother'+student).show();            
        } 
        else
        {
            $('#editstudentschoolother'+student).hide();
        }        
    }
    $('.studentsave').click(function(){
        var studid=$(this).attr('data-id');
        var firstname=$('#editstudfirstname'+studid).val();
        var lastname=$('#editstudlastname'+studid).val();
        var school=$('#editstudentschool'+studid).val();
        var schoolother=$('#editstudentschoolother'+studid).val();
        var error=0;
        var regexs = /^[a-zA-Z�`']+$/;
        $('.error').remove();
        if(firstname=='')
        {
            $('#editstudfirstname'+studid).addClass('errorborder');
            $('#editstudfirstname'+studid).after( '<div class=\"error alert alert-danger\">Enter First Name</div>' );
            error=1;
        }
        else if(!regexs.test(firstname))
        {
            $('#editstudfirstname'+studid).addClass('errorborder');
            $('#editstudfirstname'+studid).after( '<div class=\"error alert alert-danger\">Special characters not allowed</div>' );
            error=1;
        }        
        else
        {
             $('#editstudfirstname'+studid).removeClass('errorborder');
        }
        if(school=='')
        {
            $('#editstudentschool'+studid).addClass('errorborder');
            $('#editstudentschool'+studid).after( '<div class=\"error alert alert-danger\">Select School</div>' );
            error=1;            
        }
        else
        {
           $('#editstudentschool'+studid).removeClass('errorborder'); 
        }
        if(school=='0' & schoolother=='')
        {
            $('#editstudentschoolother'+studid).addClass('errorborder');
            $('#editstudentschoolother'+studid).after( '<div class=\"error alert alert-danger\">Enter School Name</div>' );                       
        }
        else
        {
            $('#editstudentschoolother'+studid).removeClass('errorborder')
        }
        if(lastname=='')
        {
            $('#editstudlastname'+studid).addClass('errorborder');
            $('#editstudlastname'+studid).after( '<div class=\"error alert alert-danger\">Enter Last Name</div>' );
            error=1;
        }
        else if(!regexs.test(lastname))
        {
            $('#editstudlastname'+studid).addClass('errorborder');
            $('#editstudlastname'+studid).after( '<div class=\"error alert alert-danger\">Special characters not allowed</div>' );
            error=1;
        }        
        else
        {
            $('#editstudlastname'+studid).removeClass('errorborder');
        }
        if(error==0)
        {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl."/parent/studentupdate";?>',
            dataType:'json',
            data: {studid:studid,firstname:firstname,lastname:lastname,school:school,schoolother:schoolother},
            success: function(data){
                if(data.error=='No')
                {
                    $('#studfirstname'+studid).text(data.firstname);
                    $('#studlastname'+studid).text(data.lastname);
                    $('#studenthead'+studid).text(data.firstname+' '+data.lastname);                    
                    $('#studfirstname'+studid).toggle();
                    $('#editstudfirstname'+studid).toggle();
                    $('#studlastname'+studid).toggle();
                    $('#editstudlastname'+studid).toggle();
                    $('#studentsave'+studid).toggle();
                    $('#studentcancel'+studid).toggle();
                    $('#editstudentschool'+studid).hide(); 
                    $('#editstudentschoolother'+studid).hide();
                    $('#studentschool'+studid).toggle(); 
                    $('#studentedit'+studid).toggle();
                    $('#hidepassword'+studid).toggle();
                    if(school=='0')
                    {
                        $('#studentschool'+studid).text(data.schoolother);    
                    }
                    else
                    {
                        $('#studentschool'+studid).text(data.school);   
                    }                    
                     
                }
                else
                {
                    alert(data.errortext);
                }
            }
        });
        }

    });
    $('.removestud').click(function(){
        if(confirm('Are you sure you want to remove this student?'))
        {
            $('.studentremove').remove();
            var studid=$(this).attr('data-id');
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl."/parent/removestudent";?>',
                dataType:'json',
                data: {studid:studid},
                success: function(data){
                    if(data.error=='No')
                    {
                        $('#studentraw'+studid).remove();

                    }
                    else
                    {
                        $('#studentraw'+studid).before('<div class="alert alert-danger studentremove">'+data.errortext+'</div>');
                        setTimeout(function(){
                            $('.studentremove').remove();
                        },4000)
                    }
                }
            });
        }
    });

</script>
<?php
/*
Yii::app()->clientScript->registerScript('success', "
        setTimeout(function(){
            $('.alert-success').fadeOut(function(){
                    $('.alert-success').remove()
            })
        },100000);
        setTimeout(function(){
        $('.alert-danger').fadeOut(function(){
                $('.alert-danger').remove()
        })
        },100000);
    ");
*/
if(base64_decode($_GET['mode'])=='subscription'):
    Yii::app()->clientScript->registerScript('subscription', "
        $('.formdiv').remove();
        $('#newstudent').hide();
        var student='".base64_decode($_GET['action'])."';        
        $('#removestud'+student).hide();
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/GetPlanForm')."',
              data: { student:student,mode:'subscription'}
            })
              .done(function( html ) {

                $('#studentraw'+student).before(html);
        }); 
    ");
endif;
Yii::app()->clientScript->registerScript('tooltip', "
    $(document).on('change','#plan',function(){
            var plan=$('#plan').val();
            var student=$('#idst').val();        
            checkPlan(plan,student);      
    })
    $(document).on('submit','#planform',function(){
        $('.error').remove();
        var count=0;
        $('.subscripvalid').each(function(){
            if($(this).val()=='')
            {
                var item=$(this).attr('data-item');
                $(this).after( '<div class=\"error alert alert-danger\">Select '+item+'</div>' );
                count++;
            }
        });
        if($('#planexist').val()!='0')
        {
            $('#plan').after( '<div class=\"error alert alert-danger\">Plan already active </div>' );            
            count++;            
                    
        }        
        if(count==0)
        {                       
            if($('#plan').val()!='' & $('#amounthidden').val()=='0 INR')
            {                
                $(this).attr('action','".Yii::app()->request->baseUrl."/parent/PaymentComplete');
                return true;
            }
            else
            {
                return true;
            }
               
        }
        else
        {
            return false;
        }

    });
    function checkPlan(plan,student)
    {
        $('.error').hide();
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('parent/checkactiveplan')."',
          data: { plan:plan,student:student}
        })
          .done(function( data ) {
            
            if(data>0)
            {
                $('#plan').after( '<div class=\"error alert alert-danger\">Plan already active </div>' );
                $('#planexist').val('1');
            }
            else
            {
                $('#planexist').val('0');
            }               
        });                
    }
    $(document).on('submit','#studentform',function(){
        $('.error').remove();
        var regex = /^[a-zA-Z�`' ]+$/;
        var count=0;
        $('.studentvalid').each(function(){
            if($(this).val()=='' & $(this).attr('id')!='country')
            {
                var item=$(this).attr('data-item');
                $(this).after( '<div class=\"error alert alert-danger\">Enter '+item+'</div>' );
                count++
            }
            else if($(this).val()=='' & $(this).attr('id')=='country')
            {
                var item=$(this).attr('data-item');
                $(this).after( '<div class=\"error alert alert-danger\">Select '+item+'</div>' );
                            
            }                        
            else if(!regex.test($(this).val()) & $(this).attr('id')!='country' & $(this).attr('id')!='school')
            {
                var item=$(this).attr('data-item');
                $(this).after( '<div class=\"error alert alert-danger\">Special characters not allowed</div>' );
                count++
            }            
        })
        if(count==0)
        {
        return true;
        }
        else
        {
        return false;
        }

    });
    $(document).on('submit','#changepassword-form',function(){
        $('.error').remove();
        var errorcount=0;
        var count=0;
        var password1='';
        var password2='';
        $('.passwordfield').each(function(){
            if($(this).val()=='')
            {
                var item=$(this).attr('data-item');
                $(this).after( '<div class=\"error alert alert-danger\">Enter '+item+'</div>' );
                errorcount++
            }
            else if($(this).val().length<4)
            {
                var item=$(this).attr('data-item');
                $(this).after( '<div class=\"error alert alert-danger\">'+item+' is too short (minimum is 4 characters).</div>' );
                errorcount++
            }
            else
            {
                if(count==0)
                {
                    password1=$(this).val()
                }
                else
                {
                    password2=$(this).val()
                }
            }
            count++
        })
        if(errorcount==0)
        {
            if(password1!=password2)
            {
                $('.verify').after( '<div class=\"error alert alert-danger\">Retype password does not match</div>' );
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
        return false;
        }

    });
    $('.showtooltip').tooltip();
    $('.showpassword').click(function(){
        var student=$(this).attr('data-student');
        $('#maskpassword'+student).toggle();
        $('#unmaskpassword'+student).toggle();
    })
    $('.addplan').click(function(){
        $('.formdiv').remove();
        $('#newstudent').hide();
        var student=$(this).attr('data-student');        
        $('#removestud'+student).hide();
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/GetPlanForm')."',
              data: { student:student}
            })
              .done(function( html ) {

                $('#studentraw'+student).before(html);
              });
    });
    $('.changepassword').click(function(){
        $('.formdiv').remove();
        $('#newstudent').hide();

        var user=$(this).attr('data-id');
        var type=$(this).attr('data-type');
        var parent=$(this).attr('data-parent');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/GetChangePassword')."',
              data: { user:user,type:type}
            })
              .done(function( html ) {
                if(type=='student')
                {
                    $('#'+parent).before(html);
                }
                else
                {
                    $('#'+parent).after(html);
                }

              });
    });
    $('#addstudent').click(function(){
            $('.formdiv').remove();
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/GetStudentForm')."',
            })
              .done(function( html ) {
                    $('#newstudent').html(html).slideDown('slow');
              });
    })
    $(document).on('change','#syllabus',function(){
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/GetStandardList')."',
              data: { id:$(this).val()}
            })
        .done(function( html ) {
            $('#standard').html(html);
            $('#plan').html('<option >Select</option>');
        });
    })
    $(document).on('change','#standard',function(){

        var publisher=$('#publisher').val();
        var syllabus=$('#syllabus').val();
        var standard=$('#standard').val();
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/GetPlanList')."',
              data: { publisher:publisher,syllabus:syllabus,standard:standard}
            })
        .done(function( html ) {
            $('#plan').html(html);
        });
    })
    $(document).on('click','.closeform',function(){
            $('.formdiv').remove();
            $('#newstudent').hide();
            $('.removestud').show();
    });
    $(document).on('change','#plan',function(){
        var plan=$(this).val();
        var voucher=$('#voucher').val();
        var student=$('#idst').val();
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/GetPlanAmount')."',
              dataType:'json',
              data: { plan:plan,voucher:voucher,student:student}
            })
        .done(function( data ) {
            $('#amount').html(data.planrate+' '+data.plancurrency);
            $('#amounthidden').val(data.planrate);
            $('#item_name').val(data.planname);
            $('#currency_code').val(data.plancurrency); 
            if(data.schoolcoupon=='yes')
                {   
                    
                    $('#voucher').val('-1');
                    $('#voucheridhidden').val('-1');
                    voucher='-1'; 
                                                                                                   
                }                           
            $('#custom').val('parent-'+$('#studentid').val()+'-'+data.planid+'-'+$('#username').val()+'-'+voucher);            
                                    
        });
    });
    $(document).on('click','.redeem',function(){        
        var voucher=$('#voucher').val();
        var plan=$('#plan').val();        
        if(voucher=='')
        {
            $('.vouchererror').html('<div class=\"alert alert-danger voucheralert\" role=\"alert\">Enter vocher code</div>');
            setTimeout(function(){                        
                        $('.vouchererror').html('');
            },5000)
        }
        if(plan=='')
        {
            $('.vouchererror').html('<div class=\"alert alert-danger voucheralert\" role=\"alert\">Select a plan</div>');
            setTimeout(function(){                        
                        $('.vouchererror').html('');
            },5000)
        }
        if(voucher!='' & plan!='')
        {
            $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('coupons/GetCouvherRate')."',                  
                  dataType:'json',
                  data: { plan:plan,voucher:voucher}
                })
            .done(function( data ) {                
                if(data.error=='no')
                {                    
                    $('#amount').html(data.rate);
                    $('#amounthidden').val(data.rate);
                    $('#voucheridhidden').val(data.voucher);
                    $('.vouchererror').html(data.message);
                    setTimeout(function(){                        
                        $('.voucher').val('')
                        $('.vouchererror').html('');
                        $('.vouchercancel').trigger('click');                        
                    },5000)                    
                }
                else
                {                    
                    $('.vouchererror').html(data.error);
                    $('#amount').html(data.rate);
                    $('#amounthidden').val(data.rate);
                    setTimeout(function(){
                        $('.voucher').val('')
                        $('.vouchererror').html('');
                    },5000)
                }
            });
        }
    })
 ");
?>
<?php Yii::app()->clientScript->registerScript('showimg',"
$(document).on('mouseenter', '.imagespan', function() {
            $(this).find('.deletespan').stop( true, true ).show();
        });

    $(document).on('mouseleave', '.imagespan', function() {
             $(this).find('.deletespan').stop( true, true ).hide();
        });



$(document).on('mouseenter', '.imagespanchild', function() {
            $(this).find('.deletespanchild').stop( true, true ).show();
        });

    $(document).on('mouseleave', '.imagespanchild', function() {
             $(this).find('.deletespanchild').stop( true, true ).hide();
        });


 $('.closepreofpic').click(function(){
 $('.oldpic').show();

 });
  $('.closepreofpicchild').click(function(){
  var id=$(this).data('id');

 $('.oldpicchild').show();

 });

  $('.closepreofpicchild').click(function(){
              var id=$(this).data('id');
              $('.previewchild'+id).hide();
              $('#photoimgchild'+id).val('');
              $('.savespanchild').hide();
  });
              $('.closepreofpic').click(function(){
              var id=$(this).data('id');
              $('.preview').hide();
              $('#photoimg').val('');
              $('.savespan').hide();
  });
  $('.closemodalchild').click(function(){

                 $('.closepreofpicchild').trigger('click');

         });
  $('.closemodalparent').click(function(){

                 $('.closepreofpic').trigger('click');

         });


 $('.savespan').click(function(){
            var name=$('#nameimg').val();
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/uploadImageSave')."',
              data: { name: name}

            })
.done  (function(data, textStatus, jqXHR)        {

                    $('.oldpicimg').attr('src','".Yii::app()->request->baseUrl."/images/userimages/'+data);
                    $('#leftpanelimg').attr('src','".Yii::app()->request->baseUrl."/images/userimages/'+data);

                    $('.oldpic').show();
                    $('.preview').hide();
                     $('#photoimg').val('');
                     $('.savespan').hide();
                   /* $('#photoimg').replaceWith($('#photoimg').clone(true));*/

 })

 });
 $('.savespanchild').click(function(){

            var id=$(this).data('id');

             var name=$('#nameimgchild'+id).val();
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('parent/uploadImageSaveChild')."',
              data: { name: name, id: id}

            })
.done  (function(data, textStatus, jqXHR)        {


                    $('.oldpicimgchild'+id).attr('src','".Yii::app()->request->baseUrl."/images/userimages/'+data);

                    $('.oldpicchild').show();
                    $('.previewchild'+id).hide();
                    $('#photoimgchild'+id).val('');
                    $('.savespanchild').hide();
 })

 });


    $(document).on('change','.imageselect',function(){ 
            $('#newimg').attr('src','');
            $('.preview').show();                    
            var filesize=this.files[0].size;
            filesize=filesize/1024;
            if(filesize<=2000)
            {                
                $('#imagetype').val($(this).attr('data-type'));
                $('#imagesection').val($(this).attr('data-section'));
                var section=$(this).attr('data-section');
                var type=$(this).attr('data-type');
                $('#name').val($(this).attr('name'));
    
                var dataid=$(this).attr('id');
                var form = $('#imageupdate')[0]; // You need to use standart javascript object here
                var formData = new FormData(form);
                $.ajax({
                        url: '".Yii::app()->createUrl('parent/uploadImage')."', // Url to which the request is send
                        type: 'POST',             // Type of request to be send, called as method
                        data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                        contentType: false,       // The content type used when sending data to the server.
                        cache: false,             // To unable request pages to be cached
                        processData:false,        // To send DOMDocument or non processed data file it is set to false
                        beforeSend: function() {
                                        $('#newimg').attr('src','".Yii::app()->request->baseUrl."/images/imageloader.gif');
                                        $('.preview').show();
    
                          },
                        success: function(data)   // A function to be called if request succeeds
                        {
                            var retstr='failed';
                            var retstr1='Maximum allowed size is 2Mb';
                            var retstr2='Invalid file format..';
                            var retstr3='Please select image..!';
                            if(data===retstr)
                            {
                            	alert (retstr);
                            	$('.savespan').hide();
                            	return false;
                            }
                            else if(data===retstr1)
                            {
                            	$('.errorupload').addClass('alert alert-danger');
                            		setTimeout(function(){
                            			$('.errorupload').removeClass('alert alert-danger');
                            		}, 6000);
                            	$('.savespan').hide();
                            	$('#newimg').attr('src','');
                            	$('.preview').hide();                   
                            	return false;
                            }
                            else if(data===retstr2)
                            {
                               alert (retstr2);
                               $('.savespan').hide();
                               return false;
                            }
                            else if(data===retstr3)
                            {
                               alert (retstr2);
                               $('.savespan').hide();
                               return false;
                            }
                            else
                            {
                            	$('#newimg').attr('src','').hide();                                                               	
                            	//$('.oldpic').hide();                                                         	
                            	$('#nameimg').attr('value',data);                        
                            	$('.preview').show();
                                $('#newimg').attr('src','". Yii::app()->request->baseUrl."/site/Imagerender/id/'+data+'?size=large&type=user&width=140&height=140').ready(function() {
                                	$('#newimg').show(function(){
                                		$('.savespan').show();
                                	});                                  
                                });                                                                                                
                            }                        
    
                        }
                });
            }
            else
            {
                $('#newimg').attr('src','');
                $('.preview').hide();
                $('.savespan').hide();                  
            $('.errorupload').addClass('alert alert-danger');
                setTimeout(function(){
                $('.errorupload').removeClass('alert alert-danger');
                }, 6000);  
                             
            }
            //readURL(this)
        });
        $(document).on('change','.childimg',function(){
            $('#name').val($(this).attr('name'));
            var dataid=$(this).attr('data-name');
            var filesize=this.files[0].size;
            filesize=filesize/1024;
            if(filesize<=2000)
            {
                var form = $('#imageupdatechild'+dataid)[0]; // You need to use standart javascript object here
                var formData = new FormData(form);
                $.ajax({
                        url: '".Yii::app()->createUrl('parent/uploadChildImage')."', // Url to which the request is send
                        type: 'POST',             // Type of request to be send, called as method
                        data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                        contentType: false,       // The content type used when sending data to the server.
                        cache: false,             // To unable request pages to be cached
                        processData:false,        // To send DOMDocument or non processed data file it is set to false
                        beforeSend: function()
						{
                                                $('#newimgchild'+dataid).attr('src','".Yii::app()->request->baseUrl."/images/imageloader.gif');
                                                $('.previewchild'+dataid).show();    
                        },
                        success: function(data)   // A function to be called if request succeeds
                        {
							var retstr='failed';
							var retstr1='Image file size max 2MB';
							var retstr2='Invalid file format..';
							var retstr3='Please select image..!';
							if(data===retstr)
							{							
								alert(retstr);
								$('.savespanchild').hide();
								return false;
							}
							else if(data===retstr1)
							{
								alert(retstr1);
								$('.errorupload').addClass('alert alert-danger');
									setTimeout(function()
									{
										$('.errorupload').removeClass('alert alert-danger');
									}, 6000);
								$('.savespanchild').hide();
								$('#newimgchild'+dataid).attr('src','');
								$('.previewchild'+dataid).hide();                   
								return false;
							}
							else if(data===retstr2)
							{
							   alert(retstr2);
							   $('.savespanchild').hide();
							   return false;
							}
							else if(data===retstr3){
							   alert(retstr3);
							   $('.savespanchild').hide();
							   return false;
							}
							else
							{
								$('#newimgchild'+dataid).attr('src','').hide();                        
								//$('.oldpicchild').hide();
                                $('#newimgchild'+dataid).attr('src','". Yii::app()->request->baseUrl."/site/Imagerender/id/'+data+'?size=large&type=user&width=140&height=140').ready(function() {
                                	$('#newimgchild'+dataid).show(function(){
                                		$('.savespanchild').show();
                                	});                                  
                                });                                                                								
								$('#nameimgchild'+dataid).attr('value',data);
								
								$('.previewchild'+dataid).show();
							}
    
                        }
                });
            }
            else
            {
                $('#newimgchild'+dataid).attr('src','');
                $('.previewchild'+dataid).hide(); 
                $('.savespanchild').hide();                 
                $('.errorupload').addClass('alert alert-danger');
                    setTimeout(function(){
                    $('.errorupload').removeClass('alert alert-danger');
                    }, 6000);  
                             
            }
        });


") ?>
<div class="overlay"></div> 
<img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" id="loader">
<style>
    .otherschool
    {
        display:none;
    }  
</style>
<?php 

Yii::app()->clientScript->registerScript('loadschools', "
    $(document).on('change','#school',function(){         
            if($(this).val()=='0')
            {
                $('.otherschool').show();
            }   
            else
            {
                $('.otherschool').hide();
            }
    });
    $(document).on('change','#country',function(){        
        $.ajax({
            method: 'POST',
            url: '".Yii::app()->createUrl('user/registration/getschools')."',
            data: { country:$(this).val()}
        })
            .done(function( data ) {
            $('#school').html(data);
        });                
    });    
        
            
");
?>