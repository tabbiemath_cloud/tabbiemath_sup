<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">

        <div class="bs-example">

            <div class="sky1">
                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky1.png', '', array('class' => 'img-responsive')); ?>
            </div>
            <h1 class="h125">You have the following Students added</h1>

            <div class="panel panel-default">
                <!-- Default panel contents -->

                <!-- Table -->
                <div class="table-responsive-vertical shadow-z-1">
                    <!-- Table starts here -->


                    <div class="table-responsive shadow-z-1">
                        <table class="table table-hover table-mc-light-blue">
                            <thead>
                            <tr>
                                <th>Student</th>
                                <th>Subscription</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody class="font2">
                            <?php foreach($students AS $student):
                                $plan=StudentPlan::model()->find(array('condition'=>'TM_SPN_StudentId=:student AND TM_SPN_Status=0','params'=>array(':student'=>$student->TM_STU_User_Id)));                                
                                ?>
                                <tr>
                                    <td><?php echo $student->TM_STU_First_Name.$student->TM_STU_User_Id;?></td>
                                    <td><?php
                                    
                                     if(count($plan)==0):?>
                                               <a href="" class="btn btn-warning">Manage</a>
                                        <?php
                                            else:                                            
                                                echo $plan->plan->TM_PN_Name;
                                            endif;?>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>


                </div>

            </div>
            <div
                class="sky3"><?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/sky3.png', '', array('class' => 'img-responsive')); ?></div>
        </div>
    </div>
</div>