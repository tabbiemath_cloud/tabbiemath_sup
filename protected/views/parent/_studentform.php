
<div class="panel-heading" style="background-color: rgb(154, 163, 182);color: white;">
    <div class="row">
        <div class="col-md-6 col-xs-6 ">
            <h5><b>Add New Student</b></h5>
        </div>
        <button class="pull-right btn btn-default btn-xs closeform" style="margin-right: 10px;">X</button>
    </div>
</div>
<div class="panel-body">
    <form id="studentform" method="post" action="<?php echo Yii::app()->request->baseUrl.'/parent/addstudent';?>">
        <div class="row">

            <div class="col-md-3 col-sm-6">
                <dl>
                    <dt>First Name</dt>
                    <dd><input type="text" id="firstname studentvalid" class="form-control studentvalid" data-item="First Name" name="firstname"></dd>
                </dl>

            </div>
            <div class="col-md-3 col-sm-6">
            <dl>
                <dt>Last Name</dt>
                <dd><input type="text" id="lastname" class="form-control studentvalid"  data-item="Last Name"  name="lastname"></dd>
            </dl>

        </div>
        <div class="col-md-3 col-sm-6">
                <dl>
                    <dt>Country</dt>
                    <dd>
                        <select name="country" id="country" class="form-control studentvalid" data-item="Country">
                        <option value="">Select Country</option>
                        <?php
                            $countries=Country::model()->findAll(array('condition' => 'TM_CON_Status=0','order' => 'TM_CON_Id'));
                            foreach($countries AS $country):
                                echo '<option value="'.$country->TM_CON_Id.'">'.$country->TM_CON_Name.'</option>';
                            endforeach; 
                        ?>
                        </select>   
                    </dd>
                </dl>

            </div>        
            <div class="col-md-3 col-sm-6 ">
                <dl>
                    <dt>Name of School</dt>
                    <dd>
                    <select name="school" id="school" class="form-control studentvalid" data-item="School">
                        <option value="">Select School</option>                        
                        </select>                                            
                    </dd>
                </dl>
            </div>
</div>                        
<div class="row otherschool">
            <div class="col-md-3 col-sm-6 otherschool">
                <dl>
                    <dt>School Name</dt>
                    <dd>
                        <input type="text" id="schoolcustom" class="form-control" data-item="School Name"  name="schoolcustom">
                    </dd>
                </dl>
            </div>
            <div class="col-md-3 col-sm-6 otherschool">
                <dl>
                    <dt>City</dt>
                    <dd>
                        <input type="text" id="city" class="form-control" data-item="City"  name="city">
                    </dd>
                </dl>
            </div>
            <div class="col-md-3 col-sm-6 otherschool">
                <dl>
                    <dt>State</dt>
                    <dd>
                        <input type="text" id="state" class="form-control" data-item="State"   name="state">
                    </dd>
                </dl>

            </div>                        

    </div>            
    <div class="row">
            <div class="col-md-12 col-sm-12" style="padding-top: 20px;">
                <input type="hidden" name="idst" value="">
                <button type="button" class="btn btn-warning closeform pull-right">Cancel</button>                
                <button type="submit" class="btn btn-warning pull-right" style="margin-right: 20px;"><span class="glyphicon glyphicon-floppy-save"></span>continue to step 2</button>                   
            </div>                
        </div>
    </form>
</div>
