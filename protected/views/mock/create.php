<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
	'Worksheet'=>array('admin'),
	'Create Worksheet',
);

$this->menu=array(
    array('label'=>'Manage Worksheet', 'class'=>'nav-header'),
	array('label'=>'List Worksheet', 'url'=>array('admin')),
);
?>

<h3>Create Worksheet</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>