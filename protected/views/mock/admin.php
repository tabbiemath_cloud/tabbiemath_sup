<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
    'Mock'=>array('admin'),
    'Manage Mock',
);

$this->menu=array(
    array('label'=>'Manage Worksheet', 'class'=>'nav-header'),
    array('label'=>'List Worksheet', 'url'=>array('admin')),
    array('label'=>'Create Worksheet', 'url'=>array('create')),
    array('label'=>'Export Worksheet', 'url'=>array('exportworksheet')),
	array('label'=>'Import Worksheet', 'url'=>array('importworksheet')),
);

?>

<h3>Manage Worksheet Tests</h3>
<div class="row brd1">
    <div class="col-lg-12">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'mock-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'columns'=>array(

            array(
                'name'=>'TM_MK_Thumbnail',
                'type' => 'raw',
                "filter"=>false,
                'value'=>function($data){
                 if($data->TM_MK_Thumbnail!='')
                 {   
                        if(strpos($data->TM_MK_Thumbnail, 'youtube') > 0)
                         {
                            return "<img src='".$data->TM_MK_Thumbnail."' width='150px' height='100px'>";
                        }
                        else
                        {
                        return "<img src='".Yii::app()->baseUrl . "/worksheets/".$data->TM_MK_Thumbnail."' width='150px' height='100px'>";

                        }
                }
                else
                {
                    return "<img src='".Yii::app()->baseUrl . "/images/worksheet.jpg' width='150px' height='100px'>";
                }
                },
            ),
            'TM_MK_Name',
            'TM_MK_Description',
            array(
                'name' => 'TM_MK_Status',
                'filter'=>false,
                'value' => '$data->itemAlias("MockStatus",$data->TM_MK_Status)',
            ),
            array(
                'name'=>'TM_MK_Publisher_Id',
                'value'=>'Publishers::model()->findByPk($data->TM_MK_Publisher_Id)->TM_PR_Name',
                'filter'=>CHtml::listData(Publishers::model()->findAll(array('order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),

            ),
            array(
                'name'=>'TM_MK_Syllabus_Id',
                'value'=>'Syllabus::model()->findByPk($data->TM_MK_Syllabus_Id)->TM_SB_Name',
                'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),
            ),
            array(
                'name'=>'TM_MK_Standard_Id',
                'value'=>'Standard::model()->findByPk($data->TM_MK_Standard_Id)->TM_SD_Name',
                            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_MK_Syllabus_Id) ? Standard::model()->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))->findAll(new CDbCriteria(array(
                        'condition' => "TM_SD_Syllabus_Id = :syllabusid AND TM_SD_Status='0'",
                        'params' => array(':syllabusid' => $model->TM_MK_Syllabus_Id),
                        'order' => 'TM_SD_Id'
                    ))) : Standard::model()->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))->findAll(array('condition' => "TM_SD_Status='0'",'order' => 'TM_SD_Id')),'TM_SD_Id','TM_SD_Name'),
                    //'option'=>'SELECT'

            ),                        
            array(
                'name' => 'TM_MK_Availability',
                'filter'=>false,
                'value' => '$data->itemAlias("MockAvail",$data->TM_MK_Availability)',
            ),
            array(
                'name'=>'TM_MK_Resource',
                'value'=>'Mock::itemAlias("MockResource",$data->TM_MK_Resource)',
                'filter'=>Mock::itemAlias("MockResource"),
            ),
            array(
                'name' => 'TM_MK_CreatedBy',
                'filter'=>false,
                'value' => 'Mock::GetCreatorAdmin($data->TM_MK_CreatedBy)',
            ),
            array(
                'header'=>'Manage Questions',
                'type'=>'raw',
                'value'=>'Mock::GetMangeLink($data->TM_MK_Id)',
            ),
            array(
                'header'=>'Sort Order',
                'type'=>'raw',
                'value'=>'Mock::GetSortColumn($data->TM_MK_Id,$data->TM_MK_Sort_Order)',
            ),                        
            array(
                'class'=>'CButtonColumn',
                'template'=>'{printmock}{printmocksolution}{view}{update}{delete}',
                'deleteButtonImageUrl'=>false,
                'updateButtonImageUrl'=>false,
                'viewButtonImageUrl'=>false,
                'buttons'=>array
                (
                    'printmock'=>array(
                        'label'=>'<span class="glyphicon glyphicon-print"></span>',
                        'url'=>'$data->TM_MK_Id',
                        'options'=>array(
                            'class' => 'printworksh',
                            'title'=>'Print Worksheet',
                             'data-backdrop' => 'static',
                              'data-toggle' => 'modal',
                        )),
                    'printmocksolution'=>array(
                        'label'=>'<span class="glyphicon glyphicon-list-alt"></span>',
                        'url'=>'$data->TM_MK_Id',
                        'options'=>array(
                            'class' => 'printworksol',
                            'title'=>'Print Worksheet Solution',
                            'data-backdrop' => 'static',
                            'data-toggle' => 'modal', 
                        )),
                    'view'=>array(
                        'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                        'options'=>array(
                            'title'=>'View'
                        )),                                                
                    'update'=>array(
                        'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                        'options'=>array(
                            'title'=>'Update'
                        )),
                    'delete'=>array(
                        'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                        'options'=>array(

                            'title'=>'Delete',
                        ),
                    )
                ),
            ),
        ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
    )); ?>
    </div>
</div>
<script>
$( 'body' ).on( 'click', '.printworksh', function () {
     var worksheetid = $(this).attr('href');
    // $('#showworksheet').html('');
    window.open('printmock?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
});

$( 'body' ).on( 'click', '.printworksol', function () {
     var worksheetid = $(this).attr('href');
    // $('#showworksheet').html('');
    window.open('printmocksolution?id='+worksheetid, 'myWin', 'width = 700px, height = 500px');
});

$(function(){
    $(document).on('click','.order_link',function(){
        var id=$(this).attr('data-id');
        var info=$(this).attr('data-info');

        var order=($(this).attr('data-order')*1);                

        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/mock/reordermock',
            beforeSend : function(){
                $('.order_link').click(false);
            },
            dataType: "text",
            data: {id:id,info:info,order:order},
            success: function(data){
                if(data=='yes')
                {
                    $.fn.yiiGridView.update('mock-grid');
                }
            }
        });
    });
    /*$('select option')
        .filter(function() {
            return !this.value || $.trim(this.value).length == 0 || $.trim(this.text).length == 0;
        })
        .remove();*/
})
</script>
