<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
    'Mock'=>array('admin'),
    'Manage Mock',
);

$this->menu=array(
    array('label'=>'Manage Tags', 'class'=>'nav-header'),
    array('label'=>'List Tags', 'url'=>array('tags')),
    array('label'=>'Create Tags', 'url'=>array('createtags')),
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tags-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

?>
<h3>Manage Worksheet Tags</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'tags-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                array(
                    'name'=>'TM_MT_Syllabus_Id',
                    'value'=>'Syllabus::model()->findByPk($data->TM_MT_Syllabus_Id)->TM_SB_Name',
                    'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),
                ),
                array(
                    'name'=>'TM_MT_Standard_Id',
                    'value'=>'Standard::model()->findByPk($data->TM_MT_Standard_Id)->TM_SD_Name',
                    'filter'=>CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),
                ),
                array(
                    'name'=>'TM_MT_Type',
                    'value'=>'MockTags::itemAlias("MockResource",$data->TM_MT_Type)',
                    'filter'=>MockTags::itemAlias("MockResource"),
                ),
                'TM_MT_Tags',
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            ),
                            'url'=>'Yii::app()->createUrl("mock/Viewtags",array("id"=>$data->TM_MT_Id))'
                        ),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            ),
                            'url'=>'Yii::app()->createUrl("mock/Updatetags",array("id"=>$data->TM_MT_Id))'
                            ),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(
                                'title'=>'Delete',
                            ),
                            'url'=>'Yii::app()->createUrl("mock/Deletetags",array("id"=>$data->TM_MT_Id))'
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
