<?php
/* @var $this MockController */
/* @var $model Mock */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('mock/admin')."';
    });
");
?>


<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mock-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
	    <p class="note">Fields with <span class="required">*</span> are required.</p>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_MK_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_MK_Name'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Description',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textArea($model,'TM_MK_Description',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_MK_Description'); ?>
            </div>
        </div> 
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Publisher_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php

                 if(Yii::app()->session['usertype']=='1'):
                    $publishers=CHtml::listData(Publishers::model()->findAll(array("condition"=>"TM_PR_Status = '0'",'order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name');
                elseif(Yii::app()->session['usertype']=='2'):
                    $userpublisher=User::model()->findbyPk(Yii::app()->user->id)->publisher;                    
                    $publishers=CHtml::listData(Publishers::model()->findAll(array("condition"=>"TM_PR_Status = '0' AND TM_PR_Id=".$userpublisher,'order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name');
                endif;
                if(count($publishers)=='1'):
                    $selectarray=array('class'=>'form-control');
                else:
                    $selectarray=array('empty'=>'Select Publisher','class'=>'form-control');
                endif;
                echo $form->dropDownList($model,'TM_MK_Publisher_Id',$publishers,$selectarray); ?>
                <?php echo $form->error($model,'TM_MK_Publisher_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Syllabus_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php
                $syllabuses=CHtml::listData(Syllabus::model()->findAll(array("condition"=>"TM_SB_Status = '0'",'order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name');
                if(count($syllabuses)=='1'):
                    $selectarray=array('empty'=>'Select Syllabus','class'=>'form-control','ajax' =>
                        array(
                            'type'=>'POST', //request type
                            'url'=>CController::createUrl('chapter/GetstandardQuestion'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                            //Style: CController::createUrl('currentController/methodToCall')
                            'update'=>'#Mock_TM_MK_Standard_Id', //selector to update
                            'data'=>'js:{id:$(this).val()}'
                            //leave out the data key to pass all form values through
                        ));
                else:
                    $selectarray=array('empty'=>'Select Syllabus','class'=>'form-control','ajax' =>
                        array(
                            'type'=>'POST', //request type
                            'url'=>CController::createUrl('chapter/GetstandardQuestion'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                            //Style: CController::createUrl('currentController/methodToCall')
                            'update'=>'#Mock_TM_MK_Standard_Id', //selector to update
                            'data'=>'js:{id:$(this).val()}'
                            //leave out the data key to pass all form values through
                        ));
                endif;
                echo $form->dropDownList($model,'TM_MK_Syllabus_Id',$syllabuses,$selectarray); ?>
                <?php echo $form->error($model,'TM_MK_Syllabus_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Standard_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php

                if(!$model->isNewRecord) {
                    $standards = CHtml::listData(Standard::model()->findAll(array('condition' => "TM_SD_Syllabus_Id =  $model->TM_MK_Syllabus_Id AND TM_SD_Status = '0'", 'order' => 'TM_SD_Name')), 'TM_SD_Id', 'TM_SD_Name');
                    if (count($standards) == '1'):
                        $selectarray = array('class' => 'form-control');
                    else:
                        $selectarray = array('empty' => 'Select Standard', 'class' => 'form-control');
                    endif;
                }
                else{
                    $standards=array(''=>'Select Standard');
                    $selectarray=array('class' => 'form-control');
                }
                echo $form->dropDownList($model,'TM_MK_Standard_Id',$standards,$selectarray); ?>
                <?php echo $form->error($model,'TM_MK_Standard_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->hiddenField($model,'TM_MK_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
                <?php 
                if($model->isNewRecord):
                    echo $form->dropDownList($model,'TM_MK_Status',Mock::itemAlias('MockStatus'),array('class'=>'form-control','options' => array('1'=>array('selected'=>true))));
                else:
                    echo $form->dropDownList($model,'TM_MK_Status',Mock::itemAlias('MockStatus'),array('class'=>'form-control'));
                endif;
                 ?>
                
                <?php echo $form->error($model,'TM_MK_Status'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Availability',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">                
                <?php                 
                    echo $form->dropDownList($model,'TM_MK_Availability',Mock::itemAlias('MockAvail'),array('class'=>'form-control','empty'=>'Select Availability'));
                 ?>
                
                <?php echo $form->error($model,'TM_MK_Availability'); ?>
            </div>
        </div>
        <div class="form-group" id="schoollist" 
        <?php 
        if($model->isNewRecord):
            echo 'style="display: none;"';
        else:
            if($model->TM_MK_Availability=='0'):
                echo 'style="display: none;"';
            endif;
        endif;?>                    
        >
            <?php echo $form->labelEx($model,'schools',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">                
                <?php 
                $schools = CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')), 'TM_SCL_Id', 'TM_SCL_Name');
                
                //    echo $form->dropDownList($model,'schools',,array('class'=>'form-control','options' => array('1'=>array('selected'=>true))));            
                    echo $form->dropDownList($model,'schools',$schools,array('class'=>'form-control','multiple'=>'true','options'=>$mockschools));                
                 ?>
                
                <?php echo $form->error($model,'schools'); ?>
            </div>
        </div>
        <?php /*  
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Sort_Order',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">                
                <?php                 
                    echo $form->dropDownList($model,'TM_MK_Sort_Order',array('0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'),array('class'=>'form-control','empty'=>'Select Sort Order'));
                 ?>
                
                <?php echo $form->error($model,'TM_MK_Sort_Order'); ?>
            </div>
        </div> */?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Resource',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php
                echo $form->dropDownList($model,'TM_MK_Resource',Mock::itemAlias('MockResource'),array('class'=>'form-control','empty'=>'Select Type'));
                ?>

                <?php echo $form->error($model,'TM_MK_Resource'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Link_Resource',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->hiddenField($model,'TM_MK_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
                <?php 
                if($model->isNewRecord):
                    echo $form->dropDownList($model,'TM_MK_Link_Resource',Mock::itemAlias('Linkresource'),array('class'=>'form-control','options' => array('0'=>array('selected'=>true))));
                else:
                    echo $form->dropDownList($model,'TM_MK_Link_Resource',Mock::itemAlias('Linkresource'),array('class'=>'form-control'));
                endif;
                 ?>
                
                <?php echo $form->error($model,'TM_MK_Link_Resource'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Tags',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">                
                <?php                 
                    echo $form->textArea($model,'TM_MK_Tags',array('class'=>'form-control','placeholder'=>'Enter search tags separated by comma'));
                 ?>
                
                <?php echo $form->error($model,'TM_MK_Tags'); ?>
            </div>
        </div> 
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Worksheet',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">                
                <?php                 
                    echo $form->fileField($model,'TM_MK_Worksheet',array('class'=>'form-control'));
                 ?>
                <?php echo $form->error($model,'TM_MK_Worksheet'); ?>
            </div>
        </div>
        <?php if(!$model->isNewRecord):?>
            <div class="form-group" id="worksheetdiv">
                <label class="col-sm-3 control-label">Uploaded Worksheet File</label>
                <div class="col-sm-9">                
                    <ul class="list-group">
                      <li class="list-group-item">
                      <?php
                        if($model->TM_MK_Worksheet==''):
                            echo "No file uploaded";
                        else:
                            echo $model->TM_MK_Worksheet;
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;<a id=\"deleteworksheet\" data-id=\"".$model->TM_MK_Id."\" href=\"javascript:void(0);\" style=\"float:right;\" title=\"Delete File\"><span class=\"glyphicon  glyphicon-trash\"></span></a>";
                        endif; 
                      ?></li>
                    </ul>                    
                </div>
            </div>            
        <?php endif;?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Video_Url',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_MK_Video_Url',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_MK_Video_Url'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Solution',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">                
                <?php                 
                    echo $form->fileField($model,'TM_MK_Solution',array('class'=>'form-control'));
                 ?>
                
                <?php echo $form->error($model,'TM_MK_Solution'); ?>
            </div>
        </div>   
        <?php if(!$model->isNewRecord):?>
            <div class="form-group" id="solutiondiv">
                <label class="col-sm-3 control-label">Uploaded Solution File</label>
                <div class="col-sm-9">                
                    <ul class="list-group">
                      <li class="list-group-item">
                      <?php
                        if($model->TM_MK_Solution==''):
                            echo "No file uploaded";
                        else:
                            echo $model->TM_MK_Solution;
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;<a id=\"deletesolution\" data-id=\"".$model->TM_MK_Id."\" href=\"javascript:void(0);\" style=\"float:right;\" title=\"Delete File\"><span class=\"glyphicon  glyphicon-trash\"></span></a>";
                        endif; 
                      ?></li>
                    </ul>                    
                </div>
            </div>            
        <?php endif;?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MK_Thumbnail',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php
                echo $form->fileField($model,'TM_MK_Thumbnail',array('class'=>'form-control'));
                ?>
                <?php echo $form->error($model,'TM_MK_Thumbnail'); ?>
                <p>Optimal Size 221 * 151 px</p>
            </div>
        </div>
        <?php if(!$model->isNewRecord):?>
            <div class="form-group" id="thumbnaildiv">
                <label class="col-sm-3 control-label">Uploaded Thumbnail Image</label>
                <div class="col-sm-9">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <?php
                            if($model->TM_MK_Thumbnail==''):
                                echo "No file uploaded";
                            else:
                                echo $model->TM_MK_Thumbnail;
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;<a id=\"deletethumbnail\" data-id=\"".$model->TM_MK_Id."\" href=\"javascript:void(0);\" style=\"float:right;\" title=\"Delete File\"><span class=\"glyphicon  glyphicon-trash\"></span></a>";
                            endif;
                            ?></li>
                    </ul>
                </div>
            </div>
        <?php endif;?>
        <div class="form-group">
            <div class="row">
            <div class="col-sm-3 pull-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Continue' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                </div>

        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
$(function(){
    $('#Mock_TM_MK_Availability').change(function(){
        var selectedvalue=$(this).val();
        if(selectedvalue=='0'){
            $('#schoollist').hide();
        }
        else{
            $('#schoollist').show();
            if(selectedvalue=='1')
            {
                $('#schoollist option').prop('selected', true);
            }
            else
            {
                $('#schoollist option').prop('selected', true);
            }
        }        
    }); 
    $('#deleteworksheet').click(function(){
        if(confirm('Are you sure you want to delete this worksheet?'))
        {
            var mock=$('#deleteworksheet').attr('data-id');                            
            $.ajax({
               method:'POST',
               url:'<?php echo Yii::app()->request->baseUrl;?>/mock/deletefile',
               data: {mock:mock,type:'worksheet'},
               success: function(data){
                    if(data=='yes')
                    {
                        $('#worksheetdiv').remove();
                    }
                }                                            
            });            
        }
    });
    $('#deletesolution').click(function(){
        if(confirm('Are you sure you want to delete this solution?'))
        {
            var mock=$('#deletesolution').attr('data-id');                
    
            $.ajax({
                method: 'POST',                
                url: '<?php echo Yii::app()->request->baseUrl;?>/mock/deletefile',                
                data: {mock:mock,type:'solution'},
                success: function(data){
                    if(data=='yes')
                    {
                        $('#solutiondiv').remove();
                    }
                }
            });            
        }        
    });
    $('#deletethumbnail').click(function(){
        if(confirm('Are you sure you want to delete this thumbnail?'))
        {
            var mock=$('#deletethumbnail').attr('data-id');

            $.ajax({
                method: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl;?>/mock/deletefile',
                data: {mock:mock,type:'thumbnail'},
                success: function(data){
                    if(data=='yes')
                    {
                        $('#thumbnaildiv').remove();
                    }
                }
            });
        }
    });
});
</script>