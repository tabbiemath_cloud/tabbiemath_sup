<?php

$this->menu=array(
    array('label'=>'Manage Tags', 'class'=>'nav-header'),
    array('label'=>'List Tags', 'url'=>array('admin')),
    array('label'=>'Create Tags', 'url'=>array('create')),
    array('label'=>'View Tags', 'url'=>array('view', 'id'=>$model->TM_MT_Id)),

);
?>

<h3>Update Tags</h3>
<?php $this->renderPartial('_formtags', array('model'=>$model)); ?>
