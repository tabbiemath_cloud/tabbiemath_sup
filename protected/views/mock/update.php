<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
	'Worksheet'=>array('admin'),
	$model->TM_MK_Id=>array('view','id'=>$model->TM_MK_Id),
	'Update Worksheet Test',
);

$this->menu=array(
    array('label'=>'Manage Worksheet', 'class'=>'nav-header'),
	array('label'=>'List Worksheet', 'url'=>array('admin')),
	array('label'=>'Create Worksheet', 'url'=>array('create')),
	array('label'=>'View Worksheet', 'url'=>array('view', 'id'=>$model->TM_MK_Id)),
    array('label'=>'Manage Questions', 'url'=>array('managequestions', 'id'=>$model->TM_MK_Id)),
    array('label'=>'Delete Worksheet', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_MK_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>Update Mock <?php echo $model->TM_MK_Name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'mockschools'=>$mockschools)); ?>
