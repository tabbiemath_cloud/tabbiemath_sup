<?php
/* @var $this MockController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mocks',
);

$this->menu=array(
	array('label'=>'Create Mock', 'url'=>array('create')),
	array('label'=>'Manage Mock', 'url'=>array('admin')),
);
?>

<h1>Mocks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
