<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
	'Mock'=>array('admin'),
	'Manage Mock',
);

$this->menu=array(
    array('label'=>'Manage Worksheet', 'class'=>'nav-header'),
	array('label'=>'List Worksheet', 'url'=>array('admin')),
	array('label'=>'Create Worksheet', 'url'=>array('create')),
	array('label'=>'Export Worksheet', 'url'=>array('exportworksheet')),
);

?>

<h3>Import Worksheet</h3>
<div class="row brd1">
    <form method="post" action="<?php echo CController::createUrl('mock/importworksheet');?>" enctype="multipart/form-data" id="importform">
        <div class="panel-body form-horizontal payment-form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select CSV <span class="required">*</span></label>
                <div class="col-sm-4">
                    <input type="file" name="importworksheetcsv" class="form-control" id="importworksheetcsv">
                    <span class="info">Maximum number of rows in a csv is 500</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 pull-left"> </div>
                <div class="col-sm-3 pull-left">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                <div class="col-sm-3 pull-left">
                    <?php echo CHtml::submitButton('Import',array('class'=>'btn btn-warning btn-lg btn-block','name'=>'submit')); ?>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
    if(count($importlog)>0):
    ?>
<div class="panel panel-yellow addedqstable">
        <div class="panel-heading">
            <h3 class="panel-title">Import Status</h3>
        </div>
        <div class="scrol-body">
            <table class="table table-striped table-bordered ">
                <thead>
                    <th>Row #</th>
                    <th>Import Status</th>
                    <th>Notes</th>
                </thead>
                <tbody>
                <?php for($i=1;$i<=count($importlog);$i++):?>
                    <tr><td><?php echo $i;?></td><td><?php echo ($importlog[$i]['worksheet']=='0'?'Worksheet Import Failed': 'Worksheet Import Successful');?></td><td><?php echo $importlog[$i]['status'];?></td></tr>
                <?php endfor;?>
                </tbody>
            </table>
        </div>
</div>
<?php endif;?>