<?php

Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('mock/tags')."';
    });
");
?>

<div class="row brd1">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'tags-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MT_Standard_Id',array('class'=>'col-sm-3 control-label'));?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_MT_Standard_Id',CHtml::listData(Standard::model()->findAll(array('order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),array('empty'=>'Select','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_MT_Standard_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MT_Type',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php
                echo $form->dropDownList($model,'TM_MT_Type',MockTags::itemAlias('MockResource'),array('class'=>'form-control','empty'=>'Select Type'));
                ?>

                <?php echo $form->error($model,'TM_MT_Type'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_MT_Tags',array('class'=>'col-sm-3 control-label'));?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_MT_Tags',array('size'=>60,'maxlength'=>500,'class'=>'form-control','placeholder'=>"Enter search tags separated by comma")); ?>
                <?php echo $form->error($model,'TM_MT_Tags'); ?>
            </div>
        </div>

        <!--<div class="form-group">
            <?php /*echo $form->labelEx($model,'TM_MT_Status',array('class'=>'col-sm-3 control-label')); */?>
            <div class="col-sm-9">
                <?php /*//echo $form->hiddenField($model,'TM_SD_CreatedBy',array('value'=>Yii::app()->user->id)); */?>
                <?php /*echo $form->dropDownList($model,'TM_MT_Status',MockTags::itemAlias('MockStatus'),array('class'=>'form-control')); */?>
                <?php /*echo $form->error($model,'TM_MT_Status'); */?>
            </div>
        </div>-->

        <div class="form-group">
            <div class="raw">
                <div class="col-sm-3 pull-right">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>