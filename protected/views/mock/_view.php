<?php
/* @var $this MockController */
/* @var $data Mock */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_MK_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_MK_Id), array('view', 'id'=>$data->TM_MK_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_MK_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_MK_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_MK_Description')); ?>:</b>
	<?php echo CHtml::encode($data->TM_MK_Description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_MK_Publisher_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_MK_Publisher_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_MK_Syllabus_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_MK_Syllabus_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_MK_Standard_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_MK_Standard_Id); ?>
	<br />


</div>