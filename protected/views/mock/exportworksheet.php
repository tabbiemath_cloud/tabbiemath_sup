<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
    'Worksheet'=>array('index'),
    $model->TM_MK_Id,
);
$this->pageTitle='Export Worksheet';
$this->menu=array(
    array('label'=>'Manage Worksheets', 'class'=>'nav-header'),
    array('label'=>'List Worksheet', 'url'=>array('admin')),
    array('label'=>'Create Worksheet', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('mock/admin')."';
    });
");
?>
<h3>Export Worksheet</h3>
<div class="row brd1">
    <form method="post" action="<?php echo CController::createUrl('mock/exportworksheetcsv');?>" enctype="multipart/form-data" id="exportform">
        <div class="panel-body form-horizontal payment-form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select Syllabus <span class="required">*</span></label>
                <div class="col-sm-4">
                    <select name="syllabus" id="syllabus" class="form-control">
                        <option value="">Select Syllabus</option>
                        <?php foreach($syllabus AS $syllab):?>
                            <option value="<?=$syllab->TM_SB_Id;?>"><?=$syllab->TM_SB_Name;?></option>
                        <?php endforeach;?>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Type">Select Standard <span class="required">*</span></label>
                <div class="col-sm-4">
                    <select name="standard" id="standard" class="form-control">
                        <option value="">Select Standard</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 pull-left"> </div>
                <div class="col-sm-3 pull-left">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                <div class="col-sm-3 pull-left">
                    <?php echo CHtml::submitButton('Export',array('class'=>'btn btn-warning btn-lg btn-block','name'=>'submit')); ?>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $('#syllabus').change(function(){
        $("#standard").empty().append('<option value="">Select Standard</option>');
        if($(this).val()!=''){
            var id=$(this).val();
            $.ajax({
                method: 'POST',
                url: '<?php echo CController::createUrl('chapter/GetstandardQuestion');?>',
                data: {id:id},
                success: function(data){
                    $("#standard").empty().append(data);
                }
            });
        }
    });
    $('#exportform').submit(function(event){
        $('.errorMessage').remove();
        var syllabus=$('#syllabus').val();
        var standard=$('#standard').val();
        if(syllabus!='' & standard!='' )
        {
            return true;
        }
        else
        {
            if(syllabus=='')
            {
                $('#syllabus').after('<div class="errorMessage">Select Syllabus</div>');
            }
            if(standard=='')
            {
                $('#standard').after('<div class="errorMessage">Select Standard</div>');
            }
            return false;
        }
        event.preventDefault();
    })
</script>
