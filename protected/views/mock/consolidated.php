<?php
/* @var $this PlanController */
/* @var $model Plan */

?>

<?php
/* @var $this BlueprintsController */
/* @var $model Blueprints */

// $this->breadcrumbs=array(
//     'Blueprints'=>array('index'),
//     'Manage',
// );

$this->menu=array(
    array('label'=>'Thematic Report', 'class'=>'nav-header'),
);

?>

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css">
<style>

    ul.pagination {
        display: inline-block;
        padding: 0;
        margin: 0;
    }

    ul.pagination li {display: inline;}

    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    ul.pagination li a.active {
        background-color: #4CAF50;
        color: white;
    }

    ul.pagination li a:hover:not(.active) {background-color: #ddd;}
</style>

<h3>Thematic Report</h3>
<div class="row brd1">

        <div class="col-lg-12">
            <form method="get" id="searchform">
                <table class="table table-bordered table-hover table-striped admintable">
                    <tr><th colspan="5"><h3><?php echo UserModule::t("Search"); ?></h3></th></tr>
                    <tr>
                    	<?php
                    		if(!Yii::app()->user->isSchoolAdmin() && !Yii::app()->user->isTeacherAdmin()){
                    	?>
                    			<th>School</th>
                    	<?php
                    		} 
                    	?>
                        
                        <th >Select Class</th> 
                        <th >Teacher</th>                         
                       <!--  <th colspan="2">
                            <input type="radio" class="date" <?php echo ($formvals['date']=='before'?'checked="checked"':'');?> name="date" value="before" id="before">Before a date
                            <input type="radio" class="date" <?php echo ($formvals['date']=='after'?'checked="checked"':'');?> name="date" value="after" id="after">After a date
                            <input type="radio" class="dates" <?php echo ($formvals['date']=='between' || $formvals['date']=='' ?'checked="checked"':'');?> name="date" value="between" id="days">Between dates
                        </th>      -->              
                    </tr>
                    <tr>
                    	<?php
                    		if(!Yii::app()->user->isSchoolAdmin() && !Yii::app()->user->isTeacherAdmin()){
                    	?>
                        <td>
                        	
	                            <select onchange="loadPlan()" name="school" id="school" class="form-control">                                    
	                            <?php 
	                                $data=CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');
	                                
	                                echo CHtml::tag('option',
	                                               array('value'=>''),'Select School',true);
	                                foreach($data as $value=>$name)
	                                {
	                                    if($formvals['school']==$value):
	                                        $selected=array('value'=>$value,'selected'=>'selected');              
	                                    else:
	                                        $selected=array('value'=>$value);
	                                    endif;
	                                    echo CHtml::tag('option',
	                                               $selected,CHtml::encode($name),true);
	                                }
	                                if($formvals['school']=='0'):
	                                    $selectedother=array('value'=>0,'selected'=>'selected');              
	                                else:
	                                    $selectedother=array('value'=>0);
	                                endif;                                        
	                                //echo CHtml::tag('option',$selectedother,'Other',true);                                    
	                            ?>
	                            </select>
	                         
                            <span style="color:red;display: none;;" class="error errorschool">Select School</span>
                           
                        </td>
                        <?php
	                     	}
	                     	else {
	                     		$masterschool=UserModule::GetSchoolDetails();
	                     ?>
	                     	<input type="hidden" name="school" id="school" value="<?= $masterschool->TM_SCL_Id ?>">

	                     <?php
	                     	}
	                     ?>
                        <td >
	                        <select name="plan" id="plan" class="form-control" >
	                            <option value="">Select Class</option>
	                        </select>
	                        <span style="color:red;display: none;;" class="error errordiv">Select A Plan</span>
	                    </td>
                        <td>
                            <select name="teachername" id="teachername" class="form-control" >
	                            <option value="">Select Teacher</option>
	                        </select>
	                       <!--  <span style="color:red;display: none;" class="error errordiv">Select A Teacher</span> -->
                        </td>                                                                                               
                       
                        <td><input type="date" class="form-control" value="<?php echo ($formvals['fromdate']!=''?$formvals['fromdate']:'');?>" id="fromdate" name="fromdate" >
                        <span style="color:red;display: none;;" class="error errordivdate">Select A Date</span></td>
                        <td><input type="date" class="form-control" value="<?php echo ($formvals['todate']!=''?$formvals['todate']:'');?>" id="todate" name="todate" >
                        	 <span style="color:red;display: none;;" class="error errordivtodate">Select A Date</span>
                        </td>                       
                    </tr>                    
                    <tr>
                        <?php if(count($formvals)>0):?>
                        	<?php
                    		if(!Yii::app()->user->isSchoolAdmin() && !Yii::app()->user->isTeacherAdmin()){
                    			?>
                            	<td colspan="2"></td>
                            <?php } else{
                            	?>
                            	<td></td>
                            <?php } ?>                        
                            <td ><a href="<?php echo Yii::app()->createUrl('teachers/Downloadassessment',array('teachername'=>$formvals['teachername'],'school'=>$formvals['school'],'date'=>$formvals['date'],'fromdate'=>$formvals['fromdate'],'todate'=>$formvals['todate']))?>" class="btn btn-warning btn-md">Export</a></td>
                        
                        <?php else:
                        	if(!Yii::app()->user->isSchoolAdmin() && !Yii::app()->user->isTeacherAdmin()){
                    			
                        	?>
                            <td colspan="3"></td>
                            <?php }
                            else
                            { ?>
                            	<td colspan="2"></td>
                                                        
                        <?php }
                         endif;?>                                                                        
                        <td><button class="btn btn-warning btn-lg btn-block" name="search" value="search" type="submit">Download PDF</button></td>
                        <td ><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
   
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>                
    <script type="text/javascript">



        function loadPlan()
        {
            let schoolid = $('#school').val();
            console.log(schoolid);
            if(schoolid =='')
            {
                $('#schooltext').show();
            }   
            else
            {
                $('#schooltext').hide();
                $.ajax({
                    type: 'POST',
                    dataType:'json',
                    url: '<?php echo Yii::app()->request->baseUrl."/mock/Schoolplan";?>',
                    data: {id:schoolid},

                    success: function(data){
                        if(data.success=='yes')
                        {
                           $('#plan').html(data.planslist);
                           $('#teachername').html(data.teacherslist);
                        }
                    }

                });
            }

        }
        $(function(){
            $('#clear').click(function(){
                $('#student ').val('');
                $('#parent ').val('');
                $('#school ').val('');
                $('#plan ').val('');
                return true;
            });
            $('#searchform').submit(function(){
                var error=0
               $('.error').hide()               
               if($('#plan').val()=='')
               {
                    $('.errordiv').show();
                    error=1;
                    
               } 
               else
               {
                    $('.errordiv').hide();                                        
               }               
               if($('#fromdate').val()=='')
               {
                    $('.errordivdate').show();
                    error=1;                
               }
               else
               {
                    $('.errordivdate').hide();
               }
               if($('#todate').val()=='')
               {
                    $('.errordivtodate').show();
                    error=1;                
               }
               else
               {
                    $('.errordivtodate').hide();
               }
               if($('#school').val()=='')
               {
                    $('.errorschool').show();
                    error=1;                
               }
               else
               {
                    $('.errorschool').hide();
               }
               
                if(error==1)
                {
                    return false;
                }                
                else
                {
                    return true;
                }              
            });
            $('.date').click(function(){
                $('#fromdate').attr('disabled',false);
                $('#todate').attr('disabled',true);
            })
            $('.dates').click(function(){
                $('#fromdate').attr('disabled',false);
                $('#todate').attr('disabled',false);
            })
            <?php
                if($formvals['date']=='before'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',true);";
                elseif($formvals['date']=='after'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',true);";                
                elseif($formvals['date']=='between'):
                    echo  "$('#fromdate').attr('disabled',false);";
                    echo  "$('#todate').attr('disabled',false);";                
                endif; 
            ?>
        }); 
    </script>
<script type="text/javascript">
        $(function(){
            $(window).load(function(){
                loadPlan();
                if ( $('[type="date"]').prop('type') != 'date' ) {
                    $('[type="date"]').datepicker();
                }
            });

            $('#schooltext').hide();                        
                        
        });
        /*$(document).on('click', '#export', function(){
            $.ajax({
                type: 'POST',
                url: '../plan/Export',
                data: $("#searchform" ).serialize()
            });
        });*/
    </script>    