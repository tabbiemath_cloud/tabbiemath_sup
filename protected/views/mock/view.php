<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
	'Worksheet'=>array('index'),
	$model->TM_MK_Id,
);

$this->menu=array(
    array('label'=>'Manage Worksheet', 'class'=>'nav-header'),
	array('label'=>'List Worksheet', 'url'=>array('admin')),
	array('label'=>'Create Worksheet', 'url'=>array('create')),
	array('label'=>'Update Worksheet', 'url'=>array('update', 'id'=>$model->TM_MK_Id)),
	array('label'=>'Manage Questions', 'url'=>array('managequestions', 'id'=>$model->TM_MK_Id)),
	array('label'=>'Delete Worksheet', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_MK_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>
<h3>View <?php echo $model->TM_MK_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_MK_Name',
		'TM_MK_Description',
        array(
            'name'=>'TM_MK_Publisher_Id',
            'type'=>'raw',
            'value'=>Publishers::model()->findByPk($model->TM_MK_Publisher_Id)->TM_PR_Name,
        ),
        array(
            'name'=>'TM_MK_Syllabus_Id',
            'type'=>'raw',
            'value'=>Syllabus::model()->findByPk($model->TM_MK_Syllabus_Id)->TM_SB_Name,
        ),
        array(
            'name'=>'TM_MK_Standard_Id',
            'type'=>'raw',
            'value'=>Standard::model()->findByPk($model->TM_MK_Standard_Id)->TM_SD_Name,
        ),
        array(
            'label'=>'Status',
            'type'=>'raw',
            'value'=>Mock::itemAlias("MockStatus",$model->TM_MK_Status),
        ),
        array(
            'name'=>'TM_MK_Availability',
            'type'=>'raw',
            'value'=>Mock::itemAlias("MockAvail",$model->TM_MK_Availability),
        ),
			array(
					'name'=>'TM_MK_Resource',
					'type'=>'raw',
					'value'=>Mock::itemAlias("MockResource",$model->TM_MK_Resource),
			),
        'TM_MK_Tags',
        array(
            'name'=>'TM_MK_Worksheet',
            'visible'=>$model->TM_MK_Worksheet!="", 
            'value'=>$model->TM_MK_Worksheet,           
        ),
        array(
            'name'=>'TM_MK_Solution',
            'visible'=>$model->TM_MK_Solution!="", 
            'value'=>$model->TM_MK_Solution, 
        ), 

       array(
            'name'=>'TM_MK_Link_Resource',
            'type'=>'raw',
            'value'=>Mock::itemAlias("Linkresource",$model->TM_MK_Link_Resource),
        ),               
	),'htmlOptions' => array('class' => 'table table-bordered table-hover')
)); ?>
    </div>
</div>
