<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
	'Manage Mock Tests'=>array('admin'),
	'Create Mock Tests'=>array('create'),
	'Manage Mock Test Questions',
);

$this->menu=array(
    array('label'=>'Manage Mock', 'class'=>'nav-header'),
	array('label'=>'List Mock', 'url'=>array('admin')),
	array('label'=>'Create Mock', 'url'=>array('create')),
    array('label'=>'Update Mock', 'url'=>array('update', 'id'=>$model->TM_MK_Id)),
    array('label'=>'View Mock', 'url'=>array('view', 'id'=>$model->TM_MK_Id)),
    array('label'=>'Delete Mock', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_MK_Id),'confirm'=>'Are you sure you want to delete this item?')),
);

?>
<h3>Manage Mock Questions</h3>
<div class="panel panel-yellow">
    <div class="panel-heading">
        <h3 class="panel-title">Mock test</h3>
    </div>
    <table class="table">
        <tr>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Name')); ?></label>
                <h4> <?php echo CHtml::encode($model->TM_MK_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Description')); ?> </label>
                <h4> <?php echo CHtml::encode($model->TM_MK_Description); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Publisher_Id')); ?> </label>
                <h4> <?php echo CHtml::encode($model->publisher->TM_PR_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Syllabus_Id')); ?> </label>
                <h4> <?php echo CHtml::encode($model->syllabus->TM_SB_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Standard_Id')); ?> </label>
                <h4> <?php echo CHtml::encode($model->standard->TM_SD_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Status')); ?> </label>
                <h4> <?php echo CHtml::encode(Mock::itemAlias("MockStatus",$model->TM_MK_Status)); ?></h4>
            </td>
            <td>
                <a href="<?php echo Yii::app()->createUrl('mock/view',array('id'=>$model->TM_MK_Id))?>"><button type="button" class="btn btn-warning btn-block" >Save</button></a>
            </td>

        </tr>
    </table>
</div>
<div class="panel panel-yellow addedqstable">
    <div class="panel-heading">
        <h3 class="panel-title">Questions Added</h3>
    </div>
    <div class="scrol-body">
        <table class="table table-striped table-bordered ">
            <thead>
            <tr>
<!--                <th>Chapter</th>
                <th>Topic</th>-->
                <th>Question</th>
                <th>Type</th>
                <th>Reference</th>
                <th>Pattern</th>
                <th class="td-actions"></th>
            </tr>
            </thead>
            <tbody id="removequestionlist">
            <?php

                if($addedQuestion!=''):
                    foreach($addedQuestion AS $mockquestion):
                    $question=Questions::model()->findByPk($mockquestion->TM_MQ_Question_Id);
            ?>
                        <tr id="removequestion<?php echo $question->TM_QN_Id;?>">
<!--                            <td><?php /*echo Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name;*/?></td>
                            <td><?php /*echo Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name;*/?></td>-->
                            <td><?php echo $question->TM_QN_Question; ?></td>
                            <td><?php echo Types::model()->findByPk($question->TM_QN_Type_Id)->TM_TP_Name;?></td>
                            <td><?php echo $question->TM_QN_QuestionReff;?></td>
                            <td><?php echo $question->TM_QN_Pattern;?></td>
                            <td class="td-actions">
                                <button class="btn btn-warning btn-block removequestion" data-id="<?php echo base64_encode($question->TM_QN_Id);?>" data-element="removequestion<?php echo $question->TM_QN_Id;?>">Remove</button>
                            </td>
                        </tr>
            <?php   endforeach;
                else:
            ?>
                    <tr>
                        <td colspan="5">No Questions added yet</td>
                    </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>
<div class="panel panel-yellow">
    <div class="panel-heading">
        <h3 class="panel-title">Filter Questions</h3>
    </div>
    <form id="Filterform">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Chapter</th>
            <th>Topic</th>
            <th>Type</th>
            <th>Reference</th>
            <th>Pattern</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?php
                $standards = CHtml::listData(Chapter::model()->findAll(array('condition' => "TM_TP_Standard_Id =  $model->TM_MK_Standard_Id AND TM_TP_Status = '0'", 'order' => 'TM_TP_order ASC')), 'TM_TP_Id', 'TM_TP_Name');
                $selectarray = array('empty' => 'Select Chapter', 'class' => 'form-control','ajax' =>
                    array(
                        'type'=>'POST', //request type
                        'url'=>CController::createUrl('questions/GetTopicList'), //url to call.
    //                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                        //Style: CController::createUrl('currentController/methodToCall')
                        'update'=>'#topic', //selector to update
                        'data'=>'js:{id:$(this).val()}'
                        //leave out the data key to pass all form values through
                    ));
                echo CHtml::dropDownList('chapter', '',$standards,$selectarray);
                ?>
            </td>
            <td>
                <?php
                echo CHtml::dropDownList('topic', '',array(''=>'Select Topic'),array('class'=>'form-control'));
                ?>
            </td>
            <td>
                <?php
                echo CHtml::dropDownList('type', '',CHtml::listData(Types::model()->findAll(array("condition"=>"TM_TP_Status = '0'",'order'=>'TM_TP_Id')),'TM_TP_Id','TM_TP_Name'),array('empty'=>'Select Type','class'=>'form-control'));
            ?></td>


            <td>
        <?php
                //$refs=Questions::model()->getReffs();
            ?>
                <input type="text" class="form-control" name="reference" id="reference">
                    
            </td>
            <td>
        <?php
                //$refs=Questions::model()->getReffs();
            ?>
                <input type="text" class="form-control" name="pattern" id="pattern">

            </td>

            <td class="td-actions">
                <input type="hidden" name="publisher" value="<?php echo $model->TM_MK_Publisher_Id;?>">
                <input type="hidden" name="syllabus" value="<?php echo $model->TM_MK_Syllabus_Id;?>">
                <input type="hidden" name="standard" value="<?php echo $model->TM_MK_Standard_Id;?>">
                <input type="hidden" name="mock" value="<?php echo $model->TM_MK_Id;?>">
                <button type="button" class="btn btn-warning btn-block" id="fliterquestion">Search</button>
            </td>
        </tr>
        </tbody>
    </table>
    </form>
</div>
<div class="panel panel-yellow listqstable">
    <div class="panel-heading">
        <h3 class="panel-title">Select Question To Add</h3>
    </div>
    <div class="scrol-body">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
<!--                <th>Chapter</th>
                <th>Topic</th>-->
                <th>Question</th>
                <th>Type</th>
                <th>Reference</th>
                <th>Pattern</th>
                <th class="td-actions"></th>
            </tr>
            </thead>
            <tbody id="addqiestionlist">
               <td colspan="6">Search questions to add</td>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#fliterquestion').click(function(){
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl;?>/mock/getquestions',
                data: $('#Filterform').serialize(),
                beforeSend: function(){
                    $('#addqiestionlist').html('<tr><td colspan="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader"></td></tr>');
                },
                success: function(data){
                    $('#addqiestionlist').html(data);
                }
            });
        });
    });
    $(document).on('click','.addquestion',function(){
        var question=$(this).attr('data-id');
        var questionrow=$(this).attr('data-element');
        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/mock/addquestion',
            dataType: "json",
            data: {mock:'<?php echo base64_encode($model->TM_MK_Id);?>',question:question},
            success: function(data){
                if(data.success=='yes')
                {
                    $('#'+questionrow).remove();
                    if(data.count=='0')
                    {
                        $('#removequestionlist').html(data.insertrow);
                    }
                    else
                    {
                        $('#removequestionlist').append(data.insertrow);
                    }

                }
            }
        });
    });
    $(document).on('click','.removequestion',function(){
        var question=$(this).attr('data-id');
        var questionrow=$(this).attr('data-element');
        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/mock/removequestion',
            dataType: "json",
            data: {mock:'<?php echo base64_encode($model->TM_MK_Id);?>',question:question},
            success: function(data){
                if(data.success=='yes')
                {
                    $('#'+questionrow).remove();
                    $('#fliterquestion').trigger('click');
                }
            }
        });
    });

    $(".listqstable .scrol-body").mCustomScrollbar({
        setHeight:500
    });
</script>
