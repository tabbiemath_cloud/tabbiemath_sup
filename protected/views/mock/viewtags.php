<?php

$this->menu=array(
    array('label'=>'Manage Tags', 'class'=>'nav-header'),
    array('label'=>'List Tags', 'url'=>array('tags')),
    array('label'=>'Create Tags', 'url'=>array('createtags')),
    array('label'=>'Update Tags', 'url'=>array('updatetags', 'id'=>$model->TM_MT_Id)),
    //array('label'=>'Delete Tags', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_MT_Id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        //$id=$model->TM_SD_Status;
        //$userid=$model->TM_SD_CreatedBy;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_MT_Id',
                array(
                    'name'=>'TM_MT_Syllabus_Id',
                    'type'=>'raw',
                    'value'=>Syllabus::model()->findByPk($model->TM_MT_Syllabus_Id)->TM_SB_Name,
                ),
                array(
                    'name'=>'TM_MT_Standard_Id',
                    'type'=>'raw',
                    'value'=>Standard::model()->findByPk($model->TM_MT_Standard_Id)->TM_SD_Name,
                ),
                array(
                    'name'=>'TM_MT_Type',
                    'type'=>'raw',
                    'value'=>MockTags::itemAlias("MockResource",$model->TM_MT_Type),
                ),
                'TM_MT_Tags',
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
