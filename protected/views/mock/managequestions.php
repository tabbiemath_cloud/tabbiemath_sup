<?php
/* @var $this MockController */
/* @var $model Mock */

$this->breadcrumbs=array(
	'Manage Worksheet Tests'=>array('admin'),
	'Create Worksheet Tests'=>array('create'),
	'Manage Worksheet Test Questions',
);

$this->menu=array(
    array('label'=>'Manage Worksheet', 'class'=>'nav-header'),
	array('label'=>'List Worksheet', 'url'=>array('admin')),
	array('label'=>'Create Worksheet', 'url'=>array('create')),
    array('label'=>'Update Worksheet', 'url'=>array('update', 'id'=>$model->TM_MK_Id)),
    array('label'=>'View Worksheet', 'url'=>array('view', 'id'=>$model->TM_MK_Id)),
    array('label'=>'Delete Worksheet', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_MK_Id),'confirm'=>'Are you sure you want to delete this item?')),
);

?>
<style>
    .textcolor{
        color: #f0ad4e;
    }
    .switch {
        position: relative;
        display: inline-block;
        width: 44px;
        height: 24px;
    }
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }
    .slider:before {
        position: absolute;
        content: "";
        height: 20px;
        width: 20px;
        left: 1px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }
    input:checked + .slider {
        background-color: #f68b01;
    }
    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }
    input:checked + .slider:before {
        -webkit-transform: translateX(21px);
        -ms-transform: translateX(21px);
        transform: translateX(21px);
    }
    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }
    .slider.round:before {
        border-radius: 50%;
    }
    /* radio button */
    .addedqstable .switch {
        width: 15px;
        height: 15px;
    }
    .addedqstable input:checked + .slider:before {
        -webkit-transform: translateX(1px);
        -ms-transform: translateX(1px);
        transform: translateX(1px);
        border: 2px solid white;
        background: #f68b01;
        width: 11px;
        height: 11px;
    }
    .addedqstable input + .slider:before {
        -webkit-transform: translateX(1px);
        -ms-transform: translateX(1px);
        transform: translateX(1px);
        border: 2px solid white;
        background: #dadada;
        width: 11px;
        height: 11px;
    }
    #question_type
    {
        width: 189px;
    }
    #skills
    {
        width: 130px;

    }
</style>
<h3>Manage Worksheet Questions</h3>
<div class="panel panel-yellow">
    <div class="panel-heading">
        <h3 class="panel-title">Worksheet test</h3>
    </div>
    <table class="table">
        <tr>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Name')); ?></label>
                <h4> <?php  echo CHtml::encode($model->TM_MK_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Description')); ?> </label>
                <h4> <?php echo CHtml::encode($model->TM_MK_Description); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Publisher_Id')); ?> </label>
                <h4> <?php echo CHtml::encode($model->publisher->TM_PR_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Syllabus_Id')); ?> </label>
                <h4> <?php echo CHtml::encode($model->syllabus->TM_SB_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Standard_Id')); ?> </label>
                <h4> <?php echo CHtml::encode($model->standard->TM_SD_Name); ?></h4>
            </td>
            <td>
                <label class="font18"><?php echo CHtml::encode($model->getAttributeLabel('TM_MK_Status')); ?> </label>
                <h4> <?php echo CHtml::encode(Mock::itemAlias("MockStatus",$model->TM_MK_Status)); ?></h4>
            </td>
            <td>
                <a href="<?php echo Yii::app()->createUrl('mock/savestatus',array('id'=>$model->TM_MK_Id))?>"><button type="button" class="btn btn-warning btn-block" ><?php echo ($model->TM_MK_Status==0?'Deactivate':'Activate');?></button></a>
            </td>

        </tr>
    </table>
</div>
<div class="panel panel-yellow addedqstable">
    <div class="panel-heading">
        <h3 class="panel-title">Questions Added (<span id="questioncount"><?=$questioncount;?></span>) <span id="mocktotal" class="pull-right"> </span></h3> 
    </div>
    <div class="scrol-body" style="overflow-x: scroll;">
        <table class="table table-striped table-bordered ">
            <thead>
            <tr>
<!--                <th>Chapter</th>
                <th>Topic</th>-->
                <th>Order</th>
                <th class="td-actions"></th>
		<th>Question ID</th>
                <th>Question</th>
                <th>Mark</th>
                <th>Type</th>
                <th>Difficulty</th>
                <th>Reference</th>
                <th>Pattern</th>
                <th>Skill</th>
                <th>Info</th>
                
               
                                
            </tr>
            </thead>
            <tbody id="removequestionlist">
            <?php

                if($addedQuestion!=''):
                    foreach($addedQuestion AS $mockquestion):
                    $question=Questions::model()->findByPk($mockquestion->TM_MQ_Question_Id);
            ?>
                        <tr id="removequestion<?php echo $question->TM_QN_Id;?>" class="order<?php echo $mockquestion->TM_MQ_Order?>qn" data-rawid="<?php echo $question->TM_QN_Id;?>" data-reorder="<?php echo $mockquestion->TM_MQ_Order?>">
<!--                            <td><?php /*echo Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name;*/?></td>
                            <td><?php /*echo Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name;*/?></td>-->
			    <td>
                                    <input TYPE="image" id="orderlimkup<?php echo $question->TM_QN_Id;?>" class="order_link" data-info="up" SRC="<?php echo Yii::app()->request->baseUrl.'/images/up.gif'?>" ALT="SUBMIT" data-id="<?php echo $mockquestion->TM_MQ_Mock_Id?>" data-order="<?php echo $mockquestion->TM_MQ_Order?>" data-qnId="<?php echo $question->TM_QN_Id;?>">
                                    <input TYPE="image" id="orderlimkdown<?php echo $question->TM_QN_Id;?>" class="order_link" data-info="down" SRC="<?php echo Yii::app()->request->baseUrl.'/images/down.gif'?>" ALT="SUBMIT" data-id="<?php echo $mockquestion->TM_MQ_Mock_Id?>" data-order="<?php echo $mockquestion->TM_MQ_Order?>" data-qnId="<?php echo $question->TM_QN_Id;?>">
                             </td>
                            <td class="td-actions">
                                <a class="removequestion" data-id="<?php echo base64_encode($question->TM_QN_Id);?>" data-element="removequestion<?php echo $question->TM_QN_Id;?>"><span style="color: #848484; cursor: pointer; " class="glyphicon glyphicon-remove"></span></a>
                                <?php if($mockquestion->TM_MQ_Header!=''){ ?>
                                    <a href="javascript:void(0)" style="color: #848484 !important;" class="sectionheader delete" data-backdrop="static" data-toggle="modal" data-target="#SelectHeader" data-id="<?php echo $mockquestion->TM_MQ_Mock_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>" ><span style="font-size: 13px;"  class="glyphicon glyphicon-header textcolor"></span></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" style="color: #848484 !important;" class="sectionheader" data-backdrop="static" data-toggle="modal" data-target="#SelectHeader" data-id="<?php echo $mockquestion->TM_MQ_Mock_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>" ><span style="font-size: 13px;" class="glyphicon glyphicon-header"></span></a>
                                <?php } ?>
                                <?php
                                $questionoptions=MockQuestions::model()->find(array('condition'=>"TM_MQ_Mock_Id='".$mockquestion->TM_MQ_Mock_Id."' AND TM_MQ_Question_Id='".$question->TM_QN_Id."' "));
                                if($questionoptions['TM_MQ_Show_Options']==0)
                                {
                                ?>
                                <label class="switch" title="MCQ">
                                <input class="save" id="saveoptions" type="checkbox" data-id="<?php echo $mockquestion->TM_MQ_Mock_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>" name="option" >
                                <span class="slider round"></span>
                                </label>
                            <?php } elseif($questionoptions['TM_MQ_Show_Options']==1) { ?>
                                <label class="switch" title="MCQ">
                                <input class="save" id="saveoptions" type="checkbox" checked="checked" data-id="<?php echo $mockquestion->TM_MQ_Mock_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>"  name="option">
                                <span class="slider round"></span>
                                </label>
                            <?php } ?>

                                <!-- <a href="javascript:void(0)" style="color: #848484 !important;" class="qstnoptions" data-backdrop="static" data-toggle="modal" data-target="#Selectoptions" data-id="<?php echo $mockquestion->TM_MQ_Mock_Id;?>" data-value="<?php echo $question->TM_QN_Id;?>" ><span class="glyphicon glyphicon-info-sign"></span></a> -->

                            </td>
                            <td><?php echo $question->TM_QN_Id; ?></td>
                            <td><?php echo $question->TM_QN_Question; ?></td>
                            <td class="marks"><?php echo $this->Getmarks($question->TM_QN_Id);?></td>
                            <td><?php echo Types::model()->findByPk($question->TM_QN_Type_Id)->TM_TP_Name;?></td>
                            <td><?php echo Difficulty::model()->findByPk($question->TM_QN_Dificulty_Id)->TM_DF_Name;?></td>
                            <td><?php echo $question->TM_QN_QuestionReff;?></td>
                            <td><?php echo $question->TM_QN_Pattern;?></td>

                            <td><?php 
                                $skill_ids=explode(",",$question->TM_QN_Skill);
                                $skill_names=array();
                               foreach ($skill_ids as $key => $skill_id) {
                                $skill_name = TmSkill::model()->findByPk($skill_id)->tm_skill_name;
                                array_push($skill_names,$skill_name);

                               }
                               $skills=implode(", ",$skill_names);
                               echo $skills;?>
                            </td>
                            <td><?php 
                            $type_ids=explode(",",$question->TM_QN_Question_type);
                            $type_names=array();
                            foreach ($type_ids as $key => $type_id) {
                             $type_name = QuestionType::model()->findByPk($type_id)->TM_Question_Type; 
                             array_push($type_names,$type_name);

                            }
                            $types=implode(",",$type_names); echo  $types;  ?>
                                
                            </td>
                            


                        </tr>
            <?php   endforeach;
                else:
            ?>
                    <tr>
                        <td colspan="9">No Questions added yet</td>
                    </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>
<div class="panel panel-yellow">
    <div class="panel-heading">
        <h3 class="panel-title">Filter Questions</h3>
    </div>
    <form id="Filterform">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Chapter</th>
            <th>Topic</th>
            <th>Type</th>
            <th>Difficulty</th>
            <th>Reference</th>
            <th>Mark</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <?php
                $standards = CHtml::listData(Chapter::model()->findAll(array('condition' => "TM_TP_Standard_Id =  $model->TM_MK_Standard_Id AND TM_TP_Status = '0'", 'order' => 'TM_TP_order ASC')), 'TM_TP_Id', 'TM_TP_Name');
                $selectarray = array('empty' => 'Select Chapter', 'class' => 'form-control','ajax' =>
                    array(
                        'type'=>'POST', //request type
                        'url'=>CController::createUrl('questions/GetTopicList'), //url to call.
    //                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                        //Style: CController::createUrl('currentController/methodToCall')
                        'update'=>'#topic', //selector to update
                        'data'=>'js:{id:$(this).val()}'
                        //leave out the data key to pass all form values through
                    ));
                echo CHtml::dropDownList('chapter', '',$standards,$selectarray);
                ?>
            </td>
            <td>
                <?php
                echo CHtml::dropDownList('topic', '',array(''=>'Select Topic'),array('class'=>'form-control'));
                ?>
            </td>
            <td>
                <?php
                echo CHtml::dropDownList('type', '',CHtml::listData(Types::model()->findAll(array("condition"=>"TM_TP_Status = '0'",'order'=>'TM_TP_Id')),'TM_TP_Id','TM_TP_Name'),array('empty'=>'Select Type','class'=>'form-control'));
            ?></td>
            <td>
                <?php
                echo CHtml::dropDownList('dificulty', '',CHtml::listData(Difficulty::model()->findAll(array('order' => 'TM_DF_Name')),'TM_DF_Id','TM_DF_Name'),array('empty'=>'Select Type','class'=>'form-control'));
            ?></td>


            <td>
        <?php
                //$refs=Questions::model()->getReffs();
            ?>
                <input type="text" class="form-control" name="reference" id="reference">
                    
            </td>

            <td>
                <input type="text" class="form-control" name="mark" id="mark">
            </td>
            <td></td>
        </tr>
        <tr>
            <th>Pattern</th>
            <th>Status</th>
            <th>Skill</th>
            <th>Additional Info</th>
            <th>Notes</th>
            <th colspan="1"></th>
            <th></th>
        </tr>
        <tr>
            
            <td>
         
                <input type="text" class="form-control" name="pattern" id="pattern">

            </td>



            <td>
                <?php
                echo CHtml::dropDownList('status', '',Questions::itemAlias('QuestionStatus'),array('empty'=>'Select Status','class'=>'form-control'));
            ?></td>
            <td>
                <?php 
                echo CHtml::dropDownList('skills', '',CHtml::listData(TmSkill::model()->findAll(array("condition"=>"tm_standard_id = ".$model->standard->TM_SD_Id."",'order' => 'id')),'id','tm_skill_name'),array('empty'=>'Select Skills','class'=>'form-control'));
            ?>
            </td>

            <td>
                <?php
                echo CHtml::dropDownList('question_type', '',CHtml::listData(QuestionType::model()->findAll(array("condition"=>"tm_standard_id = ".$model->standard->TM_SD_Id."",'order' => 'id')),'id','TM_Question_Type'),array('empty'=>'Select Info','class'=>'form-control','multiple'=>true));
            ?>
            </td>
            <td>
            <?php
                //$refs=Questions::model()->getReffs();
            ?>
                <input type="text" class="form-control" name="question_info" id="question_info">

            </td>
            <td colspan="1"></td>

                <td class="td-actions">
                <input type="hidden" name="publisher" value="<?php echo $model->TM_MK_Publisher_Id;?>">
                <input type="hidden" name="syllabus" value="<?php echo $model->TM_MK_Syllabus_Id;?>">
                <input type="hidden" name="standard" value="<?php echo $model->TM_MK_Standard_Id;?>">
                <input type="hidden" name="mock" value="<?php echo $model->TM_MK_Id;?>">
                <button type="button" class="btn btn-warning btn-block" id="fliterquestion">Search</button>
            </td>
        </tr>
        </tbody>
    </table>
    </form>
</div>
<div class="panel panel-yellow listqstable">
    <div class="panel-heading">
        <h3 class="panel-title">Select Question To Add</h3>
    </div>
    <div class="scrol-body" style="overflow: scroll;width:100%;height: 500px;">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
<!--                <th>Chapter</th>
                <th>Topic</th>-->
                <th class="td-actions"></th>
				<th>ID</th>
                <th>Question</th>
                <th>Type</th>
                <th>Chapter</th>
                <th>Topic</th>
                <th>Skill</th>
                <th>Info</th>
                <th>Reference</th>
                <th>Pattern</th>
                
            </tr>
            </thead>
            <tbody id="addqiestionlist">
               <td colspan="6">Search questions to add</td>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="SelectHeader">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Section Header</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body form-horizontal payment-form">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Header</label>
                            <div class="col-sm-7">
                                <input class="form-control" type="text" id="practiceheader" name="Headername">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Instructions</label>
                            <div class="col-sm-7">
                                <textarea name="instructions" class="form-control" id="instructions"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="headquestionid">
                <input type="hidden" id="practiceid">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="addsectionhead">Add</button>
                <button style="display: none;" type="button" class="btn btn-warning"  id="deleteheader">Delete</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal" id="inviteclose">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    function gettotal()
    {
        var totalmark=0;
        $('.marks').each(function(){
            totalmark=totalmark+($(this).html()*1);
        });
        $('#mocktotal').html('Total Marks: '+totalmark)        
    }
    $(function(){
        gettotal();
        $('#fliterquestion').click(function(){
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl;?>/mock/getquestions',
                data: $('#Filterform').serialize(),
                beforeSend: function(){
                    $('#addqiestionlist').html('<tr><td colspan="5"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" class="preloader"></td></tr>');
                },
                success: function(data){
                    $('#addqiestionlist').html(data);
                    MathJax.Hub.Queue(["Typeset",MathJax.Hub, "page-wrapper"]);
                }
            });
        });
    });
    //$('.sectionheader').click(function(){
    $(document).on('click touchstart','.sectionheader',function(){
        var practquestid = $(this).data('value');
        var hwid=$(this).attr('data-id');
        $('#headquestionid').val(practquestid);
        $('#practiceid').val(hwid);
        $('#deleteheader').hide();
        $.ajax({
            type: 'POST',
            dataType:'json',
            url: '<?php echo Yii::app()->request->baseUrl."/mock/GetHeaderCustom";?>',
            data: {questionid:practquestid,homeworkid:hwid},
            success: function(data){
                $('#practiceheader').val(data.header);
                $('#instructions').val(data.section);
            }
        });
    });
    $(document).on('click','.delete',function(){
        $('#deleteheader').show();
    });
    $('#addsectionhead').click(function(){
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('mock/AddSection');?>',
                beforeSend : function(){
                    $("#addsectionhead").prop("disabled", true);
                },
                data: {qnids: $('#headquestionid').val(),practiceid:$('#practiceid').val(),header:$('#practiceheader').val(),section:$('#instructions').val()}
            })
            .done(function (data) {
                $("#addsectionhead").prop("disabled", false);
                setTimeout(function() {
                    $('#SelectHeader').modal('toggle');
                    ///window.location.reload();
                    location.reload();
                }, 1000);
            });
    });
    $(document).on('click','.addquestion',function(){
        var question=$(this).attr('data-id');
        var questionrow=$(this).attr('data-element');
        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/mock/addquestion',
            dataType: "json",
            data: {mock:'<?php echo base64_encode($model->TM_MK_Id);?>',question:question},
            success: function(data){
                if(data.success=='yes')
                {
                    $('#'+questionrow).remove();
                    if(data.count=='0')
                    {
                        $('#removequestionlist').html(data.insertrow);
                    }
                    else
                    {
                        $('#removequestionlist').append(data.insertrow);
                    }
                    $('#questioncount').html(data.total);
                    gettotal();
                    MathJax.Hub.Queue(["Typeset",MathJax.Hub, "page-wrapper"]);
                }
            }
        });
    });
    $(document).on('click','#deleteheader',function(){
        var questionid = $('#headquestionid').val();
        var homeworkid = $('#practiceid').val();
        $.ajax({
            type: 'POST',
            dataType:'json',
            url: '<?php echo Yii::app()->request->baseUrl."/mock/deleteheader";?>',
            data: {questionid:questionid,homeworkid:homeworkid},
            success: function(data){
                location.reload();

            }
        });

    });
    $(document).on('click','.removequestion',function(){
        var question=$(this).attr('data-id');
        var questionrow=$(this).attr('data-element');
        $.ajax({
            type: 'POST',
            //async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/mock/removequestion',
            dataType: "json",
            data: {mock:'<?php echo base64_encode($model->TM_MK_Id);?>',question:question},
            success: function(data){
                if(data.success=='yes')
                {
                    $('#'+questionrow).remove();
                    //$('#fliterquestion').trigger('click');
                    $('#questioncount').html(data.count);
                    gettotal();
                }
            }
        });
    });
    $(document).on('click','.order_link',function(){
        var id=$(this).attr('data-id');
        var info=$(this).attr('data-info');

        var order=($(this).attr('data-order')*1);
        var qnId=$(this).attr('data-qnId');
        var orderprev=order-1;
        var ordernext=order+1;

        var prevquestionId=$('.order'+orderprev+'qn').attr('data-rawid');
        var nextquestionId=$('.order'+ordernext+'qn').attr('data-rawid');


        var orderclass=$('.order'+orderprev+'qn');

        var orderclassprev=$('.order'+order+'qn');

        var orderclassnext=$('.order'+ordernext+'qn');


        $.ajax({
            type: 'POST',
            async: false,
            url: '<?php echo Yii::app()->request->baseUrl;?>/mock/MockOrder',
            dataType: "text",
            data: {id:id,info:info,order:order},
            success: function(data){
                if(data=='yes')
                {
                    if(info=='up'){

                        orderclass.insertAfter(orderclassprev);

                        $('#orderlimkup'+qnId).attr('data-order',order-1);
                        $('#orderlimkdown'+qnId).attr('data-order',order-1);
                        orderclassprev.toggleClass('order'+order+'qn order'+orderprev+'qn');
                        orderclass.toggleClass('order'+orderprev+'qn order'+order+'qn');
                        $('#orderlimkup'+prevquestionId).attr('data-order',order);
                        $('#orderlimkdown'+prevquestionId).attr('data-order',order);

                    }
                    else{

                        orderclassprev.insertAfter(orderclassnext);

                        $('#orderlimkup'+qnId).attr('data-order',order+1);
                        $('#orderlimkdown'+qnId).attr('data-order',order+1);

                        $('.order'+ordernext+'qn').toggleClass('order'+ordernext+'qn  order'+order+'qn');
                        ordernext=order+1;
                        orderclassprev.toggleClass('order'+order+'qn  order'+ordernext+'qn');
                        $('#orderlimkup'+nextquestionId).attr('data-order',order);
                        $('#orderlimkdown'+nextquestionId).attr('data-order',order);





                    }

                }
            }
        });
    });
    $(document).on('change','#saveoptions',function(){


        if($(this).is(':checked'))
        {
            var option = 1;
        }
        else
        {
            var option =0;
        }

            
            

        var qnids = $(this).data('value');
        var hwid  =  $(this).attr('data-id');
        $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createUrl('mock/qstnoptions');?>',
                data: {qnids:qnids ,practiceid:hwid,option:option}
            })
            .done(function (data) {
            });
    });

</script>
