<?php
/* @var $this MockController */
/* @var $model Mock */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_MK_Id'); ?>
		<?php echo $form->textField($model,'TM_MK_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MK_Name'); ?>
		<?php echo $form->textField($model,'TM_MK_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MK_Description'); ?>
		<?php echo $form->textArea($model,'TM_MK_Description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MK_Publisher_Id'); ?>
		<?php echo $form->textField($model,'TM_MK_Publisher_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MK_Syllabus_Id'); ?>
		<?php echo $form->textField($model,'TM_MK_Syllabus_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_MK_Standard_Id'); ?>
		<?php echo $form->textField($model,'TM_MK_Standard_Id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->