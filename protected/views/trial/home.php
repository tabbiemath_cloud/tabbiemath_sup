    <style type="text/css">
    #wrapper
    {
        padding-left:0px !important;
    }
</style>

<div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3 main">

    <div class="contentarea">
        <form action="" method="post" id="trialform">
            <div class="col-md-7 col-md-offset-2" style="margin-top: 20px;">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please select syllabus and standard</h3>
                    </div>
                    <div class="panel-body">
                        <!--<h5></h5>-->
                        <form action="/dev/tabbiemestaging/user/login.html" method="post"> 
                        <fieldset>
                                <?php
                                    $syllabuscount=Syllabus::model()->with('plan')->count(array('condition'=>'TM_SB_Status=0 AND TM_PN_Visibility=0'));                                  
                                    $syllabus=Syllabus::model()->with('plan')->findAll(array('condition'=>'TM_SB_Status=0 AND TM_PN_Visibility=0'));
                                    if($syllabuscount>1):
                                ?>
                                <div class="form-group">                                
                                    <label for="exampleInputEmail1">Select Syllabus</label>                                    
                                    <select name="syllabusdrop" id="syllabusdrop" class="form-control validate" data-item="Syllabus" >
                                        <option value="">Select Syllabus</option>
                                        <?php foreach($syllabus as $syllabus):?>

                                            <?php echo '<option value="'.$syllabus->TM_SB_Id.'">'.$syllabus->TM_SB_Name.' </option>';
                                        endforeach;
                                        ?>

                                    </select>                                     
                                </div>
                                <?php else:
                                    $syllabus=Syllabus::model()->with('plan')->find(array('condition'=>'TM_SB_Status=0 AND TM_PN_Visibility=0'));
                                ?>                                    
                                    <input type="hidden" name="syllabusdrop" id="syllabusdrop"  value="<?php echo $syllabus->TM_SB_Id;?>" />
                                <?php endif;?>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Select Standard</label>
                                    <select id="standard" name="standard" class="form-control validate"  data-item="Standard">
                                        <option value="">Select Standard</option>
                                    </select>                                                   </div>

                                <!-- Change this to a button or input when using this as a form -->
                                <button name="startTrial" type="submit" class="btn btn-warning sanfont cmplt" id="trialbutton"
                                        style="margin-right:20px;width: 155px;">Proceed
                                </button>
                            </fieldset>
                        </form>            </div>
                </div>
            </div>
        </form>

        </div>
    </div>


        <?php 
if($syllabuscount==1):
        Yii::app()->clientScript->registerScript('syllabusscrupt', "
          $.ajax({
                type:'POST',
                url:'".CController::createUrl('trial/Getstandard')."',
                data:{id:'".$syllabus->TM_SB_Id."'},
            }).done(function( data ) {
              $('#standard').html(data);
            });            
        ");
endif;
        
        
        Yii::app()->clientScript->registerScript('trialscript', "
 $('#syllabusdrop').change(function()
     {
         var syllabus = $('#syllabusdrop').val();

          $.ajax({
                type:'POST',
                url:'".CController::createUrl('trial/Getstandard')."',
                data:{id:syllabus},
            }).done(function( data ) {
              $('#standard').html(data);
            });

});
    $(document).on('submit','#trialform',function(){
        $('.error').remove();
        var count=0;
        $('.validate').each(function(){
            if($(this).val()=='')
            {
                var item=$(this).attr('data-item');
                $(this).after( '<div class=\"error alert alert-danger\">Select '+item+'</div>' );
                count++
            }
        })
        if(count==0)
        {
        return true;
        }
        else
        {
        return false;
        }

    });

");
        ?>

