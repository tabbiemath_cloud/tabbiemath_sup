<style type="text/css">
    .cls
    {
        position: absolute;
        margin-top: -21px;
        right: 5px;
        background-color: rgb(19, 19, 19);
        opacity: 10;
        color: white;
        font-size: 16px;
        /* padding: 10px; */
        height: 20px;
        line-height: 17px;
        width: 20px;
        text-align: center;
        border-radius: 33px;
    }
</style>
<script type="text/javascript">
  
    jQuery(document).ready(function($){
        $('.pie_progress2').asPieProgress({
            namespace: 'pie_progress2'
        });       
		$('.pie_progress2').asPieProgress('go');        
    });
</script>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <div class="contentarea">
        <div class="bs-example">
            <h1 class="h125" >View History</h1>
            <div class="modal-body">


                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation"class="active"><a href="#revisions" aria-controls="revisions" role="tab"
                                                   data-toggle="tab">My Revisions</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">



                        <div role="tabpanel" class="tab-pane fade in active" id="revisions">
                            <div class="panel panel-default">
                                <div class="tab-pane fade active in" id="revisions">
                                        <table class="rwd-tables table-mc-light-blue">
                                                <tbody>
                                                <tr>
												<th><b>Topic</b></th>
												<th><b>Date</b></th>
												<th class="Score"><b>Score</b></th>
												<th></th>
												<th></th>
												<th></th>                                                   
                                                    <!--<th> </th>-->
                                                </tr>
                                            <?php
                                            if(count($totaltests)>0):
                                            $count=0;
                                            foreach ($totaltests AS $viewTotaltests):
                                                $chapters=StudentTestChaptersTrial::model()->findAll(array('condition'=>'TM_TR_STC_Test_Id='.$viewTotaltests->TM_TR_STU_TT_Id))
                                                ?>

                                                <tr>
                                                    <td><span class="rwd-tables thead"><b>Topic </b></span><span class="rwd-tables tbody"><?php
                                                            foreach($chapters AS $key=>$chapter):
                                                                echo ($key!='0'?' ,':'').$chapter->chapters->TM_TP_Name;
                                                            endforeach;?></span></td>



                                                    <td><span class="rwd-tables thead"><b>Date</b></span><span class="rwd-tables tbody"><?php echo date("d-m-Y", strtotime($viewTotaltests->TM_TR_STU_TT_CreateOn)); ?></span></td>
                                                    <td>
                                                        <div class="pie_progress2 showprogress<?php echo $count;?>" role="progressbar<?php echo $count;?>" data-goal="<?php echo round($viewTotaltests->TM_TR_STU_TT_Percentage);?>" aria-valuemin="0" data-step="2" aria-valuemax="100">
                                                            <span class="pie_progress2__number"><?php echo $viewTotaltests->TM_TR_STU_TT_Percentage."%"?></span>
                                                        </div>

                                                    </td>
                                                    <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><img id="viewsolution" data-toggle="modal" data-target="#solutionModel" class="clickable-row" data-ID="<?php echo $viewTotaltests->TM_TR_STU_TT_Id?>" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" class="hvr-pop mCS_img_loaded"></span></td>
                                                    <!-- <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><a href="#"><span class="rwd-tables thead"></span><span class="glyphicon glyphicon-open hvr-pop" style="font-size: 20px;"></span></a></span></td>-->
                                                    <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody">
												<select class="form-control redochange" data-item="<?php echo $viewTotaltests->TM_TR_STU_TT_Id?>" id="selectRedo<?php echo $viewTotaltests->TM_TR_STU_TT_Id?>">
                                                    <?php if($this->GetRedoCount($viewTotaltests->TM_TR_STU_TT_Id)>0): echo ' <option value="redoflagged">Redo Flagged Only</option>'; endif;?>
                                                    <option value="redoall" selected="selected">Redo All</option>


                                                </select>
												</span></td>
                                                    <td><span class="rwd-tables thead"></span><span class="rwd-tables tbody"><form action="<?php echo Yii::app()->createUrl('trial/trialredo',array('id'=>$viewTotaltests->TM_TR_STU_TT_Id))?>" method="post"><!--<input type="hidden" name="testid" value="<?php /*echo $viewTotaltests->TM_TR_STU_TT_Id*/?>"/>--><input type="hidden" name="redo" id="redohidden<?php echo $viewTotaltests->TM_TR_STU_TT_Id;?>" style="width: 100%;" class="<?php echo $viewTotaltests->TM_TR_STU_TT_Id?>" value="all"> <input type="submit" value="Go" class="redoButton btn btn-warning"/></form> </span></td>
                                                </tr>
                                                <?php                                                
                                                $count++;
                                            endforeach;
                                            else:
                                            ?>
                                            <tr>
                                                <td colspan="6" class="font2" style="text-align: center" align="center"><a href="<?php echo Yii::app()->createUrl('trial/home');?>" class="btn btn-warning">Start Revision</a> </td>

                                                <!--<th> </th>-->
                                            </tr>
                                            <?php endif;?>
                                            </tbody>
                                        </table>


                                </div>
                            </div>
                        </div>


                    </div>

                </div>


            </div>
        </div>


    </div>


</div>


<div class="modal fade " id="solutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>
            </div>
            <div class="panel-heading">
                
            </div>
            <div class="modal-body" >
                <table class="table ">
                    <thead>

                    </thead>
                    <tbody class="font3" id="showsolution">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(document).ready(function () {
        $(".rwd-tables table, .table.rwd-tables").rwdTables();
    });
</script>
<script type="text/javascript">
    $(document).on('click','.hover',function(){
        var id=$(this).attr('data-id');
        var tooltip=$(this).attr('data-tooltip');
        $('.tooltiptable').hide();
        $('#tooltip'+id).fadeIn();
    });
    $(document).on('click','#close',function(){
        $('.tooltiptable').hide();

    });
	
    });
</script>

<?php Yii::app()->clientScript->registerScript('viewhistoryredo', "
$('.redochange').change(function(){

    var item=$(this).attr('data-item');

    $('#redohidden'+item).val($(this).val());
});
$('#viewsolution').click(function(){
var testid=$(this).attr('data-ID');
$.ajax({
type: 'POST',
url: '".Yii::app()->createUrl('trial/GetSolution')."',
data: { testid: testid}
})
.done(function( html ) {
$('#showsolution').html( html );
});
});

");?>
<?php Yii::app()->clientScript->registerScript('sendmail', "
    $(document).on('click','.showSolutionItem',function(){
            $('.sendmail').slideUp('fast');
            $(this).next('.sendmail').slideDown('slow')
    });

    $(document).on('click','#mailSend',function(){
var comments= $('#comments').val();

var questionId=$(this).data('id');

            var questionReff=$(this).data('reff');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {  alert('Message Sens');$('.sendmail').slideUp('fast'); })

    });
");?>
<?php Yii::app()->clientScript->registerScript('sendmailtest', "
    $(document).on('click','.showSolutionItemtest',function(){
            var item= $(this).attr('data-reff');
            $('.sendmailtest').slideUp('fast');
            $('.suggestion'+item).slideDown('slow')
    });

    $(document).on('click','#mailSendtest',function(){
var comments= $('#comments').val();

var questionId=$(this).data('id');

            var questionReff=$(this).data('reff');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        { alert('Message Sens'); $('.sendmailtest').slideUp('fast'); })

    });
");
?>