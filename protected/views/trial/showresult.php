    <style type="text/css">
    .modal-body a {
    color: #EB7900;
    text-decoration: none;
}
    </style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="contentarea">
        <h1 class="h125"> </h1>
        <div class="bs-example">
            <div class="panel panel-default">


                <div class="row">
                    <div class="col-md-4 col-lg-3 col-xs-9 col-lg-offset-0 col-md-offset-0 col-xs-offset-1">
                        <div class="pie_progress" role="progressbar" data-goal="<?php echo $test->TM_TR_STU_TT_Percentage;?>" aria-valuemin="0" aria-valuemax="100">
                            <span class="pie_progress__number">0%</span>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-6 col-xs-12 text12525">
                        <h1 style="font-weight: 700;">You scored <?php echo $test->TM_TR_STU_TT_Mark.'/'.$test->TM_TR_STU_TT_TotalMarks;?></h1>
                        <!--<h4 style="font-size: 14px;">All hai the top scorer !!!</h4>-->
                        <!--<p class="yf">YOU HAVE JUST COMPLETED LEVEL 2</p>-->
                        <h4 style="font-weight: 700;"><?php echo $this->ResultMessage($test->TM_TR_STU_TT_Percentage);?></h4>
                        <a href="javascript:void(0)"  class="btn btn-warning registerclick" style="margin-right:20px;" >Click here to subscribe to TabbieMath</a>
                    </div>

                    <div class="col-md-3" style="text-align: center;">
                        <?php if($test->TM_TR_STU_TT_Percentage>=40):?>
                            <i class="fa fa-thumbs-up fa-6 hvr-buzz-out mCS_img_loaded"></i>
                        <?php else:?>
                            <i class="fa fa-thumbs-down fa-6 hvr-buzz-out mCS_img_loaded"></i>
                        <?php endif;?>
                    </div>
                </div>


            </div>
        </div>

        <div class="bs-example">


            <h1 class="h125" style="padding-top: 0;">Here is your test result</h1>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div style="text-align: right;">
                        View Solution <img id="viewsolution" data-toggle="modal" data-target="#solutionModel" src="<?php echo Yii::app()->request->baseUrl;?>/images/solutions.png" style="margin-left:10px;cursor: pointer">

                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive shadow-z-1">
                    <table class="table" style="background-color: #ffffff;">
                        <tbody class="font3">
                        <?php echo $testresult;?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Please choose your location</h3>
      </div>
      <div class="modal-body">
        <ul class="list-group">
          <li class="list-group-item"><a href="<?php echo Yii::app()->createUrl("user/registration/setlocation",array("id"=>0));?>"><b>India</b></a></li>
          <li class="list-group-item"><a href="<?php echo Yii::app()->createUrl("user/registration/setlocation",array("id"=>1));?>"><b>Outside India</b></a></li>
        </ul> 
		<p>Please Note: We ask you this information so that we can route you the appropriate secure payment gateway</p>	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div class="modal fade " id="solutionModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Solutions</h4>
            </div>
            <div class="panel-heading">
                <div style="text-align: right;">
                    <span style="padding-right: 10px;"><a href="<?php echo Yii::app()->createUrl('trial/getpdfanswer',array('id'=>$test->TM_TR_STU_TT_Id));?>" target="_blank">Print</a></span><a href="<?php echo Yii::app()->createUrl('trial/getpdfanswer',array('id'=>$test->TM_TR_STU_TT_Id));?>" target="_blank"><i class="fa fa-print fa-lg"></i></a>

                </div>
            </div>
            <div class="modal-body" >
                <table class="table" style="background-color: #ffffff;">
                    <thead>

                    </thead>
                    <tbody class="font3" id="showsolution">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(function(){
    $('.registerclick').click(function(){
        $('#myModal').modal('show')
            
    })
    
})
</script>
<?php Yii::app()->clientScript->registerScript('showmarks', "
    $('#viewsolution').click(function(){
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('trial/GetSolution')."',
          data: { testid: '".$test->TM_TR_STU_TT_Id."'}
        })
          .done(function( html ) {
             $('#showsolution').html( html );
          });
    });
	$('.pie_progress').asPieProgress({
            namespace: 'pie_progress'
	});
	$('.pie_progress').asPieProgress('go');
        

");?>

<?php Yii::app()->clientScript->registerScript('sendmail', "
    $(document).on('click','.showSolutionItem',function(){
            var item= $(this).attr('data-reff');
            $('.sendmail').slideUp('fast');
            $('.suggestiontest'+item).slideDown('slow');



    });

    $(document).on('click','#mailSend',function(){
var comments= $('#comments').val();

var questionId=$(this).data('id');

            var questionReff=$(this).data('reff');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {
$('.mailSendComplete'+questionId).slideDown();
setTimeout(function(){
$('.mailSendComplete'+questionId).hide();
$('.sendmail').slideUp();
$('#comments').val('');
}, 6000);
 })

    });
");?>

?>
<?php Yii::app()->clientScript->registerScript('sendmailtest', "
    $(document).on('click','.showSolutionItemtest',function(){
            var item= $(this).attr('data-reff');
            $('.sendmailtest').slideUp('fast');
            $('.suggestion'+item).slideDown('slow')


    });

    $(document).on('click','#mailSendtest',function(){
var comments= $('#comments').val();

var questionId=$(this).data('id');

            var questionReff=$(this).data('reff');
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/SendMail')."',
              data: { questionId: questionId,questionReff: questionReff,comments: comments}

            })
.done  (function(data, textStatus, jqXHR)        {
$('.mailSendCompletetest'+questionId).slideDown();
setTimeout(function(){
$('.mailSendCompletetest'+questionId).hide();
$('.sendmailtest').slideUp();
$('#comments').val('');
}, 6000);
 })

    });
");
?>
<?php Yii::app()->clientScript->registerScript('togglestar', "
$(document).on('click','.startoggle',function(){

if ( $(this).find('span.glyphicon').hasClass('glyphicon-star-empty')) {

var deleteflag=0;
}
else{
var deleteflag=1


}
var displaystar=$(this);


  var questionId=$(this).attr('data-questionId');
  var questionType=$(this).attr('data-questionTypeId');
  var testId=$(this).attr('data-testId');

                $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('student/redo')."',
              data: { questonid: questionId, type: questionType, testid: testId, deleteflag: deleteflag },
              success:function(data)
              {


                    displaystar.find('span.glyphicon').toggleClass('glyphicon-star').toggleClass('glyphicon-star sty');

              }


            });



});");?>