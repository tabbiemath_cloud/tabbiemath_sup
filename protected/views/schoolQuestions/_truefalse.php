<div class="col-lg-12 mater_bg_border">
    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_QuestionTF]" id="Questions_TM_QN_QuestionTF"></textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="form-group">
                    <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImageTF" data-type="question" data-section="main" data-name="TM_QN_ImageTF"  id="Questions_TM_QN_ImageTF" class="imageselect" type="file">
                     </span>

        <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImageTFDiv">
            <figure class="pull-left imagespan">
                <div class="deletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageTFDiv" data-file="Questions_TM_QN_ImageTF" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImageTFPreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
    </div>

</div>
<div class="col-lg-12 mater_bg_border">
    <h3>Answer Options</h3>
    <div class="row row_with_brd option1" id="TFoption">
        <div class="col-lg-2 marginfive">
            <input type="radio" name="correctanswerTF" class="correctanswerTF" value="True"><label>True</label>
        </div>
        <div class="col-lg-2 marginfive">
            <input type="radio" name="correctanswerTF" class="correctanswerTF" value="False"><label>False</label>
        </div>
        <div class="col-lg-2 topmarg">
            <label  class="mark">marks</label>
            <input type="text" name="marksTF" id="marksTF"  class="form-control marks">
        </div>
        <div class="col-lg-8">
            <label>Please select correct answer for True Or False</label>
        </div>

    </div>
</div>