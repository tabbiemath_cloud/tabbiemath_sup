<div class="col-lg-12 mater_bg_border">

    <div class="form-group">
        <?php echo $form->labelEx($model,'Question Title',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control Questions_TM_QN_QuestionMP" name="Questions[TM_QN_Questionpart]" id="Questions_TM_QN_Question">
            <?php echo ($model->TM_QN_Type_Id=='5'?CHtml::encode($model->TM_QN_Question):''); ?>
        </textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="form-group">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImagePart" data-type="question" data-section="main" data-name="TM_QN_ImagePart"  data-main="questionimagediv<?php echo $model->TM_QN_Id;?>" class="imageselect" id="Questions_TM_QN_Image" type="file">
                     </span>

        <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
   <?php
    $command = Yii::app()->db->createCommand();
    $questions = $command->select('COUNT(*) AS total')
        ->where('TM_QN_Parent_Id=:question', array(':question'=>$model->TM_QN_Id))
        ->from('tm_question')->queryAll();
    $numquestions=$questions[0]['total'];
    ?>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImageDiv">
            <figure class="pull-left previmagespan">
                <div class="previewdeletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageDiv" data-file="Questions_TM_QN_Image" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePreview" src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
            </figure>
        </div>
        <?php if($model->TM_QN_Image!=''):?>
            <div class="media col-md-3" id="questionimagediv<?php echo $model->TM_QN_Id;?>">
                <figure class="pull-left imagespan">
                    <div class="deletespan" data-id="<?php echo $model->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $model->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                    <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
                </figure>
            </div>
        <?php endif;?>
        <div class="col-lg-3 options">
                <label>No. Of Parts</label>
                <select class="form-control" id="multipleqstnum" name="multipleqstnum">
                    <option selected="selected" value="0">Select</option>
                    <option value="2" <?php echo ($numquestions=='2'?'selected="selected"':'');?>>2</option>
                    <option value="3" <?php echo ($numquestions=='3'?'selected="selected"':'');?>>3</option>
                    <option value="4" <?php echo ($numquestions=='4'?'selected="selected"':'');?>>4</option>
                </select>
                <input type="hidden" class="selecteqstdcount" value="<?php echo $numquestions;?>">
        </div>
    </div>
</div>

<div id="mulpartqstarea">
   <?php 
      $showoption=0;
   $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$model->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
        foreach($questions AS $key=>$question):
            $identifier=$key+1;
    ?>            
            <div class="col-lg-12 mater_bg_border questioncount<?php echo $identifier;?>">
                <div class="form-group">
                    <label for="Questions_Enter_Question" class="font2">Enter  Question <?php echo $identifier;?></label>
                    <textarea rows="6" class="form-control multypartQuestions" name="multypartQuestions<?php echo $identifier;?>" id="multypartQuestions<?php echo $identifier;?>" >
                        <?php echo CHtml::encode($question->TM_QN_Question);?>
                    </textarea>
                    <input type="hidden" name="multyquestionid<?php echo $identifier;?>" class="multyquestionid<?php echo $identifier;?>"  value="<?php echo $question->TM_QN_Id;?>"/>
                    <input type="hidden" name="multypartType<?php echo $identifier;?>" class="multypartType<?php echo $identifier;?>"  id="multypartType<?php echo $identifier;?>" value="<?php echo $question->TM_QN_Type_Id;?>"/>
                </div>
                <div class="form-group">
                    <span class="btn btn-default btn-file">
                        <i class="fa fa-picture-o fa-lg yellow"></i>Upload Image<input name="multypartQuestionsImage<?php echo $identifier;?>" data-type="question" data-section="<?php echo $identifier;?>" data-name="multypartQuestionsImage<?php echo $identifier;?>" id="multypartQuestionsImage<?php echo $identifier;?>" data-main="questionimagediv<?php echo $question->TM_QN_Id;?>" class="imageselect"  type="file">
                    </span>
                </div>
                <div class="row">
                    <div class="media col-md-3 imagepreview" id="multypartQuestionsImage<?php echo $identifier;?>Div">
                        <figure class="pull-left previmagespan">
                            <div class="previewdeletespan"  data-type="question" data-section="<?php echo $identifier;?>" data-parent="multypartQuestionsImage<?php echo $identifier;?>Div" data-file="multypartQuestionsImage<?php echo $identifier;?>" ><span class="glyphicon glyphicon-trash"> </span></div>
                            <img class="media-object img-rounded img-responsive" id="multypartQuestionsImage<?php echo $identifier;?>Preview" src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
                        </figure>
                    </div>
                    <?php if($question->TM_QN_Image!=''):?>
                        <div class="media col-md-3" id="questionimagediv<?php echo $question->TM_QN_Id;?>">
                            <figure class="pull-left imagespan">
                                <div class="deletespan" data-id="<?php echo $question->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $question->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                                <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$question->TM_QN_Image;?>" alt="placehold.it/350x250">
                            </figure>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="answerpart<?php echo $identifier;?>">
            <?php if($question->TM_QN_Type_Id=='1'||$question->TM_QN_Type_Id=='2'):
            $showoption=1;
                        if($question->TM_QN_Type_Id=='1'):
                            $qtype='checkbox';
                        elseif($question->TM_QN_Type_Id=='2'):
                            $qtype='radio';
                        endif;
                        echo '<input type="hidden" value="'.count($question->answers).'" id="multypartOptions'.$identifier.'" data-count="'.$identifier.'" name="multypartOptions'.$identifier.'"/>';
                        foreach($question->answers AS $set=>$answer ):
                            $count=$set+1;
                        ?>
                        <div class="col-lg-12 mater_bg_border option <?php echo $identifier.$count;?>">
                            <div class="col-lg-10">
                                <label >Option <?php echo $count;?></label>
                                <textarea rows="4" class="form-control answer" id="MPCanswer<?php echo $identifier.$count;?>" name="answer<?php echo $identifier;?>[]" >
                                       <?php echo CHtml::encode($answer->TM_AR_Answer);?>
                                </textarea>
                            </div>
                            <div class="col-lg-2 topmarg">
                                <label class="mark">marks</label>
                                <input type="text" name="marks<?php echo $identifier.$count;?>"  id="MPCmarks<?php echo $identifier.$count;?>"  value="<?php echo ($answer->TM_AR_Correct?$answer->TM_AR_Marks:'');?>" <?php echo ($answer->TM_AR_Correct?:'disabled="true"');?> class="form-control marks MPCmarks<?php echo $identifier;?>">
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-8">
                                    <span class="btn btn-default btn-file">
                                    <i class="fa fa-picture-o fa-lg yellow"></i>
                                    Upload Image <input type="file" data-type="answer" data-section="<?php echo $identifier.$count;?>" data-name="anserimage<?php echo $identifier.$count;?>" name="anserimage<?php echo $identifier.$count;?>" data-main="answerimagediv<?php echo $answer->TM_AR_Id;?>" id="anserimage<?php echo $identifier.$count;?>" class="imageselect answerimage">
                                        <input type="hidden" id="checkanswer<?php echo $identifier.$count;?>" value="<?php echo ($answer->TM_AR_Image!=''?'true':'false');?>">
                                    </span>
                                </div>
                                <div class="col-lg-3">
                                    <input type="<?php echo $qtype;?>" name="correctanswer<?php echo ($qtype=='radio'?$identifier:$identifier.$count);?>" value="<?php echo ($qtype=='radio'?$answer->TM_AR_Id:'1');?>" <?php echo ($answer->TM_AR_Correct?'checked':'');?> id="MPCcorrectanswer<?php echo $identifier.$count;?>"   class="correctacswer MPCcorrectanswer<?php echo $identifier;?> MPCcorrectanswer">
                                    <label>Tick for correct answer</label>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <?php if($answer->TM_AR_Image!=''):?>
                                    <div class="media col-md-3" id="answerimagediv<?php echo $answer->TM_AR_Id;?>">
                                        <figure class="pull-left imagespan">
                                            <div class="deletespan" data-type="answer" data-section="<?php echo $identifier.$count;?>" data-id="<?php echo $answer->TM_AR_Id;?>" data-parent="answerimagediv<?php echo $answer->TM_AR_Id;?>" data-type="Answer" ><span class="glyphicon glyphicon-trash"> </span></div>
                                            <img class="media-object img-rounded img-responsive" src="<?php echo Yii::app()->baseUrl.'/images/answerimages/thumbs/'.$answer->TM_AR_Image;?>" alt="placehold.it/350x250">

                                        </figure>
                                    </div>
                                <?php endif;?>
                                <div class="media col-md-3 imagepreview" id="anserimage<?php echo $identifier.$count;?>Div">
                                    <figure class="pull-left previmagespan">
                                        <div class="previewdeletespan" data-type="answer" data-section="<?php echo $identifier.$count;?>" data-parent="anserimage<?php echo $identifier.$count;?>Div" data-file="anserimage<?php echo $identifier.$count;?>" ><span class="glyphicon glyphicon-trash"> </span></div>
                                        <img class="media-object img-rounded img-responsive" id="anserimage<?php echo $identifier.$count;?>Preview" src="" alt="placehold.it/350x250">
                                    </figure>
                                </div>
                            </div>
                            <input type="hidden" name="answeridOPT<?php echo $identifier.$count;?>" value="<?php echo $answer->TM_AR_Id;?>">
                            <input type="hidden" id="answerimagehiddenOPT<?php echo $identifier.$count;?>" value="<?php echo $answer->TM_AR_Image;?>">
                        </div>
                            <?php
                    endforeach;
                elseif($question->TM_QN_Type_Id=='3'):
                $showoption=1;
                ?>

                    <div class="col-lg-12 mater_bg_border option<?php echo $identifier;?>">
                        <h3>Answer Options</h3>
                        <div class="row row_with_brd " id="MPTFoption<?php echo $identifier;?>">
                            <?php foreach($question->answers AS $key=>$answer):
								$count=$key+1;
                            if($answer->TM_AR_Correct):
                                $marks=$answer->TM_AR_Marks;
                            endif;
                            ?>
                            <div class="col-lg-2 marginfive">
                                <input type="radio" name="correctanswer<?php echo $identifier;?>" class="correctanswerTF<?php echo $identifier;?>" value="<?php echo ($key=='0'?'True':'False');?>" <?php echo ($answer->TM_AR_Correct?'checked':'');?>><label><?php echo $answer->TM_AR_Answer;?></label>
                                <input type="hidden" name="answeridTF<?php echo $identifier.$count;?>" value="<?php echo $answer->TM_AR_Id;?>">
                            </div>
                            <?php endforeach;?>
                            <div class="col-lg-2 topmarg">
                                <label  class="mark">marks</label>
                                <input type="text" name="marks<?php echo $identifier;?>" id="marksTF<?php echo $identifier;?>" value="<?php echo $marks;?>"  class="form-control marks">
                            </div>
                            <div class="col-lg-8"><label>Please select correct answer for True Or False</label>
                            </div>
                        </div>
                    </div>
                <?php
            elseif($question->TM_QN_Type_Id=='4'):
                $answer=$question->answers[0];
                ?>
                <div class="col-lg-12 mater_bg_border">
                    <h3>Answer Options</h3>
                    <div class="row row_with_brd">
                        <div class="col-lg-10">
                            <label >Please provide probable solition seperated by <b>;</b></label>
                            <textarea rows="4" class="form-control answer" name="answeroptions<?php echo $identifier;?>" id="MPansweroptions<?php echo $identifier;?>"><?php echo CHtml::encode($answer->TM_AR_Answer);?></textarea>
							<input type="hidden" name="answeridTEXT<?php echo $identifier;?>" value="<?php echo $answer->TM_AR_Id;?>">
                        </div>
                        <div class="col-lg-2 topmarg">
                            <label class="mark">marks</label>
                            <input type="text" value="<?php echo $answer->TM_AR_Marks;?>" name="marks<?php echo $identifier;?>" id="MPmarksTEXT<?php echo $identifier;?>" class="form-control marks">
                            <input type="hidden" name="answeridTEXT<?php echo $identifier;?>" value="<?php echo $answer->TM_AR_Id;?>">
                        </div>
                        <div class="col-lg-4 pull-right">
                            <input size="60" maxlength="250" name="Editor<?php echo $identifier;?>" <?php echo ($question->TM_QN_Editor?'checked':'');?>  value="1" type="checkbox">
                            <label for="Questions_Click_To_enable_editor_for_student_answering">Click  To Enable Editor For Student Answering</label>
                        </div>
                    </div>
                </div>
            <?php endif;?>

            </div>
                <script type="text/javascript">
                    $( '#multypartQuestions<?php echo $identifier;?>' ).ckeditor();
                </script>
    <?php endforeach;?>
</div>
<?php
    Yii::app()->clientScript->registerScript('multypart', "
        var optionshow=".$showoption.";        
        if(optionshow==1)
        {            
           $('#showoptiondiv').show(); 
        }
        else
        {               
            $('#showoptiondiv').hide();
        }    
         $('#multipleqstnum').change(function(){
                var count=($(this).val()*1);
                var prevcount=($('.selecteqstdcount').val()*1);
                var newcount=0;
                if(count!='0')
                {

                    if(count>prevcount)
                    {

                        newcount=count-prevcount;
                        
                        for(i=1;i<=newcount;i++)
                        {
                            optioncount=prevcount+i
                            var questionhtml=getQuestion(optioncount)
                             $('#multypartQuestions'+i).ckeditor();
                            $('#mulpartqstarea').append(questionhtml);

                        }
                        $('.selecteqstdcount').val(count);
                    }
                    else if(prevcount>count)
                    {
                        
                        if(confirm('You are about to delete child questions. Once it is deleted it can not be retreived back. Are you sure you want to continue?'))
                        {                           
                            newcount=prevcount-count;
                            var loopcount=0                    
                            for(i=prevcount;i>count;i--)
                            {
                                var questionid=$('.multyquestionid'+i).val();                          
                                /*if(removeChild($('.multyquestionid'+i).val(),i))
                                {
                                    loopcount++;                                
                                }  */
                                $.ajax({
                                  type: 'POST',
                                  url: '".Yii::app()->createUrl('questions/deletechildquestion')."',
                                  data: { question:questionid}
                                })
                                  .done(function( data ) {
                                        loopcount=loopcount+1; 
                                        $('.questioncount'+count).remove();
                                        $('.answerpart'+count).remove();  
                                        if(loopcount==newcount)
                                        {
                                            $('.selecteqstdcount').val(count)
                							location.reload(true);
                                        }                                                                                
                                });                                                             
                            }                                                                            
                            

                             
                        }
                        else
                        {
                            $(this).val(prevcount)
                            $('.selecteqstdcount').val(prevcount)
                        }
                        
                    }
                }
                else
                {
                   $('#mulpartqstarea').html('')
                   $('.selecteqstdcount').val(count)
                }
                 

         });
        function removeChild(id,count)
        {
            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('questions/deletechildquestion')."',
              data: { question:id}
            })
              .done(function( data ) {
                    $('.questioncount'+count).remove();
                    $('.answerpart'+count).remove();  

            });
            return true;
        }
         $(document).on('change','.multypartType',function(){
                var selectedval=$(this).val();
                var identifier=$(this).attr('data-count');
                if(selectedval=='1' | selectedval=='2')
                {
                     $('#multypartOptions'+identifier).attr('disabled',false);
                     $('.answerpart'+identifier).html('');
                     $('#multypartselectedcount'+identifier).val('0');
                }
                else if(selectedval=='3')
                {
                    $('#multypartOptions'+identifier).attr('disabled',true)
                    $('#multypartOptions'+identifier+' option[value=0]').attr('selected','selected');
                    var trufalseanswer=GetTrufalseanswer(identifier)
                    $('.answerpart'+identifier).html(trufalseanswer);
                }
                else if(selectedval=='4')
                {
                    $('#multypartOptions'+identifier).attr('disabled',true)
                    $('#multypartOptions'+identifier+' option[value=0]').attr('selected','selected');
                    var trufalseanswer=Gettextanswer(identifier)
                    $('.answerpart'+identifier).html(trufalseanswer);
                    //$('#MPansweroptions'+identifier).ckeditor();
                      $('.answer').ckeditor();

                }
                else
                {
                     $('#multypartOptions'+identifier).attr('disabled',true)
                     $('#multypartOptions'+identifier+' option[value=0]').attr('selected','selected');
                     $('.answerpart'+identifier).html('');
                }
         })
         $(document).on('change','.multypartOptions',function(){
                var identifier=$(this).attr('data-count');
                var questiontype=$('#multypartType'+identifier).val();
                var count=($(this).val()*1);
                var prevcount=($('#multypartselectedcount'+identifier).val()*1);
                var newcount=0;

                if(count>prevcount)
                {
                    newcount=count-prevcount;
                    for(i=1;i<=newcount;i++)
                    {
                        optioncount=prevcount+i
                        var answerhtml=getanswerhtml(identifier,optioncount,questiontype)
                        $('.answerpart'+identifier).append(answerhtml);
                        //$('#MPCanswer'+identifier+optioncount).ckeditor();
                          $('.answer').ckeditor();
                    }
                }
                else if(prevcount>count)
                {

                    newcount=prevcount-count;

                    for(i=prevcount;i>count;i--)
                    {
                          $('.option'+identifier+i).remove();
                    }
                }
                $('#multypartselectedcount'+identifier).val(count)

         })
    ");
?>
<script type="text/javascript">
    function getQuestion(count)
    {
        var questionhtml='<div class="col-lg-12 mater_bg_border questioncount'+count+'">' +
            '<div class="form-group">' +
            '<label for="Questions_Enter_Question" class="font2">Enter  Question '+count+'</label>' +
            '<textarea rows="6" class="form-control multypartQuestions" name="multypartQuestions'+count+'" id="multypartQuestions'+count+'" ></textarea>' +
            '</div>'+
            '<div class="form-group"><span class="btn btn-default btn-file">' +
            '<i class="fa fa-picture-o fa-lg yellow"></i>Upload Image' +
            '<input name="multypartQuestionsImage'+count+'" id="Questions_TM_QN_ImagePart'+count+'" class="imageselect"  type="file">' +
            '</span>' +
            '</div>' +
            '<div class="row">' +
                '<div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePart'+count+'Div">' +
                '<figure class="pull-left previmagespan">' +
                '<div class="previewdeletespan" data-parent="Questions_TM_QN_ImagePart'+count+'Div" data-file="Questions_TM_QN_ImagePart'+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>' +
                '<img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePart'+count+'Preview" src="" alt="placehold.it/350x250">' +
                '</figure></div>'+
                '<div class="col-lg-3 options">' +
                '<label >Question Type </label>' +
                '<select class="form-control multypartType" id="multypartType'+count+'" data-count="'+count+'" name="multypartType'+count+'" >' +
                '<option value="0" selected="selected">Selected</option><option value="1" >Multiple Option</option><option value="2">Single Option</option><option value="3">True/false</option><option value="4">Text Entry</option></select>' +
                '</div>' +
                '<div class="col-lg-3 options">' +
                '<label >No. Of Options </label>' +
                '<select class="form-control multypartOptions" disabled id="multypartOptions'+count+'" data-count="'+count+'" name="multypartOptions'+count+'" >' +
                '<option value="0" selected>Select</option><option value="2" >2</option><option value="3">3</option><option value="4">4</option></select>' +
                '<input type="hidden" class="multypartselectedcount" id="multypartselectedcount'+count+'" value="0">' +
                '</div>' +
            '</div>' +
            '</div>' +
            '<div class="answerpart'+count+'"></div>';
        return questionhtml
    }
    function getanswerhtml(identifier,count,type)
    {
        if(type=='1')
        {
            var qtype='checkbox';
        }
        else if(type='2')
        {
            var qtype='radio';
        }
		if(qtype=='radio')
		{
			var correctone='<input type="'+qtype+'" name="correctanswer'+identifier+'" value="'+count+'" id="MPCcorrectanswer'+identifier+count+'"   class="correctacswer MPCcorrectanswer'+identifier+' MPCcorrectanswer">';
		}
		else
		{
			var correctone='<input type="'+qtype+'" name="correctanswer'+identifier+count+'" value="1" id="MPCcorrectanswer'+identifier+count+'"   class="correctacswer MPCcorrectanswer'+identifier+' MPCcorrectanswer">';
		}
        var answertemp='<div class="col-lg-12 mater_bg_border option'+identifier+count+'">' +
            '<div class="col-lg-10">' +
            '<label >Option '+count+'</label>' +
            '<textarea rows="4" class="form-control answer" id="MPCanswer'+identifier+count+'" name="answer'+identifier+'[]" ></textarea>' +
            '</div>' +
            '<div class="col-lg-2 topmarg">' +
            '<label class="mark">marks</label>' +
            '<input type="text" name="marks'+identifier+count+'"  id="MPCmarks'+identifier+count+'"  disabled="true" class="form-control marks MPCmarks'+identifier+'">' +
            '</div>' +
            '<div class="col-lg-12"><div class="col-lg-8">' +
            '<span class="btn btn-default btn-file">' +
            '<i class="fa fa-picture-o fa-lg yellow"></i>' +
            'Upload Image <input type="file" name="anserimage'+identifier+count+'" id="anserimage'+identifier+count+'" class="imageselect answerimage">' +
            '</span>' +
            '</div>' +
            '<div class="col-lg-3">' +correctone
             +
            '<label>Tick for correct answer</label>' +
            '</div></div>' +
            '<div class="col-lg-12">' +
            '<div class="media col-md-3 imagepreview" id="anserimage'+identifier+count+'Div">' +
            '<figure class="pull-left previmagespan">' +
            '<div class="previewdeletespan" data-parent="anserimage'+identifier+count+'Div" data-file="anserimage'+identifier+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>'+
            '<img class="media-object img-rounded img-responsive" id="anserimage'+identifier+count+'Preview" src="" alt="placehold.it/350x250"></figure></div>'+
            '</div>'+
            '</div>';
        return answertemp;
    }
    function GetTrufalseanswer(identifier)
    {
            var answertemp='<div class="col-lg-12 mater_bg_border option'+identifier+'">' +
                '<h3>Answer Options</h3>' +
                '<div class="row row_with_brd " id="MPTFoption'+identifier+'">' +
                '<div class="col-lg-2 marginfive">' +
                '<input type="radio" name="correctanswer'+identifier+'" class="correctanswerTF'+identifier+'" value="True"><label>True</label>' +
                '</div>' +
                '<div class="col-lg-2 marginfive">' +
                '<input type="radio" name="correctanswer'+identifier+'" class="correctanswerTF'+identifier+'" value="False"><label>False</label>' +
                '</div>' +
                '<div class="col-lg-2 topmarg">' +
                '<label class="mark">marks</label>' +
                '<input type="text" name="marks'+identifier+'" id="MPmarksTF'+identifier+'" class="form-control marks">' +
                '</div>' +
                '<div class="col-lg-8"><label>Please select correct answer for True Or False</label>' +
                '</div>'+
                '</div>' +
                '</div>';
            return answertemp
    }
    function Gettextanswer(identifier)
    {
            var answertemp='<div class="col-lg-12 mater_bg_border">' +
                '<h3>Answer Options</h3>' +
                    '<div class="row row_with_brd">' +
                        '<div class="col-lg-10">' +
                            '<label >Please provide probable solition seperated by \'<b>;</b>\'</label>'+
                            '<textarea rows="4" class="form-control answer" name="answeroptions'+identifier+'" id="MPansweroptions'+identifier+'"></textarea>' +
                        '</div>'+
                        '<div class="col-lg-2 topmarg">' +
                        '<label class="mark">marks</label>' +
                        '<input type="text" name="marks'+identifier+'" id="MPmarksTEXT'+identifier+'" class="form-control marks">' +
                        '</div>' +
                        '<div class="col-lg-4 pull-right">' +
                        '<input size="60" maxlength="250" name="Editor'+identifier+'"  value="1" type="checkbox"> ' +
                        '<label for="Questions_Click_To_enable_editor_for_student_answering">Click  To Enable Editor For Student Answering</label>' +
                        '</div>' +
                    '</div>' +
                '</div>';
            return answertemp
    }
    function GetMulpartCont()
    {

    }
</script>