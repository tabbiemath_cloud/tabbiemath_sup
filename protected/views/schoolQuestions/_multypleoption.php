<div class="col-lg-12 mater_bg_border">

    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_Questionchoice]" id="Questions_TM_QN_Questionchoice"></textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="form-group">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="Imagechoice" id="Questions_TM_QN_Image" data-type="question" data-section="main" data-name="Imagechoice" class="imageselect" type="file">
                     </span>

            <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImageDiv">

            <figure class="pull-left imagespan">
                <div class="deletespan" data-type="question" data-section="main" data-parent="Questions_TM_QN_ImageDiv" data-file="Questions_TM_QN_Image" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePreview" src="" alt="placehold.it/350x250">
            </figure>
        </div>
        <div class="col-lg-3 options">
            <label for="Questions_TM_QN_Option_Countchoice">No. Of Options<span class="required">*</span></label>
            <select class="form-control" name="Questions[TM_QN_Option_Countchoice]" id="Questions_TM_QN_Option_Countchoice">
                <option value="0">Select</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4" >4</option>
                <option value="5">5</option>
            </select>
            <?php echo $form->error($model,'TM_QN_Option_Count'); ?>
            <input type="hidden" class="selectedcount" value="0">
        </div>

    </div>

</div>
<div class="col-lg-12 mater_bg_border">
    <h3>Answer Options</h3>
    <div id="answeroptions">

    </div>
</div>
<?php
Yii::app()->clientScript->registerScript('answeroptions', "
        $('#Questions_TM_QN_Option_Countchoice').change(function(){
            var count=($(this).val()*1);
            var prevcount=($('.selectedcount').val()*1);
            var newcount=0;
            if(count>prevcount)
            {
                newcount=count-prevcount;
                for(i=1;i<=newcount;i++)
                {
                    optioncount=prevcount+i
                    var randomclass='rand'+Math.floor((Math.random() * 1000) + 1);
                    var answerhtml=getanswerhtmlmultychoice(optioncount,randomclass)
                    $('#answeroptions').append(answerhtml);
                    CKEDITOR.replace( 'choiceanswer'+randomclass);
                    //$( '.choiceanswer'+randomclass ).ckeditor();
                }
            }
            else if(prevcount>count)
            {

                newcount=prevcount-count;

                for(i=prevcount;i>count;i--)
                {
                      $('.option'+i).remove();
                }
            }
            $('.selectedcount').val(count)
        })
    ");
?>
<script type="text/javascript">
    function getanswerhtmlmultychoice(count,randomclass)
    {
        var type=$('.typeselect:checked').val();
        var fieldtype='';
        var fieldname='';
        var fieldvalue='';
        if(type=='1'){
            fieldtype='checkbox';
            fieldname='correctanswer'+count;
            fieldvalue='1'

        }
        else if(type=='2')
        {
            fieldtype='radio'
            fieldname='correctanswer';
            fieldvalue=count;
        }
        var answertemp='<div class="row row_with_brd option'+count+'">' +
            '<div class="col-lg-10">' +
            '<label >Option '+count+'</label>' +
            '<textarea rows="4" class="form-control answer marbott multychoice choiceanswer'+count+'" id="choiceanswer'+randomclass+'" name="answer[]" ></textarea>' +
            '</div>' +
            '<div class="col-lg-2 topmarg">' +
            '<label class="mark">marks</label>' +
            '<input type="text" disabled="disabled" name="marks'+count+'" id="choicemark'+count+'" class="form-control marks markdisply choicemark">' +
            '</div>' +
            '<div class="col-lg-12">' +
                '<div class="col-lg-8">' +
                '<span class="btn btn-default btn-file">' +
                '<i class="fa fa-picture-o fa-lg yellow"></i>' +
                'Upload Image <input type="file" name="anserimage'+count+'"  data-type="answer" data-section="'+count+'" data-name="anserimage'+count+'"   id="anserimage'+count+'" class="answerimage imageselect"></span>' +
                '<input type="hidden" id="checkanswer'+count+'" value="false">'+
                '</div>' +
                '<div class="col-lg-3">' +
                '<input type="'+fieldtype+'"  name="'+fieldname+'" value="'+fieldvalue+'" id="correctacswerchoice'+count+'" class="correctacswer correctacswerchoice">' +
                '<label>Tick for correct answer</label>' +
                '</div>' +
            '</div>' +
            '<div class="col-lg-12">' +
                '<div class="media col-md-3 imagepreview" id="anserimage'+count+'Div">' +
                    '<figure class="pull-left imagespan">' +
                    '<div class="deletespan" data-type="answer" data-section="'+count+'" data-parent="anserimage'+count+'Div" data-file="anserimage'+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>' +
                    '<img class="media-object img-rounded img-responsive" id="anserimage'+count+'Preview" src="http://placehold.it/100x100" alt="placehold.it/350x250">' +
                    '</figure>' +
                '</div>'+
            '</div>' +
            '</div>';
        return answertemp;
    }

</script>



