<div class="col-lg-12 mater_bg_border">
    <div class="form-group">
        <?php echo $form->labelEx($model,'Enter Question',array('class'=>'font2')); ?>
        <textarea rows="6" class="form-control" name="Questions[TM_QN_QuestionMPPaperOnly]" id="Questions_TM_QN_QuestionMPPaperOnly">
		<?php echo ($model->TM_QN_Type_Id=='7'?CHtml::encode($model->TM_QN_Question):''); ?>
		</textarea>
        <?php echo $form->error($model,'TM_QN_Question'); ?>
    </div>
    <div class="form-group">
            <span class="btn btn-default btn-file">
                       <i class="fa fa-picture-o fa-lg yellow"></i>
                            Upload Image <input name="TM_QN_ImagePaperPart" data-type="question" data-section="main" data-name="TM_QN_ImagePaperPart" id="Questions_TM_QN_Image" data-main="questionimagediv<?php echo $model->TM_QN_Id;?>" class="imageselect"  type="file">
                     </span>

        <?php echo $form->error($model,'TM_QN_Image'); ?>
    </div>
	<?php
    $command = Yii::app()->db->createCommand();
    $questions = $command->select('COUNT(*) AS total')
        ->where('TM_QN_Parent_Id=:question', array(':question'=>$model->TM_QN_Id))
        ->from('tm_question')->queryAll();
    $numquestions=$questions[0]['total'];
    ?>
    <div class="row">
        <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImageDiv">
            <figure class="pull-left previmagespan">
                <div class="previewdeletespan" data-type="question" data-section="main"   data-parent="Questions_TM_QN_ImageDiv" data-file="Questions_TM_QN_Image" ><span class="glyphicon glyphicon-trash"> </span></div>
                <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePreview" src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
            </figure>
        </div>
        <?php if($model->TM_QN_Image!=''):?>
            <div class="media col-md-3"  id="questionimagediv<?php echo $model->TM_QN_Id;?>">
                <figure class="pull-left imagespan">
                    <div class="deletespan" data-type="question" data-section="main" data-id="<?php echo $model->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $model->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                    <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$model->TM_QN_Image;?>" alt="placehold.it/350x250">
                </figure>
            </div>
        <?php endif;?>
        <div class="col-lg-3 options">
            <label>No. Of Parts</label>
            <select class="form-control" id="PoMPqstnum" name="PoMPqstnum">
                <option selected="selected" value="0">Select</option>
				<option value="2" <?php echo ($numquestions=='2'?'selected="selected"':'');?>>2</option>
				<option value="3" <?php echo ($numquestions=='3'?'selected="selected"':'');?>>3</option>
				<option value="4" <?php echo ($numquestions=='4'?'selected="selected"':'');?>>4</option>				
            </select>
            <input type="hidden" class="selectePoMPqstnum" value="<?php echo $numquestions;?>">
        </div>
    </div>
</div>
<div id="papermulpartqstarea">
   <?php $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$model->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
        foreach($questions AS $key=>$question):
            $identifier=$key+1;
    ?>
	<div class="col-lg-12 mater_bg_border paperquestioncount<?php echo $identifier;?>">
				<div class="form-group">
				<label for="Questions_Enter_Question" class="font2">Enter  Question <?php echo $identifier;?></label>
				<textarea rows="6" class="form-control PapermultypartQuestions" name="PapermultypartQuestions[]" id="PapermultypartQuestions<?php echo $identifier;?>" ><?php echo CHtml::encode($question->TM_QN_Question);?></textarea>
                <input type="hidden" name="multyquestionpaperid<?php echo $identifier;?>" class="multyquestionpaperid<?php echo $identifier;?>"  value="<?php echo $question->TM_QN_Id;?>"/>
				</div>
				<div class="row">
					<div class="col-lg-2"><span class="btn btn-default btn-file">
					<i class="fa fa-picture-o fa-lg yellow"></i>Upload Image
					<input name="PapermultypartQuestionsImage<?php echo $identifier;?>"  data-type="question" data-section="<?php echo $identifier;?>" data-name="PapermultypartQuestionsImage" class="imageselect"  data-main="questionimagediv<?php echo $question->TM_QN_Id;?>" id="Questions_TM_QN_ImagePaper<?php echo $identifier;?>" type="file">
					</span>
					</div>
					<?php if($question->TM_QN_Image!=''):?>
                        <div class="media col-md-3"  id="questionimagediv<?php echo $question->TM_QN_Id;?>">
                            <figure class="pull-left imagespan">
                                <div class="deletespan" data-type="question" data-section="<?php echo $identifier;?>"  data-id="<?php echo $question->TM_QN_Id;?>" data-parent="questionimagediv<?php echo $question->TM_QN_Id;?>" data-type="Question" ><span class="glyphicon glyphicon-trash"> </span></div>
                                <img class="media-object img-rounded img-responsive"  src="<?php echo Yii::app()->baseUrl.'/images/questionimages/thumbs/'.$question->TM_QN_Image;?>" alt="placehold.it/350x250">
                            </figure>
                        </div>
                    <?php endif;?>
                    <div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePaper<?php echo $identifier;?>Div">
                        <figure class="pull-left previmagespan">
                            <div class="previewdeletespan" data-type="question" data-section="<?php echo $identifier;?>" data-parent="Questions_TM_QN_ImagePaper<?php echo $identifier;?>Div" data-file="Questions_TM_QN_ImagePaper<?php echo $identifier;?>" ><span class="glyphicon glyphicon-trash"> </span></div>
                            <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePaper<?php echo $identifier;?>Preview" src="" alt="placehold.it/350x250">
                        </figure>
                    </div>
                    <div class="col-lg-2 pull-right">
					<label class="mark">marks</label>
					<input type="text" name="PaperMPmarks[]" value="<?php echo $question->TM_QN_Totalmarks;?>" id="PaperMPmarks<?php echo $identifier;?>"   class="form-control marks PaperMPmarks<?php echo $identifier;?>">
					</div>
					<input type="hidden" name="papermultyquestionid<?php echo $identifier;?>" value="<?php echo $question->TM_QN_Id;?>"/>
				</div>
				</div>
				<script type="text/javascript">
                    $( '#PapermultypartQuestions<?php echo $identifier;?>' ).ckeditor();
                </script>				
	<?php endforeach;?>
</div>
<?php
    Yii::app()->clientScript->registerScript('papermultypart', "
        $('#PoMPqstnum').change(function(){
                var count=($(this).val()*1);
                var prevcount=($('.selectePoMPqstnum').val()*1);
                var newcount=0;
                if(count!='0')
                {

                    if(count>prevcount)
                    {

                        newcount=count-prevcount;
                        for(i=1;i<=newcount;i++)
                        {
                            optioncount=prevcount+i
                            var questionhtml=getQuestionpaper(optioncount)
                            $('#papermulpartqstarea').append(questionhtml);
                            $('#PapermultypartQuestions'+optioncount).ckeditor();
                        }
                        $('.selectePoMPqstnum').val(count)
                    }
                    else if(prevcount>count)
                    {
                        if(confirm('You are about to delete child questions. Once it is deleted it can not be retreived back. Are you sure you want to continue?'))
                        {  
                            newcount=prevcount-count;
                            var loopcount=0;
                            for(i=prevcount;i>count;i--)
                            {
                                var questionid=$('.multyquestionpaperid'+i).val();   
                                $.ajax({
                                  type: 'POST',
                                  url: '".Yii::app()->createUrl('questions/deletechildquestion')."',
                                  data: { question:questionid}
                                })
                                  .done(function( data ) {
                                        loopcount=loopcount+1; 
                                        $('.paperquestioncount'+i).remove();
                                        if(loopcount==newcount)
                                        {
                                            $('.selectePoMPqstnum').val(count)
                                			location.reload(true);
                                        }                                                                                
                                });                                   
                                
                            }                            
                        }
                        else
                        {
                            $(this).val(prevcount)
                            $('.selectePoMPqstnum').val(prevcount)
                        }                        
                    }
                }
                else
                {
                   $('#papermulpartqstarea').html('')
                   $('.selectePoMPqstnum').val(count)
                }
                 

         });
    ");
?>
<script type="text/javascript">
    function getQuestionpaper(count)
    {
        var questionhtml='<div class="col-lg-12 mater_bg_border paperquestioncount'+count+'">' +
            '<div class="form-group">' +
            '<label for="Questions_Enter_Question" class="font2">Enter  Question '+count+'</label>' +
            '<textarea rows="6" class="form-control PapermultypartQuestions" name="PapermultypartQuestions[]" id="PapermultypartQuestions'+count+'" ></textarea>' +
            '</div>'+
            '<div class="row">' +
                '<div class="col-lg-2"><span class="btn btn-default btn-file">' +
                '<i class="fa fa-picture-o fa-lg yellow"></i>Upload Image' +
                '<input name="PapermultypartQuestionsImage'+count+'" id="Questions_TM_QN_ImagePaperPart'+count+'" class="imageselect" type="file">' +
                '</span>' +
                '</div>' +
                '<div class="col-lg-2">' +
                '<label class="mark">marks</label>' +
                '<input type="text" name="PaperMPmarks[]"  id="PaperMPmarks'+count+'"   class="form-control marks PaperMPmarks'+count+'">' +
                '</div>' +
                '<div class="media col-md-3 imagepreview" id="Questions_TM_QN_ImagePaperPart'+count+'Div">'+
                '<figure class="pull-left previmagespan">' +
                '<div class="previewdeletespan" data-parent="Questions_TM_QN_ImagePaperPart'+count+'Div" data-file="Questions_TM_QN_ImagePaperPart'+count+'" ><span class="glyphicon glyphicon-trash"> </span></div>' +
                '<img class="media-object img-rounded img-responsive" id="Questions_TM_QN_ImagePaperPart'+count+'Preview" src="" alt="placehold.it/350x250"></figure>' +
                '</div>'+
            '</div>' +
            '</div>' +
            '</div>';
        return questionhtml
    }
</script>