<?php if($teacher==9 || $usermode=='Teacher'):?>
<style>
<?php if(Yii::app()->session['userlevel']==1):?>
    #page-wrapper-pro
    {
        width:250%;
    }
<?php else:?>
    #page-wrapper
    {
        width:250%;
    }
<?php endif;?>
#wrapper
{
        background-color: #f8f8f8;
        width:268.7%;
}
.navbar
{
    width:37%;
}
#questions-grid
{
    font-style: normal !important;
    font-family: STIXGeneral normal !important;
    font-size: 16px;
    color: #333333e0;
}
</style>
<?php endif;?>
<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs=array(
	'Schools'=>array('index'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'View School', 'url'=>array('School/view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'Create Question', 'url'=>array('create', 'id'=>$id)),
    array('label'=>'Manage Questions', 'url'=>array('admin','id'=>$id,)), 
    array('label'=>'Manage System Questions', 'url'=>array('systemquestions', 'id'=>$id)),
    array('label'=>'Hidden System Questions', 'url'=>array('importquestions', 'id'=>$id)),
);


        if(Yii::app()->user->isSchoolAdmin()):?>
<h3>Manage Questions</h3>
        <?php 
        elseif(Yii::app()->user->isTeacher()):?>
        <div class="row">
            <div  <?php if($teacher==9 & $usermode=='Teacher'): echo 'class="col-lg-2" style="width: 30%;"'; else: echo 'class="col-lg-12"'; endif; ?>>
                <h3 class="h125 pull-left">Manage Questions</h3>
                <a href="<?php echo Yii::app()->createUrl('SchoolQuestions/create',array('id'=>$id))?>"
                <button class="btn pull-right btn-warning" type="button" style="margin-top: 18px; margin-bottom: 10px;">Create</button></a>
            </div>


        </div>

<?php 
        endif;
?>


<div class="row brd1" <?php if($teacher!=9 & $usermode!='Teacher'): echo "style=width:250%;overflow:auto;"; endif;?>>
    <div class="col-lg-12" >
    <!--code by amal-->
    <?php if($teacher!=9 & $usermode!='Teacher'):?>
        <div class="col-lg-4" style="margin-bottom: 10px;">
            <div class="col-sm-3">
                <?php echo CHtml::dropDownList('changestatus',$select,Questions::itemAlias('QuestionStatus'),array('prompt'=>'select status','class'=>'form-control' ));?>
            </div>
            <div class="col-sm-3">
                <?php
                echo CHtml::ajaxLink("Change Status", $this->createUrl('questions/ChangeStatus'), array(
                    "type" => "post",
                    "data" => 'js:{theIds : $.fn.yiiGridView.getChecked("questions-grid","question").toString(),"status":$("#changestatus").val()}',
                    "success" => 'js:function(data){ $.fn.yiiGridView.update("questions-grid")  }' ),array(
                        'class' => 'btn btn-warning btn-block'
                    )
                );
                ?>
            </div>
            <form method="get">
                <div class="col-sm-3">
                    <?php echo CHtml::dropDownList('createdby',$select,SchoolQuestions::model()->GetCreatorList(),array('class'=>'form-control','options' => array($_GET['createdby']=>array('selected'=>true))));?>
                </div>
                <div class="col-sm-3">
                    <button class="btn btn-warning btn-block" name="search" value="search" type="submit">Search</button>
                </div>
            </form>
        </div>
    <?php else:?>
    <div class="col-lg-4" style="margin-bottom: 10px;">
        <form method="get">
            <div class="col-sm-3">
                <?php echo CHtml::dropDownList('createdby',$select,SchoolQuestions::model()->GetCreatorList(),array('class'=>'form-control','options' => array($_GET['createdby']=>array('selected'=>true))));?>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-warning" name="search" value="search" type="submit">Search</button>
            </div>
        </form>
    </div>
    <?php endif;?>
    <!--ends-->
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'questions-grid',
	'dataProvider'=>$type,
        'pager' => array('maxButtonCount' => 5),
	'filter'=>$model,
    'selectableRows'=>2,
	'columns'=>array(
        array(
            'class'=>'CCheckBoxColumn',
            'name'=>'question',
            'value'=>'$data->TM_QN_Id',
            'id'=>'question'
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{copy}{view}{update}{delete}',
            //'template'=>'{view}{delete}',
            'deleteButtonImageUrl'=>false,
            'updateButtonImageUrl'=>false,
            'viewButtonImageUrl'=>false,
            'buttons'=>array
            (
                'copy'=>array(
                    'label'=>'<span class="glyphicon glyphicon-copy"></span>',
                    'url'=>'Yii::app()->createUrl("schoolQuestions/copy", array("id"=>'.$id.',"question"=>$data->TM_QN_Id))',
                    'options'=>array(
                        'title'=>'Copy',
                    )),
                'view'=>array(
                    'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                    'url'=>'Yii::app()->createUrl("schoolQuestions/view", array("id"=>'.$id.',"question"=>$data->TM_QN_Id))',
                    'options'=>array(
                        'title'=>'View',
                    )),
                'update'=>array(
                    'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                    'url'=>'Yii::app()->createUrl("schoolQuestions/update", array("id"=>$data->TM_QN_School_Id,"question"=>$data->TM_QN_Id))',
                    'visible'=>'Questions::model()->findByPk($data->TM_QN_Id)->TM_QN_School_Id > 0',
                    'options'=>array(
                        'title'=>'Update'
                    )),
                'delete'=>array(
                    'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                    'url'=>'Yii::app()->createUrl("schoolQuestions/delete", array("id"=>$data->TM_QN_School_Id,"question"=>$data->TM_QN_Id))',
                    'visible'=>'Questions::model()->findByPk($data->TM_QN_Id)->TM_QN_School_Id > 0',
                    'options'=>array(
                        'title'=>'Delete',
                    ),
                )
            ),
        ),
        'TM_QN_Id',
        'TM_QN_QuestionReff',
        'TM_QN_Pattern',
        array(
            'name'=>'TM_QN_Question',
            'type'=>'raw',
            'value'=>'Questions::model()->GetQuestion($data->TM_QN_Id)',
        ),
        array(
            'name'=>'TM_QN_Status',
            'value'=>'Questions::itemAlias("QuestionStatus",$data->TM_QN_Status)',
            'filter'=>Questions::itemAlias('QuestionStatus'),
            'visible'=>!Yii::app()->user->isTeacher(),
        ),
        array(
            'name'=>'TM_QN_Show_Option',
            'value'=>'Questions::OptionAlias("OptionStatus",$data->TM_QN_Show_Option)',
            'filter'=>Questions::OptionAlias('OptionStatus'),
        ),        
        /*array(
            'name'=>'TM_QN_Syllabus_Id',
            'type' => 'raw',
            'value'=>'($data->TM_QN_Syllabus_Id!="0")?Syllabus::model()->findByPk($data->TM_QN_Syllabus_Id)->TM_SB_Name:""',
            'filter'=>CHtml::listData(Syllabus::model()->findAll(array('condition' => "TM_SB_Status='0'",'order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),

        ),*/
        array(
            'name'=>'TM_QN_Standard_Id',
            'value'=>'($data->TM_QN_Standard_Id!="0")?Standard::model()->findByPk($data->TM_QN_Standard_Id)->TM_SD_Name:""',
            'filter'=>CHtml::listData(Standard::model()->findAll(array('condition' => "TM_SD_Status='0' AND TM_SD_Id IN (".$planstandards.")",'order' => 'TM_SD_Id')),'TM_SD_Id','TM_SD_Name'),

        ),
        array(
            'name'=>'TM_QN_Topic_Id',
            'value'=>'($data->TM_QN_Topic_Id!="0")?Chapter::model()->findByPk($data->TM_QN_Topic_Id)->TM_TP_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_Standard_Id) ? Chapter::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_TP_Standard_Id = :standard AND TM_TP_Status='0'",
                        'params' => array(':standard' => $model->TM_QN_Standard_Id),
                        'order' => 'TM_TP_order ASC '
                    ))) : Chapter::model()->findAll(array('condition' => "TM_TP_Status='0'",'order' => ' TM_TP_order ASC')),'TM_TP_Id','TM_TP_Name'),
        ),
        array(
            'name'=>'TM_QN_Section_Id',
            'value'=>'($data->TM_QN_Section_Id!="0")?Topic::model()->findByPk($data->TM_QN_Section_Id)->TM_SN_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_Topic_Id) ? Topic::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_SN_Topic_Id = :chapter AND TM_SN_Status='0'",
                        'params' => array(':chapter' => $model->TM_QN_Topic_Id),
                        'order' => 'TM_SN_order ASC'
                    ))) : Topic::model()->findAll(array('condition' => "TM_SN_Status='0'",'order' => 'TM_SN_order ASC')),'TM_SN_Id','TM_SN_Name'),
        ),


        array(
            'name'=>'TM_QN_Dificulty_Id',
            'value'=>'($data->TM_QN_Dificulty_Id!="0")?Difficulty::model()->findByPk($data->TM_QN_Dificulty_Id)->TM_DF_Name:""',
            'filter'=>CHtml::listData(Difficulty::model()->findAll(array('order' => 'TM_DF_Name')),'TM_DF_Id','TM_DF_Name'),

        ),
        array(
            'name'=>'TM_QN_Type_Id',
            'value'=>'($data->TM_QN_Type_Id!="0")?Types::model()->findByPk($data->TM_QN_Type_Id)->TM_TP_Name:""',
            'filter'=>CHtml::listData(Types::model()->findAll(array('order' => 'TM_TP_Name')),'TM_TP_Id','TM_TP_Name'),

        ),

        array(
            'name'=>'TM_QN_Teacher_Id',
            'value'=>'($data->TM_QN_Teacher_Id!="0")?Questions::model()->GetTeacher($data->TM_QN_Teacher_Id,$data->TM_QN_Id):""',
            'filter'=>SchoolQuestions::model()->GetTeachorList($id),
            //'value'=>'($data->TM_QN_Teacher_Id!="0")?Teachers::model()->findByPk($data->TM_QN_Teacher_Id)->TM_TH_Name:""',
            //'filter'=>CHtml::listData(Teachers::model()->findAll(array('order' => 'TM_TH_Name')),'TM_TH_Id','TM_TH_Name'),

        ),
        array(
            'name'=>'TM_QN_CreatedBy',
            'type'=>'raw',
            'value'=>'Questions::model()->CheckUser($data->TM_QN_CreatedBy,$data->TM_QN_Id)',
            'filter'=>false
            //'filter'=>SchoolQuestions::model()->GetCreatorList(),
        ),
        /*array(
            'name'=>'TM_QN_CreatedOn',
            'type'=>'raw',
            'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->TM_QN_CreatedOn))',
            'filter'=>false,
        ),*/
        array(
            'name'=>'TM_QN_UpdatedBy',
            'type'=>'raw',
            'value'=>'($data->TM_QN_UpdatedBy!="0")?Questions::model()->CheckUser($data->TM_QN_UpdatedBy,$data->TM_QN_Id):""',
            'filter'=>false
            //'filter'=>CHtml::listData(Profile::model()->findAll(array('order' => 'user_id')),'user_id','firstname'),
        ),
        /*array(
            'name'=>'TM_QN_UpdatedOn',
            'type'=>'raw',
            'value'=>'($data->TM_QN_UpdatedOn!="0000-00-00")?Yii::app()->dateFormatter->format("d MMM y",strtotime($data->TM_QN_UpdatedOn)):""',
            'filter'=>false,
        ),
        array(
            'name'=>'TM_QN_Publisher_Id',
            'type'=>'raw',
            'value'=>'($data->TM_QN_Publisher_Id!="0")?Publishers::model()->findByPk($data->TM_QN_Publisher_Id)->TM_PR_Name:""',
            'filter'=>CHtml::listData(Publishers::model()->findAll(array('order' => 'TM_PR_Id')),'TM_PR_Id','TM_PR_Name'),
        ),*/
        /*
        // for standard admin filter removed code
        ->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))
                    
        // filter ends here
        array(
            'name'=>'TM_QN_Question',
            'value'=>'strip_tags($data->TM_QN_Question)',

        ),*/
/*        array(
            'name'=>'TM_QN_Subject_Id',
            'value'=>'Subject::model()->findByPk($data->TM_QN_Subject_Id)->TM_ST_Name',
            'filter'=>CHtml::listData(Subject::model()->findAll(array('order' => 'TM_ST_Name')),'TM_ST_Id','TM_ST_Name'),
        ),
		'TM_QN_Topic_Id',
		'TM_QN_Section_Id',
		'TM_QN_Dificulty_Id',
		'TM_QN_Type_Id',
		'TM_QN_Question',
		'TM_QN_Image',
		'TM_QN_Answer_Type',
		'TM_QN_Option_Count',
		'TM_QN_Editor',
		'TM_QN_Totalmarks',
		'TM_QN_CreatedOn',
		'TM_QN_CreatedBy',
		'TM_QN_Status',
		*/

	),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
)); ?>

    </div>

</div>
    <?php 
	Yii::app()->clientScript->registerScript('popup', "
	$(document).on('click','.showpopup',function(){		
		var id=$(this).attr('data-id');
		
		if($('#preview'+id).is(':visible'))
		{
				$('.popup').hide();
		}
		else
		{
				$('.popup').hide();
				$('#preview'+id).fadeIn();
		}
		
		return false;
	})
	");
	?>
<script type="text/javascript">
    $( document ).ajaxComplete(function() { 
      MathJax.Hub.Queue(["Typeset",MathJax.Hub, "questions-grid"]);
    });
</script>