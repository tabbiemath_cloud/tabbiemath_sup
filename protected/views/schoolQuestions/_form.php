<style>
    .wrs_editor td
    {
        padding: 0 !important;
    }
    .errorMessage
    {
        color: red;
    }
</style>
<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
if($_GET['assignment']!=''):
    $statusstyle='style="display:none"';
    $newstd=Yii::app()->session['standard'];
    Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('teachers/managecustomquestions/'.$_GET['assignment'])."';
    });
");
else:
    $statusstyle='';
    $newstd=$standards;
    Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('schoolQuestions/admin/'.$id)."';
    });
");
endif;
$lastid=Questions::model()->find(array('order'=>'TM_QN_Id DESC','limit'=>1))->TM_QN_Id;
$tempid=($lastid+1).rand(999,9999999).rand(1111,1111111);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'questions-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    /// See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data','accept-charset'=>"UTF-8"),
)); ?>
        <!-- Publisher,syllabus,standard,Topic,Section-->
        <div class="row brd1">            
            <div class="col-lg-2">
                <input type="hidden" name="imagetempid" id="imagetempid" value="<?php echo $tempid;?>">
                <input type="hidden" name="imagetype" id="imagetype" value="">
                <input type="hidden" name="imagesection" id="imagesection"  value="">
                <input type="hidden" name="imagefieldname" id="imagefieldname"  value="">  
                <?php echo $form->hiddenField($model,'TM_QN_Publisher_Id',array('value'=>'0'));?>
                <?php echo $form->hiddenField($model,'TM_QN_School_Id',array('value'=>$id));?>
                <?php echo $form->hiddenField($model,'availability',array('value'=>'0'));?>
                <?php 
                $ref=rand(11111111, 99999999);
                echo $form->hiddenField($model,'TM_QN_QuestionReff',array('value'=>$ref));?>
                <?php echo $form->hiddenField($model,'TM_QN_Syllabus_Id',array('value'=>$school->TM_SCL_Sylllabus_Id));?>                                  
                <?php echo $form->labelEx($model,'TM_QN_Standard_Id',array('class'=>'font2')); ?>
                <?php echo $form->dropDownList($model,'TM_QN_Standard_Id',CHtml::listData(Standard::model()->findAll(array("condition"=>"TM_SD_Id IN (".$newstd.")",'order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),array('empty'=>'Select','class'=>'form-control','ajax' =>
            array(
                'type'=>'POST', //request type
                'url'=>CController::createUrl('topic/GetChapterList'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                //Style: CController::createUrl('currentController/methodToCall')
                'update'=>'#Questions_TM_QN_Topic_Id', //selector to update
                'data'=>'js:{id:$(this).val()}'
                //leave out the data key to pass all form values through
            ))); ?>
                <?php echo $form->error($model,'TM_QN_Standard_Id'); ?>
            </div>
            <div class="col-lg-2">
                <?php echo $form->labelEx($model,'TM_QN_Topic_Id',array('class'=>'font2')); ?>
                <?php echo $form->dropDownList($model,'TM_QN_Topic_Id',array('empty'=>'Select'),array('class'=>'form-control','ajax' =>
            array(
                'type'=>'POST', //request type
                'url'=>CController::createUrl('schoolQuestions/GetTopicList'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
                //Style: CController::createUrl('currentController/methodToCall')
                'update'=>'#Questions_TM_QN_Section_Id', //selector to update
                'data'=>'js:{id:$(this).val()}'
                //leave out the data key to pass all form values through
            ))); ?>
                <?php echo $form->error($model,'TM_QN_Topic_Id'); ?>
            </div>
            <div class="col-lg-3">
                <?php echo $form->labelEx($model,'TM_QN_Section_Id',array('class'=>'font2')); ?>
                <?php echo $form->dropDownList($model,'TM_QN_Section_Id',array('empty'=>'Select'),array('class'=>'form-control')); ?>
                <?php //echo $form->dropDownList($model,'TM_QN_Section_Id',array(),array('empty'=>'Select','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_QN_Section_Id'); ?>
            </div>
        </div>
        <!--Ends-->

        <!-- Difficulty,Type,availability-->
        <div class="row brd1">
            <div class="col-lg-4">
                <div class="form-group tarea">
                    <?php echo $form->labelEx($model,'TM_QN_Dificulty_Id',array('class'=>'font2')); ?>
                    <?php $listdata=CHtml::listData(Difficulty::model()->findAll(array("condition"=>"TM_DF_Status = '0'",'order' => 'TM_DF_Name')),'TM_DF_Id','TM_DF_Name');?>
                    <?php echo $form->radioButtonList($model,'TM_QN_Dificulty_Id',$listdata,array('template'=>'<div class="radio"> {input} {label}</div>', 'separator'=>'', 'encode'=>true,'class'=>'difficselect')); ?>
                    <?php echo $form->error($model,'TM_QN_Dificulty_Id'); ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group tarea">
                <?php echo $form->labelEx($model,'TM_QN_Type_Id',array('class'=>'font2')); ?>
                <?php echo $form->error($model,'TM_QN_Type_Id'); ?>
                <?php echo $form->radioButtonList($model,'TM_QN_Type_Id',CHtml::listData(Types::model()->findAll(array("condition"=>"TM_TP_Status = '0'",'order'=>'TM_TP_Id')),'TM_TP_Id','TM_TP_Name'),array('template'=>'<div class="radio">{input} {label}</div>','class'=>'typeselect','separator'=>'')); ?>

                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group tarea">
                <?php echo $form->labelEx($model,'TM_QN_Teacher_Id'); ?>
                <?php echo $form->error($model,'TM_QN_Teacher_Id'); ?>
               <?php
                    $teachers=CHtml::listData(Teachers::model()->with('user')->findAll(array("condition"=>"status = '1' AND TM_TH_SchoolId=".$id,'order' => 'TM_TH_Name')),'TM_TH_Id','TM_TH_Name');
                    if(count($teachers)=='1'):
                        $selectarray=array('class'=>'form-control');
                    else:
                        $selectarray=array('empty'=>'Select','class'=>'form-control','options' => array(Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.Yii::app()->user->id))->TM_TH_Id=>array('selected'=>true)));
                    endif;                    
                    echo $form->dropDownList($model,'TM_QN_Teacher_Id',$teachers,$selectarray); ?>
                </div>
            </div>
        </div>
        <div class="row brd1">
            <div class="col-lg-12 mater_bg_border">

                <div class="col-lg-4">
                    <?php echo $form->labelEx($model,'TM_QN_Pattern'); ?>
                    <?php echo $form->textField($model,'TM_QN_Pattern',array('class'=>'form-control','value'=>$ref)); ?>
                    <?php echo $form->error($model,'TM_QN_Pattern'); ?>
                </div>

                <div class="col-lg-4" <?=$statusstyle;?>>
                    <?php echo $form->labelEx($model,'TM_QN_Status'); ?>
                    <?php
                    if($model->isNewRecord):
                        echo $form->dropDownList($model,'TM_QN_Status',Questions::itemAlias('QuestionStatus'),array('class'=>'form-control','options' => array('10'=>array('selected'=>true))));
                    else:
                            echo $form->dropDownList($model,'TM_QN_Status',Questions::itemAlias('QuestionStatus'),array('class'=>'form-control'));
                    endif;
                    ?>
                    <?php echo $form->error($model,'TM_QN_Status'); ?>
                </div>
                <div class="col-lg-4">
                    <?php echo $form->labelEx($model,'TM_QN_Show_Option'); ?>
                    <?php echo $form->dropDownList($model,'TM_QN_Show_Option',Questions::OptionAlias('OptionStatus'),array('class'=>'form-control'));?>
                    <?php echo $form->error($model,'TM_QN_Show_Option'); ?>
                </div>                
                <!--<div class="col-lg-4">
                    <?php /*echo $form->labelEx($model,'TM_QN_QuestionReff'); */?>
                    <?php /*echo $form->textField($model,'TM_QN_QuestionReff',array('class'=>'form-control')); */?>
                    <?php /*echo $form->error($model,'TM_QN_QuestionReff'); */?>
                </div>-->


            </div>
            <!--<div class="col-lg-12 mater_bg_border">
                <div class="col-lg-4">
                    <?php /*echo $form->labelEx($model,'TM_QN_QuestionNumber'); */?>
                    <?php /*echo $form->textField($model,'TM_QN_QuestionNumber',array('class'=>'form-control')); */?>
                    <?php /*echo $form->error($model,'TM_QN_QuestionNumber'); */?>
                </div>

                
            </div>-->
        </div>
        <div class="row brd1">
            <div class="col-lg-12 mater_bg_border">
                  <div class="col-lg-4">
                    <label>Skills</label>
                   
                   <?php $skills=CHtml::listData(TmSkill::model()->findAll(array('order' => 'tm_skill_name',"condition"=>"tm_standard_id IN (".$newstd.")")),'id','tm_skill_name');
                    if(count($skills)=='1'):
                        $selectarray=array('class'=>'form-control','multiple'=>true);
                    else:
                        $selectarray=array('empty'=>'Select','class'=>'form-control','multiple'=>true);
                    endif;
                    echo $form->dropDownList($model,'TM_QN_Skill',$skills,$selectarray); ?>
                    <?php echo $form->error($model,'TM_QN_Skill'); ?>
                  </div>

                   <div class="col-lg-4">
                    <label>Additional Info</label>
                   <?php $skills=CHtml::listData(QuestionType::model()->findAll(array('order' => 'TM_Question_Type',"condition"=>"tm_standard_id IN (".$newstd.")")),'id','TM_Question_Type');
                    if(count($skills)=='1'):
                        $selectarray=array('empty'=>'Select','class'=>'form-control','multiple'=>true);
                    else:
                        $selectarray=array('empty'=>'Select','class'=>'form-control','multiple'=>true);
                    endif;
                    echo $form->dropDownList($model,'TM_QN_Question_type',$skills,$selectarray); ?>
                    <?php echo $form->error($model,'TM_QN_Question_type'); ?>
                   </div>
                   <div class="col-lg-4">
                     <label>Notes</label>
                    <?php echo $form->textField($model,'TM_QN_Info',array('class'=>'form-control')); ?>
                    <?php echo $form->error($model,'TM_QN_Info'); ?>
                   </div>

            </div>
        </div>
        <!--Ends -->

        <!--Questions For  Multiple option,single option-->
        <div class="row brd1 questionsec" id="multysingle" >
            <?php $this->renderPartial('_multypleoption',array('form'=>$form,'model'=>$model));?>
        </div>
        <!-- Ends -->
        <!--Questions For  True/False option-->
        <div class="row brd1 questionsec" id="truefalse" >
            <?php $this->renderPartial('_truefalse',array('form'=>$form,'model'=>$model));?>
        </div>
        <!-- Ends -->
        <!--Questions For  Text option-->
        <div class="row brd1 questionsec" id="textoption">
            <?php $this->renderPartial('_textoption',array('form'=>$form,'model'=>$model));?>
        </div>
        <!-- Ends -->
        <!--Questions For  Multypart option-->
        <div class="row brd1 questionsec" id="multypart">
            <?php $this->renderPartial('_multypart',array('form'=>$form,'model'=>$model));?>
        </div>
        <!-- Ends -->
        <!--Questions For  Paperonly option-->
        <div class="row brd1 questionsec" id="paperonly">
            <?php $this->renderPartial('_paperonly',array('form'=>$form,'model'=>$model));?>
        </div>
        <!-- Ends -->
        <!--Questions For  Paperonly Multypart option-->
        <div class="row brd1 questionsec" id="paperonlymultypart">
            <?php $this->renderPartial('_paperonlymultypart',array('form'=>$form,'model'=>$model));?>
        </div>
        <!-- Ends -->
        <div class="row brd1">
            <div class="col-lg-12 mater_bg_border">
                <div class="form-group">
                    <?php echo $form->labelEx($model,'TM_QN_Solutions',array('class'=>'font2')); ?>
                    <?php echo $form->textArea($model,'TM_QN_Solutions',array('class'=>'form-control','rows'=>'6')); ?>
                    <?php echo $form->error($model,'TM_QN_Solutions'); ?>
                </div>
                <div class="form-group">
				 <span class="btn btn-default btn-file">
                   <i class="fa fa-picture-o fa-lg yellow"></i>
                        Upload Image <input type="file" name="solutionimage"  data-type="solution" data-section="main" data-name="solutionimage"  id="Questions_TM_QN_SolutionImage" class="imageselect solutionimage">
                 </span>
                </div>
                <div class="media col-md-3 imagepreview" id="Questions_TM_QN_SolutionImageDiv">
                    <figure class="pull-left imagespan">

                        <div class="deletespan" data-type="solution" data-section="main"  data-parent="Questions_TM_QN_SolutionImageDiv" data-file="Questions_TM_QN_SolutionImage" ><span class="glyphicon glyphicon-trash"> </span></div>
                        <img class="media-object img-rounded img-responsive" id="Questions_TM_QN_SolutionImagePreview" src="" alt="placehold.it/350x250">
                    </figure>
                </div>
            </div>

        </div>
        <div class="row brd1">


            <div class="col-lg-3 pull-right">
                <?php echo $form->hiddenField($model,'TM_QN_Subject_Id',array('value'=>'1')); ?>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block','id'=>'createquest')); ?>
            </div>
            <div class="col-lg-3 pull-right">
                <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
            </div>
        </div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
        $(function(){        
        $('#Questions_TM_QN_Standard_Id').on('change',function(){
            var id =$(this).val();
            $.ajax({
            type: 'POST',
            url: '../../questions/getskill',
            dataType:"json",
            data:{id:id},
            success: function(data){
                if(data.error=='No')

                {
                    $('#Questions_TM_QN_Skill').html(data.result);
                }
                else
                {
                    alert(data.error);
                }
            }
        });

            $.ajax({
            type: 'POST',
            url: '../../questions/getqtype',
            dataType:"json",
            data:{id:id},
            success: function(data){
                if(data.error=='No')

                {
                    $('#Questions_TM_QN_Question_type').html(data.result);
                }
                else
                {
                    alert(data.error);
                }
            }
        });  
                

        })
    })
</script>


<?php

    Yii::app()->clientScript->registerScript('imagepreview', "

        $(document).on('mouseenter', '.imagespan', function() {
            $(this).find('.deletespan').stop( true, true ).show()
        });
        $(document).on('mouseleave', '.imagespan', function() {
             $(this).find('.deletespan').stop( true, true ).hide()
        });
        $(document).on('click','.deletespan',function(){
            var file=$(this).attr('data-file');
            var parent=$(this).attr('data-parent');
            var type=$(this).attr('data-type');
            var section=$(this).attr('data-section');
            var imagetempid=$('#imagetempid').val();

            $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('schoolQuestions/deleteimagetemp')."',
              data: { imagetempid: imagetempid, type: type,section:section }
            })
              .done(function( data ) {
                $('#'+file).replaceWith($('#'+file).clone());
                $('#'+parent).hide();
                if(type=='answer')
                {
                    $('#checkanswer'+section).val('false');
                }

              });


        })
        $(document).on('change','.imageselect',function(){
            $('#imagetype').val($(this).attr('data-type'));
            $('#imagesection').val($(this).attr('data-section'));
            var section=$(this).attr('data-section');
            var type=$(this).attr('data-type');
            $('#imagefieldname').val($(this).attr('name'));
            var dataid=$(this).attr('id');
            var form = $('#questions-form')[0]; // You need to use standart javascript object here
            var formData = new FormData(form);
            $.ajax({
                    url: '".Yii::app()->createUrl('schoolQuestions/uploadimagetemp')."', // Url to which the request is send
                    type: 'POST',             // Type of request to be send, called as method
                    data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                    beforeSend: function() {
                        $('#'+dataid+'Preview').attr('src','".Yii::app()->request->baseUrl."/images/imageloader.gif');
                        $('#'+dataid+'Div').show();
                      },
                    success: function(data)   // A function to be called if request succeeds
                    {
                        $('#'+dataid+'Div').hide();
                        $('#'+dataid+'Preview').attr('src',data);
                        $('#'+dataid+'Div').slideDown();
                        if(type=='answer')
                        {
                            $('#checkanswer'+section).val('true');
                        }

                    }
            });
            //readURL(this)
        })
        /*function readURL(input) {
            var identifier=$(input).attr('id');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+identifier+'Div').show();
                $('#'+identifier+'Preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }*/
    ");
    Yii::app()->clientScript->registerScript('editor', "
        $( '#Questions_TM_QN_Questionchoice' ).ckeditor();
        $( '#Questions_TM_QN_QuestionTF' ).ckeditor();
        $( '#Questions_TM_QN_QuestionTEXT' ).ckeditor();
        $( '#Questions_TM_QN_Question' ).ckeditor();
        $( '#Questions_TM_QN_Solutions' ).ckeditor();
        $( '#Questions_TM_QN_QuestionPaperOnly' ).ckeditor();
        $( '#Questions_TM_QN_QuestionMPPaperOnly' ).ckeditor();
        $( '.answer' ).ckeditor();
    ");
    Yii::app()->clientScript->registerScript('questiontype', "
        $('.typeselect').click(function(){
           if($(this).val()=='1' | $(this).val()=='2')
           {
                 $('#showoptiondiv').show();
                $('.questionsec').fadeOut('fast');
                if($(this).val()=='1')
                {
                    $('.correctacswerchoice').attr('type','checkbox')
                    var idcount=1;
                    $('.correctacswerchoice').each(function(){
                         $(this).attr('name','correctanswer'+idcount);
                         $(this).val('1');
                        idcount++
                    });

                }
                else if($(this).val()=='2')
                {
                    $('.correctacswerchoice').attr('type','radio')
                    var idcount=1;
                    $('.correctacswerchoice').each(function(){
                         $(this).attr('name','correctanswer')
                         $(this).val(idcount);
                        idcount++
                    });
                    $('.correctacswerchoice').attr('name','correctanswer')
                }
                $('#multysingle').slideDown('slow');
                $('.availelect').prop('disabled',false);
           }
           else if($(this).val()=='3')
           {
                 $('#showoptiondiv').show();
                $('.questionsec').fadeOut('fast');
                $('#truefalse').slideDown('slow');
                $('.availelect').prop('disabled',false);
           }
           else if($(this).val()=='4')
           {
                 $('#showoptiondiv').hide();
                $('.questionsec').fadeOut('fast');
                $('#textoption').slideDown('slow');
                $('.availelect').prop('disabled',false);
           }
           else if($(this).val()=='5')
           {
                 $('#showoptiondiv').hide();
                $('.questionsec').fadeOut('fast');
                $('#multypart').slideDown('slow');
                $('.availelect').prop('disabled',false);
           }
           else if($(this).val()=='6')
           {
                $('#showoptiondiv').hide();
                $('.questionsec').fadeOut('fast');

                $('#paperonly').slideDown('slow');
           }
           else if($(this).val()=='7')
           {
                $('#showoptiondiv').hide();
                $('.questionsec').fadeOut('fast');
                
                $('#paperonlymultypart').slideDown('slow');
           }
        });
    ");
    if($model->TM_QN_Type_Id!=''):
        Yii::app()->clientScript->registerScript('selectedtype', "
            var selectedtype='".$model->TM_QN_Type_Id."';
            if(selectedtype=='1' | selectedtype=='2')
           {
                $('.questionsec').hide();
                $('#multysingle').slideDown();
           }
           else if(selectedtype=='3')
           {
                $('.questionsec').hide();
                $('#truefalse').slideDown();
           }
           else if(selectedtype=='4')
           {
                $('.questionsec').hide()
                $('#textoption').slideDown();
           }
           else if(selectedtype=='5')
           {
                $('.questionsec').hide()
                $('#multypart').slideDown();
           }
           else if(selectedtype=='6')
           {
                $('.questionsec').fadeOut('fast');
                
                $('#paperonly').slideDown('slow');
           }
           else if(selectedtype=='7')
           {
                $('.questionsec').fadeOut('fast');                
                $('#paperonlymultypart').slideDown('slow');
           }
        ");
    endif;
    Yii::app()->clientScript->registerScript('enablemarks', "
        $('.choicemark').prop('disabled',true);
        $('.correctmark').prop('disabled',false);
        $(document).on('click','.correctacswer',function(){
            var type=$('.typeselect:checked').val();
            if(type=='2')
            {
                var itemid=$(this).prop('id');
                var lastChar = itemid.substr(itemid.length - 1);
                $('.choicemark').prop('disabled',true).val('');
                if($(this).is(':checked'))
                {
                    $('#choicemark'+lastChar).prop('disabled',false);
                }
            }
            else if(type='1')
            {
                var itemid=$(this).prop('id');
                var lastChar = itemid.substr(itemid.length - 1);
                if($(this).is(':checked'))
                {
                    $('#choicemark'+lastChar).prop('disabled',false);
                }
                else
                {
                  $('#choicemark'+lastChar).prop('disabled',true).val('');
                }
            }
        });
        $(document).on('click','.MPCcorrectanswer',function(){
            var itemid=$(this).prop('id');
            var lastChar = itemid.substr(itemid.length - 2);
            var type=$(this).prop('type');
            if(type=='checkbox')
            {
                if($(this).is(':checked'))
                {
                    $('#MPCmarks'+lastChar).prop('disabled',false);
                }
                else
                {
                  $('#MPCmarks'+lastChar).prop('disabled',true);
                }
            }
            else
            {
                var identifier=lastChar.substr(0,1);
                $('.MPCmarks'+identifier).prop('disabled',true).val('');
                if($(this).is(':checked'))
                {
                    $('#MPCmarks'+lastChar).prop('disabled',false);
                }
            }
        })
    ");
Yii::app()->clientScript->registerScript('validation', "

        $('#questions-form').submit(function()
        {
            CKupdate();
            var error=0;
            $('.errorMessage').remove();
            if($('#Questions_TM_QN_Publisher_Id').val()=='')
            {
                $('#Questions_TM_QN_Publisher_Id').after('<div class=\"errorMessage\">Select Publisher.</div>')
                error++;
            }
            if($('#Questions_TM_QN_Syllabus_Id').val()=='')
            {
                $('#Questions_TM_QN_Syllabus_Id').after('<div class=\"errorMessage\">Select Syllabus.</div>')
                error++;
            }
            if($('#Questions_TM_QN_Standard_Id').val()=='')
            {
                $('#Questions_TM_QN_Standard_Id').after('<div class=\"errorMessage\">Select Year.</div>')
                error++;
            }
            if($('#Questions_TM_QN_Topic_Id').val()=='')
            {
                $('#Questions_TM_QN_Topic_Id').after('<div class=\"errorMessage\">Select Topic.</div>')
                error++;
            }
            if($('#Questions_TM_QN_Section_Id').val()=='')
            {
                $('#Questions_TM_QN_Section_Id').after('<div class=\"errorMessage\">Select Section</div>')
                error++;
            }
            if($('.difficselect:checked').length ==0)
            {
                $('#ytQuestions_TM_QN_Dificulty_Id').after('<div class=\"errorMessage\">Select Dificulty</div>')
                error++;
            }
            if($('.typeselect:checked').length ==0)
            {
                $('#ytQuestions_TM_QN_Type_Id').after('<div class=\"errorMessage\">Select Type</div>')
                error++;
            }

            if($('#Questions_TM_QN_Teacher_Id').val() =='')
            {
                $('#Questions_TM_QN_Teacher_Id').after('<div class=\"errorMessage\">Select Provider</div>')
                error++;
            }
            if($('#Questions_TM_QN_Pattern').val() =='')
            {
                $('#Questions_TM_QN_Pattern').after('<div class=\"errorMessage\">Enter Pattern</div>')
                error++;
            }
            if($('.typeselect:checked').val()=='1')
            {
                if($('#Questions_TM_QN_Questionchoice').val()=='')
                {
                    $('#Questions_TM_QN_Questionchoice').after('<div class=\"errorMessage\">Enter Question</div>')
                    error++;
                }
                if($('#Questions_TM_QN_Option_Countchoice').val()=='')
                {
                    $('#Questions_TM_QN_Option_Countchoice').after('<div class=\"errorMessage\">Enter No. of Options</div>')
                    error++;
                }
                else
                {
                    var optioncount=$('#Questions_TM_QN_Option_Countchoice').val();
                    for(i=1;i<=optioncount;i++)
                    {
                       if($('.choiceanswer'+i).val()=='' & $('#checkanswer'+i).val()=='false')
                        {
                            $('.choiceanswer'+i).after('<div class=\"errorMessage\">Enter Answer Option OR Select an Image</div>')
                            error++;
                        }
                        if($('#correctacswerchoice'+i).is(':checked'))
                        {
                              if($('#choicemark'+i).val()=='')
                              {
                                  $('#choicemark'+i).after('<div class=\"errorMessage\">Enter Mark</div>')
                                  error++;
                              }
                        }
                    }

                }
                if($('.correctacswerchoice:checked').length ==0)
                {
                    $('.choiceanswer'+1).before('<div class=\"errorMessage\">Atleast one correct answer has to be checked</div>')
                    error++;
                }


            }
            else if($('.typeselect:checked').val()=='2')
            {
                if($('#Questions_TM_QN_Questionchoice').val()=='')
                {
                    $('#Questions_TM_QN_Questionchoice').after('<div class=\"errorMessage\">Enter Question</div>')
                    error++;
                }
                if($('#Questions_TM_QN_Option_Countchoice').val()=='')
                {
                    $('#Questions_TM_QN_Option_Countchoice').after('<div class=\"errorMessage\">Enter No. of Options</div>')
                    error++;
                }
                else
                {
                    var optioncount=$('#Questions_TM_QN_Option_Countchoice').val();
                    for(i=1;i<=optioncount;i++)
                    {
                       if($('.choiceanswer'+i).val()=='' & $('#checkanswer'+i).val()=='false')
                        {
                            $('.choiceanswer'+i).after('<div class=\"errorMessage\">Enter Answer Option OR Select an Image</div>')
                            error++;
                        }
                        if($('#correctacswerchoice'+i).is(':checked'))
                        {
                              if($('#choicemark'+i).val()=='')
                              {
                                  $('#choicemark'+i).after('<div class=\"errorMessage\">Enter Mark</div>')
                                  error++;
                              }
                        }
                    }
                }
                if($('.correctacswerchoice:checked').length ==0)
                {
                    $('.choiceanswer'+1).before('<div class=\"errorMessage\">Atleast one correct answer has to be checked</div>')
                    error++;
                }
                else if($('.correctacswerchoice:checked').length >1)
                {
                    $('.choiceanswer'+1).before('<div class=\"errorMessage\">Only one answer to be checked as correct.</div>')
                    error++;
                }

            }
            else if($('.typeselect:checked').val()=='3')
            {
                if($('#Questions_TM_QN_QuestionTF').val()=='')
                {
                    $('#Questions_TM_QN_QuestionTF').after('<div class=\"errorMessage\">Enter Question</div>')
                    error++;
                }
                if(!$('.correctanswerTF').is(':checked'))
                {
                   $('#TFoption').before('<div class=\"errorMessage\">Please select correct answer for True Or False</div>');
                   error++;
                }
                if($('#marksTF').val()=='')
                {
                    $('#marksTF').after('<div class=\"errorMessage\">Enter Mark</div>')
                }
            }
            else if($('.typeselect:checked').val()=='4')
            {
                if($('#Questions_TM_QN_QuestionTEXT').val()=='')
                {
                    $('#Questions_TM_QN_QuestionTEXT').after('<div class=\"errorMessage\">Enter Question</div>')
                    error++;
                }
                if($('#answeroptionsTEXT').val()=='')
                {
                    $('#answeroptionsTEXT').after('<div class=\"errorMessage\">Enter Answer Options</div>')
                    error++;
                }
                if($('#marksTEXT').val()=='')
                {
                    $('#marksTEXT').after('<div class=\"errorMessage\">Enter Marks</div>')
                    error++;
                }
            }
            else if($('.typeselect:checked').val()=='5')
            {
                if($('#multipleqstnum').val()=='0')
                {
                    $('#multipleqstnum').after('<div class=\"errorMessage\">Select No. Of Parts</div>')
                    error++;
                }
                else
                {
                    var questioncount=$('#multipleqstnum').val();
                    for(j=1;j<=questioncount;j++)
                    {
                        if($('.multypartQuestions'+j).val()=='')
                        {
                           $('.multypartQuestions'+j).after('<div class=\"errorMessage\">Enter question</div>')
                           error++;
                        }
                        if($('#multypartType'+j).val()=='0')
                        {
                           $('#multypartType'+j).after('<div class=\"errorMessage\">Select Type</div>')
                           error++;
                        }
                        else if($('#multypartType'+j).val()=='1')
                        {
                            if($('#multypartOptions'+j).val()=='0')
                            {
                                $('#multypartOptions'+j).after('<div class=\"errorMessage\">Select No. Of Options</div>')
                                error++;
                            }
                            else
                            {
                                var MPoptioncount=$('#multypartOptions'+j).val();
                                for(k=1;k<=MPoptioncount;k++)
                                {
                                    if($('.MPCanswer'+j+k).val()=='' & $('#checkanswer'+j+k).val()=='false')
                                    {
                                        $('.MPCanswer'+j+k).after('<div class=\"errorMessage\">Enter Answer OR Select an Image</div>')
                                        error++;
                                    }
                                    if($('#MPCcorrectanswer'+j+k).is(':checked'))
                                    {
                                          if($('#MPCmarks'+j+k).val()=='')
                                          {
                                              $('#MPCmarks'+j+k).after('<div class=\"errorMessage\">Enter Mark</div>')
                                              error++;
                                          }
                                    }
                                }
                                if($('.MPCcorrectanswer'+j+':checked').length ==0)
                                {
                                    $('.MPCanswer'+j+'1').before('<div class=\"errorMessage\">Atleast one correct answer has to be checked</div>')
                                    error++;
                                }
                            }
                        }
                        else if($('#multypartType'+j).val()=='2')
                        {
                            if($('#multypartOptions'+j).val()=='0')
                            {
                                $('#multypartOptions'+j).after('<div class=\"errorMessage\">Select No. Of Options</div>')
                                error++;
                            }
                            else
                            {
                                var MPoptioncount=$('#multypartOptions'+j).val();
                                for(k=1;k<=MPoptioncount;k++)
                                {
                                    if($('.MPCanswer'+j+k).val()=='' & $('#checkanswer'+j+k).val()=='false')
                                    {
                                        $('.MPCanswer'+j+k).after('<div class=\"errorMessage\">Enter Answer OR Select an Image</div>')
                                        error++;
                                    }
                                    if($('#MPCcorrectanswer'+j+k).is(':checked'))
                                    {
                                          if($('#MPCmarks'+j+k).val()=='')
                                          {
                                              $('#MPCmarks'+j+k).after('<div class=\"errorMessage\">Enter Mark</div>')
                                              error++;
                                          }
                                    }
                                }
                                if($('.MPCcorrectanswer'+j+':checked').length ==0)
                                {
                                    $('.MPCanswer'+j+'1').before('<div class=\"errorMessage\">Atleast one correct answer has to be checked</div>')
                                    error++;
                                }
                                else if($('.MPCcorrectanswer'+j+':checked').length >1)
                                {
                                    $('.MPCanswer'+j+'1').before('<div class=\"errorMessage\">Only one answer to be checked as correct.</div>')
                                    error++;
                                }
                            }
                        }
                        else if($('#multypartType'+j).val()=='3')
                        {
                            if(!$('.correctanswerTF'+j).is(':checked'))
                            {
                               $('#MPTFoption'+j).before('<div class=\"errorMessage\">Please select correct answer for True Or False</div>');
                               error++;
                            }
                            if($('#MPmarksTF'+j).val()=='')
                            {
                                $('#MPmarksTF'+j).after('<div class=\"errorMessage\">Enter Mark</div>')
                            }
                        }
                        else if($('#multypartType'+j).val()=='4')
                        {
                            if($('#MPansweroptions'+j).val()=='')
                            {
                                $('#MPansweroptions'+j).after('<div class=\"errorMessage\">Enter Answer Options</div>')
                                error++;
                            }
                            if($('#MPmarksTEXT'+j).val()=='')
                            {
                                $('#MPmarksTEXT'+j).after('<div class=\"errorMessage\">Enter Marks</div>')
                                error++;
                            }
                        }
                    }
                }
            }
            else if($('.typeselect:checked').val()=='6')
            {
                if($('#Questions_TM_QN_QuestionPaperOnly').val()=='')
                {
                    $('#Questions_TM_QN_QuestionPaperOnly').after('<div class=\"errorMessage\">Enter Question</div>')
                    error++
                }
                if($('#Papermarks').val()=='')
                {
                    $('#Papermarks').after('<div class=\"errorMessage\">Enter marks</div>');
                    error++
                }
            }
            else if($('.typeselect:checked').val()=='7')
            {
               if($('#PoMPqstnum').val()=='0')
               {
                    $('#PoMPqstnum').after('<div class=\"errorMessage\">No. Of Parts</div>');
                     error++
               }
               else
               {
                    var paperparts=$('#PoMPqstnum').val();
                    for(i=1;i<=paperparts;i++)
                    {
                        if($('#PapermultypartQuestions'+i).val()=='')
                        {
                            $('#PapermultypartQuestions'+i).after('<div class=\"errorMessage\">Enter Question</div>');
                             error++
                        }
                        if($('#PaperMPmarks'+i).val()=='')
                        {
                            $('#PaperMPmarks'+i).after('<div class=\"errorMessage\">Enter Marks</div>');
                             error++
                        }
                    }
               }
            }
            if(error!='0')
            {
                return false;
            }
            else
            {
                return true;
            }

        })
        function CKupdate()
        {
            for ( instance in CKEDITOR.instances )
            {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    ");

    

?>
<!--if($('#ytQuestions_TM_QN_Type_Id').val()=='1')
{
    if($('#TM_QN_Questionchoice').val()=='')
    {

    }
}-->