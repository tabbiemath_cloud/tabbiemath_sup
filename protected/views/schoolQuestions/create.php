<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
    'Questions'=>array('admin'),
    'Create',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
    array('label'=>'Manage Questions', 'url'=>array('admin','id'=>$id,)), 
    array('label'=>'Manage System Questions', 'url'=>array('systemquestions', 'id'=>$id)),
    array('label'=>'Hidden System Questions', 'url'=>array('importquestions', 'id'=>$id)),   
);

if(Yii::app()->user->isSchoolAdmin()):?>
<h3>Create Question</h3>
        <?php 
        elseif(Yii::app()->user->isTeacher()):?>
<div class="row">
    <div class="col-lg-12">
        <h3>Create Question</h3>        
    </div>
   
    <!-- /.col-lg-12 -->
</div>
<?php 
        endif;
?>


<?php $this->renderPartial('_form', array('model'=>$model,'id'=>$id,'school'=>$school,'standards'=>$standards)); ?>