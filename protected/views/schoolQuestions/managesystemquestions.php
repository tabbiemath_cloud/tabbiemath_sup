<?php if($teacher==9 || $usermode=='Teacher'):?>
<style>
#page-wrapper
{
    width:250%;
}
#wrapper
{
        background-color: #f8f8f8;
        width:268.7%;
}
.navbar
{
    width:37%;
}
</style>
<?php endif;?>
<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs=array(
	'Schools'=>array('index'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'View School', 'url'=>array('School/view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'Create Question', 'url'=>array('create', 'id'=>$id)),
    array('label'=>'Manage Questions', 'url'=>array('admin','id'=>$id,)), 
    array('label'=>'Manage System Questions', 'url'=>array('systemquestions', 'id'=>$id)),
    array('label'=>'Hidden System Questions', 'url'=>array('importquestions', 'id'=>$id)),    	
);

?>
<h3>Manage System Questions</h3>
<div class="row brd1" <?php if($teacher!=9 & $usermode!='Teacher'): echo "style=width:250%;overflow:auto;"; endif;?>>
  <div class="col-lg-12">
        <div class="col-lg-2" style="margin-bottom: 10px;">
            <div class="col-sm-6">
                <?php
                echo CHtml::ajaxLink("Hide questions", $this->createUrl('schoolQuestions/hidequestions',array('id'=>$id)), array(
                    "type" => "post",
                    "data" => 'js:{theIds : $.fn.yiiGridView.getChecked("questions-grid","question").toString(),"school":"'.$id.'"}',
                    "success" => 'js:function(data){ $.fn.yiiGridView.update("questions-grid")  }' ),array(
                        'class' => 'btn btn-warning btn-block'
                    )
                );
                ?>
            </div>
        </div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'questions-grid',
	'dataProvider'=>$model->searchmastermanage($id),
        'pager' => array('maxButtonCount' => 5),
	'filter'=>$model,
    'selectableRows'=>2,
	'columns'=>array(
        array(
            'class'=>'CCheckBoxColumn',
            'name'=>'question',
            'value'=>'$data->TM_QN_Id',
            'id'=>'question'
        ),        
        'TM_QN_QuestionReff',        
        array(
            'name'=>'TM_QN_Question',
            'type'=>'raw',
            'value'=>'Questions::model()->GetQuestion($data->TM_QN_Id)',
        ),

        array(
            'name'=>'TM_QN_Standard_Id',
            'value'=>'($data->TM_QN_Standard_Id!="0")?Standard::model()->findByPk($data->TM_QN_Standard_Id)->TM_SD_Name:""',
            'filter'=>CHtml::listData(Standard::model()->findAll(array('condition' => "TM_SD_Status='0' AND TM_SD_Id IN (".$planstandards.")",'order' => 'TM_SD_Id')),'TM_SD_Id','TM_SD_Name'),
        ),
        array(
            'name'=>'TM_QN_Topic_Id',
            'value'=>'($data->TM_QN_Topic_Id!="0")?Chapter::model()->findByPk($data->TM_QN_Topic_Id)->TM_TP_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_Standard_Id) ? Chapter::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_TP_Standard_Id = :standard AND TM_TP_Status='0'",
                        'params' => array(':standard' => $model->TM_QN_Standard_Id),
                        'order' => 'TM_TP_order ASC '
                    ))) : Chapter::model()->findAll(array('condition' => "TM_TP_Status='0'",'order' => ' TM_TP_order ASC')),'TM_TP_Id','TM_TP_Name'),
        ),
        array(
            'name'=>'TM_QN_Section_Id',
            'value'=>'($data->TM_QN_Section_Id!="0")?Topic::model()->findByPk($data->TM_QN_Section_Id)->TM_SN_Name:""',
            'filter'=>
                CHtml::listData(
                    is_numeric($model->TM_QN_Topic_Id) ? Topic::model()->findAll(new CDbCriteria(array(
                        'condition' => "TM_SN_Topic_Id = :chapter AND TM_SN_Status='0'",
                        'params' => array(':chapter' => $model->TM_QN_Topic_Id),
                        'order' => 'TM_SN_order ASC'
                    ))) : Topic::model()->findAll(array('condition' => "TM_SN_Status='0'",'order' => 'TM_SN_order ASC')),'TM_SN_Id','TM_SN_Name'),
        ),
        array(
            'name'=>'TM_QN_Dificulty_Id',
            'value'=>'($data->TM_QN_Dificulty_Id!="0")?Difficulty::model()->findByPk($data->TM_QN_Dificulty_Id)->TM_DF_Name:""',
            'filter'=>CHtml::listData(Difficulty::model()->findAll(array('order' => 'TM_DF_Name')),'TM_DF_Id','TM_DF_Name'),

        ),
        array(
            'name'=>'TM_QN_Type_Id',
            'value'=>'($data->TM_QN_Type_Id!="0")?Types::model()->findByPk($data->TM_QN_Type_Id)->TM_TP_Name:""',
            'filter'=>CHtml::listData(Types::model()->findAll(array('order' => 'TM_TP_Name')),'TM_TP_Id','TM_TP_Name'),

        ),
        array(
            'name'=>'TM_QN_Publisher_Id',
            'type'=>'raw',
            'value'=>'($data->TM_QN_Publisher_Id!="0")?Publishers::model()->findByPk($data->TM_QN_Publisher_Id)->TM_PR_Name:""',
            'filter'=>CHtml::listData(Publishers::model()->findAll(array('order' => 'TM_PR_Id')),'TM_PR_Id','TM_PR_Name'),
        ),
        /*
        // for standard admin filter removed code
        ->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))
                    
        // filter ends here
        array(
            'name'=>'TM_QN_Question',
            'value'=>'strip_tags($data->TM_QN_Question)',

        ),*/
/*        array(
            'name'=>'TM_QN_Subject_Id',
            'value'=>'Subject::model()->findByPk($data->TM_QN_Subject_Id)->TM_ST_Name',
            'filter'=>CHtml::listData(Subject::model()->findAll(array('order' => 'TM_ST_Name')),'TM_ST_Id','TM_ST_Name'),
        ),
		'TM_QN_Topic_Id',
		'TM_QN_Section_Id',
		'TM_QN_Dificulty_Id',
		'TM_QN_Type_Id',
		'TM_QN_Question',
		'TM_QN_Image',
		'TM_QN_Answer_Type',
		'TM_QN_Option_Count',
		'TM_QN_Editor',
		'TM_QN_Totalmarks',
		'TM_QN_CreatedOn',
		'TM_QN_CreatedBy',
		'TM_QN_Status',
		*/

	),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
)); ?>

    </div>

</div>

<script type="text/javascript">
    $( document ).ajaxComplete(function() { 
        MathJax.Hub.Queue(["Typeset",MathJax.Hub, "questions-grid"]);
    });
</script>