<?php
/* @var $this PlanController */
/* @var $model Plan */

$this->breadcrumbs=array(
    'Questions'=>array('admin'),
    'Create',
);

$this->menu=array(
    array('label'=>'Manage Questions', 'class'=>'nav-header'),    
	array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'View School', 'url'=>array('School/view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'List Questions', 'url'=>array('admin','id'=>$id,)),    	         
    array('label'=>'View Question', 'url'=>array('view', 'id'=>$id,'question'=>$model->TM_QN_Id)),
    array('label'=>'Copy Question', 'url'=>array('copy', 'id'=>$id,'question'=>$model->TM_QN_Id)),    
    //array('label'=>'Copy Question', 'url'=>'#', 'linkOptions'=>array('submit'=>array('copy','id'=>$model->TM_QN_Id))),
    array('label'=>'Delete Question', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$id,'question'=>$model->TM_QN_Id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage System Questions', 'url'=>array('systemquestions', 'id'=>$id)), 
);
if(Yii::app()->user->isSchoolAdmin()):?>
<h3>Update Question <?php echo $model->TM_QN_Id; ?></h3>
        <?php 
        elseif(Yii::app()->user->isTeacher()):?>
<div class="row">
    <div class="col-lg-12">
        <h3>Update Question <?php echo $model->TM_QN_Id; ?></h3>        
    </div>
   
    <!-- /.col-lg-12 -->
</div>
<?php 
        endif;
?>



<?php $this->renderPartial('_formupdate', array('model'=>$model,'id'=>$id,'school'=>$school,'standards'=>$standards)); ?>