<?php
/* @var $this LevelsController */
/* @var $model Levels */

$this->breadcrumbs=array(
	'Levels'=>array('admin'),
	$model->TM_LV_Id,
);
$this->menu=array(
    array('label'=>'Manage Levels', 'class'=>'nav-header'),
    array('label'=>'List Levels', 'url'=>array('admin')),
    array('label'=>'Create Level', 'url'=>array('create')),
    array('label'=>'Update Level', 'url'=>array('update', 'id'=>$model->TM_LV_Id)),
    array('label'=>'Delete Level', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_LV_Id),'confirm'=>'Are you sure you want to delete this item?')),

);

?>
<h3>View <?php echo $model->TM_LV_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_LV_Id',
                'TM_LV_Name',
                array(
                    'name'=>'TM_LV_icon',
                    'type'=>'raw',
                    'value'=>(!empty($model->TM_LV_icon)?CHtml::image(Yii::app()->baseUrl . "/site/Imagerender/id/".$model->TM_LV_icon."?size=large&type=level&width=100&height=100"):""),

                ),
                'TM_LV_Minpoint',
                'TM_LV_MaxPoint',
                'TM_LV_Status',
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Levels::itemAlias("LevelStatus",$model->TM_LV_Status),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>

    </div>
</div>
