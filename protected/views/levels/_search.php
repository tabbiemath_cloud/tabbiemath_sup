<?php
/* @var $this LevelsController */
/* @var $model Levels */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_LV_Id'); ?>
		<?php echo $form->textField($model,'TM_LV_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_LV_Name'); ?>
		<?php echo $form->textField($model,'TM_LV_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_LV_icon'); ?>
		<?php echo $form->textField($model,'TM_LV_icon',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_LV_Minpoint'); ?>
		<?php echo $form->textField($model,'TM_LV_Minpoint'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_LV_MaxPoint'); ?>
		<?php echo $form->textField($model,'TM_LV_MaxPoint'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_LV_Status'); ?>
		<?php echo $form->textField($model,'TM_LV_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->