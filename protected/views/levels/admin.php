<?php
/* @var $this LevelsController */
/* @var $model Levels */

$this->breadcrumbs=array(
	'Levels'=>array('admin'),
	'Manage',
);
$this->menu=array(
    array('label'=>'Manage Levels', 'class'=>'nav-header'),
    array('label'=>'List Levels', 'url'=>array('admin')),
    array('label'=>'Create Levels', 'url'=>array('create')),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#levels-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Manage Levels</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'levels-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                'TM_LV_Id',
                'TM_LV_Name',
                array(
                    'name'=>'TM_LV_icon',
                    'type'=>'raw',
                    'value'=>'(!empty($data->TM_LV_icon)?CHtml::image(Yii::app()->baseUrl . "/site/Imagerender/id/".$data->TM_LV_icon."?size=large&type=level&width=100&height=100"):"")',

                ),
                'TM_LV_Minpoint',
                'TM_LV_MaxPoint',
                array(
                    'name'=>'TM_LV_Status',
                    'value'=>'Levels::itemAlias("LevelStatus",$data->TM_LV_Status)',
                    'filter'=>Coupons::itemAlias("LevelStatus"),
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    //'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        /*'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            )),*/
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
