<?php
/* @var $this LevelsController */
/* @var $data Levels */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_LV_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_LV_Id), array('view', 'id'=>$data->TM_LV_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_LV_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_LV_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_LV_icon')); ?>:</b>
	<?php echo CHtml::encode($data->TM_LV_icon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_LV_Minpoint')); ?>:</b>
	<?php echo CHtml::encode($data->TM_LV_Minpoint); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_LV_MaxPoint')); ?>:</b>
	<?php echo CHtml::encode($data->TM_LV_MaxPoint); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_LV_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_LV_Status); ?>
	<br />


</div>