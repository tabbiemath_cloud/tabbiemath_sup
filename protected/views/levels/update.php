<?php
/* @var $this CouponsController */
/* @var $model Coupons */

$this->breadcrumbs=array(
    'Levels'=>array('admin'),
    $model->TM_LV_Id=>array('view','id'=>$model->TM_LV_Id),
    'Update',
);

$this->menu=array(
    array('label'=>'Manage Levels', 'class'=>'nav-header'),
    array('label'=>'List Levels', 'url'=>array('admin')),
    array('label'=>'Create Level', 'url'=>array('create')),
    array('label'=>'View Level', 'url'=>array('view', 'id'=>$model->TM_LV_Id)),
);
?>

    <h3>Update <?php echo $model->TM_LV_Id; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>