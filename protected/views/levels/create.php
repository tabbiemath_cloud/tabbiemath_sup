<?php
/* @var $this CouponsController */
/* @var $model Coupons */

$this->breadcrumbs=array(
    'Levels'=>array('admin'),
    'Create',
);

$this->menu=array(
    array('label'=>'Manage Levels', 'class'=>'nav-header'),
    array('label'=>'List Levels', 'url'=>array('admin')),
);
?>

    <h3>Create Level</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>