<?php
/* @var $this TypesController */
/* @var $model Types */

$this->breadcrumbs=array(
	'Types'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Question Types', 'class'=>'nav-header'),
	array('label'=>'List Question Types', 'url'=>array('admin')),
	array('label'=>'Create Question Types', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#types-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="row-fluid">
    <div class="col-lg-12">
        <h3>Manage Question Types</h3>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'types-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                'TM_TP_Name',
                array(
                    'name'=>'TM_TP_Status',
                    'value'=>'Types::itemAlias("TypesStatus",$data->TM_TP_Status)',
                ),

                array(
                    'class'=>'CButtonColumn',
                ),
            ),
        )); ?>
    </div>
</div>
