<?php
/* @var $this TypesController */
/* @var $model Types */

$this->breadcrumbs=array(
	'Types'=>array('admin'),
	$model->TM_TP_Id=>array('view','id'=>$model->TM_TP_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Question Types', 'class'=>'nav-header'),
	array('label'=>'List Question Types', 'url'=>array('admin')),
	array('label'=>'Create Question Types', 'url'=>array('create')),
	array('label'=>'View Question Types', 'url'=>array('view', 'id'=>$model->TM_TP_Id)),
);
?>

<h3>Update Question Type <?php echo $model->TM_TP_Id; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>