<?php
/* @var $this TypesController */
/* @var $model Types */

$this->breadcrumbs=array(
	'Types'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Question Types', 'class'=>'nav-header'),
	array('label'=>'List Question Types', 'url'=>array('admin')),

);
?>

<h3>Create Question Type</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>