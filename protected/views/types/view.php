<?php
/* @var $this TypesController */
/* @var $model Types */

$this->breadcrumbs=array(
	'Types'=>array('admin'),
	$model->TM_TP_Id,
);

$this->menu=array(
    array('label'=>'Manage Question Types', 'class'=>'nav-header'),
	array('label'=>'List Question Types', 'url'=>array('admin')),
	array('label'=>'Create Question Types', 'url'=>array('create')),
	array('label'=>'Update Question Types', 'url'=>array('update', 'id'=>$model->TM_TP_Id)),
	array('label'=>'Delete Question Types', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_TP_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3>View Question Type #<?php echo $model->TM_TP_Id; ?></h3>

<?php
$id=$model->TM_TP_Status;
$userid=$model->TM_TP_CreatedBy;
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_TP_Id',
		'TM_TP_Name',
        array(
            'label'=>'Created By',
            'type'=>'raw',
            'value'=>User::model()->GetUser($userid),
        ),
        array(
            'label'=>'Status',
            'type'=>'raw',
            'value'=>Types::itemAlias("TypesStatus",$id),
        ),

	),
)); ?>
