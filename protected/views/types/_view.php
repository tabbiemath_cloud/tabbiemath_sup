<?php
/* @var $this TypesController */
/* @var $data Types */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TP_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_TP_Id), array('view', 'id'=>$data->TM_TP_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TP_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_TP_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_TP_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_TP_Status); ?>
	<br />


</div>