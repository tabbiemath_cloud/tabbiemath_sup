<?php
/* @var $this QuestionTypeController */
/* @var $model QuestionType */

$this->breadcrumbs=array(
	'Question Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Additional Info', 'url'=>array('admin')),
	array('label'=>'Manage Additional Info', 'url'=>array('admin')),
);
?>

<h1>Create Additional Info</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>