<?php
/* @var $this QuestionTypeController */
/* @var $data QuestionType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_Question_Type')); ?>:</b>
	<?php echo CHtml::encode($data->TM_Question_Type); ?>
	<br />


</div>