<?php
/* @var $this QuestionTypeController */
/* @var $model QuestionType */

$this->breadcrumbs=array(
	'Question Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Additional Info', 'url'=>array('admin')),
	array('label'=>'Create Additional Info', 'url'=>array('create')),
	array('label'=>'View Additional Info', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Additional Info', 'url'=>array('admin')),
);
?>

<h1>Update <?php echo $model->TM_Question_Type; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>