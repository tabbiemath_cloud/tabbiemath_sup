<?php
/* @var $this SyllabusController */
/* @var $model Syllabus */

$this->breadcrumbs=array(
	'Syllabuses'=>array('admin'),
	$model->id,
);

$this->menu=array(
    array('label'=>'Manage Additional Info', 'class'=>'nav-header'),
	array('label'=>'List Additional Info', 'url'=>array('admin')),
	array('label'=>'Create Additional Info', 'url'=>array('create')),
	array('label'=>'Update Additional Info', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Additional Info', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View <?php echo $model->TM_Question_Type; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'id',
                  array(
                        'name'=>'tm_standard_id',
                        'type'=>'raw',
                        'value'=>function($model)
                         {
                            $sdname=Standard::model()->findByPk($model->tm_standard_id)->TM_SD_Name;
                            return $sdname; 
                            
                        },
                    ),

                    array(
                        'name'=>'TM_Qt_Syllabus_Id',
                        'type'=>'raw',
                        'header'=>'Syllabus',
                        'value'=>function($model)
                         {
                            $syllabus_name=Syllabus::model()->findByPk($model->TM_Qt_Syllabus_Id)->TM_SB_Name;
                            return $syllabus_name; 
                            
                        },
                     ),
                'TM_Question_Type',
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
