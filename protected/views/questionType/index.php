<?php
/* @var $this QuestionTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Question Types',
);

$this->menu=array(
	array('label'=>'Create QuestionType', 'url'=>array('create')),
	array('label'=>'Manage QuestionType', 'url'=>array('admin')),
);
?>

<h1>Question Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
