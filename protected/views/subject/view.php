<?php
/* @var $this SubjectController */
/* @var $model Subject */

$this->breadcrumbs=array(
	'Subjects'=>array('admin'),
	$model->TM_ST_Id,
);

$this->menu=array(
    array('label'=>'Manage Subject', 'class'=>'nav-header'),
	array('label'=>'List Subject', 'url'=>array('admin')),
	array('label'=>'Create Subject', 'url'=>array('create')),
	array('label'=>'Update Subject', 'url'=>array('update', 'id'=>$model->TM_ST_Id)),
	array('label'=>'Delete Subject', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_ST_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3>View <?php echo $model->TM_ST_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $id=$model->TM_ST_Status;
        $userid=$model->TM_ST_CreatedBy;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_ST_Id',
                'TM_ST_Name',
                'TM_ST_CreatedOn',
                array(
                    'label'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Subject::itemAlias("SubjectStatus",$id),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
