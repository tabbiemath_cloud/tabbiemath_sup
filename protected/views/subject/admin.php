<?php
/* @var $this SubjectController */
/* @var $model Subject */

$this->breadcrumbs=array(
	'Subjects'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Subject', 'class'=>'nav-header'),
	array('label'=>'List Subject', 'url'=>array('admin')),
	array('label'=>'Create Subject', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#subject-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>Manage Subjects</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'subject-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                'TM_ST_Name',
                array(
                    'name'=>'TM_ST_Status',
                    'value'=>'Subject::itemAlias("SubjectStatus",$data->TM_ST_Status)',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            )),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
