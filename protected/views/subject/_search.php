<?php
/* @var $this SubjectController */
/* @var $model Subject */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_ST_Id'); ?>
		<?php echo $form->textField($model,'TM_ST_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_ST_Name'); ?>
		<?php echo $form->textField($model,'TM_ST_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_ST_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_ST_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_ST_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_ST_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_ST_Status'); ?>
		<?php echo $form->textField($model,'TM_ST_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->