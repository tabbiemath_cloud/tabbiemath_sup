<?php
/* @var $this SubjectController */
/* @var $data Subject */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_ST_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_ST_Id), array('view', 'id'=>$data->TM_ST_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_ST_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_ST_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_ST_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_ST_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_ST_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_ST_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_ST_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_ST_Status); ?>
	<br />


</div>