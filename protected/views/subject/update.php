<?php
/* @var $this SubjectController */
/* @var $model Subject */

$this->breadcrumbs=array(
	'Subjects'=>array('admin'),
	$model->TM_ST_Id=>array('view','id'=>$model->TM_ST_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Subject', 'class'=>'nav-header'),
	array('label'=>'List Subject', 'url'=>array('admin')),
	array('label'=>'Create Subject', 'url'=>array('create')),
	array('label'=>'View Subject', 'url'=>array('view', 'id'=>$model->TM_ST_Id)),
);
?>

<h3>Update <?php echo $model->TM_ST_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>