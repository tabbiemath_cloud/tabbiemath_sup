<?php
/* @var $this SubjectController */
/* @var $model Subject */

$this->breadcrumbs=array(
	'Subjects'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Subject', 'class'=>'nav-header'),
	array('label'=>'List Subject', 'url'=>array('admin')),
);
?>

<h3>Create Subject</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>