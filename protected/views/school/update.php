<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs=array(
	'Schools'=>array('index'),
	$model->TM_SCL_Id=>array('view','id'=>$model->TM_SCL_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Schools', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin')),    
	array('label'=>'Create School', 'url'=>array('create')),
	array('label'=>'View School', 'url'=>array('view', 'id'=>$model->TM_SCL_Id)),	
);
?>

<h3>Update School <?php echo $model->TM_SCL_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>