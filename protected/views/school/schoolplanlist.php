
    <div class="col-lg-12">
        <h3><?php echo $school->TM_SCL_Name.' Plans'; ?></h3>
        <div class="col-lg-4 pull-left" style="margin-bottom: 10px;padding-left: 0px;;">            
                <a href="<?php echo Yii::app()->createUrl('school/addplan',array('id'=>$school->TM_SCL_Id))?>" class="btn btn-warning btn-block">Add subscription</a>            
        </div>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,
            'columns'=>array(                
                array(
                    'name'=>'Plan',
                    'type'=>'raw',
                    'value'=>'Plan::model()->findByPk($data->TM_SPN_PlanId)->TM_PN_Name',
                ),
                array(
                    'name'=>'Standard',
                    'type'=>'raw',
                    'value'=>'Standard::model()->findByPk($data->TM_SPN_StandardId)->TM_SD_Name',
                ),
                array(
                    'name' => 'Agreed Rate',
                    'value'=>'$data->TM_SPN_Rate',                    
                ),
                array(
                    'name' => 'Currency',
                    'value'=>'Currency::model()->findByPk($data->TM_SPN_Currency)->TM_CR_Name',                    
                ),                                 
                array(
                    'name' => 'Maximum Students',
                    'value'=>'$data->TM_SPN_No_of_Students',                    
                ),                                                                          
                array(
                    'name' => 'Subscribed On',
                    'value' => 'date("d-m-Y",strtotime($data->TM_SPN_CreatedOn))',
                ),                                                                         
                array(
                    'name' => 'Start Date',
                    'value' => 'date("d-m-Y",strtotime($data->TM_SPN_StartDate))',
                ),                                                           
                array(
                    'name' => 'expires On',                    
                    'value'=>'$data->TM_SPN_ExpieryDate',                    
                ),                                                             
                array(
                    'name' => 'Type',
                    'value'=>'SchoolPlan::itemAlias("PaymentType",$data->TM_SPN_Type)',                    
                ),                 
                array(
                    'name'=>'Status',
                    'value'=>'SchoolPlan::itemAlias("SubsciptionStatus",$data->TM_SPN_Status)',
                ),                                       
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{activate}{inactivate}{deleteplan}',//
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (                        
                        'deleteplan'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',                            
                            'options'=>array(
                                'title'=>'Delete Subscription',
                                'class'=>'deleteplan'
                            ),
                            'url'=>'Yii::app()->createUrl("school/deleteplan",array("id"=>$data->TM_SPN_Id,"school"=>$data->TM_SPN_SchoolId))',
                        ),
                        'activate'=>array(
                            'label'=>'<span class="glyphicon glyphicon-ok"></span>',
                            'visible'=>'$data->TM_SPN_Status>0',
                            'options'=>array(
                                'title'=>'Activate Subscription',
                                'class'=>'activate'
                            ),
                            'url'=>'Yii::app()->createUrl("school/activate",array("id"=>$data->TM_SPN_Id,"school"=>$data->TM_SPN_SchoolId))',
                        ),
                        'inactivate'=>array(
                            'label'=>'<span class="glyphicon glyphicon-remove"></span>',
                            'visible'=>'$data->TM_SPN_Status<1',
                            'options'=>array(
                                'title'=>'Deactivate Subscription',
                                'class'=>'deactivate'
                            ),
                            'url'=>'Yii::app()->createUrl("school/deactivate",array("id"=>$data->TM_SPN_Id,"school"=>$data->TM_SPN_SchoolId))',
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
        )); ?>
    </div>
