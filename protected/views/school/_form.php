<?php
/* @var $this SchoolController */
/* @var $model School */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('school/admin')."';
    });
");
?>

<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'school-form',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype' => 'multipart/form-data')
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
)); ?>
    <div class="panel-body form-horizontal payment-form">
    <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Sylllabus_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_SCL_Sylllabus_Id',CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty'=>'Select Syllabus','class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Sylllabus_Id'); ?>
            </div>
        </div> 
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_Name',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Name'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Address',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textArea($model,'TM_SCL_Address',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Address'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Contactperson',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_Contactperson',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Contactperson'); ?>
            </div>
        </div> 
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Contactdesignation',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_Contactdesignation',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Contactdesignation'); ?>
            </div>
        </div> 
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Email',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_Email',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Email'); ?>
            </div>
        </div>  
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Phone',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_Phone',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Phone'); ?>
            </div>
        </div>
  
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Mobile',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_Mobile',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_Mobile'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_City',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_City',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_City'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_State',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_SCL_State',array('class'=>'form-control')); ?>                
                <?php echo $form->error($model,'TM_SCL_State'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_SCL_Country_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_SCL_Country_Id',CHtml::listData(Country::model()->findAll(array('condition' => 'TM_CON_Status=0','order' => 'TM_CON_Id')),'TM_CON_Id','TM_CON_Name'),array('empty'=>'Select Country','class'=>'form-control')); ?>                                
                <?php echo $form->error($model,'TM_SCL_Country_Id'); ?>
            </div>
        </div>

        <!--<div class="form-group">
            <?php // echo $form->labelEx($model,'TM_SCL_Logo',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php // echo $form->dropDownList($model,'TM_SCL_Logo',CHtml::listData(Country::model()->findAll(array('condition' => 'TM_CON_Status=0','order' => 'TM_CON_Id')),'TM_CON_Id','TM_CON_Name'),array('empty'=>'Select Country','class'=>'form-control')); ?>                                
                <?php // echo $form->error($model,'TM_SCL_Logo'); ?>
            </div>
        </div>-->        
        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Headteacher',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->textField($model,'TM_SCL_Headteacher',array('class'=>'form-control')); ?>                                
                <?php  echo $form->error($model,'TM_SCL_Headteacher'); ?>
                <?php echo $form->hiddenField($model,'TM_SCL_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
            </div>
        </div>

        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Billing_Reference',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->textField($model,'TM_SCL_Billing_Reference',array('class'=>'form-control')); ?>                                
                <?php  echo $form->error($model,'TM_SCL_Billing_Reference'); ?>                
            </div>
        </div>
        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Billing_Contact',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->textField($model,'TM_SCL_Billing_Contact',array('class'=>'form-control')); ?>                                
                <?php  echo $form->error($model,'TM_SCL_Billing_Contact'); ?>                
            </div>
        </div>     
        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Billing_Contact_Email',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->emailField($model,'TM_SCL_Billing_Contact_Email',array('class'=>'form-control')); ?>                                
                <?php  echo $form->error($model,'TM_SCL_Billing_Contact_Email'); ?>                
            </div>
        </div>    
        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Billing_Notes',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->TextArea($model,'TM_SCL_Billing_Notes',array('class'=>'form-control')); ?>                                
                <?php  echo $form->error($model,'TM_SCL_Billing_Notes'); ?>                
            </div>
        </div> 

        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Discount',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->radioButtonList($model,'TM_SCL_Discount',array('0'=>'No','1'=>'Yes'),array('separator'=>' ')); ?>                                
                <?php  echo $form->error($model,'TM_SCL_Discount'); ?>                
            </div>
        </div>



        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Discount_Amount',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->textField($model,'TM_SCL_Discount_Amount',array('class'=>'form-control')); ?>                                
                <?php  echo $form->error($model,'TM_SCL_Discount_Amount'); ?>                
            </div>
        </div>   

        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Discount_Type',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_SCL_Discount_Type',Coupons::itemAlias('Coupontype'),array('empty'=>'Select Type','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_SCL_Discount_Type'); ?>               
            </div>
        </div>

        <div  class="form-group">
           <?php echo $form->labelEx($model,'TM_SCL_Image',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
            <?php echo $form->fileField($model,'TM_SCL_Image',array('class'=>'form-control')); ?>
             <p>Optimal Size 1200 * 150 px </p>
              <?php  echo $form->error($model,'TM_SCL_Image'); ?>
            </div>
        </div>

        <?php if($model->TM_SCL_Image!=''){ ?>
                <div  class="form-group" id="thumbnaildiv">
                    <label class="col-sm-3 control-label">Uploaded Custom Header</label>
                    <div class="col-sm-9">
                       <ul class="list-group">
                           <li class="list-group-item">
                               <?php echo $model->TM_SCL_Image; ?>
                               <a id="deletethumbnail" data-id="<?php echo $model->TM_SCL_Id; ?>" href="javascript:void(0);" style="float:right;" title="Delete File"><span   class="glyphicon  glyphicon-trash"></span></a>
                           </li>
                       </ul>
                    </div>
                </div>
        <?php } ?>
        
        <div class="form-group">
            <?php  echo $form->labelEx($model,'TM_SCL_Code',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php  echo $form->textField($model,'TM_SCL_Code',array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_SCL_Code'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
            <div class="col-sm-3 pull-right">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    
</div><!-- form -->

<script type="text/javascript">
    $(document).on('click','#deletethumbnail',function(){
       var cnfrm =  confirm("Are you sure want to delete ?");
        var idval = $(this).data("id");

        if(cnfrm==true)
        {
            $('#thumbnaildiv').remove();
            $.ajax({
                method: 'POST',
               
                url: '../deletefile',
                dataType: "json",
                async:false,
                data: {idval:idval},
                success: function(data){
                    if(data=='yes')
                    {
                        //$('#thumbnaildiv').hide();
                    }
                }
            });
        }

    });
</script>