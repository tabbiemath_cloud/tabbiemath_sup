<?php
$this->breadcrumbs=array(
    UserModule::t('Student')=>array('studentlist'),
    UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('view','id'=>$student->TM_STU_School_Id), 'visible'=>UserModule::isAllowedSchool()),       
    array('label' => 'List Students', 'url' => array('manageStudents','id'=>$student->TM_STU_School_Id)),
    array('label'=>'Change Password', 'url'=>array('studentpassword','id'=>$student->TM_STU_School_Id,'stud'=>$model->id)),    
   /* array('label'=>'Change Password', 'url'=>array('studentpassword','id'=>$model->id)),*/
    array('label'=>'Edit Student', 'url'=>array('Editstudent','id'=>$student->TM_STU_School_Id,'studnet'=>$model->id)),
    array('label'=>'Add Student', 'url'=>array('Addstudent','id'=>$student->TM_STU_School_Id)),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<h3>View <?php echo $student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover" id="yw0">

            <tr>
                <th>Username</th>
                <td><?php echo $model->username;?></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><?php echo $student->TM_STU_First_Name;?></td>
            </tr>

            <tr>
                <th>Last Name</th>
                <td><?php echo $student->TM_STU_Last_Name;?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $student->TM_STU_Email;?></td>
            </tr>
            <tr>
                <th>Name of School</th>
                <td><?php echo School::model()->findByPk($student->TM_STU_School_Id)->TM_SCL_Name;?></td>
            </tr>
            <tr>
                <th>City</th>
                <td><?php echo $student->TM_STU_City;?></td>
            </tr>
            <tr>
                <th>State</th>
                <td><?php echo $student->TM_STU_State;?></td>
            </tr>
            <?php if($student->TM_STU_Country_Id!=''):  ?>
            <tr>
                <th>Country</th>
                <td><?php
                    $country=Country::model()->find(array('condition'=>'TM_CON_Id='.$student->TM_STU_Country_Id));
                    echo $country->TM_CON_Name;?></td>
            </tr>
            <tr>
                <th>User Type</th>
                <td><?php echo User::itemAlias("UserLevel",$model->userlevel);?></td>
            </tr>
            <tr>
                <th>Created By</th>
                <td><?php
                    echo User::Creator($model->id);?></td>
            </tr>
            <tr>
                <th>Updated By</th>
                <td><?php
                    echo User::Updator($model->id);?></td>
            </tr>
            <?php endif;?>
            <?php if(count($plan)>0):?>
            <tr>
                <th colspan="2" style="text-align: center;">Subscriptions</th>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" class="table table-bordered table-hover">
                        <tr>
                            <th>Syllabus</th>
                            <th>Standard</th>
                            <th>Plan</th>
                            <th>Activated On</th>
                            <th>Status</th>
                        </tr>

                        <tr>
                            <td><?php echo Syllabus::model()->findByPk($plan->TM_SPN_SyllabusId)->TM_SB_Name;?></td>
                            <td><?php echo Standard::model()->findByPk($plan->TM_SPN_StandardId)->TM_SD_Name;?></td>
                            <td><?php echo Plan::model()->findByPk($plan->TM_SPN_PlanId)->TM_PN_Name;?></td>
                            <td><?php echo date('d-m-Y',strtotime($plan->TM_SPN_CreatedOn));?></td>
                            <td><?php echo ($plan->TM_SPN_Status=='0'?'Active':'Inactive');?></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <?php else:?>
            <tr>
                <th >Subscriptions</th>
                <td >No Subscriptions</td>
            </tr>
            <?php endif;?>
        </table>
    </div>
</div>    