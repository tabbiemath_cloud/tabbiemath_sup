<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs=array(
	'Schools'=>array('admin'),
	$model->TM_SCL_Id,
);
$plancount=count($plan);
$this->menu=array(
    array('label'=>'Manage School', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin')),
	array('label'=>'Create School', 'url'=>array('create'),'visible'=>UserModule::isSuperAdmin()),
	array('label'=>'Update School', 'url'=>array('update', 'id'=>$model->TM_SCL_Id),'visible'=>UserModule::isSuperAdmin()),
    array('label'=>'Manage Admins', 'url'=>array('manageStaff', 'id'=>$model->TM_SCL_Id)),
    array('label'=>'Manage Teachers', 'url'=>array('Teachers/ManageTeachers','id'=>$model->TM_SCL_Id)),    
    array('label'=>'Manage Students', 'url'=>array('manageStudents', 'id'=>$model->TM_SCL_Id)),
    array('label'=>'Manage Groups', 'url'=>array('SchoolGroups/managegroups', 'id'=>$model->TM_SCL_Id)),    
    array('label'=>'Manage Questions', 'url'=>array('SchoolQuestions/admin', 'id'=>$model->TM_SCL_Id),'visible'=>$plancount),
	array('label'=>'Delete School', 'url'=>'#','visible'=>UserModule::isSuperAdmin(),'visible'=>UserModule::isSuperAdmin(), 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_SCL_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3>View <?php echo $model->TM_SCL_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
<?php
    $userid=$model->TM_SCL_CreatedBy;
    $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_SCL_Id',
        array(
            'name'=>'TM_SCL_Sylllabus_Id',
            'type'=>'raw',
            'value'=>Syllabus::model()->findByPk($model->TM_SCL_Sylllabus_Id)->TM_SB_Name,
        ),        		
		'TM_SCL_Name',
		'TM_SCL_Address',
		'TM_SCL_Contactperson',
		'TM_SCL_Contactdesignation',
		'TM_SCL_Email',
		'TM_SCL_Phone',
		'TM_SCL_Mobile',
		'TM_SCL_City',
		'TM_SCL_State',
        array(
            'name'=>'TM_SCL_Country_Id',
            'type'=>'raw',
            'value'=>Country::model()->findByPk($model->TM_SCL_Country_Id)->TM_CON_Name,
        ),         		
		//'TM_SCL_Logo',
		'TM_SCL_Headteacher',
        'TM_SCL_Billing_Reference',
        'TM_SCL_Billing_Contact',
        'TM_SCL_Billing_Contact_Email',
        'TM_SCL_Billing_Notes',        
		'TM_SCL_CreatedOn',
        array(
            'label'=>'Created By',
            'type'=>'raw',
            'value'=>User::model()->GetUser($userid),
        ), 
        array(
            'name'=>'TM_SCL_Discount',
            'type'=>'raw',
            'value'=>($model->TM_SCL_Discount?'Yes':'No'),
        ),
        array(
            'name'=>'TM_SCL_Discount_Amount',
            'type'=>'raw',
            'value'=>$model->TM_SCL_Discount_Amount,
            'visibile'=>$model->TM_SCL_Discount
        ), 
        array(
            'name'=>'TM_SCL_Discount_Type',
            'type'=>'raw',
            'value'=>($model->TM_SCL_Discount_Type?'Percentage':'Amount'),
            'visibile'=>$model->TM_SCL_Discount
        ),
		'TM_SCL_Code',
        array(
            'type'=>'raw',
            'name'=>'TM_SCL_Image',
        
            'value' => function($model) { 

             if($model->TM_SCL_Image!='')
             {
                return CHtml::tag('img',array("title"=>"HeaderImage","src"=>Yii::app()->baseUrl."/images/headers/".$model->TM_SCL_Image,
                "style"=>"height:70px") );
             }
             else
             {
                return "No Image";
             }
             },
           //'value'=> CHtml::tag('img',array("title"=>"HeaderImage","src"=>Yii::app()->baseUrl."/images/headers/".$model->TM_SCL_Image,
                //"style"=>"height:70px") )

            ),
		//'TM_SCL_CreatedBy',
	),'htmlOptions' => array('class' => 'table table-bordered table-hover')
)); ?>
    </div>
</div>
