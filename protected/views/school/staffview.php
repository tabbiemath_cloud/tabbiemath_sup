<?php
$this->breadcrumbs=array(
	'Schools'=>array('admin'),
	'Manage',
);
$this->menu=array(
    array('label'=>'Manage Admin', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin'), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'View School', 'url'=>array('view','id'=>$school), 'visible'=>UserModule::isAllowedSchool()),    
    array('label'=>'List Admins', 'url'=>array('manageStaff', 'id'=>$school)),
    array('label'=>'Change Password', 'url'=>array('staffpassword','id'=>$school,'staff'=>$model->id)),
	array('label'=>'Add Admin', 'url'=>array('AddStaff', 'id'=>$school)),
	array('label'=>'Update Admin', 'url'=>array('Editstaff', 'id'=>$school,'master'=>$model->id)),  	 
    array('label'=>'Delete Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$school,'master'=>$model->id),'confirm'=>'Are you sure you want to delete this Staff?')),         	
);
?>
<h3>View Admin <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover" id="yw0">
            <tr>
                <th>Username</th>
                <td><?php echo $model->username;?></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><?php echo $model->profile->firstname;?></td>
            </tr>
            
            <tr>
                <th>Last Name</th>
                <td><?php echo $model->profile->lastname;?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $model->email;?></td>
            </tr> 
            
            <tr>
                <th>Phone Number</th>
                <td><?php echo $model->profile->phonenumber;?></td>
            </tr>
            <tr>
                <th>School</th>
                <td><?php echo School::model()->findByPk($model->school_id)->TM_SCL_Name;?></td>
            </tr>            
            <tr>
                <th>Admin Type</th>
                <td><?php echo User::itemAlias("StaffTypes",$schoolstaff->TM_SCS_Type);?></td>
            </tr>                    
        </table>
    </div>
</div>
