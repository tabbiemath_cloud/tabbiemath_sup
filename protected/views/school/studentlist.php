<?php
$this->breadcrumbs=array(
    UserModule::t('Student')=>array('studentlist'),
    UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),       
    array('label' => 'List Students', 'url' => array('manageStudents','id'=>$id)),
    array('label'=>'Add Student', 'url'=>array('Addstudent','id'=>$id)),
    array('label'=>'Import Students', 'url'=>array('ImportStudents', 'id'=>$id)),	
    //array('label'=>'Send Email Notfication', 'url'=>array('Sendregistrationemails','id'=>$id)),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<?php /*echo $this->renderPartial('_menu', array(
    'list'=> array(
        CHtml::link(UserModule::t('Create User'),array('create')),
    ),
));
*/?>
    <h3><?php echo UserModule::t("Manage Students"); ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <form method="get" id="searchform">
            <table class="table table-bordered table-hover table-striped admintable">
                <tr><th colspan="6"><h5><?php echo UserModule::t("Search"); ?></h5></th></tr>
                <tr>
                    <th>Student Name</th>
                    <th>Standard</th>
                    <th>Email Sent Status</th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td><input type="text" name="student" id="student" class="form-control" value="<?php echo ($_GET['student']!=''?$_GET['student']:'');?>"/></td>
                   <td>
                       <?php
                       $result = $this->getplan($id);
                       $listdata = CHtml::listData($result, 'TM_PN_Id', 'TM_SD_Name');

                       $selected = array();

                        foreach($result AS $resul):
                            if($_GET['plan']!=''):

                                $selected = array($_GET['plan']);
                            else:
                                $selected='';
                            endif;
                        endforeach;


                       echo CHtml::dropDownList('plan', $selected, $listdata, array('class' => 'form-control', 'empty' => 'Select Standard','id'=>'plan')); ?>
                    </td>
                    <td><?php
                       if(isset($_GET['mail'])):
                            if($_GET['mail']!=''):
                                $selected=array($_GET['mail']);
                            else:
                                $selected='';
                            endif;
                        else:
                            $selected='';
                        endif;
                     echo CHtml::dropDownList('mail', $selected, array('0'=>'Not Sent','1'=>'Sent'), array('class' => 'form-control', 'empty' => 'Select Email Status','id'=>'plan')); ?></td>
                    <td><button class="btn btn-warning btn-lg btn-block" name="search" value="search" type="submit">Search</button></td>
                    <td><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
                </tr>
            </table>
        </form>
    </div>
    <?php if($dataProvider!=''):?>
        <div class="col-lg-12" style="<?php echo $style;?>">
            <div class="col-lg-2" style="margin-bottom: 10px;">
                <div class="col-sm-12">
                    <?php
                    echo CHtml::ajaxLink("Send Mails", $this->createUrl('school/Sendregistrationemails'), array(
                        "type" => "post",
                        "data" => 'js:{theIds : $("#userids").val().toString()}',
                        "success" => 'js:function(data){location.reload()}' ),array(
                            'class' => 'btn btn-warning btn-block'
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="col-lg-2" style="margin-bottom: 10px;">
                <div class="col-sm-12">
                    <?php if(count($formvals)>0):?>
                    <a href="<?php echo Yii::app()->createUrl('school/studentexport',array('id'=>$id,'student'=>$formvals['student'],'mail'=>$formvals['mail'],'plan'=>$formvals['plan']))?>" class="btn btn-warning btn-block">Export</a>
                    <?php else:?>
                    <a href="<?php echo Yii::app()->createUrl('school/studentexport',array('id'=>$id))?>" class="btn btn-warning btn-block">Export</a>
                    <?php endif;?>

                </div>
            </div>
            <div class="col-lg-2" style="margin-bottom: 10px;">
                <div class="col-sm-12">
                    <a href="<?php echo Yii::app()->createUrl('school/studentexport',array('id'=>$id))?>" class="btn btn-warning btn-block">Export All</a>
                </div>
            </div>
            <div id="students-grid" class="grid-view">
                <table class="table table-bordered table-hover table-striped admintable">
                    <thead>
                    <tr>
                        <th class="checkbox-column" id="sendemail">
                            <input type="checkbox" value="1" name="sendemail_all" id="sendemail_all">
                            <input type="hidden" name="userids" id="userids">
                        </th>
                        <th id="students-grid_c1">Student</th>
                        <th id="students-grid_c2">
                            <a class="sort-link" href="<?php echo Yii::app()->createUrl('school/manageStudents',array('id'=>$id,'student'=>$formvals['student'],'plan'=>$formvals['plan'],'mail'=>$formvals['mail'],'search'=>'search','User_sort'=>'username','order'=>$formvals['order']))?>">Username</a>
                        </th>
                        <th id="students-grid_c3">Password</th>
                        <th id="students-grid_c4">
                            <a class="sort-link" href="<?php echo Yii::app()->createUrl('school/manageStudents',array('id'=>$id,'student'=>$formvals['student'],'plan'=>$formvals['plan'],'mail'=>$formvals['mail'],'search'=>'search','User_sort'=>'createtime','order'=>$formvals['order']))?>">Registration date</a>
                        </th>
                        <th id="students-grid_c5">
                            <a class="sort-link" href="<?php echo Yii::app()->createUrl('school/manageStudents',array('id'=>$id,'student'=>$formvals['student'],'plan'=>$formvals['plan'],'mail'=>$formvals['mail'],'search'=>'search','User_sort'=>'status','order'=>$formvals['order']))?>">Status</a>
                        </th>
                        <th id="students-grid_c6">Email  Status</th>
                        <th id="students-grid_c7">Standard</th>
                        <th class="button-column" id="students-grid_c8">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($dataProvider AS $key=>$data):
                        $student=$data['TM_STU_First_Name'].' '.$data['TM_STU_Last_Name'];
                        if($data['status']==1):
                            $status="Active";
                        else:
                            $status="Inactive";
                        endif;
                        if($data['TM_STU_Email_Status']==0):
                            $emailstatus="Not Send";
                        else:
                            $emailstatus="Send";
                        endif;
                        echo '<tr class="odd">
                                      <td class="checkbox-column">
                                      <input value="'.$data['TM_STU_User_Id'].'" class="sendemail" id="sendemail_'.$key.'" type="checkbox" name="sendemail[]">
                                      </td>
                                      <td>'.$student.'</td>
                                      <td>'.$data['username'].'</td>
                                      <td><a href="javascript:void(0);" class="showtooltip showpassword" id="showpassword'.$data['TM_STU_User_Id'].'" data-student="'.$data['TM_STU_Id'].'" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Show/Hide password">
                                      <span id="maskpassword'.$data['TM_STU_Id'].'" style="display: inline;">xxxxxxxx</span>
                                      <span class="unmaskpassword" id="unmaskpassword'.$data['TM_STU_Id'].'" style="display: none;">'.$data['TM_STU_Password'].'</span></a></td>
                                      <td>'.date("d-m-Y",$data['createtime']).'</td>
                                      <td>'.$status.'</td>
                                      <td>'.$emailstatus.'</td>
                                      <td>'.$data['TM_SD_Name'].'</td>
                                      <td class="button-column">
                                      <a title="View" href="'.Yii::app()->createUrl('school/Viewstudent',array('id'=>$id,'studnet'=>$data['TM_STU_User_Id'])).'"><span class="glyphicon glyphicon-eye-open"></span></a>
                                      <a title="Update" href="'.Yii::app()->createUrl('school/Editstudent',array('id'=>$id,'studnet'=>$data['TM_STU_User_Id'])).'"><span class="glyphicon glyphicon-pencil"></span></a>
                                      <a title="Delete" class="delete" href="'.Yii::app()->createUrl('school/Deletestudent',array('id'=>$id,'studnet'=>$data['TM_STU_User_Id'])).'">
                                      <span class="glyphicon glyphicon-trash"></span></a></td>
                                      </tr>';
                    endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif;?>
</div>
<div class="modal fade " id="invitModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="managesubscription">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    $(function(){
        $('.subscription').click(function(){
            var url=$(this).attr('href');
            $.ajax({
                type: 'POST',
                url: url,
                data: {url: url}
            })
                .done(function (data) {
                    $('#managesubscription').html(data);
                });
            $('#invitModel').modal('toggle');
            return false;
        })
    })
    $(document).on('click','.activate',function(){
        var url= $(this).attr('href');
    })

    $(document).on('click','.deactivate',function(){
        var url= $(this).attr('href');
    })
    $('#clear').click(function(){
        $('#student ').val('');
        $('#parent ').val('');
        $('#school ').val('');
        $('#plan ').val('0');
        return true;

    });
    $(document).on('click','.deleteplan',function(){
        if(confirm('Are you sure you want to delte this item?'))
        {
            return true
        }
        else
        {
            return false;
        }
    })
</script>
    <script type="text/javascript">
        $(function(){
            $('.showtooltip').tooltip();
            $('.showpassword').click(function(){
                var student=$(this).attr('data-student');
                $('#maskpassword'+student).toggle();
                $('#unmaskpassword'+student).toggle();
            });
            $('#sendemail_all').click(function(){
                if ($('#sendemail_all').is(":checked"))
                {
                    $('.sendemail').prop('checked', true);
                    var values = [];
                    $('.sendemail').each(function() {
                        values.push($(this).val());
                    });
                    $('[name="userids"]').attr({value: values.join(', ')});
                }
                else {
                    $('.sendemail').prop('checked', false);
                    $('[name="userids"]').attr({value: ''});
                }

            });
        });
        $(document).on('click','input',function(){
            var values = [];
            $('input:checked').each(function() {
                if ($(this).val()!=1)
                {
                    values.push($(this).val());
                }
            });
            $('[name="userids"]').attr({value: values.join(', ')});
        });
    </script>
