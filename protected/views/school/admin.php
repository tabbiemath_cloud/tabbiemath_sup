<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs = array(
    'Schools' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Manage Schools', 'class' => 'nav-header'),
    array('label' => 'List Schools', 'url' => array('admin')),
    array('label' => 'Create School', 'url' => array('create'),'visible'=>UserModule::isSuperAdmin()),
);

?>
<h3>Manage Schools</h3>
<div class="row brd1">
    <div class="col-lg-12">

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'school-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'columns' => array(
//to make a link by vigil
                array(
                    'name' => 'TM_SCL_Name',
                    'value' => 'CHtml::link($data->TM_SCL_Name, Yii::app()
 ->createUrl("school/view",array("id"=>$data->TM_SCL_Id)))',
                    'type' => 'raw',
                ),
                'TM_SCL_Address',
                'TM_SCL_Contactperson',
                'TM_SCL_Email',
                'TM_SCL_Phone',
                /*
                'TM_SCL_Sylllabus_Id',
                'TM_SCL_Email',
                'TM_SCL_Phone',
                'TM_SCL_Mobile',
                'TM_SCL_City',
                'TM_SCL_State',
                'TM_SCL_Country',
                'TM_SCL_Logo',
                'TM_SCL_Headteacher',
                'TM_SCL_CreatedOn',
                'TM_SCL_CreatedBy',
                */
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{subscription}{update}{delete}{view}',
                    'deleteButtonImageUrl' => false,
                    'updateButtonImageUrl' => false,
                    'viewButtonImageUrl' => false,
                    'buttons' => array
                    (
                        'subscription' => array(
                            'label' => '<span class="glyphicon glyphicon glyphicon-log-in "></span>',
                            'options' => array(
                                'title' => 'Manage Subscription',
                                'class' => 'subscription',
                            ),
                            'url' => 'Yii::app()->createUrl("school/managesubscription",array("id"=>$data->TM_SCL_Id))',
                        ),

                        'update' => array(
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'visible'=>'UserModule::isSuperAdmin()',                            
                            'options' => array(
                                'title' => 'Update'
                            )),
                        'delete' => array(
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'visible'=>'UserModule::isSuperAdmin()',
                            'options' => array(

                                'title' => 'Delete',
                            ),

                        ),//changed the eye to a buoon
                        'view' => array(
                            'label' => '<span class="glyphicon glyphicon-eye-open"></span>',                            
                            'options' => array(
                                'title' => 'View'
                            ))
                    ),
                ),
            ), 'itemsCssClass' => "table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
<div class="modal fade " id="invitModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="managesubscription">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>

     $(document).ready(function(){
        var school='<?php echo $_GET['school']?>';
        //alert(school)
        if (school!='')
        {
            var url='<?php echo Yii::app()->request->baseUrl."/school/managesubscription/";?>'+school+'.html';
            //console.log(url);
            $.ajax({
                    type: 'POST',
                    url: url,
                    data: {url: url}
                })
                .done(function (data) {
                    $('#managesubscription').html(data);
                });
            $('#invitModel').modal('toggle');
            return false;
        }

    }); 
     
    $(function () {
		
    })
		$(document).on('click', '.subscription', function () {        
            var url = $(this).attr('href');
            $.ajax({
                    type: 'POST',
                    url: url,
                    data: {url: url}
                })
                .done(function (data) {
                    $('#managesubscription').html(data);
                });
            $('#invitModel').modal('toggle');
            return false;
        });	
    $(document).on('click', '.activate', function () {
        var url = $(this).attr('href');
    })

    $(document).on('click', '.deactivate', function () {
        var url = $(this).attr('href');
    })
    $('#clear').click(function () {
        $('#student ').val('');
        $('#parent ').val('');
        $('#school ').val('');
        $('#plan ').val('0');
        return true;

    });
    $(document).on('click', '.deleteplan', function () {
        if (confirm('Are you sure you want to delte this item?')) {
            return true
        }
        else {
            return false;
        }
    })

    $(document).on('click', '.next a', function () {
        var url = $(this).attr('href');
             $.ajax({
                        type: 'POST',
                        url: url,
                        data: {url: url}
                    })
                    .done(function (data) {
                        $('#managesubscription').html(data);
                    });
       
                    return false;
     });

    $(document).on('click', '.page a', function () {
        var url = $(this).attr('href');
             $.ajax({
                        type: 'POST',
                        url: url,
                        data: {url: url}
                    })
                    .done(function (data) {
                        $('#managesubscription').html(data);
                    });
       
                    return false;
     });
    $(document).on('click', '.previous a', function () {
        var url = $(this).attr('href');
             $.ajax({
                        type: 'POST',
                        url: url,
                        data: {url: url}
                    })
                    .done(function (data) {
                        $('#managesubscription').html(data);
                    });
       
                    return false;
     });



</script>