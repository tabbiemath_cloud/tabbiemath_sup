<?php
/* @var $this SchoolController */
/* @var $model School */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Id'); ?>
		<?php echo $form->textField($model,'TM_SCL_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Sylllabus_Id'); ?>
		<?php echo $form->textField($model,'TM_SCL_Sylllabus_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Name'); ?>
		<?php echo $form->textField($model,'TM_SCL_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Address'); ?>
		<?php echo $form->textArea($model,'TM_SCL_Address',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Contactperson'); ?>
		<?php echo $form->textField($model,'TM_SCL_Contactperson',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Contactdesignation'); ?>
		<?php echo $form->textField($model,'TM_SCL_Contactdesignation',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Email'); ?>
		<?php echo $form->textField($model,'TM_SCL_Email',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Phone'); ?>
		<?php echo $form->textField($model,'TM_SCL_Phone',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Mobile'); ?>
		<?php echo $form->textField($model,'TM_SCL_Mobile',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_City'); ?>
		<?php echo $form->textField($model,'TM_SCL_City',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_State'); ?>
		<?php echo $form->textField($model,'TM_SCL_State',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Country_Id'); ?>
		<?php echo $form->textField($model,'TM_SCL_Country_Id',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Logo'); ?>
		<?php echo $form->textField($model,'TM_SCL_Logo',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_Headteacher'); ?>
		<?php echo $form->textField($model,'TM_SCL_Headteacher',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_SCL_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_SCL_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_SCL_CreatedBy'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->