<?php
$this->breadcrumbs=array(
	'Schools'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Admin', 'class'=>'nav-header'),	
);
  
?>
<?php /*echo $this->renderPartial('_menu', array(
    'list'=> array(
        CHtml::link(UserModule::t('Create User'),array('create')),
    ),
));
*/?>
    <h3><?php echo UserModule::t("Manage School Subscriptions"); ?></h3>
<div class="row brd1">
    <div class="col-lg-12"> 
<?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,
            'columns'=>array(                
                array(
                    'name'=>'Plan',
                    'type'=>'raw',
                    'value'=>'Plan::model()->findByPk($data->TM_SPN_PlanId)->TM_PN_Name',
                ), 
                array(
                    'name' => 'TM_SPN_Rate',                    
                    'value'=>'$data->TM_SPN_Rate',                    
                ),
                array(
                    'name' => 'TM_SPN_Currency',                    
                    'value'=>'Currency::model()->findByPk($data->TM_SPN_Currency)->TM_CR_Name',                    
                ),                                 
                array(
                    'name' => 'TM_SPN_No_of_Students',                    
                    'value'=>'$data->TM_SPN_No_of_Students',                    
                ),                                                                          
                array(
                    'name' => 'Subscribed On',
                    'value' => 'date("d-m-Y",strtotime($data->TM_SPN_CreatedOn))',
                ),                                                                         
                array(
                    'name' => 'TM_SPN_StartDate',
                    'value' => 'date("d-m-Y",strtotime($data->TM_SPN_StartDate))',
                ),                                                           
                array(
                    'name' => 'expires On',                    
                    'value'=>'$data->TM_SPN_ExpieryDate',                    
                ),                                                             
                array(
                    'name' => 'TM_SPN_Type',                    
                    'value'=>'SchoolPlan::itemAlias("PaymentType",$data->TM_SPN_Type)',                    
                ),                 
                array(
                    'name'=>'TM_SPN_Status',
                    'value'=>'SchoolPlan::itemAlias("SubsciptionStatus",$data->TM_SPN_Status)',
                ),                                       
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{activate}{inactivate}',//
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (                        

                        'activate'=>array(
                            'label'=>'<span class="glyphicon glyphicon-ok"></span>',
                            'visible'=>'$data->TM_SPN_Status>0',
                            'options'=>array(
                                'title'=>'Activate Subscription',
                                'class'=>'activate'
                            ),
                            'url'=>'Yii::app()->createUrl("school/activate",array("id"=>$data->TM_SPN_Id))',
                        ),
                        'inactivate'=>array(
                            'label'=>'<span class="glyphicon glyphicon-remove"></span>',
                            'visible'=>'$data->TM_SPN_Status<1',
                            'options'=>array(
                                'title'=>'Deactivate Subscription',
                                'class'=>'deactivate'
                            ),
                            'url'=>'Yii::app()->createUrl("school/deactivate",array("id"=>$data->TM_SPN_Id))',
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
        )); ?>          
        
    </div>
</div>
