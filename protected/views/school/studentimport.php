<?php
$this->breadcrumbs=array(
    'Schools'=>array('admin'),
    'Manage',
);

$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List School', 'url'=>array('School/admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('School/view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'List Students', 'url'=>array('manageStudents', 'id'=>$id)),
); 
?>
<h3>Import CSV Students</h3>
<div class="row brd1">
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body form-horizontal payment-form">
            <div class="form-group">
                <label class="col-sm-3 control-label">Select CSV <span class="required">*</span></label>
                <div class="col-sm-4">
                    <input type="file" name="importcsv" class="form-control" id="importcsv">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 pull-left"> </div>
                <div class="col-sm-3 pull-left">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                </div>
                <div class="col-sm-3 pull-left">
                    <?php echo CHtml::submitButton('Upload',array('class'=>'btn btn-warning btn-lg btn-block','name'=>'submit')); ?>
                </div>
            </div>
        </div>
    </form>
</div>
<!--code by amal!-->
<?php
if(count($importlog)>0):
    ?>
<div class="panel panel-yellow addedqstable">
    <div class="panel-heading">
        <h3 class="panel-title">Import Status</h3>
    </div>
    <div class="scrol-body">
        <table class="table table-striped table-bordered ">
            <thead>
            <th>Row #</th>
            <th>Import Status</th>
            <th>Notes</th>
            </thead>
            <tbody>
            <?php for($i=1;$i<=count($importlog);$i++):?>
            <tr>
            <td><?php echo $i;?></td>
            <td>
            <?php
            if($importlog[$i]['student']=='0')
                echo "Student Import Failed";
            elseif($importlog[$i]['student']=='2')
                echo "Student Import Failed (Username already exists)";
            else
                echo "Student Import Successful";

            //  echo ($importlog[$i]['student']=='0'?'Student Import Failed': 'Student Import Successful');
            ?>
            </td>
            <td><?php echo $importlog[$i]['status'];?>
            </td>
            </tr>
                <?php endfor;?>
            </tbody>
        </table>
    </div>
</div>
<?php endif;?>
<!--ends!-->