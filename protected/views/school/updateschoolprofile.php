<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs=array(
	'Schools'=>array('index'),
	$model->TM_SCL_Id=>array('view','id'=>$model->TM_SCL_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage School', 'class'=>'nav-header'),
	array('label'=>'School Profile', 'url'=>array('schoolprofile', 'id'=>$model->TM_SCL_Id)),	
);
?>

<h3>Update School Profile</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>