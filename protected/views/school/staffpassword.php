<?php
$this->breadcrumbs=array(
    UserModule::t('Admins')=>array('Stafflist'),
    UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Admins', 'class'=>'nav-header'),
    array('label'=>'List Admins', 'url'=>array('manageStaff', 'id'=>$id)),
    array('label'=>'View Admin', 'url'=>array('Viewstaff','id'=>$id,'master'=>$profile->user_id)),
    array('label'=>'Add Admin', 'url'=>array('AddStaff', 'id'=>$id)),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
Yii::app()->clientScript->registerScript('back', "
    $('.backbtn').click(function(){
        window.location = '".Yii::app()->createUrl('school/Viewstaff',array('id'=>$id,'master'=>$profile->user_id))."';
    });
");
?>
<?php if(Yii::app()->user->isSchoolAdmin()):?>
<div class="col-sm-2 pull-right">
    <input class="btn btn-warning btn-lg btn-block backbtn" type="button" name="yt0" value="Back">
</div>
<?php endif;?>
<h3>Change Password of <?php echo $profile->firstname.' '.$profile->lastname; ?></h3>
<div class="row brd1">

    <?php $form=$this->beginWidget('UActiveForm', array(
    'id'=>'changepassword-form',
    'enableAjaxValidation'=>true,
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <?php if(Yii::app()->user->hasFlash('passwordchange')): ?>
        <div class="alert alert-success" role="alert">
            <?php echo Yii::app()->user->getFlash('passwordchange'); ?>
        </div>
        <?php endif; ?>
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'password',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'verifyPassword',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->passwordField($model,'verifyPassword',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'verifyPassword'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="raw">
                <div class="col-sm-3 pull-right">
                    <?php echo CHtml::submitButton(UserModule::t("Save"),array('class'=>'btn btn-warning btn-lg btn-block')); ?>

                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>