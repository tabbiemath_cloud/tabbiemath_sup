<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
    UserModule::t("Registration"),
);
?>
<style>
    .clry{
        background-color: #FFF !important;
        color: #999 !important;
        border: 1px solid #BDBAB7 !important;
    }
    .totalbord{border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;}

    ::-webkit-input-placeholder {
        color: #fff !important;
    }
    :-moz-placeholder { /* older Firefox*/

        color: #fff !important;

    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: #fff !important;
    }
    :-ms-input-placeholder {
        color: #fff !important;
    }

    .bgrg_form{    padding: 15px;
        margin: 0 auto;
        background-color: rgba(71, 71, 71, 0.78);
        border-radius: 8px;
        border: 1px solid rgba(130, 130, 130, 0.18);
        color: white;}


    .reg_bdy{background-color: rgba(71, 71, 71, 0.78);
        border-radius: 8px;
        color: white !important;    padding-right: 20px;
        padding-left: 20px;}

    .colorgraph {
        height: 1px;
        border-top: 0;
        background: #c4e17f;
        border-radius: 5px;
        background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
    }

    .errorMessage2 {
        padding: 5px;
        color: #F0776C;
    }
    .table td
    {
        border-top: 0px !important;
        border-bottom: 1px solid rgba(255, 251, 251, 0.14);
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6 reg_bdy col-md-offset-3">
            <h2 class="text-center">Select Your Class</h2>
            <hr class="colorgraph">
            <?php echo CHtml::beginForm(); ?>
            <div class="col-md-12">
                <div class="panel-body">
                    <fieldset>
                        <div class="form-group">
                            <?php

                            $result = $this->getplan($parent['student_school']);
                            //print_r($result);exit;
                            $listdata = CHtml::listData($result, 'TM_PN_Id', 'TM_SD_Name');

                            $selected = array();

                            $selected[] = $planprev['TM_SPN_PlanId'];


                            echo CHtml::dropDownList('plan', $selected, $listdata, array('class' => 'form-control clry validate', 'empty' => 'Select Standard','data-item'=>'Standard',
                                'ajax' => array(
                                    'type'=>'POST', //request type
                                    'url'=>CController::createUrl('school/getgroups/id/'.$parent['student_school'].''), //url to call.
                                    //Style: CController::createUrl('currentController/methodToCall')
                                    'update'=>'#groups', //selector to update
                                    //'data'=>'js:javascript statement'
                                    //leave out the data key to pass all form values through
                                ))); ?>
                            <div id="Student_Standard" class="errorMessage" style="display: none">Select Standard</div>
                        </div>
                        <div class="form-group">
                            <?php echo CHtml::dropDownList('groups','groups',array('none'=>'Select Group'),array('class'=>'form-control clry validate')); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="row" style="margin-top: 15px;text-align: right;">
                    <?php echo CHtml::submitButton(UserModule::t("Continue"),array('class'=>'btn btn-warning','style'=>'margin-bottom: 10px;','id'=>'complete')); ?>
                </div>
            </div>
            <?php echo CHtml::endForm(); ?>
        </div>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('complete', "
    $('#complete').click(function(){
        var error=0;
        if($('#plan').val()=='')
        {
            $('#Student_Standard').show();
            setTimeout(function(){
                $('#Student_Standard').hide();
            },3000);
            error=1;
        }
        if(error==1)
        {
            return false;
        }
        else
        {
            return true;
        }
    });
");

?>