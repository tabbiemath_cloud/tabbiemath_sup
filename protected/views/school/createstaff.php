<?php
$this->breadcrumbs=array(
	'Schools'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Staff', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin'), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'View School', 'url'=>array('view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),    
	array('label'=>'List Admins', 'url'=>array('manageStaff', 'id'=>$id)),	
);
?>
<h1><?php echo UserModule::t("Create Admin"); ?></h1>

<?php 
	/*echo $this->renderPartial('_menu',array(
		'list'=> array(),
	));*/
	echo $this->renderPartial('_staffform', array('id'=>$id,'model'=>$model,'profile'=>$profile,'staffschool'=>$staffschool));
?>
