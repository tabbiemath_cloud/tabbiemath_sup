<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
    UserModule::t("Registration"),
);
?>
<style>
    <?php
    if($student->TM_STU_School=='0'):?>
    .otherschool
    {
        display:block;
    }
    <?php else:?>
    .otherschool
    {
        display:none;
    }
    <?php endif;?>
    .clry{
        background-color: #FFF !important;
        color: #999 !important;
        border: 1px solid #BDBAB7 !important;
    }

    ::-webkit-input-placeholder {
        color: #fff !important;
    }
    :-moz-placeholder { /* older Firefox*/

        color: #fff !important;

    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: #fff !important;
    }
    :-ms-input-placeholder {
        color: #fff !important;
    }

    .bgrg_form{    padding: 15px;
        margin: 0 auto;
        background-color: rgba(71, 71, 71, 0.78);
        border-radius: 8px;
        border: 1px solid rgba(130, 130, 130, 0.18);
        color: white;}


    .reg_bdy{background-color: rgba(71, 71, 71, 0.78);
        border-radius: 8px;
        color: white !important;    padding-right: 20px;
        padding-left: 20px;}

    .colorgraph {
        height: 1px;
        border-top: 0;
        background: #c4e17f;
        border-radius: 5px;
        background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
    }

    .errorMessage2 {
        padding: 5px;
        color: #F0776C;
    }
</style>

<?php if(Yii::app()->user->hasFlash('registration')): ?>
    <div class="container">
        <?php if(Yii::app()->user->hasFlash('subscriptionsuccess')): ?>
            <div class="alert alert-success" role="alert" style="margin-top: 52px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
                <?php echo Yii::app()->user->getFlash('subscriptionsuccess'); ?></div>
        <?php endif;?>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 login-panel panel panel-default">
                <div class="panel-body">

                    <img class="loghed1" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" style="display: block !important;margin: auto !important;">

                    <h3>Welcome !</h3>
                    <p>We are excited to welcome you as the newest member of our growing community. <br />
                        <b>Please check your email for instructions to activate your account.</b><br />
                        If you cannot find the email, please check in your junk folder too, just in case we have ended up there. <br />
                        So, what are you waiting for... Lets get started ! </p>
                    <!--<a href="<?php //echo $this->createAbsoluteUrl('/user/login');?>">Click Here To Login</a>-->
                </div>
            </div>
        </div>
    </div>
<?php else:?>
<div class="container">
    <?php if(Yii::app()->user->hasFlash('subscriptionfail')): ?>
        <div class="alert alert-danger" role="alert" style="margin-top: 52px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
            <?php echo Yii::app()->user->getFlash('subscriptionfail'); ?></div>
    <?php endif;?>
    <div class="row">

        <div class="col-md-8 reg_bdy col-md-offset-2">

            <h2 class="text-center">Reinforce your learning</h2>
            <hr class="colorgraph">


            <?php $form=$this->beginWidget('UActiveForm', array(
                'id'=>'registration-form',
                'enableAjaxValidation'=>true,
                'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
                'htmlOptions' => array('enctype'=>'multipart/form-data'),
            )); ?>
            <div class="col-md-6">

                <!--<div class="panel-heading">
                            <h3 class="panel-title"><?php echo UserModule::t("Registration"); ?></h3>
                        </div>-->
                <div class="panel-body" style="padding-top: 0px;">
                    <?php //echo CHtml::errorSummary($model); ?>

                    <fieldset>
                        <h3 style="font-weight: normal;margin-top: 0px;">Parent Details</h3>
                        <hr>

                        <?php
                        $profileFields=$profile->getFields();
                        if ($profileFields) {
                            foreach($profileFields as $field) {
                                ?>
                                <div class="form-group">
                                    <!--                                --><?php //echo $form->labelEx($profile,$field->varname); ?>
                                    <?php
                                    if ($field->widgetEdit($profile)) {
                                        echo $field->widgetEdit($profile);
                                    } elseif ($field->range) {
                                        echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                                    } elseif ($field->field_type=="TEXT") {
                                        echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                                    } else {
                                        echo $form->textField($profile,$field->varname,array('class'=>'form-control clry','placeholder'=>$field->title,'size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                    }
                                    ?>
                                    <?php echo $form->error($profile,$field->varname); ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <div class="form-group">
                            <?php echo $form->textField($model,'email',array('class'=>'form-control clry existvalidation','placeholder'=>'E-mail')); ?>
                            <?php echo $form->error($model,'email'); ?>
                            <?php echo $form->hiddenField($model, 'password',array('value'=>'test')); ?>
                            <?php echo $form->hiddenField($model, 'verifyPassword',array('value'=>'test')); ?>
                        </div>
                        <?php if (UserModule::doCaptcha('registration')): ?>
                        <div class="form-group">
                            <?php $this->widget('CCaptcha'); ?>
                            <p class="hint"><?php echo UserModule::t("Please enter the letters as they are shown in the image above. Letters are not case-sensitive."); ?></p>
                        </div>
                        <div class="form-group">
                            <?php echo $form->textField($model,'verifyCode',array('class'=>'form-control clry','placeholder'=>'Verification Code')); ?>
                            <?php echo $form->error($model,'verifyCode'); ?>
                        </div>
                        <?php endif; ?>
                        <!--<div class="form-group">
                                    <?php //echo $form->textField($model,'username',array('class'=>'form-control clry','placeholder'=>'Username')); ?>
                                    <?php //echo $form->error($model,'username'); ?>
                                </div>-->
                        <!--<div class="form-group">
                            <?php /*echo $form->PasswordField($model,'password',array('class'=>'form-control clry','placeholder'=>'Password')); */?>
                            <?php /*echo $form->error($model,'password'); */?>
                        </div>-->
                        <!--<div class="form-group">
                            <?php /*echo $form->PasswordField($model,'verifyPassword',array('class'=>'form-control clry','placeholder'=>'Verify Password')); */?>
                            <?php /*echo $form->error($model,'verifyPassword'); */?>
                        </div>-->
                    </fieldset>
                </div>

            </div>
            <div class="col-md-6">
                <div class="panel-body" style="padding-top: 0px;">
                    <fieldset>
                        <h3 style="font-weight: normal;margin-top: 0px;">Student Details</h3>
                        <hr>
                        <div class="form-group">
                            <?php echo $form->textField($student,'TM_STU_First_Name',array('class'=>'form-control clry','placeholder'=>'First Name')); ?>
                            <?php echo $form->error($student,'TM_STU_First_Name'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->textField($student,'TM_STU_Last_Name',array('class'=>'form-control clry','placeholder'=>'Last Name')); ?>
                            <?php echo $form->error($student,'TM_STU_Last_Name'); ?>
                        </div>
                        <div class="form-group">
                           <?php echo $form->textField($student,'TM_STU_student_Id',array('class'=>'form-control clry existvalidation','placeholder'=>'Enter Preferred Username')); ?>
                           <?php echo $form->error($student,'TM_STU_student_Id'); ?>
                          <!-- <p class="hint">Leaving this blank will generate a username</p> -->
                       </div>
                       <div class="form-group">
                           <?php echo $form->PasswordField($student,'TM_STU_Password',array('class'=>'form-control clry','placeholder'=>'Enter Preferred Password')); ?>
                           <?php echo $form->error($student,'TM_STU_Password'); ?>
                       </div>
                        <div class="form-group">
                            <?php echo $form->dropDownList($student,'TM_STU_Country_Id',CHtml::listData(Country::model()->findAll(array('condition' => 'TM_CON_Status=0','order' => 'TM_CON_Id')),'TM_CON_Id','TM_CON_Name'),array('empty'=>'Select Country','class'=>'form-control clry',
                                'ajax' => array(
                                    'type'=>'POST', //request type
                                    'url'=>CController::createUrl('getschools'), //url to call.
                                    //Style: CController::createUrl('currentController/methodToCall')
                                    'update'=>'#Student_TM_STU_School', //selector to update
                                    //'data'=>'js:javascript statement'
                                    //leave out the data key to pass all form values through
                                ))); ?>
                            <?php echo $form->error($student,'TM_STU_Country_Id'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->dropDownList($student,'TM_STU_School',array('none'=>'Select School'),array('class'=>'form-control clry')); ?>
                            <?php echo $form->error($student,'TM_STU_School'); ?>
                        </div>
                        <div class="form-group otherschool">
                            <?php echo $form->textField($student,'TM_STU_SchoolCustomname',array('class'=>'form-control clry otherschool','placeholder'=>'School Name')); ?>
                            <?php echo $form->error($student,'TM_STU_SchoolCustomname'); ?>
                        </div>
                        <div class="form-group otherschool">
                            <?php echo $form->textField($student,'TM_STU_City',array('class'=>'form-control clry otherschool','placeholder'=>'City')); ?>
                            <?php echo $form->error($student,'TM_STU_City'); ?>
                        </div>

                        <div class="form-group otherschool">
                            <?php echo $form->textField($student,'TM_STU_State',array('class'=>'form-control clry otherschool','placeholder'=>'State')); ?>
                            <?php echo $form->error($student,'TM_STU_State'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->textField($student,'TM_STU_School_code',array('class'=>'form-control clry','placeholder'=>'School Code')); ?>
                            <?php echo $form->error($student,'TM_STU_School_code'); ?>
                        </div>
                    </fieldset>
                </div>
            </div>




            <div class="col-md-6"> <div class="panel-body" style="padding-top: 0px;">
                    <?php echo $form->checkBox($model,'terms'); ?> I accept the <a href="http://www.tabbiemath.com/terms-conditions/" target="_blank">terms and conditions</a>
                    <?php echo $form->error($model,'terms'); ?>
                </div></div>
            <div class="col-md-12"> <div class="panel-body"  style="padding-top: 0px;"><?php
                    if($model->terms):
                        $disabled=false;
                    else:
                        $disabled=true;
                    endif;
                    echo CHtml::submitButton(UserModule::t("Continue "),array('class'=>'btn btn-warning  pull-right','disabled'=>$disabled,'id'=>'registration')); ?></div></div>
            <?php $this->endWidget(); ?>
        </div></div>

    <?php endif;?>
    <?php
    if($student->TM_STU_Country_Id!=''):
        Yii::app()->clientScript->registerScript('loadschools', "
            $.ajax({
              method: 'POST',
              url: '".Yii::app()->createUrl('user/registration/getschools')."',
              data: { country:".$student->TM_STU_Country_Id.",school:'".$student->TM_STU_School."'}
            })
              .done(function( data ) {
                $('#Student_TM_STU_School').html(data);
              });
");
    endif;
    Yii::app()->clientScript->registerScript('terms', "
    $('#RegistrationForm_terms').click(function(){
        if($(this).is(':checked'))
        {
            if(!$('.errorMessage').is(':visible')) {
                $('#registration').prop('disabled', false);
            }
        }
        else
        {
            $('#registration').prop('disabled', true);
        }
    });
    $('#Student_TM_STU_School').change(function(){
        if($(this).val()=='0')
        {
            $('.otherschool').show();
        }
        else
        {
            $('.otherschool').hide();
        }
    });
");
    ?>
<script>
jQuery(function($) {
    $(".existvalidation").focusout(function(){
        // alert('success');
        var parentEmail = $('#RegistrationForm_email').val();
        var studentUserName = $('#Student_TM_STU_student_Id').val();
        if(studentUserName != '') {
            $('#registration').attr('disabled','disabled');
            $.ajax({
                method: 'POST',
                url: '<?= Yii::app()->createUrl('school/isexist') ?>',
                dataType:'json',
                data: { parentEmail:parentEmail,studentUserName:studentUserName},
                success(data){
                    if(data.error == 'no') {
                        // alert('TRUE');
                        $('#Student_TM_STU_student_Id_em_').hide();
                        if($('#RegistrationForm_terms').is(':checked'))
                            $('#registration').removeAttr('disabled');
                        return true;
                    }
                    else
                        // alert('FALSE');
                        $('#Student_TM_STU_student_Id_em_').show();
                        $('#Student_TM_STU_student_Id_em_').html('Username already exists');
                        
                        
                }
            });
        }        
    });
});
</script>
