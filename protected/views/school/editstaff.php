<?php
$this->breadcrumbs=array(
	'Schools'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Admins', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin'), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'View School', 'url'=>array('view','id'=>$school), 'visible'=>UserModule::isAllowedSchool()),    
    array('label'=>'List Admins', 'url'=>array('manageStaff', 'id'=>$school)),
    array('label'=>'Change Password', 'url'=>array('staffpassword','id'=>$school,'staff'=>$model->id)),    
	array('label'=>'Add Admin', 'url'=>array('AddStaff', 'id'=>$school)),
	array('label'=>'View Admin', 'url'=>array('Viewstaff', 'id'=>$school,'master'=>$model->id)),  	 
    array('label'=>'Delete Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$school,'master'=>$model->id),'confirm'=>'Are you sure you want to delete this Staff?')),         	
);
?><h3>Update Admin <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>


<?php 	echo $this->renderPartial('_staffform', array('model'=>$model,'profile'=>$profile,'staffschool'=>$schoolstaff)); ?>