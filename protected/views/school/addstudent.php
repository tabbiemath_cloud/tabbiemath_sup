
<?php

$this->menu = array(
    array('label' => 'Manage Students', 'class' => 'nav-header'),
    array('label'=>'List School', 'url'=>array('admin'), 'visible'=>UserModule::isAllowedSchool()),
    array('label'=>'View School', 'url'=>array('view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),    
    array('label' => 'List Students', 'url' => array('manageStudents','id'=>$id)), 
    array('label'=>'Change Password', 'url'=>array('studentpassword','id'=>$student->TM_STU_School_Id,'stud'=>$student->TM_STU_User_Id),'visible'=>!$student->isNewRecord),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('school/manageStudents',array('id'=>$id))."';
    });
");
 
?>
    <?php if($student->isNewRecord):?>
        <h3><?php echo  UserModule::t('Add Student'); ?></h3>
    <?php else:?>
        <h3><?php echo  UserModule::t('Edit Student'); ?></h3>
    <?php endif;?>
<div class="row brd1">
        <?php $form = $this->beginWidget('UActiveForm', array(
        'id' => 'registration-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong>
		<?php if($student->hasErrors()){
  echo CHtml::errorSummary($student);
} ?>
		</div>
            <?php

                $profileFields=$parentprofile->getFields();

                if ($profileFields) {
                    foreach($profileFields as $field) {
                        ?>
                        <div class="form-group" style="min-height:70px;">
                            <?php echo $form->labelEx($parentprofile,'Parent '.$field->varname, array('class' => 'col-sm-3 control-label')); ?>
                            <div class="col-sm-9">
                            <?php
                            if ($field->widgetEdit($parentprofile)) {
                                echo $field->widgetEdit($parentprofile);
                            } elseif ($field->range) {
                                echo $form->dropDownList($parentprofile,$field->varname,Profile::range($field->range));
                            } elseif ($field->field_type=="TEXT") {
                                echo$form->textArea($parentprofile,$field->varname,array('rows'=>6, 'cols'=>50));
                            } else {
                                echo $form->textField($parentprofile,$field->varname,array('class'=>'form-control','placeholder'=>$field->title,'size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                            }
                            ?>
                            <?php echo $form->error($parentprofile,$field->varname); ?>
                            </div>
                        </div>
                    <?php
                    }
                }
                
                ?> 
                <div class="form-group" style="min-height:70px;">
                    <label class="col-sm-3 control-label required" for="User_Parent_email">Parent Email<span class="required">*</span></label>
                    <div class="col-sm-9">
                        <?php echo $form->textField($parentmodel, 'email', array('class' => 'form-control existvalidation', 'placeholder' => 'Email')); ?>
                        <?php echo $form->error($parentmodel, 'email'); ?>
                        <div class="error errorMessage parentmail" style="display:none;">Email already exists</div>
                    </div>
                </div>
                <div class="form-group" style="min-height:70px;">
                    <label class="col-sm-3 control-label required" for="User_Parent_password">Parent Password<span class="required">*</span></label>
                    <div class="col-sm-9">
                        <?php echo $form->passwordField($parentmodel, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                        <?php echo $form->error($parentmodel, 'password'); ?>
                    </div>
                </div>

                <div class="form-group" style="min-height:70px;">
                    <?php echo $form->labelEx($student, 'TM_STU_First_Name', array('class' => 'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->textField($student, 'TM_STU_First_Name', array('class' => 'form-control', 'placeholder' => 'First Name')); ?>
                        <?php echo $form->error($student, 'TM_STU_First_Name'); ?>
                    </div>
                </div>
                <div class="form-group" style="min-height:70px;">
                    <?php echo $form->labelEx($student, 'TM_STU_Last_Name', array('class' => 'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->textField($student, 'TM_STU_Last_Name', array('class' => 'form-control', 'placeholder' => 'Last Name')); ?>
                        <?php echo $form->error($student, 'TM_STU_Last_Name'); ?>
                    </div>
                </div>
                <?php if($student->isNewRecord):?>
                    <div class="form-group" style="min-height:70px;">
                        <?php echo $form->labelEx($student,'TM_STU_student_Id',array('class'=>'col-sm-3 control-label')); ?>
                        <div class="col-sm-9 username_">
                            <?php echo $form->textField($student,'TM_STU_student_Id',array('placeholder' => 'Enter Preferred Username','class'=>'form-control existvalidation')); ?>
                            <?php echo $form->error($student,'TM_STU_student_Id'); ?>
                        </div>
                    </div>
                    <div class="form-group" style="min-height:70px;">
                        <?php echo $form->labelEx($student,'TM_STU_Password',array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-9">
                           <?php echo $form->PasswordField($student,'TM_STU_Password',array('placeholder' => 'Enter Preferred Password','class'=>'form-control'));?>
                           <?php echo $form->error($student,'TM_STU_Password');?>
                         </div>
                    </div>
                <?php else:?>
                    <div class="form-group" style="min-height:70px;">
                        <?php echo $form->labelEx($studentuser,'username',array('class'=>'col-sm-3 control-label')); ?>
                        <div class="col-sm-9">
                            <?php echo $form->textField($studentuser,'username',array('placeholder' => 'Username','class'=>'form-control')); ?>
                            <?php echo $form->hiddenField($student,'TM_STU_student_Id',array('value'=>$studentuser['username'])); ?>
                            <?php echo $form->error($studentuser,'username'); ?>
                        </div>
                    </div>
                <?php endif;?>
                <div class="form-group">
                    <?php echo $form->labelEx($studentuser,'userlevel',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->dropDownList($studentuser,'userlevel',User::itemAlias('UserLevel'),array('class'=>'form-control')); ?>
                        <?php echo $form->error($studentuser,'userlevel'); ?>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px">
                    <label class="col-sm-3 control-label required" for="Student_TM_STU_Email">Select Standard</label>
                    <div class="col-sm-9 pull-right">
                    <?php

                    $result = $this->getplan($id);
                    //print_r($result);exit;
                    $listdata = CHtml::listData($result, 'TM_PN_Id', 'TM_SD_Name');

                        $selected = array();

                            $selected[] = $planprev['TM_SPN_PlanId'];


                    echo CHtml::dropDownList('plan', $selected, $listdata, array('class' => 'form-control', 'empty' => 'Select Standard')); ?>
                </div>

                </div>
                
                <div class="form-group">
                    <?php echo $form->labelEx($studentuser,'status',array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->dropDownList($studentuser,'status',User::itemAlias('UserStatus'),array('class'=>'form-control')); ?>
                        <?php echo $form->error($studentuser,'status'); ?>
                    </div>
                </div>

                <div class="form-group"  >
                    <div class="row">
                    
                        <div class="col-sm-3 pull-right">
                        <?php echo CHtml::submitButton(UserModule::t("Save "), array('class' => 'btn btn-warning btn-lg btn-block','id'=>'saveform')); ?>
                        </div>
                       <div class="col-lg-3 pull-right">
                            <button class="btn btn-warning btn-lg btn-block cancelbtn" type="button" value="Reset">Cancel</button>
                        </div>
                    </div>                
                </div>
            </div>

        <?php $this->endWidget(); ?>
    </div>
<script>
$(function(){
   $('#User_email').change(function(){
    var email=$(this).val();
    <?php if(isset($parentmodel->id)) { ?>
    var parentId = <?= $parentmodel->id ?>;
    <?php } else { ?>
        var parentId = '';
   <?php } ?>
    $.ajax({
        type: "POST",
        url: '<?php echo Yii::app()->createUrl('school/checkparentemail');?>',
        data: {email: email, parentId: parentId}
    })
    .done(function (data) {
            if(data==0)
            {
                $('.parentmail').hide();
                $('#saveform').prop('disabled',false);
            }
            else
            {
                $('.parentmail').show();
                $('#saveform').prop('disabled',true);
            }
        });    
   })

   $(".existvalidation").focusout(function(){
        // alert('success');
        var parentEmail = $('#User_email').val();
        var studentUserName = $('#Student_TM_STU_student_Id').val();
        if(studentUserName != '') {
            $('#saveform').attr('disabled','disabled');
            $.ajax({
                method: 'POST',
                url: '<?= Yii::app()->createUrl('school/isexist') ?>',
                dataType:'json',
                data: { parentEmail:parentEmail,studentUserName:studentUserName},
                success(data){
                    if(data.error == 'no') {
                        // alert('TRUE');
                        $('#Student_TM_STU_student_Id_em_').remove();
                        if(!$('.errorMessage').is(":visible"))
                            $('#saveform').removeAttr('disabled');
                        return true;
                    }
                    else
                        // alert('FALSE');
                        $('#Student_TM_STU_student_Id_em_').show();
                        $('.username_').append('<div class="errorMessage" id="Student_TM_STU_student_Id_em_">Username already exists</div>');
                        
                        
                }
            });
        }        
    });
    
});
</script>


