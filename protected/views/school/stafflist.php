<?php
$this->breadcrumbs=array(
	'Schools'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Admins', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin'), 'visible'=>UserModule::isAllowedSchool()),
	array('label'=>'View School', 'url'=>array('view','id'=>$id), 'visible'=>UserModule::isAllowedSchool()),    
	array('label'=>'Add Admin', 'url'=>array('AddStaff', 'id'=>$id)),	
);
?>
<?php /*echo $this->renderPartial('_menu', array(
    'list'=> array(
        CHtml::link(UserModule::t('Create User'),array('create')),
    ),
));
*/?>
    <h3><?php echo UserModule::t("Support Admin"); ?></h3>
<div class="row brd1">
    <div class="col-lg-12">        
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,        
            'columns'=>array(
                array(
                    'name'=>'Admin',
                    'type'=>'raw',
                    'value'=>'User::StaffName($data->id)',
                ),                
                'username',
/*                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
                ),
                array(
                    'name' => 'username',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->username),array("admin/view","id"=>$data->id))',
                ),*/
                array(
                    'name'=>'Role',
                    'type'=>'raw',
                    'value'=>'User::StaffRole($data->id,$data->school_id)',
                ),
                array(
                    'name'=>'School',
                    'type'=>'raw',
                    'value'=>'User::StaffSchool($data->school_id)',
                ),                                
                array(
                    'name' => 'createtime',
                    'value' => 'date("d-m-Y",$data->createtime)',
                ),
                array(
                    'name'=>'status',
                    'value'=>'User::itemAlias("UserStatus",$data->status)',
                ),                                                               
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            ),
                            'url'=>'Yii::app()->createUrl("school/Viewstaff",array("id"=>$data->school_id,"master"=>$data->id))'),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            ),
                            'url'=>'Yii::app()->createUrl("school/Editstaff",array("id"=>$data->school_id,"master"=>$data->id))'),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                            'visible'=>'$data->id != Yii::app()->user->id',
                            'url'=>'Yii::app()->createUrl("school/deletestaff",array("id"=>$data->school_id,"master"=>$data->id))'),
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
        )); ?>
    </div>
</div>
<div class="modal fade " id="invitModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
            </div>
            <div class="modal-body" id="managesubscription">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(function(){
    $('.subscription').click(function(){
        var url=$(this).attr('href');
        $.ajax({
            type: 'POST',
            url: url,
            data: {url: url}
        })
        .done(function (data) {
            $('#managesubscription').html(data);
        });
        $('#invitModel').modal('toggle');
        return false;
    })
})
$(document).on('click','.activate',function(){
    var url= $(this).attr('href');
})

$(document).on('click','.deactivate',function(){
    var url= $(this).attr('href');
})
$('#clear').click(function(){        
    $('#student ').val('');
    $('#parent ').val('');
    $('#school ').val('');
    $('#plan ').val('0');
    return true;
         
});
$(document).on('click','.deleteplan',function(){
    if(confirm('Are you sure you want to delte this item?'))
    {
        return true
    }
    else
    {
        return false;
    }
})
</script>
