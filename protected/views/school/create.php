<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs=array(
	'Schools'=>array('index'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Schools', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin')),

);
?>

<h3>Create School</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>