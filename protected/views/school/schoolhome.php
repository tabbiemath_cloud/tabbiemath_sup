 <!-- /.row -->
            <div class="row">                         
                 <?php
                  if(UserModule::isSchoolStaffSSupAdmin($masterschool->TM_SCL_Id)):?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                           <a href="<?php echo Yii::app()->createUrl('school/Schoolprofile',array('id'=>$masterschool->TM_SCL_Id));?>">
						   <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa fa-university fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">School Profile</div>
                                  </div>
                            </div>
                        </div>
                     
                        
                        </a>
                    </div>
                </div>
                <?php endif;?>
                <?php if(UserModule::isSchoolStaffSSupAdmin($masterschool->TM_SCL_Id)):?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('school/managestaff',array('id'=>$masterschool->TM_SCL_Id))?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Admins</div>
                                                                 </div>
                            </div>
                        </div>
                       
                         
                        </a>
                    </div>
                </div>
                <?php endif;?>
                <?php if(UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)):?>                
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('Teachers/ManageTeachers',array('id'=>$masterschool->TM_SCL_Id))?>">
                        
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Teachers</div>
                                                                 </div>
                            </div>
                        </div>


                        </a>
                    </div>
                </div>
                <?php endif;?>
                <?php if(UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)):?> 
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('school/manageStudents',array('id'=>$masterschool->TM_SCL_Id))?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Students</div>
                         
                                </div>
                            </div>
                        </div>
                        
                         
                        </a>
                    </div>
                </div>
                <?php endif;?>

                <?php if(UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)):?> 
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('blueprints/admin')?>">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Blueprints</div>
                         
                                </div>
                            </div>
                        </div>
                        
                         
                        </a>
                    </div>
                </div>
                <?php endif;?>

                <?php if(UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)):?>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <a href="<?php echo Yii::app()->createUrl('mock/consolidatedreport')?>">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-th-list fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">Thematic Report</div>
                                        </div>
                                    </div>
                                </div>


                            </a>
                        </div>
                    </div>
                <?php endif;?>
                
                <?php if(UserModule::isSchoolStaffAdmin($masterschool->TM_SCL_Id)):?>                 
                <!--<div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php /*echo Yii::app()->createUrl('#')*/?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa fa-bookmark fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Subscription</div>
                         
                                </div>
                            </div>
                        </div>
                        
                         
                        </a>
                    </div>
                </div>-->
                <?php endif;?> 
</div>                                               
<style type="text/css">
    #wrapper
    {
        padding-left:0px !important;
    }
    #page-wrapper
    {
        padding-top: 40px;
    }
</style>