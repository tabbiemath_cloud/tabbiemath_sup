<?php
/* @var $this SchoolController */
/* @var $model School */

$this->breadcrumbs=array(
	'Schools'=>array('admin'),
	$model->TM_SCL_Id,
);
$this->menu=array(
    array('label'=>'Manage School', 'class'=>'nav-header'),
	array('label'=>'Update School Profile', 'url'=>array('Updateschoolprofile', 'id'=>$model->TM_SCL_Id)),
);
?>

<h3>School Profile</h3>
<div class="row brd1">
    <div class="col-lg-12">
<?php
    $userid=$model->TM_SCL_CreatedBy;
    $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_SCL_Id',
        array(
            'name'=>'TM_SCL_Sylllabus_Id',
            'type'=>'raw',
            'value'=>Syllabus::model()->findByPk($model->TM_SCL_Sylllabus_Id)->TM_SB_Name,
        ),        		
		'TM_SCL_Name',
		'TM_SCL_Address',
		'TM_SCL_Contactperson',
		'TM_SCL_Contactdesignation',
		'TM_SCL_Email',
		'TM_SCL_Phone',
		'TM_SCL_Mobile',
		'TM_SCL_City',
		'TM_SCL_State',
        array(
            'name'=>'TM_SCL_Country_Id',
            'type'=>'raw',
            'value'=>Country::model()->findByPk($model->TM_SCL_Country_Id)->TM_CON_Name,
        ),         		
		//'TM_SCL_Logo',
		'TM_SCL_Headteacher',
		'TM_SCL_CreatedOn',
        array(
            'label'=>'Created By',
            'type'=>'raw',
            'value'=>User::model()->GetUser($userid),
        ), 
           array(
            'type'=>'raw',
           'name'=>'TM_SCL_Image',
        
            'value' => function($model) { 

             if($model->TM_SCL_Image!='')
             {
                return CHtml::tag('img',array("title"=>"HeaderImage","src"=>Yii::app()->baseUrl."/images/headers/".$model->TM_SCL_Image,
                "style"=>"height:70px") );
             }
             else
             {
                return "No Image";
             }
              },
            //'value'=> CHtml::tag('img',array("title"=>"HeaderImage","src"=>Yii::app()->baseUrl."/images/headers/".$model->TM_SCL_Image,
                //"style"=>"height:70px") )

            ),         
		//'TM_SCL_CreatedBy',
	),'htmlOptions' => array('class' => 'table table-bordered table-hover')
)); ?>
    </div>
</div>
