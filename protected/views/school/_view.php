<?php
/* @var $this SchoolController */
/* @var $data School */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_SCL_Id), array('view', 'id'=>$data->TM_SCL_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Sylllabus_Id')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Sylllabus_Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Address')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Contactperson')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Contactperson); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Contactdesignation')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Contactdesignation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Email')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Phone')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Mobile')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_City')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_City); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_State')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_State); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Country')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Logo')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_Headteacher')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_Headteacher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_SCL_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_SCL_CreatedBy); ?>
	<br />

	*/ ?>

</div>