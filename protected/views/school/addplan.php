<?php

$this->menu=array(
    array('label'=>'Manage School', 'class'=>'nav-header'),
	array('label'=>'List School', 'url'=>array('admin')),
	array('label'=>'Create School', 'url'=>array('create'),'visible'=>UserModule::isSuperAdmin()),
	array('label'=>'Update School', 'url'=>array('update', 'id'=>$school->TM_SCL_Id),'visible'=>UserModule::isSuperAdmin()),
	array('label'=>'Delete School', 'url'=>'#','visible'=>UserModule::isSuperAdmin(), 'linkOptions'=>array('submit'=>array('delete','id'=>$school->TM_SCL_Id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h3><?php echo  UserModule::t('Add Plan') ?></h3>

<div class="row brd1">
<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data','id'=>'planform')); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_Publisher_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_Publisher_Id',CHtml::listData(Publishers::model()->findAll(array('condition'=>'TM_PR_Status=0','order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),array('empty' => 'Select Publisher','class'=>'form-control')); ?>                
                <?php echo CHtml::error($model,'TM_SPN_Publisher_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_SyllabusId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_SyllabusId',CHtml::listData(Syllabus::model()->findAll(array('condition'=>'TM_SB_Status=0','order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty' => 'Select Syllabus','class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_SyllabusId'); ?>
            </div>
        </div>  
        
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_StandardId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_StandardId',array(''=>'Select Standard'),array('empty' => 'Select Standard','class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_StandardId'); ?>
            </div>
        </div>
          
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_PlanId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_PlanId',array(''=>'Select Plan'),array('class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_PlanId'); ?>
            </div>
        </div>  
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_Rate',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextField($model,'TM_SPN_Rate',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_Rate'); ?>
            </div>
        </div>        
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_Currency',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_Currency',CHtml::listData(Currency::model()->findAll(array('order' => 'TM_CR_Name')),'TM_CR_Id','TM_CR_Name'),array('empty'=>'Select Currency','class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_Currency'); ?>
            </div>
        </div>               
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_StartDate',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activedateField($model,'TM_SPN_StartDate',array('class'=>'form-control','value'=>date('Y-m-d'))); ?>
                <?php echo CHtml::error($model,'TM_SPN_StartDate'); ?>
            </div>
        </div>
        
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_ExpieryDate',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activedateField($model,'TM_SPN_ExpieryDate',array('class'=>'form-control','value'=>date('Y-m-d'))); ?>
                <?php echo CHtml::error($model,'TM_SPN_ExpieryDate'); ?>
            </div>
        </div>         
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_No_of_Students',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextField($model,'TM_SPN_No_of_Students',array('class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_No_of_Students'); ?>
            </div>
        </div>               
    	<div class="form-group">
    		<?php echo CHtml::activeLabelEx($model,'TM_SPN_Type',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
    		<?php echo CHtml::activeDropDownList($model,'TM_SPN_Type',SchoolPlan::itemAlias('PaymentType'),array('class'=>'form-control')); ?>
    		<?php echo CHtml::error($model,'TM_SPN_Type'); ?>
                </div>
    	</div>

        
                

        <!--<div class="form-group">
            <?php //echo CHtml::activeLabelEx($model,'TM_SPN_CouponId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-3">
                <?php  //echo CHtml::activeTextField($model,'TM_SPN_CouponId',array('class'=>'form-control','style'=>'width:200px;')); ?>
                <?php //echo CHtml::error($model,'TM_SPN_CouponId'); ?>
                
            </div>
            <div class="col-sm-2">
                <a href="javascript:void(0)" id="redeem">Recalculate</a>
            </div>
            <div class="col-sm-4">
              <div class="vouchererror" ></div>
            </div>  
        </div> --> 
         <div class="form-group">

            <div class="col-sm-3 pull-right">
                    <input type="hidden" id="planexist" value="0" />
                    <?php echo CHtml::activeHiddenfield($model,'TM_SPN_SchoolId',array('value'=>$id,'class'=>'form-control')); ?>
                    <?php echo CHtml::activeHiddenfield($model,'TM_SPN_SubjectId',array('value'=>'1','class'=>'form-control')); ?>                    
	              <?php echo CHtml::submitButton('Add Plan',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
            
            <div class="col-md-3 pull-right" id="planrate" style="font-weight: bold;;"></div>
        </div>     
<?php echo CHtml::endForm(); ?>
</div>
<?php 

Yii::app()->clientScript->registerScript('jsfunctions', "  
    $(document).on('change','#SchoolPlan_TM_SPN_SyllabusId',function(){
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('school/GetStandardList')."',
              data: { id:$(this).val()}
            })
        .done(function( html ) {            
            $('#SchoolPlan_TM_SPN_StandardId').html(html);
            $('#SchoolPlan_TM_SPN_PlanId').html('<option >Select</option>');
            
        });
    })
    $(document).on('change','#SchoolPlan_TM_SPN_StandardId',function(){

        var publisher=$('#SchoolPlan_TM_SPN_Publisher_Id').val();
        var syllabus=$('#SchoolPlan_TM_SPN_SyllabusId').val();
        var standard=$('#SchoolPlan_TM_SPN_StandardId').val();
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('school/GetPlanList')."',
              data: { publisher:publisher,syllabus:syllabus,standard:standard}
            })
        .done(function( html ) {
            $('#SchoolPlan_TM_SPN_PlanId').html(html);
        });
    })
    $(document).on('click','#redeem',function(){
        var voucher=$('#SchoolPlan_TM_SPN_CouponId').val();
        var plan=$('#SchoolPlan_TM_SPN_PlanId').val();
            $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('coupons/GetCouvherRate')."',                  
                  dataType:'json',
                  data: { plan:plan,voucher:voucher}
                })
            .done(function( data ) {                
                if(data.error=='no')
                {                    
                    $('#planrate').html('Total Amount: '+data.rate);
                }
                else
                {                    
                    $('.vouchererror').html(data.error);
                    $('#planrate').html('Total Amount: '+data.rate);                    
                    setTimeout(function(){
                        $('#SchoolPlan_TM_SPN_CouponId').val('')
                        $('.vouchererror').html('');
                    },5000)
                }
            });
    });
    $(document).on('change','#SchoolPlan_TM_SPN_PlanId',function(){
            var plan=$(this).val();
            var school=".$id.";        
            checkPlan(plan,school);      
    });
    function checkPlan(plan,school)
    {
        $('.error').hide();
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('user/admin/Checkactiveplanschool')."',
          data: { plan:plan,school:school}
        })
          .done(function( data ) {
            if(data>0)
            {
                $('#SchoolPlan_TM_SPN_PlanId').after( '<div class=\"error alert alert-danger\">Plan already active </div>' );
                $('#planexist').val('1');
            }
            else
            {
                $('#planexist').val('0');
            }               
        });                
    }        
    $(document).on('change','#SchoolPlan_TM_SPN_PlanId',function(){
        var voucher=$('#SchoolPlan_TM_SPN_CouponId').val();
        var plan=$(this).val();
        if(voucher!=''){
            $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('coupons/GetCouvherRate')."',                  
                  dataType:'json',
                  data: { plan:plan,voucher:voucher}
                })
            .done(function( data ) {                
                if(data.error=='no')
                {                    
                    $('#planrate').html('Total Amount: '+data.rate);
                }
                else
                {                    
                    $('.vouchererror').html(data.error);
                    $('#planrate').html('Total Amount: '+data.rate);                    
                    setTimeout(function(){
                        $('#SchoolPlan_TM_SPN_CouponId').val('')
                        $('.vouchererror').html('');
                    },5000)
                }
            });
        }
    }); 
    $('#planform').submit(function(){
        if($('#planexist').val()==0)
        {
            return true;
        }    
        else
        {
            $('#SchoolPlan_TM_SPN_PlanId').after( '<div class=\"error alert alert-danger\">Plan already active </div>' );
            return false;
        }
    });   
 ");
 
 ?>
 