<?php
/* @var $this CurrencyController */
/* @var $data Currency */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CR_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_CR_Id), array('view', 'id'=>$data->TM_CR_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CR_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CR_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CR_Code')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CR_Code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CR_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CR_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CR_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CR_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_CR_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_CR_Status); ?>
	<br />


</div>