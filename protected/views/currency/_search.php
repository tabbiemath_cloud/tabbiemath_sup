<?php
/* @var $this CurrencyController */
/* @var $model Currency */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_CR_Id'); ?>
		<?php echo $form->textField($model,'TM_CR_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CR_Name'); ?>
		<?php echo $form->textField($model,'TM_CR_Name',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CR_Code'); ?>
		<?php echo $form->textField($model,'TM_CR_Code',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CR_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_CR_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CR_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_CR_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_CR_Status'); ?>
		<?php echo $form->textField($model,'TM_CR_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->