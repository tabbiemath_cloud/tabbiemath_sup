<?php
/* @var $this CurrencyController */
/* @var $model Currency */

$this->breadcrumbs=array(
	'Currencies'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Currencies', 'class'=>'nav-header'),
	array('label'=>'List Currency', 'url'=>array('admin')),
	array('label'=>'Create Currency', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#currency-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>Manage Currencies</h3>
<div class="row brd1">
    <div class="col-lg-12">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'currency-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'TM_CR_Name',
		'TM_CR_Code',
        array(
            'name'=>'TM_CR_Status',
            'value'=>'Currency::itemAlias("CurrencyStatus",$data->TM_CR_Status)',
            'filter'=>Currency::itemAlias("CurrencyStatus"),
        ), array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'deleteButtonImageUrl'=>false,
            'updateButtonImageUrl'=>false,
            'viewButtonImageUrl'=>false,
            'buttons'=>array
            (
                'view'=>array(
                    'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                    'options'=>array(
                        'title'=>'View'
                    )),
                'update'=>array(
                    'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                    'options'=>array(
                        'title'=>'Update'
                    )),
                'delete'=>array(
                    'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                    'options'=>array(

                        'title'=>'Delete',
                    ),
                )
            ),
        ),
	),'itemsCssClass'=>"table table-bordered table-hover table-striped"
)); ?>
    </div>
</div>
