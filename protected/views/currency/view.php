<?php
/* @var $this CurrencyController */
/* @var $model Currency */

$this->breadcrumbs=array(
	'Currencies'=>array('admin'),
	$model->TM_CR_Id,
);

$this->menu=array(
    array('label'=>'Manage Currency', 'class'=>'nav-header'),
    array('label'=>'List Currency', 'url'=>array('admin')),
    array('label'=>'Create Currency', 'url'=>array('create')),
    array('label'=>'Update Currency', 'url'=>array('update', 'id'=>$model->TM_CR_Id)),
    array('label'=>'Delete Currency', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_CR_Id),'confirm'=>'Are you sure you want to delete this item?')),

);;
?>

<h3>View <?php echo $model->TM_CR_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $id=$model->TM_CR_Id;
        $userid=$model->TM_CR_CreatedBy;
        $status=$model->TM_CR_Status;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_CR_Id',
                'TM_CR_Name',
                'TM_CR_Code',
                'TM_CR_CreatedOn',
                array(
                    'name'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Coupons::itemAlias("CouponsStatus",$status),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
