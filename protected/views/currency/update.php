<?php
/* @var $this CurrencyController */
/* @var $model Currency */

$this->breadcrumbs=array(
	'Currencies'=>array('admin'),
	$model->TM_CR_Id=>array('view','id'=>$model->TM_CR_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Currency', 'class'=>'nav-header'),
    array('label'=>'List Currency', 'url'=>array('admin')),
    array('label'=>'Create Currency', 'url'=>array('create')),
    array('label'=>'View Currency', 'url'=>array('view', 'id'=>$model->TM_CR_Id)),
);
?>

<h1>Update Currency <?php echo $model->TM_CR_Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>