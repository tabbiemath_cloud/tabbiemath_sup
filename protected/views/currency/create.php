<?php
/* @var $this CurrencyController */
/* @var $model Currency */

$this->breadcrumbs=array(
	'Currencies'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Currency', 'class'=>'nav-header'),
	array('label'=>'List Currency', 'url'=>array('admin')),
);
?>

<h3>Create Currency</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>