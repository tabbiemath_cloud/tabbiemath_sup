<?php
/* @var $this PublishersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Publishers',
);

$this->menu=array(
	array('label'=>'Create Publishers', 'url'=>array('create')),
	array('label'=>'Manage Publishers', 'url'=>array('admin')),
);
?>

<h1>Publishers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
