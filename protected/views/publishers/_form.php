<?php
/* @var $this PublishersController */
/* @var $model Publishers */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('publishers/admin')."';
    });
");
?>

<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'publishers-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="panel-body form-horizontal payment-form">
    <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
	<?php /*echo $form->errorSummary($model); */?>
    <div class="form-group">
		<?php echo $form->labelEx($model,'TM_PR_Name',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?php echo $form->textField($model,'TM_PR_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
            <?php echo $form->error($model,'TM_PR_Name'); ?>
        </div>
	</div>
    <div class="form-group">
        <?php echo $form->labelEx($model,'TM_PR_Status',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?php echo $form->hiddenField($model,'TM_PR_CreatedBy',array('value'=>Yii::app()->user->id)); ?>
            <?php echo $form->dropDownList($model,'TM_PR_Status',Publishers::itemAlias('PublisherStatus'),array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'TM_PR_Status'); ?>
        </div>
	</div>

    <div class="form-group">
        <div class="row">
        <div class="col-sm-3 pull-right">
		    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
        </div>
            <div class="col-lg-3 pull-right">
                <button class="btn btn-warning btn-lg btn-block cancelbtn" type="reset" value="Reset">Cancel</button>
            </div>
            </div>
	</div>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
