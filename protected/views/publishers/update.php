<?php
/* @var $this PublishersController */
/* @var $model Publishers */

$this->breadcrumbs=array(
	'Publishers'=>array('admin'),
	$model->TM_PR_Id=>array('view','id'=>$model->TM_PR_Id),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Publishers', 'class'=>'nav-header'),
	array('label'=>'List Publishers', 'url'=>array('admin')),
	array('label'=>'Create Publishers', 'url'=>array('create')),
	array('label'=>'View Publishers', 'url'=>array('view', 'id'=>$model->TM_PR_Id)),

);
?>

<h3>Update <?php echo $model->TM_PR_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>