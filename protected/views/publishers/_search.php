<?php
/* @var $this PublishersController */
/* @var $model Publishers */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_PR_Id'); ?>
		<?php echo $form->textField($model,'TM_PR_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PR_Name'); ?>
		<?php echo $form->textField($model,'TM_PR_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PR_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_PR_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PR_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_PR_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_PR_Status'); ?>
		<?php echo $form->textField($model,'TM_PR_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->