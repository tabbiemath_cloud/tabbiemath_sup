<?php
/* @var $this PublishersController */
/* @var $model Publishers */

$this->breadcrumbs=array(
	'Publishers'=>array('admin'),
	$model->TM_PR_Id,
);

$this->menu=array(
    array('label'=>'Manage Publishers', 'class'=>'nav-header'),
	array('label'=>'List Publishers', 'url'=>array('admin')),
	array('label'=>'Create Publishers', 'url'=>array('create')),
	array('label'=>'Update Publishers', 'url'=>array('update', 'id'=>$model->TM_PR_Id)),
	array('label'=>'Delete Publishers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_PR_Id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View <?php echo $model->TM_PR_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $id=$model->TM_PR_Status;
        $userid=$model->TM_PR_CreatedBy;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_PR_Id',
                'TM_PR_Name',
                'TM_PR_CreatedOn',
                array(
                    'label'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Publishers::itemAlias("PublisherStatus",$id),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
