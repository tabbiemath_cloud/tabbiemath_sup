<?php
/* @var $this PublishersController */
/* @var $model Publishers */

$this->breadcrumbs=array(
	'Publishers'=>array('admin'),
	'Create',
);
$this->menu=array(
    array('label'=>'Manage Publishers', 'class'=>'nav-header'),
    array('label'=>'List Publishers', 'url'=>array('admin')),
);
?>

<h3>Create Publishers</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>