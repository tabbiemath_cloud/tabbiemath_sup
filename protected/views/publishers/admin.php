<?php
/* @var $this PublishersController */
/* @var $model Publishers */


$this->breadcrumbs=array(
	'Publishers'=>array('admin'),
);

$this->menu=array(
	array('label'=>'Manage Publishers', 'class'=>'nav-header'),
	array('label'=>'List Publishers', 'url'=>array('admin')),
	array('label'=>'Create Publishers', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#publishers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>Manage Publishers</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'publishers-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'columns'=>array(
            'TM_PR_Name',
            array(
                'name'=>'TM_PR_Status',
                'value'=>'Publishers::itemAlias("PublisherStatus",$data->TM_PR_Status)',
                'filter'=>Publishers::itemAlias('PublisherStatus'),
            ),
            array(
                'class'=>'CButtonColumn',
                'template'=>'{view}{update}{delete}',
                'deleteButtonImageUrl'=>false,
                'updateButtonImageUrl'=>false,
                'viewButtonImageUrl'=>false,
                'buttons'=>array
                (
                    'view'=>array(
                        'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                        'options'=>array(
                            'title'=>'View'
                        )),
                    'update'=>array(
                        'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                        'options'=>array(
                            'title'=>'Update'
                        )),
                    'delete'=>array(
                        'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                        'options'=>array(

                            'title'=>'Delete',
                        ),
                    )
                ),
            ),
        ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
    )); ?>
    </div><!--/span-->

</div><!--/row-->

