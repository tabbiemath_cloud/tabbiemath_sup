<?php
/* @var $this PublishersController */
/* @var $data Publishers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PR_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_PR_Id), array('view', 'id'=>$data->TM_PR_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PR_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PR_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PR_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PR_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PR_CreatedBy')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PR_CreatedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_PR_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_PR_Status); ?>
	<br />


</div>