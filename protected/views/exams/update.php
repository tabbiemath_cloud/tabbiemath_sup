<?php
/* @var $this ExamsController */
/* @var $model Exams */

$this->breadcrumbs=array(
	'Exams'=>array('index'),
	$model->TM_EX_Id=>array('view','id'=>$model->TM_EX_Id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Exams', 'url'=>array('index')),
	array('label'=>'Create Exams', 'url'=>array('create')),
	array('label'=>'View Exams', 'url'=>array('view', 'id'=>$model->TM_EX_Id)),
	array('label'=>'Manage Exams', 'url'=>array('admin')),
);
?>

<h1>Update Exams <?php echo $model->TM_EX_Id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>