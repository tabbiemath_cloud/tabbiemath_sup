<?php
/* @var $this ExamsController */
/* @var $data Exams */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_EX_Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->TM_EX_Id), array('view', 'id'=>$data->TM_EX_Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_EX_Name')); ?>:</b>
	<?php echo CHtml::encode($data->TM_EX_Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_EX_CreatedOn')); ?>:</b>
	<?php echo CHtml::encode($data->TM_EX_CreatedOn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_EX_Createdby')); ?>:</b>
	<?php echo CHtml::encode($data->TM_EX_Createdby); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TM_EX_Status')); ?>:</b>
	<?php echo CHtml::encode($data->TM_EX_Status); ?>
	<br />


</div>