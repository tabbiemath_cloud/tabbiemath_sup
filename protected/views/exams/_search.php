<?php
/* @var $this ExamsController */
/* @var $model Exams */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_EX_Id'); ?>
		<?php echo $form->textField($model,'TM_EX_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_EX_Name'); ?>
		<?php echo $form->textField($model,'TM_EX_Name',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_EX_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_EX_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_EX_Createdby'); ?>
		<?php echo $form->textField($model,'TM_EX_Createdby'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_EX_Status'); ?>
		<?php echo $form->textField($model,'TM_EX_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->