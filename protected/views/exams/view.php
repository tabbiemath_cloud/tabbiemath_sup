<?php
/* @var $this ExamsController */
/* @var $model Exams */

$this->breadcrumbs=array(
	'Exams'=>array('index'),
	$model->TM_EX_Id,
);

$this->menu=array(
	array('label'=>'List Exams', 'url'=>array('index')),
	array('label'=>'Create Exams', 'url'=>array('create')),
	array('label'=>'Update Exams', 'url'=>array('update', 'id'=>$model->TM_EX_Id)),
	array('label'=>'Delete Exams', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_EX_Id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Exams', 'url'=>array('admin')),
);
?>

<h1>View Exams #<?php echo $model->TM_EX_Id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'TM_EX_Id',
		'TM_EX_Name',
		'TM_EX_CreatedOn',
		'TM_EX_Createdby',
		'TM_EX_Status',
	),
)); ?>
