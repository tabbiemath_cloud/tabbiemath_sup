<?php
/* @var $this ExamsController */
/* @var $model Exams */

$this->breadcrumbs=array(
	'Exams'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Exams', 'url'=>array('index')),
	array('label'=>'Create Exams', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#exams-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Exams</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'exams-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'TM_EX_Id',
		'TM_EX_Name',
		'TM_EX_CreatedOn',
		'TM_EX_Createdby',
		'TM_EX_Status',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
