<?php
/* @var $this ExamsController */
/* @var $model Exams */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exams-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_EX_Name'); ?>
		<?php echo $form->textField($model,'TM_EX_Name',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'TM_EX_Name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_EX_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_EX_CreatedOn'); ?>
		<?php echo $form->error($model,'TM_EX_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_EX_Createdby'); ?>
		<?php echo $form->textField($model,'TM_EX_Createdby'); ?>
		<?php echo $form->error($model,'TM_EX_Createdby'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TM_EX_Status'); ?>
		<?php echo $form->textField($model,'TM_EX_Status'); ?>
		<?php echo $form->error($model,'TM_EX_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->