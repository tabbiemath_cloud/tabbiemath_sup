<?php
/* @var $this TopicController */
/* @var $model Topic */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'TM_TP_Id'); ?>
		<?php echo $form->textField($model,'TM_TP_Id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TP_Name'); ?>
		<?php echo $form->textField($model,'TM_TP_Name',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TP_CreatedOn'); ?>
		<?php echo $form->textField($model,'TM_TP_CreatedOn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TP_CreatedBy'); ?>
		<?php echo $form->textField($model,'TM_TP_CreatedBy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TM_TP_Status'); ?>
		<?php echo $form->textField($model,'TM_TP_Status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->