<?php
/* @var $this TopicController */
/* @var $model Topic */

$this->breadcrumbs=array(
	'Chapter'=>array('admin'),
	'Update',
);

$this->menu=array(
    array('label'=>'Manage Chapter', 'class'=>'nav-header'),
	array('label'=>'List Chapter', 'url'=>array('admin')),
	array('label'=>'Create Chapter', 'url'=>array('create')),
	array('label'=>'View Chapter', 'url'=>array('view', 'id'=>$model->TM_TP_Id)),
);
?>

<h3>Update <?php echo $model->TM_TP_Name; ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>