<?php
/* @var $this TopicController */
/* @var $model Topic */

$this->breadcrumbs=array(
	'Chapter'=>array('admin'),
	'Create',
);

$this->menu=array(
    array('label'=>'Manage Chapter', 'class'=>'nav-header'),
	array('label'=>'List Chapter', 'url'=>array('admin')),
);
?>

<h3>Create Chapter</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>