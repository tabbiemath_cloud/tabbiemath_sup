<?php
/* @var $this TopicController */
/* @var $model Topic */

$this->breadcrumbs=array(
	'Chapter'=>array('admin'),
	$model->TM_TP_Id,
);

$this->menu=array(
    array('label'=>'Manage Chapter', 'class'=>'nav-header'),
	array('label'=>'List Chapter', 'url'=>array('admin')),
	array('label'=>'Create Chapter', 'url'=>array('create')),
	array('label'=>'Update Chapter', 'url'=>array('update', 'id'=>$model->TM_TP_Id)),
	array('label'=>'Delete Chapter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->TM_TP_Id),'confirm'=>'Are you sure you want to delete this item?')),

);
?>

<h3>View <?php echo $model->TM_TP_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php
        $topicid=$model->TM_TP_Status;
        $userid=$model->TM_TP_CreatedBy;
        $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'TM_TP_Id',
                array(
                    'name'=>'TM_TP_Syllabus_Id',
                    'type'=>'raw',
                    'value'=>Syllabus::model()->findByPk($model->TM_TP_Syllabus_Id)->TM_SB_Name,
                ),
                array(
                    'name'=>'TM_TP_Standard_Id',
                    'type'=>'raw',
                    'value'=>Standard::model()->findByPk($model->TM_TP_Standard_Id)->TM_SD_Name,
                ),
                'TM_TP_Name',
                'TM_TP_CreatedOn',
                array(
                    'label'=>'Created By',
                    'type'=>'raw',
                    'value'=>User::model()->GetUser($userid),
                ),
                array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>Chapter::itemAlias("ChapterStatus",$topicid),
                ),
            ),
            'htmlOptions' => array('class' => 'table table-bordered table-hover')
        )); ?>
    </div>
</div>
