<?php
/* @var $this TopicController */
/* @var $model Topic */

$this->breadcrumbs=array(
	'Chapter'=>array('admin'),
	'Manage',
);

$this->menu=array(
    array('label'=>'Manage Chapter', 'class'=>'nav-header'),
	array('label'=>'List Chapters', 'url'=>array('admin')),
	array('label'=>'Create Chapter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#topic-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>Manage Chapters</h3>
<div class="row brd1">
    <div class="col-lg-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'chapter-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'enableSorting' => false,
            'columns'=>array(
                'TM_TP_Name',
                array(
                    'name'=>'TM_TP_Syllabus_Id',
                    'value'=>'Syllabus::model()->findByPk($data->TM_TP_Syllabus_Id)->TM_SB_Name',
                    'filter'=>CHtml::listData(Syllabus::model()->findAll(array('order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),
                ),
                array(
                    'name'=>'TM_TP_Standard_Id',
                    'value'=>'Standard::model()->findByPk($data->TM_TP_Standard_Id)->TM_SD_Name',
                    'filter'=>
                        CHtml::listData(
                            is_numeric($model->TM_TP_Syllabus_Id) ? Standard::model()->findAll(new CDbCriteria(array(
                                'condition' => "TM_SD_Syllabus_Id = :syllabusid AND TM_SD_Status='0'",
                                'params' => array(':syllabusid' => $model->TM_TP_Syllabus_Id),
                                'order' => 'TM_SD_Id'
                            ))) : Standard::model()->findAll(array('condition' => "TM_SD_Status='0'",'order' => 'TM_SD_Id')),'TM_SD_Id','TM_SD_Name'),
                ),

                array(
                    'name'=>'TM_TP_Status',
                    'value'=>'Chapter::itemAlias("ChapterStatus",$data->TM_TP_Status)',
                    'filter'=>Chapter::itemAlias("ChapterStatus"),
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            )),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
                array(
                    'header' => 'Order',
                    'name' => 'TM_TP_order',
                    'class' => 'ext.OrderColumn.OrderColumn',
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped"
        )); ?>
    </div>
</div>
