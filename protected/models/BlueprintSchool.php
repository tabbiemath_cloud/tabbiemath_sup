<?php

/**
 * This is the model class for table "{{blueprint_school}}".
 *
 * The followings are the available columns in table '{{blueprint_school}}':
 * @property integer $TM_BPS_Id
 * @property integer $TM_BPS_Blueprint_Id
 * @property integer $TM_BPS_School_Id
 * @property integer $TM_BPS_Status
 */
class BlueprintSchool extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{blueprint_school}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_BPS_Blueprint_Id, TM_BPS_School_Id, TM_BPS_Status', 'required'),
			array('TM_BPS_Blueprint_Id, TM_BPS_School_Id, TM_BPS_Status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_BPS_Id, TM_BPS_Blueprint_Id, TM_BPS_School_Id, TM_BPS_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_BPS_Id' => 'Tm Bps',
			'TM_BPS_Blueprint_Id' => 'Tm Bps Blueprint',
			'TM_BPS_School_Id' => 'Tm Bps School',
			'TM_BPS_Status' => 'Tm Bps Status',
		);
	}

	public static function itemAlias($type,$code=NULL) {
        $_items = array(
            'BlueprintActivate' => array(
                '1' => ('Active'),
                '0' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_BPS_Id',$this->TM_BPS_Id);
		$criteria->compare('TM_BPS_Blueprint_Id',$this->TM_BPS_Blueprint_Id);
		$criteria->compare('TM_BPS_School_Id',$this->TM_BPS_School_Id);
		$criteria->compare('TM_BPS_Status',$this->TM_BPS_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlueprintSchool the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
