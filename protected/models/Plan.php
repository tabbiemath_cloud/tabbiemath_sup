<?php

/**
 * This is the model class for table "{{plan}}".
 *
 * The followings are the available columns in table '{{plan}}':
 * @property integer $TM_PN_Id
 * @property string $TM_PN_Name
 * @property string $TM_PN_Rate
 * @property integer $TM_PN_Duration
 * @property integer $TM_PN_Publisher_Id
 * @property integer $TM_PN_Syllabus_Id
 * @property integer $TM_PN_Standard_Id
 * @property integer $TM_PN_Currency_Id
 * @property string $TM_PN_CreatedOn
 * @property integer $TM_PN_CreatedBy
 * @property integer $TM_PN_Status
 * @property integer $TM_PN_Type
 * @property integer $TM_PN_No_of_Students
 * @property integer $TM_PN_Additional_Rate
 * @property integer $TM_PN_Additional_Currency_Id
 * @property integer $TM_PN_Visibility
 */
class Plan extends CActiveRecord
{
	public $planmodules;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{plan}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_PN_Name,TM_PN_Publisher_Id,TM_PN_Syllabus_Id,TM_PN_Standard_Id', 'required'),
			array('TM_PN_Duration,TM_PN_Rate, TM_PN_CreatedBy, TM_PN_Status,TM_PN_Visibility', 'numerical', 'integerOnly'=>true),
			array('TM_PN_Name,TM_PN_Publisher_Id,TM_PN_Syllabus_Id,TM_PN_Standard_Id,TM_PN_Status,TM_PN_Type', 'required', 'on' => 'createschoolplan'),
            array('planmodules,TM_PN_Rate,TM_PN_Currency_Id', 'required', 'except' => 'createschoolplan'), 
			array('TM_PN_Name', 'length', 'max'=>250),
			array('TM_PN_Rate,TM_PN_Additional_Rate', 'length', 'max'=>20),
            array('TM_PN_Additional_Currency_Id, TM_PN_Additional_Rate', 'currencycheck'),                       
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_PN_Id, TM_PN_Name, TM_PN_Rate, TM_PN_Duration, TM_PN_CreatedOn, TM_PN_CreatedBy, TM_PN_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'modules'=>array(self::HAS_MANY, 'Planmodules', 'TM_PM_Plan_Id'),
            'student'=>array(self::HAS_MANY, 'StudentPlan', 'TM_SPN_PlanId'),
            'currency'=>array(self::BELONGS_TO, 'Currency', 'TM_PN_Currency_Id'),
            'additional'=>array(self::BELONGS_TO, 'Currency', 'TM_PN_Additional_Currency_Id'),            
        );
	}
    public function getModules($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT TM_PM_Module AS Id
              FROM  tm_planmodules AS a
              WHERE TM_PM_Plan_Id='".$id."'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $plans=$dataReader->readAll();
        return $plans;
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_PN_Id' => 'ID',
			'TM_PN_Name' => 'Plan Name',
            'TM_PN_Publisher_Id' => 'Publisher',
            'TM_PN_Syllabus_Id' => 'Syllabus',
            'TM_PN_Standard_Id' => 'Standard',
            'TM_PN_Currency_Id' => 'Currency',
			'TM_PN_Rate' => 'Rate',
			'TM_PN_Duration' => 'Duration',
			'TM_PN_CreatedOn' => 'Created On',
			'TM_PN_CreatedBy' => 'Created By',
			'TM_PN_Status' => 'Status',
			'planmodules' => 'Plan Modules',
            'TM_PN_Type'=>'Type',
            'TM_PN_No_of_Students'=>'Maximum Students',			
            'TM_PN_Additional_Currency_Id'=>'Additional Currency',
            'TM_PN_Additional_Rate'=>'Additional Rate',
            'TM_PN_Visibility'=>'Visibility'
		);
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'PlanStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
            'PlanVisibility' => array(
                '0' => ('Public'),
                '1' => ('Admin Only'),
            ), 
			);
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_PN_Id',$this->TM_PN_Id);
		$criteria->compare('TM_PN_Name',$this->TM_PN_Name,true);
		$criteria->compare('TM_PN_Rate',$this->TM_PN_Rate,true);
		$criteria->compare('TM_PN_Duration',$this->TM_PN_Duration);
		$criteria->compare('TM_PN_Publisher_Id',$this->TM_PN_Publisher_Id);
		$criteria->compare('TM_PN_Syllabus_Id',$this->TM_PN_Syllabus_Id);
		$criteria->compare('TM_PN_Standard_Id',$this->TM_PN_Standard_Id);
		$criteria->compare('TM_PN_Currency_Id',$this->TM_PN_Currency_Id);
		$criteria->compare('TM_PN_CreatedOn',$this->TM_PN_CreatedOn,true);
		$criteria->compare('TM_PN_CreatedBy',$this->TM_PN_CreatedBy);
        if($this->TM_PN_Visibility=='public' || $this->TM_PN_Visibility=='Public'):
            $criteria->compare('TM_PN_Visibility','0');
        elseif($this->TM_PN_Visibility=='Admin Only' || $this->TM_PN_Visibility=='Admin only' || $this->TM_PN_Visibility=='admin'):
            $criteria->compare('TM_PN_Visibility','1');
        endif;        
		$criteria->compare('TM_PN_Status',$this->TM_PN_Status);
		$criteria->addCondition('TM_PN_Type = 0');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}
    public function currencycheck($attribute,$params)
    {
      if(!empty($this->TM_PN_Additional_Currency_Id) && empty($this->TM_PN_Additional_Rate))
      {
        $this->addError('TM_PN_Additional_Rate','Additional Rate cannot be blank');
      } 
      elseif(empty($this->TM_PN_Additional_Currency_Id) && !empty($this->TM_PN_Additional_Rate))
      { 
        $this->addError('TM_PN_Additional_Currency_Id','Additional Currency cannot be blank.');
      } 
    }
	public function searchschool()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_PN_Id',$this->TM_PN_Id);
		$criteria->compare('TM_PN_Name',$this->TM_PN_Name,true);
		$criteria->compare('TM_PN_Rate',$this->TM_PN_Rate,true);
		$criteria->compare('TM_PN_Duration',$this->TM_PN_Duration);
		$criteria->compare('TM_PN_Publisher_Id',$this->TM_PN_Publisher_Id);
		$criteria->compare('TM_PN_Syllabus_Id',$this->TM_PN_Syllabus_Id);
		$criteria->compare('TM_PN_Standard_Id',$this->TM_PN_Standard_Id);
		$criteria->compare('TM_PN_Currency_Id',$this->TM_PN_Currency_Id);
		$criteria->compare('TM_PN_CreatedOn',$this->TM_PN_CreatedOn,true);
		$criteria->compare('TM_PN_CreatedBy',$this->TM_PN_CreatedBy);
		$criteria->compare('TM_PN_Status',$this->TM_PN_Status);
        $criteria->addCondition('TM_PN_Type = 1');		
        $criteria->compare('TM_PN_No_of_Students',$this->TM_PN_No_of_Students);                
		return new CActiveDataProvider('Plan', array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Plan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function getCurrency($id)
    {
        if($id!=0):
            return Currency::model()->findByPk($id)->TM_CR_Code;
        else:
            return "";
        endif;        
    }
}
