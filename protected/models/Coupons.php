<?php

/**
 * This is the model class for table "{{coupons}}".
 *
 * The followings are the available columns in table '{{coupons}}':
 * @property integer $TM_CP_Id
 * @property string $TM_CP_Name
 * @property string $TM_CP_Code
 * @property integer $TM_CP_Type
 * @property string $TM_CP_value
 * @property string $TM_CP_duration
 * @property integer $TM_CP_Currency_Id
 * @property string $TM_CP_startdate
 * @property string $TM_CP_enddate
 * @property integer $TM_CP_Allocation
 * @property integer $TM_CP_Status
 * @property string $TM_CP_createdOn
 * @property integer $TM_CP_createdBy
 */
class Coupons extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{coupons}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CP_Name, TM_CP_Code, TM_CP_Type, TM_CP_value, TM_CP_startdate, TM_CP_enddate,TM_CP_Currency_Id', 'required'),
			array('TM_CP_Type, TM_CP_Allocation, TM_CP_Status, TM_CP_createdBy', 'numerical', 'integerOnly'=>true),
			array('TM_CP_Name, TM_CP_Code, TM_CP_value, TM_CP_duration', 'length', 'max'=>250),
            array('TM_CP_Code','unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CP_Id, TM_CP_Name, TM_CP_Code, TM_CP_Type, TM_CP_value, TM_CP_duration, TM_CP_startdate, TM_CP_enddate, TM_CP_Allocation, TM_CP_Status, TM_CP_createdOn, TM_CP_createdBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'currency'=>array(self::BELONGS_TO, 'Currency', 'TM_CP_Currency_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CP_Id' => 'Coupon ID',
			'TM_CP_Name' => 'Coupon Name',
			'TM_CP_Code' => 'Coupon Code',
			'TM_CP_Type' => 'Type',
			'TM_CP_Currency_Id' => 'Currency',
			'TM_CP_value' => 'Value',
			'TM_CP_duration' => 'Duration',
			'TM_CP_startdate' => 'Startdate',
			'TM_CP_enddate' => 'Enddate',
			'TM_CP_Allocation' => 'Allocation',
			'TM_CP_Status' => 'Status',
			'TM_CP_createdOn' => 'Created On',
			'TM_CP_createdBy' => 'Created By',
		);
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'CouponsStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
            'Coupontype' => array(
                '0' => ('Amount'),
                '1' => ('Percentage'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }


    public function GetType($id)
    {
        if($id=='0')
            $type='Amount';
        else
            $type='Percentage';
        return $type;
    }
    public function GetAllocation($allocid)
    {
       if($allocid=='0')
           $alloctype='All';
        elseif($allocid=='1')
            $alloctype='Student';
        else
            $alloctype='School';
        return $alloctype;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CP_Id',$this->TM_CP_Id);
		$criteria->compare('TM_CP_Name',$this->TM_CP_Name,true);
		$criteria->compare('TM_CP_Code',$this->TM_CP_Code,true);
		$criteria->compare('TM_CP_Currency_Id',$this->TM_CP_Currency_Id,true);
		$criteria->compare('TM_CP_Type',$this->TM_CP_Type);
		$criteria->compare('TM_CP_value',$this->TM_CP_value,true);
		$criteria->compare('TM_CP_duration',$this->TM_CP_duration,true);
		$criteria->compare('TM_CP_startdate',$this->TM_CP_startdate,true);
		$criteria->compare('TM_CP_enddate',$this->TM_CP_enddate,true);
		$criteria->compare('TM_CP_Allocation',$this->TM_CP_Allocation);
		$criteria->compare('TM_CP_Status',$this->TM_CP_Status);
		$criteria->compare('TM_CP_createdOn',$this->TM_CP_createdOn,true);
		$criteria->compare('TM_CP_createdBy',$this->TM_CP_createdBy);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}
    public function Getcouponname($id)
    {
        if($id=='-1'):
            return 'School Discount';
        else:
            return Coupons::model()->findByPk($data->TM_SPN_CouponId)->TM_CP_Code;
        endif;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Coupons the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
