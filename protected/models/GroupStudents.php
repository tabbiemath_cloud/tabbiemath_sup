<?php

/**
 * This is the model class for table "{{group_students}}".
 *
 * The followings are the available columns in table '{{group_students}}':
 * @property integer $TM_GRP_STU_Id
 * @property integer $TM_GRP_STUId
 * @property string $TM_GRP_STUName
 * @property integer $TM_GRP_Id
 * @property integer $TM_GRP_SCL_Id
 * @property integer $TM_GRP_SD_Id
 * @property integer $TM_GRP_PN_Id
 */
class GroupStudents extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{group_students}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_GRP_STUId, TM_GRP_STUName, TM_GRP_Id, TM_GRP_SCL_Id, TM_GRP_SD_Id, TM_GRP_PN_Id', 'required'),
			array('TM_GRP_STUId, TM_GRP_Id, TM_GRP_SCL_Id, TM_GRP_SD_Id, TM_GRP_PN_Id', 'numerical', 'integerOnly'=>true),
			array('TM_GRP_STUName', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_GRP_STU_Id, TM_GRP_STUId, TM_GRP_STUName, TM_GRP_Id, TM_GRP_SCL_Id, TM_GRP_SD_Id, TM_GRP_PN_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_GRP_STU_Id' => 'Tm Grp Stu',
			'TM_GRP_STUId' => 'Tm Grp Stuid',
			'TM_GRP_STUName' => 'Tm Grp Stuname',
			'TM_GRP_Id' => 'Tm Grp',
			'TM_GRP_SCL_Id' => 'Tm Grp Scl',
			'TM_GRP_SD_Id' => 'Tm Grp Sd',
			'TM_GRP_PN_Id' => 'Tm Grp Pn',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_GRP_STU_Id',$this->TM_GRP_STU_Id);
		$criteria->compare('TM_GRP_STUId',$this->TM_GRP_STUId);
		$criteria->compare('TM_GRP_STUName',$this->TM_GRP_STUName,true);
		$criteria->compare('TM_GRP_Id',$this->TM_GRP_Id);
		$criteria->compare('TM_GRP_SCL_Id',$this->TM_GRP_SCL_Id);
		$criteria->compare('TM_GRP_SD_Id',$this->TM_GRP_SD_Id);
		$criteria->compare('TM_GRP_PN_Id',$this->TM_GRP_PN_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GroupStudents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
