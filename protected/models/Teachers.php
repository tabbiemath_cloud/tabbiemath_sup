<?php

/**
 * This is the model class for table "{{teachers}}".
 *
 * The followings are the available columns in table '{{teachers}}':
 * @property integer $TM_TH_Id
 * @property string $TM_TH_Name
 * @property string $TM_TH_CreatedOn
 * @property integer $TM_TH_CreatedBy
 * @property integer $TM_TH_UpdatedBy
 * @property integer $TM_TH_UpdatedOn 
 * @property integer $TM_TH_Status
 * @property integer $TM_TH_SchoolId
 * @property integer $TM_TH_User_Id
 * @property integer $TM_TH_Manage_Questions
 */
class Teachers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{teachers}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' TM_TH_Name,TM_TH_CreatedBy, TM_TH_Status', 'required'),
            array('TM_TH_Manage_Questions,TM_TH_UserType','safe'),
			array('TM_TH_Id, TM_TH_CreatedBy, TM_TH_Status,TM_TH_SchoolId,TM_TH_UserType', 'numerical', 'integerOnly'=>true),
			array('TM_TH_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_TH_Id, TM_TH_Name, TM_TH_CreatedOn, TM_TH_CreatedBy, TM_TH_Status,TM_TH_SchoolId,TM_TH_UserType', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'question'=>array(self::HAS_MANY, 'Questions', 'TM_QN_Teacher_Id'),
            'user'=>array(self::BELONGS_TO, 'User', 'TM_TH_User_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_TH_Id' => 'Id',
			'TM_TH_Name' => 'Name',
			'TM_TH_CreatedOn' => 'Created On',
			'TM_TH_CreatedBy' => 'Created By',
			'TM_TH_Status' => 'Status',
			'TM_TH_SchoolId' => 'School Id',
			'TM_TH_User_Id' => 'User Id',
			'TM_TH_UserType' => 'User Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_TH_Id',$this->TM_TH_Id);
		$criteria->compare('TM_TH_Name',$this->TM_TH_Name,true);
		$criteria->compare('TM_TH_CreatedOn',$this->TM_TH_CreatedOn,true);
		$criteria->compare('TM_TH_CreatedBy',$this->TM_TH_CreatedBy);
		$criteria->compare('TM_TH_Status',$this->TM_TH_Status);
		$criteria->compare('TM_TH_SchoolId',$this->TM_TH_SchoolId);
		$criteria->compare('TM_TH_User_Id',$this->TM_TH_User_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'TeachersStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
			'UserLevel' => array(
					'0' => UserModule::t('Regular'),
					'1' => UserModule::t('Professional'),
			),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Teachers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
