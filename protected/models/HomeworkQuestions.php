<?php

/**
 * This is the model class for table "{{homework_questions}}".
 *
 * The followings are the available columns in table '{{homework_questions}}':
 * @property integer $TM_HQ_Id
 * @property integer $TM_HQ_Homework_Id
 * @property integer $TM_HQ_Question_Id
 * @property integer $TM_HQ_Type
 * @property integer $TM_HQ_Order
 */
class HomeworkQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{homework_questions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_HQ_Homework_Id, TM_HQ_Question_Id, TM_HQ_Type, TM_HQ_Order', 'required'),
			array('TM_HQ_Homework_Id, TM_HQ_Question_Id, TM_HQ_Type, TM_HQ_Order', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_HQ_Id, TM_HQ_Homework_Id, TM_HQ_Question_Id, TM_HQ_Type, TM_HQ_Order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_HQ_Id' => 'Tm Hq',
			'TM_HQ_Homework_Id' => 'Tm Hq Homework',
			'TM_HQ_Question_Id' => 'Tm Hq Question',
			'TM_HQ_Type' => 'Tm Hq Type',
			'TM_HQ_Order' => 'Tm Hq Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_HQ_Id',$this->TM_HQ_Id);
		$criteria->compare('TM_HQ_Homework_Id',$this->TM_HQ_Homework_Id);
		$criteria->compare('TM_HQ_Question_Id',$this->TM_HQ_Question_Id);
		$criteria->compare('TM_HQ_Type',$this->TM_HQ_Type);
		$criteria->compare('TM_HQ_Order',$this->TM_HQ_Order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HomeworkQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
