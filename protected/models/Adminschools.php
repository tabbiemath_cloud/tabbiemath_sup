<?php

/**
 * This is the model class for table "{{adminschools}}".
 *
 * The followings are the available columns in table '{{adminschools}}':
 * @property integer $TM_SA_Id
 * @property integer $TM_SA_Admin_Id
 * @property integer $TM_SA_School_Id
 */
class Adminschools extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{adminschools}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SA_Admin_Id, TM_SA_School_Id', 'required'),
			array('TM_SA_Admin_Id, TM_SA_School_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SA_Id, TM_SA_Admin_Id, TM_SA_School_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SA_Id' => 'Tm Sa',
			'TM_SA_Admin_Id' => 'Tm Sa Admin',
			'TM_SA_School_Id' => 'Tm Sa School',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SA_Id',$this->TM_SA_Id);
		$criteria->compare('TM_SA_Admin_Id',$this->TM_SA_Admin_Id);
		$criteria->compare('TM_SA_School_Id',$this->TM_SA_School_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Adminschools the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function getSchools($id)
    {
                $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_SCL_Name AS Name,a.TM_SCL_Id AS Id
              FROM  tm_school AS a, tm_adminschools AS b
              WHERE b.TM_SA_Admin_Id='".$id."' AND b.TM_SA_School_Id=a.TM_SCL_Id";              
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $exams=$dataReader->readAll();
        return $exams;
    }
}
