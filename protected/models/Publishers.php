<?php

/**
 * This is the model class for table "tm_publisher".
 *
 * The followings are the available columns in table 'tm_publisher':
 * @property integer $TM_PR_Id
 * @property string $TM_PR_Name
 * @property string $TM_PR_CreatedOn
 * @property integer $TM_PR_CreatedBy
 * @property integer $TM_PR_Status
 */
class Publishers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tm_publisher';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_PR_Name', 'required'),
			array('TM_PR_CreatedBy, TM_PR_Status', 'numerical', 'integerOnly'=>true),
			array('TM_PR_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_PR_Id, TM_PR_Name, TM_PR_CreatedOn, TM_PR_CreatedBy, TM_PR_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'question'=>array(self::HAS_MANY, 'Questions', 'TM_QN_Publisher_Id'),
            'studenttest'=>array(self::HAS_MANY, 'StudentTest', 'TM_STU_TT_Publisher_Id'),
            'mock'=>array(self::HAS_MANY, 'Mock', 'TM_MK_Publisher_Id'),
			'plan'=>array(self::HAS_MANY, 'Plan', 'TM_PN_Publisher_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_PR_Id' => 'ID',
			'TM_PR_Name' => 'Publisher Name',
			'TM_PR_CreatedOn' => 'Created On',
			'TM_PR_CreatedBy' => 'Created By',
			'TM_PR_Status' => 'Status',
		);
	}
    /*public function scopes()
    {
        return array(
            'active' => array(
                'condition' => 'status=0',
            ),
            'disabled' => array(
                'condition' => 'status=1',
            ),
        );
    }*/
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'PublisherStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_PR_Id',$this->TM_PR_Id);
		$criteria->compare('TM_PR_Name',$this->TM_PR_Name,true);
		$criteria->compare('TM_PR_CreatedOn',$this->TM_PR_CreatedOn,true);
		$criteria->compare('TM_PR_CreatedBy',$this->TM_PR_CreatedBy);
		$criteria->compare('TM_PR_Status',$this->TM_PR_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Publishers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
