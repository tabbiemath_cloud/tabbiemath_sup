<?php

/**
 * This is the model class for table "{{student_mocks}}".
 *
 * The followings are the available columns in table '{{student_mocks}}':
 * @property integer $TM_STMK_Id
 * @property integer $TM_STMK_Student_Id
 * @property integer $TM_STMK_Mock_Id
 * @property integer $TM_STMK_Plan_Id
 * @property double $TM_STMK_Marks
 * @property double $TM_STMK_TotalMarks
 * @property double $TM_STMK_Percentage
 * @property integer $TM_STMK_Status
 * @property integer $TM_STMK_Date
 * @property integer $TM_STMK_Action
 * @property integer $TM_STMK_Redo_Action
 */
class StudentMocks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{student_mocks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STMK_Student_Id, TM_STMK_Mock_Id, TM_STMK_Marks, TM_STMK_Status', 'required'),
			array('TM_STMK_Student_Id, TM_STMK_Mock_Id, TM_STMK_Status', 'numerical', 'integerOnly'=>true),
			array('TM_STMK_Marks', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STMK_Id, TM_STMK_Student_Id, TM_STMK_Mock_Id, TM_STMK_Marks, TM_STMK_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'mocks'=>array(self::BELONGS_TO, 'Mock', 'TM_STMK_Mock_Id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STMK_Id' => 'Tm Stmk',
			'TM_STMK_Student_Id' => 'Tm Stmk Student',
			'TM_STMK_Mock_Id' => 'Tm Stmk Mock',
			'TM_STMK_Marks' => 'Tm Stmk Marks',
			'TM_STMK_Status' => 'Tm Stmk Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STMK_Id',$this->TM_STMK_Id);
		$criteria->compare('TM_STMK_Student_Id',$this->TM_STMK_Student_Id);
		$criteria->compare('TM_STMK_Mock_Id',$this->TM_STMK_Mock_Id);
		$criteria->compare('TM_STMK_Marks',$this->TM_STMK_Marks);
		$criteria->compare('TM_STMK_Status',$this->TM_STMK_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentMocks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
