<?php

/**
 * This is the model class for table "{{topic}}".
 *
 * The followings are the available columns in table '{{topic}}':
 * @property integer $TM_TP_Id
 * @property integer $TM_TP_Standard_Id
 * @property integer $TM_TP_Syllabus_Id
 * @property string $TM_TP_Name
 * @property string $TM_TP_CreatedOn
 * @property integer $TM_TP_CreatedBy
 * @property integer $TM_TP_Status
 * @property integer $TM_TP_order
 */
class Chapter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{topic}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_TP_Syllabus_Id,TM_TP_Name,TM_TP_Standard_Id', 'required'),
			array('TM_TP_CreatedBy, TM_TP_Status', 'numerical', 'integerOnly'=>true),
			array('TM_TP_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_TP_Id, TM_TP_Name,TM_TP_Standard_Id, TM_TP_Parent_Id, TM_TP_CreatedOn, TM_TP_CreatedBy, TM_TP_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'topic'=>array(self::HAS_MANY, 'Topic', 'TM_SN_Topic_Id','condition'=>'TM_SN_Status=0','order'=>'TM_SN_order ASC',),
            'question'=>array(self::HAS_MANY, 'Questions', 'TM_QN_Topic_Id'),                        
            'standard'=>array(self::BELONGS_TO, 'Standard', 'TM_TP_Standard_Id'),
            'syllabus'=>array(self::BELONGS_TO, 'Syllabus', 'TM_TP_Syllabus_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_TP_Id' => 'ID',
			'TM_TP_Syllabus_Id' => 'Syllabus',
			'TM_TP_Standard_Id' => 'Standard',
			'TM_TP_Name' => 'Chapter',
			'TM_TP_CreatedOn' => 'Created On',
			'TM_TP_CreatedBy' => 'Created By',
			'TM_TP_Status' => 'Status',
		);
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'ChapterStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
    
    
        $criteria=new CDbCriteria;
    
        $criteria->compare('TM_TP_Id',$this->TM_TP_Id);
        $criteria->compare('TM_TP_Syllabus_Id',$this->TM_TP_Syllabus_Id);
        $criteria->compare('TM_TP_Standard_Id',$this->TM_TP_Standard_Id);
        $criteria->compare('TM_TP_Name',$this->TM_TP_Name,true);
        $criteria->compare('TM_TP_CreatedOn',$this->TM_TP_CreatedOn,true);
        $criteria->compare('TM_TP_CreatedBy',$this->TM_TP_CreatedBy);
        $criteria->compare('TM_TP_Status',$this->TM_TP_Status);
        $criteria->compare('TM_TP_order',$this->TM_TP_order);
    
    
    
        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'TM_TP_order asc',
                ),
                'pagination'=>array(
                    'pageSize'=>30,
                ),
            )
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Topic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
