<?php

/**
 * This is the model class for table "{{chalanges}}".
 *
 * The followings are the available columns in table '{{chalanges}}':
 * @property integer $TM_CL_Id
 * @property integer $TM_CL_Chalanger_Id
 * @property integer $TM_CL_QuestionCount
 * @property string $TM_CL_Created_On
 * @property integer $TM_CL_Plan
 * @property integer $TM_CL_Status
 */
class Challenges extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{chalanges}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CL_Chalanger_Id, TM_CL_QuestionCount, TM_CL_Created_On, TM_CL_Status', 'required'),
			array('TM_CL_Chalanger_Id, TM_CL_QuestionCount, TM_CL_Status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CL_Id, TM_CL_Chalanger_Id, TM_CL_QuestionCount, TM_CL_Created_On, TM_CL_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'challengeuser'=>array(self::HAS_MANY, 'Chalangeuserquestions', 'TM_CE_RE_Chalange_Id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CL_Id' => 'Tm Cl',
			'TM_CL_Chalanger_Id' => 'Tm Cl Chalanger',
			'TM_CL_QuestionCount' => 'Tm Cl Question Count',
			'TM_CL_Created_On' => 'Tm Cl Created On',
			'TM_CL_Status' => 'Tm Cl Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CL_Id',$this->TM_CL_Id);
		$criteria->compare('TM_CL_Chalanger_Id',$this->TM_CL_Chalanger_Id);
		$criteria->compare('TM_CL_QuestionCount',$this->TM_CL_QuestionCount);
		$criteria->compare('TM_CL_Created_On',$this->TM_CL_Created_On,true);
		$criteria->compare('TM_CL_Status',$this->TM_CL_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Challenges the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
