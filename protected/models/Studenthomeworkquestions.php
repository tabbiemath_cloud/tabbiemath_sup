<?php

/**
 * This is the model class for table "{{studenthomeworkquestions}}".
 *
 * The followings are the available columns in table '{{studenthomeworkquestions}}':
 * @property integer $TM_STHWQT_Id
 * @property integer $TM_STHWQT_Student_Id
 * @property integer $TM_STHWQT_Mock_Id
 * @property integer $TM_STHWQT_Parent_Id
 * @property integer $TM_STHWQT_Question_Id
 * @property integer $TM_STHWQT_Type
 * @property string $TM_STHWQT_Answer
 * @property double $TM_STHWQT_Marks
 * @property integer $TM_STHWQT_Status
 * @property integer $TM_STHWQT_Flag
 * @property integer $TM_STHWQT_Order
 * @property integer $TM_STHWQT_Redo_Flag
 * @property integer $TM_STHWQT_RedoStatus
 */
class Studenthomeworkquestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{studenthomeworkquestions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STHWQT_Student_Id, TM_STHWQT_Mock_Id, TM_STHWQT_Parent_Id, TM_STHWQT_Question_Id, TM_STHWQT_Type, TM_STHWQT_Answer, TM_STHWQT_Marks, TM_STHWQT_Status, TM_STHWQT_Flag, TM_STHWQT_Order, TM_STHWQT_Redo_Flag, TM_STHWQT_RedoStatus', 'required'),
			array('TM_STHWQT_Student_Id, TM_STHWQT_Mock_Id, TM_STHWQT_Parent_Id, TM_STHWQT_Question_Id, TM_STHWQT_Type, TM_STHWQT_Status, TM_STHWQT_Flag, TM_STHWQT_Order, TM_STHWQT_Redo_Flag, TM_STHWQT_RedoStatus', 'numerical', 'integerOnly'=>true),
			array('TM_STHWQT_Marks', 'numerical'),
			array('TM_STHWQT_Answer', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STHWQT_Id, TM_STHWQT_Student_Id, TM_STHWQT_Mock_Id, TM_STHWQT_Parent_Id, TM_STHWQT_Question_Id, TM_STHWQT_Type, TM_STHWQT_Answer, TM_STHWQT_Marks, TM_STHWQT_Status, TM_STHWQT_Flag, TM_STHWQT_Order, TM_STHWQT_Redo_Flag, TM_STHWQT_RedoStatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STHWQT_Id' => 'Tm Sthwqt',
			'TM_STHWQT_Student_Id' => 'Tm Sthwqt Student',
			'TM_STHWQT_Mock_Id' => 'Tm Sthwqt Mock',
			'TM_STHWQT_Parent_Id' => 'Tm Sthwqt Parent',
			'TM_STHWQT_Question_Id' => 'Tm Sthwqt Question',
			'TM_STHWQT_Type' => 'Tm Sthwqt Type',
			'TM_STHWQT_Answer' => 'Tm Sthwqt Answer',
			'TM_STHWQT_Marks' => 'Tm Sthwqt Marks',
			'TM_STHWQT_Status' => 'Tm Sthwqt Status',
			'TM_STHWQT_Flag' => 'Tm Sthwqt Flag',
			'TM_STHWQT_Order' => 'Tm Sthwqt Order',
			'TM_STHWQT_Redo_Flag' => 'Tm Sthwqt Redo Flag',
			'TM_STHWQT_RedoStatus' => 'Tm Sthwqt Redo Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STHWQT_Id',$this->TM_STHWQT_Id);
		$criteria->compare('TM_STHWQT_Student_Id',$this->TM_STHWQT_Student_Id);
		$criteria->compare('TM_STHWQT_Mock_Id',$this->TM_STHWQT_Mock_Id);
		$criteria->compare('TM_STHWQT_Parent_Id',$this->TM_STHWQT_Parent_Id);
		$criteria->compare('TM_STHWQT_Question_Id',$this->TM_STHWQT_Question_Id);
		$criteria->compare('TM_STHWQT_Type',$this->TM_STHWQT_Type);
		$criteria->compare('TM_STHWQT_Answer',$this->TM_STHWQT_Answer,true);
		$criteria->compare('TM_STHWQT_Marks',$this->TM_STHWQT_Marks);
		$criteria->compare('TM_STHWQT_Status',$this->TM_STHWQT_Status);
		$criteria->compare('TM_STHWQT_Flag',$this->TM_STHWQT_Flag);
		$criteria->compare('TM_STHWQT_Order',$this->TM_STHWQT_Order);
		$criteria->compare('TM_STHWQT_Redo_Flag',$this->TM_STHWQT_Redo_Flag);
		$criteria->compare('TM_STHWQT_RedoStatus',$this->TM_STHWQT_RedoStatus);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Studenthomeworkquestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
