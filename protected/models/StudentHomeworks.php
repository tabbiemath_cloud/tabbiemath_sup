<?php

/**
 * This is the model class for table "{{student_homeworks}}".
 *
 * The followings are the available columns in table '{{student_homeworks}}':
 * @property integer $TM_STHW_Id
 * @property integer $TM_STHW_Student_Id
 * @property integer $TM_STHW_HomeWork_Id
 * @property integer $TM_STHW_Plan_Id
 * @property double $TM_STHW_Marks
 * @property double $TM_STHW_TotalMarks
 * @property string $TM_STHW_Percentage
 * @property integer $TM_STHW_Status
 * @property string $TM_STHW_Date
 * @property string $TM_STHW_Redo_Action
 * @property string $TM_STHW_Assigned_On
 * @property integer $TM_STHW_Assigned_By
  * @property string $TM_STHW_DueDate
 * @property string $TM_STHW_Comments 
 * @property string $TM_STHW_PublishDate
 * @property string $TM_STHW_Type
 * @property string $TM_STHW_ShowSolution
 */
class StudentHomeworks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{student_homeworks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STHW_Student_Id, TM_STHW_HomeWork_Id, TM_STHW_Plan_Id, TM_STHW_Marks, TM_STHW_TotalMarks, TM_STHW_Percentage, TM_STHW_Status, TM_STHW_Date, TM_STHW_Redo_Action, TM_STHW_Assigned_On, TM_STHW_Assigned_By', 'required'),
			array('TM_STHW_Student_Id, TM_STHW_HomeWork_Id, TM_STHW_Plan_Id, TM_STHW_Status, TM_STHW_Assigned_By', 'numerical', 'integerOnly'=>true),
			array('TM_STHW_Marks, TM_STHW_TotalMarks', 'numerical'),
			array('TM_STHW_Percentage', 'length', 'max'=>10),
			array('TM_STHW_Redo_Action', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STHW_Id, TM_STHW_Student_Id, TM_STHW_HomeWork_Id, TM_STHW_Plan_Id, TM_STHW_Marks, TM_STHW_TotalMarks, TM_STHW_Percentage, TM_STHW_Status, TM_STHW_Date, TM_STHW_Redo_Action, TM_STHW_Assigned_On, TM_STHW_Assigned_By', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'homework'=>array(self::BELONGS_TO, 'Schoolhomework', 'TM_STHW_HomeWork_Id'),
            'student'=>array(self::BELONGS_TO, 'User', 'TM_STHW_Student_Id'),
            
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STHW_Id' => 'Tm Sthw',
			'TM_STHW_Student_Id' => 'Tm Sthw Student',
			'TM_STHW_HomeWork_Id' => 'Tm Sthw Home Work',
			'TM_STHW_Plan_Id' => 'Tm Sthw Plan',
			'TM_STHW_Marks' => 'Tm Sthw Marks',
			'TM_STHW_TotalMarks' => 'Tm Sthw Total Marks',
			'TM_STHW_Percentage' => 'Tm Sthw Percentage',
			'TM_STHW_Status' => 'Tm Sthw Status',
			'TM_STHW_Date' => 'Tm Sthw Date',
			'TM_STHW_Redo_Action' => 'Tm Sthw Redo Action',
			'TM_STHW_Assigned_On' => 'Tm Sthw Assigned On',
			'TM_STHW_Assigned_By' => 'Tm Sthw Assigned By',
			'TM_STHW_DueDate' => 'Due Date', 
            'TM_STHW_Comments' => 'Comments',                
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STHW_Id',$this->TM_STHW_Id);
		$criteria->compare('TM_STHW_Student_Id',$this->TM_STHW_Student_Id);
		$criteria->compare('TM_STHW_HomeWork_Id',$this->TM_STHW_HomeWork_Id);
		$criteria->compare('TM_STHW_Plan_Id',$this->TM_STHW_Plan_Id);
		$criteria->compare('TM_STHW_Marks',$this->TM_STHW_Marks);
		$criteria->compare('TM_STHW_TotalMarks',$this->TM_STHW_TotalMarks);
		$criteria->compare('TM_STHW_Percentage',$this->TM_STHW_Percentage,true);
		$criteria->compare('TM_STHW_Status',$this->TM_STHW_Status);
		$criteria->compare('TM_STHW_Date',$this->TM_STHW_Date,true);
		$criteria->compare('TM_STHW_Redo_Action',$this->TM_STHW_Redo_Action,true);
		$criteria->compare('TM_STHW_Assigned_On',$this->TM_STHW_Assigned_On,true);
		$criteria->compare('TM_STHW_Assigned_By',$this->TM_STHW_Assigned_By);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentHomeworks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
