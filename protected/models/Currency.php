<?php

/**
 * This is the model class for table "{{currency}}".
 *
 * The followings are the available columns in table '{{currency}}':
 * @property integer $TM_CR_Id
 * @property string $TM_CR_Name
 * @property string $TM_CR_Code
 * @property string $TM_CR_CreatedOn
 * @property integer $TM_CR_CreatedBy
 * @property integer $TM_CR_Status
 */
class Currency extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{currency}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CR_Name, TM_CR_Code,TM_CR_Status', 'required'),
			array('TM_CR_CreatedBy, TM_CR_Status', 'numerical', 'integerOnly'=>true),
			array('TM_CR_Name', 'length', 'max'=>200),
			array('TM_CR_Code', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CR_Id, TM_CR_Name, TM_CR_Code, TM_CR_CreatedOn, TM_CR_CreatedBy, TM_CR_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                'plan'=>array(self::HAS_MANY, 'Plan', 'TM_PN_Currency_Id'),
                'coupon'=>array(self::HAS_MANY, 'Coupons', 'TM_CP_Currency_Id'),                                 
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CR_Id' => 'Currency Id',
			'TM_CR_Name' => 'Currency',
			'TM_CR_Code' => 'Currency Code',
			'TM_CR_CreatedOn' => 'Created On',
			'TM_CR_CreatedBy' => 'Created By',
			'TM_CR_Status' => 'Status',
		);
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'CurrencyStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CR_Id',$this->TM_CR_Id);
		$criteria->compare('TM_CR_Name',$this->TM_CR_Name,true);
		$criteria->compare('TM_CR_Code',$this->TM_CR_Code,true);
		$criteria->compare('TM_CR_CreatedOn',$this->TM_CR_CreatedOn,true);
		$criteria->compare('TM_CR_CreatedBy',$this->TM_CR_CreatedBy);
		$criteria->compare('TM_CR_Status',$this->TM_CR_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Currency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
