<?php

/**
 * This is the model class for table "{{types}}".
 *
 * The followings are the available columns in table '{{types}}':
 * @property integer $TM_TP_Id
 * @property string $TM_TP_Name
 * @property string $TM_TP_CreatedOn
 * @property string $TM_TP_CreatedBy
 * @property integer $TM_TP_Status
 */
class Types extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{types}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_TP_Name', 'required'),
			array('TM_TP_Status', 'numerical', 'integerOnly'=>true),
			array('TM_TP_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_TP_Id, TM_TP_Name, TM_TP_Status, TM_TP_CreatedOn, TM_TP_CreatedBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'question'=>array(self::HAS_MANY, 'Questions', 'TM_QN_Type_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_TP_Id' => 'Tm Tp',
			'TM_TP_Name' => 'Name',
            'TM_TP_CreatedOn' => 'Created On',
            'TM_TP_CreatedBy' => 'Created By',
			'TM_TP_Status' => 'Status',
		);
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'TypesStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_TP_Id',$this->TM_TP_Id);
		$criteria->compare('TM_TP_Name',$this->TM_TP_Name,true);
		$criteria->compare('TM_TP_Status',$this->TM_TP_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Types the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
