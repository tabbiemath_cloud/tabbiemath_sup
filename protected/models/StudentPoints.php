<?php

/**
 * This is the model class for table "{{studentpoints}}".
 *
 * The followings are the available columns in table '{{studentpoints}}':
 * @property integer $TM_STP_Id
 * @property integer $TM_STP_Student_Id
 * @property string $TM_STP_Points
 * @property integer $TM_STP_Test_Id
 * @property string $TM_STP_Test_Type
 * @property integer $TM_STP_Standard_Id
 * @property string $TM_STP_Date
 */
class StudentPoints extends CActiveRecord
{
	public $totalpoints;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{studentpoints}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STP_Id, TM_STP_Student_Id, TM_STP_Points, TM_STP_Test_Id, TM_STP_Test_Type, TM_STP_Standard_Id, TM_STP_Date', 'required'),
			array('TM_STP_Id, TM_STP_Student_Id, TM_STP_Test_Id, TM_STP_Standard_Id', 'numerical', 'integerOnly'=>true),
			array('TM_STP_Points, TM_STP_Test_Type', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STP_Id, TM_STP_Student_Id, TM_STP_Points, TM_STP_Test_Id, TM_STP_Test_Type, TM_STP_Standard_Id, TM_STP_Date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'users'=>array(self::BELONGS_TO, 'User', 'TM_STP_Student_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STP_Id' => 'Tm Stp',
			'TM_STP_Student_Id' => 'Tm Stp Student',
			'TM_STP_Points' => 'Tm Stp Points',
			'TM_STP_Test_Id' => 'Tm Stp Test',
			'TM_STP_Test_Type' => 'Tm Stp Test Type',
			'TM_STP_Standard_Id' => 'Tm Stp Standard',
			'TM_STP_Date' => 'Tm Stp Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STP_Id',$this->TM_STP_Id);
		$criteria->compare('TM_STP_Student_Id',$this->TM_STP_Student_Id);
		$criteria->compare('TM_STP_Points',$this->TM_STP_Points,true);
		$criteria->compare('TM_STP_Test_Id',$this->TM_STP_Test_Id);
		$criteria->compare('TM_STP_Test_Type',$this->TM_STP_Test_Type,true);
		$criteria->compare('TM_STP_Standard_Id',$this->TM_STP_Standard_Id);
		$criteria->compare('TM_STP_Date',$this->TM_STP_Date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentPoints the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
