<?php

/**
 * This is the model class for table "{{chalangerecepient}}".
 *
 * The followings are the available columns in table '{{chalangerecepient}}':
 * @property integer $TM_CE_RE_Id
 * @property integer $TM_CE_RE_Chalange_Id
 * @property integer $TM_CE_RE_Recepient_Id
 * @property integer $TM_CE_RE_ObtainedMarks
 * @property integer $TM_CE_RE_Date
 * @property integer $TM_CE_RE_Status
 */
class Chalangerecepient extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{chalangerecepient}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CE_RE_Chalange_Id, TM_CE_RE_Recepient_Id, TM_CE_RE_Status', 'required'),
			array('TM_CE_RE_Chalange_Id, TM_CE_RE_Recepient_Id, TM_CE_RE_Status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CE_RE_Id, TM_CE_RE_Chalange_Id, TM_CE_RE_Recepient_Id, TM_CE_RE_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'challenge'=>array(self::BELONGS_TO, 'Challenges', 'TM_CE_RE_Chalange_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CE_RE_Id' => 'Tm Ce Re',
			'TM_CE_RE_Chalange_Id' => 'Tm Ce Re Chalange',
			'TM_CE_RE_Recepient_Id' => 'Tm Ce Re Recepient',
			'TM_CE_RE_Status' => 'Tm Ce Re Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CE_RE_Id',$this->TM_CE_RE_Id);
		$criteria->compare('TM_CE_RE_Chalange_Id',$this->TM_CE_RE_Chalange_Id);
		$criteria->compare('TM_CE_RE_Recepient_Id',$this->TM_CE_RE_Recepient_Id);
		$criteria->compare('TM_CE_RE_Status',$this->TM_CE_RE_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Chalangerecepient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
