<?php

/**
 * This is the model class for table "image_tag".
 *
 * The followings are the available columns in table 'image_tag':
 * @property integer $id
 * @property integer $pic_id
 * @property string $comments
 * @property integer $pic_x
 * @property integer $pic_y
 * @property string $image
 */
class Imagetag extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'image_tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pic_id, comments, pic_x, pic_y, image', 'required'),
			array('pic_id, pic_x, pic_y', 'numerical', 'integerOnly'=>true),
			array('comments', 'length', 'max'=>255),
			array('image', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pic_id, comments, pic_x, pic_y, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pic_id' => 'Pic',
			'comments' => 'Comments',
			'pic_x' => 'Pic X',
			'pic_y' => 'Pic Y',
			'image' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pic_id',$this->pic_id);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('pic_x',$this->pic_x);
		$criteria->compare('pic_y',$this->pic_y);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Imagetag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
