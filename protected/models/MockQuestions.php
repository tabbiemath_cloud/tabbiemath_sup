<?php

/**
 * This is the model class for table "{{mock_questions}}".
 *
 * The followings are the available columns in table '{{mock_questions}}':
 * @property integer $TM_MQ_Id
 * @property integer $TM_MQ_Mock_Id
 * @property integer $TM_MQ_Question_Id
 * @property integer $TM_MQ_Type
 * @property integer $TM_MQ_Order 
 */
class MockQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{mock_questions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_MQ_Mock_Id, TM_MQ_Question_Id', 'required'),
			array('TM_MQ_Mock_Id, TM_MQ_Question_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_MQ_Id, TM_MQ_Mock_Id, TM_MQ_Question_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_MQ_Id' => 'Tm Mq',
			'TM_MQ_Mock_Id' => 'Tm Mq Mock',
			'TM_MQ_Question_Id' => 'Tm Mq Question',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_MQ_Id',$this->TM_MQ_Id);
		$criteria->compare('TM_MQ_Mock_Id',$this->TM_MQ_Mock_Id);
		$criteria->compare('TM_MQ_Question_Id',$this->TM_MQ_Question_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MockQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
