<?php

/**
 * This is the model class for table "{{section}}".
 *
 * The followings are the available columns in table '{{section}}':
 * @property integer $TM_SN_Id
 * @property string $TM_SN_Name
 * @property integer $TM_SN_Syllabus_Id
 * @property integer $TM_SN_Standard_Id
 * @property integer $TM_SN_Topic_Id
 * @property string $TM_SN_CreatedOn
 * @property integer $TM_SN_CreatedBy
 * @property integer $TM_SN_Status
 * @property integer $TM_SN_order  
 */
class Topic extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{section}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SN_Name, TM_SN_Topic_Id,TM_SN_Syllabus_Id,TM_SN_Standard_Id', 'required'),
			array('TM_SN_Topic_Id, TM_SN_CreatedBy, TM_SN_Status', 'numerical', 'integerOnly'=>true),
			array('TM_SN_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SN_Id, TM_SN_Name, TM_SN_Topic_Id, TM_SN_CreatedOn, TM_SN_CreatedBy, TM_SN_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'chapter'=>array(self::BELONGS_TO, 'Chapter', 'TM_SN_Topic_Id'),
            'question'=>array(self::HAS_MANY, 'Questions', 'TM_QN_Section_Id'),
            'StudentChapter'=>array(self::HAS_MANY, 'StudentTestChapters', 'TM_STC_Chapter_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SN_Id' => 'Topic Id',
			'TM_SN_Syllabus_Id' => 'Syllabus',
			'TM_SN_Standard_Id' => 'Standard',
			'TM_SN_Name' => 'Topic',
			'TM_SN_Topic_Id' => 'Chapter',
			'TM_SN_CreatedOn' => 'Created On',
			'TM_SN_CreatedBy' => 'Created By',
			'TM_SN_Status' => 'Status',
		);
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'TopicStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    public function GetChapter($chapterid)
    {
        if($chapterid!='0')
            $chapter=Chapter::model()->findByPk($chapterid)->TM_TP_Name;
        else
            $chapter='';

        return $chapter;
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SN_Id',$this->TM_SN_Id);
		$criteria->compare('TM_SN_Name',$this->TM_SN_Name,true);
		$criteria->compare('TM_SN_Topic_Id',$this->TM_SN_Topic_Id);
		$criteria->compare('TM_SN_Syllabus_Id',$this->TM_SN_Syllabus_Id);
		$criteria->compare('TM_SN_Standard_Id',$this->TM_SN_Standard_Id);
		$criteria->compare('TM_SN_CreatedOn',$this->TM_SN_CreatedOn,true);
		$criteria->compare('TM_SN_CreatedBy',$this->TM_SN_CreatedBy);
		$criteria->compare('TM_SN_Status',$this->TM_SN_Status);
		$criteria->compare('TM_SN_order',$this->TM_SN_order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'TM_SN_Topic_Id,TM_SN_order asc',
            ),
            'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sections the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
