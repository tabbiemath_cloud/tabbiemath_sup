<?php

/**
 * This is the model class for table "{{homeworkmarking}}".
 *
 * The followings are the available columns in table '{{homeworkmarking}}':
 * @property integer $TM_HWM_Id
 * @property integer $TM_HWM_Homework_Id
 * @property integer $TM_HWM_User_Id
 * @property string $TM_HWM_Assigned_On
 * @property string $TM_HWM_Assigned_By
 * @property string $TM_HWM_Status
 */
class Homeworkmarking extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{homeworkmarking}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_HWM_Homework_Id, TM_HWM_User_Id, TM_HWM_Assigned_On', 'required'),
			array('TM_HWM_Homework_Id, TM_HWM_User_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_HWM_Id, TM_HWM_Homework_Id, TM_HWM_User_Id, TM_HWM_Assigned_On', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
        'homework'=>array(self::BELONGS_TO, 'Schoolhomework', 'TM_HWM_Homework_Id'), 
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_HWM_Id' => 'Tm Hwm',
			'TM_HWM_Homework_Id' => 'Tm Hwm Homework',
			'TM_HWM_User_Id' => 'Tm Hwm User',
			'TM_HWM_Assigned_On' => 'Assigned On',
            'TM_HWM_Assigned_By'=>'Assigned By'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id,$status)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_HWM_Id',$this->TM_HWM_Id);
		$criteria->compare('TM_HWM_Homework_Id',$this->TM_HWM_Homework_Id);
		$criteria->compare('TM_HWM_User_Id',$id);
		$criteria->compare('TM_HWM_Assigned_On',$this->TM_HWM_Assigned_On,true);
        $criteria->compare('TM_HWM_Status',$status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function checkAssignes($id)
    {

        $hwqstns=Homeworkmarking::model()->count(array('condition' => "TM_HWM_Homework_Id='".$id."'"));
        
        if($hwqstns==0):                     
            return CHtml::link("<span class='glyphicon glyphicon-user'></span>","javascript:void(0)",array(
                            "title"=>"Assign",                            
                            "class"=>"choosegroups",
                            "data-backdrop"=>"static",
                            "data-toggle"=>"modal",
                            "data-target"=>"#SelectGroup",
                            "data-value"=>"$id"
                            ));
        else:            
            $users=User::model()->findAll(array('join'=>'LEFT JOIN tm_homeworkmarking AS b ON TM_HWM_User_Id=t.id','condition'=>"usertype IN (11,6) AND school_id=".Yii::app()->session['school']." AND status=1 AND b.TM_HWM_Homework_Id=".$id));
            $returnvalue='';
            foreach($users AS $key=>$user):
                    $returnvalue.=($key==0?$user->profile->firstname.' '.$user->profile->lastname:','.$user->profile->firstname.' '.$user->profile->lastname);
            endforeach;              
             
            return $returnvalue;
        endif;                            

    }
    public function GetCount()
    {
        $hwqstns=Homeworkmarking::model()->with('homework')->count(array('condition' => "TM_HWM_User_Id='".Yii::app()->user->id."' AND TM_SCH_Status!='2'"));
        return $hwqstns; 
    }
    public function checkAssignesStatus($id)
    {
        $startedstudents=StudentHomeworks::model()->count(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        //$notstartedstudents=StudentHomeworks::model()->count(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
		$notstartedstudents=StudentHomeworks::model()->count(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS users ON users.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5) AND users.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $status="Completed : ".$startedstudents." <br> Pending : ".$notstartedstudents;                        
        return $status;        
    }
    public function checkCompleteStatus($id)
    {
        $startedstudents=StudentHomeworks::model()->count(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        /*$notstartedstudents=StudentHomeworks::model()->count(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $status="Completed : ".$startedstudents." <br> Pending : ".$notstartedstudents; */                      
        if($startedstudents==0):
            return true;
        else:
            return false;
        endif;
    }    
    public function getAssignee($id)
    {
      $user=User::model()->findByPk($id);
      return $user->profile->firstname.' '.$user->profile->lastname;   
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Homeworkmarking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
