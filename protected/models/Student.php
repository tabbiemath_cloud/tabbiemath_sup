<?php

/**
 * This is the model class for table "{{student}}".
 *
 * The followings are the available columns in table '{{student}}':
 * @property integer $TM_STU_Id
 * @property integer $TM_STU_Parent_Id
 * @property integer $TM_STU_User_Id
 * @property string $TM_STU_First_Name 
 * @property string $TM_STU_Last_Name
 * @property string $TM_STU_Password
 * @property string $TM_STU_School
 * @property string $TM_STU_SchoolCustomname
 * @property string $TM_STU_City
 * @property string $TM_STU_State
 * @property integer $TM_STU_Country_Id
 * @property string $TM_STU_CreatedOn
 * @property integer $TM_STU_CreatedBy
 * @property integer $TM_STU_UpdatedBy
 * @property string $TM_STU_UpdatedOn
 * @property integer $TM_STU_Status
 * @property integer $TM_STU_School_Id
 * @property integer $TM_STU_Email
 * @property integer $TM_STU_Email_Status
 * @property string $TM_STU_Parentpass
 * @property string $TM_STU_Email_Subscription 
 * @property string $TM_STU_School_code
 */
class Student extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{student}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STU_City, TM_STU_State', 'safe', 'on'=>'school'),
			array('TM_STU_First_Name, TM_STU_Last_Name, TM_STU_School, TM_STU_Password, TM_STU_Country_Id, TM_STU_student_Id', 'required'),
            array('TM_STU_Email', 'required','on'=>'school'),
			array('TM_STU_Country_Id, TM_STU_CreatedBy, TM_STU_Status,TM_STU_School_Id', 'numerical', 'integerOnly'=>true),
            array('TM_STU_Email', 'email'),
            array('TM_STU_Parentpass,TM_STU_Email_Subscription', 'safe'),
            array('TM_STU_School', 'schoolvalid'),
            array('TM_STU_School_code', 'schoolcodevalid','on'=>'registerstudent'),
            array('TM_STU_student_Id', 'usernamevalid'),
			array('TM_STU_First_Name, TM_STU_Last_Name', 'length', 'max'=>125),
			array('TM_STU_SchoolCustomname, TM_STU_City, TM_STU_State', 'length', 'max'=>100),            
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STU_Id, TM_STU_Parent_Id, TM_STU_First_Name, TM_STU_Last_Name, TM_STU_School, TM_STU_City, TM_STU_State, TM_STU_Country_Id, TM_STU_CreatedOn, TM_STU_CreatedBy, TM_STU_Status,TM_STU_School_Id,TM_STU_Email,TM_STU_School_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user'=>array(self::BELONGS_TO, 'User', 'TM_STU_User_Id'),
            'plan'=>array(self::HAS_MANY, 'StudentPlan', 'TM_SPN_StudentId'),
            'parent'=>array(self::BELONGS_TO, 'User', 'TM_STU_Parent_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STU_Id' => 'Tm Student',
			'TM_STU_student_Id' => 'Username',
			'TM_STU_Parent_Id' => 'Tm Stu Parent',
			'TM_STU_First_Name' => 'First Name',
			'TM_STU_Last_Name' => 'Last Name',
			'TM_STU_Password' => 'Password',
			'TM_STU_School' => 'School',
			'TM_STU_City' => 'City',
			'TM_STU_State' => 'State', 
			'TM_STU_Country_Id' => 'Country',
			'TM_STU_CreatedOn' => 'Created On',
			'TM_STU_CreatedBy' => 'Created By',
			'TM_STU_Status' => 'Status',
			'TM_STU_School_Id' => 'School Id',
			'TM_STU_Email' => 'Parent Email',
            'TM_STU_Email_Status'=>'Email Status',
            'TM_STU_SchoolCustomname'=>'School Name'
		);
	}

	public function usernamevalid()
	{
		if($this->TM_STU_student_Id!='')
		{
			if(preg_match('/[^A-Za-z0-9.@]/', $this->TM_STU_student_Id)) {
				$this->addError('TM_STU_student_Id','Username allows only letters,numbers,@ and "." ');
			}
		}
	}
    public function schoolvalid()
    {
      if($this->TM_STU_School=='0' & $this->TM_STU_SchoolCustomname=='')
      {
        $this->addError('TM_STU_SchoolCustomname','School Name cannot be blank');
      } 
    }
	public function schoolcodevalid()
	{
		if($this->TM_STU_School!='none')
		{
			$school=School::model()->find(array('condition'=>'TM_SCL_Code LIKE "'.$this->TM_STU_School_code.'" AND TM_SCL_Id='.$this->TM_STU_School.' '));
			if(count($school)==0 || $this->TM_STU_School_code=='')
			{
				$this->addError('TM_STU_School_code','The code entered is incorrect. Please check with your teacher and enter the correct code');
			}
		}

	}
	// public function isexist() {
	// 	if($this->TM_STU_student_Id!='none') 
	// 	{ 
	// 		$parent=User::model()->find(array('condition'=>'email="'.$this->pa.'" AND usertype=7'));
	// 		$user=User::model()->find(array('condition'=>'username="'.$this->TM_STU_student_Id.'" AND usertype=6'));
	// 		if(count($user) > 0 && count($parent > 0)) 
	// 		{
	// 			$userExists = Student::model()->find(array('condition'=>'TM_STU_Parent_Id="'.$parent->id.'" AND TM_STU_User_Id="'.$user->id.'" '));
	// 			if(count($userExists) > 0)
	// 				return true;
	// 			else
	// 				$this->addError('TM_STU_student_Id','Username already exists');
	// 		} 
	// 		elseif(count($user) > 0)
	// 			$this->addError('TM_STU_student_Id','already exists');				
	// 		 else			
	// 		 	return true;

	// 	}
	// }
    public static function itemAlias($type,$code=NULL) {
        /*$_items = array(

            'EmailStatus' => array(
                '0' => ('Email Sent'),
                '1' => ('Email Not Sent'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
                 return isset($_items[$type]) ? $_items[$type] : false;*/
        echo $code;                 
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STU_Id',$this->TM_STU_Id);
		$criteria->compare('TM_STU_Parent_Id',$this->TM_STU_Parent_Id);
		$criteria->compare('TM_STU_First_Name',$this->TM_STU_First_Name,true);
		$criteria->compare('TM_STU_Last_Name',$this->TM_STU_Last_Name,true);
		$criteria->compare('TM_STU_School',$this->TM_STU_School,true);
		$criteria->compare('TM_STU_City',$this->TM_STU_City,true);
		$criteria->compare('TM_STU_State',$this->TM_STU_State,true);
		$criteria->compare('TM_STU_Country_Id',$this->TM_STU_Country_Id);
		$criteria->compare('TM_STU_CreatedOn',$this->TM_STU_CreatedOn,true);
		$criteria->compare('TM_STU_CreatedBy',$this->TM_STU_CreatedBy);
		$criteria->compare('TM_STU_Status',$this->TM_STU_Status);
		$criteria->compare('TM_STU_School_Id',$this->TM_STU_School_Id);
		$criteria->compare('TM_STU_Email',$this->TM_STU_Email);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}
    public function GetTestStatus($test)
    {
        $count = StudentQuestions::model()->count(
            array(
                'condition'=>'TM_STU_QN_Flag=:flag AND TM_STU_QN_Test_Id=:test',
                'params'=>array(':flag'=>'1',':test'=>$test)
            ));
        return $count;
    }
    public function GetChallengeStatus($challenge,$user)
    {
        $count = Chalangeuserquestions::model()->count(
            array(
                'condition'=>'TM_CEUQ_Challenge=:challenge AND TM_CEUQ_User_Id=:user',
                'params'=>array(':challenge'=>$challenge,':user'=>$user)
            ));
        return $count;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Student the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
