<?php

/**
 * This is the model class for table "{{answer}}".
 *
 * The followings are the available columns in table '{{answer}}':
 * @property integer $TM_AR_Id
 * @property integer $TM_AR_Question_Id
 * @property string $TM_AR_Answer
 * @property integer $TM_AR_Correct
 * @property string $TM_AR_Image
 * @property integer $TM_AR_Marks
 * @property string $TM_AR_CreatedOn
 * @property integer $TM_AR_CreatedBy
 * @property integer $TM_AR_Status
 */
class Answers extends CActiveRecord
{
	public $totalmarks;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{answer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_AR_Question_Id, TM_AR_Answer, TM_AR_Correct, TM_AR_Image, TM_AR_Marks, TM_AR_CreatedOn, TM_AR_CreatedBy, TM_AR_Status', 'required'),
			array('TM_AR_Question_Id, TM_AR_Correct, TM_AR_Marks, TM_AR_CreatedBy, TM_AR_Status', 'numerical', 'integerOnly'=>true),
			array('TM_AR_Image', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_AR_Id, TM_AR_Question_Id, TM_AR_Answer, TM_AR_Correct, TM_AR_Image, TM_AR_Marks, TM_AR_CreatedOn, TM_AR_CreatedBy, TM_AR_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'question'=>array(self::BELONGS_TO, 'Questions', 'TM_AR_Question_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_AR_Id' => 'Tm Ar',
			'TM_AR_Question_Id' => 'Tm Ar Question',
			'TM_AR_Answer' => 'Tm Ar Answer',
			'TM_AR_Correct' => 'Tm Ar Correct',
			'TM_AR_Image' => 'Tm Ar Image',
			'TM_AR_Marks' => 'Tm Ar Marks',
			'TM_AR_CreatedOn' => 'Tm Ar Created On',
			'TM_AR_CreatedBy' => 'Tm Ar Created By',
			'TM_AR_Status' => 'Tm Ar Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_AR_Id',$this->TM_AR_Id);
		$criteria->compare('TM_AR_Question_Id',$this->TM_AR_Question_Id);
		$criteria->compare('TM_AR_Answer',$this->TM_AR_Answer,true);
		$criteria->compare('TM_AR_Correct',$this->TM_AR_Correct);
		$criteria->compare('TM_AR_Image',$this->TM_AR_Image,true);
		$criteria->compare('TM_AR_Marks',$this->TM_AR_Marks);
		$criteria->compare('TM_AR_CreatedOn',$this->TM_AR_CreatedOn,true);
		$criteria->compare('TM_AR_CreatedBy',$this->TM_AR_CreatedBy);
		$criteria->compare('TM_AR_Status',$this->TM_AR_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Answers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
