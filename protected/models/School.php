<?php

/**
 * This is the model class for table "{{school}}".
 *
 * The followings are the available columns in table '{{school}}':
 * @property integer $TM_SCL_Id
 * @property integer $TM_SCL_Sylllabus_Id
 * @property string $TM_SCL_Name
 * @property string $TM_SCL_Address
 * @property string $TM_SCL_Contactperson
 * @property string $TM_SCL_Contactdesignation
 * @property string $TM_SCL_Email
 * @property string $TM_SCL_Phone
 * @property string $TM_SCL_Mobile
 * @property string $TM_SCL_City
 * @property string $TM_SCL_State
 * @property string $TM_SCL_Country_Id
 * @property string $TM_SCL_Logo
 * @property string $TM_SCL_Headteacher
 * @property string $TM_SCL_Billing_Reference
 * @property string $TM_SCL_Billing_Contact
 * @property string $TM_SCL_Billing_Contact_Email
 * @property string $TM_SCL_Billing_Notes  
 * @property string $TM_SCL_CreatedOn
 * @property integer $TM_SCL_CreatedBy
 * @property integer $TM_SCL_Discount
 * @property integer $TM_SCL_Discount_Amount
 * @property integer $TM_SCL_Discount_Type
 */  
class School extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{school}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            //array('TM_SCL_Sylllabus_Id, TM_SCL_Name, TM_SCL_Address, TM_SCL_Contactperson, TM_SCL_Email, TM_SCL_Phone, TM_SCL_City, TM_SCL_State, TM_SCL_Country_Id', 'required'),
            array('TM_SCL_Sylllabus_Id, TM_SCL_Name,TM_SCL_Country_Id,TM_SCL_City,TM_SCL_State', 'required'),
            array('TM_SCL_Email,TM_SCL_Billing_Contact_Email','email'),
			array('TM_SCL_Sylllabus_Id, TM_SCL_CreatedBy,TM_SCL_Mobile,TM_SCL_Phone', 'numerical', 'integerOnly'=>true),
			array('TM_SCL_Name, TM_SCL_Contactperson, TM_SCL_Contactdesignation, TM_SCL_Email, TM_SCL_City, TM_SCL_State, TM_SCL_Country_Id, TM_SCL_Headteacher', 'length', 'max'=>250),
			array('TM_SCL_Phone, TM_SCL_Mobile', 'length', 'min'=>10,'max'=>10),
			array('TM_SCL_Logo', 'length', 'max'=>50),            
            //array('TM_SCL_Image', 'filevalid','on'=>'create'), 
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SCL_Id, TM_SCL_Sylllabus_Id, TM_SCL_Name, TM_SCL_Address, TM_SCL_Contactperson, TM_SCL_Contactdesignation, TM_SCL_Email, TM_SCL_Phone, TM_SCL_Mobile, TM_SCL_City, TM_SCL_State, TM_SCL_Country_Id, TM_SCL_Logo, TM_SCL_Headteacher, TM_SCL_CreatedOn, TM_SCL_CreatedBy,TM_SCL_Billing_Reference,TM_SCL_Billing_Contact,TM_SCL_Billing_Contact_Email, TM_SCL_Billing_Notes', 'safe', 'on'=>'search'),
			array('TM_SCL_Id, TM_SCL_Sylllabus_Id, TM_SCL_Name, TM_SCL_Address, TM_SCL_Contactperson, TM_SCL_Contactdesignation, TM_SCL_Email, TM_SCL_Phone, TM_SCL_Mobile, TM_SCL_City, TM_SCL_State, TM_SCL_Country_Id, TM_SCL_Logo, TM_SCL_Headteacher, TM_SCL_CreatedOn, TM_SCL_CreatedBy,TM_SCL_Billing_Reference,TM_SCL_Billing_Contact,TM_SCL_Billing_Contact_Email, TM_SCL_Billing_Notes,TM_SCL_Discount,TM_SCL_Discount_Amount,TM_SCL_Discount_Type,TM_SCL_Code',  'safe'),
		);
	}



	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SCL_Id' => 'Id',
			'TM_SCL_Sylllabus_Id' => 'Sylllabus',
			'TM_SCL_Name' => 'Name of Shool',
			'TM_SCL_Address' => 'Address',
			'TM_SCL_Contactperson' => 'Contact Person',
			'TM_SCL_Contactdesignation' => 'Contact Person Designation',
			'TM_SCL_Email' => 'Email',
			'TM_SCL_Phone' => 'Phone',
			'TM_SCL_Mobile' => 'Mobile',
			'TM_SCL_City' => 'City',
			'TM_SCL_State' => 'State',
			'TM_SCL_Country_Id' => 'Country',
			'TM_SCL_Logo' => 'Logo',
			'TM_SCL_Headteacher' => 'Head Teacher',
			'TM_SCL_CreatedOn' => 'Created On',
			'TM_SCL_CreatedBy' => 'Created By',
			'TM_SCL_Billing_Reference' => 'Billing Reference',
			'TM_SCL_Billing_Contact' => 'Billing Contact',
			'TM_SCL_Billing_Contact_Email' => 'Billing Contact Email',
            'TM_SCL_Billing_Notes' => 'Billing Notes', 
            'TM_SCL_Discount'=>'Discount',
            'TM_SCL_Discount_Amount'=>'Discount Value',
            'TM_SCL_Discount_Type'=>'Discount Type',
			'TM_SCL_Code'=>'School Code',
			'TM_SCL_Image'=>'Custom Header',
			
			
		);
	}

		public function filevalid()
    {
        if($this->TM_SCL_Image_Status==1)
        { 
        	//echo $this->TM_SCL_Image; exit;
            if($this->TM_SCL_Image =='')
            { 

                $this->addError('TM_SCL_Image','Please select the image.');
            }
        }

    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
    public function GetAdminSchools()
    {
        $adminschools=Adminschools::model()->findAll(array('condition'=>'TM_SA_Admin_Id='.Yii::app()->user->id));
        $assignedschools='';
        foreach($adminschools AS $key=>$schools):
            $assignedschools.=($key==0?'':',').$schools->TM_SA_School_Id;
        endforeach;
        return $assignedschools;
    }
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        if(Yii::app()->user->isAccountAdmin()):
            $schools=$this->GetAdminSchools();
            $criteria->addCondition('TM_SCL_Id IN('.$schools.')');
        else:
            $criteria->compare('TM_SCL_Id',$this->TM_SCL_Id);
        endif;
		$criteria->compare('TM_SCL_Sylllabus_Id',$this->TM_SCL_Sylllabus_Id);
		$criteria->compare('TM_SCL_Name',$this->TM_SCL_Name,true);
		$criteria->compare('TM_SCL_Address',$this->TM_SCL_Address,true);
		$criteria->compare('TM_SCL_Contactperson',$this->TM_SCL_Contactperson,true);
		$criteria->compare('TM_SCL_Contactdesignation',$this->TM_SCL_Contactdesignation,true);
		$criteria->compare('TM_SCL_Email',$this->TM_SCL_Email,true);
		$criteria->compare('TM_SCL_Phone',$this->TM_SCL_Phone,true);
		$criteria->compare('TM_SCL_Mobile',$this->TM_SCL_Mobile,true);
		$criteria->compare('TM_SCL_City',$this->TM_SCL_City,true);
		$criteria->compare('TM_SCL_State',$this->TM_SCL_State,true);
		$criteria->compare('TM_SCL_Country_Id',$this->TM_SCL_Country_Id,true);
		$criteria->compare('TM_SCL_Logo',$this->TM_SCL_Logo,true);
		$criteria->compare('TM_SCL_Headteacher',$this->TM_SCL_Headteacher,true);
		$criteria->compare('TM_SCL_CreatedOn',$this->TM_SCL_CreatedOn,true);
		$criteria->compare('TM_SCL_CreatedBy',$this->TM_SCL_CreatedBy);
		$criteria->compare('TM_SCL_Billing_Reference',$this->TM_SCL_Billing_Reference);
		$criteria->compare('TM_SCL_Billing_Contact',$this->TM_SCL_Billing_Contact);
		$criteria->compare('TM_SCL_Billing_Contact_Email',$this->TM_SCL_Billing_Contact_Email);
		$criteria->compare('TM_SCL_Billing_Notes',$this->TM_SCL_Billing_Notes);		                                        
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>100,
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return School the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
