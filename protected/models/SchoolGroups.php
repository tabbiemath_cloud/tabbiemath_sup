<?php

/**
 * This is the model class for table "{{school_groups}}".
 *
 * The followings are the available columns in table '{{school_groups}}':
 * @property integer $TM_SCL_GRP_Id
 * @property string $TM_SCL_GRP_Name
 * @property integer $TM_SCL_SD_Id
 * @property integer $TM_SCL_Id
 * @property integer $TM_SCL_PN_Id
 */
class SchoolGroups extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{school_groups}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('TM_SCL_GRP_Name, TM_SCL_SD_Id ', 'required'),
            array('TM_SCL_SD_Id, TM_SCL_Id, TM_SCL_PN_Id', 'numerical', 'integerOnly' => true),
            array('TM_SCL_GRP_Name', 'length', 'max' => 250),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('TM_SCL_GRP_Id, TM_SCL_GRP_Name, TM_SCL_SD_Id, TM_SCL_Id, TM_SCL_PN_Id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'TM_SCL_GRP_Id' => 'Tm Scl Grp',
            'TM_SCL_GRP_Name' => 'Group Name',
            'TM_SCL_SD_Id' => 'Standard',
            'TM_SCL_Id' => 'School',
            'TM_SCL_PN_Id' => 'Plan',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($id)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('TM_SCL_GRP_Id', $this->TM_SCL_GRP_Id);
        $criteria->compare('TM_SCL_GRP_Name', $this->TM_SCL_GRP_Name, true);
        $criteria->compare('TM_SCL_SD_Id', $this->TM_SCL_SD_Id);
        $criteria->compare('TM_SCL_Id', $id);
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->session['mode']=='TeacherAdmin'):
            $criteria->compare('TM_SCL_PN_Id', $this->TM_SCL_PN_Id);
        elseif(Yii::app()->user->isTeacher()):
            $criteria->compare('TM_SCL_PN_Id', $this->GetPlanId($id));
        endif;        
        

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function GetPlanId($id)
    {
        if (Yii::app()->session['standard'] != ''):
            $plan = SchoolPlan::model()->find(array('condition' => 'TM_SPN_StandardId=' . Yii::app()->session['standard'] . ' AND TM_SPN_SchoolId=' . Yii::app()->session['school'].' AND TM_SPN_Status=0'));
        else:
            $plan = SchoolPlan::model()->find(array('condition' => ' TM_SPN_SchoolId=' . $id.' AND TM_SPN_Status=0'));
        endif;

        return $plan->TM_SPN_PlanId;
    }

    public static function StandardName($id)
    {
        $standard = Standard::model()->findByPk($id);
        return $standard['TM_SD_Name'];
    }

    public static function SchoolName($id)
    {
        $school = School::model()->findByPk($id);
        return $school['TM_SCL_Name'];
    }

    public static function PlanName($id)
    {
        $plan = Plan::model()->findByPk($id);
        return $plan['TM_PN_Name'];
    }
    public function GetMangeLink($id,$grp)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isAdmin()):
            $html = "<a href='" . Yii::app()->createUrl('SchoolGroups/addstudents', array('id' => $id,'group'=>$grp)) . "' class='btn btn-warning pull-right' style='padding: 3px 15px;'>Manage Students</a>";
        else:
            $html = "<a href='" . Yii::app()->createUrl('SchoolGroups/addstudents', array('id' => $id,'group'=>$grp)) . "' class='btn btn-warning pull-right'>Manage Students</a>";
        endif;
        return $html;
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SchoolGroups the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
