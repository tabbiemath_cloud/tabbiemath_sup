<?php

/**
 * This is the model class for table "{{student_test_chapters}}".
 *
 * The followings are the available columns in table '{{student_test_chapters}}':
 * @property integer $TM_STC_Id
 * @property integer $TM_STC_Student_Id
 * @property integer $TM_STC_Test_Id
 * @property integer $TM_STC_Chapter_Id
 */
class StudentTestChapters extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{student_test_chapters}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STC_Student_Id, TM_STC_Test_Id, TM_STC_Chapter_Id', 'required'),
			array('TM_STC_Student_Id, TM_STC_Test_Id, TM_STC_Chapter_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STC_Id, TM_STC_Student_Id, TM_STC_Test_Id, TM_STC_Chapter_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'chapters'=>array(self::BELONGS_TO, 'Chapter', 'TM_STC_Chapter_Id'),
            'test'=>array(self::BELONGS_TO, 'StudentTest', 'TM_STC_Chapter_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STC_Id' => 'Tm Stc',
			'TM_STC_Student_Id' => 'Tm Stc Student',
			'TM_STC_Test_Id' => 'Tm Stc Test',
			'TM_STC_Chapter_Id' => 'Tm Stc Chapter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STC_Id',$this->TM_STC_Id);
		$criteria->compare('TM_STC_Student_Id',$this->TM_STC_Student_Id);
		$criteria->compare('TM_STC_Test_Id',$this->TM_STC_Test_Id);
		$criteria->compare('TM_STC_Chapter_Id',$this->TM_STC_Chapter_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentTestChapters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
