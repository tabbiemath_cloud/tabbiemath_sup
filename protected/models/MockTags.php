<?php

/**
 * This is the model class for table "{{mock_tags}}".
 *
 * The followings are the available columns in table '{{mock_tags}}':
 * @property integer $TM_MT_Id
 * @property integer $TM_MT_Standard_Id
 * @property string $TM_MT_Tags
 * @property integer $TM_MT_Status
 * @property integer $TM_MT_Type
 * @property integer $TM_MT_Syllabus_Id
 */
class MockTags extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{mock_tags}}';
	}
	public function behaviors() {
		return array(
				'ERememberFiltersBehavior' => array(
						'class' => 'application.components.ERememberFiltersBehavior',
						'defaults'=>array(),           /* optional line */
						'defaultStickOnClear'=>false   /* optional line */
				),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_MT_Standard_Id, TM_MT_Tags', 'required'),
			array('TM_MT_Standard_Id, TM_MT_Status', 'numerical', 'integerOnly'=>true),
			//array('TM_MT_Tags', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_MT_Id, TM_MT_Standard_Id, TM_MT_Tags, TM_MT_Status,TM_MT_Type,TM_MT_Syllabus_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_MT_Id' => 'ID',
			'TM_MT_Syllabus_Id' => 'Syllabus',
			'TM_MT_Standard_Id' => 'Standard',
			'TM_MT_Tags' => 'Tags',
			'TM_MT_Status' => 'Status',
			'TM_MT_Type' => 'Type',
		);
	}

	public static function itemAlias($type,$code=NULL) {
		$_items = array(

				'MockStatus' => array(
						'0' => ('Active'),
						'1' => ('Inactive'),
				),
				'MockResource' => array(
						'0' => ('Worksheet'),
						'1' => ('Resource'),
				),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_MT_Id',$this->TM_MT_Id);
		$criteria->compare('TM_MT_Syllabus_Id',$this->TM_MT_Syllabus_Id);
		$criteria->compare('TM_MT_Standard_Id',$this->TM_MT_Standard_Id);
		$criteria->compare('TM_MT_Tags',$this->TM_MT_Tags,true);
		$criteria->compare('TM_MT_Status',$this->TM_MT_Status);
		$criteria->compare('TM_MT_Type',$this->TM_MT_Type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,'pagination'=>array(
						'pageSize'=>30,
				),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MockTags the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
