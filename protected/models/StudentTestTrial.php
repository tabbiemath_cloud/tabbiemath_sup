<?php

/**
 * This is the model class for table "{{student_test}}".
 *
 * The followings are the available columns in table '{{student_test}}':
 * @property integer $TM_TR_STU_TT_Id
 * @property integer $TM_TR_STU_TT_Publisher_Id
 * @property integer $TM_TR_STU_TT_Student_Id
 * @property integer $TM_TR_STU_TT_Type
 * @property integer $TM_TR_STU_TT_Plan
 * @property integer $TM_TR_STU_TT_Mode
 * @property string $TM_TR_STU_TT_CreateOn
 * @property integer $TM_TR_STU_TT_Status
 * @property string $TM_TR_STU_TT_Mark
 * @property string $TM_TR_STU_TT_TotalMarks
 * @property string $TM_TR_STU_TT_Percentage
 * @property integer $TM_TR_STU_TT_Redo_Action
 */
class StudentTestTrial extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{student_test_trial}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_TR_STU_TT_Publisher_Id, TM_TR_STU_TT_Student_Id, TM_TR_STU_TT_Type, TM_TR_STU_TT_Mode, TM_TR_STU_TT_CreateOn, TM_TR_STU_TT_Status, TM_TR_STU_TT_Mark, TM_TR_STU_TT_TotalMarks, TM_TR_STU_TT_Percentage', 'required'),
			array('TM_TR_STU_TT_Publisher_Id, TM_TR_STU_TT_Student_Id, TM_TR_STU_TT_Type, TM_TR_STU_TT_Mode, TM_TR_STU_TT_Status', 'numerical', 'integerOnly'=>true),
			array('TM_TR_STU_TT_Mark, TM_TR_STU_TT_TotalMarks, TM_TR_STU_TT_Percentage', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_TR_STU_TT_Id, TM_TR_STU_TT_Publisher_Id, TM_TR_STU_TT_Student_Id, TM_TR_STU_TT_Type, TM_TR_STU_TT_Mode, TM_TR_STU_TT_CreateOn, TM_TR_STU_TT_Status, TM_TR_STU_TT_Mark, TM_TR_STU_TT_TotalMarks, TM_TR_STU_TT_Percentage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'publisher'=>array(self::BELONGS_TO, 'Publishers', 'TM_TR_STU_TT_Publisher_Id'),
            'chapters'=>array(self::HAS_MANY, 'StudentTestChaptersTrial', 'TM_TR_STC_Test_Id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_TR_STU_TT_Id' => 'Tm Tr Stu Tt',
			'TM_TR_STU_TT_Publisher_Id' => 'Tm Tr Stu Tt Publisher',
			'TM_TR_STU_TT_Student_Id' => 'Tm Tr Stu Tt Student',
			'TM_TR_STU_TT_Type' => 'Tm Tr Stu Tt Type',
			'TM_TR_STU_TT_Mode' => 'Tm Tr Stu Tt Mode',
			'TM_TR_STU_TT_CreateOn' => 'Tm Tr Stu Tt Create On',
			'TM_TR_STU_TT_Status' => 'Tm Tr Stu Tt Status',
			'TM_TR_STU_TT_Mark' => 'Tm Tr Stu Tt Mark',
			'TM_TR_STU_TT_TotalMarks' => 'Tm Tr Stu Tt Total Marks',
			'TM_TR_STU_TT_Percentage' => 'Tm Tr Stu Tt Percentage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_TR_STU_TT_Id',$this->TM_TR_STU_TT_Id);
		$criteria->compare('TM_TR_STU_TT_Publisher_Id',$this->TM_TR_STU_TT_Publisher_Id);
		$criteria->compare('TM_TR_STU_TT_Student_Id',$this->TM_TR_STU_TT_Student_Id);
		$criteria->compare('TM_TR_STU_TT_Type',$this->TM_TR_STU_TT_Type);
		$criteria->compare('TM_TR_STU_TT_Mode',$this->TM_TR_STU_TT_Mode);
		$criteria->compare('TM_TR_STU_TT_CreateOn',$this->TM_TR_STU_TT_CreateOn,true);
		$criteria->compare('TM_TR_STU_TT_Status',$this->TM_TR_STU_TT_Status);
		$criteria->compare('TM_TR_STU_TT_Mark',$this->TM_TR_STU_TT_Mark,true);
		$criteria->compare('TM_TR_STU_TT_TotalMarks',$this->TM_TR_STU_TT_TotalMarks,true);
		$criteria->compare('TM_TR_STU_TT_Percentage',$this->TM_TR_STU_TT_Percentage,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentTestTrial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
