<?php

/**
 * This is the model class for table "{{blueprints}}".
 *
 * The followings are the available columns in table '{{blueprints}}':
 * @property integer $TM_BP_Id
 * @property string $TM_BP_Name
 * @property string $TM_BP_Description
 * @property integer $TM_BP_Syllabus_Id
 * @property integer $TM_BP_Standard_Id
 * @property integer $TM_BP_Totalmarks
 * @property integer $TM_BP_Status
 * @property string $TM_BP_CreatedOn
 * @property integer $TM_BP_CreatedBy
 */
class Blueprints extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{blueprints}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_BP_Name, TM_BP_Description, TM_BP_Syllabus_Id, TM_BP_Standard_Id, TM_BP_Totalmarks, TM_BP_Status, TM_BP_CreatedOn, TM_BP_CreatedBy', 'required'),
			array('TM_BP_Syllabus_Id, TM_BP_Standard_Id, TM_BP_Totalmarks, TM_BP_Status, TM_BP_CreatedBy', 'numerical', 'integerOnly'=>true),
			array('TM_BP_Name, TM_BP_Description', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_BP_Id, TM_BP_Name, TM_BP_Description, TM_BP_Syllabus_Id, TM_BP_Standard_Id, TM_BP_Totalmarks, TM_BP_Status, TM_BP_CreatedOn, TM_BP_CreatedBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'school' => array(self::BELONGS_TO, 'tm_blueprint_school', 'TM_BPS_Blueprint_Id'),
			'blueprintschool'=>array(self::HAS_MANY, 'BlueprintSchool', 'TM_BPS_Blueprint_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_BP_Id' => 'Blue Print ID',
			'TM_BP_Name' => 'Name',
			'TM_BP_Description' => 'Description',
			'TM_BP_Syllabus_Id' => 'Syllabus',
			'TM_BP_Standard_Id' => 'Standard',
			'TM_BP_Totalmarks' => 'Totalmarks',
			'TM_BP_Status' => 'Status',
			'TM_BP_CreatedOn' => 'Created On',
			'TM_BP_CreatedBy' => 'Created By',
			'TM_BP_Availability' => 'Availability',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		//echo "yes";exit;
		$criteria->compare('TM_BP_Id',$this->TM_BP_Id);
		$criteria->compare('TM_BP_Name',$this->TM_BP_Name,true);
		$criteria->compare('TM_BP_Description',$this->TM_BP_Description,true);
		$criteria->compare('TM_BP_Syllabus_Id',$this->TM_BP_Syllabus_Id);
		$criteria->compare('TM_BP_Standard_Id',$this->TM_BP_Standard_Id);
		$criteria->compare('TM_BP_Totalmarks',$this->TM_BP_Totalmarks);
		$criteria->compare('TM_BP_Status',$this->TM_BP_Status);
		$criteria->compare('TM_BP_CreatedOn',$this->TM_BP_CreatedOn,true);
		$criteria->compare('TM_BP_CreatedBy',$this->TM_BP_CreatedBy);
		$criteria->order = 'TM_BP_Id DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function schoolsearch($id)
	{
		//echo $id; exit;
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_BP_Id',$this->TM_BP_Id);
		$criteria->compare('TM_BP_Name',$this->TM_BP_Name,true);
		$criteria->compare('TM_BP_Description',$this->TM_BP_Description,true);
		$criteria->compare('TM_BP_Syllabus_Id',$this->TM_BP_Syllabus_Id);
		$criteria->compare('TM_BP_Standard_Id',$this->TM_BP_Standard_Id);
		$criteria->compare('TM_BP_Totalmarks',$this->TM_BP_Totalmarks);
		// $criteria->compare('TM_BP_Status',$this->TM_BP_Status);
		$criteria->compare('TM_BP_CreatedOn',$this->TM_BP_CreatedOn,true);
		$criteria->compare('TM_BP_CreatedBy',$this->TM_BP_CreatedBy);
		
		$criteria->addInCondition('TM_BP_Id', $id);
		if ($this->TM_BP_Status!=''):
			$masterschool=UserModule::GetSchoolDetails();
			$criteria->with = array('blueprintschool'=>array('select' => false,'joinType'=>'LEFT JOIN','condition'=>' TM_BPS_School_Id='.$masterschool->TM_SCL_Id.' AND  TM_BPS_Status="'.$this->TM_BP_Status.'" '));
			$criteria->together=true;
		else:
			$criteria->compare('TM_BP_Status',$this->TM_BP_Status);
		endif;
		$criteria->order = 'TM_BP_Id DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function GetMangeLink($id)
    {
        $html = "<a href='" . Yii::app()->createUrl('teachers/viewblueprint', array('id' => $id)) . "' class='btn btn-warning btn-xs' >View</a>";
        return $html;
    }
	public function GetStudMangeLink($id)
	{
		$html = "<a href='" . Yii::app()->createUrl('student/viewblueprint', array('id' => $id)) . "' style='color: #ffa900;' target='_blank'><span class='glyphicon glyphicon-eye-open'></span></a>";
		return $html;
	}

    public function GetCreatedBy($id)
    {
    	$uid = $id;
		$user=User::model()->find(array('condition'=>'id="'.$uid.'"'));
		$html = '';
		if($user->usertype == 1)
			$html = 'System';
		elseif($user->usertype == 12 || $user->usertype == 9){
				$userSchool = $user->school_id;
				$schoolName = School::model()->findByPk($userSchool)->TM_SCL_Name;
				$html = $schoolName;
		}
		else
			$html = $user->username;
		return $html;
    }
    public function GetCreatedByForSchool($id)
    {
    	
    	$uid = $id;
		$user=User::model()->find(array('condition'=>'id="'.$uid.'"'));
		
		$html = '';
		if($user->usertype == 1)
			$html = 'System';
		else
			$html = $user->profile->firstname;
	
		return $html;

    }
	public function GetAvailablity($avail)
	{
		if($avail==0):
			$type="Student";
		elseif($avail==1):
			$type="School - All";
		elseif($avail==2):
			$type="Both";
		elseif($avail==3):
			$type="School - Specific";
		elseif($avail==4):
			$type="Both - Specific Schools";
		endif;
		return $type;
	}

    public function GetStatusUpdateLink($id)
    {
    	$uid = Yii::app()->user->id;
		$user=User::model()->find(array('condition'=>'id="'.$uid.'"'));

		$userSchool = $user->school_id;

		$status = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id="'.$id.'" AND TM_BPS_School_Id="'.$userSchool.'"'))->TM_BPS_Status;
		if($status ==1)
		{
        $html = "<a href='" . Yii::app()->createUrl("blueprints/changeStatusInactive",array("id"=>$id)) . "' class='btn btn-warning btn-xs' >Activate</a>";
    	}
    	else
    	{
    		 $html = "<a href='" . Yii::app()->createUrl("blueprints/changeStatus",array("id"=>$id)) . "' class='btn btn-warning btn-xs' >Inactivate</a>";
    	}
        return $html;
    } 
	
	public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'BlueprintStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
            'BlueprintActivate' => array(
                '1' => ('Active'),
                '0' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Blueprints the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
