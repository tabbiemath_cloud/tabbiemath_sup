<?php

/**
 * This is the model class for table "{{mock}}".
 *
 * The followings are the available columns in table '{{mock}}':
 * @property integer $TM_MK_Id
 * @property string $TM_MK_Name
 * @property string $TM_MK_Description
 * @property integer $TM_MK_Publisher_Id
 * @property integer $TM_MK_Syllabus_Id
 * @property integer $TM_MK_Standard_Id
 * @property integer $TM_MK_Status
 * @property integer $TM_MK_Sort_Order
 * @property string $TM_MK_Tags  
 * @property string $TM_MK_CreatedOn
 * @property integer $TM_MK_CreatedBy
 * @property integer $TM_MK_Availability
 * @property integer $TM_MK_Resource
 * @property integer $TM_MK_TeacherAvailability
 * @property string $TM_MK_Worksheet
 * @property string $TM_MK_Solution
 * @property string $TM_MK_Video_Url
 */
 
class Mock extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $schools;
    public $Publish;
    public $schoolsearch;    
     
    public function tableName()
    {
        return '{{mock}}';
    }
    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('TM_MK_Name, TM_MK_Publisher_Id, TM_MK_Syllabus_Id, TM_MK_Standard_Id,TM_MK_Status,TM_MK_Resource', 'required','on' => 'Insert'),
            array('TM_MK_Publisher_Id,TM_MK_Syllabus_Id', 'safe','on' => 'teachercreation'),
            array('TM_MK_Publisher_Id,TM_MK_Syllabus_Id', 'safe','on' => 'resourcecreation'),
            array('TM_MK_Publisher_Id, TM_MK_Syllabus_Id, TM_MK_Standard_Id', 'numerical', 'integerOnly' => true),
            array('TM_MK_Name', 'length', 'max' => 250),
            array('TM_MK_Name,TM_MK_Worksheet', 'required','on' => 'teachercreation'),
            array('TM_MK_Name,TM_MK_Video_Url', 'required','on' => 'resourcecreation'),
            array('TM_MK_Video_Url', 'filevalid','on'=>'Insert'),
            array('TM_MK_Privacy', 'safe'),
            array('TM_MK_Worksheet', 'file', 'types'=>'pdf, doc, docx,png,jpg,jpeg,gif','allowEmpty'=>false, 'safe' => true,'on' => 'teachercreation'),
            array('TM_MK_Solution', 'file', 'types'=>'pdf, doc, docx,png,jpg,jpeg,gif','allowEmpty'=>true, 'safe' => true,'on' => 'teachercreation'),            
            array('TM_MK_Thumbnail', 'file', 'types'=>'png,jpg,jpeg','allowEmpty'=>true, 'safe' => true,'on' => 'teachercreation'),
            array('TM_MK_Thumbnail', 'file', 'types'=>'png,jpg,jpeg','allowEmpty'=>true, 'safe' => true,'on' => 'resourcecreation'),
            array('TM_MK_Description,schools,TM_MK_Availability,TM_MK_Sort_Order,TM_MK_Tags,TM_MK_CreatedBy,TM_MK_Video_Url', 'safe'),
                        
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('TM_MK_Id, TM_MK_Name, TM_MK_Description, TM_MK_Publisher_Id, TM_MK_Syllabus_Id, TM_MK_Standard_Id,TM_MK_Status,TM_MK_CreatedOn,TM_MK_CreatedBy,TM_MK_Resource', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'syllabus' => array(self::BELONGS_TO, 'Syllabus', 'TM_MK_Syllabus_Id'),
            'publisher' => array(self::BELONGS_TO, 'Publishers', 'TM_MK_Publisher_Id'),
            'standard' => array(self::BELONGS_TO, 'Standard', 'TM_MK_Standard_Id'),
            'questions' => array(self::MANY_MANY, 'Questions',' tm_mock_questions(TM_MQ_Question_Id, TM_MQ_Mock_Id)'),
            'schools' => array(self::MANY_MANY, 'MockSchool',' tm_mock_school(TM_MS_School_Id, TM_MS_Mock_Id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'TM_MK_Id' => 'Id',
            'TM_MK_Name' => 'Name',
            'TM_MK_Description' => 'Description',
            'TM_MK_Publisher_Id' => 'Publisher',
            'TM_MK_Syllabus_Id' => 'Syllabus',
            'TM_MK_Standard_Id' => 'Standard',
            'TM_MK_CreatedOn' => 'Created On',
            'TM_MK_CreatedBy' => 'Created By',
            'TM_MK_Status' => 'Status',
            'TM_MK_Availability'=>'Availability',
            'TM_MK_Privacy'=>'Availability',
            'schools'=>'Schools',
            'TM_MK_Worksheet'=>'Worksheet File',
            'TM_MK_Sort_Order'=>'Sort Order',
            'TM_MK_Tags'=>'Tags',      
            'TM_MK_Type'=>'Publish Type',
            'TM_MK_Solution'=>'Solution File',
            'TM_MK_Thumbnail'=>'Thumbnail Image',
            'TM_MK_Video_Url'=>'Specify URL',
            'TM_MK_Resource'=>'Type',
            'TM_MK_Link_Resource'=>'Link to Resource'
        );
    }

    public function filevalid()
    {
        if($this->TM_MK_Video_Url!='')
        {
            if($this->TM_MK_Worksheet!='')
            {
                $this->addError('TM_MK_Worksheet','Cannot upload file and video url');
            }
        }

    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('TM_MK_Id', $this->TM_MK_Id);
        $criteria->compare('TM_MK_Name', $this->TM_MK_Name, true);
        $criteria->compare('TM_MK_Description', $this->TM_MK_Description, true);
        $criteria->compare('TM_MK_Publisher_Id', $this->TM_MK_Publisher_Id);
        $criteria->compare('TM_MK_Syllabus_Id', $this->TM_MK_Syllabus_Id);
        $criteria->compare('TM_MK_Standard_Id', $this->TM_MK_Standard_Id);
        $criteria->compare('TM_MK_CreatedOn', $this->TM_MK_CreatedOn);
        $criteria->compare('TM_MK_CreatedBy', $this->TM_MK_CreatedBy);
        $criteria->compare('TM_MK_Status', $this->TM_MK_Status);
        $criteria->compare('TM_MK_Availability', $this->TM_MK_Availability);
        $criteria->compare('TM_MK_Resource', $this->TM_MK_Resource);
        
        /*if($this->TM_MK_CreatedBy!='0'):
		    $criteria->compare('TM_MK_CreatedBy',($this->TM_MK_CreatedBy!=''?$this->TM_MK_CreatedBy:Yii::app()->user->id));
        endif;*/
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array(
                'pageSize' => 500,
            ),'sort'=>array(
                    'defaultOrder'=>'TM_MK_Sort_Order DESC',
                  )
        ));
    }
    public function searchschool($user,$standard)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.        
        $criteria = new CDbCriteria;

         
        /*if($this->TM_MK_CreatedBy!='0'):
		    $criteria->compare('TM_MK_CreatedBy',($this->TM_MK_CreatedBy!=''?$this->TM_MK_CreatedBy:Yii::app()->user->id));
        endif;*/
        if($this->schoolsearch=='true'):
            $criteria->join = 'INNER JOIN tm_mock_school AS m ON m.TM_MS_Mock_Id= TM_MK_Id LEFT JOIN tm_profiles AS us ON us.user_id=TM_MK_CreatedBy';                 
            $criteria->addCondition("TM_MK_Status=0 AND TM_MK_Availability != 0 AND m.TM_MS_School_Id='".Yii::app()->session['school']."' AND TM_MK_Tags LIKE '%".$this->TM_MK_Tags."%'
                        AND (
                            TM_MK_Name LIKE '%".$this->TM_MK_Name."%' OR
                            TM_MK_Description LIKE '%".$this->TM_MK_Name."%' OR 
                            TM_MK_Tags LIKE '%".$this->TM_MK_Name."%' OR
                            lastname LIKE '%".$this->TM_MK_Name."%' OR
                            firstname LIKE '%".$this->TM_MK_Name."%' 
                            )
                        ");       
        else:            
            $criteria->compare('TM_MK_Id', $this->TM_MK_Id);
            $criteria->compare('TM_MK_Name', $this->TM_MK_Name, true);
            $criteria->compare('TM_MK_Description', $this->TM_MK_Description, true);
            //$criteria->compare('TM_MK_Publisher_Id', $this->TM_MK_Publisher_Id);
            //$criteria->compare('TM_MK_Syllabus_Id', $this->TM_MK_Syllabus_Id);
            $criteria->compare('TM_MK_Standard_Id', $standard);
            $criteria->compare('TM_MK_CreatedOn', $this->TM_MK_CreatedOn);
            $criteria->compare('TM_MK_CreatedBy', $this->TM_MK_CreatedBy);
            //$criteria->compare('TM_MK_Status', $this->TM_MK_Status);
            $criteria->join = 'INNER JOIN tm_mock_school AS m ON m.TM_MS_Mock_Id= TM_MK_Id';                 
            $criteria->addCondition("TM_MK_Status=0 AND TM_MK_Availability != 0 AND m.TM_MS_School_Id='".Yii::app()->session['school']."' AND TM_MK_Tags LIKE '%".$this->TM_MK_Tags."%'");
        endif;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array(
                'pageSize' => 100,
            ),'sort'=>array(
                    'defaultOrder'=>'TM_MK_Sort_Order DESC',
                  )
        ));
    }
    public static function itemAlias($type, $code = NULL)
    {
        $_items = array(

            'MockStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
            'MockAvail' => array(
                '0' => ('Student'),
                '1' => ('School - All'),
                '3' => ('School - Specific'),
                '4' => ('Both - Specific Schools'),
                '2' => ('Both'),
            ),  
            'MockPublish' => array(
                '0' => ('Online'),
                '1' => ('Worksheet'),                
            ),  
            'MockPrivacy' => array(
                '0' => ('Public'),
                '1' => ('Private'),                
            ),
            'MockResource' => array(
                '0' => ('Worksheet'),
                '1' => ('Resource'),
            ),
            'Linkresource'=>array(
                '1'=>('Yes'),
                '0'=>('No'),
            ),
            
                                 
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function GetMangeLink($id)
    {
        $html = "<a href='" . Yii::app()->createUrl('mock/managequestions', array('id' => $id)) . "'>Manage Questions</a>";
        return $html;
    }

    public function GetPrintLink($id)
    {
        $mock=Mock::model()->findByPk($id);
        if($mock['TM_MK_Resource']!=0):
            $html=$mock['TM_MK_Video_Url'];
        else:
            $html = Yii::app()->createUrl('mock/printmock', array('id' => $id));
        endif;
        return $html;
    }

    public function GetSortColumn($id,$sortorder)
    {
        $column='<INPUT TYPE="image" id="orderlinkup'.$id.'" 	class="order_link" data-info="up" 	SRC="'.Yii::app()->request->baseUrl.'/images/up.gif" ALT="SUBMIT" data-id="'.$id.'" data-order="'.$sortorder.'"><INPUT TYPE="image" id="orderlinkdown'.$id.'" 	class="order_link" data-info="down" SRC="'.Yii::app()->request->baseUrl.'/images/down.gif" ALT="SUBMIT" data-id="'.$id.'" data-order="'.$sortorder.'" >';
        return $column;
    }
    public function GetCreator($id)
    {
        $user=User::model()->findByPk($id);
        if(count($user)>0):
            if($user->school_id=='0'):
                return 'System';
            else:
                return $user->profile->firstname;
            endif;
        else:
            return 'System';
        endif;
    }
    public function GetCreatorAdmin($id)
    {
        $user=User::model()->findByPk($id);
        if($user->school_id=='0'):
            return 'System';
        else:
            return School::model()->findByPk($user->school_id)->TM_SCL_Name;
        endif;
    }    
    public function checkQuestions($id)
    {
        $mock=Mock::model()->findByPk($id);
        if($mock->TM_MK_Solution!=''):
            return "visibility:visible";
        else:
            $questioncount=MockQuestions::model()->count(array('condition'=>'TM_MQ_Mock_Id='.$id));
            if($questioncount>0):
                return "visibility:visible";
            else:
                return "visibility:hidden";
            endif;
        endif;
    }

    public function GetSchool($id)
    {
        $school=MockSchool::model()->find(array('condition'=>'TM_MS_Mock_Id='.$id.' '));
        $schoolname=School::model()->findByPk($school->TM_MS_School_Id)->TM_SCL_Name;
        return $schoolname;
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Mock the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
