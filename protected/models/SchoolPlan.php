<?php

/**
 * This is the model class for table "{{student_plan}}".
 *
 * The followings are the available columns in table '{{student_plan}}':
 * @property integer $TM_SPN_Id
 * @property integer $TM_SPN_Publisher_Id
 * @property integer $TM_SPN_SchoolId
 * @property integer $TM_SPN_PlanId
 * @property integer $TM_SPN_SyllabusId
 * @property integer $TM_SPN_StandardId
 * @property integer $TM_SPN_SubjectId
 * @property integer $TM_SPN_Status
 * @property integer $TM_SPN_CreatedOn
 * @property integer $TM_SPN_ExpieryDate 
 * @property string $TM_SPN_PaymentReference
 * @property string $TM_SPN_CouponId
 * @property string $TM_SPN_CancelDate
 * @property integer $TM_SPN_No_of_Students
 * @property integer $TM_SPN_StartDate 
 * @property integer $TM_SPN_Type
 * @property integer $TM_SPN_Rate
 * @property integer $TM_SPN_Currency
 * @property string $TM_SPN_Provider
 */
 
class SchoolPlan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{school_plan}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SPN_Publisher_Id,TM_SPN_SchoolId, TM_SPN_PlanId,TM_SPN_StartDate, TM_SPN_SyllabusId, TM_SPN_StandardId, TM_SPN_SubjectId', 'required'),
			array('TM_SPN_SchoolId, TM_SPN_PlanId, TM_SPN_SyllabusId, TM_SPN_StandardId, TM_SPN_SubjectId, TM_SPN_Status,TM_SPN_Currency,TM_SPN_Rate,TM_SPN_Type,TM_SPN_No_of_Students', 'numerical', 'integerOnly'=>true),
            array('TM_SPN_CouponId,TM_SPN_Currency,TM_SPN_Rate,TM_SPN_Type,TM_SPN_No_of_Students,TM_SPN_CreatedOn','safe'),
            array('TM_SPN_PlanId','Checkexists','on'=>'create'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SPN_Id, TM_SPN_SchoolId, TM_SPN_PlanId, TM_SPN_SyllabusId, TM_SPN_StandardId, TM_SPN_SubjectId, TM_SPN_Status,TM_SPN_CreatedOn,TM_SPN_Currency,TM_SPN_Rate,TM_SPN_Type,TM_SPN_StartDate,TM_SPN_No_of_Students', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'plan'=>array(self::BELONGS_TO, 'Plan', 'TM_SPN_PlanId'),
            'school'=>array(self::BELONGS_TO, 'School', 'TM_SPN_SchoolId'),
            'syllabus'=>array(self::BELONGS_TO, 'Syllabus', 'TM_SPN_SyllabusId'),
            'standard'=>array(self::BELONGS_TO, 'Standard', 'TM_SPN_StandardId'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SPN_Id' => 'Tm Spn',
            'TM_SPN_Publisher_Id'=>'Publisher',
			'TM_SPN_SchoolId' => 'School',
			'TM_SPN_PlanId' => 'Plan',
			'TM_SPN_SyllabusId' => 'Syllabus',
			'TM_SPN_StandardId' => 'Standard',
			'TM_SPN_SubjectId' => 'Subject',
			'TM_SPN_Status' => 'Status',
            'TM_SPN_CouponId'=>'Coupon Code',
            'TM_SPN_PaymentReference'=>'Transaction',
            'TM_SPN_Currency'=>'Currency',
            'TM_SPN_Rate'=>'Agreed Rate',
            'TM_SPN_Type'=>'Type', 
            'TM_SPN_StartDate'=>'Start Date',
            'TM_SPN_ExpieryDate'=>'End Date',                        
            'TM_SPN_No_of_Students'=>'Maximum Students',
            'TM_SPN_Provider'=>'Gateway',             
            
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SPN_Id',$this->TM_SPN_Id);
		$criteria->compare('TM_SPN_SchoolId',$this->TM_SPN_SchoolId);
		$criteria->compare('TM_SPN_PlanId',$this->TM_SPN_PlanId);
		$criteria->compare('TM_SPN_SyllabusId',$this->TM_SPN_SyllabusId);
		$criteria->compare('TM_SPN_StandardId',$this->TM_SPN_StandardId);
		$criteria->compare('TM_SPN_SubjectId',$this->TM_SPN_SubjectId);
		$criteria->compare('TM_SPN_Status',$this->TM_SPN_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}
    public function Checkexists($attribute)
    {
        
        if(SchoolPlan::model()->exists('TM_SPN_PlanId=:plan AND TM_SPN_SchoolId=:school AND TM_SPN_Status=0',array(':plan'=>$this->$attribute,':school'=>$this->TM_SPN_SchoolId))):
            $this->addError($attribute, 'Selected plan is already active.');
        endif;
    }
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'SubsciptionStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
			'PaymentType' => array(
				'0' => ('Paid'),
				'1' => ('Trial'),
			),            
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }    
    public function GetCancelldate($date)
    {
        if($date=='0000-00-00')
        {
            return '';
        }
        else
        {
            return date("d-m-Y",strtotime($date));
        }
    }    
    public function GetExpiredate($date)
    {
        if($date=='0000-00-00')
        {
            return '';
        }
        else
        {
            return date("d-m-Y",mktime(0, 0, 0, date("m",strtotime($date)),   date("d",strtotime($date)),   date("Y",strtotime($date))+1));
        }
    }    
    public function GetStatus($mode,$id)
    {
        $studentplan=StudentPlan::model()->findByPk($id);

        if($studentplan->TM_SPN_Status=='0'):
            if($mode=='deactive'):
                return "display:".$mode.$studentplan->TM_SPN_Status;
            elseif($mode=='active'):
                return "display:".$mode.$studentplan->TM_SPN_Status;
            endif;
        else:
            if($mode=='deactive'):
                return "display:".$mode.$studentplan->TM_SPN_Status;
            elseif($mode=='active'):
                return "display:".$mode.$studentplan->TM_SPN_Status;
            endif;
        endif;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentPlan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
