<?php

/**
 * This is the model class for table "{{school_staffs}}".
 *
 * The followings are the available columns in table '{{school_staffs}}':
 * @property integer $TM_SCS_Id
 * @property integer $TM_SCS_School_Id
 * @property integer $TM_SCS_User_Id
 * @property integer $TM_SCS_Type
 */
class SchoolStaffs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{school_staffs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SCS_Type', 'required'),
			array('TM_SCS_School_Id, TM_SCS_User_Id, TM_SCS_Type', 'numerical', 'integerOnly'=>true),
            array('TM_SCS_School_Id, TM_SCS_User_Id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SCS_Id, TM_SCS_School_Id, TM_SCS_User_Id, TM_SCS_Type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SCS_Id' => 'Id',
			'TM_SCS_School_Id' => 'School ID',
			'TM_SCS_User_Id' => 'Staff Id',
			'TM_SCS_Type' => 'Staff Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SCS_Id',$this->TM_SCS_Id);
		$criteria->compare('TM_SCS_School_Id',$this->TM_SCS_School_Id);
		$criteria->compare('TM_SCS_User_Id',$this->TM_SCS_User_Id);
		$criteria->compare('TM_SCS_Type',$this->TM_SCS_Type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SchoolStaffs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
