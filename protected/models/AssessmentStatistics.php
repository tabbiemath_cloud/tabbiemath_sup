<?php

/**
 * This is the model class for table "assessment_statistics".
 *
 * The followings are the available columns in table 'assessment_statistics':
 * @property integer $id
 * @property string $date
 * @property string $group_ids
 * @property integer $teacher_id
 * @property integer $assignment_id
 * @property integer $assignment_name
 * @property integer $students_completed
 * @property integer $students_not_attempted
 * @property double $average_score
 */
class AssessmentStatistics extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'assessment_statistics';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, group_ids, teacher_id, assignment_id, assignment_name, students_completed, students_not_attempted, average_score', 'required'),
			array('teacher_id, assignment_id, assignment_name, students_completed, students_not_attempted', 'numerical', 'integerOnly'=>true),
			array('average_score', 'numerical'),
			array('group_ids', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, group_ids, teacher_id, assignment_id, assignment_name, students_completed, students_not_attempted, average_score', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'group_ids' => 'Group Ids',
			'teacher_id' => 'Teacher',
			'assignment_id' => 'Assignment',
			'assignment_name' => 'Assignment Name',
			'students_completed' => 'Students Completed',
			'students_not_attempted' => 'Students Not Attempted',
			'average_score' => 'Average Score',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('group_ids',$this->group_ids,true);
		$criteria->compare('teacher_id',$this->teacher_id);
		$criteria->compare('assignment_id',$this->assignment_id);
		$criteria->compare('assignment_name',$this->assignment_name);
		$criteria->compare('students_completed',$this->students_completed);
		$criteria->compare('students_not_attempted',$this->students_not_attempted);
		$criteria->compare('average_score',$this->average_score);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AssessmentStatistics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
