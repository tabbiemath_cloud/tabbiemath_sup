<?php

/**
 * This is the model class for table "{{blueprint_template}}".
 *
 * The followings are the available columns in table '{{blueprint_template}}':
 * @property integer $TM_BPT_Id
 * @property integer $TM_BPT_Blueprint_Id
 * @property integer $TM_BPT_Chapter_Id
 * @property integer $TM_BPT_Mark_type
 * @property integer $TM_BPT_No_questions
 */
class blueprintTemplate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{blueprint_template}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_BPT_Blueprint_Id, TM_BPT_Chapter_Id, TM_BPT_Mark_type, TM_BPT_No_questions', 'required'),
			array('TM_BPT_Blueprint_Id, TM_BPT_Chapter_Id, TM_BPT_Mark_type, TM_BPT_No_questions', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_BPT_Id, TM_BPT_Blueprint_Id, TM_BPT_Chapter_Id, TM_BPT_Mark_type, TM_BPT_No_questions', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_BPT_Id' => 'Tm Bpt',
			'TM_BPT_Blueprint_Id' => 'Tm Bpt Blueprint',
			'TM_BPT_Chapter_Id' => 'Tm Bpt Chapter',
			'TM_BPT_Mark_type' => 'Tm Bpt Mark Type',
			'TM_BPT_No_questions' => 'Tm Bpt No Questions',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_BPT_Id',$this->TM_BPT_Id);
		$criteria->compare('TM_BPT_Blueprint_Id',$this->TM_BPT_Blueprint_Id);
		$criteria->compare('TM_BPT_Chapter_Id',$this->TM_BPT_Chapter_Id);
		$criteria->compare('TM_BPT_Mark_type',$this->TM_BPT_Mark_type);
		$criteria->compare('TM_BPT_No_questions',$this->TM_BPT_No_questions);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return blueprintTemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
