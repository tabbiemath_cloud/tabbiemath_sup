<?php

/**
 * This is the model class for table "{{blueprint_topics}}".
 *
 * The followings are the available columns in table '{{blueprint_topics}}':
 * @property integer $TM_BP_TP_Id
 * @property integer $TM_BP_TP_Template_Id
 * @property integer $TM_BP_TP_Chapter_Id
 * @property integer $TM_BP_TP_Topic_Id
 * @property integer $TM_BP_TP_Blueprint_Id
 */
class BlueprintTopics extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{blueprint_topics}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_BP_TP_Template_Id, TM_BP_TP_Chapter_Id, TM_BP_TP_Topic_Id', 'required'),
			array('TM_BP_TP_Template_Id, TM_BP_TP_Chapter_Id, TM_BP_TP_Topic_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_BP_TP_Id, TM_BP_TP_Template_Id, TM_BP_TP_Chapter_Id, TM_BP_TP_Topic_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_BP_TP_Id' => 'Tm Bp Tp',
			'TM_BP_TP_Template_Id' => 'Tm Bp Tp Template',
			'TM_BP_TP_Chapter_Id' => 'Tm Bp Tp Chapter',
			'TM_BP_TP_Topic_Id' => 'Tm Bp Tp Topic',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_BP_TP_Id',$this->TM_BP_TP_Id);
		$criteria->compare('TM_BP_TP_Template_Id',$this->TM_BP_TP_Template_Id);
		$criteria->compare('TM_BP_TP_Chapter_Id',$this->TM_BP_TP_Chapter_Id);
		$criteria->compare('TM_BP_TP_Topic_Id',$this->TM_BP_TP_Topic_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlueprintTopics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
