<?php

/**
 * This is the model class for table "{{worksheetprint_report}}".
 *
 * The followings are the available columns in table '{{worksheetprint_report}}':
 * @property integer $TM_WPR_Id
 * @property string $TM_WPR_Date
 * @property string $TM_WPR_Username
 * @property string $TM_WPR_FirstName
 * @property string $TM_WPR_LastName
 * @property integer $TM_WPR_Standard
 * @property integer $TM_WPR_School
 * @property integer $TM_WPR_School_id
 * @property integer $TM_WPR_Type
 * @property string $TM_WPR_Location
 * @property string $TM_WPR_Worksheetname
 */
class WorksheetprintReport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{worksheetprint_report}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_WPR_Date, TM_WPR_Username, TM_WPR_FirstName, TM_WPR_LastName, TM_WPR_Standard, TM_WPR_School, TM_WPR_Type, TM_WPR_Location, TM_WPR_Worksheetname', 'required'),
			array('TM_WPR_Standard, TM_WPR_School, TM_WPR_Type', 'numerical', 'integerOnly'=>true),
			array('TM_WPR_Username, TM_WPR_FirstName, TM_WPR_LastName, TM_WPR_Location, TM_WPR_Worksheetname', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_WPR_Id, TM_WPR_Date, TM_WPR_Username, TM_WPR_FirstName, TM_WPR_LastName, TM_WPR_Standard, TM_WPR_School, TM_WPR_Type, TM_WPR_Location, TM_WPR_Worksheetname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_WPR_Id' => 'Tm Wpr',
			'TM_WPR_Date' => 'Tm Wpr Date',
			'TM_WPR_Username' => 'Tm Wpr Username',
			'TM_WPR_FirstName' => 'Tm Wpr First Name',
			'TM_WPR_LastName' => 'Tm Wpr Last Name',
			'TM_WPR_Standard' => 'Tm Wpr Standard',
			'TM_WPR_School' => 'Tm Wpr School',
			'TM_WPR_Type' => 'Tm Wpr Type',
			'TM_WPR_Location' => 'Tm Wpr Location',
			'TM_WPR_Worksheetname' => 'Tm Wpr Worksheetname',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_WPR_Id',$this->TM_WPR_Id);
		$criteria->compare('TM_WPR_Date',$this->TM_WPR_Date,true);
		$criteria->compare('TM_WPR_Username',$this->TM_WPR_Username,true);
		$criteria->compare('TM_WPR_FirstName',$this->TM_WPR_FirstName,true);
		$criteria->compare('TM_WPR_LastName',$this->TM_WPR_LastName,true);
		$criteria->compare('TM_WPR_Standard',$this->TM_WPR_Standard);
		$criteria->compare('TM_WPR_School',$this->TM_WPR_School);
		$criteria->compare('TM_WPR_Type',$this->TM_WPR_Type);
		$criteria->compare('TM_WPR_Location',$this->TM_WPR_Location,true);
		$criteria->compare('TM_WPR_Worksheetname',$this->TM_WPR_Worksheetname,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorksheetprintReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
