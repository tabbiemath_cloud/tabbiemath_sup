<?php

/**
 * This is the model class for table "{{notifications}}".
 *
 * The followings are the available columns in table '{{notifications}}':
 * @property integer $TM_NT_Id
 * @property integer $TM_NT_User
 * @property string $TM_NT_Type
 * @property integer $TM_NT_Target_Id
 * @property integer $TM_NT_Item_Id
 * @property integer $TM_NT_Status
 */
class Notifications extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{notifications}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_NT_User, TM_NT_Type, TM_NT_Target_Id, TM_NT_Status', 'required'),
			array('TM_NT_User, TM_NT_Target_Id, TM_NT_Status', 'numerical', 'integerOnly'=>true),
			array('TM_NT_Type', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_NT_Id, TM_NT_User, TM_NT_Type, TM_NT_Target_Id, TM_NT_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_NT_Id' => 'Tm Nt',
			'TM_NT_User' => 'Tm Nt User',
			'TM_NT_Type' => 'Tm Nt Type',
			'TM_NT_Target_Id' => 'Tm Nt Target',
			'TM_NT_Status' => 'Tm Nt Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_NT_Id',$this->TM_NT_Id);
		$criteria->compare('TM_NT_User',$this->TM_NT_User);
		$criteria->compare('TM_NT_Type',$this->TM_NT_Type,true);
		$criteria->compare('TM_NT_Target_Id',$this->TM_NT_Target_Id);
		$criteria->compare('TM_NT_Status',$this->TM_NT_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Notifications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
