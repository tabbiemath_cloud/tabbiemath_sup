<?php

/**
 * This is the model class for table "{{available_marks}}".
 *
 * The followings are the available columns in table '{{available_marks}}':
 * @property integer $TM_AV_MK_Id
 * @property integer $TM_AV_MK_Standard_Id
 * @property double $TM_AV_MK_Mark
 */
class TmAvailableMarks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{available_marks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_AV_MK_Standard_Id, TM_AV_MK_Mark', 'required'),
			array('TM_AV_MK_Standard_Id', 'numerical', 'integerOnly'=>true),
			array('TM_AV_MK_Mark', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_AV_MK_Id, TM_AV_MK_Standard_Id, TM_AV_MK_Mark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_AV_MK_Id' => 'Tm Av Mk',
			'TM_AV_MK_Standard_Id' => 'Tm Av Mk Standard',
			'TM_AV_MK_Mark' => 'Tm Av Mk Mark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_AV_MK_Id',$this->TM_AV_MK_Id);
		$criteria->compare('TM_AV_MK_Standard_Id',$this->TM_AV_MK_Standard_Id);
		$criteria->compare('TM_AV_MK_Mark',$this->TM_AV_MK_Mark);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TmAvailableMarks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
