<?php

/**
 * This is the model class for table "{{exams}}".
 *
 * The followings are the available columns in table '{{exams}}':
 * @property integer $TM_EX_Id
 * @property string $TM_EX_Name
 * @property string $TM_EX_CreatedOn
 * @property integer $TM_EX_Createdby
 * @property integer $TM_EX_Status
 * @property integer $TM_EX_Order
 */
class Exams extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{exams}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_EX_Name, TM_EX_CreatedOn, TM_EX_Createdby, TM_EX_Status', 'required'),
			array('TM_EX_Createdby, TM_EX_Status', 'numerical', 'integerOnly'=>true),
			array('TM_EX_Name', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_EX_Id, TM_EX_Name, TM_EX_CreatedOn, TM_EX_Createdby, TM_EX_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'questions'=>array(self::HAS_MANY, 'QuestionExams', 'TM_QE_Exam_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_EX_Id' => 'Tm Ex',
			'TM_EX_Name' => 'Tm Ex Name',
			'TM_EX_CreatedOn' => 'Tm Ex Created On',
			'TM_EX_Createdby' => 'Tm Ex Createdby',
			'TM_EX_Status' => 'Tm Ex Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_EX_Id',$this->TM_EX_Id);
		$criteria->compare('TM_EX_Name',$this->TM_EX_Name,true);
		$criteria->compare('TM_EX_CreatedOn',$this->TM_EX_CreatedOn,true);
		$criteria->compare('TM_EX_Createdby',$this->TM_EX_Createdby);
		$criteria->compare('TM_EX_Status',$this->TM_EX_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Exams the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
