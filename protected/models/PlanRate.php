<?php

/**
 * This is the model class for table "{{plan_rate}}".
 *
 * The followings are the available columns in table '{{plan_rate}}':
 * @property integer $TM_PR_Id
 * @property integer $TM_PR_Plan_Id
 * @property string $TM_PR_Rate
 * @property integer $TM_PR_Currency_Id
 */
class PlanRate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{plan_rate}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_PR_Plan_Id, TM_PR_Rate, TM_PR_Currency_Id', 'required'),
			array('TM_PR_Plan_Id, TM_PR_Currency_Id', 'numerical', 'integerOnly'=>true),
			array('TM_PR_Rate', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_PR_Id, TM_PR_Plan_Id, TM_PR_Rate, TM_PR_Currency_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_PR_Id' => 'Tm Pr',
			'TM_PR_Plan_Id' => 'Tm Pr Plan',
			'TM_PR_Rate' => 'Tm Pr Rate',
			'TM_PR_Currency_Id' => 'Tm Pr Currency',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_PR_Id',$this->TM_PR_Id);
		$criteria->compare('TM_PR_Plan_Id',$this->TM_PR_Plan_Id);
		$criteria->compare('TM_PR_Rate',$this->TM_PR_Rate,true);
		$criteria->compare('TM_PR_Currency_Id',$this->TM_PR_Currency_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PlanRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
