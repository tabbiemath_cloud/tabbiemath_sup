<?php

class TeachersController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admincolumn2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),*/
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'CreateSchoolTeachers', 'ManageTeachers', 'ViewTeachers', 'EditTeachers', 'DeleteTeachers', 'AddTeacher', 'AddStandards','Home','ImportTeacher','Teacherexport'),
                'users' => UserModule::getAccountAdmins(),
            ),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('CreateSchoolTeachers', 'ManageTeachers', 'ViewTeachers', 'EditTeachers', 'DeleteTeachers', 'AddTeacher', 'AddStandards','Home'),
                'users'=>array('@'),
                'expression'=>'UserModule::isSchoolStaffAdmin($_GET["id"])'				
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Home','TeacherPassword','ChangeStandard','createquickhomework','Quickhomework','Listquickhomework','GetGroups','GetTopics','createCustomeHomework','updateCustomeHomework','deleteHomework','Deleteassignedhomework','PublishHomework','Listcustomehomework','managecustomquestions','Getquestions','Addquestion','Removequestion','markhomework','savemarks','Markcomplete','Completeallmarking','sendreminder','Solution','Answer','History','SaveStatus','Getmarkers','assignmarking','Staffhome','Previewquestion','Printworksheet','Printsolution','PublishCustomHomework','GetGroupsCustom','Republishhomework','questionstatus','homeworkstatus','ImportTeacher','Mockorder','editmarkcomplete','EditHomework','Changehomeworkdetails','Passwordchange','submitstudent','submitall','Teacherexport'),
                'users'=>array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionHome()
    {        
        Yii::app()->session['mode'] = "Teacher";
        $this->layout = '//layouts/teacher';
        $masterschool=UserModule::GetSchoolDetails();
        $model=new Schoolhomework('search');
        $modelmarking=new Homeworkmarking('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Schoolhomework']))
            $model->attributes=$_GET['Schoolhomework'];
        $this->render('teacherhome',array('masterschool'=>$masterschool,'model'=>$model,'modelmarking'=>$modelmarking));

    }
    //code by amal
    public function actionHistory()
    {
        $this->layout = '//layouts/teacher';
        $model=new Schoolhomework('search');
        $this->render('history',array('model'=>$model));
    }
    //ends
    public function actionCreate()
    {
        $model = new Teachers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Teachers'])) {
            $model->attributes = $_POST['Teachers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->TM_TH_Id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionCreateSchoolTeachers($id)
    {
        $model = new Teachers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Teachers'])) {
            $model->attributes = $_POST['Teachers'];
            if ($model->save())

                $this->redirect(array('view', 'id' => $model->TM_TH_Id));
        }

        $this->render('create', array(
            'model' => $model, 'schoolId' => $id
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Teachers'])) {
            $model->attributes = $_POST['Teachers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->TM_TH_Id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Teachers');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Teachers('search');
        /*		$model->unsetAttributes();  // clear any default values
          if(isset($_GET['Teachers']))
              $model->attributes=$_GET['Teachers'];*/

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Teachers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Teachers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Teachers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'teachers-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionManageTeachers($id)
    {
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        $dataProvider = new CActiveDataProvider('User', array(
            'criteria' => array('condition' => 'usertype IN (9,12) AND school_id=' . $id),
            'pagination' => array(
                'pageSize' => 1000,
            ),
        ));
        $this->render('teacherslist', array(
            'dataProvider' => $dataProvider,
            'id' => $id
        ));
    }

    public function actionTeacherexport($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_TH_Name,DATE_FORMAT(a.TM_TH_CreatedOn,'%d-%m-%Y') AS regdate,a.TM_TH_Status,";
        $sql.="a.TM_TH_User_Id,a.TM_TH_Manage_Questions,b.username,b.status,b.createtime,b.usertype,c.TM_SCL_Name,d.firstname,d.lastname,d.phonenumber ";
        $sql.="FROM tm_teachers AS a LEFT JOIN tm_users AS b ON a.TM_TH_User_Id=b.id ";
        $sql.="LEFT JOIN tm_school AS c ON a.TM_TH_SchoolId=c.TM_SCL_Id ";
        $sql.="LEFT JOIN tm_profiles AS d ON a.TM_TH_User_Id=d.user_id WHERE b.usertype IN (9,12) AND a.TM_TH_SchoolId='$id'";

        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataval=$dataReader->readAll();
        foreach($dataval AS $data):
            if($data['status']==0):
                $status="Not Active";
            else:
                $status="Active";
            endif;
            if($data['usertype']==9):
                $usertype="Teacher";
            else:
                $usertype="Teacher Admin";
            endif;
            if($data['TM_TH_Manage_Questions']==1):
                $questions="Yes";
            else:
                $questions="No";
            endif;
            $standards=$this->getstandards($data['TM_TH_User_Id']);
            $password='';
            $items[]=array($data['firstname'],
                $data['lastname'],
                $data['username'],
                $password,
                $data['TM_SCL_Name'],
                date("d-m-Y",$data['createtime']),
                $status,
                $standards,
                $data['phonenumber'],
                $usertype,
                $questions,
            );
        endforeach;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Teachers.csv');
        $output = fopen('php://output', 'w');
        /*fputcsv($output, array(
            "Firstname",
            "Lastname",
            "Username",
            "Password",
            "School",
            "Registration date",
            "Status",
            "Standard",
            "Phone Number",
            "Usertype",
            "Manage Questions",
        ));*/
        foreach($items as $row) {
            // display field/column names as first row
            fputcsv($output, $row);
        }
    }

    public function getstandards($id){
        $c = new CDbCriteria;
        $c->select ='t.TM_SD_Id, t.TM_SD_Name';
        $c->join = "INNER JOIN tm_teacher_standards tm_teacher_standards ON t.TM_SD_Id = tm_teacher_standards.TM_TE_ST_Standard_Id";
        $c->compare("tm_teacher_standards.TM_TE_ST_User_Id", $id);

        $result=Standard::model()->findAll($c);

        if(count($result)!=0):
            $standardname='';
            foreach($result AS $key=>$res):
                $standardname.=($key==0?$res->TM_SD_Name:','.$res->TM_SD_Name);
            endforeach;
            $standards= $standardname;
        else:
            $standards= "No standards assigned";
        endif;
        return $standards;
    }

    public function actionViewTeachers($id)
    {
        
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        if (isset($_GET['master'])):
            $school = $id;
            $model = User::model()->findByPk($_GET['master']);
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $school . ' AND TM_TH_User_Id=' . $_GET['master']));
            $this->render('teacherview', array(
                'model' => $model,
                'school' => $school,
                'schoolstaff' => $schoolstaff
            ));
        else:
            $this->redirect(array('admin'));
        endif;
    }

    public function actionTeacherPassword($id)
    {
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        $model = new UserChangePassword;
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$_GET['master']));
        if(isset($_POST['UserChangePassword'])) {
            $model->attributes=$_POST['UserChangePassword'];
            if($model->validate()) {
                $new_password = User::model()->notsafe()->findbyPk($_GET['master']);
                $new_password->password = UserModule::encrypting($model->password);
                $new_password->activkey=UserModule::encrypting(microtime().$model->password);
                $new_password->save(false);
                Yii::app()->user->setFlash('passwordchange',UserModule::t("New password is saved."));
                $this->refresh();
            }
        }
        $this->render('teacherpassword',array('model'=>$model,'teacher'=>$teacher));
    }

    public function actionAddStandards($id)
    {
        
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        if (isset($_GET['master'])):
            $school = $id;
            $model = User::model()->findByPk($_GET['master']);
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $school . ' AND TM_TH_User_Id=' . $_GET['master']));
            $schoolstandards = SchoolPlan::model()->findAll(array('condition' => 'TM_SPN_SchoolId=' . $school));
            $techerprevdata=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_Teacher_Id='.$schoolstaff->TM_TH_Id));

            if (isset($_POST['Teachers'])):
                $usertypesselected = $_POST['standards'];
                
                $teacherId = $_POST['Teachers']['TM_TH_Id'];
                $userId = $_POST['User']['id'];
                $techerprevdata=TeacherStandards::model()->deleteAll(array('condition'=>'TM_TE_ST_Teacher_Id='.$teacherId));
                if(count($usertypesselected)>0):
                    for ($i = 0; $i < count($usertypesselected); $i++):
                        $standards=new TeacherStandards();
                        $standards->TM_TE_ST_Standard_Id=$usertypesselected[$i];
                        $standards->TM_TE_ST_Teacher_Id=$teacherId;
                        $standards->TM_TE_ST_User_Id=$userId;
                        //code by amal
                        if($usertypesselected[$i]!=''):
                            $standards->save(false);
                            $model->status=1;
                            $model->update();
                        else:
                            $model->status=0;
                            $model->update();
                        endif;
						//ends
                    endfor;
                endif;
    
                $this->redirect(array('ViewTeachers', 'id' => $school,'master'=>$userId));


            endif;
            $this->render('addstandards', array(
                'model' => $model,
                'school' => $school,
                'schoolstaff' => $schoolstaff,
                'techerprevdata' => $techerprevdata
            ));
        else:
            $this->redirect(array('admin'));
        endif;


    }

    public function actionEditTeachers($id)
    {
        
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        if (isset($_GET['master'])):
            $school = $id;
            $model = User::model()->notsafe()->findByPk($_GET['master']);
            $profile = $model->profile;
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $school . ' AND TM_TH_User_Id=' . $_GET['master']));
            $teacherstandards=TeacherStandards::model()->count(array('condition'=>'TM_TE_ST_Teacher_Id='.$schoolstaff->TM_TH_Id));            
            if (isset($_POST['User'])) {                
                $model->attributes = $_POST['User'];
                $model->username=$_POST['User']['email']; 
                $model->email=$_POST['User']['email'];                                                
                //code by amal
                if($_POST['User']['type']!=''):
                    $model->usertype = $_POST['User']['type'];
                endif;
                //ends
                if($teacherstandards>0):
                    $model->status = $_POST['User']['status'];
                else:
                    $model->status = '0';
                endif;
                $profile->attributes = $_POST['Profile'];
                $schoolstaff->attributes = $_POST['Teachers'];
                if ($model->validate() & $profile->validate()) {
                    $model->save();
                    $profile->save();
                    $schoolstaff->TM_TH_Name = $profile->firstname . $profile->lastname;
                    $schoolstaff->TM_TH_UpdatedBy = Yii::app()->user->id;
                    $schoolstaff->save(false);
                    $this->redirect(array('manageTeachers', 'id' => $model->school_id));
                }
            }
            $this->render('editteachers', array(
                'model' => $model,
                'profile' => $profile,
                'school' => $school,
                'schoolstaff' => $schoolstaff
            ));
        else:
            $this->redirect(array('admin'));
        endif;
    }

    public function actionDeleteTeachers($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = User::model()->notsafe()->findByPk($_GET['master']);
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $model->school_id . ' AND TM_TH_User_Id=' . $model->id));
            $schoolstaff->delete();
            $profile = Profile::model()->findByPk($model->id);
            $profile->delete();
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_POST['ajax']))
                $this->redirect(array('manageTeachers', 'id' => $model->school_id));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAddTeacher($id)
    {
        
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        $model = new User;
        $profile = new Profile;
        $staffschool = new Teachers;
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            //$model->usertype = '9';
            $model->usertype = $_POST['User']['type'];
            $model->school_id = $id;
            $model->username=$_POST['User']['email'];            
            $model->email=$_POST['User']['email'];
            $profile->attributes = ((isset($_POST['Profile']) ? $_POST['Profile'] : array()));
            $staffschool->attributes = ((isset($_POST['Teachers']) ? $_POST['Teachers'] : array()));
            //$user->scenario='teacher'; 
            if ($model->validate() & $profile->validate()) {
                $model->activkey = UserModule::encrypting(microtime() . $model->password);
                $model->password = UserModule::encrypting($model->password);
                $model->createtime = time();
                $model->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
                $model->superuser = 0;
                $model->status = '0';
                if ($model->save()) {
                    $profile->user_id = $model->id;
                    $profile->save(false);
                    $staffschool->TM_TH_User_Id = $model->id;
                    $staffschool->TM_TH_Name = $profile->firstname . $profile->lastname;
                    $staffschool->save(false);
                    $password=$_POST['User']['password'];
                    $this->SendTeacherMail($model->id,$password);
                    $this->redirect(array('AddStandards', 'id' => $id,'master'=>$staffschool->TM_TH_User_Id));
                }
            }
        }
        $this->render('createTeacher', array(
            'model' => $model,
            'profile' => $profile,
            'staffschool' => $staffschool,
            'id' => $id
        ));
    }
    public function SendTeacherMail($id,$password)
    {
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$id));
        $user=User::model()->findByPk($teacher->TM_TH_User_Id);
        $userprofile=Profile::model()->findByPk($teacher->TM_TH_User_Id);
        $school=School::model()->findByPk($teacher->TM_TH_SchoolId);
        $adminEmail = Yii::app()->params['noreplayEmail'];
        $email=$user->email;              
        $message="<p>Dear $userprofile->firstname,</p>
                    <p>You have been added as teacher to school: $school->TM_SCL_Name. Your login details are as below.</p>                    
                    <p><div style='border:solid 1px black;background-color: #E6E6FA;width:450px;height: 150px;padding: 20px;text-align: center;'>
                        <p><b>Username </b>: ".$user->username."</p>
                        <p><b>Password </b>: ".$password."</p>
                    </div></p>
                    <p>Once you login with these details, you can go into settings and change password at any time.</p>
                    <p>Regards,<br>TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "smtpout.secureserver.net"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;
        $mail->Username = "services@tabbiemath.com"; // SMTP username
        $mail->Password = "Ta88ieMe"; // SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($email);
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Teacher Registration ';
        $mail->Body = $message;

        return $mail->Send();
    }
    
    public function actionChangeStandard($id)
    {
        Yii::app()->session['standard'] = $id;
        $this->redirect(array('home'));
    }
    public function actionCreatequickhomework()
    {
        $this->layout = '//layouts/teacher';
        $model=new Schoolhomework(); 
		if(isset($_POST['Schoolhomework']))
		{
			$model->attributes=$_POST['Schoolhomework'];
			if($model->save())
				$this->redirect(array('Quickhomework','id'=>$model->TM_SCH_Id));
		}                       
        $this->render('createquickhomework',array('model'=>$model,'type'=>'quick'));       
    }
    public function actionCreateCustomeHomework()
    {
        $this->layout = '//layouts/teacher';
        $model=new Customtemplate(); 
		if(isset($_POST['Customtemplate']))
		{			
            $model->attributes=$_POST['Customtemplate'];
			if($model->save())
				$this->redirect(array('managecustomquestions','id'=>$model->TM_SCT_Id));
		}                       
        $this->render('customhomework',array('model'=>$model,'type'=>'custom'));       
    }

    public function actionUpdateCustomeHomework($id)
    {
        $this->layout = '//layouts/teacher';
        $model=Customtemplate::model()->findByPk($id);		
        if(isset($_POST['Customtemplate']))
		{
			$model->attributes=$_POST['Customtemplate'];
			if($model->save())
				$this->redirect(array('managecustomquestions','id'=>$model->TM_SCT_Id));
		}
        $this->render('customhomeworkupdate',array('model'=>$model));                 
    }

    public function actionListcustomehomework()
    {
        $this->layout = '//layouts/teacher';       
       	$model=new Customtemplate('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Mock']))
			$model->attributes=$_GET['Mock'];

		$this->render('listcustomehomework',array(
			'model'=>$model,
		));  
    }

    public function actiondeleteHomework($id)
    {
        $model=Customtemplate::model()->findByPk($id);
        if($model->delete())
        {
            $questions=CustomtemplateQuestions::model()->deleteAll(array('condition'=>'TM_CTQ_Custom_Id='.$id));
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            if($model->TM_SCH_Type==1):
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listcustomehomework'));
            else:
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listquickhomework'));
            endif;

    }

    public function actionDeleteassignedhomework($id)
    {
        $model=Schoolhomework::model()->findByPk($id);
        if($model->delete())
        {
            $questions=HomeworkQuestions::model()->deleteAll(array('condition'=>'TM_HQ_Homework_Id='.$id));
            $studenthomeworks=StudentHomeworks::model()->deleteAll(array('condition'=>'TM_STHW_HomeWork_Id='.$id));
            $studentquestions=Studenthomeworkquestions::model()->deleteAll(array('condition'=>'TM_STHWQT_Mock_Id='.$id));
            $notifications=Notifications::model()->deleteAll(array('condition'=>"TM_NT_Item_Id='".$id."' AND TM_NT_Type IN ('HWAssign','HWReminder','HWMarking','HWSubmit')"));
			$homeworkmarking=Homeworkmarking::model()->deleteAll(array('condition'=>'TM_HWM_Homework_Id='.$id));				
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            if($model->TM_SCH_Type==1):
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listcustomehomework'));
            else:
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listquickhomework'));
            endif;

    }
    public function actionManagecustomquestions($id)
    {
        $this->layout = '//layouts/teacher';         
        $model=Customtemplate::model()->findByPk($id);
        $homeworkquestions=CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$id."'",'order'=>'TM_CTQ_Order ASC'));
        if(count($homeworkquestions)>0):
            $HQS='';
            foreach($homeworkquestions AS $key=>$HQ)
            {
                if($key=='0'):
                    $HQS.="'".$MQ->TM_CTQ_Question_Id."'";
                else:
                    $HQS.=",'".$MQ->TM_CTQ_Question_Id."'";
                endif;
            }
            $addedQuestion=$homeworkquestions;
            $condition="AND TM_QN_Id NOT IN (".$HQS.")";
        else:
            $addedQuestion='';
            $condition='';
        endif;
        //$questions=Questions::model()->findAll(array('condition'=>"TM_QN_Publisher_Id='".$model->TM_MK_Publisher_Id."' AND TM_QN_Syllabus_Id='".$model->TM_MK_Syllabus_Id."'  AND TM_QN_Parent_Id='0' AND TM_QN_Standard_Id='".$model->TM_MK_Standard_Id."' AND TM_QN_Status='0' ".$condition));
        $this->render('managequestions',array(
            'model'=>$model,
            //'questions'=>$questions,
            'addedQuestion'=>$addedQuestion
        ));        
    }
    public function actionGetquestions()
    {
       
		$condition='';
        if($_POST['chapter']!=''):
            $condition.=" AND TM_SQ_Chapter_Id='".$_POST['chapter']."'";
        endif;
        /*if($_POST['publisher']!=''):
            $condition.=" AND TM_SQ_Publisher_id='".$_POST['publisher']."'";
        endif;*/
        if($_POST['publisher']!=''):
            if($_POST['publisher']=='0'):
                $condition.=" AND TM_SQ_School_Id='".Yii::app()->session['school']."' AND TM_SQ_Type=0";
            elseif($_POST['publisher']=='1'):
                $condition.=" AND TM_SQ_School_Id='".Yii::app()->session['school']."' AND TM_SQ_Type=1";
				//$condition.=" AND TM_QN_School_Id='0'";
            endif;
        else:
            $condition.=" AND TM_SQ_School_Id='".Yii::app()->session['school']."'";                  
        endif;
        if($_POST['topic']!=''):
            $condition.=" AND TM_SQ_Topic_Id='".$_POST['topic']."'";
        endif;
        if($_POST['type']!=''):
            $condition.=" AND TM_SQ_Question_Type='".$_POST['type']."'";
        endif;
        if($_POST['difficulty']!=''):
            $condition.=" AND TM_QN_Dificulty_Id='".$_POST['difficulty']."'";
        endif;        
        if($_POST['teacher']!=''):
            $condition.=" AND TM_QN_Teacher_Id='".$_POST['teacher']."'";
        endif;       
        if($_POST['pattern']!=''):
            $condition.=" AND TM_QN_Pattern LIKE '%".$_POST['pattern']."%'";
        endif;                
        if($_POST['reference']!=''):
            $condition.=" AND TM_SQ_QuestionReff LIKE '%".$_POST['reference']."%'";
        endif;
        $homeworkquestions=CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$_POST['mock']."'"));
        if(count($homeworkquestions)>0):
            $HQS='';
            foreach($homeworkquestions AS $key=>$HQ)
            {
                if($key=='0'):
                    $HQS.="'".$HQ->TM_CTQ_Question_Id."'";
                else:
                    $HQS.=",'".$HQ->TM_CTQ_Question_Id."'";
                endif;
            }
            $condition.=" AND TM_SQ_Question_Id NOT IN (".$HQS.")";
        endif;
		  
        $questions=SchoolQuestions::model()->with('question')->findAll(array('condition'=>"TM_SQ_Standard_Id='".Yii::app()->session['standard']."' ".$condition.""));         	
        //print_r($questions);exit;
        // AND TM_QN_Status='0'
        $questionlist='';
        if(count($questions)>0):
            foreach($questions AS $question):
                $questionlist.='<tr id="addquestion'.$question->question->TM_QN_Id.'">
                            <td class="button-column">
                                <a class="previewquestion" title="Preview Question" data-id="'.base64_encode($question->question->TM_QN_Id).'" ><span class="glyphicon glyphicon-eye-open"></span></a>
                                <a class="addquestion" title="Add Question"  data-id="'.base64_encode($question->question->TM_QN_Id).'" data-element="addquestion'.$question->question->TM_QN_Id.'"><span class="glyphicon glyphicon-plus"></span></a>
                                
                            </td>                                            
                            <td>'.$question->question->TM_QN_Question.'</td>
                            <td>'.Types::model()->findByPk($question->question->TM_QN_Type_Id)->TM_TP_Name.'</td>
                            <td>'.$question->question->TM_QN_QuestionReff.'</td>
                            <td>'.$question->question->TM_QN_Pattern.'</td>

                        </tr>';
/*                            <td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                            <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>*/
            endforeach;
        else:
            $questionlist='<tr><td colspan="6">No Questions Found</td></tr>';
        endif;
        echo $questionlist;
    }     
/*code by amal*/
    public function actionPreviewquestion()
    {
        $questionid=base64_decode($_POST['question']);
        $question=Questions::model()->findByPk($questionid);
        if($question->TM_QN_Type_Id!='5'):
            $datarow="<div class='row'><div class='col-md-12 col-lg-12' style='margin-bottom:10px '>
                    <div class='featured-article'>
                    <div class='col-md-8'>
                    <div class='block-title'>
                    <span>
                    ".$question->TM_QN_Question."
                    </blockquote>
                    </span>
                    </div></div>";
            if($question->TM_QN_Image!=''):
                $datarow.="<div class='col-md-4'>
                        <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=large&type=question&width=400&height=400'."' alt=''>
                        </div>";
            endif;
            $datarow.="</div><br>";
            $datarow.="<div class='col-md-12 col-lg-12'>";
            if($question->TM_QN_Type_Id=='1' || $question->TM_QN_Type_Id=='2' || $question->TM_QN_Type_Id=='3'):
                $datarow.="<ul class='media-list main-list'>";
                foreach($question->answers AS $answer):
                    $datarow.="<li class='media'>";
                    if($answer->TM_AR_Answer!='' & $answer->TM_AR_Image!=''):
                        $datarow.="<span class='".$answer->TM_AR_Answer!=''?'pull-right':''."' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span>";
                    endif;
                    $datarow.="<div class='media-body ptp25'>".$answer->TM_AR_Answer."</div>";
                    if($answer->TM_AR_Answer=='' & $answer->TM_AR_Image!=''):
                    $datarow.="<span class='".$answer->TM_AR_Answer!=''?'pull-right':''."' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span></li>";
                    endif;
                endforeach;
                $datarow.="<ul>";
            endif;
            $datarow.="</div></div>";
        elseif($question->TM_QN_Type_Id=='5' || $question->TM_QN_Type_Id=='7'):
            $datarow="<div class='row'><div class='col-md-12 col-lg-12' style='margin-bottom:10px '>
                    <div class='featured-article'>
                    <div class='col-md-8'>
                    <div class='block-title'>
                    <span>
                    ".$question->TM_QN_Question."
                    </blockquote>
                    </span>
                    </div></div>";
            if($question->TM_QN_Image!=''):
                $datarow.="<div class='col-md-4'>
                        <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=large&type=question&width=400&height=400'."' alt=''>
                        </div>";
            endif;
            $datarow.="</div></div></div>";
            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
            $countchild=0;
            $childrow='';
            foreach ($questions AS $key=>$childquestion):
                $countchild=$key+1;
                $childrow.="<div class='row'><div class='col-md-12 col-lg-12' style='margin-bottom:10px '>
                    <div class='featured-article'>
                    <div class='col-md-8'>
                    <div class='block-title'>
                    <span>
                    ".$childquestion->TM_QN_Question."
                    </blockquote>
                    </span>
                    </div></div>";
                if($childquestion->TM_QN_Image!=''):
                    $childrow.="<div class='col-md-4'>
                        <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childquestion->TM_QN_Image.'?size=large&type=question&width=400&height=400'."' alt=''>
                        </div>";
                endif;
                $childrow.="</div>";
                $childrow.="<div class='col-md-12 col-lg-12'>";
                if($childquestion->TM_QN_Type_Id=='1' || $childquestion->TM_QN_Type_Id=='2' || $childquestion->TM_QN_Type_Id=='3'):
                    $childrow.="<ul class='media-list main-list parttest".$childquestion->TM_QN_Id."'>";
                    foreach($childquestion->answers AS $childanswer):
                        $childrow.="<li class='media'>";
                        if($childanswer->TM_AR_Answer!='' & $childanswer->TM_AR_Image!=''):
                            $childrow.="<span class='".$childanswer->TM_AR_Answer!=''?'pull-right':''."' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childanswer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span>";
                        endif;
                        $childrow.="<div class='media-body ptp25'>".$childanswer->TM_AR_Answer."</div>";
                        if($childanswer->TM_AR_Answer=='' & $childanswer->TM_AR_Image!=''):
                            $childrow.="<span class='".$childanswer->TM_AR_Answer!=''?'pull-right':''."' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childanswer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span>";
                        endif;
                        $childrow.="</li>";
                    endforeach;
                    $childrow.="</ul>";
                endif;
                $childrow.="</div></div></div>";
            endforeach;
            $datarow.=$childrow;
            //$datarow.="</div>";
        endif;
        $retarray=array('success'=>'yes','result'=>$datarow);
        echo json_encode($retarray);
    }
    /*ends*/

    public function actionAddquestion()
    {
        $mockid=base64_decode($_POST['mock']);
        $questionid=base64_decode($_POST['question']);
        $homeworkquestion=CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."' AND TM_CTQ_Question_Id='".$questionid."'"));
        $homeworkqstcount=count(CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."'")));
        if(count($homeworkquestion)==0)
        {
            $ordernumber=CustomtemplateQuestions::model()->find(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."'",'order'=>'TM_CTQ_Order DESC'));
            $order='';
            if(count($ordernumber)>0):
            $order=$ordernumber->TM_CTQ_Order+1;
            else:
                $order=1;
            endif;
            $question=Questions::model()->findByPk($questionid);
            $questionhomework=new CustomtemplateQuestions();
            $questionhomework->TM_CTQ_Custom_Id=$mockid;
            $questionhomework->TM_CTQ_Question_Id=$questionid;
            $questionhomework->TM_CTQ_Type=$question->TM_QN_Type_Id;
            $questionhomework->TM_CTQ_Order=$order;
            $questionhomework->save(false);
            $returnrow='<tr id="removequestion'.$question->TM_QN_Id.'"class="order'.$questionhomework->TM_CTQ_Order.'qn" data-rawid="'.$question->TM_QN_Id.'">
                        <td>
                        <INPUT TYPE="image" id="orderlimkup'.$question->TM_QN_Id.'" class="order_link" data-info="up" SRC="'.Yii::app()->request->baseUrl.'/images/up.gif" ALT="SUBMIT" data-id="'.$questionhomework->TM_CTQ_Custom_Id.'" data-order="'.$questionhomework->TM_CTQ_Order.'" data-qnId="'.$question->TM_QN_Id.'">
                        <INPUT TYPE="image" id="orderlimkdown'.$question->TM_QN_Id.'" class="order_link" data-info="down" SRC="'.Yii::app()->request->baseUrl.'/images/down.gif" ALT="SUBMIT" data-id="'.$questionhomework->TM_CTQ_Custom_Id.'" data-order="'.$questionhomework->TM_CTQ_Order.'" data-qnId="'.$question->TM_QN_Id.'">
                        </td>
                        <td class="td-actions">
                            <a class="removequestion" title="Remove Question"  data-id="'.base64_encode($question->TM_QN_Id).'" data-element="removequestion'.$question->TM_QN_Id.'"><span class="glyphicon glyphicon-remove"></span></a>
                        </td>
                        <td>'.$question->TM_QN_Question.'</td>
                        <td>'.Types::model()->findByPk($question->TM_QN_Type_Id)->TM_TP_Name.'</td>
                        <td>'.$question->TM_QN_QuestionReff.'</td>
                        <td>'.$question->TM_QN_Pattern.'</td>

                        
                    </tr>';
            /*<td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                        <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>*/
            $count=CustomtemplateQuestions::model()->count(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."'"));                        
            $retarray=array('success'=>'yes','insertrow'=>$returnrow,'count'=>$count);
            echo json_encode($retarray);
        }
        else
        {
            echo "Question Already Added";
        }
    }

    public function actionRemovequestion()
    {
        if(isset($_POST['mock']))
        {
            $mockid=base64_decode($_POST['mock']);
            $questionid=base64_decode($_POST['question']);                  
            $mockquestion=CustomtemplateQuestions::model()->find(array('condition'=>'TM_CTQ_Custom_Id='.$mockid.' AND TM_CTQ_Question_Id='.$questionid));
            $questionorder=$mockquestion->TM_CTQ_Order;
            if($mockquestion->delete(false))
            {
                $ordermock=CustomtemplateQuestions::model()->findAll(array('condition'=>'TM_CTQ_Custom_Id='.$mockid.' AND TM_CTQ_Order >'.$questionorder,'order'=>'TM_CTQ_Order ASC'));
                $ordermockcount=CustomtemplateQuestions::model()->count(array('condition'=>'TM_CTQ_Custom_Id='.$mockid));
                $neworder=$questionorder;
                 foreach($ordermock AS $order):
                        $order->TM_CTQ_Order=$neworder;
                        $order->save(false);
                        $neworder++;
                 endforeach;                  
                $retarray=array('success'=>'yes','count'=>$ordermockcount);
                echo json_encode($retarray);
            }
            else
            {
                $retarray=array('success'=>'no');
                echo json_encode($retarray);
            }
        }
    }

    public function actionQuickhomework()
    {
        $this->layout = '//layouts/teacher';
        $standard=Yii::app()->session['standard'];
        $syllabus=Yii::app()->session['syllabus'];
        $school=Yii::app()->session['school'];        
        $chapters = Chapter::model()->findAll(array('condition' => "TM_TP_Syllabus_Id='$syllabus' AND TM_TP_Standard_Id='$standard'",'order'=>'TM_TP_order ASC'));
        
        if (isset($_POST['name'])):
        
      
            $criteria = new CDbCriteria;
            $criteria->addInCondition('TM_TP_Id', $_POST['quickchapter']);
            $selectedchapters = Chapter::model()->findAll($criteria);
            $schoolhw=new Schoolhomework();
            $schoolhw->TM_SCH_Name=$_POST['name'];
            $schoolhw->TM_SCH_School_Id=$school;
            $schoolhw->TM_SCH_Standard_Id=$standard;
            $schoolhw->TM_SCH_Syllabus_Id=$syllabus;
            $schoolhw->TM_SCH_Type=0;
            $schoolhw->TM_SCH_Status=0;
            $schoolhw->TM_SCH_CreatedBy=Yii::app()->user->id;
            $schoolhw->TM_SCH_ShowSolution=$_POST['solution'];            
            $schoolhw->save(false);
            $count = 1;
            $totalquestions = 0;
            foreach ($selectedchapters AS $key => $chapter):

                if ($_POST['quickchaptertotal' . $chapter->TM_TP_Id] != '0'):
                    $quicklimits = $this->GetQuickLimits($_POST['quickchaptertotal' . $chapter->TM_TP_Id]);
                    $topics = '';
                    for ($i = 0; $i < count($_POST['quicktopic' . $chapter->TM_TP_Id]); $i++):
                        $topics = $topics . ($i == 0 ? $_POST['quicktopic' . $chapter->TM_TP_Id][$i] : "," . $_POST['quicktopic' . $chapter->TM_TP_Id][$i]);
                    endfor;
                    //$topics='"'.$topics.'"';
                    $totalquestions = $totalquestions + $_POST['quick' . $chapter->TM_TP_Id];
                    if ($quicklimits['basic'] != 0):
                        $Syllabus = $syllabus;
                        $Standard = $standard;
                        $Chapter = $chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $homework = $schoolhw->TM_SCH_Id;
                        $dificulty = '1';
                        $limit = $quicklimits['basic'];
                        $command = Yii::app()->db->createCommand("CALL NewSetHWQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Homework,:dificulty,:questioncount,:school,'0',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit", $limit);
                        $command->bindParam(":Homework", $homework);
                        $command->bindParam(":dificulty", $dificulty);
                        $command->bindParam(":questioncount", $count);
                        $command->bindParam(":school", $school);
                        $command->query();
                        $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();                        
                    endif;
                    if ($quicklimits['intermediate'] != 0):
                        $Syllabus = $syllabus;
                        $Standard = $standard;
                        $Chapter = $chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $homework = $schoolhw->TM_SCH_Id;
                        $dificulty = '2';
                        $limit = $quicklimits['intermediate'];
                        $command = Yii::app()->db->createCommand("CALL NewSetHWQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Homework,:dificulty,:questioncount,:school,'0',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit", $limit);
                        $command->bindParam(":Homework", $homework);
                        $command->bindParam(":dificulty", $dificulty);
                        $command->bindParam(":questioncount", $count);
                        $command->bindParam(":school", $school);
                        $command->query();
                        $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                    endif;
                    if ($quicklimits['advanced'] != 0):
                        $Syllabus = $syllabus;
                        $Standard = $standard;
                        $Chapter = $chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $homework = $schoolhw->TM_SCH_Id;
                        $dificulty = '3';
                        $limit = $quicklimits['advanced'];
                        $command = Yii::app()->db->createCommand("CALL NewSetHWQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Homework,:dificulty,:questioncount,:school,'0',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit", $limit);
                        $command->bindParam(":Homework", $homework);
                        $command->bindParam(":dificulty", $dificulty);
                        $command->bindParam(":questioncount", $count);
                        $command->bindParam(":school", $school);
                        $command->query();
                        $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                endif;
            endforeach;
       
            if ($count != '0'):
                $command = Yii::app()->db->createCommand("CALL SetHWQuestionsNum(:Homework)");
                $command->bindParam(":Homework", $schoolhw->TM_SCH_Id);
                $command->query();                
                $this->PublishHomework($schoolhw->TM_SCH_Id,$_POST['assigngroups'],$_POST['duedate'],$_POST['comments'],$_POST['publishdate'],$_POST['type'],$_POST['solution']);
                $this->redirect(array('Home'));
            endif;
            
        endif;
        $this->render('quickhomework', array('chapters' => $chapters));
    }

    public function actionListquickhomework()
    {
        $this->layout = '//layouts/teacher';
        $model=new Schoolhomework('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Mock']))
            $model->attributes=$_GET['Mock'];
        $this->render('listquickhomework',array(
            'model'=>$model,
        ));
    }

    public function actionPublishHomework()
    {
        $school=Yii::app()->session['school'];
        $homeworkid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
		$homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                $grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal                    
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
                    if($_POST['publishdate']==date('Y-m-d')):
                        $notification=new Notifications();
                        $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                        $notification->TM_NT_Type='HWAssign';
                        $notification->TM_NT_Item_Id=$homeworkid;
                        $notification->TM_NT_Target_Id=Yii::app()->user->id;
                        $notification->TM_NT_Status='0';                        
                        $notification->save(false);
                    endif;
                    $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));                    
                    foreach($hwquestions AS $hwquestion):
                        $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                        $studhwqstns=New Studenthomeworkquestions();
                        $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                        $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                        $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                        $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                        $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                        $studhwqstns->save(false);
                        $questions=Questions::model()->findAllByAttributes(array(
                            'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                        ));
                        //print_r($questions);
                        if(count($questions)!=0):
                            foreach($questions AS $childqstn):
                                $studhwqstns=New Studenthomeworkquestions();
                                $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                $studhwqstns->save(false);
                            endforeach;
                        endif;
                    endforeach;                    
                    $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhomework->save(false);                    
                endforeach;
            }
        endif;
            $homework = Schoolhomework::model()->findByPk($homeworkid);
            $homework->TM_SCH_Status='1';
            $homework->TM_SCH_DueDate=$_POST['dudate'];
            $homework->TM_SCH_Comments=$_POST['comments'];
            $homework->TM_SCH_PublishDate=$_POST['publishdate'];
            $homework->TM_SCH_PublishType=$_POST['type'];
            $homework->TM_SCH_ShowSolution=$_POST['solution'];
			$homework->TM_SCH_CreatedBy=Yii::app()->user->id;
            $homework->save(false);                
    }    
    public function GetQuickLimits($total)
    {
        $mode = $total % 3;
        if ($mode == 0):
            $count = $total / 3;
            return array('basic' => $count, 'intermediate' => $count, 'advanced' => $count);
        elseif ($mode == 1):
            $count = round($total / 3);
            return array('basic' => $count, 'intermediate' => $count + 1, 'advanced' => $count);
        elseif ($mode == 2):
            $count = $total / 3;
            return array('basic' => ceil($count), 'intermediate' => ceil($count), 'advanced' => floor($count));
        endif;

    }

    public function GetChapterTotal($chapter, $type)
    {
        $criteria = new CDbCriteria;
        //$criteria->condition = 'TM_QN_Topic_Id=:chapter AND TM_QN_Status=0 AND TM_QN_Parent_Id=0 AND TM_QE_Exam_Id=' . $type;
        $criteria->condition = 'TM_QN_Topic_Id=:chapter AND TM_QN_Parent_Id=0 AND TM_QE_Exam_Id=' . $type;
        $criteria->params = array(':chapter' => $chapter);
        $questions = Questions::model()->with('exams')->count($criteria);
        return $questions;
    }

    public function actionGetTopics()
    {
        $arr = explode(',', $_POST['topicids']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_TP_Id', $arr);
        $model = Chapter::model()->findAll($criteria);
        $topic = '';
        foreach ($model as $key => $value) {
            $topic = ($key == '0' ? $value->TM_TP_Name : $topic . ',' . $value->TM_TP_Name);
        }
        echo $topic;
    }

/*    public function actionGetGroups()
    {
        $groups=SchoolGroups::model()->findAll(array('condition' => "TM_SCL_Id='".$_POST['id']."'"));
        $count=count($groups);
        echo $count;
    }
*/

    public function actionGetGroups()
    {
        $groups=SchoolGroups::model()->findAll(array('condition' => "TM_SCL_Id='".$_POST['id']."'"));
        $count=count($groups);
        /*$paperquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='".$_POST['homeworkid']."' AND TM_HQ_Type IN(6,7)"));
        $papercount=count($paperquestions);
        $onlineqstns=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='".$_POST['homeworkid']."' AND TM_HQ_Type NOT IN(6,7)"));
        $onlinecount=count($onlineqstns);
        if($papercount!=0 & $onlinecount==0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' disabled>Online & Worksheet</label>";
        elseif($papercount!=0 & $onlinecount!=0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online & Worksheet</label>";
        else:
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online & Worksheet</label>";
        endif;*/
        $schoolhw=Schoolhomework::model()->findByPk($_POST['homeworkid']);
        $ondate=$schoolhw->TM_SCH_PublishDate;
        $name=$schoolhw->TM_SCH_Name;
        $duedate=$schoolhw->TM_SCH_DueDate;
        $type=$schoolhw->TM_SCH_PublishType;
        if($type==1):
            $data="<label class='radio-inline'><input type='radio' name='type' value='1' checked='checked'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2'>Online & Worksheet</label>";
        else:
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online & Worksheet</label>";
        endif;
        $solution=$schoolhw->TM_SCH_ShowSolution;
        if($solution==1):
            $sol="<label class='radio-inline'><input type='radio' name='solution' value='1' checked='checked'>Yes</label><label class='radio-inline'><input type='radio' name='solution' value='0'>No</label>";
        else:
            $sol="<label class='radio-inline'><input type='radio' name='solution' value='1'>Yes</label><label class='radio-inline'><input type='radio' name='solution' value='0' checked='checked'>No</label>";
        endif;
        $comments=$schoolhw->TM_SCH_Comments;
        $arr=array('result' => $data,'count'=>$count,'name'=>$name,'ondate'=>date('Y-m-d'),'duedate'=>date("Y-m-d", strtotime('tomorrow')),'solution'=>$sol,'comments'=>$comments);
        echo json_encode($arr);
    }  
   public function actionGetGroupsCustom()
    {
        $groups=SchoolGroups::model()->findAll(array('condition' => "TM_SCL_Id='".$_POST['id']."'"));
        $count=count($groups);
        $paperquestions=CustomtemplateQuestions::model()->findAll(array('condition' => "TM_CTQ_Custom_Id='".$_POST['homeworkid']."' AND TM_CTQ_Type IN(6,7)"));
        $papercount=count($paperquestions);
        $onlineqstns=CustomtemplateQuestions::model()->findAll(array('condition' => "TM_CTQ_Custom_Id='".$_POST['homeworkid']."' AND TM_CTQ_Type NOT IN(6,7)"));
        $onlinecount=count($onlineqstns);
        if($papercount!=0 & $onlinecount==0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='1' checked='checked'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' disabled>Online & Worksheet</label>";
        elseif($papercount!=0 & $onlinecount!=0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online & Worksheet</label>";
        else:
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online & Worksheet</label>";
        endif;
        //<option value='0'>Online only</option>
        $arr=array('result' => $data,'count'=>$count);
        echo json_encode($arr);
    }      
    public function actionGetmarkers()
    {            
        //$users=User::model()->count(array('join'=>'LEFT JOIN tm_homeworkmarking AS b ON TM_HWM_User_Id=t.id','condition'=>"usertype='11' AND school_id=".$_POST['id']." AND status=1 AND b.TM_HWM_Homework_Id=".$_POST['homework']));
        $users=User::model()->count(array('condition'=>"usertype IN (11,6,9,12) AND school_id=".$_POST['id']." AND status=1"));
        $data['count']=$users;
        if($users!==0):    
            $usersassigned=User::model()->findAll(array('join'=>'LEFT JOIN tm_homeworkmarking AS b ON TM_HWM_User_Id=t.id','condition'=>"usertype IN (11,6,9,12) AND school_id=".$_POST['id']." AND status=1 AND b.TM_HWM_Homework_Id=".$_POST['homework']));
            $users='';
            foreach($usersassigned AS $key=>$assigned):
                $users.=($key=='0'?$assigned->profile->firstname.' '.$assigned->profile->lastname:','.$assigned->profile->firstname.' '.$assigned->profile->lastname);                
            endforeach;
            $data['users']=$users;
        endif;
        echo json_encode($data);
    }
    public function Groups($id)
    {
        $plan=SchoolGroups::model()->GetPlanId('1');
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_SCL_Id=' . $id . ' AND TM_SCL_PN_Id= '.$plan;
        $groups = SchoolGroups::model()->findAll($criteria);
        $groupsjson = '';
        if (count($groups) > 0):
            //$groupsjson .='{"id":0, "name":"All"}';
            foreach ($groups AS $key => $group):
                $groupstuents=GroupStudents::model()->count(array('condition'=>'TM_GRP_Id='.$group->TM_SCL_GRP_Id.''));
                if($groupstuents>0)
                {
                    $groupsjson .= ($groupsjson == '' ? '{"id":' . $group->TM_SCL_GRP_Id . ', "name":"' . $group->TM_SCL_GRP_Name . ' "}' : ',{"id":' . $group->TM_SCL_GRP_Id . ', "name":"' . $group->TM_SCL_GRP_Name . ' "}');
                }
            endforeach;
        endif;
        return $groupsjson;
    }      
    public function Markers($id)
    {
        $users=User::model()->findAll(array('condition'=>"usertype IN (11,6,9,12) AND school_id=".$id." AND status=1 AND id!=".Yii::app()->user->id,'limit'=>'25'));
        $groupsjson = '';        
        if (count($users) > 0):
        foreach ($users AS $key => $user):                
            $groupsjson .= ($groupsjson == '' ? '{"id":' . $user->id . ', "name":"' . $user->profile->firstname.' '.$user->profile->lastname. ' ('.$user->username.') "}' : ',{"id":' . $user->id . ', "name":"' . $user->profile->firstname.' '.$user->profile->lastname . ' ('.$user->username.') "}');
        endforeach;
        endif;        
        return $groupsjson;
    }   
    /** Teacher marking action **/
    public function actionMarkhomework($id)
    {
        $this->layout = '//layouts/teachermarkig';
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $notstartedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));                        
        $homework=Schoolhomework::model()->findByPk($id);
        $this->render('teachermarking',array(
            'startedstudents'=>$startedstudents,
            'homework'=>$homework,
            'notstartedstudents'=>$notstartedstudents
        )); 
          
    }
    
    //code by amal
    public function actionSolution()
    {
        $student=$_POST['student'];
        $homework=$_POST['homework'];
        /*$hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $solution='';
        foreach($hwquestions AS $hwq):
            $question=Questions::model()->find(array('condition'=>'TM_QN_Id='.$hwq->TM_STHWQT_Question_Id));
            $solution.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4>$question->TM_QN_Solutions</li>";
        endforeach;
        echo $solution;*/
        $homeworkqstns=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $username=User::model()->findByPk(Yii::app()->user->id)->username;
        $homework=Schoolhomework::model()->findByPK($homework)->TM_SCH_Name;
        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>Homework : ".$homework."<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";
        foreach ($homeworkqstns AS $key => $homeworkqstn):
            $homeworkqstncount = $key + 1;
            $question = Questions::model()->findByPk($homeworkqstn->TM_STHWQT_Question_Id);
            //code by amal
            if($homeworkqstn->TM_STHWQT_Type!=5):
                if($homeworkqstn->TM_STHWQT_Type==4):
                    $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_STHWQT_Question_Id' "));
                    $marks=$answers->TM_AR_Marks;
                else:
                    $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_STHWQT_Question_Id' AND TM_AR_Correct='1' "));
                    $marks=$answers->TM_AR_Marks;
                endif;
            endif;
            if($homeworkqstn->TM_STHWQT_Type==5):
                $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_STHWQT_Question_Id' "));
                $childtotmarks=0;
                foreach($childqstns AS $childqstn):
                    //$childhwqstns = Questions::model()->findByPk($childqstn->TM_STHWQT_Question_Id);
                    if($childqstn->TM_QN_Type_Id==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                        $childmarks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                        $childmarks=$answers->TM_AR_Marks;
                    endif;
                    $childtotmarks=$childtotmarks + $childmarks;
                endforeach;
                $marks=$childtotmarks;
            endif;
            if($homeworkqstn->TM_STHWQT_Type==6):
                $marks=$question->TM_QN_Totalmarks;
            endif;
            //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $marks . " &nbsp;&nbsp;Ref : ".$question->TM_QN_QuestionReff."</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;                        
                                            
                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html .= "<tr><td colspan='3' style='text-decoration: underline;'>".$childquestion->TM_QN_Question." </td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            foreach($childquestion->answers AS $answer):
                                $html.="<tr><td>$answer->TM_AR_Answer</td></tr>";
                            endforeach;
                        endforeach;
                    endif;
                    $html.="<tr><td colspan='3'><b>Solution</b></td></tr><tr><td colspan='3'>$question->TM_QN_Solutions</td></tr>";
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $displaymark . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $html.="<tr><td colspan='3'><b>Solution</b></td></tr><tr><td colspan='3'>$question->TM_QN_Solutions</td></tr>";
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
        endforeach;
        $html .= "</table>";
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                <td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
                <td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
                <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                </tr></table>';

        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
        <td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Educational Solutions PVT Ltd.</span></td>
        </tr></table>');

        //echo $html;exit;
        ///$name=Yii::app()->request->baseUrl.'/homework/SOLUTION'.time().'.pdf';
        $name='SOLUTION'.time().'.pdf';        
        $mpdf->WriteHTML($html);
        $mpdf->Output($name,'F');
        $answer='<object data="'.Yii::app()->request->baseUrl.'/'.$name.'" type="application/pdf" width="100%" height="100%">                  
                </object>';  
        echo $answer;                      
        //$mpdf->Output($name, 'F');        
    }

public function actionAnswer()
    {
        $student=$_POST['student'];
        $homework=$_POST['homework'];
        $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $answer='<ul style="list-style-type: decimal;">';        
        foreach($hwquestions AS $hwq):
            $studanswer='';
            $question=Questions::model()->find(array('condition'=>'TM_QN_Id='.$hwq->TM_STHWQT_Question_Id));
            if($hwq['TM_STHWQT_Type']==4):
                $studanswer=$hwq->TM_STHWQT_Answer;
                $correctans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$hwq->TM_STHWQT_Question_Id.''));
                $correctanswer=$correctans->TM_AR_Answer;
			elseif($hwq['TM_STHWQT_Type']==1):
                if($hwq['TM_STHWQT_Status']==2):
                    $studanswer='Skipped';
                else:
                    $studans=explode(',', $hwq->TM_STHWQT_Answer);
                    $studanswer='';
                    foreach($studans AS $studansw):
                        $studentanswer=Answers::model()->findByPk($studansw);
                        $studanswer.=$studentanswer->TM_AR_Answer.'<br>';
                    endforeach;
                endif;
            elseif($hwq['TM_STHWQT_Type']==5):
                $childquestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$hwq->TM_STHWQT_Question_Id'"));
                $childlist='<ul style="list-style-type: decimal;">';
                foreach ($childquestions AS $key => $childs):
                    $childanswer=Studenthomeworkquestions::model()->find(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Question_Id='.$childs->TM_QN_Id.' AND TM_STHWQT_Mock_Id='.$homework,))->TM_STHWQT_Answer;
                    if($childs->TM_QN_Type_Id==4):
                    	$childstudanswer=$childanswer;
                    	//$childcorrectans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$childs->TM_QN_Id.''));
                    	//$childcorrectanswer=$childcorrectans->TM_AR_Answer;
                    else:
                    	$childstudentanswer=Answers::model()->findByPk($childanswer);                     
                    	$childstudanswer=$childstudentanswer->TM_AR_Answer;
                    	$childcorrectans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$childs->TM_QN_Id.' AND TM_AR_Correct=1'));
                    	$childcorrectanswer=$childcorrectans->TM_AR_Answer;
                    endif;                
                    $childlist.="<li><h4 class='media-heading'>$childs->TM_QN_Question </h4></li>".($childstudanswer==''?'<p>Not Answered</p>': '<p><b>Answer</b></p>'.$childstudanswer)."</li>";
				endforeach; 
                $childlist.='</ul>';
			else:
                $studentanswer=Answers::model()->findByPk($hwq->TM_STHWQT_Answer);
                $studanswer=$studentanswer->TM_AR_Answer;
                $correctans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$hwq->TM_STHWQT_Question_Id.' AND TM_AR_Correct=1'));
                $correctanswer=$correctans->TM_AR_Answer;
            endif;
            if($hwq['TM_STHWQT_Type']==6 || $hwq['TM_STHWQT_Type']==7):
                $answer.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4></li>";
            elseif($hwq['TM_STHWQT_Type']==5):
                $answer.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4>".$childlist."</li>";                
            else:
                $answer.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4>".($studanswer==''?'<p>Not Answered</p>': '<p><b>Answer</b></p>'.$studanswer)."</li>";
            endif;
        endforeach;
        /*if(count($hwanswers)>0 ||$hwanswers->TM_STHWAR_Filename!=''):
            $answer='<object data="'.Yii::app()->request->baseUrl.'/homework/'.$hwanswers->TM_STHWAR_Filename.'" type="application/pdf" width="100%" height="100%">
                  <p>Click on the link if you face problem Viewing the <a href="'.Yii::app()->request->baseUrl.'/homework/'.$hwanswers->TM_STHWAR_Filename.'">to the PDF!file</a></p>
                </object>';
        else:
            $answer='<p>No Answer sheet uploaded</p>';            
        endif;*/
        echo '</ul>'.$answer;
    }
    public function CheckAnswersheetCount($student,$homework)
    {
        $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        if(count($hwanswers)>0 ||$hwanswers->TM_STHWAR_Filename!=''):
            return true;            
        else:
            return false;            
        endif;        
    }
    public function GetAnswersheetCount($student,$homework)
    {
        $hwanswers=Studenthomeworkanswers::model()->findAll(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        return $hwanswers;
    }    
    public function CheckAnswersheet($student,$homework)
    {
      $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        if(count($hwanswers)>0 ||$hwanswers->TM_STHWAR_Filename!=''):
            return Yii::app()->request->baseUrl.'/homework/'.$hwanswers->TM_STHWAR_Filename;
        else:
            return false;            
        endif;        
    }
    //ends
    public function actionSavemarks()
    {
        $student=$_POST["student"];
        $homework=$_POST["homework"];
        $totalmarks=0;
        $homeworkdata=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homework.' AND TM_STHW_Student_Id='.$student));
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));

        foreach($hwquestions AS $hwq):
            /*if($hwq->TM_STHWQT_Marks!=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id]):
                $hwq->TM_STHWQT_Marks=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id];
                $hwq->save(false);
            endif;*/
            $hwq->TM_STHWQT_Marks=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id];
            $totalmarks=$totalmarks+$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id];
            $hwq->save(false);
        endforeach;
		$homeworkdata->TM_STHW_Marks=$totalmarks;

		if($homeworkdata->TM_STHW_TotalMarks!=0):
            $percentage = ($totalmarks / $homeworkdata->TM_STHW_TotalMarks) * 100;
        else:
            $percentage=0;
        endif;
		$homeworkdata->TM_STHW_Percentage=$percentage;		
		$homeworkdata->save(false);              
        echo "saved";
    }
    public function actionMarkcomplete()
    {
        $studentvalue=$_POST["student"];
        $homeworkvalue=$_POST["homework"];
        $homework=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homeworkvalue.' AND TM_STHW_Student_Id='.$studentvalue));
        $homework->TM_STHW_Status='5';
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$studentvalue.' AND TM_STHWQT_Mock_Id='.$homeworkvalue,'order'=>'TM_STHWQT_Order ASC'));
		$totalmarks=0;
        foreach($hwquestions AS $hwq):
            if($hwq->TM_STHWQT_Marks!=$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id]):
                $hwq->TM_STHWQT_Marks=$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id];
                $hwq->save(false);
            endif;
			$totalmarks=$totalmarks+$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id];
        endforeach;
		$homework->TM_STHW_Marks=$totalmarks;
		if($homework->TM_STHW_TotalMarks!=0):
            $percentage = ($totalmarks / $homework->TM_STHW_TotalMarks) * 100;
        else:
            $percentage=0;
        endif;
		$homework->TM_STHW_Percentage=$percentage;		
		$homework->save(false);
        echo "saved";
    }
    public function actioneditmarkcomplete()
    {
        $studentvalue=$_POST["student"];
        $homeworkvalue=$_POST["homework"];
        $homework=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homeworkvalue.' AND TM_STHW_Student_Id='.$studentvalue));
        $homework->TM_STHW_Status='4';
        $homework->save(false);        
    }
    public function GetAverage($student,$homework)
    {
        $homework=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homework.' AND TM_STHW_Student_Id='.$student));
        return round($homework->TM_STHW_Percentage);     
    }
    public function actionSaveStatus($id)
    {
        $model=Customtemplate::model()->findByPk($id);
        if($model->TM_SCT_Status==0):
            $model->TM_SCT_Status=1;
        else:
            $model->TM_SCT_Status=0;
        endif;
        $model->save(false);
        $this->redirect(array('managecustomquestions','id' => $id));

    }
    public function actionCompleteallmarking($id)
    {        
        $homework=Schoolhomework::model()->findByPk($id);
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)'));
        foreach($startedstudents AS $started):
            $started->TM_STHW_Status='5';
			$hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$started->TM_STHW_Student_Id.' AND TM_STHWQT_Mock_Id='.$id,'order'=>'TM_STHWQT_Order ASC'));
			$totalmarks=0;
			foreach($hwquestions AS $hwq):
				$totalmarks=$totalmarks+$hwq->TM_STHWQT_Marks;
			endforeach;	
			$started->TM_STHW_Marks=$totalmarks;
			if($started->TM_STHW_TotalMarks!=0):
				$percentage = ($totalmarks / $started->TM_STHW_TotalMarks) * 100;
			else:
				$percentage=0;
			endif;
			$started->TM_STHW_Percentage=$percentage;							
            $started->save(false);
            $pointlevels = $this->AddPoints($started->TM_STHW_Marks, $id, 'Homework', $started->TM_STHW_Student_Id);
        endforeach;
        $homework->TM_SCH_Status='2';
        $homework->TM_SCH_CompletedOn=date('Y-m-d h:i:s');
        $homework->save(false);
        $assignedusers=Homeworkmarking::model()->findAll(array('condition'=>'TM_HWM_Homework_Id='.$id.''));
        foreach($assignedusers AS $assigned):
            $assigned->TM_HWM_Status=1;
            $assigned->save(false);
        endforeach;
        if(Yii::app()->user->isTeacher()):
            $this->redirect(array('home'));
        elseif(Yii::app()->user->isMarker()):
            $this->redirect(array('staffhome'));
        endif;

    }
    public function actionSendreminder()
    {
        $notification=new Notifications();
        $notification->TM_NT_User=$_POST['student'];
        $notification->TM_NT_Type='HWReminder';
        $notification->TM_NT_Item_Id=$_POST['homework'];
        $notification->TM_NT_Target_Id=Yii::app()->user->id;
        $notification->TM_NT_Status='0';
        $notification->save(false);        
        echo "yes";
    }
    public function GetQuestionColumnsCompleted($homework,$student)
    {
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $returnraw='';
        $count=1;
        foreach($hwquestions AS $hwq):
            $totalmarks=$this->Getmarks($hwq->TM_STHWQT_Question_Id);
            $checkclass='';
            $returnraw.='<td><figure class="thumbnail completed">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
                            <figcaption class="text-center">'.$hwq->TM_STHWQT_Marks.'</figcaption>                           																														
						</figure>
					</td>';  
            $count++;          
        endforeach;
        return $returnraw;        
                
    }
    public function GetQuestionColumns($homework,$student)
    {
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $returnraw='';
        $count=1;
        foreach($hwquestions AS $hwq):
            $totalmarks=$this->Getmarks($hwq->TM_STHWQT_Question_Id);
            $checkclass='';
            if($hwq->TM_STHWQT_Type=='6' || $hwq->TM_STHWQT_Type=='7'):
                $checkclass='myred';
            elseif($hwq->TM_STHWQT_Marks==$totalmarks):
                $checkclass='mygreen';
            elseif(($hwq->TM_STHWQT_Type!='6' || $hwq->TM_STHWQT_Type!='7') & $hwq->TM_STHWQT_Marks!=$totalmarks):
                $checkclass='myyellow';
            endif;
            $returnraw.='<td><figure class="thumbnail '.$checkclass.'">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
							<span class="badge mybdg">'.$totalmarks.'</span>
                            <input type="hidden" id="totmarks'.$hwq->TM_STHWQT_Question_Id.'" value="'.$totalmarks.'">
							<input class="form-control setmark'.$student.' mark" data-id="'.$hwq->TM_STHWQT_Question_Id.'" id="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" name="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" value="'.$hwq->TM_STHWQT_Marks.'" type="text">
						</figure>
						<span id="markinfo'.$hwq->TM_STHWQT_Question_Id.'" style="color:red;display: none;"></span>
					</td>';  
            $count++;          
        endforeach;
        return $returnraw;
        
    }
    public function Getmarks($questionid)
    {
            $question = Questions::model()->findByPk($questionid);
            if ($question->TM_QN_Parent_Id == '0'):
                if($question->TM_QN_Type_Id!='5' && $question->TM_QN_Type_Id!='6' && $question->TM_QN_Type_Id!='7'):
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                    $total = 0;
                    foreach ($marks AS $key => $marksdisplay):
                        $total = $total + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                elseif($question->TM_QN_Type_Id=='5'):
                    $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                    $total=0;                    
                    foreach ($childquestions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($marks AS $key => $marksdisplay):
                            $total = $total + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                    endforeach;
                elseif($question->TM_QN_Type_Id=='6'):
                    $total=$question->TM_QN_Totalmarks;
                elseif($question->TM_QN_Type_Id=='7'):
                    $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                    $total=0;                    
                    foreach ($childquestions AS $childquestion):
                        $total = $total + (float)$childquestion->TM_QN_Totalmarks;
                    endforeach;
                endif;
            endif;
            return $total;
    }
    /** Teacher marking action ends **/
    /** assign home work for marking**/
    public function actionAssignmarking()
    {
        $school=Yii::app()->session['school'];
        $homeworkid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
        for($i=0;$i<count($groupid);$i++)
        {
            $homework=new Homeworkmarking();
            $homework->TM_HWM_Homework_Id=$homeworkid;
            $homework->TM_HWM_User_Id= $groupid[$i];  
            $homework->TM_HWM_Assigned_On=date('Y-m-d');
            $homework->TM_HWM_Assigned_By=Yii::app()->user->id;
            $homework->TM_HWM_Status='0';
            $marker=Homeworkmarking::model()->find(array('condition' => "TM_HWM_Homework_Id='$homeworkid' AND TM_HWM_User_Id='$groupid[$i]' "));
            $count=count($marker);
            if($count==0)
            {
                $homework->save(false);
                $notification=new Notifications();
                $notification->TM_NT_User=$groupid[$i];
                $notification->TM_NT_Type='HWMarking';
                $notification->TM_NT_Item_Id=$homeworkid;
                $notification->TM_NT_Target_Id=Yii::app()->user->id;
                $notification->TM_NT_Status='0';
                $notification->save(false);
            }
        }       
    }   
    /** assign home work for marking ends **/
    /** staff function*/
    public function actionStaffhome()
    {
        $this->layout = '//layouts/teacher';
        $masterschool=UserModule::GetSchoolDetails();
        $model=new Homeworkmarking('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Mock']))
            $model->attributes=$_GET['Mock'];        
        $this->render('markerhome',array('masterschool'=>$masterschool,'model'=>$model));        
    }
    /** staff function*/
    /**
     * add points function
     */
         
    public function AddPoints($earnedmarks, $testid, $testtype, $user)
    {
        $studentpintcount = StudentPoints::model()->count(
            array('condition' => 'TM_STP_Student_Id=:student AND TM_STP_Test_Id=:test AND TM_STP_Test_Type=:type',
                'params' => array(':student' => $user, ':test' => $testid, 'type' => $testtype)
            ));
        if ($studentpintcount == 0) {
            $studentpoint = new StudentPoints();
            $studentpoint->TM_STP_Student_Id = $user;
            $studentpoint->TM_STP_Points = $earnedmarks;
            $studentpoint->TM_STP_Test_Id = $testid;
            $studentpoint->TM_STP_Test_Type = $testtype;
            $studentpoint->TM_STP_Standard_Id = Yii::app()->session['standard'];
            $studentpoint->save(false);
        }
        return true;
    }   
 //code by amal
    public function actionPrintworksheet($id)
    {
        $homeworkqstns=HomeworkQuestions::model()->findAll(array(
            'condition' => 'TM_HQ_Homework_Id=:test',
            'params' => array(':test' => $id),
            'order'=>'TM_HQ_Order ASC'
        ));
        $username=User::model()->findByPk(Yii::app()->user->id)->username;
                $homework=Schoolhomework::model()->findByPK($id)->TM_SCH_Name;
        $html = "<div style='border:solid 1px #000;'><table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>"; 
        $html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";
        $totalquestions=count($homeworkqstns);         
        $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
        foreach ($homeworkqstns AS $key => $homeworkqstn):
            $homeworkqstncount = $key + 1;
            $question = Questions::model()->findByPk($homeworkqstn->TM_HQ_Question_Id);
            //code by amal
            if($homeworkqstn->TM_HQ_Type!=5):
                if($homeworkqstn->TM_HQ_Type==4):
                    $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_HQ_Question_Id' "));
                    $marks=$answers->TM_AR_Marks;
                else:
                    $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_HQ_Question_Id' AND TM_AR_Correct='1' "));
                    $marks=$answers->TM_AR_Marks;
                endif;
            endif;
            if($homeworkqstn->TM_HQ_Type==5):
                $childqstns=Studenthomeworkquestions::model()->findAll(array('condition' => "TM_STHWQT_Parent_Id='$homeworkqstn->TM_HQ_Question_Id' AND TM_STHWQT_Mock_Id='$id' "));
                $childtotmarks=0;
                foreach($childqstns AS $childqstn):
                    $childhwqstns = Questions::model()->findByPk($childqstn->TM_STHWQT_Question_Id);
                    if($childhwqstns->TM_QN_Type_Id==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_STHWQT_Question_Id' "));
                        $childmarks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_STHWQT_Question_Id' AND TM_AR_Correct='1' "));
                        $childmarks=$answers->TM_AR_Marks;
                    endif;
                $childtotmarks=$childtotmarks + $childmarks;
                endforeach;
                $marks=$childtotmarks;
            endif;
            if($homeworkqstn->TM_HQ_Type==6):
                $marks=$question->TM_QN_Totalmarks;
            endif;
            //ends
                if ($question->TM_QN_Type_Id!='7'):
                    $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $marks . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    foreach($question->answers AS $key=>$answer):
                        $ans=$answer->TM_AR_Answer;
                        $find='<p>';
                        $pos = strpos($ans, $find);
                        if ($pos === false) {
                            $html.="<tr><td colspan='3'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td></tr>";
                        } else {
                            $html.="<tr><td colspan='3'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td></tr>";
                        }
                    endforeach;
                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html .= "<tr><td colspan='3' style='text-decoration: underline;'>".$childquestion->TM_QN_Question." </td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            foreach($childquestion->answers AS $key=>$answer):
                                $ans=$answer->TM_AR_Answer;
                                $find='<p>';
                                $pos = strpos($ans, $find);
                                if ($pos === false)
                                {
                                    $html.="<tr><td colspan='3'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td></tr>";
                                }
                                else
                                {
                                    $html.="<tr><td colspan='3'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td></tr>";
                                }
                            endforeach;
                        endforeach;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $displaymark . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
        endforeach;
		$html .= "</table></div>";
		$schoolhw=Schoolhomework::model()->findByPK($id);
        $school=School::model()->findByPK($schoolhw->TM_SCH_School_Id);
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                <td width="33%"><span style="font-weight: lighter; color: #afafaf; ">For use by '.$school->TM_SCL_Name.' only</span></td>
                <td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
                <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                </tr></table>';

        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = false;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
        <td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Ltd.</span></td>
        </tr></table>');

        //echo $html;exit;
        $name='WORKSHEET'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

    }
    //ends
    /**
     * print solution 
     */
     public function actionPrintsolution($id)
    {
        $homeworkqstns=HomeworkQuestions::model()->findAll(array(
            'condition' => 'TM_HQ_Homework_Id=:test',
            'params' => array(':test' => $id),
            'order'=>'TM_HQ_Order ASC'
        ));
        $username=User::model()->findByPk(Yii::app()->user->id)->username;
        $homework=Schoolhomework::model()->findByPK($id)->TM_SCH_Name;
        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>Homework : ".$homework."<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";
        foreach ($homeworkqstns AS $key => $homeworkqstn):
            $homeworkqstncount = $key + 1;
            $question = Questions::model()->findByPk($homeworkqstn->TM_HQ_Question_Id);
            //print_r($question);exit;
                if ($question->TM_QN_Type_Id!='7'):
                    $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'> </td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html .= "<tr><td colspan='3' style='text-decoration: underline;'>".$childquestion->TM_QN_Question." </td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                        endforeach;
                    endif;
                    $html .= "<tr><td colspan='3' ><p><b>Solution</b></p>".$question->TM_QN_Solutions."</td></tr>";
                    if ($question->TM_QN_Solution_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300"></td></tr>';
                    endif;                   
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $displaymark . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $html .= "<tr><td colspan='3' ><p><b>Solution</b></p>".$question->TM_QN_Solutions."</td></tr>";
                    if ($question->TM_QN_Solution_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300"></td></tr>';
                    endif;                     
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
        endforeach;
        $html .= "</table>";
        $schoolhw=Schoolhomework::model()->findByPK($id);
        $school=School::model()->findByPK($schoolhw->TM_SCH_School_Id);
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                <td width="33%"><span style="font-weight: lighter; color: #afafaf; ">For use by '.$school->TM_SCL_Name.' only</span></td>
                <td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
                <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                </tr></table>';

        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
        <td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Ltd.</span></td>
        </tr></table>');

        //echo $html;exit;
        $name='HOMEWORKSOLUTION'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

    }
    public function actionPublishCustomHomework()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
		$homeworkid=$this->InsertTemlapteQuestions($templateid);
        $homeworktotal=$this->Gethomeworktotal($homeworkid);                  
		if(count($groupid)> 0): 
                        
            for ($i = 0; $i < count($groupid); $i++) {
                
                $grpid=$groupid[$i];
                $grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                foreach($grpstudents AS $grpstudent):                    
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal                  
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));                                                
                        foreach($hwquestions AS $hwquestion):
                            $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);                            
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;                        
                        $studhomework->TM_STHW_TotalMarks=$homeworktotal;                        
                        $studhomework->save(false);
                    }
                    
                endforeach;
            }
        endif;
        $homework = Schoolhomework::model()->findByPk($homeworkid);
        $homework->TM_SCH_Status='1';
        $homework->TM_SCH_DueDate=$_POST['dudate'];
        $homework->TM_SCH_Comments=$_POST['comments'];
        $homework->TM_SCH_PublishDate=$_POST['publishdate'];
        $homework->TM_SCH_PublishType=$_POST['type'];
        $homework->TM_SCH_ShowSolution=$_POST['solution'];
		$homework->TM_SCH_CreatedBy=Yii::app()->user->id;
        if($homework->save(false)):
                echo "yes";
        endif;                
    } 
    public function actionRepublishhomework()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];        
    	$homeworkid=$this->InsertQuestions($templateid);
    	$homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):	
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                $grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal                                      
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
					$studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];	
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        $studhomework->save(false);
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                    }
                endforeach;
            }
        endif;
            $homework = Schoolhomework::model()->findByPk($homeworkid);
            $homework->TM_SCH_Status='1';
            $homework->TM_SCH_Name=$_POST['name'];
            $homework->TM_SCH_DueDate=$_POST['dudate'];
            $homework->TM_SCH_Comments=$_POST['comments'];
            $homework->TM_SCH_PublishDate=$_POST['publishdate'];
            $homework->TM_SCH_PublishType=$_POST['type'];
            $homework->TM_SCH_ShowSolution=$_POST['solution'];
            $homework->TM_SCH_CreatedBy=Yii::app()->user->id;
            $homework->save(false);                
    }	
	public function InsertTemlapteQuestions($homeworkid)
	{
			$template = Customtemplate::model()->findByPk($homeworkid);
			$schoolhomework=new Schoolhomework();
			$schoolhomework->TM_SCH_Name=$template->TM_SCT_Name;
			$schoolhomework->TM_SCH_Description=$template->TM_SCT_Description;
			$schoolhomework->TM_SCH_School_Id=$template->TM_SCT_School_Id;
			$schoolhomework->TM_SCH_Publisher_Id=$template->TM_SCT_Publisher_Id;
			$schoolhomework->TM_SCH_Syllabus_Id=$template->TM_SCT_Syllabus_Id;
			$schoolhomework->TM_SCH_Standard_Id=$template->TM_SCT_Standard_Id;
			$schoolhomework->TM_SCH_Type='1';
			$schoolhomework->TM_SCH_Status='1';
			$schoolhomework->TM_SCH_CreatedOn=$template->TM_SCT_CreatedOn;
			$schoolhomework->TM_SCH_CreatedBy=$template->TM_SCT_CreatedBy;
			$schoolhomework->save(false);
			$questions= CustomtemplateQuestions::model()->findAll(array('condition'=>'TM_CTQ_Custom_Id='.$homeworkid));
			foreach($questions AS $question):
				$homeworkquestion=new HomeworkQuestions();
				$homeworkquestion->TM_HQ_Homework_Id=$schoolhomework->TM_SCH_Id;
				$homeworkquestion->TM_HQ_Question_Id=$question->TM_CTQ_Question_Id;
				$homeworkquestion->TM_HQ_Type=$question->TM_CTQ_Type;
				$homeworkquestion->TM_HQ_Order=$question->TM_CTQ_Order;
				$homeworkquestion->save(false);
			endforeach;
			return $schoolhomework->TM_SCH_Id;
				
	}
    public function InsertQuestions($homeworkid)
	{
			$template = Schoolhomework::model()->findByPk($homeworkid);
			$schoolhomework=new Schoolhomework();
			$schoolhomework->TM_SCH_Name=$template->TM_SCH_Name;
			$schoolhomework->TM_SCH_Description=$template->TM_SCH_Description;
			$schoolhomework->TM_SCH_School_Id=$template->TM_SCH_School_Id;
			$schoolhomework->TM_SCH_Publisher_Id=$template->TM_SCH_Publisher_Id;
			$schoolhomework->TM_SCH_Syllabus_Id=$template->TM_SCH_Syllabus_Id;
			$schoolhomework->TM_SCH_Standard_Id=$template->TM_SCH_Standard_Id;
			$schoolhomework->TM_SCH_Type=$template->TM_SCH_Type;
			$schoolhomework->TM_SCH_Status='1';
			$schoolhomework->TM_SCH_CreatedOn=$template->TM_SCH_CreatedOn;
			$schoolhomework->TM_SCH_CreatedBy=$template->TM_SCH_CreatedBy;
			$schoolhomework->save(false);
			$questions= HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$homeworkid));
			foreach($questions AS $question):
				$homeworkquestion=new HomeworkQuestions();
				$homeworkquestion->TM_HQ_Homework_Id=$schoolhomework->TM_SCH_Id;
				$homeworkquestion->TM_HQ_Question_Id=$question->TM_HQ_Question_Id;
				$homeworkquestion->TM_HQ_Type=$question->TM_HQ_Type;
				$homeworkquestion->TM_HQ_Order=$question->TM_HQ_Order;
				$homeworkquestion->save(false);
			endforeach;
			return $schoolhomework->TM_SCH_Id;
				
	}
    public function PublishHomework($homeworkid,$groupid,$duedate,$comments,$publishdate,$type,$solution)
    {
        $school=Yii::app()->session['school'];
        $homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                $grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal                                      
                    if($publishdate==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$duedate;
                    $studhomework->TM_STHW_Comments=$comments;
                    $studhomework->TM_STHW_PublishDate=$publishdate;
                    $studhomework->TM_STHW_Type=$type;
                    $studhomework->TM_STHW_ShowSolution=$solution;
                    $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {                        
                        $studhomework->save(false);
                        if($publishdate==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;                         
                    }
                endforeach;
            }
        endif;
            $homework = Schoolhomework::model()->findByPk($homeworkid);
            $homework->TM_SCH_Status='1';
            $homework->TM_SCH_DueDate=$duedate;
            $homework->TM_SCH_Comments=$comments;
            $homework->TM_SCH_PublishDate=$publishdate;
            $homework->TM_SCH_PublishType=$type;
            $homework->save(false);                
    } 
    
    /**
     * homework assessment
     */            
    public function actionQuestionstatus($id)
    {
        $this->layout = '//layouts/teacher';
        $homework = Schoolhomework::model()->findByPk($id);
        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$id'"));
        //print_r($hwquestions);              
        $this->render('questionstatus',array('homework'=>$homework,'hwquestions'=>$hwquestions));                   
    } 
    public function actionHomeworkstatus($id)
    {
        $this->layout = '//layouts/teacher';
        $homework = Schoolhomework::model()->findByPk($id);
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $notstartedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));                                
        
        //print_r($hwquestions);              
        $this->render('homeworkstatus',array('homework'=>$homework,'startedstudents'=>$startedstudents,'notstartedstudents'=>$notstartedstudents));                   
    }    
    public function GetIndividualquestionstatus($question,$homework)
    {
        $totalstudents=Studenthomeworkquestions::model()->count(array('condition'=>'TM_STHWQT_Question_Id='.$question.' AND TM_STHWQT_Mock_Id='.$homework));
        $totalstudentscorrect=Studenthomeworkquestions::model()->count(array('condition'=>'TM_STHWQT_Question_Id='.$question.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Marks!=0'));
        if($totalstudentscorrect!=0):
            $percentage = ($totalstudentscorrect / $totalstudents) * 100;
        else:
            $percentage = 0;
        endif;
        return round($percentage);  
    }        
    /**
     * Homework assessment ends
     */    
public function actionImportTeacher($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        $importlog=array();
        if(isset($_POST['submit'])):
            $file = fopen($_FILES['importcsv']['tmp_name'], "r");
            $count=1;
            while(! feof($file))
            {
                $values=fgetcsv($file);
                if($values[0]!=''):
                    if($values[9]=='Teacher'):
                        $usertype=9;
                    else:
                        $usertype=12;
                    endif;
                    if($values[6]=='Active'):
                        $status=1;
                    else:
                        $status=0;
                    endif;
                    if($values[10]=='Yes'):
                        $questions=1;
                    else:
                        $questions=0;
                    endif;
                    $user=User::model()->find(array(
                            'condition' => 'username=:name',
                            'params' => array(':name' => $values[2],)
                        )
                    );
                    if(User::model()->count('id=:value',array(':value'=>$user->id)))
                    {
                        $model=User::model()->findByPk($user->id);
                        $model->username=$values[2];
                        $model->password = UserModule::encrypting($values[3]);
                        $model->email=$values[2];
                        $model->usertype = $usertype;
                        $model->status = $status;
                        $model->save(false);
                        $profile=Profile::model()->findByPk($model->id);
                        $profile->firstname = $values[0];
                        $profile->lastname = $values[1];
                        $profile->phonenumber = $values[8];
                        $profile->save(false);
                        $staffschool=Teachers::model()->find(array(
                                'condition' => 'TM_TH_User_Id=:id',
                                'params' => array(':id' => $model->id)
                            )
                        );
                        $staffschool->TM_TH_Name = $profile->firstname . $profile->lastname;
                        $staffschool->TM_TH_Manage_Questions = $questions;
                        $staffschool->save(false);
                        $criteria = new CDbCriteria;
                        $criteria->addCondition('TM_TE_ST_User_Id=' . $model->id);
                        $teacherstds = TeacherStandards::model()->deleteAll($criteria);
                        if($values[7]!='No standards assigned')
                        {
                            $standards = explode(',', $values[7]);
                            foreach($standards AS $standard):
                                $data=Standard::model()->find(array(
                                        'condition' => 'TM_SD_Name=:id',
                                        'params' => array(':id' => $standard)
                                    )
                                );
                                $tchrstds=New TeacherStandards();
                                $tchrstds->TM_TE_ST_Standard_Id=$data->TM_SD_Id;
                                $tchrstds->TM_TE_ST_User_Id=$model->id;
                                $tchrstds->TM_TE_ST_Teacher_Id=$staffschool->TM_TH_Id;
                                $tchrstds->save(false);
                            endforeach;
                        }
                    }
                    else
                    {
                        if($values[3]==''):
                            $password=$this->generateRandomString();
                        else:
                            $password=$values[3];
                        endif;                        
                        $model = new User;
                        $profile = new Profile;
                        $staffschool = new Teachers;
                        $model->usertype = $usertype;
                        $model->school_id = $id;
                        $model->username=$values[2];
                        $model->email=$values[2];
                        $model->activkey = UserModule::encrypting(microtime() . $password);
                        $model->password = UserModule::encrypting($password);
                        $model->createtime = time();
                        $model->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
                        $model->superuser = 0;
                        $model->status = '0';
                        $importlog[$count]['status']=$this->GetImportitems($values[2]);
                        if($importlog[$count]['status']==''):
                            $model->save(false);
                            $importlog[$count]['teacher']='1';
                            $profile->user_id = $model->id;
                            $profile->firstname = $values[0];
                            $profile->lastname = $values[1];
                            $profile->phonenumber = $values[8];
                            $profile->save(false);
                            $staffschool->TM_TH_User_Id = $model->id;
                            $staffschool->TM_TH_Name = $profile->firstname . $profile->lastname;
                            $staffschool->TM_TH_SchoolId = $id;
                            $staffschool->TM_TH_Manage_Questions = $questions;
                            $staffschool->TM_TH_CreatedBy=Yii::app()->user->id;
                            $staffschool->save(false);
                            //change by amal
                            if($values[7]!='No standards assigned' || $values[7]!='')
                            {
                                $standards = explode(',', $values[7]);
                                foreach($standards AS $standard):
                                    $data=Standard::model()->find(array(
                                            'condition' => 'TM_SD_Name=:id',
                                            'params' => array(':id' => $standard)
                                        )
                                    );
                                    $tchrstds=New TeacherStandards();
                                    $tchrstds->TM_TE_ST_Standard_Id=$data->TM_SD_Id;
                                    $tchrstds->TM_TE_ST_User_Id=$model->id;
                                    $tchrstds->TM_TE_ST_Teacher_Id=$staffschool->TM_TH_Id;
                                    $tchrstds->save(false);
                                endforeach;
                                $teacheruser=User::model()->find(array(
                                        'condition' => 'id=:teacherid',
                                        'params' => array(':teacherid' => $model->id,)
                                    )
                                );
                                $teacheruser->status='1';
                                $teacheruser->save(false);
                            }
                            //ends                            
                            
                            $this->SendTeacherMail($model->id,$password);
                        else:
                            $importlog[$count]['teacher']='0';
                        endif;
                    }
                endif;
                $count++;
            }
        endif;
        $this->render('ImportTeacher', array(
            'id' => $id,
            'importlog'=>$importlog
        ));
    }
    public function GetImportitems($values)
    {
        $message="";
        /*if(User::model()->count('username=:value',array(':value'=>$values)))
        {
            $messagearray['message'].='Username \''.$values.'\' Exists<br>';
            $countarray=1;
        }*/

        if(User::model()->count('email=:value',array(':value'=>$values)))
        {
            $message.='Email \''.$values.'\' Exists<br>';

        }

        return $message;
    } 
    
    public function actionMockorder()
    {
    
        if (isset($_POST['id']))
        {
            $info=$_POST['info'];
            $id=$_POST['id'];
            $order=$_POST['order'];
            if (($_POST['info']=='up')):
                $modelold=CustomtemplateQuestions::model()->find(array(
                        'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order<:order ',
                        'limit' => 1,
                        'order'=>'TM_CTQ_Order DESC',
                        'params' => array(':id' => $id, ':order' => $order)
                    )
                );
                
                $model=CustomtemplateQuestions::model()->find(array(
                        'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order=:order',
                        'params' => array(':id' => $id, ':order' => $order)
                    )
                );
                $temp=$modelold->TM_CTQ_Order;
                $modelold->TM_CTQ_Order=$model->TM_CTQ_Order;
                $model->TM_CTQ_Order=$temp;
                $modelold->save(false);
                $model->save(false);
                echo "yes";
            else:
                    $modelold=CustomtemplateQuestions::model()->find(array(
                            'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order>:order ',
                            'limit' => 1,
                            'order'=>'TM_CTQ_Order ASC',
                            'params' => array(':id' => $id, ':order' => $order)
                        )
                    );
    
                    $model=CustomtemplateQuestions::model()->find(array(
                            'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order=:order',
                            'params' => array(':id' => $id, ':order' => $order)
                        )
                    );
                    $temp=$modelold->TM_CTQ_Order;
                    $modelold->TM_CTQ_Order=$model->TM_CTQ_Order;
                    $model->TM_CTQ_Order=$temp;
                    $modelold->save(false);
                    $model->save(false);
                    echo "yes";
            endif;

        }
    }
    public function actionEditHomework()
    {
        $this->layout = '//layouts/teacher';
        $homework = Schoolhomework::model()->findByPk($_POST['id']);

        $modaldiv='';

        $modaldiv .='

                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publish Date <span class="glyphicon glyphicon-calendar"></span></label>
                            <div class="col-sm-7">
                                <input type="date" id="publishdate" class="form-control" name="publishdate"
                                       value="';
        $datetime = new DateTime($homework->TM_SCH_PublishDate);
        $modaldiv .= $datetime->format('Y-m-d') . '">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Due On <span class="glyphicon glyphicon-calendar"></span></label>
                            <div class="col-sm-7">
                                <input type="date" id="duedate" class="form-control" name="duedate"
                                       value="';
        $datetime = new DateTime($homework->TM_SCH_DueDate);
        $modaldiv .= $datetime->format('Y-m-d').'">
</div>
</div>
</div>
<div class="row">
    <div class="form-group">
        <label class="col-sm-3 control-label">To be submitted</label>
        <div class="col-sm-7">';
        if ($homework->TM_SCH_PublishType == 2):

            $modaldiv .= '<input type="radio" class="showsolution" name="type"
                       value="1"/><span>Worksheet Only</span>
                <input type="radio" class="showsolution" name="type" value="2"
                       checked="checked"/><span>Online &amp; Worksheet</span>';
        else:
            $modaldiv .= '<input type="radio" class="showsolution" name="type"
                       checked="checked" value="1"/><span>Worksheet Only</span>
                <input type="radio" class="showsolution" name="type" value="2"/><span>Online &amp; Worksheet</span>';
        endif;
        $modaldiv .= '</div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <label class="col-sm-3 control-label">Show Solution</label>

        <div class="col-sm-7">';
        if ($homework->TM_SCH_ShowSolution == 1):
            $modaldiv .= '<input type="radio" class="showsolution" name="solution" value="1"
                       checked="checked"/><span>Yes</span>
                <input type="radio" class="showsolution" name="solution" value="0"/><span>No</span>';
        else:
            $modaldiv .= '<input type="radio" class="showsolution" name="solution" value="1"
                       checked="checked"/><span>Yes</span>
                <input type="radio" class="showsolution" name="solution" value="0"/><span>No</span>';

        endif;
        $modaldiv .= '</div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <label class="col-sm-3 control-label">Comments</label>

        <div class="col-sm-7">
            <textarea name="comments" class="form-control"
                      id="comments">';
        $modaldiv .= $homework->TM_SCH_Comments . '</textarea>
        </div>
</div>
</div>
';


        echo $modaldiv;
        //$this->render('edithomework', array('homework'=>$homework));
    }
    public function actionchangehomeworkdetails()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['id'];
        $groupid=$_POST['nameids'];        
    	$homeworkid=$_POST['id'];
    	$homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):	
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                $grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal                                      
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
					$studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];	
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        $studhomework->save(false);
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                    }
                endforeach;
            }
        endif;	
		$homework = Schoolhomework::model()->findByPk($_POST['id']);
        $homework->TM_SCH_Status = '1';
        $homework->TM_SCH_DueDate = $_POST['dudate'];
        $homework->TM_SCH_Comments = $_POST['comments'];
        $homework->TM_SCH_PublishDate = $_POST['publishdate'];
        $homework->TM_SCH_PublishType = $_POST['type'];
        $homework->TM_SCH_ShowSolution = $_POST['solution'];
        $homework->save(false);               
    }

    public function actionPasswordchange()
    {
        $this->layout = '//layouts/teacher';
        $model = new UserChangePassword;
        if (Yii::app()->user->id) {
            // ajax validator
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'changepassword-form') {
                echo UActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['UserChangePassword'])) {
                $model->attributes = $_POST['UserChangePassword'];
                if ($model->validate()) {
                    $new_password = User::model()->findbyPk(Yii::app()->user->id);
                    $new_password->password = UserModule::encrypting($model->password);
                    $new_password->activkey = UserModule::encrypting(microtime() . $model->password);
                    $new_password->save(false);
                    Yii::app()->user->setFlash('profileMessage', UserModule::t("New password is saved."));
                    $this->refresh();
                }
            }
            $this->render('changepassword', array('model' => $model));
        }
    }
    public function actionSubmitstudent()
    {
        $criteria = new CDbCriteria;                
        $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
        $criteria->params = array(':test' => $_POST['homework'], 'student' => $_POST['student']);
        $homework=StudentHomeworks::model()->find($criteria);
        $papertotal=$homework->TM_STHW_TotalMarks;
        $paperquestions= Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$_POST['homework'].' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id='.$_POST['student']));
        foreach($paperquestions AS $question):
            $question->TM_STHWQT_Marks='0';
            $question->save(false);
        endforeach;
        $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
        $test->TM_STHW_Status = '4';
        $test->TM_STHW_TotalMarks = $papertotal;
        $test->save(false);
        echo "yes";                       
    }    
    public function actionSubmitall($id)
    {
        $notstarted=StudentHomeworks::model()->findAll(array('condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)'));
        foreach($notstarted AS $test):                       
            $paperquestions= Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$id.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id='.$test->TM_STHW_Student_Id));
            foreach($paperquestions AS $question):                
                $question->TM_STHWQT_Marks='0';
                $question->save(false);
            endforeach;              
            $test->TM_STHW_Status = '4';            
            $test->save(false);                  
        endforeach;
        $this->redirect(array('markhomework', 'id' =>$id));
    }
    public function Gethomeworktotal($homeworkid)
    {
        $questions= HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$homeworkid));
        $homeworktotal=0;
        foreach($questions AS $question):
            $question=Questions::model()->findByPk($question->TM_HQ_Question_Id);
             
            switch($question->TM_QN_Type_Id)
            {
               
                case 5:                    
                    $command = Yii::app()->db->createCommand();
                    $row = Yii::app()->db->createCommand(array(
                        'select' => array('SUM(TM_AR_Marks) AS totalmarks'),
                        'from' => 'tm_answer',
                        'where' => 'TM_AR_Question_Id IN (SELECT TM_QN_Id FROM 	tm_question WHERE TM_QN_Parent_Id='.$question->TM_QN_Id.')',                    
                    ))->queryRow(); 
                    $homeworktotal=$homeworktotal+$row['totalmarks'];                               
                    break;    
                case 6:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;                                
                case 7:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;               
                default:                 
                    $marks=Answers::model()->find(array('select'=>'SUM(TM_AR_Marks) AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' AND TM_AR_Correct=1'));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;                
                    break;                                                                   
                
            }
         
        endforeach;        
        return $homeworktotal;
    }
    public function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

