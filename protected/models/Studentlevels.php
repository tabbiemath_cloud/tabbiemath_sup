<?php

/**
 * This is the model class for table "{{studentlevels}}".
 *
 * The followings are the available columns in table '{{studentlevels}}':
 * @property integer $TM_STL_Id
 * @property integer $TM_STL_Level_Id
 * @property integer $TM_STL_Student_Id
 * @property integer $TM_STL_Status
 * @property integer $TM_STL_Order
 * @property string $TM_STL_Date
 */
class Studentlevels extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{studentlevels}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STL_Level_Id, TM_STL_Student_Id, TM_STL_Status', 'required'),
			array('TM_STL_Level_Id, TM_STL_Student_Id, TM_STL_Status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STL_Id, TM_STL_Level_Id, TM_STL_Student_Id, TM_STL_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STL_Id' => 'Tm Stl',
			'TM_STL_Level_Id' => 'Tm Stl Level',
			'TM_STL_Student_Id' => 'Tm Stl Student',
			'TM_STL_Status' => 'Tm Stl Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STL_Id',$this->TM_STL_Id);
		$criteria->compare('TM_STL_Level_Id',$this->TM_STL_Level_Id);
		$criteria->compare('TM_STL_Student_Id',$this->TM_STL_Student_Id);
		$criteria->compare('TM_STL_Status',$this->TM_STL_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Studentlevels the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
