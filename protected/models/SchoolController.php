<?php

class SchoolController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
            'postOnly + Deletestaff', // we only allow deletion via POST request            
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','admin','managesubscription','Addplan','GetStandardList','GetPlanList','Activate','Deactivate','Deleteplan','ManageStaff','AddStaff','Viewstaff','Editstaff','deletestaff', 'ManageStudents', 'Addstudent', 'Viewstudent', 'Editstudent', 'Deletestudent','Sendregistrationemails','Staffpassword','Studentpassword','ImportStudents','Studentexport'),
                'users'=>array('@'),
                'expression'=>'UserModule::isAllowedSchool()'				
			), 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','delete'),
                'users'=>array('@'),
                'expression'=>'UserModule::isSuperAdmin()'				
			),            
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('Home','Studentexport'),
                'users'=>array('@'),
                'expression'=>'UserModule::isSchoolAdmin()'				
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Home','Switchadmin'),
                'users'=>array('@'),
                'expression'=>'UserModule::isTeacherAdmin()'
            ),            
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('Updateschoolprofile','Schoolprofile','Subscriptions','Staffpassword'),
                'users'=>array('@'),
                'expression'=>'UserModule::isSchoolStaffAdmin($_GET["id"])'				
			), 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('ManageStaff','AddStaff','Viewstaff','Editstaff','deletestaff','Subscriptions','Activate','Deactivate', 'ManageStudents', 'Addstudent', 'Viewstudent', 'Editstudent', 'Deletestudent','Sendregistrationemails','Staffpassword','Studentpassword','ImportStudents','Studentexport'),
                'users'=>array('@'),
                'expression'=>'UserModule::isSchoolStaffSSupAdmin($_GET["id"])'				
			),             
                              
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $plan=SchoolPlan::model()->findByAttributes(array('TM_SPN_SchoolId'=>$id));
		$this->render('view',array(
			'model'=>$this->loadModel($id),
            'plan'=>$plan,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new School;
        $model->TM_SCL_Discount=0;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['School']))
		{
			$model->attributes=$_POST['School'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_SCL_Id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
    public function actionSchoolprofile($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;		
        
        $this->render('schoolprofile',array(
			'model'=>$this->loadModel($id),
		));                
    }
    public function actionUpdateschoolprofile($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;	
        $model=$this->loadModel($id);
        //echo $_POST['TM_SCL_Sylllabus_Id'];exit;
		if(isset($_POST['School']))
		{
			$model->attributes=$_POST['School'];
			if($model->save(false))
				$this->redirect(array('schoolprofile','id'=>$model->TM_SCL_Id));
		}
        
        $this->render('updateschoolprofile',array(
			'model'=>$model,
		));           
    }    
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['School']))
		{
			$model->attributes=$_POST['School'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_SCL_Id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if($this->loadModel($id)->delete()):
            $users=User::model()->findAll(array('condition'=>'school_id='.$id.' AND usertype NOT IN(5,6)'));
            foreach($users AS $user):
                $user->delete();
            endforeach;
            SchoolStaffs::model()->deleteAll(array('condition'=>'TM_SCS_School_Id='.$id));
            SchoolQuestions::model()->deleteAll(array('condition'=>'TM_SQ_School_Id='.$id));
            SchoolPlan::model()->deleteAll(array('condition'=>'TM_SPN_SchoolId='.$id));
            Schoolhomework::model()->deleteAll(array('condition'=>'TM_SCH_School_Id='.$id));
            SchoolGroups::model()->deleteAll(array('condition'=>'TM_SCL_Id='.$id));
        endif;
        
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('School');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new School('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['School']))
			$model->attributes=$_GET['School'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    public function actionManagesubscription($id)
    {
        $dataProvider=new CActiveDataProvider('SchoolPlan', array(
			'criteria'=>array('condition'=>'TM_SPN_SchoolId='.$id),
			'pagination'=>array(
				'pageSize'=>Yii::app()->controller->module->user_page_size,
			),
		));         
        $school=School::model()->find(array('condition'=>'TM_SCL_Id='.$id));             
		$this->renderPartial('schoolplanlist',array(
			'dataProvider'=>$dataProvider,
            'school'=>$school
		));        
    }
    public function actionAddplan($id)
    {
        $schoolplan=new SchoolPlan();
        $schoolplan->scenario = 'create';
        $school=$this->loadModel($id);
        if(isset($_POST['SchoolPlan'])):  
                     
        	$schoolplan->attributes=$_POST['SchoolPlan'];   
            $plan=Plan::model()->findByPk($schoolplan->TM_SPN_PlanId);  
            $duration=$plan->TM_PN_Duration+1;                                          
            $schoolplan->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
                
            if($schoolplan->save()):
                $questions=Questions::model()->findAll(array('condition'=>"TM_QN_Standard_Id='".$schoolplan->TM_SPN_StandardId."' AND TM_QN_Parent_Id='0'"));
                $insertarray=array();                
                if(count($questions)>0):                                        
                    foreach($questions AS $question):
                        $insertarray[]=array('TM_SQ_Chapter_Id'=>$question->TM_QN_Topic_Id,
                                             'TM_SQ_Type'=>'1',
                                             'TM_SQ_Publisher_id'=>$question->TM_QN_Publisher_Id,
                                             'TM_SQ_Question_Id'=>$question->TM_QN_Id,
                                             'TM_SQ_Question_Type'=>$question->TM_QN_Type_Id,
                                             'TM_SQ_QuestionReff'=>$question->TM_QN_QuestionReff,
                                             'TM_SQ_School_Id'=>$schoolplan->TM_SPN_SchoolId,
                                             'TM_SQ_Standard_Id'=>$schoolplan->TM_SPN_StandardId,
                                             'TM_SQ_Status'=>$question->TM_QN_Status,
                                             'TM_SQ_Syllabus_Id'=>$question->TM_QN_Syllabus_Id,
                                             'TM_SQ_Topic_Id'=>$question->TM_QN_Section_Id
                                            );
                    endforeach;
                    $builder=Yii::app()->db->schema->commandBuilder;
                    $command=$builder->createMultipleInsertCommand('tm_school_questions', $insertarray);
                    $command->execute();                    
                endif;                 
                           
                $standard=Standard::model()->findByPk($schoolplan->TM_SPN_StandardId);
                $schoolgroups=New SchoolGroups();
                $schoolgroups->TM_SCL_GRP_Name="ALL Standard ".$standard->TM_SD_Name."";
                $schoolgroups->TM_SCL_Id=$schoolplan->TM_SPN_SchoolId;
                $schoolgroups->TM_SCL_PN_Id=$schoolplan->TM_SPN_PlanId;
                $schoolgroups->TM_SCL_SD_Id=$schoolplan->TM_SPN_StandardId;
                $schoolgroups->save(false);                
                $this->redirect(array('admin'));
            endif;
        endif;
        $this->render('addplan',array(
			'model'=>$schoolplan,
            'id'=>$id,
            'school'=>$school
		));
    } 
    
    public function actionDeleteplan($id)
    {		
    	$plan = SchoolPlan::model()->findByPk($id);
        $student=$plan->TM_SPN_SchoolId;            
    	$plan->delete();
        /*$studentplans=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student.' AND TM_SPN_Status=0'));
        if($studentplans==0)
        {
            $user=User::model()->findByPk($student);
            $user->status='0';
            $user->save(false);
        }*/
    	$this->redirect(array('admin'));	      				  													
    }
    public function actionDeactivate($id)
    {
        $plan = SchoolPlan::model()->findByPk($id);  
        $school=$plan->TM_SPN_SchoolId; 
        $plan->TM_SPN_CancelDate=date('y-m-d');          
    	$plan->TM_SPN_Status='1';
        $plan->save(false);
        /*$studentplans=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student.' AND TM_SPN_Status=0'));
        if($studentplans==0)
        {
            $user=User::model()->findByPk($student);
            $user->status='0';
            $user->save(false);
        }*/
        if(UserModule::isSchoolAdmin()):
            $this->redirect(array('subscriptions','id'=>$school));
        else:        
            $this->redirect(array('admin'));
        endif;   
    }
    public function actionActivate($id)
    {
        $plan = SchoolPlan::model()->findByPk($id);  
        $school=$plan->TM_SPN_SchoolId; 
        $plan->TM_SPN_CancelDate=date('y-m-d');          
    	$plan->TM_SPN_Status='0';
        $plan->save(false);
        //$studentplans=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student.' AND TM_SPN_Status=0'));
        /*if($studentplans>0)
        {
            $user=User::model()->findByPk($student);
            $user->status='1';
            $user->save(false);
        }*/
        if(UserModule::isSchoolAdmin()):
            $this->redirect(array('subscriptions','id'=>$school));
        else:        
            $this->redirect(array('admin'));
        endif;              
    }         
    public function actionGetPlanList()
    {
        if(isset($_POST['publisher']))
        {
            $data=Plan::model()->findAll(
                array(
                        'condition'=>"TM_PN_Publisher_Id=:publisher AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND TM_PN_Type=1",
                        'params'=>array(':publisher'=>$_POST['publisher'],':syllabus'=>$_POST['syllabus'],':standard'=>$_POST['standard'])
                )
            );

            echo CHtml::tag('option',array('value'=>''),'Select',true);
            foreach($data as $key=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$name->TM_PN_Id),CHtml::encode($name->TM_PN_Name." (".$name->TM_PN_Rate.")"),true);
            }
        }
    }  
	public function actionGetStandardList()
    {
        if(isset($_POST['id']))
        {
            $data=Standard::model()->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0'",
                array(':syllabus'=>(int) $_POST['id']));

            $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
            /*if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Chapter',true);
            endif;*/
            echo CHtml::tag('option',array('value'=>''),'Select',true);
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$value),CHtml::encode($name),true);
            }
        }
    }
    public function actionManageStaff($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
    	$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array('condition'=>'usertype IN (8,10,11) AND school_id='.$id),
			'pagination'=>array(
				'pageSize'=>1000,
			),
		));            
        else:
        	$dataProvider=new CActiveDataProvider('User', array(
    			'criteria'=>array('condition'=>'usertype IN (8,10,11) AND school_id='.$id),
    			'pagination'=>array(
    				'pageSize'=>1000,
    			),
    		));
        endif;
		$this->render('stafflist',array(
			'dataProvider'=>$dataProvider,
            'id'=>$id
		));
    }

    public function actionViewstaff($id)
    {
        if(Yii::app()->user->isSchoolStaffAdmin($id) || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;        
		if(isset($_GET['master'])):
            $school=$id;            
            $model = User::model()->findByPk($_GET['master']);
            $schoolstaff=SchoolStaffs::model()->find(array('condition'=>'TM_SCS_School_Id='.$school.' AND TM_SCS_User_Id='.$_GET['master']));
    		$this->render('staffview',array(
    			'model'=>$model,
                'school'=>$school,
                'schoolstaff'=>$schoolstaff
    		));
        else:
            $this->redirect(array('admin','id'=>$id));
        endif;        
    }
    public function actionEditstaff($id)
    {
        if(Yii::app()->user->isSchoolStaffAdmin($id) || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;		
        if(isset($_GET['master'])):
            $school=$id;
            $model = User::model()->notsafe()->findByPk($_GET['master']);
            $profile=$model->profile;
            $schoolstaff=SchoolStaffs::model()->find(array('condition'=>'TM_SCS_School_Id='.$school.' AND TM_SCS_User_Id='.$_GET['master']));
    		if(isset($_POST['User']))
    		{
                $model->attributes=$_POST['User'];
                $profile->attributes=$_POST['Profile'];
                $schoolstaff->attributes=$_POST['SchoolStaffs'];
                if($model->validate()&  $profile->validate())
                {
                        switch ($schoolstaff->TM_SCS_Type){
                            case 1:
                                $model->usertype='8';
                                break;
                            case 2:
                                $model->usertype='8';
                                break;
                            case 3:
                                $model->usertype='11';
                                break;
                        }                     
    				$model->save();
    				$profile->save(); 
                    $schoolstaff->save();
                    $this->redirect(array('manageStaff','id'=>$model->school_id));                   
                }
    		}
    		$this->render('editstaff',array(
    			'model'=>$model,
                'profile'=>$profile,
                'school'=>$school,
                'schoolstaff'=>$schoolstaff
    		));
        else:
            $this->redirect(array('admin','id'=>$id));
        endif;         
    }
    public function actionDeletestaff($id)
    {        
        
        if(Yii::app()->request->isPostRequest)
		{		      
			     // we only allow deletion via POST request 
                $model = User::model()->notsafe()->findByPk($_GET['master'] );                
                $schoolstaff=SchoolStaffs::model()->find(array('condition'=>'TM_SCS_School_Id='.$model->school_id.' AND TM_SCS_User_Id='.$model->id));
                $schoolstaff->delete();
    			$profile = Profile::model()->findByPk($model->id);
    			$profile->delete();                
    			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(array('manageStaff','id'=>$model->school_id));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');        
    } 
    public function actionAddStaff($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;   	    
        $model = new User;
        $profile=new Profile;
        $staffschool=new SchoolStaffs;
        if(isset($_POST['User']))
        {
					$model->attributes=$_POST['User'];                    
                    $model->usertype='8';					                                                          
                    $model->school_id=$id;                     
                    $profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
                    $staffschool->attributes=((isset($_POST['SchoolStaffs'])?$_POST['SchoolStaffs']:array()));
					if($model->validate() &  $profile->validate())
					{
                        switch ($staffschool->TM_SCS_Type){
                            case 1:
                                $model->usertype='8';
                                break;
                            case 2:
                                $model->usertype='8';
                                break;
                            case 3:
                                $model->usertype='11';
                                break;
                        }        
                        $password=$model->password;                                      
                        $model->activkey=UserModule::encrypting(microtime().$model->password);
						$model->password=UserModule::encrypting($model->password);						
						$model->createtime=time();
						$model->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
						$model->superuser=0;
						//$model->status='1';						
						if ($model->save())
                        {
							$this->SendAdminMail($model->id,$password);
                            $profile->user_id=$model->id;
							$profile->save();                            
                            $staffschool->TM_SCS_User_Id=$model->id;
                            $staffschool->save();
                            $this->redirect(array('ManageStaff','id'=>$id));
                        }                        
					}
        }
		$this->render('createstaff',array(
			'model'=>$model,
			'profile'=>$profile,
            'staffschool'=>$staffschool,
            'id'=>$id
		));                
    }   
/**********************************************************************************************************
*   School admin section
**********************************************************************************************************/ 
    public function actionHome()
    {
        Yii::app()->session['mode'] = "TeacherAdmin";
        $this->layout = '//layouts/schoolcolumn1';
        $masterschool=UserModule::GetSchoolDetails();
		$this->render('schoolhome',array('masterschool'=>$masterschool));        
    }




/**********************************************************************************************************    
*   School admin section Ends
**********************************************************************************************************/     


/**********************************************************************************************************    
*   Manage schools Subscription
**********************************************************************************************************/  
    public function actionSubscriptions($id)
    {
        
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;          
        $dataProvider=new CActiveDataProvider('SchoolPlan', array(
			'criteria'=>array('condition'=>'TM_SPN_SchoolId='.$id),
			'pagination'=>array(
				'pageSize'=>Yii::app()->controller->module->user_page_size,
			),
		));         
        $school=School::model()->find(array('condition'=>'TM_SCL_Id='.$id));
                
		$this->render('subscriptionlist',array(
			'dataProvider'=>$dataProvider,
            'school'=>$school
		));  
        
    }

/**********************************************************************************************************    
*   Manage schools Ends
**********************************************************************************************************/ 

/**********************************************************************************************************    
*   Manage students under the schools Ends
**********************************************************************************************************/  

   public function actionManageStudents($id)
    {
        
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;        
        if(isset($_GET['search'])):
            $formvals=array();
            $condition='usertype =6 AND school_id=' . $id . '';
            $join='';
            if($_GET['student']!='' || $_GET['mail']!=''):
                $join.='LEFT JOIN tm_student AS st ON st.TM_STU_User_Id=id ';
                if($_GET['student']!=''):
                    $formvals['student']=$_GET['student'];
                    $condition.=" AND (st.TM_STU_First_Name LIKE '%".$_GET['student']."%' OR st.TM_STU_Last_Name LIKE '%".$_GET['student']."%' OR username LIKE '%".$_GET['student']."%' ) ";
                endif;
                if($_GET['mail']!=''):
                    $formvals['mail']=$_GET['mail'];
                    $condition.=" AND st.TM_STU_Email_Status='".$_GET['mail']."'";
                endif;
            endif;
            if($_GET['plan']!=''):
                $formvals['plan']=$_GET['plan'];
                $selectedplan=$_GET['plan'];
                $condition.=" AND (pl.TM_SPN_PlanId='".$_GET['plan']."') ";
                $join.='LEFT JOIN tm_student_plan AS pl ON pl.TM_SPN_StudentId=id ';
            endif;
            $dataProvider=new CActiveDataProvider('User', array(
                'criteria'=>array('condition'=>$condition,'join'=>$join),
                'pagination'=>array(
                    'pageSize'=>1000,
                ),
            ));
        else:
            $dataProvider = new CActiveDataProvider('User', array(
                'criteria' => array('condition' => 'usertype =6 AND school_id=' . $id . ''),
                'pagination' => array(
                    'pageSize' => 1000,
                ),
            ));
        endif;

        $this->render('studentlist', array(
            'dataProvider' => $dataProvider, 'id' => $id,'selectedplan'=>$selectedplan,'formvals'=>$formvals
        ));
    }

    public function actionStudentexport($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_Status,a.TM_STU_Email_Status,a.TM_STU_Password,a.TM_STU_Parent_Id,b.username,b.createtime,c.TM_SCL_Name,d.firstname,d.lastname,";
        $sql.="DATE_FORMAT(e.TM_SPN_CreatedOn,'%d-%m-%Y') AS regdate,e.TM_SPN_StandardId,f.TM_SD_Name ";
        $sql.="FROM tm_student AS a LEFT JOIN tm_users AS b ON a.TM_STU_User_Id=b.id ";
        $sql.="LEFT JOIN tm_school AS c  ON a.TM_STU_School_Id=c.TM_SCL_Id ";
        $sql.="LEFT JOIN tm_profiles AS d ON a.TM_STU_CreatedBy=d.user_id ";
        $sql.="LEFT JOIN tm_student_plan AS e ON a.TM_STU_User_Id=e.TM_SPN_StudentId ";
        $sql.="LEFT JOIN tm_standard AS f ON e.TM_SPN_StandardId=f.TM_SD_Id WHERE a.TM_STU_School_Id='$id' AND b.usertype =6 ";
        if($_GET['student']!='' || $_GET['mail']!=''):
            if($_GET['student']!=''):
                $sql.="AND (a.TM_STU_First_Name LIKE '%".$_GET['student']."%' OR a.TM_STU_Last_Name LIKE '%".$_GET['student']."%') ";
            endif;
            if($_GET['mail']!=''):
                $sql.="AND a.TM_STU_Email_Status='".$_GET['mail']."' ";
            endif;
        endif;
        if($_GET['plan']!=''):
            $sql.="AND e.TM_SPN_PlanId='".$_GET['plan']."' ";
        endif;
        $sql.=" GROUP BY a.TM_STU_User_Id";
        //echo $sql;exit;
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataval=$dataReader->readAll();
        foreach($dataval AS $data):
            if($data['TM_STU_Status']==0):
                $status="Active";
            elseif($data['TM_STU_Status']==1):
                $status="Inactive";
            else:
                $status="Blocked";
            endif;
            if($data['TM_STU_Email_Status']==0):
                $emailstatus="Not Send";
            else:
                $emailstatus="Send";
            endif;
            $parentpro=Profile::model()->findByPk($data['TM_STU_Parent_Id']);
            $parent=User::model()->findByPk($data['TM_STU_Parent_Id']);
            $items[]=array($data['TM_STU_First_Name'],
                $data['TM_STU_Last_Name'],
                $data['username'],
                $data['TM_STU_Password'],
                $data['TM_SCL_Name'],
                $data['firstname'].' '.$data['lastname'],
                date("d-m-Y",$data['createtime']),
                $status,
                $emailstatus,
                $data['TM_SD_Name'],
                $parentpro->firstname,
                $parentpro->lastname,
                $parentpro->phonenumber,
                $parent->email,
            );
        endforeach;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Students.csv');
        $output = fopen('php://output', 'w');
        /*fputcsv($output, array(
            "Student",
            "Username",
            "Password",
            "School",
            "Created By",
            "Registration date",
            "Status",
            "Email Status",
            "Standard",
        ));*/

        foreach($items as $row) {
            // display field/column names as first row
            fputcsv($output, $row);
        }
    }

    public function actionDeletestudent($id)
    {
        $studentuser = User::model()->find(array('condition' => 'id=' . $_GET['studnet']));
        $student = Student::model()->find(array('condition' => 'TM_STU_User_Id=' . $studentuser->id));
        $school = School::model()->findByPk($student->TM_STU_School_Id);
        $parent=User::model()->find(array('condition'=>'id='.$student->TM_STU_Parent_Id));        
        $c = new CDbCriteria;
        $c->select = 't.TM_SPN_Id, t.TM_SPN_Publisher_Id,t.TM_SPN_StudentId,t.TM_SPN_PlanId,t.TM_SPN_SyllabusId,t.TM_SPN_StandardId,t.TM_SPN_SubjectId,t.TM_SPN_Status,t.TM_SPN_PaymentReference,t.TM_SPN_CouponId,t.TM_SPN_CreatedOn,t.TM_SPN_ExpieryDate,t.TM_SPN_CancelDate';
        $c->join = "INNER JOIN tm_school_plan tm_school_plan ON t.TM_SPN_PlanId = tm_school_plan.TM_SPN_PlanId";
        $c->compare("t.TM_SPN_StudentId", $studentuser->id);
        $planprev = StudentPlan::model()->find($c);        
        $count = StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$studentuser->id.' AND TM_SPN_PlanId!='.$planprev->TM_SPN_PlanId));               
        if($count>0):            
                $studentuser->school_id = 0;
                $student->TM_STU_School_Id = 0;
                StudentPlan::model()->deleteAll(array('condition' => 'TM_SPN_PlanId=' . $planprev->TM_SPN_PlanId . ' AND TM_SPN_StudentId=' . $studentuser->id));
                GroupStudents::model()->deleteAll(array('condition' => 'TM_GRP_PN_Id=' . $planprev->TM_SPN_PlanId . ' AND TM_GRP_STUId=' . $studentuser->id));
                Homeworkmarking::model()->deleteAll(array('condition' => 'TM_HWM_User_Id=' . $planprev->TM_SPN_PlanId));
                $student->save(false);
                $studentuser->save(false);            
        else:
            $studentcount=Student::model()->count(array('condition' => 'TM_STU_Parent_Id=' . $parent->id.' AND TM_STU_User_Id!='.$studentuser->id));
            if($studentcount==0):
                $parent->delete();
            endif;
            StudentPlan::model()->deleteAll(array('condition' => 'TM_SPN_PlanId=' . $planprev->TM_SPN_PlanId . ' AND TM_SPN_StudentId=' . $studentuser->id));
            GroupStudents::model()->deleteAll(array('condition' => 'TM_GRP_PN_Id=' . $planprev->TM_SPN_PlanId . ' AND TM_GRP_STUId=' . $studentuser->id));
            Homeworkmarking::model()->deleteAll(array('condition' => 'TM_HWM_User_Id=' . $planprev->TM_SPN_PlanId));        
            $studentuser->delete();
            $student->delete();
        endif;

    }

    public function actionAddstudent($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;        
        $student = new Student();
        $student->scenario = 'school';
        $school = School::model()->findByPk($id);
        $studentuser = new User();
   	    $parentmodel = new User;
        $parentprofile=new Profile;
            if (isset($_POST['Student'])) {
					$parentprofile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
                    $student->attributes=((isset($_POST['Student'])?$_POST['Student']:array()));                                                                        
                    $parentmodel->attributes = ((isset($_POST['User']) ? $_POST['User'] : array()));
                    $parentmodel->email=$_POST['User']['email'];  
                    $parentmodel->password=$_POST['User']['password'];                 
                    $optionalusername=$parentmodel->username;
                    $parentmodel->username=$parentmodel->email;
                    $student->TM_STU_School_Id = $id;
                    $student->TM_STU_City = $school->TM_SCL_City;
                    $student->TM_STU_State = $school->TM_SCL_State;
                    $student->TM_STU_Country_Id = $school->TM_SCL_Country_Id;
                    $student->TM_STU_School = $id;  
                    $student->TM_STU_Email=$parentmodel->email;
                    $student->TM_STU_Parentpass=$_POST['User']['password'];                                       
                    $username = strtoupper(substr($student->TM_STU_First_Name, 0, 1) . substr($student->TM_STU_Last_Name, 0, 1) . rand(11111, 99999));
                    $password = $this->generateRandomString();                    
                    $parentmodel->password=md5($parentmodel->password);
                    $studentuser->username = $username;
                    $studentuser->school_id = $id;
                    $studentuser->password = md5($password);
                    $studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
                    $studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
                    $studentuser->superuser = 0;
                    $studentuser->usertype = '6';
                    $studentuser->createtime = time();
                    $studentuser->status = (User::STATUS_ACTIVE);
                    $checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$parentmodel->email . '"'));
               
                    if ($student->validate() & $parentprofile->validate()):
                        if (count($checkprevmail) != 0):                           
                           if($optionalusername!=''):
                                $studentdetails=User::model()->find(array('condition'=>"username='".$optionalusername."'"));                                
                                $parentStudent=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studentdetails->id));
                           else:
                                $parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "' . $student->TM_STU_First_Name . '" AND TM_STU_Last_Name LIKE "' . $student->TM_STU_Last_Name . ' " AND TM_STU_Parent_Id='.$checkprevmail->id));                                
                           endif;

                           if(count($parentStudent)>0):
                                $sttudentnewplan = new StudentPlan();
                                $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                                $currentplan = Plan::model()->findByPk($_POST['plan']);
                                $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                                $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                $sttudentnewplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
                                //code by amal
                                $parentStudent->TM_STU_City = $school->TM_SCL_City;
                                $parentStudent->TM_STU_State = $school->TM_SCL_State;
                                $parentStudent->TM_STU_Country_Id = $school->TM_SCL_Country_Id;
                                $parentStudent->TM_STU_School = $school->TM_SCL_Name;
                                $parentStudent->save(false);
                                //ends                                
                                if($sttudentnewplan->save(false)):
                                    ///send plan subscription mail
                                    $this->redirect(array('Viewstudent', 'id' =>$id,'studnet'=>$parentStudent->TM_STU_User_Id));
                                    //code by amal
                                    $schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$id."' AND TM_SCL_SD_Id='".$currentplan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
                                    if(count($schoolgrps)>0):
                                        $grpstudents=New GroupStudents();
                                        $grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
                                        $grpstudents->TM_GRP_PN_Id=$_POST['plan'];
                                        $grpstudents->TM_GRP_SCL_Id=$id;
                                        $grpstudents->TM_GRP_SD_Id=$currentplan->TM_PN_Standard_Id;
                                        $grpstudents->TM_GRP_STUId=$student->TM_STU_User_Id;
                                        $grpstudents->TM_GRP_STUName=$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name;
                                        $grpstudents->save(false);
                                    endif;
                                    //ends                                    
                                endif;                          
                           else:
                                $studentuser->save(false);
                                $sttudentnewplan = new StudentPlan();
                                $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                                $currentplan = Plan::model()->findByPk($_POST['plan']);
                                $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                                $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                                $studentuserprofile = new Profile();
                                $studentuserprofile->user_id = $studentuser->id;
                                $studentuserprofile->lastname = $student->TM_STU_Last_Name;
                                $studentuserprofile->firstname = $student->TM_STU_First_Name;
                                $studentuserprofile->save(false);
                                $sttudentnewplan->save(false);
                                //$student->TM_STU_Parent_Id=$id;
                                $student->TM_STU_Password = $password;
                                $student->TM_STU_Parent_Id=$checkprevmail->id;
                                $student->TM_STU_User_Id = $studentuser->id;
                                $student->TM_STU_CreatedBy = Yii::app()->user->id;
                                if ($student->save(false)) {
                                    //$this->SendStudentMail($studentuser->id);
                                //code by amal
                                $schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$id."' AND TM_SCL_SD_Id='".$currentplan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
                                if(count($schoolgrps)>0):
                                    $grpstudents=New GroupStudents();
                                    $grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
                                    $grpstudents->TM_GRP_PN_Id=$_POST['plan'];
                                    $grpstudents->TM_GRP_SCL_Id=$id;
                                    $grpstudents->TM_GRP_SD_Id=$currentplan->TM_PN_Standard_Id;
                                    $grpstudents->TM_GRP_STUId=$student->TM_STU_User_Id;
                                    $grpstudents->TM_GRP_STUName=$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name;
                                    $grpstudents->save(false);
                                endif;
                                //ends                                    
                                    $this->redirect(array('Viewstudent', 'id' =>$id,'studnet'=>$studentuser->id));
                                }                            
                           endif;
                        else:
                            $parentmodel->usertype='7';
                            $parentmodel->status='1';
                            $parentmodel->save(false);
                            $parentprofile->user_id=$parentmodel->id;
                            $parentprofile->save(false);
                            $studentuser->save(false);
                            $sttudentnewplan = new StudentPlan();
                            $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                            $currentplan = Plan::model()->findByPk($_POST['plan']);
                            $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                            $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                            $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                            $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                            $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                            $studentuserprofile = new Profile();
                            $studentuserprofile->user_id = $studentuser->id;
                            $studentuserprofile->lastname = $student->TM_STU_Last_Name;
                            $studentuserprofile->firstname = $student->TM_STU_First_Name;
                            $studentuserprofile->save(false);
                            $sttudentnewplan->save(false);
                            //$student->TM_STU_Parent_Id=$id;
                            $student->TM_STU_Password = $password;
                            $student->TM_STU_Parent_Id=$parentmodel->id;
                            $student->TM_STU_User_Id = $studentuser->id;
                            $student->TM_STU_CreatedBy = Yii::app()->user->id;
                            if ($student->save(false)) {
                                //$this->SendStudentMail($studentuser->id);
                                                                //code by amal
                                $schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$id."' AND TM_SCL_SD_Id='".$currentplan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
                                if(count($schoolgrps)>0):
                                    $grpstudents=New GroupStudents();
                                    $grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
                                    $grpstudents->TM_GRP_PN_Id=$_POST['plan'];
                                    $grpstudents->TM_GRP_SCL_Id=$id;
                                    $grpstudents->TM_GRP_SD_Id=$currentplan->TM_PN_Standard_Id;
                                    $grpstudents->TM_GRP_STUId=$student->TM_STU_User_Id;
                                    $grpstudents->TM_GRP_STUName=$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name;
                                    $grpstudents->save(false);
                                endif;
                                //ends
                                $this->redirect(array('Viewstudent', 'id' =>$id,'studnet'=>$studentuser->id));
                            }                        
                        endif;
                    endif;
                                                                              
                                
            }
                
        $this->render('addstudent', array('student' => $student, 'studentuser' => $studentuser, 'id' => $id,'parentmodel'=>$parentmodel,'parentprofile'=>$parentprofile));
    }

    public function actionEditstudent($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        $studentuser = User::model()->findByPk($_GET['studnet']);
        $student = Student::model()->find(array('condition' => 'TM_STU_User_Id=' . $studentuser->id));
        $school = School::model()->findByPk($id);
   	    $parentmodel = User::model()->findByPk($student->TM_STU_Parent_Id);
        $parentprofile=$parentmodel->profile;
        $c = new CDbCriteria;
        $c->select = 't.TM_SPN_Id, t.TM_SPN_Publisher_Id,t.TM_SPN_StudentId,t.TM_SPN_PlanId,t.TM_SPN_SyllabusId,t.TM_SPN_StandardId,t.TM_SPN_SubjectId,t.TM_SPN_Status,t.TM_SPN_PaymentReference,t.TM_SPN_CouponId,t.TM_SPN_CreatedOn,t.TM_SPN_ExpieryDate,t.TM_SPN_CancelDate';
        $c->join = "INNER JOIN tm_school_plan tm_school_plan ON t.TM_SPN_PlanId = tm_school_plan.TM_SPN_PlanId";
        $c->compare("t.TM_SPN_StudentId", $_GET['studnet']);
        $planprev = StudentPlan::model()->find($c);
        //$id = $school->TM_SCL_Id;
        if (isset($_POST['Student'])) {

            $student->attributes = ((isset($_POST['Student']) ? $_POST['Student'] : array()));

            $studentuser->attributes = ((isset($_POST['User']) ? $_POST['User'] : array()));

            $student->TM_STU_School_Id = $student->TM_STU_School_Id;

			
            $student->TM_STU_City = $school->TM_SCL_City;
            $student->TM_STU_State = $school->TM_SCL_State;
            $student->TM_STU_Country_Id = $school->TM_SCL_Country_Id;
            $student->TM_STU_School = $student->TM_STU_School_Id;
           // $username = strtoupper(substr($student->TM_STU_First_Name, 0, 1) . substr($student->TM_STU_Last_Name, 0, 1) . rand(11111, 99999));
           // $password = $this->generateRandomString();

         //   $studentuser->username = $username;
            $studentuser->school_id = $id;
          //  $studentuser->password = md5($password);
            $studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
            $studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
            $studentuser->superuser = 0;
            $studentuser->usertype = '6';
            $studentuser->createtime = time();
            // $studentuser->email=$username;
            $studentuser->status = (User::STATUS_ACTIVE);

            $checkprevmail = User::model()->find(array('condition' => 'email LIKE "' . $student->TM_STU_Email . '"'));

            if ($student->validate()):
                /*if (count($checkprevmail) != 0):
                    $parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "' . $student->TM_STU_First_Name . '" AND TM_STU_Last_Name LIKE "' . $student->TM_STU_Last_Name . '"'));

                    $studentID = 0;

                    if ($parentStudent->TM_STU_First_Name === $student->TM_STU_First_Name && $parentStudent->TM_STU_First_Name === $student->TM_STU_First_Name):
                        $studentID = $parentStudent->TM_STU_Id;
                        if (count($planprev) != 0):
                            StudentPlan::model()->deleteAll(array('condition' => 'TM_SPN_PlanId=' . $planprev->TM_SPN_PlanId . ' AND TM_SPN_StudentId=' . $studentuser->id));
                        endif;
                        $sttudentnewplan = new StudentPlan();
                        $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                        $currentplan = Plan::model()->findByPk($_POST['plan']);
                        $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                        $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                        $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                        $prevemail = $student->TM_STU_Email;
                        $student = Student::model()->findByPk($studentID);
                        $student->TM_STU_Email = $prevemail;
                        $studentuser = User::model()->findByPk($student->TM_STU_User_Id);
                        $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                        $studentuser->school_id = $id;
                        $student->TM_STU_School_Id = $student->TM_STU_School_Id;
                        $studentuser->save(false);
                        $student->save(false);
                        $parentmodel->email=$_POST['User']['email'];
                        $parentmodel->save(false);
                        $parentprofile->lastname=$_POST['Profile']['lastname'];
                        $parentprofile->firstname=$_POST['Profile']['firstname'];
                        $parentprofile->phonenumber=$_POST['Profile']['phonenumber'];
                        $parentprofile->save(false);
                        if ($sttudentnewplan->save(false)) :
                            $this->redirect(array('Viewstudent', 'id' =>$id,'studnet'=>$studentuser->id));
                        endif;
                    endif;

                else:*/
                    //$studentuser->save(false);
                    if (count($planprev) != 0):
                        StudentPlan::model()->deleteAll(array('condition' => 'TM_SPN_PlanId=' . $planprev->TM_SPN_PlanId . ' AND TM_SPN_StudentId=' . $studentuser->id));
                    endif;
                    $sttudentnewplan = new StudentPlan();
                    $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                    $currentplan = Plan::model()->findByPk($_POST['plan']);
                    $sttudentnewplan->TM_SPN_PlanId = $_POST['plan'];
                    $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                    $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                    $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                    $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                    /*$studentuserprofile = new Profile();
                    $studentuserprofile->user_id = $studentuser->id;
                    $studentuserprofile->lastname = $student->TM_STU_Last_Name;
                    $studentuserprofile->firstname = $student->TM_STU_First_Name;
                    $studentuserprofile->save(false);*/
                    $sttudentnewplan->save(false);
                    //$student->TM_STU_Parent_Id=$id;
                   // $student->TM_STU_Password = $password;
                    $student->TM_STU_User_Id = $studentuser->id;
                    $student->TM_STU_UpdatedBy = Yii::app()->user->id;
                    $student->TM_STU_Email=$_POST['User']['email'];
                    $parentmodel->email=$_POST['User']['email'];
                    $parentmodel->password=md5($_POST['User']['password']);
                    $parentmodel->save(false);
                    $parentprofile->lastname=$_POST['Profile']['lastname'];
                    $parentprofile->firstname=$_POST['Profile']['firstname'];
                    $parentprofile->phonenumber=$_POST['Profile']['phonenumber'];
                    $parentprofile->save(false);					
                    if ($student->save(false)) {
						
                        //$this->SendStudentMail($studentuser->id);
                        $this->redirect(array('Viewstudent', 'id' =>$id,'studnet'=>$studentuser->id));
                    }
                //endif;

            endif;
        }
        $this->render('addstudent', array('student' => $student, 'studentuser' => $studentuser, 'id' => $id, 'planprev' => $planprev,'parentmodel'=>$parentmodel,'parentprofile'=>$parentprofile));
    }

    public function getplan($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_PN_Id,c.TM_SD_Name FROM tm_plan AS a INNER JOIN tm_school_plan AS b ON a.TM_PN_Id=b.TM_SPN_PlanId INNER JOIN tm_standard AS c ON a.TM_PN_Standard_Id=c.TM_SD_Id WHERE b.TM_SPN_SchoolId='$id'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataProvider=$dataReader->readAll();
        return $dataProvider;
    }
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function actionViewstudent($id)
    {
        
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;                    
        $model = User::model()->findByPk($_GET['studnet']);
        $student = Student::model()->find(array('condition' => 'TM_STU_User_Id=' . $model->id));
        $parent = User::model()->findByPk($student->TM_STU_Parent_Id);

        $c = new CDbCriteria;
        $c->select = 't.TM_SPN_Id, t.TM_SPN_Publisher_Id,t.TM_SPN_StudentId,t.TM_SPN_PlanId,t.TM_SPN_SyllabusId,t.TM_SPN_StandardId,t.TM_SPN_SubjectId,t.TM_SPN_Status,t.TM_SPN_PaymentReference,t.TM_SPN_CouponId,t.TM_SPN_CreatedOn,t.TM_SPN_ExpieryDate,t.TM_SPN_CancelDate';
        $c->join = "INNER JOIN tm_school_plan tm_school_plan ON t.TM_SPN_PlanId = tm_school_plan.TM_SPN_PlanId";
        $c->compare("t.TM_SPN_StudentId", $model->id);

        $plan = StudentPlan::model()->find($c);
        $this->render('studentview', array(
            'model' => $model,
            'student' => $student,
            'parent' => $parent,
            'plan' => $plan
        ));
    }

/**********************************************************************************************************    
*   Manage students under the schools Ends
**********************************************************************************************************/  

    
    
    
         
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return School the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=School::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	/**
	 * Performs the AJAX validation.
	 * @param School $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='school-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    /**
     * function to send bulk emails for registered students.
     */
     public function actionSendregistrationemails()
     {
        $arr = explode(',', $_POST['theIds']);        
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_STU_User_Id' ,$arr );        
        $model = Student::model()->findAll($criteria);        
        foreach ($model as $value) {
            //update Order`s Status
            $this->SendStudentMail($value->TM_STU_User_Id);
            $value->TM_STU_Email_Status = '1';
            $value->save(false);
        }        
     }
     /**
      *Functions ends 
      */    
      /**
       * By Amal staff change password
       */
    public function actionStaffpassword($id)
    {
        $model = new UserChangePassword;
        $profile=Profile::model()->find(array('condition'=>'user_id='.$_GET['staff']));
        if(isset($_POST['UserChangePassword'])) {
            $model->attributes=$_POST['UserChangePassword'];
            if($model->validate()) {
                $new_password = User::model()->notsafe()->findbyPk($_GET['staff']);
                $new_password->password = UserModule::encrypting($model->password);
                $new_password->activkey=UserModule::encrypting(microtime().$model->password);
                $new_password->save(false);
                Yii::app()->user->setFlash('passwordchange',UserModule::t("New password is saved."));
                $this->refresh();
            }
        }
        $this->render('staffpassword',array('model'=>$model,'id'=>$id,'profile'=>$profile));
    }
      /**
       * By Amal staff change password Ends
       */ 
    /**
    * By Amal student change password
    */        
    public function actionStudentpassword($id)
    {
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;          
        $model = new UserChangePassword;
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$_GET['stud']));
        if(isset($_POST['UserChangePassword'])) {
            $model->attributes=$_POST['UserChangePassword'];
            if($model->validate()) {
                $new_password = User::model()->notsafe()->findbyPk($_GET['stud']);
                $new_password->password = UserModule::encrypting($model->password);
                $new_password->activkey=UserModule::encrypting(microtime().$model->password);
                $new_password->save(false);
                $student->TM_STU_Password=$model->password;
                $student->save(false);
                Yii::app()->user->setFlash('passwordchange',UserModule::t("New password is saved."));
                $this->refresh();
            }
        }
        $this->render('studentpassword',array('model'=>$model,'student'=>$student,'id'=>$id));
    } 
     /**
    * By Amal student change password Ends
    */    

    public function actionImportStudents($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;         
        $importlog=array();
        if(isset($_POST['submit'])):
            $file = fopen($_FILES['importcsv']['tmp_name'], "r");
            $count=1;
            while(! feof($file))
            {
                $values=fgetcsv($file);
                if($values[0]!=''):
                    $user=User::model()->find(array(
                            'condition' => 'username=:name',
                            'params' => array(':name' => $values[2])
                        )
                    );
                    if(User::model()->count('id=:value',array(':value'=>$user->id)))
                    {
                        $student=Student::model()->find(array(
                                'condition' => 'TM_STU_User_Id=:id',
                                'params' => array(':id' => $user->id)
                            )
                        );
                        if($values[8]=='Send'):
                            $mailstatus=0;
                        else:
                            $mailstatus=1;
                        endif;
                        $student->TM_STU_First_Name=$values[0];
                        $student->TM_STU_Last_Name=$values[1];
                        $student->TM_STU_Password =$values[3];
                        $student->TM_STU_Email_Status=$mailstatus;
                        $student->save(false);
                        $criteria = new CDbCriteria;
                        $criteria->addCondition('TM_SPN_StudentId=' . $user->id);
                        $studplan = StudentPlan::model()->deleteAll($criteria);
                        $std=Standard::model()->find(array(
                                'condition' => 'TM_SD_Name=:name',
                                'params' => array(':name' => $values[9])
                            )
                        );
                        $newplan = Plan::model()->find(array(
                                'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                            )
                        );
                        $sttudentnewplan=New StudentPlan();
                        $sttudentnewplan->TM_SPN_StudentId = $user->id;
                        $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                        $sttudentnewplan->TM_SPN_SyllabusId = $newplan->TM_PN_Syllabus_Id;
                        $sttudentnewplan->TM_SPN_StandardId = $newplan->TM_PN_Standard_Id;
                        $sttudentnewplan->TM_SPN_Status = 0;
                        $sttudentnewplan->save(false);
                        $studentuser=User::model()->findByPk($user->id);
                        $studentuser->password=md5($values[3]);
                        $studentuser->save(false);
                        $studentuserprofile =Profile::model()->findByPk($user->id);
                        $studentuserprofile->firstname = $values[0];
                        $studentuserprofile->lastname = $values[1];
                        $studentuserprofile->save(false);
                        $parentprofile=Profile::model()->findByPk($student->TM_STU_Parent_Id);
                        $parentprofile->firstname = $values[10];
                        $parentprofile->lastname = $values[11];
                        $parentprofile->phonenumber = $values[12];
                        $parentprofile->save(false);
                    }
                    else
                    {
                        $student = new Student();
                        $school = School::model()->findByPk($id);
                        $studentuser = new User();
                        $parentmodel = new User;
                        $parentprofile=new Profile;
                        $optionalusername=$values[14];
                        $student->TM_STU_School_Id = $id;
                        $student->TM_STU_City = $school->TM_SCL_City;
                        $student->TM_STU_State = $school->TM_SCL_State;
                        $student->TM_STU_Country_Id = $school->TM_SCL_Country_Id;
                        $student->TM_STU_School = $school->TM_SCL_Name;
                        $student->TM_STU_Email=$values[13];
                        if($values[8]=='Send'):
                            $mailstatus=1;
                        else:
                            $mailstatus=0;
                        endif;
                        $student->TM_STU_Email_Status=$mailstatus;
                        if($values[3]!='')
                        {
                            $student->TM_STU_Password =$values[3]; 
                            $studentuser->password = md5($values[3]);   
                        }
                        else
                        {
                            $password = $this->generateRandomString();
                            $student->TM_STU_Password =$password; 
                            $studentuser->password = md5($password);                             
                        }
                        $username = strtoupper(substr($values[0], 0, 1) . substr($values[1], 0, 1) . rand(11111, 99999));
                        $passwordparent = $this->generateRandomString();
                        $student->TM_STU_Parentpass=$passwordparent;
                        $parentmodel->username=$values[13];
                        $parentmodel->password=md5($passwordparent);
                        $parentmodel->email=$values[13];
                        $studentuser->username = $username;
                        //print_r($studentuser->username);exit;
                        $studentuser->school_id = $id;
                        
                        $studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
                        $studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
                        $studentuser->superuser = 0;
                        $studentuser->usertype = '6';
                        $studentuser->createtime = time();
                        if($values[7]=='Active'):
                            $status=1;
                        else:
                            $status=0;
                        endif;
                        $studentuser->status = $status;
                        $checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$values[13]. '"'));
                        if (count($checkprevmail) != 0):
                            if($optionalusername!=''):
                                $studentdetails=User::model()->find(array('condition'=>"username='".$optionalusername."'"));
                                $parentStudent=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studentdetails->id));
                            else:
                                $parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "' . $student->TM_STU_First_Name . '" AND TM_STU_Last_Name LIKE "' . $student->TM_STU_Last_Name . ' " AND TM_STU_Parent_Id='.$checkprevmail->id));
                            endif;
                            if(count($parentStudent)>0):
                                $std=Standard::model()->find(array(
                                        'condition' => 'TM_SD_Name=:name',
                                        'params' => array(':name' => $values[9])
                                    )
                                );
                                $newplan = Plan::model()->find(array(
                                        'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                        'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                                    )
                                );
                                $sttudentnewplan = new StudentPlan();
                                $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                $currentplan = Plan::model()->findByPk($newplan->TM_PN_Id);
                                $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                $sttudentnewplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
                                $sttudentnewplan->save(false);
                                //code by amal
                                $parentStudent->TM_STU_City = $school->TM_SCL_City;
                                $parentStudent->TM_STU_State = $school->TM_SCL_State;
                                $parentStudent->TM_STU_Country_Id = $school->TM_SCL_Country_Id;
                                $parentStudent->TM_STU_School = $school->TM_SCL_Name;
                                $parentStudent->save(false);
                            //ends
                            else:
                                $studentuser->save(false);
                                $studentuserprofile = new Profile();
                                $studentuserprofile->user_id = $studentuser->id;
                                $studentuserprofile->lastname = $values[1];
                                $studentuserprofile->firstname = $values[0];
                                $studentuserprofile->save(false);
                                $std=Standard::model()->find(array(
                                        'condition' => 'TM_SD_Name=:name',
                                        'params' => array(':name' => $values[9])
                                    )
                                );
                                $newplan = Plan::model()->find(array(
                                        'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                        'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                                    )
                                );
                                $sttudentnewplan = new StudentPlan();
                                $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                $currentplan = Plan::model()->findByPk($newplan->TM_PN_Id);
                                $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                                $sttudentnewplan->save(false);
                                $student->TM_STU_First_Name=$values[0];
                                $student->TM_STU_Last_Name=$values[1];
                                //$student->TM_STU_Password = $password;
                                $student->TM_STU_Parent_Id=$checkprevmail->id;
                                $student->TM_STU_User_Id = $studentuser->id;
                                $student->TM_STU_CreatedBy = Yii::app()->user->id;
                                $student->save(false);
                                //$this->SendStudentMail($studentuser->id);
                                //code by amal
                                $schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$id."' AND TM_SCL_SD_Id='".$currentplan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
                                if(count($schoolgrps)>0):
                                    $grpstudents=New GroupStudents();
                                    $grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
                                    $grpstudents->TM_GRP_PN_Id=$newplan->TM_PN_Id;
                                    $grpstudents->TM_GRP_SCL_Id=$id;
                                    $grpstudents->TM_GRP_SD_Id=$currentplan->TM_PN_Standard_Id;
                                    $grpstudents->TM_GRP_STUId=$student->TM_STU_User_Id;
                                    $grpstudents->TM_GRP_STUName=$values[0].' '.$values[1];
                                    $grpstudents->save(false);
                                endif;
                            endif;
                        else:
                            $checkprevuser=User::model()->find(array('condition' => 'email ="' .$values[13]. '"'));
                            if(count($checkprevuser)==0)
                            {
                                $parentmodel->usertype='7';
                                $parentmodel->status='1';
                                $parentmodel->save(false);
                                //print_r($parentmodel);exit;
                                $parentprofile->user_id=$parentmodel->id;
                                $parentprofile->lastname = $values[11];
                                $parentprofile->firstname = $values[10];
                                $parentprofile->phonenumber = $values[12];
                                $parentprofile->save(false);
                                $studentuser->save(false);
                                $studentuserprofile = new Profile();
                                $studentuserprofile->user_id = $studentuser->id;
                                $studentuserprofile->lastname = $values[1];
                                $studentuserprofile->firstname = $values[0];
                                $studentuserprofile->save(false);
                                $std=Standard::model()->find(array(
                                        'condition' => 'TM_SD_Name=:name',
                                        'params' => array(':name' => $values[9])
                                    )
                                );
                                $newplan = Plan::model()->find(array(
                                        'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                        'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                                    )
                                );
                                $sttudentnewplan = new StudentPlan();
                                $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                $currentplan = Plan::model()->findByPk($newplan->TM_PN_Id);
                                $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                                $sttudentnewplan->save(false);

                                //$student->TM_STU_Parent_Id=$id;
                                $student->TM_STU_First_Name=$values[0];
                                $student->TM_STU_Last_Name=$values[1];
                                //$student->TM_STU_Password = $password;
                                $student->TM_STU_Parent_Id=$parentmodel->id;
                                $student->TM_STU_User_Id = $studentuser->id;
                                $student->TM_STU_CreatedBy = Yii::app()->user->id;
                                $student->save(false);
                                //$this->SendStudentMail($studentuser->id);
                                $schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$id."' AND TM_SCL_SD_Id='".$currentplan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
                                if(count($schoolgrps)>0):
                                    $grpstudents=New GroupStudents();
                                    $grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
                                    $grpstudents->TM_GRP_PN_Id=$newplan->TM_PN_Id;
                                    $grpstudents->TM_GRP_SCL_Id=$id;
                                    $grpstudents->TM_GRP_SD_Id=$currentplan->TM_PN_Standard_Id;
                                    $grpstudents->TM_GRP_STUId=$student->TM_STU_User_Id;
                                    $grpstudents->TM_GRP_STUName=$values[0].' '.$values[1];
                                    $grpstudents->save(false);
                                endif;
                            }
                        endif;
                        $importlog[$count]['student']='1';
                    }
                endif;
                $count++;
            }
        endif;

        $this->render('studentimport', array(
            'id' => $id,
            'importlog'=>$importlog
        ));
    } 
//code by amal
    public function SendAdminMail($userid,$password)
    {
        $user=User::model()->findByPk($userid);
        $profile=Profile::model()->findByPk($userid);
        $email=$user->email;
        $adminEmail = Yii::app()->params['noreplayEmail'];
        $userroles=UserRoles::model()->findAll('TM_UR_User_Id='.$userid);
        $count=count($userroles);
        $roles="";
        if($count!=1):
            foreach($userroles AS $key=>$userrole):
                if($key+1<$count):
                    $roles.=User::itemAlias("UserTypes",$userrole->TM_UR_Role).",";
                else:
                    $roles.=User::itemAlias("UserTypes",$userrole->TM_UR_Role);
                endif;
            endforeach;
        else:
            foreach($userroles AS $key=>$userrole):
                $roles.=User::itemAlias("UserTypes",$userrole->TM_UR_Role);
            endforeach;
        endif;
        $message="<p>Hi ".$profile['firstname'].",</p>
                    <p>You have been added as admin in TabbieMath. Your login cedentials are in as shown below.</p>
                    <p><div style='border:solid 1px black;background-color: #E6E6FA;width:450px;height: 150px;padding: 20px;text-align: center;'>
                        <p><b>Username </b>: ".$user->username."</p>
                        <p><b>Password </b>: ".$password."</p>                        
                    </div></p>
                    <p>Regards,<br>The TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "smtpout.secureserver.net"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;
        $mail->Username = "services@tabbiemath.com"; // SMTP username
        $mail->Password = "Ta88ieMe"; // SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($email);
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath: Admin Access';
        $mail->Body = $message;

        return $mail->Send();
    }
    //ends      
    public function SendStudentMail($id)
    {

        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$id));
        $parent=User::model()->findByPk($student->TM_STU_Parent_Id);
        $parentprofile=Profile::model()->findByPk($student->TM_STU_Parent_Id);
        $studentuser=User::model()->findByPk($student->TM_STU_User_Id);
        $adminEmail = Yii::app()->params['noreplayEmail'];
        $activation_url = $this->createAbsoluteUrl('/user/login/');
        $schoolname=($student->TM_STU_School=='0'?$student->TM_STU_SchoolCustomname:School::model()->findByPk($student->TM_STU_School)->TM_SCL_Name);
        $message="<p>Dear ".$parentprofile->firstname.",</p>
                    <p>Your son/daughter has been added to school: ".$schoolname." </p>
                    <p>Below are his/her login details.</p>
                    <p><div style='border:solid 1px black;background-color: #E6E6FA;padding: 10px;text-align: center;'>
                        <p><b>Student Name</b> : ".$student->TM_STU_First_Name." ".$student->TM_STU_Last_Name."</p>
                        <p><b>Username </b>: ".$studentuser->username."</p>
                        <p><b>Password </b>: ".$student->TM_STU_Password."</p>
                    </div></p>
                    <p>Your parent login details are as follows.
                    You can use this login to register your other children, add new subscriptions and manage existing ones.
                    With this single login, you can also view progress of all your children.</p>
                    <p>www.tabbiemath.com</p>
                    <p><b>Parent Name </b>: ".$parentprofile->firstname." ".$parentprofile->lastname."</p>
                    <p><b>Username </b>: ".$parent->username."</p>";
                    if($student->TM_STU_Parentpass!=''):
                        $message.="<p><b>Password </b>: ".$student->TM_STU_Parentpass."</p>";
                    endif;                    
                    $message.="<p>Kind Regards,<br>The TabbieMath Team</p>
                  ";
        if (!filter_var($parent->username, FILTER_VALIDATE_EMAIL) === false) {
          $email=$parent->username;
        } else {
          $email=$parent->email;
        }
                          
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "smtpout.secureserver.net"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;
        $mail->Username = "services@tabbiemath.com"; // SMTP username
        $mail->Password = "Ta88ieMe"; // SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        //$mail->AddAddress($email);
        $mail->AddAddress('karthik@solminds.com');
        $mail->AddCC('karthik@solminds.com', 'Karthik');
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Student Registration ';
        $mail->Body = $message;
        $mail->Send();
    }
                      
}
