<?php

/**
 * This is the model class for table "{{chalangequestions}}".
 *
 * The followings are the available columns in table '{{chalangequestions}}':
 * @property integer $TM_CL_QS_Id
 * @property integer $TM_CL_QS_Question_Id
 * @property integer $TM_CL_QS_Chalange_Id
 * @property integer $TM_CL_QS_Difficulty
 * @property integer $TM_CL_QS_Marks
 */
class Chalangequestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{chalangequestions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CL_QS_Question_Id, TM_CL_QS_Chalange_Id', 'required'),
			array('TM_CL_QS_Question_Id, TM_CL_QS_Chalange_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CL_QS_Id, TM_CL_QS_Question_Id, TM_CL_QS_Chalange_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CL_QS_Id' => 'Tm Cl Qs',
			'TM_CL_QS_Question_Id' => 'Tm Cl Qs Question',
			'TM_CL_QS_Chalange_Id' => 'Tm Cl Qs Chalange',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CL_QS_Id',$this->TM_CL_QS_Id);
		$criteria->compare('TM_CL_QS_Question_Id',$this->TM_CL_QS_Question_Id);
		$criteria->compare('TM_CL_QS_Chalange_Id',$this->TM_CL_QS_Chalange_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Chalangequestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
