<?php

/**
 * This is the model class for table "{{classtypes}}".
 *
 * The followings are the available columns in table '{{classtypes}}':
 * @property integer $TM_CT_Id
 * @property integer $TM_CT_Class_Id
 * @property integer $TM_CT_Type_Id
 * @property integer $TM_CT_Status
 */
class Classtypes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{classtypes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CT_Class_Id, TM_CT_Type_Id, TM_CT_Status', 'required'),
			array('TM_CT_Class_Id, TM_CT_Type_Id, TM_CT_Status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CT_Id, TM_CT_Class_Id, TM_CT_Type_Id, TM_CT_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CT_Id' => 'Tm Ct',
			'TM_CT_Class_Id' => 'Tm Ct Class',
			'TM_CT_Type_Id' => 'Tm Ct Type',
			'TM_CT_Status' => 'Tm Ct Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CT_Id',$this->TM_CT_Id);
		$criteria->compare('TM_CT_Class_Id',$this->TM_CT_Class_Id);
		$criteria->compare('TM_CT_Type_Id',$this->TM_CT_Type_Id);
		$criteria->compare('TM_CT_Status',$this->TM_CT_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Classtypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
