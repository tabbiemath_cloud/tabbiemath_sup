<?php

/**
 * This is the model class for table "{{challenge_chapters}}".
 *
 * The followings are the available columns in table '{{challenge_chapters}}':
 * @property integer $TM_CE_CP_Id
 * @property integer $TM_CE_CP_Challenge_Id
 * @property integer $TM_CE_CP_Chapter_Id
 */
class ChallengeChapters extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{challenge_chapters}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CE_CP_Challenge_Id, TM_CE_CP_Chapter_Id', 'required'),
			array('TM_CE_CP_Challenge_Id, TM_CE_CP_Chapter_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CE_CP_Id, TM_CE_CP_Challenge_Id, TM_CE_CP_Chapter_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CE_CP_Id' => 'Tm Ce Cp',
			'TM_CE_CP_Challenge_Id' => 'Tm Ce Cp Challenge',
			'TM_CE_CP_Chapter_Id' => 'Tm Ce Cp Chapter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CE_CP_Id',$this->TM_CE_CP_Id);
		$criteria->compare('TM_CE_CP_Challenge_Id',$this->TM_CE_CP_Challenge_Id);
		$criteria->compare('TM_CE_CP_Chapter_Id',$this->TM_CE_CP_Chapter_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChallengeChapters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
