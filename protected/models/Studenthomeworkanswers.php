<?php

/**
 * This is the model class for table "{{studenthomeworkanswers}}".
 *
 * The followings are the available columns in table '{{studenthomeworkanswers}}':
 * @property integer $TM_STHWAR_Id
 * @property integer $TM_STHWAR_Student_Id
 * @property integer $TM_STHWAR_HW_Id
 * @property string $TM_STHWAR_Filename
 */
class Studenthomeworkanswers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{studenthomeworkanswers}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STHWAR_Student_Id, TM_STHWAR_HW_Id, TM_STHWAR_Filename', 'required'),
			array('TM_STHWAR_Student_Id, TM_STHWAR_HW_Id', 'numerical', 'integerOnly'=>true),
			array('TM_STHWAR_Filename', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STHWAR_Id, TM_STHWAR_Student_Id, TM_STHWAR_HW_Id, TM_STHWAR_Filename', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STHWAR_Id' => 'Tm Sthwar',
			'TM_STHWAR_Student_Id' => 'Tm Sthwar Student',
			'TM_STHWAR_HW_Id' => 'Tm Sthwar Hw',
			'TM_STHWAR_Filename' => 'Tm Sthwar Filename',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STHWAR_Id',$this->TM_STHWAR_Id);
		$criteria->compare('TM_STHWAR_Student_Id',$this->TM_STHWAR_Student_Id);
		$criteria->compare('TM_STHWAR_HW_Id',$this->TM_STHWAR_HW_Id);
		$criteria->compare('TM_STHWAR_Filename',$this->TM_STHWAR_Filename,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Studenthomeworkanswers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
