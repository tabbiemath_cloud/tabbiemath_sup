<?php

/**
 * This is the model class for table "{{settotal}}".
 *
 * The followings are the available columns in table '{{settotal}}':
 * @property integer $TM_TOT_Id
 * @property integer $TM_TOT_Plan_Id
 * @property string $TM_TOT_Total_Attempted
 * @property string $TM_TOT_Total_Correct
 * @property string $TM_TOT_Total_Questions
 */
class Settotal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{settotal}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_TOT_Plan_Id, TM_TOT_Total_Attempted, TM_TOT_Total_Correct', 'required'),
			array('TM_TOT_Plan_Id', 'numerical', 'integerOnly'=>true),
			array('TM_TOT_Total_Attempted, TM_TOT_Total_Correct', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_TOT_Id, TM_TOT_Plan_Id, TM_TOT_Total_Attempted, TM_TOT_Total_Correct', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_TOT_Id' => 'Tm Tot',
			'TM_TOT_Plan_Id' => 'Tm Tot Plan',
			'TM_TOT_Total_Attempted' => 'Tm Tot Total Attempted',
			'TM_TOT_Total_Correct' => 'Tm Tot Total Correct',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_TOT_Id',$this->TM_TOT_Id);
		$criteria->compare('TM_TOT_Plan_Id',$this->TM_TOT_Plan_Id);
		$criteria->compare('TM_TOT_Total_Attempted',$this->TM_TOT_Total_Attempted,true);
		$criteria->compare('TM_TOT_Total_Correct',$this->TM_TOT_Total_Correct,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settotal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
