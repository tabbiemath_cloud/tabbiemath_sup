<?php

/**
 * This is the model class for table "{{studenthomeworkanswersurl}}".
 *
 * The followings are the available columns in table '{{studenthomeworkanswersurl}}':
 * @property integer $TM_STHWARU_Id
 * @property integer $TM_STHWARU_Student_Id
 * @property integer $TM_STHWARU_HW_Id
 * @property string $TM_STHWARU_Url
 */
class Studenthomeworkanswersurl extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{studenthomeworkanswersurl}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STHWARU_Id, TM_STHWARU_Student_Id, TM_STHWARU_HW_Id, TM_STHWARU_Url', 'required'),
			array('TM_STHWARU_Id, TM_STHWARU_Student_Id, TM_STHWARU_HW_Id', 'numerical', 'integerOnly'=>true),
			array('TM_STHWARU_Url', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STHWARU_Id, TM_STHWARU_Student_Id, TM_STHWARU_HW_Id, TM_STHWARU_Url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STHWARU_Id' => 'Tm Sthwaru',
			'TM_STHWARU_Student_Id' => 'Tm Sthwaru Student',
			'TM_STHWARU_HW_Id' => 'Tm Sthwaru Hw',
			'TM_STHWARU_Url' => 'Tm Sthwaru Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STHWARU_Id',$this->TM_STHWARU_Id);
		$criteria->compare('TM_STHWARU_Student_Id',$this->TM_STHWARU_Student_Id);
		$criteria->compare('TM_STHWARU_HW_Id',$this->TM_STHWARU_HW_Id);
		$criteria->compare('TM_STHWARU_Url',$this->TM_STHWARU_Url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Studenthomeworkanswersurl the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
