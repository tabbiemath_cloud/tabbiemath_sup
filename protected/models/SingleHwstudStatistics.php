<?php

/**
 * This is the model class for table "{{single_hwstud_statistics}}".
 *
 * The followings are the available columns in table '{{single_hwstud_statistics}}':
 * @property integer $TM_SAR_STU_Id
 * @property integer $TM_SAR_STU_HW_Id
 * @property integer $TM_SAR_STU_Student_Id
 * @property string $TM_SAR_STU_Name
 * @property double $TM_SAR_STU_TotalMarks
 * @property double $TM_SAR_STU_Percentage
 * @property integer $TM_SAR_STU_Status
 */
class SingleHwstudStatistics extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{single_hwstud_statistics}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SAR_STU_HW_Id, TM_SAR_STU_Student_Id, TM_SAR_STU_Name, TM_SAR_STU_TotalMarks, TM_SAR_STU_Percentage, TM_SAR_STU_Status', 'required'),
			array('TM_SAR_STU_HW_Id, TM_SAR_STU_Student_Id, TM_SAR_STU_Status', 'numerical', 'integerOnly'=>true),
			array('TM_SAR_STU_TotalMarks, TM_SAR_STU_Percentage', 'numerical'),
			array('TM_SAR_STU_Name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SAR_STU_Id, TM_SAR_STU_HW_Id, TM_SAR_STU_Student_Id, TM_SAR_STU_Name, TM_SAR_STU_TotalMarks, TM_SAR_STU_Percentage, TM_SAR_STU_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SAR_STU_Id' => 'Tm Sar Stu',
			'TM_SAR_STU_HW_Id' => 'Tm Sar Stu Hw',
			'TM_SAR_STU_Student_Id' => 'Tm Sar Stu Student',
			'TM_SAR_STU_Name' => 'Tm Sar Stu Name',
			'TM_SAR_STU_TotalMarks' => 'Tm Sar Stu Total Marks',
			'TM_SAR_STU_Percentage' => 'Tm Sar Stu Percentage',
			'TM_SAR_STU_Status' => 'Tm Sar Stu Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SAR_STU_Id',$this->TM_SAR_STU_Id);
		$criteria->compare('TM_SAR_STU_HW_Id',$this->TM_SAR_STU_HW_Id);
		$criteria->compare('TM_SAR_STU_Student_Id',$this->TM_SAR_STU_Student_Id);
		$criteria->compare('TM_SAR_STU_Name',$this->TM_SAR_STU_Name,true);
		$criteria->compare('TM_SAR_STU_TotalMarks',$this->TM_SAR_STU_TotalMarks);
		$criteria->compare('TM_SAR_STU_Percentage',$this->TM_SAR_STU_Percentage);
		$criteria->compare('TM_SAR_STU_Status',$this->TM_SAR_STU_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SingleHwstudStatistics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
