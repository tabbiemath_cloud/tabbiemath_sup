<?php

/**
 * This is the model class for table "{{subject}}".
 *
 * The followings are the available columns in table '{{subject}}':
 * @property integer $TM_ST_Id
 * @property string $TM_ST_Name
 * @property string $TM_ST_CreatedOn
 * @property integer $TM_ST_CreatedBy
 * @property integer $TM_ST_Status
 */
class Subject extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subject}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_ST_Name', 'required'),
			array('TM_ST_CreatedBy, TM_ST_Status', 'numerical', 'integerOnly'=>true),
			array('TM_ST_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_ST_Id, TM_ST_Name, TM_ST_CreatedOn, TM_ST_CreatedBy, TM_ST_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_ST_Id' => 'ID',
			'TM_ST_Name' => 'Subject Name',
			'TM_ST_CreatedOn' => 'Created On',
			'TM_ST_CreatedBy' => 'Created By',
			'TM_ST_Status' => 'Status',
		);
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'SubjectStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_ST_Id',$this->TM_ST_Id);
		$criteria->compare('TM_ST_Name',$this->TM_ST_Name,true);
		$criteria->compare('TM_ST_CreatedOn',$this->TM_ST_CreatedOn,true);
		$criteria->compare('TM_ST_CreatedBy',$this->TM_ST_CreatedBy);
		$criteria->compare('TM_ST_Status',$this->TM_ST_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
