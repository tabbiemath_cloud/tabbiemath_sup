<?php

/**
 * This is the model class for table "{{difficulty}}".
 *
 * The followings are the available columns in table '{{difficulty}}':
 * @property integer $TM_DF_Id
 * @property string $TM_DF_Name
 * @property integer $TM_DF_Status
 */
class Difficulty extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{difficulty}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_DF_Name, TM_DF_Status', 'required'),
			array('TM_DF_Status', 'numerical', 'integerOnly'=>true),
			array('TM_DF_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_DF_Id, TM_DF_Name, TM_DF_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'question'=>array(self::HAS_MANY, 'Questions', 'TM_QN_Dificulty_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_DF_Id' => 'Tm Df',
			'TM_DF_Name' => 'Tm Df Name',
			'TM_DF_Status' => 'Tm Df Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_DF_Id',$this->TM_DF_Id);
		$criteria->compare('TM_DF_Name',$this->TM_DF_Name,true);
		$criteria->compare('TM_DF_Status',$this->TM_DF_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria, 'pagination'=>array(
                'pageSize'=>30,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Difficulty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
