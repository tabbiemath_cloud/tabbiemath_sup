<?php

/**
 * This is the model class for table "{{student_questions}}".
 *
 * The followings are the available columns in table '{{student_questions}}':
 * @property integer $TM_STU_QN_Id
 * @property integer $TM_STU_QN_Type
 * @property integer $TM_STU_QN_Student_Id
 * @property integer $TM_STU_QN_Test_Id
 * @property integer $TM_STU_QN_Question_Id
 * @property string $TM_STU_QN_Answer_Id
 * @property string $TM_STU_QN_Answer
 * @property integer $TM_STU_QN_Mark
 * @property integer $TM_STU_QN_Flag
 * @property integer $TM_STU_QN_Parent_Id
 * @property integer $TM_STU_QN_Number
 * @property integer $TM_STU_QN_Redo_Flag
 * @property integer $TM_STU_QN_RedoStatus
 */
class StudentQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{student_questions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STU_QN_Student_Id, TM_STU_QN_Test_Id, TM_STU_QN_Question_Id, TM_STU_QN_Answer_Id, TM_STU_QN_Answer, TM_STU_QN_Mark, TM_STU_QN_Flag', 'required'),
			array('TM_STU_QN_Student_Id, TM_STU_QN_Test_Id, TM_STU_QN_Question_Id, TM_STU_QN_Mark, TM_STU_QN_Flag', 'numerical', 'integerOnly'=>true),
			array('TM_STU_QN_Redo_Flag', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STU_QN_Id, TM_STU_QN_Student_Id, TM_STU_QN_Test_Id, TM_STU_QN_Question_Id, TM_STU_QN_Answer_Id, TM_STU_QN_Answer, TM_STU_QN_Mark, TM_STU_QN_Flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STU_QN_Id' => 'Tm Stu Qn',
			'TM_STU_QN_Student_Id' => 'Tm Stu Qn Student',
			'TM_STU_QN_Test_Id' => 'Tm Stu Qn Test',
			'TM_STU_QN_Question_Id' => 'Tm Stu Qn Question',
			'TM_STU_QN_Answer_Id' => 'Tm Stu Qn Answer',
			'TM_STU_QN_Answer' => 'Tm Stu Qn Answer',
			'TM_STU_QN_Mark' => 'Tm Stu Qn Mark',
			'TM_STU_QN_Flag' => 'Tm Stu Qn Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STU_QN_Id',$this->TM_STU_QN_Id);
		$criteria->compare('TM_STU_QN_Student_Id',$this->TM_STU_QN_Student_Id);
		$criteria->compare('TM_STU_QN_Test_Id',$this->TM_STU_QN_Test_Id);
		$criteria->compare('TM_STU_QN_Question_Id',$this->TM_STU_QN_Question_Id);
		$criteria->compare('TM_STU_QN_Answer_Id',$this->TM_STU_QN_Answer_Id);
		$criteria->compare('TM_STU_QN_Answer',$this->TM_STU_QN_Answer,true);
		$criteria->compare('TM_STU_QN_Mark',$this->TM_STU_QN_Mark);
		$criteria->compare('TM_STU_QN_Flag',$this->TM_STU_QN_Flag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
