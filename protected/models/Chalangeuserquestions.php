<?php

/**
 * This is the model class for table "{{chalangeuserquestions}}".
 *
 * The followings are the available columns in table '{{chalangeuserquestions}}':
 * @property integer $TM_CEUQ_Id
 * @property integer $TM_CEUQ_Challenge
 * @property integer $TM_CEUQ_Question_Id
 * @property integer $TM_CEUQ_Parent_Id
 * @property integer $TM_CEUQ_Difficulty
 * @property integer $TM_CEUQ_Answer_Id
 * @property integer $TM_CEUQ_Marks
 * @property integer $TM_CEUQ_User_Id
 * @property integer $TM_CEUQ_Iteration
 * @property integer $TM_CEUQ_Status
 */
class Chalangeuserquestions extends CActiveRecord
{
    public $total;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{chalangeuserquestions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CEUQ_Question_Id, TM_CEUQ_Answer_Id, TM_CEUQ_Marks, TM_CEUQ_User_Id', 'required'),
			array('TM_CEUQ_Question_Id, TM_CEUQ_Answer_Id, TM_CEUQ_Marks, TM_CEUQ_User_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CEUQ_Id, TM_CEUQ_Question_Id, TM_CEUQ_Answer_Id, TM_CEUQ_Marks, TM_CEUQ_User_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'question'=>array(self::BELONGS_TO, 'Questions', 'TM_CEUQ_Question_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CEUQ_Id' => 'Tm Ceuq',
			'TM_CEUQ_Question_Id' => 'Tm Ceuq Question',
			'TM_CEUQ_Answer_Id' => 'Tm Ceuq Answer',
			'TM_CEUQ_Marks' => 'Tm Ceuq Marks',
			'TM_CEUQ_User_Id' => 'Tm Ceuq User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CEUQ_Id',$this->TM_CEUQ_Id);
		$criteria->compare('TM_CEUQ_Question_Id',$this->TM_CEUQ_Question_Id);
		$criteria->compare('TM_CEUQ_Answer_Id',$this->TM_CEUQ_Answer_Id);
		$criteria->compare('TM_CEUQ_Marks',$this->TM_CEUQ_Marks);
		$criteria->compare('TM_CEUQ_User_Id',$this->TM_CEUQ_User_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Chalangeuserquestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
