<?php

/**
 * This is the model class for table "{{single_hwquest_statistics}}".
 *
 * The followings are the available columns in table '{{single_hwquest_statistics}}':
 * @property integer $TM_SAR_QN_Id
 * @property integer $TM_SAR_QN_HW_Id
 * @property integer $TM_SAR_QN_Student_Id
 * @property integer $TM_SAR_QN_Parent_Id
 * @property integer $TM_SAR_QN_Question_Id
 * @property integer $TM_SAR_QN_Type
 * @property double $TM_SAR_QN_Marks
 */
class SingleHwquestStatistics extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{single_hwquest_statistics}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SAR_QN_HW_Id, TM_SAR_QN_Student_Id, TM_SAR_QN_Parent_Id, TM_SAR_QN_Question_Id, TM_SAR_QN_Type, TM_SAR_QN_Marks', 'required'),
			array('TM_SAR_QN_HW_Id, TM_SAR_QN_Student_Id, TM_SAR_QN_Parent_Id, TM_SAR_QN_Question_Id, TM_SAR_QN_Type', 'numerical', 'integerOnly'=>true),
			array('TM_SAR_QN_Marks', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SAR_QN_Id, TM_SAR_QN_HW_Id, TM_SAR_QN_Student_Id, TM_SAR_QN_Parent_Id, TM_SAR_QN_Question_Id, TM_SAR_QN_Type, TM_SAR_QN_Marks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SAR_QN_Id' => 'Tm Sar Qn',
			'TM_SAR_QN_HW_Id' => 'Tm Sar Qn Hw',
			'TM_SAR_QN_Student_Id' => 'Tm Sar Qn Student',
			'TM_SAR_QN_Parent_Id' => 'Tm Sar Qn Parent',
			'TM_SAR_QN_Question_Id' => 'Tm Sar Qn Question',
			'TM_SAR_QN_Type' => 'Tm Sar Qn Type',
			'TM_SAR_QN_Marks' => 'Tm Sar Qn Marks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SAR_QN_Id',$this->TM_SAR_QN_Id);
		$criteria->compare('TM_SAR_QN_HW_Id',$this->TM_SAR_QN_HW_Id);
		$criteria->compare('TM_SAR_QN_Student_Id',$this->TM_SAR_QN_Student_Id);
		$criteria->compare('TM_SAR_QN_Parent_Id',$this->TM_SAR_QN_Parent_Id);
		$criteria->compare('TM_SAR_QN_Question_Id',$this->TM_SAR_QN_Question_Id);
		$criteria->compare('TM_SAR_QN_Type',$this->TM_SAR_QN_Type);
		$criteria->compare('TM_SAR_QN_Marks',$this->TM_SAR_QN_Marks);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SingleHwquestStatistics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
