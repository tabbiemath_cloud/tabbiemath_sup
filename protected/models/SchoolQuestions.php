<?php

/**
 * This is the model class for table "{{school_questions}}".
 *
 * The followings are the available columns in table '{{school_questions}}':
 * @property string $TM_SQ_Id
 * @property integer $TM_SQ_School_Id
 * @property integer $TM_SQ_Question_Type
 * @property integer $TM_SQ_Question_Id
 * @property integer $TM_SQ_Standard_Id
 * @property integer $TM_SQ_Chapter_Id
 * @property integer $TM_SQ_Topic_Id
 * @property integer $TM_SQ_Syllabus_Id
 * @property integer $TM_SQ_Publisher_id
 * @property integer $TM_SQ_Type
 * @property integer $TM_SQ_Status
 * @property integer $TM_SQ_QuestionReff
 * @property integer $TM_SQ_ImportStatus
 */
class SchoolQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{school_questions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SQ_School_Id, TM_SQ_Question_Type, TM_SQ_Question_Id, TM_SQ_Chapter_Id, TM_SQ_Topic_Id, TM_SQ_Syllabus_Id, TM_SQ_Publisher_id, TM_SQ_Type, TM_SQ_Status', 'required'),
			array('TM_SQ_School_Id, TM_SQ_Question_Type, TM_SQ_Question_Id, TM_SQ_Chapter_Id, TM_SQ_Topic_Id, TM_SQ_Syllabus_Id, TM_SQ_Publisher_id, TM_SQ_Type, TM_SQ_Status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SQ_Id, TM_SQ_School_Id, TM_SQ_Question_Type, TM_SQ_Question_Id, TM_SQ_Chapter_Id, TM_SQ_Topic_Id, TM_SQ_Syllabus_Id, TM_SQ_Publisher_id, TM_SQ_Type, TM_SQ_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(        
            'question'=>array(self::BELONGS_TO, 'Questions', 'TM_SQ_Question_Id'),
            'publisher'=>array(self::BELONGS_TO, 'Publishers', 'TM_QN_Publisher_Id'),
            'standard'=>array(self::BELONGS_TO, 'Standard', 'TM_SQ_Standard_Id'),
            'chapter'=>array(self::BELONGS_TO, 'Chapter', 'TM_SQ_Chapter_Id'),
            'topic'=>array(self::BELONGS_TO, 'Topic', 'TM_SQ_Topic_Id'),            
            'types'=>array(self::BELONGS_TO, 'Types', 'TM_SQ_Question_Type'), 
            'syllabus'=>array(self::BELONGS_TO, 'Syllabus', 'TM_SQ_Syllabus_Id'),                                               
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SQ_Id' => 'Tm Sq',
			'TM_SQ_School_Id' => 'Tm Sq School',
			'TM_SQ_Question_Type' => 'Tm Sq Question Type',
			'TM_SQ_Question_Id' => 'Tm Sq Question',
			'TM_SQ_Chapter_Id' => 'Tm Sq Chapter',
			'TM_SQ_Topic_Id' => 'Tm Sq Topic',
			'TM_SQ_Syllabus_Id' => 'Tm Sq Syllabus',
			'TM_SQ_Publisher_id' => 'Tm Sq Publisher',
            'TM_SQ_Standard_Id'=>'TM_SQ_Standard_Id',
			'TM_SQ_Type' => 'Tm Sq Type',
			'TM_SQ_Status' => 'Tm Sq Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SQ_Id',$this->TM_SQ_Id,true);
		$criteria->compare('TM_SQ_School_Id',$this->TM_SQ_School_Id);
		$criteria->compare('TM_SQ_Question_Type',$this->TM_SQ_Question_Type);
		$criteria->compare('TM_SQ_Question_Id',$this->TM_SQ_Question_Id);
		$criteria->compare('TM_SQ_Chapter_Id',$this->TM_SQ_Chapter_Id);
		$criteria->compare('TM_SQ_Topic_Id',$this->TM_SQ_Topic_Id);
		$criteria->compare('TM_SQ_Syllabus_Id',$this->TM_SQ_Syllabus_Id);
		$criteria->compare('TM_SQ_Publisher_id',$this->TM_SQ_Publisher_id);
        $criteria->compare('TM_SQ_Standard_Id',$this->TM_SQ_Standard_Id);
		$criteria->compare('TM_SQ_Type',$this->TM_SQ_Type);
		$criteria->compare('TM_SQ_Status',$this->TM_SQ_Status);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function GetTeachorList($id)
    {
        $list=CHtml::listData(Teachers::model()->findAll(array('condition'=>'TM_TH_SchoolId='.$id,'order' => 'TM_TH_Name')),'TM_TH_Id','TM_TH_Name');        
        //$list[0]='System';
        return $list ;
    } 
    public function GetCreatorList()
    {
        //$list=CHtml::listData(Teachers::model()->findAll(array('condition'=>'TM_TH_SchoolId='.Yii::app()->session['school'],'order' => 'TM_TH_Name')),'TM_TH_Id','TM_TH_Name');        
        $list=array(0=>'System',1=>'School');
        return $list ;
    }    
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SchoolQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
