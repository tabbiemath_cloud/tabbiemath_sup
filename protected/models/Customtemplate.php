<?php

/**
 * This is the model class for table "{{schoolcustomtemplate}}".
 *
 * The followings are the available columns in table '{{schoolcustomtemplate}}':
 * @property integer $TM_SCT_Id
 * @property string $TM_SCT_Name
 * @property string $TM_SCT_Description
 * @property integer $TM_SCT_School_Id
 * @property integer $TM_SCT_Publisher_Id
 * @property integer $TM_SCT_Syllabus_Id
 * @property integer $TM_SCT_Standard_Id
 * @property integer $TM_SCT_Status
 * @property string $TM_SCT_CreatedOn
 * @property integer $TM_SCT_CreatedBy
 * @property integer $TM_SCT_Privacy
 */
class Customtemplate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{schoolcustomtemplate}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SCT_Name, TM_SCT_School_Id, TM_SCT_Standard_Id, TM_SCT_Status, TM_SCT_CreatedBy,TM_SCT_Privacy', 'required'),
			array('TM_SCT_School_Id, TM_SCT_Publisher_Id, TM_SCT_Syllabus_Id, TM_SCT_Standard_Id, TM_SCT_Status, TM_SCT_CreatedBy', 'numerical', 'integerOnly'=>true),
			array('TM_SCT_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SCT_Id, TM_SCT_Name, TM_SCT_Description, TM_SCT_School_Id, TM_SCT_Publisher_Id, TM_SCT_Syllabus_Id, TM_SCT_Standard_Id, TM_SCT_Status, TM_SCT_CreatedOn, TM_SCT_CreatedBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
        'standard'=>array(self::BELONGS_TO, 'Standard', 'TM_SCT_Standard_Id'),
        'homework'=>array(self::HAS_MANY, 'StudentHomeworks', 'TM_STHW_HomeWork_Id'), 
        'marker'=>array(self::HAS_MANY, 'Homeworkmarking', 'TM_HWM_Homework_Id'),       
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SCT_Id' => 'Tm Sct',
			'TM_SCT_Name' => 'Name',
			'TM_SCT_Description' => 'Description',
			'TM_SCT_School_Id' => 'School',
			'TM_SCT_Publisher_Id' => 'Publisher',
			'TM_SCT_Syllabus_Id' => 'Syllabus',
			'TM_SCT_Standard_Id' => 'Standard',
			'TM_SCT_Status' => 'Status',
			'TM_SCT_CreatedOn' => 'Created On',
			'TM_SCT_CreatedBy' => 'Created By',
            'TM_SCT_Privacy'=>'Availability'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id,$standard)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SCT_Id',$this->TM_SCT_Id);
		$criteria->compare('TM_SCT_Name',$this->TM_SCT_Name,true);
		$criteria->compare('TM_SCT_Description',$this->TM_SCT_Description,true);
		$criteria->compare('TM_SCT_School_Id',Yii::app()->session['school']);
		$criteria->compare('TM_SCT_Publisher_Id',$this->TM_SCT_Publisher_Id);
		$criteria->compare('TM_SCT_Syllabus_Id',$this->TM_SCT_Syllabus_Id);
		$criteria->compare('TM_SCT_Standard_Id',$standard);
		$criteria->compare('TM_SCT_Status',$status);
		$criteria->compare('TM_SCT_CreatedOn',$this->TM_SCT_CreatedOn,true);
		//$criteria->compare('TM_SCT_CreatedBy',$this->TM_SCT_CreatedBy);
        $criteria->addCondition("(TM_SCT_CreatedBy='".Yii::app()->user->id."' AND TM_SCT_Privacy IN (0,1)) OR (TM_SCT_CreatedBy!='".Yii::app()->user->id."' AND TM_SCT_Privacy =0 AND TM_SCT_Status=0)");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),            
		));
	}
    public static function itemAlias($type, $code = NULL)
    {
        $_items = array(

            'HomeworkStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    public static function itemAvail($type, $code = NULL)
    {
        $_items = array(

            'HomeworkAvailability' => array(
                '0' => ('Public'),
                '1' => ('Private'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    public function getBlueprintName($bpid,$type)
	{
		if($type==0):
			$type="Manual";
		else:
			$type=Blueprints::model()->findByPk($bpid)->TM_BP_Name;
		endif;
		return $type;
	}    
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Customtemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
