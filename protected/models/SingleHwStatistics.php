<?php

/**
 * This is the model class for table "{{single_hw_statistics}}".
 *
 * The followings are the available columns in table '{{single_hw_statistics}}':
 * @property integer $TM_SAR_Id
 * @property integer $TM_SAR_HW_Id
 * @property integer $TM_SAR_Total_Questions
 * @property double $TM_SAR_Total_Marks
 * @property integer $TM_SAR_Atempted
 * @property integer $TM_SAR_Not_Attempted
 */
class SingleHwStatistics extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{single_hw_statistics}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SAR_HW_Id, TM_SAR_Total_Questions, TM_SAR_Total_Marks, TM_SAR_Atempted, TM_SAR_Not_Attempted', 'required'),
			array('TM_SAR_HW_Id, TM_SAR_Total_Questions, TM_SAR_Atempted, TM_SAR_Not_Attempted', 'numerical', 'integerOnly'=>true),
			array('TM_SAR_Total_Marks', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SAR_Id, TM_SAR_HW_Id, TM_SAR_Total_Questions, TM_SAR_Total_Marks, TM_SAR_Atempted, TM_SAR_Not_Attempted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SAR_Id' => 'Tm Sar',
			'TM_SAR_HW_Id' => 'Tm Sar Hw',
			'TM_SAR_Total_Questions' => 'Tm Sar Total Questions',
			'TM_SAR_Total_Marks' => 'Tm Sar Total Marks',
			'TM_SAR_Atempted' => 'Tm Sar Atempted',
			'TM_SAR_Not_Attempted' => 'Tm Sar Not Attempted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SAR_Id',$this->TM_SAR_Id);
		$criteria->compare('TM_SAR_HW_Id',$this->TM_SAR_HW_Id);
		$criteria->compare('TM_SAR_Total_Questions',$this->TM_SAR_Total_Questions);
		$criteria->compare('TM_SAR_Total_Marks',$this->TM_SAR_Total_Marks);
		$criteria->compare('TM_SAR_Atempted',$this->TM_SAR_Atempted);
		$criteria->compare('TM_SAR_Not_Attempted',$this->TM_SAR_Not_Attempted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SingleHwStatistics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
