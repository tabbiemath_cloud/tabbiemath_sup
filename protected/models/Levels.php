<?php

/**
 * This is the model class for table "{{levels}}".
 *
 * The followings are the available columns in table '{{levels}}':
 * @property integer $TM_LV_Id
 * @property string $TM_LV_Name
 * @property string $TM_LV_icon
 * @property integer $TM_LV_Minpoint
 * @property integer $TM_LV_MaxPoint
 * @property integer $TM_LV_Status
 */
class Levels extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{levels}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_LV_Name, TM_LV_Minpoint, TM_LV_MaxPoint, TM_LV_Status', 'required'),
			array('TM_LV_Minpoint, TM_LV_MaxPoint, TM_LV_Status', 'numerical', 'integerOnly'=>true),
			array('TM_LV_Name, TM_LV_icon', 'length', 'max'=>250),
            array('TM_LV_icon', 'file', 'allowEmpty'=>false, 'types'=>'jpg,jpeg,gif,png','on'=>'create'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_LV_Id, TM_LV_Name, TM_LV_icon, TM_LV_Minpoint, TM_LV_MaxPoint, TM_LV_Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_LV_Id' => 'Id',
			'TM_LV_Name' => 'Name',
			'TM_LV_icon' => 'Icon',
			'TM_LV_Minpoint' => 'Minimum Point',
			'TM_LV_MaxPoint' => 'maximum Point',
			'TM_LV_Status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_LV_Id',$this->TM_LV_Id);
		$criteria->compare('TM_LV_Name',$this->TM_LV_Name,true);
		$criteria->compare('TM_LV_icon',$this->TM_LV_icon,true);
		$criteria->compare('TM_LV_Minpoint',$this->TM_LV_Minpoint);
		$criteria->compare('TM_LV_MaxPoint',$this->TM_LV_MaxPoint);
		$criteria->compare('TM_LV_Status',$this->TM_LV_Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public static function itemAlias($type,$code=NULL) {
        $_items = array(

            'LevelStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Levels the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
