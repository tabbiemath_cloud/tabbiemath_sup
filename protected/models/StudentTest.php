<?php

/**
 * This is the model class for table "{{student_test}}".
 *
 * The followings are the available columns in table '{{student_test}}':
 * @property integer $TM_STU_TT_Id
 * @property integer $TM_STU_TT_Publisher_Id
 * @property integer $TM_STU_TT_Student_Id
 * @property integer $TM_STU_TT_Type
 * @property integer $TM_STU_TT_Plan
 * @property integer $TM_STU_TT_Mode
 * @property string $TM_STU_TT_CreateOn
 * @property integer $TM_STU_TT_Status
 * @property string $TM_STU_TT_Mark
 * @property string $TM_STU_TT_TotalMarks
 * @property string $TM_STU_TT_Percentage
 * @property integer $TM_STU_TT_Redo_Action
 */
class StudentTest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{student_test}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STU_TT_Student_Id, TM_STU_TT_Type, TM_STU_TT_Mode, TM_STU_TT_CreateOn, TM_STU_TT_Status, TM_STU_TT_Mark', 'required'),
			array('TM_STU_TT_Student_Id, TM_STU_TT_Type, TM_STU_TT_Mode, TM_STU_TT_Status, TM_STU_TT_Mark', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STU_TT_Id, TM_STU_TT_Student_Id, TM_STU_TT_Type, TM_STU_TT_Mode, TM_STU_TT_CreateOn, TM_STU_TT_Status, TM_STU_TT_Mark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'publisher'=>array(self::BELONGS_TO, 'Publishers', 'TM_STU_TT_Publisher_Id'),
            'chapters'=>array(self::HAS_MANY, 'StudentTestChapters', 'TM_STC_Test_Id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STU_TT_Id' => 'Tm Stu Tt',
			'TM_STU_TT_Student_Id' => 'Tm Stu Tt Student',
			'TM_STU_TT_Type' => 'Tm Stu Tt Type',
			'TM_STU_TT_Mode' => 'Tm Stu Tt Mode',
			'TM_STU_TT_CreateOn' => 'Tm Stu Tt Create On',
			'TM_STU_TT_Status' => 'Tm Stu Tt Status',
			'TM_STU_TT_Mark' => 'Tm Stu Tt Mark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STU_TT_Id',$this->TM_STU_TT_Id);
		$criteria->compare('TM_STU_TT_Student_Id',$this->TM_STU_TT_Student_Id);
		$criteria->compare('TM_STU_TT_Type',$this->TM_STU_TT_Type);
		$criteria->compare('TM_STU_TT_Mode',$this->TM_STU_TT_Mode);
		$criteria->compare('TM_STU_TT_CreateOn',$this->TM_STU_TT_CreateOn,true);
		$criteria->compare('TM_STU_TT_Status',$this->TM_STU_TT_Status);
		$criteria->compare('TM_STU_TT_Mark',$this->TM_STU_TT_Mark);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
