<?php

/**
 * This is the model class for table "{{teacher_standards}}".
 *
 * The followings are the available columns in table '{{teacher_standards}}':
 * @property integer $TM_TE_ST_Id
 * @property integer $TM_TE_ST_User_Id
 * @property integer $TM_TE_ST_Teacher_Id
 * @property integer $TM_TE_ST_Standard_Id
 */
class TeacherStandards extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{teacher_standards}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_TE_ST_User_Id, TM_TE_ST_Teacher_Id, TM_TE_ST_Standard_Id', 'required'),
			array('TM_TE_ST_User_Id, TM_TE_ST_Teacher_Id, TM_TE_ST_Standard_Id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_TE_ST_Id, TM_TE_ST_User_Id, TM_TE_ST_Teacher_Id, TM_TE_ST_Standard_Id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
        'standard'=>array(self::BELONGS_TO, 'Standard', 'TM_TE_ST_Standard_Id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_TE_ST_Id' => 'Tm Te St',
			'TM_TE_ST_User_Id' => 'Tm Te St User',
			'TM_TE_ST_Teacher_Id' => 'Tm Te St Teacher',
			'TM_TE_ST_Standard_Id' => 'Tm Te St Standard',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_TE_ST_Id',$this->TM_TE_ST_Id);
		$criteria->compare('TM_TE_ST_User_Id',$this->TM_TE_ST_User_Id);
		$criteria->compare('TM_TE_ST_Teacher_Id',$this->TM_TE_ST_Teacher_Id);
		$criteria->compare('TM_TE_ST_Standard_Id',$this->TM_TE_ST_Standard_Id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TeacherStandards the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
