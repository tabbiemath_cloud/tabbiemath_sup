<?php

/**
 * This is the model class for table "{{question}}".
 *
 * The followings are the available columns in table '{{question}}':
 * @property integer $TM_QN_Id
 * @property integer $TM_QN_Parent_Id
 * @property integer $TM_QN_Publisher_Id
 * @property integer $TM_QN_Syllabus_Id
 * @property integer $TM_QN_Standard_Id
 * @property integer $TM_QN_Subject_Id
 * @property integer $TM_QN_Topic_Id
 * @property integer $TM_QN_Section_Id
 * @property integer $TM_QN_Dificulty_Id
 * @property integer $TM_QN_Type_Id
 * @property string  $TM_QN_Question
 * @property string  $TM_QN_Image
 * @property integer $TM_QN_Answer_Type
 * @property integer $TM_QN_Solutions
 * @property string  $TM_QN_Solution_Image
 * @property integer $TM_QN_Option_Count
 * @property integer $TM_QN_Editor
 * @property integer $TM_QN_Totalmarks
 * @property integer $TM_QN_Teacher_Id
 * @property integer $TM_QN_Pattern
 * @property integer $TM_QN_QuestionNumber
 * @property string  $TM_QN_QuestionReff
 * @property string  $TM_QN_CreatedOn
 * @property string  $TM_QN_UpdatedOn
 * @property integer $TM_QN_CreatedBy
 * @property integer $TM_QN_UpdatedBy
 * @property integer $TM_QN_Status
 * @property integer $TM_QN_School_Id
 * @property integer $TM_QN_Show_Option
 */
class Questions extends CActiveRecord
{

    public  $availability;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{question}}';
	}

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_QN_Publisher_Id, TM_QN_Syllabus_Id, TM_QN_Standard_Id, TM_QN_Topic_Id, TM_QN_Section_Id, TM_QN_Dificulty_Id, TM_QN_Type_Id, TM_QN_Status,availability,TM_QN_Teacher_Id,TM_QN_Pattern', 'required'),
			array('TM_QN_Parent_Id, TM_QN_Publisher_Id, TM_QN_Syllabus_Id, TM_QN_Standard_Id, TM_QN_Subject_Id, TM_QN_Topic_Id, TM_QN_Section_Id, TM_QN_Dificulty_Id, TM_QN_Type_Id, TM_QN_Answer_Type, TM_QN_Option_Count, TM_QN_Editor, TM_QN_Totalmarks, TM_QN_CreatedBy, TM_QN_Status', 'numerical', 'integerOnly'=>true,'on' => 'Create'),
            array('TM_QN_Publisher_Id, TM_QN_Syllabus_Id, TM_QN_Standard_Id, TM_QN_Subject_Id, TM_QN_Topic_Id, TM_QN_Section_Id, TM_QN_Dificulty_Id, TM_QN_Type_Id, TM_QN_Status', 'numerical', 'integerOnly'=>true,'on' => 'Update'),
			array('TM_QN_Image', 'length', 'max'=>250),
			array('TM_QN_Solutions,TM_QN_Pattern,TM_QN_Image,TM_QN_QuestionReff,TM_QN_Solution_Image,TM_QN_UpdatedOn,TM_QN_School_Id,TM_QN_Show_Option', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_QN_Id, TM_QN_Parent_Id, TM_QN_Publisher_Id, TM_QN_Syllabus_Id, TM_QN_Standard_Id, TM_QN_Subject_Id, TM_QN_Topic_Id, TM_QN_Section_Id, TM_QN_Dificulty_Id, TM_QN_Type_Id, TM_QN_Question, TM_QN_Image, TM_QN_Answer_Type, TM_QN_Option_Count, TM_QN_Editor, TM_QN_Totalmarks, TM_QN_CreatedOn, TM_QN_CreatedBy, TM_QN_Status,TM_QN_UpdatedBy,TM_QN_Skill,TM_QN_Question_type,TM_QN_Info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'syllabus'=>array(self::BELONGS_TO, 'Syllabus', 'TM_QN_Syllabus_Id'),
            'publisher'=>array(self::BELONGS_TO, 'Publishers', 'TM_QN_Publisher_Id'),
            'standard'=>array(self::BELONGS_TO, 'Standard', 'TM_QN_Standard_Id'),
            'chapter'=>array(self::BELONGS_TO, 'Chapter', 'TM_QN_Topic_Id'),
            'topic'=>array(self::BELONGS_TO, 'Topic', 'TM_QN_Section_Id'),
            'dificulty'=>array(self::BELONGS_TO, 'Difficulty', 'TM_QN_Dificulty_Id'),
            'types'=>array(self::BELONGS_TO, 'Types', 'TM_QN_Type_Id'),
            'teachers'=>array(self::BELONGS_TO, 'Teachers', 'TM_QN_Teacher_Id'),
            'answers'=>array(self::HAS_MANY, 'Answers', 'TM_AR_Question_Id'),
            'exams'=>array(self::HAS_MANY, 'QuestionExams', 'TM_QE_Question_Id'),
			'schoolquestions'=>array(self::HAS_MANY, 'SchoolQuestions', 'TM_SQ_Question_Id'),
            'mocks' => array(self::MANY_MANY, 'Mock', ' tm_mock_questions(TM_MQ_Question_Id, TM_MQ_Mock_Id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_QN_Id' => 'Id',
			'TM_QN_Parent_Id' => 'Master Question',
			'TM_QN_Publisher_Id' => 'Publisher',
			'TM_QN_Syllabus_Id' => 'Syllabus',
			'TM_QN_Standard_Id' => 'Standard',
			'TM_QN_Subject_Id' => 'Subject',
			'TM_QN_Topic_Id' => 'Chapter',
			'TM_QN_Section_Id' => 'Topic',
			'TM_QN_Dificulty_Id' => 'Difficulty',
			'TM_QN_Type_Id' => 'Type',
			'TM_QN_Question' => 'Question',
			'TM_QN_Image' => 'Image',
			'TM_QN_Answer_Type' => 'Answer Type',
			'TM_QN_Solutions' => 'Solution',
			'TM_QN_Option_Count' => 'No. Of Options',
			'TM_QN_Editor' => 'Editor',
			'TM_QN_Totalmarks' => 'Totalmarks',
			'TM_QN_Teacher_Id' => 'Teacher',
			'TM_QN_Pattern' => 'Pattern',
			'TM_QN_QuestionNumber' => 'Question Number',
			'TM_QN_QuestionReff' => 'Reference',
			'TM_QN_CreatedOn' => 'Created Date',
			'TM_QN_CreatedBy' => 'Created By User',
			'TM_QN_UpdatedOn' => 'Updated Date',
			'TM_QN_UpdatedBy' => 'Updated By User',
			'TM_QN_Status' => 'Status',
			'availability' => 'Availabilty',
            'TM_QN_Show_Option'=>'Print Options',
             'TM_QN_School_Id'=>'Created By',
             'TM_QN_Skill'=> 'Skill',
             'TM_QN_Question_type'=>'Additonal Info',
             'TM_QN_Info'=>'Notes'


		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
    public function GetAdminStandards()
    {
        $adminstandrads=Adminstandards::model()->findAll(array('condition'=>'TM_SA_Admin_Id='.Yii::app()->user->id));
        $assignedstandards='';
        foreach($adminstandrads AS $key=>$standard):
        $assignedstandards.=($key==0?'':',').$standard->TM_SA_Standard_Id;
        endforeach; 
        return $assignedstandards;        
    }
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;        
		$criteria->compare('TM_QN_Id',$this->TM_QN_Id);
		$criteria->compare('TM_QN_Parent_Id',0);
		$criteria->compare('TM_QN_Publisher_Id',$this->TM_QN_Publisher_Id);
		$criteria->compare('TM_QN_Syllabus_Id',$this->TM_QN_Syllabus_Id);
        if($this->TM_QN_Standard_Id!=''):
            $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
        else:
            $standards=$this->GetAdminStandards();
            $criteria->addCondition('TM_QN_Standard_Id IN('.$standards.')');
        endif;        		
		$criteria->compare('TM_QN_Subject_Id',$this->TM_QN_Subject_Id);
		$criteria->compare('TM_QN_Topic_Id',$this->TM_QN_Topic_Id);
		$criteria->compare('TM_QN_Section_Id',$this->TM_QN_Section_Id);
		$criteria->compare('TM_QN_Dificulty_Id',$this->TM_QN_Dificulty_Id);
		$criteria->compare('TM_QN_Type_Id',$this->TM_QN_Type_Id);
		$criteria->compare('TM_QN_Question',$this->TM_QN_Question,true);
		$criteria->compare('TM_QN_Image',$this->TM_QN_Image,true);
		$criteria->compare('TM_QN_Answer_Type',$this->TM_QN_Answer_Type);
		$criteria->compare('TM_QN_Solutions',$this->TM_QN_Solutions);
		$criteria->compare('TM_QN_Option_Count',$this->TM_QN_Option_Count);
		$criteria->compare('TM_QN_Editor',$this->TM_QN_Editor);
		$criteria->compare('TM_QN_QuestionReff',$this->TM_QN_QuestionReff,true);
		$criteria->compare('TM_QN_Totalmarks',$this->TM_QN_Totalmarks);
		$criteria->compare('TM_QN_CreatedOn',$this->TM_QN_CreatedOn,true);
        if($this->TM_QN_Skill!=''):
        $criteria->addCondition('TM_QN_Skill REGEXP '.$this->TM_QN_Skill);
        endif;
        $criteria->compare('TM_QN_Question_type ',$this->TM_QN_Question_type,true);
        $criteria->compare('TM_QN_Info',$this->TM_QN_Info,true); 
        if($this->TM_QN_School_Id==1):
            
            $criteria->compare('TM_QN_School_Id','0');
        elseif($this->TM_QN_School_Id!=0):
            $criteria->compare('TM_QN_School_Id',$this->TM_QN_School_Id);
        endif;

        if($this->TM_QN_CreatedBy!='0'):
		    $criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
        endif;
        if($this->TM_QN_UpdatedBy!='0'):
            $criteria->compare('TM_QN_UpdatedBy',($this->TM_QN_UpdatedBy!=''?$this->TM_QN_UpdatedBy:Yii::app()->user->id));
        endif;
		//$criteria->compare('TM_QN_UpdatedBy',$this->TM_QN_UpdatedBy);
        if($this->TM_QN_Status!=''):
            $criteria->compare('TM_QN_Status',$this->TM_QN_Status);
        else:
            $criteria->addCondition('TM_QN_Status != 111');
        endif;
		$criteria->compare('TM_QN_Show_Option',$this->TM_QN_Show_Option);        		
		$criteria->compare('TM_QN_Teacher_Id',$this->TM_QN_Teacher_Id);
		$criteria->compare('TM_QN_Pattern',$this->TM_QN_Pattern,true);
		//$criteria->addCondition('TM_QN_School_Id =0');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
		));
	}
    public function getExams($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_EX_Name AS Name,a.TM_EX_Id AS Id
              FROM  tm_exams AS a, tm_question_exams AS b
              WHERE b.TM_QE_Question_Id='".$id."' AND b.TM_QE_Exam_Id=a.TM_EX_Id";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $exams=$dataReader->readAll();
        return $exams;
    }
    public function getReffs()
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT DISTINCT TM_QN_QuestionReff FROM tm_question WHERE  TM_QN_QuestionReff != ''  ";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $exams=$dataReader->readAll();
        return $exams;
    }
    public static function OptionAlias($type,$code=NULL) {
        $_items = array(

            'OptionStatus' => array(
                '0' => ('Hide'),
                '1' => ('Show'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }    
    public static function itemAlias($type,$code=NULL) {
        $statuses=Questionstatusmaster::model()->findAll();
        $_items = array();
        foreach($statuses AS $status):
        $_items['QuestionStatus'][$status->TM_QSM_Id]=$status->TM_QSM_Name;
        endforeach;
        
        /*$_items = array(

            'QuestionStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
                '2' => ('Review'),
                '3' => ('Flagged for review'),
                '4' => ('Reviewed - Correct'),
                '5' => ('Reviewed - Incorrect'),
                '6' => ('Image'),
                '7' => ('editor'),
                '8' => ('Doubt'),
                '9' => ('Image Review'),
            ),
        );*/
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
	public function GetPreview($id)
	{
		$question=Questions::model()->findByPk($id);
		if($question->TM_QN_Type_Id=='5' || $question->TM_QN_Type_Id=='7'):
			$questioncontent=$question->TM_QN_Question;
			$subs=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$id."'","order"=>"TM_QN_Id ASC"));
			foreach($subs AS $key=>$sub):
				$count=$key+1;
				$questioncontent.="<br/><b>Question".$count."</b><br/>".$sub->TM_QN_Question;
			endforeach;
		else:
			$questioncontent=$question->TM_QN_Question;
		endif;
		$html="<div class='popover popup'  id='preview".$id."'><div class='arrow'></div><h3 class='popover-title' id='popover-left'>Question<a class='anchorjs-link' href='#popover-left'><span class='anchorjs-icon'></span></a></h3>  <div class='popover-content'>
	".$questioncontent."
  </div>
</div><a href='' data-id='".$id."' class='showpopup'>Preview</a>";
		return $html;
	}
    public function GetQuestion($id)
    {
        $question=Questions::model()->findByPk($id);
        if($question->TM_QN_Type_Id!='5' || $question->TM_QN_Type_Id!='7')
        {
            //return substr(strip_tags($question->TM_QN_Question), 0, 500);
            return $question->TM_QN_Question;
        }
        else
        {
            if($question->TM_QN_Question!='')
            {
//                return substr(strip_tags($question->TM_QN_Question), 0, 500);
                return $question->TM_QN_Question;
            }
            else
            {
                $subs=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$id."'","order"=>"TM_QN_Id ASC"));
                foreach($subs AS $key=>$sub):
                    if($sub->TM_QN_Question!='')
                    {
//                        return substr(strip_tags($sub->TM_QN_Question), 0, 500);
                        return $sub->TM_QN_Question;
                        break;
                    }
                    else
                    {

                    }
                endforeach;
            }

        }
    }
    public function GetUser($id)
    {
        $user=User::model()->findbyPk($id)->profile;
        return $user->firstname.' '.$user->lastname;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Questions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    
    public function GetquestionCreatorList($school)
    {
        $list=CHtml::listData(is_numeric($school) && $school!=1 ? User::model()->with('profile')->findAll(array('order' => 'profile.firstname ASC','condition'=>'school_id='.$school.' AND superuser = 0 AND usertype IN(9,12) AND status = 1')):User::model()->with('profile')->findAll(array('order' => 'profile.firstname ASC','condition'=>'superuser = 1 AND usertype=1 AND status = 1')),'id','profile.firstname');
        $list[0]='All';
        return $list ;
    }

    public function GetquestionUpdatorList($school)
    {
        $list=CHtml::listData(is_numeric($school) && $school!=1 ? User::model()->with('profile')->findAll(array('order' => 'profile.firstname ASC','condition'=>'(school_id='.$school.' AND superuser = 0 AND usertype IN(9,12) AND status = 1) OR (usertype = 1  AND superuser = 1 AND status = 1)')):User::model()->with('profile')->findAll(array('order' => 'profile.firstname ASC','condition'=>'superuser = 1 AND usertype=1 AND status = 1')),'id','profile.firstname');
        $list[0]='All';
        return $list ;
    }
    public function GetCreatorList()
    {
        $list=CHtml::listData(User::model()->with('profile')->findAll(array('order' => 'id','condition'=>'usertype NOT IN(6,7)')),'id','profile.firstname');
        $list[0]='All';
        return $list ;
    }
    public function searchschool($id,$questiontype)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_QN_Id',$this->TM_QN_Id);
		$criteria->compare('TM_QN_Parent_Id',0);
        $plans=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId),TM_SPN_Publisher_Id','condition'=>'TM_SPN_SchoolId='.$id));
        $schoolsyllabus=School::model()->findByPk($id)->TM_SCL_Sylllabus_Id;
        $publishers='';
        $standards='';
        foreach($plans AS $key=>$plan):
            $publishers.=($key=='0'?$plan->TM_SPN_Publisher_Id:','.$plan->TM_SPN_Publisher_Id);
            $standards.=($key=='0'?$plan->TM_SPN_StandardId:','.$plan->TM_SPN_StandardId);
        endforeach;     
        $criteria->addCondition('TM_QN_Syllabus_Id='.$schoolsyllabus);        		
        if($this->TM_QN_Standard_Id!=''):
            $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
        else:
            if($standards!=''):
                $criteria->addCondition('TM_QN_Standard_Id IN ('.$standards.')');
            else:
                $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
            endif;
        endif;                				
		$criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
		$criteria->compare('TM_QN_Subject_Id',$this->TM_QN_Subject_Id);
		$criteria->compare('TM_QN_Topic_Id',$this->TM_QN_Topic_Id);
		$criteria->compare('TM_QN_Section_Id',$this->TM_QN_Section_Id);
		$criteria->compare('TM_QN_Dificulty_Id',$this->TM_QN_Dificulty_Id);
		$criteria->compare('TM_QN_Type_Id',$this->TM_QN_Type_Id);
		$criteria->compare('TM_QN_Question',$this->TM_QN_Question,true);
		$criteria->compare('TM_QN_Image',$this->TM_QN_Image,true);
		$criteria->compare('TM_QN_Answer_Type',$this->TM_QN_Answer_Type);
		$criteria->compare('TM_QN_Solutions',$this->TM_QN_Solutions);
		$criteria->compare('TM_QN_Option_Count',$this->TM_QN_Option_Count);
		$criteria->compare('TM_QN_Editor',$this->TM_QN_Editor);
		$criteria->compare('TM_QN_QuestionReff',$this->TM_QN_QuestionReff);
		$criteria->compare('TM_QN_Totalmarks',$this->TM_QN_Totalmarks);
		$criteria->compare('TM_QN_CreatedOn',$this->TM_QN_CreatedOn,true);
        $criteria->compare('TM_QN_Show_Option',$this->TM_QN_Show_Option);       
        if($questiontype!=''):
            if($questiontype=='0'):
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
                $school="0";
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->addCondition('TM_QN_School_Id IN ('.$school.')');                
            else:
                $school=$id;
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
            endif;
            
        else:
            $school="0,$id";
            //$criteria->compare('TM_QN_School_Id',$id);
            $criteria->addCondition('TM_QN_School_Id IN ('.$school.')');
        endif;
		$criteria->compare('TM_QN_UpdatedBy',$this->TM_QN_UpdatedBy);
        //$criteria->addCondition('TM_QN_Status=0');
        $criteria->compare('TM_QN_Status',0);
		$criteria->compare('TM_QN_Teacher_Id',$this->TM_QN_Teacher_Id);
		$criteria->compare('TM_QN_Pattern',$this->TM_QN_Pattern,true);
        //$criteria->with = array('schoolquestions'=>array('select' => false,'on' => 'TM_SQ_Question_Id =TM_QN_Id','joinType'=>'LEFT JOIN','condition'=>' TM_SQ_School_Id='.$id.' AND   TM_QN_Status=0 AND  TM_SQ_Type IN(0,1) AND TM_SQ_ImportStatus=0 AND  TM_SQ_Question_Id IS NOT NULL'));
        $criteria->with = array('schoolquestions'=>array('select' => false,'joinType'=>'LEFT JOIN','condition'=>' TM_SQ_School_Id='.$id.' AND   TM_QN_Status=0 AND  TM_SQ_Type IN(0,1) AND  TM_SQ_Standard_Id IN ('.$standards.') AND TM_SQ_ImportStatus=0 AND  TM_SQ_Question_Id IS NOT NULL'));
        $criteria->together=true;         
		return new CActiveDataProvider('Questions', array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
		));
	}    
	public function searchmasterimport($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
        $criteria->compare('TM_QN_Id',$this->TM_QN_Id);
        $criteria->compare('TM_QN_Parent_Id',0);
        $plans=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId),TM_SPN_Publisher_Id','condition'=>'TM_SPN_SchoolId='.$id));
        $schoolsyllabus=School::model()->findByPk($id)->TM_SCL_Sylllabus_Id;
        $publishers='';
        $standards='';
        foreach($plans AS $key=>$plan):
            $publishers.=($key=='0'?$plan->TM_SPN_Publisher_Id:','.$plan->TM_SPN_Publisher_Id);
            $standards.=($key=='0'?$plan->TM_SPN_StandardId:','.$plan->TM_SPN_StandardId);
        endforeach;
        $criteria->addCondition('TM_QN_Syllabus_Id='.$schoolsyllabus);
        if($this->TM_QN_Standard_Id==''):
            if($standards!=''):
                $criteria->addCondition('TM_QN_Standard_Id IN ('.$standards.')');
            else:
                $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
            endif;                              
        else:
            $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
        endif;
        $criteria->compare('TM_QN_Subject_Id',$this->TM_QN_Subject_Id);
        $criteria->compare('TM_QN_Topic_Id',$this->TM_QN_Topic_Id);
        $criteria->compare('TM_QN_Section_Id',$this->TM_QN_Section_Id);
        $criteria->compare('TM_QN_Dificulty_Id',$this->TM_QN_Dificulty_Id);
        $criteria->compare('TM_QN_Type_Id',$this->TM_QN_Type_Id);
        $criteria->compare('TM_QN_Question',$this->TM_QN_Question,true);
        $criteria->compare('TM_QN_Image',$this->TM_QN_Image,true);
        $criteria->compare('TM_QN_Answer_Type',$this->TM_QN_Answer_Type);
        $criteria->compare('TM_QN_Solutions',$this->TM_QN_Solutions);
        $criteria->compare('TM_QN_Option_Count',$this->TM_QN_Option_Count);
        $criteria->compare('TM_QN_Editor',$this->TM_QN_Editor);
        $criteria->compare('TM_QN_QuestionReff',$this->TM_QN_QuestionReff,true);
        $criteria->compare('TM_QN_Totalmarks',$this->TM_QN_Totalmarks);
        $criteria->compare('TM_QN_CreatedOn',$this->TM_QN_CreatedOn,true);  
        $criteria->compare('TM_QN_Show_Option',$this->TM_QN_Show_Option);                      
        //$criteria->compare('TM_QN_School_Id',$id);
        $criteria->compare('TM_QN_School_Id',0);        
        /*if($this->TM_QN_CreatedBy!=''):
            if($this->TM_QN_CreatedBy=='0'):
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
                $school="0";
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->addCondition('TM_QN_School_Id IN ('.$school.')');                
            else:
                $school=$id;
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
            endif;
            
        else:
            $school="0,$id";
            //$criteria->compare('TM_QN_School_Id',$id);
            $criteria->addCondition('TM_QN_School_Id IN ('.$school.')');
        endif;
        */
        $criteria->compare('TM_QN_UpdatedBy',$this->TM_QN_UpdatedBy);
        $criteria->addCondition('TM_QN_Status=0');
        $criteria->compare('TM_QN_Teacher_Id',$this->TM_QN_Teacher_Id);
        $criteria->compare('TM_QN_Pattern',$this->TM_QN_Pattern,true);
        //print_r($criteria);exit;
        //$criteria->join = 'LEFT JOIN tm_school_questions AS sq ON sq.TM_SQ_Question_Id = t.TM_QN_Id ';
        //$criteria->condition = 'sq.TM_SQ_School_Id='.$id.' AND  t.TM_QN_Status=0 AND sq.TM_SQ_Type IN(0,1) AND sq.TM_SQ_Standard_Id='.Yii::app()->session['standard'].' AND sq.TM_SQ_Question_Id IS NOT NULL';
        //print_r($criteria);exit;
        $criteria->with = array('schoolquestions'=>array('select' => false,'joinType'=>'LEFT JOIN','condition'=>'TM_SQ_School_Id='.$id.' AND TM_SQ_ImportStatus=1 AND  TM_SQ_Question_Id IS NOT NULL'));
        $criteria->together=true; 
        return new CActiveDataProvider('Questions', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
	
	}
    public function searchmastermanage($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->compare('TM_QN_Id',$this->TM_QN_Id);
        $criteria->compare('TM_QN_Parent_Id',0);
        $plans=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId),TM_SPN_Publisher_Id','condition'=>'TM_SPN_SchoolId='.$id));
        $schoolsyllabus=School::model()->findByPk($id)->TM_SCL_Sylllabus_Id;
        $publishers='';
        $standards='';
        foreach($plans AS $key=>$plan):
            $publishers.=($key=='0'?$plan->TM_SPN_Publisher_Id:','.$plan->TM_SPN_Publisher_Id);
            $standards.=($key=='0'?$plan->TM_SPN_StandardId:','.$plan->TM_SPN_StandardId);
        endforeach;
        $criteria->addCondition('TM_QN_Syllabus_Id='.$schoolsyllabus);
        if($this->TM_QN_Standard_Id==''):
            if($standards!=''):
                $criteria->addCondition('TM_QN_Standard_Id IN ('.$standards.')');
            else:
                $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
            endif;                          
        else:
            $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
        endif;
        $criteria->compare('TM_QN_Subject_Id',$this->TM_QN_Subject_Id);
        $criteria->compare('TM_QN_Topic_Id',$this->TM_QN_Topic_Id);
        $criteria->compare('TM_QN_Section_Id',$this->TM_QN_Section_Id);
        $criteria->compare('TM_QN_Dificulty_Id',$this->TM_QN_Dificulty_Id);
        $criteria->compare('TM_QN_Type_Id',$this->TM_QN_Type_Id);
        $criteria->compare('TM_QN_Question',$this->TM_QN_Question,true);
        $criteria->compare('TM_QN_Image',$this->TM_QN_Image,true);
        $criteria->compare('TM_QN_Answer_Type',$this->TM_QN_Answer_Type);
        $criteria->compare('TM_QN_Solutions',$this->TM_QN_Solutions);
        $criteria->compare('TM_QN_Option_Count',$this->TM_QN_Option_Count);
        $criteria->compare('TM_QN_Editor',$this->TM_QN_Editor);
        $criteria->compare('TM_QN_QuestionReff',$this->TM_QN_QuestionReff,true);
        $criteria->compare('TM_QN_Totalmarks',$this->TM_QN_Totalmarks);
        $criteria->compare('TM_QN_CreatedOn',$this->TM_QN_CreatedOn,true);
        $criteria->compare('TM_QN_Show_Option',$this->TM_QN_Show_Option);                        
        //$criteria->compare('TM_QN_School_Id',$id);
        $criteria->compare('TM_QN_School_Id',0);        
        /*if($this->TM_QN_CreatedBy!=''):
            if($this->TM_QN_CreatedBy=='0'):
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
                $school="0";
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->addCondition('TM_QN_School_Id IN ('.$school.')');                
            else:
                $school=$id;
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
            endif;
            
        else:
            $school="0,$id";
            //$criteria->compare('TM_QN_School_Id',$id);
            $criteria->addCondition('TM_QN_School_Id IN ('.$school.')');
        endif;
        */
        $criteria->compare('TM_QN_UpdatedBy',$this->TM_QN_UpdatedBy);
        $criteria->addCondition('TM_QN_Status=0');
        $criteria->compare('TM_QN_Teacher_Id',$this->TM_QN_Teacher_Id);
        $criteria->compare('TM_QN_Pattern',$this->TM_QN_Pattern,true);
        //print_r($criteria);exit;
        //$criteria->join = 'LEFT JOIN tm_school_questions AS sq ON sq.TM_SQ_Question_Id = t.TM_QN_Id ';
        //$criteria->condition = 'sq.TM_SQ_School_Id='.$id.' AND  t.TM_QN_Status=0 AND sq.TM_SQ_Type IN(0,1) AND sq.TM_SQ_Standard_Id='.Yii::app()->session['standard'].' AND sq.TM_SQ_Question_Id IS NOT NULL';
        //print_r($criteria);exit;
        $criteria->with = array('schoolquestions'=>array('select' => false,'joinType'=>'LEFT JOIN','condition'=>'TM_SQ_School_Id='.$id.'  AND TM_SQ_ImportStatus=0  AND  TM_SQ_Question_Id IS NOT NULL'));
        $criteria->together=true; 
        return new CActiveDataProvider('Questions', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
	}
    /** code By Amal*/
    public function GetTeacher($userid,$qid)
    {
        $questions=Questions::model()->findbyPk($qid);
        if($questions['TM_QN_School_Id']==0):
            $teacher="System";
        else:
            $teacher=Teachers::model()->findByPk($userid)->TM_TH_Name;
        endif;

        return $teacher;

    }    
    public function teacher($id,$questiontype)
    {
        $criteria=new CDbCriteria;
        $criteria->compare('TM_QN_Id',$this->TM_QN_Id);
        $criteria->compare('TM_QN_Parent_Id',0);
        $plans=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId),TM_SPN_Publisher_Id','condition'=>'TM_SPN_SchoolId='.$id));
        $schoolsyllabus=School::model()->findByPk($id)->TM_SCL_Sylllabus_Id;
        $publishers='';
        $standards='';
        foreach($plans AS $key=>$plan):
            $publishers.=($key=='0'?$plan->TM_SPN_Publisher_Id:','.$plan->TM_SPN_Publisher_Id);
            $standards.=($key=='0'?$plan->TM_SPN_StandardId:','.$plan->TM_SPN_StandardId);
        endforeach;
        $criteria->addCondition('TM_QN_Syllabus_Id='.$schoolsyllabus);
        if($this->TM_QN_Standard_Id==''):
            if($standards!=''):
                $criteria->addCondition('TM_QN_Standard_Id IN ('.$standards.')');
            else:
                $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
            endif;                               
        else:
            $criteria->compare('TM_QN_Standard_Id',$this->TM_QN_Standard_Id);
        endif;
        $criteria->compare('TM_QN_Subject_Id',$this->TM_QN_Subject_Id);
        $criteria->compare('TM_QN_Topic_Id',$this->TM_QN_Topic_Id);
        $criteria->compare('TM_QN_Section_Id',$this->TM_QN_Section_Id);
        $criteria->compare('TM_QN_Dificulty_Id',$this->TM_QN_Dificulty_Id);
        $criteria->compare('TM_QN_Type_Id',$this->TM_QN_Type_Id);
        $criteria->compare('TM_QN_Question',$this->TM_QN_Question,true);
        $criteria->compare('TM_QN_Image',$this->TM_QN_Image,true);
        $criteria->compare('TM_QN_Answer_Type',$this->TM_QN_Answer_Type);
        $criteria->compare('TM_QN_Solutions',$this->TM_QN_Solutions);
        $criteria->compare('TM_QN_Option_Count',$this->TM_QN_Option_Count);
        $criteria->compare('TM_QN_Editor',$this->TM_QN_Editor);
        $criteria->compare('TM_QN_QuestionReff',$this->TM_QN_QuestionReff,true);
        $criteria->compare('TM_QN_Totalmarks',$this->TM_QN_Totalmarks);
        $criteria->compare('TM_QN_CreatedOn',$this->TM_QN_CreatedOn,true);
        $criteria->compare('TM_QN_Show_Option',$this->TM_QN_Show_Option);        
        if($questiontype!=''):
            if($questiontype=='0'):
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
                $school="0";
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->addCondition('TM_QN_School_Id IN ('.$school.')');                
            else:
                $school=$id;
                //$criteria->compare('TM_QN_School_Id',$id);
                $criteria->compare('TM_QN_School_Id',$school);
                //$criteria->compare('TM_QN_CreatedBy',($this->TM_QN_CreatedBy!=''?$this->TM_QN_CreatedBy:Yii::app()->user->id));
            endif;
            
        else:
            $school="0,$id";
            //$criteria->compare('TM_QN_School_Id',$id);
            $criteria->addCondition('TM_QN_School_Id IN ('.$school.')');
        endif;
        
        $criteria->compare('TM_QN_UpdatedBy',$this->TM_QN_UpdatedBy);
        //$criteria->addCondition('TM_QN_Status=0');
        $criteria->compare('TM_QN_Status','0');
        $criteria->compare('TM_QN_Teacher_Id',$this->TM_QN_Teacher_Id);
        $criteria->compare('TM_QN_Pattern',$this->TM_QN_Pattern,true);
        //print_r($criteria);exit;
        //$criteria->join = 'LEFT JOIN tm_school_questions AS sq ON sq.TM_SQ_Question_Id = t.TM_QN_Id ';
        //$criteria->condition = 'sq.TM_SQ_School_Id='.$id.' AND  t.TM_QN_Status=0 AND sq.TM_SQ_Type IN(0,1) AND sq.TM_SQ_Standard_Id='.Yii::app()->session['standard'].' AND sq.TM_SQ_Question_Id IS NOT NULL';
        //print_r($criteria);exit;         
        $criteria->with = array('schoolquestions'=>array('select' => false,'joinType'=>'LEFT JOIN','condition'=>' TM_SQ_School_Id='.$id.' AND   TM_QN_Status=0 AND  TM_SQ_Type IN(0,1) AND  TM_SQ_Standard_Id IN ('.$standards.') AND TM_SQ_ImportStatus=0 AND  TM_SQ_Question_Id IS NOT NULL'));
        $criteria->together=true; 
        return new CActiveDataProvider('Questions', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
    } 
    public function CheckUser($userid,$qid)
    {
        $questions=Questions::model()->findbyPk($qid);
        $user=User::model()->findbyPk($userid)->profile;
        if($questions['TM_QN_School_Id']==0):
            $createdby="System";
        else:
            $createdby=$user->firstname.' '.$user->lastname;
        endif;
        return $createdby;
    }    
    /** code By Amal*/
    public function GetCretedate($newdate)
    {
        if($newdate=='0000-00-00'):
            return '';
        else:
            return Yii::app()->dateFormatter->format("d MMM y",strtotime($newdate));    
        endif;               
        //return $date;
        /*
        $today=date('Y-m-d');
        if($date='0000-00-00'):
            return '';
        elseif($newdate==$today):
            return 'Today';
        else:        
            return Yii::app()->dateFormatter->format("d MMM y",strtotime($date));
        endif;*/
    }       
}
