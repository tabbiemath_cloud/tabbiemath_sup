<?php

/**
 * This is the model class for table "{{schoolhomework}}".
 *
 * The followings are the available columns in table '{{schoolhomework}}':
 * @property integer $TM_SCH_Id
 * @property string $TM_SCH_Name
 * @property string $TM_SCH_Description
 * @property integer $TM_SCH_School_Id
 * @property integer $TM_SCH_Publisher_Id
 * @property integer $TM_SCH_Syllabus_Id
 * @property integer $TM_SCH_Standard_Id
 * @property integer $TM_SCH_Type
 * @property integer $TM_SCH_Status
 * @property string $TM_SCH_CreatedOn
 * @property integer $TM_SCH_CreatedBy
 * @property string $TM_SCH_DueDate
 * @property string $TM_SCH_Comments
 * @property string $TM_SCH_PublishDate
 * @property string $TM_SCH_PublishType
 * @property string $TM_SCH_ShowSolution
 * @property string $TM_SCH_CompletedOn
 * @property string $TM_SCH_Worksheet
 * @property string $TM_SCH_Solution
 */
class Schoolhomework extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{schoolhomework}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_SCH_Name, TM_SCH_Description, TM_SCH_School_Id,TM_SCH_Standard_Id, TM_SCH_Type, TM_SCH_Status,TM_SCH_CreatedBy,TM_SCH_PublishType', 'required'),
			array('TM_SCH_School_Id, TM_SCH_Publisher_Id, TM_SCH_Syllabus_Id, TM_SCH_Standard_Id, TM_SCH_Type, TM_SCH_Status, TM_SCH_CreatedBy', 'numerical', 'integerOnly'=>true),
            array('TM_SCH_Worksheet,TM_SCH_Solution','safe'),
			array('TM_SCH_Name', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_SCH_Id, TM_SCH_Name, TM_SCH_Description, TM_SCH_School_Id, TM_SCH_Publisher_Id, TM_SCH_Syllabus_Id, TM_SCH_Standard_Id, TM_SCH_Type, TM_SCH_Status, TM_SCH_CreatedOn, TM_SCH_CreatedBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
        'standard'=>array(self::BELONGS_TO, 'Standard', 'TM_SCH_Standard_Id'),
        'homework'=>array(self::HAS_MANY, 'StudentHomeworks', 'TM_STHW_HomeWork_Id'), 
        'marker'=>array(self::HAS_MANY, 'Homeworkmarking', 'TM_HWM_Homework_Id'),       
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_SCH_Id' => 'Tm Sch',
			'TM_SCH_Name' => 'Name',
			'TM_SCH_Description' => 'Description',
			'TM_SCH_School_Id' => 'School',
			'TM_SCH_Publisher_Id' => 'Publisher',
			'TM_SCH_Syllabus_Id' => 'Syllabus',
			'TM_SCH_Standard_Id' => 'Standard',
			'TM_SCH_Type' => 'Type',
			'TM_SCH_Status' => 'Status',
			'TM_SCH_CreatedOn' => 'Created On',
			'TM_SCH_CreatedBy' => 'Created By',
            'TM_SCH_DueDate'=>'Due Date',
            'TM_SCH_Comments'=>'Comments',
            'TM_SCH_PublishDate'=>'Publish Date',
            'TM_SCH_PublishType'=>'Type',
            'TM_SCH_CompletedOn'=>'Completed On',
			'TM_SCH_Master_Stand_Id'=>'Class'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id,$standard,$type,$status,$order=false,$pagination=false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SCH_Id',$this->TM_SCH_Id);
		$criteria->compare('TM_SCH_Name',$this->TM_SCH_Name,true);
		$criteria->compare('TM_SCH_Description',$this->TM_SCH_Description,true);
		$criteria->compare('TM_SCH_School_Id',$this->TM_SCH_School_Id);
		$criteria->compare('TM_SCH_Publisher_Id',$this->TM_SCH_Publisher_Id);
		$criteria->compare('TM_SCH_Syllabus_Id',$this->TM_SCH_Syllabus_Id);
		$criteria->compare('TM_SCH_Standard_Id',$standard);
        if($type!='All'):
		  $criteria->compare('TM_SCH_Type',$type);
        endif;
		$criteria->compare('TM_SCH_Status',$status);
		$criteria->compare('TM_SCH_CreatedOn',$this->TM_SCH_CreatedOn,true);
		$criteria->compare('TM_SCH_CreatedBy',$id);
        if($order):
            $criteria->order='TM_SCH_CompletedOn DESC,TM_SCH_Id DESC';
        else:
            $criteria->order='TM_SCH_CompletedOn DESC,TM_SCH_Id DESC';
        endif;
        if($pagination):
            return new CActiveDataProvider($this, array(
    			'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>100,
                ),                
    		));        
        else:
            return new CActiveDataProvider($this, array(
    			'criteria'=>$criteria,
    		));
        endif;
		

	}
	public function searchmarker($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_SCH_Id',$this->TM_SCH_Id);
		$criteria->compare('TM_SCH_Name',$this->TM_SCH_Name,true);
		$criteria->compare('TM_SCH_Description',$this->TM_SCH_Description,true);
		$criteria->compare('TM_SCH_School_Id',$this->TM_SCH_School_Id);
		$criteria->compare('TM_SCH_Publisher_Id',$this->TM_SCH_Publisher_Id);
		$criteria->compare('TM_SCH_Syllabus_Id',$this->TM_SCH_Syllabus_Id);
		$criteria->compare('TM_SCH_Standard_Id',$standard);
        if($type!='All'):
		  $criteria->compare('TM_SCH_Type',$type);
        endif;
		$criteria->compare('TM_SCH_Status',$status);
		$criteria->compare('TM_SCH_CreatedOn',$this->TM_SCH_CreatedOn,true);
		$criteria->compare('TM_SCH_CreatedBy',$id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function checkQuestions($id)
    {

        $hwqstns=CustomtemplateQuestions::model()->findAll(array('condition' => "TM_CTQ_Custom_Id='".$id."'"));

        if(count($hwqstns)>0):
             return "visibility:visible";
        else:
            return "visibility:hidden";
        endif;

    }

    public function checkQuestionsCount($id)
    {

        $hwqstns=CustomtemplateQuestions::model()->count(array('condition' => "TM_CTQ_Custom_Id='".$id."'"));
        return $hwqstns;

    }    
    public static function itemAlias($type, $code = NULL)
    {
        $_items = array(

            'HomeworkStatus' => array(
                '0' => ('Active'),
                '1' => ('Inactive'),
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    public function GetMangeLink($id)
    {
        $html = "<a href='" . Yii::app()->createUrl('teachers/managecustomquestions', array('id' => $id)) . "' class='btn btn-warning btn-xs' >Manage Questions</a>";
        return $html;
    }    
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Schoolhomework the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function GetData($data,$headder)
    {
        if($headder=='Homework Type'):
            if($data=='1'):
                $value='Custom';            
            elseif($data=='3'):
                $value='Worksheet';
			elseif($data=='33'):
				$value='Resource';
			else:
				$value='Quick';
            endif;        
        
            return '<span class="rwd-tables thead"><b>'.$headder.'</b></span><span class="rwd-tables tbody">'.$value.'</span>';
        elseif($headder=='Submission Type'):
            if($data=='2'):
                $value='Online';
            elseif($data=='3'):
                $value='Worksheet';
            elseif($data=='1'):
                $value='Worksheet with options';
            endif;
            return '<span class="rwd-tables thead"><b>'.$headder.'</b></span><span class="rwd-tables tbody">'.$value.'</span>';
        else:
            return '<span class="rwd-tables thead"><b>'.$headder.'</b></span><span class="rwd-tables tbody">'.$data.'</span>';
        endif; 
    }
	public function GetPublish($data,$type)
	{
		$headder="Submission Type";
		if($type==33):
			$value="Resource";
			return '<span class="rwd-tables thead"><b>'.$headder.'</b></span><span class="rwd-tables tbody">'.$value.'</span>';
		else:
			if($data=='2'):
				$value='Online';
			elseif($data=='3'):
				$value='Worksheet';
			elseif($data=='1'):
				$value='Worksheet with options';
			endif;
			return '<span class="rwd-tables thead"><b>'.$headder.'</b></span><span class="rwd-tables tbody">'.$value.'</span>';
		endif;
	}
    public function GetAssignees($homework)
    {
        
    }
    public function GetMarkingStatus($homework)
    {
        $count=Homeworkmarking::model()->count(array('condition'=>'TM_HWM_Homework_Id='.$homework));
        if($count==0):
            return false;
        else:
            return true;
        endif;

    }
    public function checkWorksheet($id)
    {
                              
        if($id==''):
            return true;
        else:
            return false;
        endif;
    }  
    public function checkWorksheetStatus($id)
    {
                              
        if($id!=''):
            return true;
        else:
            return false;
        endif;
    }

	public function GetPrintLink($id)
	{
		$mock=Schoolhomework::model()->findByPk($id);
		if($mock['TM_SCH_Type']==33):
			$html=$mock['TM_SCH_Video_Url'];
		else:
			$html = Yii::app()->createUrl('teachers/printworksheet', array('id' => $id));
		endif;
		return $html;
	}
	public function GetMasterStd($id)
	{
		$standard=Standard::model()->findByPk($id)->TM_SD_Name;
		return $standard;
	}
}
