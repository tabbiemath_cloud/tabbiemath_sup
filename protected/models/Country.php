<?php

/**
 * This is the model class for table "{{country}}".
 *
 * The followings are the available columns in table '{{country}}':
 * @property integer $TM_CON_Id
 * @property string $TM_CON_Name
 * @property string $TM_CON_Code
 * @property integer $TM_CON_Status
 * @property integer $TM_CON_CreatedBy
 * @property string $TM_CON_CreatedOn
 */
class Country extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{country}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CON_Name, TM_CON_Code, TM_CON_CreatedBy, TM_CON_CreatedOn', 'required'),
			array('TM_CON_Status, TM_CON_CreatedBy', 'numerical', 'integerOnly'=>true),
			array('TM_CON_Name', 'length', 'max'=>255),
			array('TM_CON_Code', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CON_Id, TM_CON_Name, TM_CON_Code, TM_CON_Status, TM_CON_CreatedBy, TM_CON_CreatedOn', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CON_Id' => 'Tm Con',
			'TM_CON_Name' => 'Tm Con Name',
			'TM_CON_Code' => 'Tm Con Code',
			'TM_CON_Status' => 'Tm Con Status',
			'TM_CON_CreatedBy' => 'Tm Con Created By',
			'TM_CON_CreatedOn' => 'Tm Con Created On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CON_Id',$this->TM_CON_Id);
		$criteria->compare('TM_CON_Name',$this->TM_CON_Name,true);
		$criteria->compare('TM_CON_Code',$this->TM_CON_Code,true);
		$criteria->compare('TM_CON_Status',$this->TM_CON_Status);
		$criteria->compare('TM_CON_CreatedBy',$this->TM_CON_CreatedBy);
		$criteria->compare('TM_CON_CreatedOn',$this->TM_CON_CreatedOn,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
