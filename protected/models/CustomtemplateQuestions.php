<?php

/**
 * This is the model class for table "{{customtemplate_questions}}".
 *
 * The followings are the available columns in table '{{customtemplate_questions}}':
 * @property integer $TM_CTQ_Id
 * @property integer $TM_CTQ_Custom_Id
 * @property integer $TM_CTQ_Question_Id
 * @property integer $TM_CTQ_Type
 * @property integer $TM_CTQ_Order
 */
class CustomtemplateQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{customtemplate_questions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_CTQ_Id, TM_CTQ_Custom_Id, TM_CTQ_Question_Id, TM_CTQ_Type, TM_CTQ_Order', 'required'),
			array('TM_CTQ_Id, TM_CTQ_Custom_Id, TM_CTQ_Question_Id, TM_CTQ_Type, TM_CTQ_Order', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_CTQ_Id, TM_CTQ_Custom_Id, TM_CTQ_Question_Id, TM_CTQ_Type, TM_CTQ_Order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_CTQ_Id' => 'Tm Ctq',
			'TM_CTQ_Custom_Id' => 'Tm Ctq Custom',
			'TM_CTQ_Question_Id' => 'Tm Ctq Question',
			'TM_CTQ_Type' => 'Tm Ctq Type',
			'TM_CTQ_Order' => 'Tm Ctq Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_CTQ_Id',$this->TM_CTQ_Id);
		$criteria->compare('TM_CTQ_Custom_Id',$this->TM_CTQ_Custom_Id);
		$criteria->compare('TM_CTQ_Question_Id',$this->TM_CTQ_Question_Id);
		$criteria->compare('TM_CTQ_Type',$this->TM_CTQ_Type);
		$criteria->compare('TM_CTQ_Order',$this->TM_CTQ_Order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomtemplateQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
