<?php

/**
 * This is the model class for table "{{questionstatusmaster}}".
 *
 * The followings are the available columns in table '{{questionstatusmaster}}':
 * @property integer $TM_QSM_Id
 * @property string $TM_QSM_Name
 */
class Questionstatusmaster extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{questionstatusmaster}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_QSM_Id,TM_QSM_Name', 'required'),
			array('TM_QSM_Id', 'numerical', 'integerOnly'=>true),
			array('TM_QSM_Name', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_QSM_Id, TM_QSM_Name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_QSM_Id' => 'Id',
			'TM_QSM_Name' => 'Question Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_QSM_Id',$this->TM_QSM_Id);
		$criteria->compare('TM_QSM_Name',$this->TM_QSM_Name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function GetLastid()
    {
        $number=Questionstatusmaster::model()->find(array('order'=>'TM_QSM_Id DESC','limit'=>1));
        return $number->TM_QSM_Id+1; 
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Questionstatusmaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
