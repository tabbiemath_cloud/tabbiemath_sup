<?php

/**
 * This is the model class for table "{{studentmockquestions}}".
 *
 * The followings are the available columns in table '{{studentmockquestions}}':
 * @property integer $TM_STMKQT_Id
 * @property integer $TM_STMKQT_Student_Id
 * @property integer $TM_STMKQT_Mock_Id
 * @property integer $TM_STMKQT_Parent_Id
 * @property integer $TM_STMKQT_Question_Id
 * @property integer $TM_STMKQT_Type
 * @property integer $TM_STMKQT_Answer
 * @property double  $TM_STMKQT_Marks
 * @property integer $TM_STMKQT_Status
 * @property integer $TM_STMKQT_Flag
 * @property integer $TM_STMKQT_Order
 * @property integer $TM_STMKQT_Redo_Flag
 * @property integer $TM_STMKQT_RedoStatus
 */
class Studentmockquestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{studentmockquestions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TM_STMKQT_Student_Id, TM_STMKQT_Mock_Id, TM_STMKQT_Question_Id, TM_STMKQT_Marks', 'required'),
			array('TM_STMKQT_Student_Id, TM_STMKQT_Mock_Id, TM_STMKQT_Question_Id', 'numerical', 'integerOnly'=>true),
			array('TM_STMKQT_Marks', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TM_STMKQT_Id, TM_STMKQT_Student_Id, TM_STMKQT_Mock_Id, TM_STMKQT_Question_Id, TM_STMKQT_Marks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TM_STMKQT_Id' => 'Tm Stmkqt',
			'TM_STMKQT_Student_Id' => 'Tm Stmkqt Student',
			'TM_STMKQT_Mock_Id' => 'Tm Stmkqt Mock',
			'TM_STMKQT_Question_Id' => 'Tm Stmkqt Question',
			'TM_STMKQT_Marks' => 'Tm Stmkqt Marks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TM_STMKQT_Id',$this->TM_STMKQT_Id);
		$criteria->compare('TM_STMKQT_Student_Id',$this->TM_STMKQT_Student_Id);
		$criteria->compare('TM_STMKQT_Mock_Id',$this->TM_STMKQT_Mock_Id);
		$criteria->compare('TM_STMKQT_Question_Id',$this->TM_STMKQT_Question_Id);
		$criteria->compare('TM_STMKQT_Marks',$this->TM_STMKQT_Marks);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Studentmockquestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
