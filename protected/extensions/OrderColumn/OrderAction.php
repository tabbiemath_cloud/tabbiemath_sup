    <?php

/**
 * Description of OrderAction
 *
 * @author Nr Aziz
 */
class OrderAction extends CAction {

    public $modelClass;
    public $pkName;

    public function run($pk, $name, $value, $move) {
		$standardid=CActiveRecord::model($this->modelClass)->findByPk($pk)->TM_TP_Standard_Id;
        $criteria = new CDbCriteria;
        $criteria->condition='TM_TP_order=:order AND TM_TP_Standard_Id=:standard';
        $criteria->params=array(':order'=>$value,':standard'=>$standardid);
        $model = CActiveRecord::model($this->modelClass)->find($criteria);
        $criteria = new CDbCriteria;
        $criteria->condition='TM_TP_order<:order AND TM_TP_Standard_Id=:standard';
        $criteria->order='TM_TP_order DESC';
        $criteria->params=array(':order'=>$value,':standard'=>$standardid );
        $modelup = CActiveRecord::model($this->modelClass)->find($criteria);
        $criteria = new CDbCriteria;
        $criteria->condition='TM_TP_order>:order AND TM_TP_Standard_Id=:standard';
        $criteria->order='TM_TP_order ASC';
        $criteria->params=array(':order'=>$value,':standard'=>$standardid  );
        $modelbot = CActiveRecord::model($this->modelClass)->find($criteria);



        $table = $model->tableName();

        if ($move === 'up') {
            $op = '<=';
            $inOrder = 'DESC';

        } else if ($move === 'down') {
            $op = '>=';
            $inOrder = 'ASC';

        }

        $sql = "SELECT {$table}.{$name} FROM $table WHERE $this->pkName $op $pk ORDER BY $this->pkName $inOrder LIMIT 1";
        $order = Yii::app()->db->createCommand($sql)->queryScalar();


        $highestOrder = Yii::app()
                ->db
                ->createCommand("SELECT {$table}.{$name} FROM {$table} ORDER BY {$table}.{$name} DESC LIMIT 1")
                ->queryScalar();


        if ($move === 'up' && $model->{$name} != 0)
        {
            $order=$modelup->{$name};
            $order2=$model->{$name};
            $model->{$name} = $order;
            $modelup->{$name}=$order2;
            $model->save(false);
            $modelup->save(false);
        }
		
        else if ($move === 'down' && $order != $highestOrder+1)
        {

            $order=$modelbot->{$name};
            $order2=$model->{$name};
            $model->{$name} = $order;
            $modelbot->{$name}=$order2;
            $model->save(false);
            $modelbot->save(false);

        }
		








    }

}

?>
