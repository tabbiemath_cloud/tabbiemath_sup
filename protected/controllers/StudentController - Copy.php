<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/7/2015
 * Time: 12:37 PM
 */

class StudentController extends Controller {

    public $layout='//layouts/main';

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('home','Revision','RevisionTest','getTopics','TestComplete','deletetest','starttest','GetSolution','Challenge','showpaper','Getpdf','SendMail','Startchallenge','Mock','Deletemock','startmock','Mocktest','MockComplete','GetmockSolution','History','GetpdfAnswer','Invitebuddies'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudent()'
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('ChallengeTest','challengeresult','deletechallenge','Cancelchallenge','Acceptchallenge'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudentChalange($_GET["id"])'
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + deletetest,deletechallenge,Cancelchallenge,Deletemock', // mewe only allow deletion via POST request
            //'postOnly + copy', // we only allow copy via POST request
        );
    }

    public function actionHome()
    {
        $studenttests=StudentTest::model()->findAll(array('condition'=>"TM_STU_TT_Student_Id='".Yii::app()->user->id."' AND TM_STU_TT_Status!='1'"));
        $studentshallenges=Chalangerecepient::model()->with('challenge')->findAll(array('condition'=>"TM_CE_RE_Recepient_Id='".Yii::app()->user->id."' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0'"));
        $this->render('userhome',array('studenttests'=>$studenttests,'challenges'=>$studentshallenges));

    }
    public function actionRevision()
    {
        $plan=$this->GetStudentDet();
        $chapters=Chapter::model()->findAll(array('condition'=>"TM_TP_Syllabus_Id='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'"));
        if(isset($_POST['startTest'])):

            if($_POST['testtype']=='Difficulty'):
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['chapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Mode='0';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['chaptertotal'.$chapter->TM_TP_Id]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $topics='';
                        for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions=$totalquestions+$_POST['basic'.$chapter->TM_TP_Id]+$_POST['inter'.$chapter->TM_TP_Id]+$_POST['adv'.$chapter->TM_TP_Id];
                        if($_POST['basic'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$_POST['basic'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['inter'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$_POST['inter'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['adv'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='3';
                            $limit=$_POST['adv'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $selectedquestions=base64_encode('Remove_'.base64_encode($totalquestions));
                    $addedquestions=base64_encode('Remove_'.base64_encode($count-1));
                    $this->redirect(array('starttest','id'=>$StudentTest->TM_STU_TT_Id,'inque'=>$selectedquestions,'outque'=>$addedquestions));
                endif;
            else:
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['quickchapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Mode='1';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['quickchaptertotal'.$chapter->TM_TP_Id]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $quicklimits=$this->GetQuickLimits($_POST['quickchaptertotal'.$chapter->TM_TP_Id]);
                        $topics='';
                        for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions=$totalquestions+$_POST['quick'.$chapter->TM_TP_Id];
                        if($quicklimits['basic']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$quicklimits['basic'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if($quicklimits['intermediate']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['intermediate'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if($quicklimits['advanced']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['advanced'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $selectedquestions=base64_encode('Remove_'.base64_encode($totalquestions));
                    $addedquestions=base64_encode('Remove_'.base64_encode($count-1));
                    $this->redirect(array('starttest','id'=>$StudentTest->TM_STU_TT_Id,'inque'=>$selectedquestions,'outque'=>$addedquestions));
                endif;
            endif;
            //$this->redirect(array('RevisionDifficulty','id'=>$StudentTest->TM_STU_TT_Id));
        elseif(isset($_POST['addTest'])):
            if($_POST['testtype']=='Difficulty'):
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['chapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Mode='0';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['chaptertotal'.$chapter->TM_TP_Id]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $topics='';
                        for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions=$totalquestions+$_POST['basic'.$chapter->TM_TP_Id]+$_POST['inter'.$chapter->TM_TP_Id]+$_POST['adv'.$chapter->TM_TP_Id];
                        if($_POST['basic'.$key]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$_POST['basic'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['inter'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$_POST['inter'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['adv'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='3';
                            $limit=$_POST['adv'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $this->redirect(array('home'));
                endif;
            else:
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['quickchapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Mode='1';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['quickchaptertotal'.$key]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $quicklimits=$this->GetQuickLimits($_POST['quickchaptertotal'.$key]);
                        $topics='';
                        for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions=$totalquestions+$_POST['quick'.$chapter->TM_TP_Id];;
                        if($quicklimits['basic']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$quicklimits['basic'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,1,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($quicklimits['intermediate']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['intermediate'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,1,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($quicklimits['advanced']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['advanced'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,1,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $this->redirect(array('home'));
                endif;
            endif;
        endif;

        $this->render('revision',array('plan'=>$plan,'chapters'=>$chapters));
    }
    public function actionStarttest($id)
    {
        $test=StudentTest::model()->findByPk($id);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id='.$id.' AND TM_STU_QN_Type NOT IN (6,7)';
        $totalonline=count(StudentQuestions::model()->findAll($criteria));
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id='.$id.' AND TM_STU_QN_Type IN (6,7)';
        $totalpaper=count(StudentQuestions::model()->findAll($criteria));
        $totalquestions=$totalonline+$totalpaper;
        //$selectedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['inque'])));
        //$addedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['outque'])));
        $this->render('starttest',array('id'=>$id,'test'=>$test,'totalonline'=>$totalonline,'totalpaper'=>$totalpaper,'totalquestions'=>$totalquestions));

    }
    public function actionStartchallenge($id)
    {
        $test=Challenges::model()->findByPk($id);
        $this->render('startchallenge',array('id'=>$id,'test'=>$test));

    }
    public function actionRevisionTest($id)
    {
        if(isset($_POST['action']['GoNext']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = StudentQuestions::model()->findByAttributes(
                        array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new StudentQuestions();
                    endif;
                    $selectedquestion->TM_STU_QN_Test_Id=$id;
                    $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->TM_STU_QN_Flag='1';
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endif;
            $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                )
            );
        }
        if(isset($_POST['action']['GetPrevios'])):
            if($_POST['QuestionType']!='5'):
                if(isset($_POST['answer'])):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    if($_POST['QuestionType']=='1'):
                        $answer=implode(',',$_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                        $answer=$_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                else:
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    if(isset($_POST['answer'.$child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                        );
                        if(count($childquestion)=='1'):
                            $selectedquestion=$childquestion;
                        else:
                            $selectedquestion=new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id=$id;
                        $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                        if($child->TM_QN_Type_Id=='1'):
                            $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                            $answer=$_POST['answer'.$child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='4'):
                            $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endif;
            $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0,1,2) AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Number<:tableid AND TM_STU_QN_Parent_Id=0',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number DESC, TM_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                )
            );
        endif;
        if(isset($_POST['action']['DoLater'])):
            if($_POST['QuestionType']!='5'):
                if(isset($_POST['answer'])):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    if($_POST['QuestionType']=='1'):
                        $answer=implode(',',$_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                        $answer=$_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                else:
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    if(isset($_POST['answer'.$child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                        );
                        if(count($childquestion)=='1'):
                            $selectedquestion=$childquestion;
                        else:
                            $selectedquestion=new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id=$id;
                        $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                        if($child->TM_QN_Type_Id=='1'):
                            $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                            $answer=$_POST['answer'.$child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='4'):
                            $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='2';
                $selectedquestion->save(false);
            endif;
            $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                )
            );
        endif;

        if(isset($_POST['action']['Complete']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = StudentQuestions::model()->findByAttributes(
                        array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new StudentQuestions();
                    endif;
                    $selectedquestion->TM_STU_QN_Test_Id=$id;
                    $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endif;
            if($this->GetPaperCount($id)!=0):
                $test=StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status='2';
                $test->save(false);
                $this->redirect(array('showpaper','id'=>$id));
            else:
                $test=StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status='1';
                $test->save(false);
                $this->redirect(array('TestComplete','id'=>$id));
            endif;
            //$this->redirect(array('TestComplete','id'=>$id));
        }
        if(!isset($_POST['action'])):
            if(isset($_GET['questionnum'])):
                $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                    array(
                        'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0,1,2) AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number='.str_replace("question_","",base64_decode($_GET['questionnum'])),
                        'limit'=>1,
                        'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            else:
                $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                    array(
                        'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0) AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Parent_Id=0',
                        'limit'=>1,
                        'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            endif;
        endif;
        if(count($questionid)):
            $question=Questions::model()->findByPk($questionid->TM_STU_QN_Question_Id);
            $this->renderPartial('showquestion',array('testid'=>$id,'question'=>$question,'questionid'=>$questionid));
        else:
            $this->redirect(array('revision'));
        endif;
    }

    public function actionTestComplete($id)
    {
        $results = StudentQuestions::model()->findAll(
            array(
                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7)',
                "order" => 'TM_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $totalmarks = 0;
        $earnedmarks = 0;
        $result = '';
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == 0):
                if ($question->TM_QN_Type_Id != '5'):
                    $displaymark = 0;
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marks AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;

                    endforeach;
                    $marks = 0;
                    if ($question->TM_QN_Type_Id != '4' & $exam->TM_STU_QN_Flag != '2'):
                        if ($exam->TM_STU_QN_Answer_Id != ''):

                            $studentanswer = explode(',', $exam->TM_STU_QN_Answer_Id);
                            foreach ($question->answers AS $ans):
                                if ($ans->TM_AR_Correct == '1'):
                                    if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    }
                                endif;
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                for ($i = 0; $i < count($studentanswer); $i++)
                                {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);
                                    $correctanswer .= '<li class="list-group-item" style="display:flex ;"><div class="col-md' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($exam->TM_STU_QN_Answer != ''):
                            $correctanswer = '<ul class="list-group"><li class="list-group-item">' . $exam->TM_STU_QN_Answer . '</li></ul>';
                            foreach ($question->answers AS $ans):
                                $answeroption = explode(';', strip_tags($ans->TM_AR_Answer));
                                if (array_search($exam->TM_STU_QN_Answer, $answeroption) !== false) {
                                    $marks = $marks + $ans->TM_AR_Marks;

                                }
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item">Question Skipped</li></ul>';
                        endif;

                    endif;

                        $result.= '<tr><td><div class="col-md-12"><i class="fa ' . ($marks == '0' ? 'fa fa-times clrr' : 'fa-check') . '"></i>  Question.' . $exam->TM_STU_QN_Number . '  <span class="glyphicon glyphicon-star-empty">      Reference:' . $question->TM_QN_QuestionReff . '</span><span class="pull-right">Mark:' . $displaymark . '</span></div>';
                        $result.= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        if ($question->TM_QN_Image != ''):
                            $result.= '<div class="col-md-4"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>';
                        endif;
                        $result.= ' <div class="col-md-12">' . $correctanswer . '</div>';
                        $result.= '<div class="showSolutionItemtest col-md-12 clickable-row"  data-reff="' . $question->TM_QN_QuestionReff . '" ><a onclick="sendmail" href="#"  style="font-size:11px">Click here to report mistake with this question</a></div>';
                        $result.= '<div class="sendmail"  style="display:none;">
                                    <div class="col-md-10" id="maildivslidetest"  >
                                        <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                    </div>
                                    <div class="col-md-2" style="margi-top:15px;"><input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" ></div></div></td></tr>';
                        //glyphicon glyphicon-star sty
                    $exam->TM_STU_QN_Mark = $marks;
                    $exam->save(false);
                    $earnedmarks = $earnedmarks + $marks;

                else:
                    $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                    $displaymark = 0;
                    foreach ($marksQuestions AS $key => $formarks):

                        $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($gettingmarks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;



                    endforeach;
                    $result .= '<tr><td>
                            <div class="col-md-12"><i class="fa ' . ($marks == '0' ? 'fa fa-times clrr' : 'fa-check') . '"></i>  Question.' . $exam->TM_STU_QN_Number . '  <span class="glyphicon glyphicon-star-empty">      Reference:' . $question->TM_QN_QuestionReff . '</span><span class="pull-right">Mark:' . $displaymark . '</span></div>
                            <div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                    if ($question->TM_QN_Image != ''):
                        $result .= '<div class="col-md-4"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>';

                    endif;

                    $result .='</td></tr>';
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    foreach ($questions AS $childquestion):
                        $childresults = StudentQuestions::model()->find(
                            array(
                                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Question_Id=:questionid',
                                'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                            )
                        );
                        $marks = 0;
                        if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_STU_QN_Flag != '2'):
                            if ($childresults->TM_STU_QN_Answer_Id != ''):

                                $studentanswer = explode(',', $childresults->TM_STU_QN_Answer_Id);
                                foreach ($childquestion->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++)
                                    {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item" style="display:flex ;"><div class="col-md' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';

                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($childresults->TM_STU_QN_Answer != ''):
                                $correctanswer = '<ul class="list-group"><li class="list-group-item">' . $childresults->TM_STU_QN_Answer . '</li></ul>';
                                foreach ($childquestion->answers AS $ans):
                                    $answeroption = explode(';', strip_tags($ans->TM_AR_Answer));
                                    if (array_search($childresults->TM_STU_QN_Answer, $answeroption) !== false) {
                                        $marks = $marks + $ans->TM_AR_Marks;

                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($childquestion->TM_QN_Parent_Id != 0):
                            $result .= '<tr><td><div class="' . ($childquestion->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">Part' . $count . ': ' . $childquestion->TM_QN_Question . '</div>';
                            if ($question->TM_QN_Image != ''):
                                $result .= '<div class="col-md-4"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>';
                            endif;
                            $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $result .= '<div class="showSolutionItemtest col-md-12 clickable-row"  data-reff="' . $childquestion->TM_QN_QuestionReff . '" ><a onclick="sendmailtest" href="#"  style="font-size:11px">Click here to report mistake with this question</a></div>
                                    <div class="sendmailtest" style="display:none;">
                                    <div class="col-md-10" id="maildivslidetest"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="commentstest" ></textarea></div>
                                    <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $childquestion->TM_QN_Id . '" ></div>
                                    </div></td></tr>';
                            //glyphicon glyphicon-star sty
                        endif;
                        $exam->TM_STU_QN_Mark = $marks;
                        $exam->save(false);
                        $earnedmarks = $earnedmarks + $marks;
                        $count++;
                    endforeach;
                endif;


            endif;

        endforeach;
        $percentage = ($earnedmarks / $totalmarks) * 100;
        $test = StudentTest::model()->findByPk($id);
        $test->TM_STU_TT_Mark = $earnedmarks;
        $test->TM_STU_TT_TotalMarks = $totalmarks;
        $test->TM_STU_TT_Percentage = round($percentage, 1);
        $test->save(false);
        $this->render('showresult', array('test' => $test, 'testresult' => $result));
    }
    public function actionGetpdf($id)
    {
        $paperquestions=$this->GetPaperQuestions($id);
        $html="<table cellpadding='5' border='1' width='100%'>";
        foreach($paperquestions AS $key=>$paperquestion):

            $count=$key+1;
            $question=Questions::model()->findByPk($paperquestion->TM_STU_QN_Question_Id);
            if($question->TM_QN_Type_Id!='7'):
                $html.="<tr><td width='5%'>";
                $html.=$count;
                $html.="</td><td width='90%'>";
                $html.=$question->TM_QN_Question;
                $html.="</td><td  width='5%'>".$paperquestion->TM_STU_QN_Number."</td></tr>";
                if($question->TM_QN_Image!=''):
                    $html.="<tr><td colspan='3'><img src=". Yii::app()->request->baseUrl."/site/Imagerender/id/".$question->TM_QN_Image." alt="." height="."42"." width="."42"."></td></tr>";
                endif;

            endif;
            if($question->TM_QN_Type_Id=='7'):
                $html.="<tr><td width='5%'>";
                $html.=$count;
                $html.="</td><td width='95%'>";
                $html.=$question->TM_QN_Question;
                $html.="</td></tr>";
                if($question->TM_QN_Image!=''):
                    $html.="<tr><td colspan='2'><img src=". Yii::app()->request->baseUrl."/site/Imagerender/id/".$question->TM_QN_Image." alt="." height="."42"." width="."42"."></td></tr>";
                endif;
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                foreach ($questions AS $childquestion):
                    $html.="<tr><td width='5%'>";
                    $html.="</td><td width='95%'>";
                    $html.=$childquestion->TM_QN_Question;
                    $html.="</td></tr>";
                    if($childquestion->TM_QN_Image!=''):
                        $html.="<tr><td colspan='2'><img src=". Yii::app()->request->baseUrl."/site/Imagerender/id/".$childquestion->TM_QN_Image." alt="." height="."42"." width="."42"."></td></tr>";
                    endif;
                endforeach;
            endif;

        endforeach;
        $html.="</table>";
        $mpdf=new mPDF();

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf','D');

    }
    public function actionGetSolution()
    {
        if(isset($_POST['testid'])):
            $results=StudentQuestions::model()->findAll(
                array(
                    'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test',
                    'params'=>array(':student'=>Yii::app()->user->id,':test'=>$_POST['testid'])
                )
            );
            $resulthtml='';
            foreach($results AS $result):
                $question=Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
                $resulthtml.='<tr><td>

                            <div class="col-md-12">'.$question->TM_QN_Question.'</div>';
                            if($question->TM_QN_Image!=''):
                                $resulthtml.='<div class="col-md-12">
                                                    <img class="img-responsive img_right" src="'.Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=thumbs&type=question&width=200&height=200" alt="">
                                                </div>';
                            endif;
                            $resulthtml.='<div class="col-md-12">'.$question->TM_QN_Solutions.'</div>';
                            if($question->TM_QN_Solution_Image!=''):
                                $resulthtml.='<div class="col-md-12">
                                <img class="img-responsive img_right" src="'.Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Solution_Image.'?size=thumbs&type=question&width=200&height=200" alt="">
                                </div>';
                            endif;
                            $resulthtml.='<div class="showSolutionItem col-md-12 clickable-row"  data-reff="'.$question->TM_QN_QuestionReff.'" ><a href="javascript:void(0);">Do you think there is a mistake?</a></div>
                            <div class="col-md-12 sendmail" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><input type="text" class="form-control"name="comments"  id="comments" hidden/></div>
                           <div class="col-md-2"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-id="'.$question->TM_QN_Id.'"hidden ></div>
                           </div>

                        </td></tr>';
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function actionGetmockSolution()
    {
        if(isset($_POST['testid'])):
            $results=MockQuestions::model()->findAll(
                array(
                    'condition'=>'TM_MQ_Mock_Id=:test',
                    'params'=>array(':test'=>$_POST['testid'])
                )
            );
            $resulthtml='';
            foreach($results AS $result):
                $question=Questions::model()->findByPk($result->TM_MQ_Question_Id);
                $resulthtml.='<tr><td class="showSolutionItem clickable-row"  data-reff="'.$question->TM_QN_QuestionReff.'" >
                            <div class="col-md-12 topbottompadding">'.$question->TM_QN_Question.'</div>
                            <div class="col-md-12 topbottompadding">'.$question->TM_QN_Solutions.'</div>

                           <div class="col-md-12 topbottompadding sendmail" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><input type="text" class="form-control"name="comments" placeholder="Enter feedback here" id="comments" hidden/></div>
                           <div class="col-md-2"> <input type="button" class=" btn btn-warning pull-right" id="mailSend" value="Send"  data-id="'.$question->TM_QN_Id.'"hidden ></div>
                           </div>

                        </td></tr>';
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function actionShowpaper($id)
    {
        $connection = CActiveRecord::getDbConnection();
        if(isset($_POST['coomplete']))
        {

            $test=StudentTest::model()->findByPk($id);
            $test->TM_STU_TT_Status='1';
            $test->save(false);
            $this->redirect(array('TestComplete','id'=>$id));
        }

        $paperquestions=$this->GetPaperQuestions($id);
        $this->render('showpaperquestion',array('id'=>$id,'paperquestions'=>$paperquestions));
    }

    public function GetStudentDet()
    {
        $user=Yii::app()->user->id;
        $plan=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId='.$user));
        return $plan[0];
    }
    public function actionGetTopics()
    {
        $arr = explode(',', $_POST['topicids']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_TP_Id' ,$arr );
        $model = Chapter::model()->findAll($criteria);
        $topic='';
        foreach ($model as $key=>$value) {
            $topic=($key=='0'?$value->TM_TP_Name:$topic.','.$value->TM_TP_Name);
        }
        echo $topic;
    }
    public function GetPaperQuestions($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type IN (6,7) AND TM_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STU_QN_Number ASC';
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        return $selectedquestion;
    }
    public function OptionHint($type,$question)
    {
        $returnval=array();
        if($type=='1'):
            $corectans=Answers::model()->count(
                array(
                    'condition'=>'TM_AR_Question_Id=:question AND TM_AR_Correct=:correct',
                    'params'=>array(':question'=>$question,':correct'=>'1')
                ));
            $returnval['hint']='<span class="optionhint">Select any '.$corectans.' options</span>';
            $returnval['correctoptions']=$corectans;
        elseif($type=='2'):
            $returnval['hint']='<span class="optionhint">Select an option</span>';
            $returnval['correctoptions']='1';
        elseif($type=='3'):
            $returnval['hint']='<span class="optionhint">Select either True Or False</span>';
            $returnval['correctoptions']='1';
        elseif($type=='4'):
            $returnval['hint']='<span class="optionhint">Enter Your Answer</span>';
            $returnval['correctoptions']='0';
        elseif($type=='5'):
            $returnval['hint']='<span class="optionhint">Answer the following questions</span>';
            $returnval['correctoptions']='0';
        endif;
        return $returnval;
    }

    public function actionDeletetest($id)
    {
        if(StudentTest::model()->findByPk($id)->delete())
        {
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STC_Test_Id='.$id);
            $model = StudentTestChapters::model()->deleteAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_QN_Test_Id='.$id);
            $model = StudentQuestions::model()->deleteAll($criteria);
            echo "yes";
        }
        else
        {
            echo "no";
        }
    }
    public function actionDeletechallenge($id)
    {
        $challenge=Challenges::model()->findByPk($id);
        if($challenge->TM_CL_Chalanger_Id==Yii::app()->user->id):

                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CL_QS_Chalange_Id='.$id);
                $model = Chalangequestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CEUQ_Challenge='.$id);
                $model = Chalangeuserquestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CE_CP_Challenge_Id='.$id);
                $model = ChallengeChapters::model()->deleteAll($criteria);
                $challenge->TM_CL_Status='1';
                $challenge->save(false);
                echo "yes";
        else:
                echo "no";
        endif;
    }
    public function actionCancelchallenge($id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status='4';
        if($model->save(false)):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id);
            $model = Chalangeuserquestions::model()->deleteAll($criteria);
            echo "yes";
        else:
            echo "no";
        endif;
    }
    public function actionAcceptchallenge($id)
    {

        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status='1';
        $return='';
        if($model->save(false)):
            $return.='<td data-title="Date">'.($model->challenge->TM_CL_Chalanger_Id==Yii::app()->user->id?"Me":$this->GetChallenger($model->challenge->TM_CL_Chalanger_Id)).'</td>';
            $return.='<td data-title="Publisher">'.date("d-m-Y", strtotime($model->challenge->TM_CL_Created_On)).'</td>';
            $return.='<td data-title="Division">'.$this->GetChallengeInvitees($model->challenge->TM_CL_Id,Yii::app()->user->id).'</td>';
            $return.='<td data-title="Link">';
            $return.="<a href='".Yii::app()->createUrl('student/startchallenge', array('id' => $id))."' class='btn btn-warning'>".(Student::model()->GetChallengeStatus($id,Yii::app()->user->id)>0?'Continue':'Start Challenge')."</a>";
            $return.='</td>';
            $return.='<td data-title="Completed">';
            $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                'student/cancelchallenge/'.$id,
                array (
                    'type'=>'POST',
                    'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$id.'").remove();} }'
                ),
                array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$id)
            );
            $return.='</td>';
        echo $return;
        else:
            echo "no";
        endif;
    }
    public function actionChallenge()
    {
        $plan=$this->GetStudentDet();
        $mainchapters=Chapter::model()->findAll(array('condition'=>"TM_TP_Syllabus_Id='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'"));
        if(isset($_POST['Challenge'])):
           if(count($_POST['chapters'])>0):
                $chapters='';
                $topics='';
                $challenge= new Challenges();
                $challenge->TM_CL_Chalanger_Id=Yii::app()->user->id;
                $challenge->TM_CL_QuestionCount=$_POST['Challenge']['questions'];
                $challenge->save(false);
                $recepchalange= new Chalangerecepient();
                $recepchalange->TM_CE_RE_Chalange_Id=$challenge->TM_CL_Id;
                $recepchalange->TM_CE_RE_Recepient_Id=Yii::app()->user->id;
                $recepchalange->TM_CE_RE_Status='1';
                $recepchalange->save(false);
               if(count($_POST['invitebuddies'])>0):
                   for($i=0;$i<count($_POST['invitebuddies']);$i++):
                       $recepchalange= new Chalangerecepient();
                       $recepchalange->TM_CE_RE_Chalange_Id=$challenge->TM_CL_Id;
                       $recepchalange->TM_CE_RE_Recepient_Id=$_POST['invitebuddies'][$i];
                       $recepchalange->save(false);
                   endfor;
               endif;
                for($i=0;$i<count($_POST['chapters']);$i++):
                    $chapters=$chapters.($i==0?$_POST['chapters'][$i]:",".$_POST['chapters'][$i]);
                    $chapterid=$_POST['chapters'][$i];
                    $challengechapter= new ChallengeChapters();
                    $challengechapter->TM_CE_CP_Challenge_Id=$challenge->TM_CL_Id;
                    $challengechapter->TM_CE_CP_Chapter_Id=$chapterid;
                    $challengechapter->save(false);
                    for($j=0;$j<count($_POST['topic'.$chapterid]);$j++):
                        $topics=$topics.($i==0 & $j==0?$_POST['topic'.$chapterid][$j]:",".$_POST['topic'.$chapterid][$j]);
                    endfor;
                endfor;
               $Syllabus= $plan->TM_SPN_SyllabusId;
               $Standard= $plan->TM_SPN_StandardId;
               //$Topic=$topics;
               $Test=$challenge->TM_CL_Id;
               $Student=Yii::app()->user->id;
               $limit=$challenge->TM_CL_QuestionCount;
               //set questions for basic
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,1,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $basiccount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                //set questions for intermediate
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,2,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $intercount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
               //set questions for advanced
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,3,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $advancecount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
            endif;
            $this->redirect(array('home'));
        endif;
        $this->render('challenge',array('plan'=>$plan,'chapters'=>$mainchapters));
    }
    public function actionChallengeTest($id)
    {
        $challengecount=Challenges::model()->findByPk($id)->TM_CL_QuestionCount;
        if(isset($_POST['action']['GoNext'])):
                    //change challenge status to started
                    $criteria = new CDbCriteria;
                    $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
                    $model = Chalangerecepient::model()->find($criteria);
                    if($model->TM_CE_RE_Status=='1'):
                        $model->TM_CE_RE_Status='2';
                        $model->save(false);
                    endif;
                    $chuserquest=Chalangeuserquestions::model()->findByPk($_POST['ChallengeQuestionId']);
                    $masterquestion=Questions::model()->findByPk($_POST['QuestionId']);
                    $difficulty=$chuserquest->TM_CEUQ_Difficulty;
                        if($difficulty=='1'):
                            $marks='5';
                        elseif($difficulty=='2'):
                            $marks='8';
                        elseif($difficulty=='3'):
                            $marks='12';
                        endif;
                        if($_POST['QuestionType']=='1'):
                            $answer=implode(',',$_POST['answer']);
                            $chuserquest->TM_CEUQ_Answer_Id=$answer;
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                if($ans->TM_AR_Correct=='1'):
                                    $correctcount++;
                                    if (array_search($ans->TM_AR_Id, $_POST['answer'])!==false) {
                                        $usercorrect++;
                                    }
                                endif;
                            endforeach;
                                if($correctcount==$usercorrect):

                                    if($difficulty=='1'):
                                        $chuserquest->TM_CEUQ_Marks='5';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                    elseif($difficulty=='2'):
                                        $chuserquest->TM_CEUQ_Marks='8';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                    elseif($difficulty=='3'):
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                    endif;
                                else:
                                    if($difficulty=='1'):
                                        if($this->NextDiffCount($id,'1')>0):
                                            $nextdifficulty='1';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                    elseif($difficulty=='2'):
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                    elseif($difficulty=='3'):
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                    endif;
                                endif;
                        elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                            $answer=$_POST['answer'];
                            $answerarray=array($_POST['answer']);
                            $chuserquest->TM_CEUQ_Answer_Id=$answer;
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                if($ans->TM_AR_Correct=='1'):
                                    $correctcount++;
                                    if (array_search($ans->TM_AR_Id, $answerarray)!==false) {
                                        $usercorrect++;
                                    }
                                endif;
                            endforeach;
                            if($correctcount==$usercorrect):

                                if($difficulty=='1'):
                                    $chuserquest->TM_CEUQ_Marks='5';
                                    if($this->NextDiffCount($id,'2')>0):
                                        $nextdifficulty='2';
                                    else:
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                elseif($difficulty=='2'):
                                    $chuserquest->TM_CEUQ_Marks='8';
                                    if($this->NextDiffCount($id,'3')>0):
                                        $nextdifficulty='3';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                elseif($difficulty=='3'):
                                    $chuserquest->TM_CEUQ_Marks='12';
                                    if($this->NextDiffCount($id,'3')>0):
                                        $nextdifficulty='3';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                endif;
                            else:
                                if($difficulty=='1'):
                                    if($this->NextDiffCount($id,'1')>0):
                                        $nextdifficulty='1';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='3';
                                        endif;
                                    endif;
                                elseif($difficulty=='2'):
                                    if($this->NextDiffCount($id,'2')>0):
                                        $nextdifficulty='2';
                                    else:
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                elseif($difficulty=='3'):
                                    if($this->NextDiffCount($id,'3')>0):
                                        $nextdifficulty='3';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                endif;
                            endif;
                        elseif($_POST['QuestionType']=='4'):
                            $chuserquest->TM_CEUQ_Answer_Id=$_POST['answer'];
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                $answeroption=explode(';',strip_tags(strtolower($ans->TM_AR_Answer)));
                                if (array_search(strtolower($_POST['answer']), $answeroption)!==false) {
                                    $correctcount++;
                                    $usercorrect++;
                                }
                            endforeach;
                            if($correctcount==$usercorrect):

                                if($difficulty=='1'):
                                    $chuserquest->TM_CEUQ_Marks='5';
                                    if($this->NextDiffCount($id,'2')>0):
                                        $nextdifficulty='2';
                                    else:
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                elseif($difficulty=='2'):
                                    $chuserquest->TM_CEUQ_Marks='8';
                                    if($this->NextDiffCount($id,'3')>0):
                                        $nextdifficulty='3';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                elseif($difficulty=='3'):
                                    $chuserquest->TM_CEUQ_Marks='12';
                                    if($this->NextDiffCount($id,'3')>0):
                                        $nextdifficulty='3';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                endif;
                            else:
                                if($difficulty=='1'):
                                    if($this->NextDiffCount($id,'1')>0):
                                        $nextdifficulty='1';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='3';
                                        endif;
                                    endif;
                                elseif($difficulty=='2'):
                                    if($this->NextDiffCount($id,'2')>0):
                                        $nextdifficulty='2';
                                    else:
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                elseif($difficulty=='3'):
                                    if($this->NextDiffCount($id,'3')>0):
                                        $nextdifficulty='3';
                                    else:
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            $nextdifficulty='1';
                                        endif;
                                    endif;
                                endif;
                            endif;
                        endif;
                $chuserquest->TM_CEUQ_Status='1';
                $chuserquest->save(false);
                if($challengecount=='10' & $chuserquest->TM_CEUQ_Iteration<='5'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    $userquestion= new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge=$id;
                    $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration=$nextcount;
                    $userquestion->save(false);
                elseif($challengecount=='15' & $chuserquest->TM_CEUQ_Iteration<='10'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    $userquestion= new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge=$id;
                    $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration=$nextcount;
                    $userquestion->save(false);
                elseif($challengecount=='20' & $chuserquest->TM_CEUQ_Iteration<='15'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    $userquestion= new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge=$id;
                    $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration=$nextcount;
                    $userquestion->save(false);
                elseif($challengecount=='25' & $chuserquest->TM_CEUQ_Iteration<='20'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    $userquestion= new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge=$id;
                    $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration=$nextcount;
                    $userquestion->save(false);
                elseif($challengecount==$chuserquest->TM_CEUQ_Iteration):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CE_RE_Chalange_Id=:test AND TM_CE_RE_Recepient_Id=:user';
                    $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
                    $nextquestions=Chalangerecepient::model()->find($criteria);
                    $nextquestions->TM_CE_RE_Status='3';
                    $nextquestions->save(false);
                    $this->redirect(array('challengeresult','id'=>$id));
                endif;

        endif;
        if(Student::model()->GetChallengeStatus($id,Yii::app()->user->id)==0):
            $criteria=new CDbCriteria;
            $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=1';
            $criteria->limit='3';
            $criteria->order='RAND()';
            $criteria->params=array(':test'=>$id);
            $questions=Chalangequestions::model()->findAll($criteria);
            foreach($questions AS $key=>$question):
                $count=$key+1;
                $userquestion= new Chalangeuserquestions();
                $userquestion->TM_CEUQ_Challenge=$id;
                $userquestion->TM_CEUQ_Question_Id=$question->TM_CL_QS_Question_Id;
                $userquestion->TM_CEUQ_Difficulty=$question->TM_CL_QS_Difficulty;
                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                $userquestion->TM_CEUQ_Iteration=$count;
                $userquestion->save(false);
            endforeach;
            if($count>3):
                $newcount=(3-$count)+2;
            else:
                $newcount=2;
            endif;
            $criteria=new CDbCriteria;
            $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=2';
            $criteria->limit=$newcount;
            $criteria->order='RAND()';
            $criteria->params=array(':test'=>$id);
            $questions=Chalangequestions::model()->findAll($criteria);
            foreach($questions AS $key=>$question):
                $count=$count+1;
                $userquestion= new Chalangeuserquestions();
                $userquestion->TM_CEUQ_Challenge=$id;
                $userquestion->TM_CEUQ_Question_Id=$question->TM_CL_QS_Question_Id;
                $userquestion->TM_CEUQ_Difficulty=$question->TM_CL_QS_Difficulty;
                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                $userquestion->TM_CEUQ_Iteration=$count;
                $userquestion->save(false);
            endforeach;
        endif;
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CEUQ_Challenge=:test AND TM_CEUQ_Status=0 AND TM_CEUQ_User_Id=:user';
        $criteria->limit='1';
        $criteria->order='TM_CEUQ_Iteration ASC';
        $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
        $question=Chalangeuserquestions::model()->find($criteria);
        $this->renderPartial('challengequestion',array('testid'=>$id,'challengequestion'=>$question,'question'=>$question->question));

    }
    public function actionChallengeresult($id)
    {
        $challengecount=Challenges::model()->findByPk($id)->TM_CL_QuestionCount;
        if($challengecount=='10'):
            $totalmarks='79';
        elseif($challengecount=='15'):
            $totalmarks='139';
        elseif($challengecount=='20'):
            $totalmarks='199';
        elseif($challengecount=='25'):
            $totalmarks='199';
        endif;
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_CEUQ_Marks) AS total');
        $criteria->condition='TM_CEUQ_Challenge=:test AND TM_CEUQ_User_Id=:user';
        $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
        $studenttotal=Chalangeuserquestions::model()->find($criteria);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $recepient = Chalangerecepient::model()->find($criteria);
        $recepient->TM_CE_RE_ObtainedMarks=$studenttotal->total;
        $recepient->save();
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id);
        $criteria->order='TM_CE_RE_ObtainedMarks DESC';
        $participants = Chalangerecepient::model()->findAll($criteria);
        $userids=array();
        foreach($participants AS $party):
            $userids[]=$party->TM_CE_RE_Recepient_Id;
        endforeach;

        $this->render('challengeresult',array('totalmarks'=>$totalmarks,'studenttotal'=>$studenttotal->total,'participants'=>$participants,'userids'=>$userids));
    }
    public function GetPaperCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type IN (:type) AND TM_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id,':type'=>'6,7');
        $selectedquestion=StudentQuestions::model()->find($criteria);
        return count($selectedquestion);
    }
    public function GetPaperCountMock($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type IN (:type) AND TM_STMKQT_Parent_Id=0';
        $criteria->params=array(':test'=>$id,':type'=>'6,7');
        $selectedquestion=Studentmockquestions::model()->find($criteria);
        return count($selectedquestion);
    }
    public function GetQuestionCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test';
        $criteria->params=array(':test'=>$id);
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        return count($selectedquestion);
    }
    public function doLatercount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Flag=:flag';
        $criteria->params=array(':test'=>$id,':flag'=>'2');
        $selectedquestion=StudentQuestions::model()->find($criteria);
        return count($selectedquestion);
    }
    public function GetTestQuestions($id,$question)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Type NOT IN (6,7)';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STU_QN_Number ASC';
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        $pagination='';
        foreach($selectedquestion AS $key=>$testquestion):
            $count=$key+1;
            $class='';
            if($testquestion->TM_STU_QN_Question_Id==$question)
            {
                $questioncount=$count;
            }
            if($testquestion->TM_STU_QN_Type=='6' || $testquestion->TM_STU_QN_Type=='7')
            {
                $pagination.='<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">'.$count.'</a></li>';
            }
            else
            {
                $title='';
                $class='';
                if($testquestion->TM_STU_QN_Flag=='0')
                {
                    $class=($testquestion->TM_STU_QN_Question_Id==$question?'class="active"':'');
                    $title=($testquestion->TM_STU_QN_Question_Id==$question?'Current Question':'');
                }
                elseif($testquestion->TM_STU_QN_Flag=='1')
                {
                    $class=($testquestion->TM_STU_QN_Question_Id==$question?'class="active"':'class="answered"');
                    $title=($testquestion->TM_STU_QN_Question_Id==$question?'Current Question':'Already Answered');
                }
                elseif($testquestion->TM_STU_QN_Flag=='2')
                {
                    $class=($testquestion->TM_STU_QN_Question_Id==$question?'class="active"':'class="skiped"');
                    $title=($testquestion->TM_STU_QN_Question_Id==$question?'Current Question':'Skipped Question');
                }
                $pagination.='<li '.$class.' ><a data-toggle="tooltip" data-placement="bottom" title="'.$title.'" href="'.($testquestion->TM_STU_QN_Question_Id==$question?'javascript:void(0)':Yii::app()->createUrl('student/RevisionTest',array('id'=>$id,'questionnum'=>base64_encode('question_'.$testquestion->TM_STU_QN_Number)))).'">'.$count.'</a></li>';
            }
        endforeach;
        return array('pagination'=>$pagination,'questioncount'=>$questioncount,'totalcount'=>$count);
    }
    public function GetChildAnswer($type,$test,$question)
    {

        $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$test,'TM_STU_QN_Student_Id'=>Yii::app()->user->id,'TM_STU_QN_Question_Id'=>$question));
        if(count($questionid)>0):
            if($type=='1'):
                if($questionid->TM_STU_QN_Answer_Id!=''):
                    return explode(',',$questionid->TM_STU_QN_Answer_Id);
                else:
                    return array();
                endif;
            else:
                return $questionid->TM_STU_QN_Answer;
            endif;
        else:
            return array();
        endif;
    }
    public function GetQuickLimits($total)
    {
        $mode=$total%3;
        if($mode==0):
            $count=$total/3;
            return array('basic'=>$count,'intermediate'=>$count,'advanced'=>$count);
        elseif($mode==1):
            $count=round($total/3);
            return array('basic'=>$count,'intermediate'=>$count+1,'advanced'=>$count);
        elseif($mode==2):
            $count=$total/3;
            return array('basic'=>ceil($count),'intermediate'=>ceil($count),'advanced'=>floor($count));
        endif;

    }
    public function ResultMessage($percentage)
    {
        $hundred=array('Congratulations. All hail the Math Master! ','Wow ! Time to Celebrate..','High - five !! Now, thats what I call a score !');
        $ninety=array('Excellent score ! Well done.','Excellent.. Next time its 100% !','Excellent Score .. Keep it up !');
        $seventyfive=array('Well done, but the 90\'s suit you better !','Not a bad score.. Well done, you !','Keep it up ! Keep it better !');
        $sixty=array('Decent effort, But you can do better !',' Well done you ! But you can do better !','A decent effort. Aim for higher !');
        $fifty=array('Not bad, but brush up a little more..','Do revise and try again !','Practice Makes Perfect. Try again !');
        $belowforty=array('Sorry.. do brush up and try again ! ','Hmm.. do we need some more practice ?','Practice Makes Perfect. Try again !');
        if($percentage=='100'):
            $rand=array_rand($hundred);
            return $hundred[$rand];
        elseif($percentage<=99 & $percentage>=90):
            $rand=array_rand($ninety);
            return $ninety[$rand];
        elseif($percentage<=89 & $percentage>=75):
            $rand=array_rand($seventyfive);
            return $seventyfive[$rand];
        elseif($percentage<=74 & $percentage>=60):
            $rand=array_rand($sixty);
            return $sixty[$rand];
        elseif($percentage<=59 & $percentage>=40):
            $rand=array_rand($fifty);
            return $fifty[$rand];
        elseif($percentage<40):
            $rand=array_rand($belowforty);
            return $belowforty[$rand];
        endif;
        //elseif():
    }
    public function GetBuddies($user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='superuser=0 AND usertype=6 AND status=1 AND id!='.$user;
        $users=User::model()->findAll($criteria);
        $usersjson='';
        foreach($users AS $key=>$user):
            $usersjson.=($key==0?'{"id":'.$user->id.', "name":"'.$user->username.'"}':',{"id":'.$user->id.', "name":"'.$user->username.'"}');
        endforeach;
        return $usersjson;

    }
    public function GetChallengeInvitees($challenge,$user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Recepient_Id!='.$user;
        $users=Chalangerecepient::model()->findAll($criteria);
        $usersjson='Me ';
        if(count($users)>3):
            $othercount=count($users)-3;
            $others=' And '.$othercount.' Others';
        else:
            $others='';
        endif;
        foreach($users AS $key=>$user):
            if($key<3):
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username;
                $usersjson.=','.$challengeuser;
            endif;
        endforeach;
        return $usersjson.$others;

    }
    public function GetChallenger($id)
    {
        return User::model()->findByPk($id)->username;
    }
    public function GetChallangeInviteStatus($challenge)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Status IN (2,3)');
        $model = Chalangerecepient::model()->count($criteria);
        return $model;
    }
    public function getChallengeLinks($id,$user)
    {
        $challenge=Challenges::model()->findByPk($id);
        $return='';
        if($challenge->TM_CL_Chalanger_Id==$user):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge->TM_CL_Id.' AND TM_CE_RE_Recepient_Id='.$user);
            $recepient = Chalangerecepient::model()->find($criteria);
            $return.='<td data-title="Link">';
                if($recepient->TM_CE_RE_Status=='1'):
                    $return.="<a href='".Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Start Challenge</a>";
                elseif($recepient->TM_CE_RE_Status=='2'):
                    $return.="<a href='".Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Continue</a>";
                endif;
            $return.='</td>';
            $return.='<td data-title="Completed">';
                if($this->GetChallangeInviteStatus($id)>0):
                    $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                        Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?')
                    );
                else:
                    $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                        Yii::app()->request->baseUrl.'/student/deletechallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to delete this Challenge?')
                    );
                endif;
            $return.='</td>';
        else:
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge->TM_CL_Id.' AND TM_CE_RE_Recepient_Id='.$user);
            $recepient = Chalangerecepient::model()->find($criteria);
            if($recepient->TM_CE_RE_Status=='0'):
                $return.='<td data-title="Link">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/right.png','',array('class'=>'hvr-buzz-out','title'=>'Accept Challenge')),
                    Yii::app()->request->baseUrl.'/student/acceptchallenge/'.$challenge->TM_CL_Id,
                    array (
                        'type'=>'POST',
                        'success'=>'function(html){ if(html!="no"){$("#challengerow'.$challenge->TM_CL_Id.'").html(html);} }'
                    )
                );
                $return.='</td>';
                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out','title'=>'Cancel Challenge')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                    array (
                        'type'=>'POST',
                        'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                    ),
                    array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='1'):
                $return.='<td data-title="Link">';
                $return.="<a href='".Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Start Challenge</a>";
                $return.='</td>';
                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                    );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='2'):
                $return.='<td data-title="Link">';
                $return.="<a href='".Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Continue</a>";
                $return.='</td>';
                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                    );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='3'):
            elseif($recepient->TM_CE_RE_Status=='4'):
            endif;
        endif;
        return $return;
    }
    public function NextDiffCount($id,$nextdifficulty)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty;
        $criteria->limit='1';
        $criteria->order='RAND()';
        $criteria->params=array(':test'=>$id);
        return Chalangequestions::model()->count($criteria);

    }
    public function GetChallengePosition($users,$id)
    {
        $position=array_keys($users, $id);
        $position=$position[0]+1;
        if($position=='1'):
            return $position.'<sup>st</sup>';
        elseif($position=='2'):
            return $position.'<sup>nd</sup>';
        elseif($position=='3'):
            return $position.'<sup>rd</sup>';
        else:
            return $position.'<sup>th</sup>';
        endif;
    }
    public function actionSendMail()

    {
        $questionId = $_POST["questionId"];
        $comments = $_POST["comments"];

        $question=Questions::model()->findByPk($questionId);
        $sendQuestion= $question->TM_QN_Question;
        $sendReff= $question->TM_QN_QuestionReff;
        $userId=Yii::app()->user->getId();
        $userDet=User::model()->findByPk($userId);
        $content="<p>Hi,</p><p>$userDet->username </p><p>Has Suggested  a solution for a question(".$sendReff.") </p><p>". $sendQuestion."</p><p>".$comments."</p>";
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;

        $mail->IsSMTP();
        $mail->Host = 'smtp.gmail.com';
        echo $comments;
        $mail->SMTPAuth = true;
        $mail->Port = 465;
        $mail->IsHTML(true);
        $mail->SMTPSecure = "ssl";
        $mail->Username = 'social@solminds.com';
        $mail->Password = 'sols4gol';
        $mail->SetFrom('social@solminds.com', 'TabbieMe');
        $mail->Subject = 'TabbieMe Suggetion For Questions';
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($content);
        $mail->AddAddress('karthik@solminds.com', 'Rema');



        if(!$mail->Send()) {

            echo "Mailer Error: " . $mail->ErrorInfo;

        } else {

            echo "Message sent!";
            return success;

        }
    }





    /*Mock Related functions*/
    public function actionMock()
    {
        $plan=$this->GetStudentDet();
        $criteria=new CDbCriteria;
        $criteria->condition='TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Id NOT IN ( SELECT TM_STMK_Mock_Id FROM tm_student_mocks WHERE TM_STMK_Student_Id='.Yii::app()->user->id.' AND TM_STMK_Status=1)';
        $criteria->params=array(':publisher'=>'3',':syllabus'=>$plan->TM_SPN_SyllabusId,':standard'=>$plan->TM_SPN_StandardId,':status'=>'0');
        $mocks=Mock::model()->findAll($criteria);
        $this->render('mocks',array('mocks'=>$mocks));
    }
    public function actionStartmock($id)
    {
        $test=Mock::model()->findByPk($id);
        if(isset($_POST['startTest'])):
            $criteria=new CDbCriteria;
            $criteria->condition='TM_MQ_Mock_Id=:mock';
            $criteria->params=array(':mock'=>$id);
            $mockquestions=MockQuestions::model()->findAll($criteria);
            $studentmock=new StudentMocks();
            $studentmock->TM_STMK_Student_Id=Yii::app()->user->id;
            $studentmock->TM_STMK_Mock_Id=$id;
            $studentmock->TM_STMK_Status='0';
            $studentmock->save(false);
            foreach($mockquestions AS $key=>$mockquestion):
                $studentmockquestion=new Studentmockquestions();
                $studentmockquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                $studentmockquestion->TM_STMKQT_Mock_Id=$id;
                $studentmockquestion->TM_STMKQT_Type=$mockquestion->TM_MQ_Type;
                $studentmockquestion->TM_STMKQT_Question_Id=$mockquestion->TM_MQ_Question_Id;
                $studentmockquestion->TM_STMKQT_Status=='0';
                $studentmockquestion->save(false);
            endforeach;
            $this->redirect(array('Mocktest','id'=>$id));
        endif;
        $this->render('startmock',array('id'=>$id,'test'=>$test));
    }
    public function actionMocktest($id)
    {
        if(isset($_POST['action']['GoNext']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STMKQT_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id'=>$child->TM_QN_Id,'TM_STMKQT_Mock_Id'=>$id,'TM_STMKQT_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id=$id;
                    $selectedquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STMKQT_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            endif;
            $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                array(
                    'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Id>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STMKQT_Id ASC, TM_STMKQT_Status ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionTableid'])
                )
            );
        }
        if(isset($_POST['action']['GetPrevios'])):

            $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                array(
                    'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0,1,2) AND TM_STMKQT_Type NOT IN (6,7) AND TM_STMKQT_Id<:tableid AND TM_STMKQT_Parent_Id=0',
                    'limit'=>1,
                    'order'=>'TM_STMKQT_Id DESC, TM_STMKQT_Status ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionTableid'])
                )
            );
        endif;
        if(isset($_POST['action']['DoLater'])):
            if($_POST['QuestionType']!='5'):
                if(isset($_POST['answer'])):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=Studentmockquestions::model()->find($criteria);
                    if($_POST['QuestionType']=='1'):
                        $answer=implode(',',$_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                        $answer=$_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($_POST['QuestionType']=='4'):
                        $selectedquestion->TM_STMKQT_Answer=$_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_Status='2';
                    $selectedquestion->save(false);
                else:
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_Status='2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    if(isset($_POST['answer'.$child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id'=>$child->TM_QN_Id,'TM_STMKQT_Mock_Id'=>$id,'TM_STMKQT_Student_Id'=>Yii::app()->user->id)
                        );
                        if(count($childquestion)=='1'):
                            $selectedquestion=$childquestion;
                        else:
                            $selectedquestion=new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id=$id;
                        $selectedquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id=$child->TM_QN_Id;
                        if($child->TM_QN_Type_Id=='1'):
                            $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer=$answer;
                        elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                            $answer=$_POST['answer'.$child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer=$answer;
                        elseif($child->TM_QN_Type_Id=='4'):
                            $selectedquestion->TM_STMKQT_Answer=$_POST['answer'.$child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id=$_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status='2';
                $selectedquestion->save(false);
            endif;
            $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                array(
                    'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Id>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STMKQT_Id ASC, TM_STMKQT_Status ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionTableid'])
                )
            );
        endif;

        if(isset($_POST['action']['Complete']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STMKQT_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id'=>$child->TM_QN_Id,'TM_STMKQT_Mock_Id'=>$id,'TM_STMKQT_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id=$id;
                    $selectedquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STMKQT_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            endif;
            if($this->GetPaperCountMock($id)!=0):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMK_Mock_Id=:mock AND TM_STMK_Student_Id=:student';
                $criteria->params=array(':mock'=>$id,':student'=>Yii::app()->user->id);
                $test=StudentMocks::model()->find($id);
                $test->TM_STMK_Status='1';
                $test->save(false);
                $this->redirect(array('showmockpaper','id'=>$id));
            else:
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMK_Mock_Id=:mock AND TM_STMK_Student_Id=:student';
                $criteria->params=array(':mock'=>$id,':student'=>Yii::app()->user->id);
                $test=StudentMocks::model()->find($id);
                $test->TM_STMK_Status='1';
                $test->save(false);
                $this->redirect(array('mockcomplete','id'=>$id));
            endif;
            //$this->redirect(array('TestComplete','id'=>$id));
        }
        if(!isset($_POST['action'])):
            if(isset($_GET['questionnum'])):
                $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                    array(
                        'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0,1,2) AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Question_Id='.str_replace("question_","",base64_decode($_GET['questionnum'])),
                        'limit'=>1,
                        'order'=>'TM_STMKQT_Question_Id ASC, TM_STMKQT_Status ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            else:
                $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                    array(
                        'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0) AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Parent_Id=0',
                        'limit'=>1,
                        'order'=>'TM_STMKQT_Question_Id ASC, TM_STMKQT_Status ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            endif;
        endif;
        if(count($questionid)):
            $question=Questions::model()->findByPk($questionid->TM_STMKQT_Question_Id);
            $this->renderPartial('mocktest',array('testid'=>$id,'question'=>$question,'questionid'=>$questionid));
        else:
            $this->redirect(array('mock'));
        endif;
    }
    public function actionMockComplete($id)
    {
        $results=Studentmockquestions::model()->findAll(
            array(
                'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type NOT IN(6,7)',
                'params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)
            )
        );
        $totalmarks=0;
        $earnedmarks=0;
        $result='';
        foreach($results AS $exam):
            $question=Questions::model()->findByPk($exam->TM_STMKQT_Question_Id);
            $marks=0;
            if($question->TM_QN_Type_Id!='4' & $exam->TM_STMKQT_Status!='2'):
                if($exam->TM_STMKQT_Answer!=''):

                    $studentanswer=explode(',',$exam->TM_STMKQT_Answer);
                    foreach($question->answers AS $ans):
                        if($ans->TM_AR_Correct=='1'):
                            if (array_search($ans->TM_AR_Id, $studentanswer)!==false) {
                                $marks=$marks+$ans->TM_AR_Marks;
                            }
                        endif;
                        $totalmarks=$totalmarks+$ans->TM_AR_Marks;
                    endforeach;
                    if(count($studentanswer)>0):
                        $correctanswer='<ul class="list-group">';
                        for($i=0;$i<count($studentanswer);$i++)
                        {
                            $correctanswer.='<li class="list-group-item">'.Answers::model()->findByPk($studentanswer[$i])->TM_AR_Answer.'</li>';
                        }
                        $correctanswer.='</ul>';
                    endif;
                else:
                    foreach($question->answers AS $ans):
                        $totalmarks=$totalmarks+$ans->TM_AR_Marks;
                    endforeach;
                    $correctanswer='<ul class="list-group"><li class="list-group-item">Question Skipped</li></ul>';
                endif;
            else:
                if($exam->TM_STMKQT_Answer!=''):
                    $correctanswer='<ul class="list-group"><li class="list-group-item">'.$exam->TM_STMKQT_Answer.'</li></ul>';
                    foreach($question->answers AS $ans):
                        $answeroption=explode(';',strip_tags(strtolower($ans->TM_AR_Answer)));
                        if (array_search(strtolower($exam->TM_STMKQT_Answer), $answeroption)!==false) {
                            $marks=$marks+$ans->TM_AR_Marks;

                        }
                        $totalmarks=$totalmarks+$ans->TM_AR_Marks;
                    endforeach;
                else:
                    foreach($question->answers AS $ans):
                        $totalmarks=$totalmarks+$ans->TM_AR_Marks;
                    endforeach;
                    $correctanswer='<ul class="list-group"><li class="list-group-item">Question Skipped</li></ul>';
                endif;
            endif;
            $result.='<tr><td>
                            <div class="col-md-12"><i class="fa '.($marks=='0'?'fa fa-times clrr':'fa-check').'"></i> '.$question->TM_QN_Question.'<span class="glyphicon glyphicon-star-empty"></span></div>
                            <div class="col-md-12">'.$correctanswer.'</div>
                        </td>';
            //glyphicon glyphicon-star sty
            $exam->TM_STMKQT_Marks=$marks;
            $exam->save(false);
            $earnedmarks=$earnedmarks+$marks;
        endforeach;
        $percentage=($earnedmarks/$totalmarks)*100;
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMK_Mock_Id=:mockid AND TM_STMK_Student_Id=:studentid';
        $criteria->params=array(':mockid'=>$id,':studentid'=>Yii::app()->user->id);
        $test=StudentMocks::model()->find($criteria);
        $test->TM_STMK_Marks=$earnedmarks;
        $test->TM_STMK_TotalMarks=$totalmarks;
        $test->TM_STMK_Percentage=round($percentage,1);
        $test->TM_STMK_Status='1';
        $test->save(false);
        $this->render('showmockresult',array('test'=>$test,'testresult'=>$result));
    }
    public function actionDeletemock($id)
    {

        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMK_Mock_Id=:mockid AND TM_STMK_Student_Id=:studentid';
        $criteria->params=array(':mockid'=>$id,':studentid'=>Yii::app()->user->id);
        $studentmock=StudentMocks::model()->find($criteria);
        $return='';
        $mock=Mock::model()->findByPk($id);
        $return.='<td>'.$mock->TM_MK_Name.'</td>';
        $return.='<td>'.$mock->TM_MK_Description.'</td>';
        $return.='<td>'.$mock->TM_MK_CreatedOn.'</td>';
        $return.='<td colspan="2"><a href="'.Yii::app()->createUrl('student/startmock', array('id' => $mock->TM_MK_Id)).'" class="btn btn-warning">Start Mock</a></td>';

        if($studentmock->delete())
        {
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMKQT_Student_Id='.Yii::app()->user->id.' AND TM_STMKQT_Mock_Id='.$id);
            $model = Studentmockquestions::model()->deleteAll($criteria);
            echo $return;
        }
        else
        {
            echo "no";
        }
    }
    public function GetMockQuestions($id,$question)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Type NOT IN (6,7)';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STMKQT_Id ASC';
        $selectedquestion=Studentmockquestions::model()->findAll($criteria);
        $pagination='';
        foreach($selectedquestion AS $key=>$testquestion):
            $count=$key+1;
            $class='';
            if($testquestion->TM_STMKQT_Question_Id==$question)
            {
                $questioncount=$count;
            }
            if($testquestion->TM_STMKQT_Type=='6' || $testquestion->TM_STMKQT_Type=='7')
            {
                $pagination.='<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">'.$count.'</a></li>';
            }
            else
            {
                $title='';
                $class='';
                if($testquestion->TM_STMKQT_Status=='0')
                {
                    $class=($testquestion->TM_STMKQT_Question_Id==$question?'class="active"':'');
                    $title=($testquestion->TM_STMKQT_Question_Id==$question?'Current Question':'');
                }
                elseif($testquestion->TM_STMKQT_Status=='1')
                {
                    $class=($testquestion->TM_STMKQT_Question_Id==$question?'class="active"':'class="answered"');
                    $title=($testquestion->TM_STMKQT_Question_Id==$question?'Current Question':'Already Answered');
                }
                elseif($testquestion->TM_STMKQT_Status=='2')
                {
                    $class=($testquestion->TM_STMKQT_Question_Id==$question?'class="active"':'class="skiped"');
                    $title=($testquestion->TM_STMKQT_Question_Id==$question?'Current Question':'Skipped Question');
                }
                $pagination.='<li '.$class.' ><a data-toggle="tooltip" data-placement="bottom" title="'.$title.'" href="'.($testquestion->TM_STMKQT_Question_Id==$question?'javascript:void(0)':Yii::app()->createUrl('student/mocktest',array('id'=>$id,'questionnum'=>base64_encode('question_'.$testquestion->TM_STMKQT_Question_Id)))).'">'.$count.'</a></li>';
            }
        endforeach;
        return array('pagination'=>$pagination,'questioncount'=>$questioncount,'totalcount'=>$count);
    }


    // view history

    public function actionHistory(){

        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Recepient_Id='.Yii::app()->user->getId().' AND TM_CE_RE_Status=3');
        $challenges = Chalangerecepient::model()->findAll($criteria);

        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_STU_TT_Student_Id='.Yii::app()->user->id.' AND TM_STU_TT_Status=1');
        $totaltests = StudentTest::model()->findAll($criteria);


        $this->render('viewhistory',array('challenges'=>$challenges,'totaltests'=>$totaltests));
    }
    // view history ends
    public function GetTotal($id)
    {
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_AR_Marks) AS totalmarks');
        $criteria->condition='TM_AR_Question_Id=:question';
        $criteria->params=array(':question'=>$id);
        $totalmarks=Answers::model()->find($criteria);
        return $totalmarks->totalmarks;
    }
    public function actionGetpdfAnswer($id)
    {
        $results=StudentQuestions::model()->findAll(
            array(
                'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test',
                'params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)
            )
            );
        $html="<table cellpadding='5' border='0' width='100%'>";

        foreach($results AS $key=>$result):
            $count=$key+1;
            $question=Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
            $html.="<tr><td width='5%'>";
            $html.=$count.".";
            $html.="</td><td width='95%'>";
            $html.=$question->TM_QN_Question;
            $html.="<tr><td width='5%'>";
            $html.="Sol:</td><td width='95%'>";

            $html.=$question->TM_QN_Solutions;
            $html.="</td></tr>";

        endforeach;
        $html.="</table>";


        $mpdf=new mPDF();

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf','D');


    }
    public function actionInvitebuddies()
    {
        if(count($_POST['invitebuddies'])>0):
            for($i=0;$i<count($_POST['invitebuddies']);$i++):
                $recepchalange= new Chalangerecepient();
                $recepchalange->TM_CE_RE_Chalange_Id=$_POST['challengeid'];
                $recepchalange->TM_CE_RE_Recepient_Id=$_POST['invitebuddies'][$i];
                $recepchalange->save(false);
            endfor;
            echo "yes";
        else:
            echo "no";
        endif;
    }
    public function actionShowmockpaper($id)
    {

        $paperquestions=$this->GetPaperQuestions($id);
        $this->render('showpaperquestion',array('id'=>$id,'paperquestions'=>$paperquestions));
    }
}