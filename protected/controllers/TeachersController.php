<?php

class TeachersController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admincolumn2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),*/
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'CreateSchoolTeachers', 'ManageTeachers', 'ViewTeachers', 'EditTeachers', 'DeleteTeachers', 'AddTeacher', 'AddStandards','Home','ImportTeacher','Teacherexport'),
                'users' => UserModule::getAccountAdmins(),
            ),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('CreateSchoolTeachers', 'ManageTeachers', 'ViewTeachers', 'EditTeachers', 'DeleteTeachers', 'AddTeacher', 'AddStandards','Home'),
                'users'=>array('@'),
                'expression'=>'UserModule::isSchoolStaffAdmin($_GET["id"])'
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Home','TeacherPassword','ChangeStandard','createquickhomework','Quickhomework','Listquickhomework','GetGroups','GetTopics','createCustomeHomework','updateCustomeHomework','deleteHomework','Deleteassignedhomework','PublishHomework','Listcustomehomework','managecustomquestions','Getquestions','Addquestion','Removequestion','markhomework','savemarks','Markcomplete','Completeallmarking','sendreminder','Remindall','Solution','Answer','History','SaveStatus','Getmarkers','assignmarking','Staffhome','Previewquestion','Printworksheet','Printsolution','PublishCustomHomework','GetGroupsCustom','Republishhomework','questionstatus','homeworkstatus','ImportTeacher','Mockorder','editmarkcomplete','EditHomework','Changehomeworkdetails','Passwordchange','submitstudent','submitall','Teacherexport','markinglist','listworksheets','createworksheet','updateworksheet','deleteworksheet','Publishworksheet','GetGroupsWorksheet','Printmock','Printmocksolution','Movetohistory','AssignTotal','WorksheetPrintReport','Viewworksheet','Download','GetComments','UploadComments','deleteComment','Reopen','Updateresource','Createresource','Listresources','Publishresource','Printresource','Downloadworksheet','Generateblueprint','viewBlueprint','ChangeSort','Solutionnew','Assessmentreportpdf','CorrectingCompletedStatus','Groups', 'Consolidatedreportbulkdata','GetHeaderCustom','AddSection','Deleteheader','Qstnoptions','linkresources','Linkworksheet','Singleassesmentreportbulk','Studentreopen','Editscreen','Savetag','Getdata','Edittag'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('assessmentreport','Downloadassessment','Getpaperqstns'),
                'users'=>UserModule::getSuperAdmins(),
                ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionHome()
    {
        Yii::app()->session['mode'] = "Teacher";
        $this->layout = '//layouts/teacher';
        $masterschool=UserModule::GetSchoolDetails();
        $model=new Schoolhomework('search');
        $modelmarking=new Homeworkmarking('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Schoolhomework']))
            $model->attributes=$_GET['Schoolhomework'];
        $this->render('teacherhome',array('masterschool'=>$masterschool,'model'=>$model,'modelmarking'=>$modelmarking));

    }

    public function actionChangeSort()
    {
        $sortOrder = $_POST['order'];
         $connection = CActiveRecord::getDbConnection();
        $sql='SELECT TM_CTQ_Id from tm_customtemplate_questions AS t1 INNER JOIN tm_question AS t2 ON t1.TM_CTQ_Question_Id = t2.TM_QN_Id WHERE TM_CTQ_Custom_Id='.$_POST["practiceId"].' ORDER BY TM_QN_Totalmarks '.$sortOrder.'';
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $homeworkquestions=$dataReader->readAll();

         // $homeworkquestions=CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$_POST['practiceId']."'",'order'=>'TM_CTQ_Order ASC'));
        if(count($homeworkquestions)>0)
        {
            // var_dump($homeworkquestions);
            // exit;
            $order= 1;
            foreach($homeworkquestions AS $question)
            {
                // echo $question['TM_CTQ_Id'];exit;
                $question1=CustomtemplateQuestions::model()->findByPk($question['TM_CTQ_Id']);
                // var_dump($question1);exit;
                $question1->TM_CTQ_Order = $order;
                $question1->save(false);
                $order++;
            }
        }

        return true;

    }
    ///code by amal
    public function actionHistory()
    {
        $this->layout = '//layouts/teacher';
        $model=new Schoolhomework('search');
        $this->render('history',array('model'=>$model));
    }

    public function actionReopen($id)
    {
        $schoowhw=Schoolhomework::model()->findByPk($id);
        $schoowhw->TM_SCH_Status=1;
        $schoowhw->save(false);
        SingleHwquestStatistics::model()->deleteAll('TM_SAR_QN_HW_Id = '.$id.'');
        SingleHwStatistics::model()->deleteAll('TM_SAR_HW_Id = '.$id.'');
        SingleHwstudStatistics::model()->deleteAll('TM_SAR_STU_HW_Id = '.$id.'');
        $this->redirect(array('home'));
    }
    //ends
    public function actionCreate()
    {
        $model = new Teachers;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Teachers'])) {
            $model->attributes = $_POST['Teachers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->TM_TH_Id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionCreateSchoolTeachers($id)
    {
        $model = new Teachers;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Teachers'])) {
            $model->attributes = $_POST['Teachers'];
            if ($model->save())

                $this->redirect(array('view', 'id' => $model->TM_TH_Id));
        }

        $this->render('create', array(
            'model' => $model, 'schoolId' => $id
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Teachers'])) {
            $model->attributes = $_POST['Teachers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->TM_TH_Id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Teachers');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Teachers('search');
        /*		$model->unsetAttributes();  // clear any default values
          if(isset($_GET['Teachers']))
              $model->attributes=$_GET['Teachers'];*/

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Teachers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Teachers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Teachers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'teachers-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionManageTeachers($id)
    {
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        $dataProvider = new CActiveDataProvider('User', array(
            'criteria' => array('condition' => 'usertype IN (9,12) AND school_id=' . $id),
            'pagination' => array(
                'pageSize' => 1000,
            ),
        ));
        $this->render('teacherslist', array(
            'dataProvider' => $dataProvider,
            'id' => $id
        ));
    }

    public function actionTeacherexport($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_TH_Name,DATE_FORMAT(a.TM_TH_CreatedOn,'%d-%m-%Y') AS regdate,a.TM_TH_Status,";
        $sql.="a.TM_TH_User_Id,a.TM_TH_Manage_Questions,a.TM_TH_UserType,b.username,b.status,b.createtime,b.usertype,c.TM_SCL_Name,d.firstname,d.lastname,d.phonenumber ";
        $sql.="FROM tm_teachers AS a LEFT JOIN tm_users AS b ON a.TM_TH_User_Id=b.id ";
        $sql.="LEFT JOIN tm_school AS c ON a.TM_TH_SchoolId=c.TM_SCL_Id ";
        $sql.="LEFT JOIN tm_profiles AS d ON a.TM_TH_User_Id=d.user_id WHERE b.usertype IN (9,12) AND a.TM_TH_SchoolId='$id'";

        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataval=$dataReader->readAll();
        foreach($dataval AS $data):
            if($data['status']==0):
                $status="Not Active";
            else:
                $status="Active";
            endif;
            if($data['usertype']==9):
                $usertype="Teacher";
            else:
                $usertype="Teacher Admin";
            endif;
            if($data['TM_TH_Manage_Questions']==1):
                $questions="Yes";
            else:
                $questions="No";
            endif;
            if($data['TM_TH_UserType']==1):
                $type="Professional";
            else:
                $type="Regular";
            endif;
            $standards=$this->getstandards($data['TM_TH_User_Id']);
            $password='';
            $items[]=array($data['firstname'],
                $data['lastname'],
                $data['username'],
                $password,
                $data['TM_SCL_Name'],
                date("d-m-Y",$data['createtime']),
                $status,
                $standards,
                $data['phonenumber'],
                $usertype,
                $questions,
                $type,
            );
        endforeach;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Teachers.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array(
            "Firstname",
            "Lastname",
            "Username",
            "Password",
            "School",
            "Registration date",
            "Status",
            "Standard",
            "Phone Number",
            "Userlevel",
            "Manage Questions",
            "User Type",
        ));
        foreach($items as $row) {
            // display field/column names as first row
            fputcsv($output, $row);
        }
    }

    public function getstandards($id){
        $c = new CDbCriteria;
        $c->select ='t.TM_SD_Id, t.TM_SD_Name';
        $c->join = "INNER JOIN tm_teacher_standards tm_teacher_standards ON t.TM_SD_Id = tm_teacher_standards.TM_TE_ST_Standard_Id";
        $c->compare("tm_teacher_standards.TM_TE_ST_User_Id", $id);

        $result=Standard::model()->findAll($c);

        if(count($result)!=0):
            $standardname='';
            foreach($result AS $key=>$res):
                $standardname.=($key==0?$res->TM_SD_Name:','.$res->TM_SD_Name);
            endforeach;
            $standards= $standardname;
        else:
            $standards= "No standards assigned";
        endif;
        return $standards;
    }

    public function actionViewTeachers($id)
    {

        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        if (isset($_GET['master'])):
            $school = $id;
            $model = User::model()->findByPk($_GET['master']);
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $school . ' AND TM_TH_User_Id=' . $_GET['master']));
            $this->render('teacherview', array(
                'model' => $model,
                'school' => $school,
                'schoolstaff' => $schoolstaff
            ));
        else:
            $this->redirect(array('admin'));
        endif;
    }

    public function actionTeacherPassword($id)
    {
        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        $model = new UserChangePassword;
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$_GET['master']));
        if(isset($_POST['UserChangePassword'])) {
            $model->attributes=$_POST['UserChangePassword'];
            if($model->validate()) {
                $new_password = User::model()->notsafe()->findbyPk($_GET['master']);
                $new_password->password = UserModule::encrypting($model->password);
                $new_password->activkey=UserModule::encrypting(microtime().$model->password);
                $new_password->save(false);
                Yii::app()->user->setFlash('passwordchange',UserModule::t("New password is saved."));
                $this->refresh();
            }
        }
        $this->render('teacherpassword',array('model'=>$model,'teacher'=>$teacher));
    }

    public function actionAddStandards($id)
    {

        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        if (isset($_GET['master'])):
            $school = $id;
            $model = User::model()->findByPk($_GET['master']);
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $school . ' AND TM_TH_User_Id=' . $_GET['master']));
            $schoolstandards = SchoolPlan::model()->findAll(array('condition' => 'TM_SPN_SchoolId=' . $school));
            $techerprevdata=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_Teacher_Id='.$schoolstaff->TM_TH_Id));

            if (isset($_POST['Teachers'])):
                $usertypesselected = $_POST['standards'];

                $teacherId = $_POST['Teachers']['TM_TH_Id'];
                $userId = $_POST['User']['id'];
                $techerprevdata=TeacherStandards::model()->deleteAll(array('condition'=>'TM_TE_ST_Teacher_Id='.$teacherId));
                if(count($usertypesselected)>0):
                    for ($i = 0; $i < count($usertypesselected); $i++):
                        $standards=new TeacherStandards();
                        $standards->TM_TE_ST_Standard_Id=$usertypesselected[$i];
                        $standards->TM_TE_ST_Teacher_Id=$teacherId;
                        $standards->TM_TE_ST_User_Id=$userId;
                        //code by amal
                        if($usertypesselected[$i]!=''):
                            $standards->save(false);
                            $model->status=1;
                            $model->update();
                        else:
                            $model->status=0;
                            $model->update();
                        endif;
						//ends
                    endfor;
                endif;

                $this->redirect(array('ViewTeachers', 'id' => $school,'master'=>$userId));


            endif;
            $this->render('addstandards', array(
                'model' => $model,
                'school' => $school,
                'schoolstaff' => $schoolstaff,
                'techerprevdata' => $techerprevdata
            ));
        else:
            $this->redirect(array('admin'));
        endif;


    }

    public function actionEditTeachers($id)
    {

        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        if (isset($_GET['master'])):
            $school = $id;
            $model = User::model()->notsafe()->findByPk($_GET['master']);
            $profile = $model->profile;
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $school . ' AND TM_TH_User_Id=' . $_GET['master']));
            $teacherstandards=TeacherStandards::model()->count(array('condition'=>'TM_TE_ST_Teacher_Id='.$schoolstaff->TM_TH_Id));
            if (isset($_POST['User'])) {
                $model->attributes = $_POST['User'];
                $model->username=$_POST['User']['email'];
                $model->email=$_POST['User']['email'];
                //code by amal
                if($_POST['User']['type']!=''):
                    $model->usertype = $_POST['User']['type'];
                endif;
                //ends
                if($teacherstandards>0):
                    $model->status = $_POST['User']['status'];
                else:
                    $model->status = '0';
                endif;
                $profile->attributes = $_POST['Profile'];
                $schoolstaff->attributes = $_POST['Teachers'];
                if ($model->validate() & $profile->validate()) {
                    $model->save();
                    $profile->save();
                    $schoolstaff->TM_TH_Name = $profile->firstname . $profile->lastname;
                    $schoolstaff->TM_TH_UpdatedBy = Yii::app()->user->id;
                    $schoolstaff->save(false);
                    $this->redirect(array('manageTeachers', 'id' => $model->school_id));
                }
            }
            $this->render('editteachers', array(
                'model' => $model,
                'profile' => $profile,
                'school' => $school,
                'schoolstaff' => $schoolstaff
            ));
        else:
            $this->redirect(array('admin'));
        endif;
    }

    public function actionDeleteTeachers($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = User::model()->notsafe()->findByPk($_GET['master']);
            $schoolstaff = Teachers::model()->find(array('condition' => 'TM_TH_SchoolId=' . $model->school_id . ' AND TM_TH_User_Id=' . $model->id));
            $schoolstaff->delete();
            $profile = Profile::model()->findByPk($model->id);
            $profile->delete();
            $model->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_POST['ajax']))
                $this->redirect(array('manageTeachers', 'id' => $model->school_id));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionAddTeacher($id)
    {

        if(Yii::app()->user->isSchoolStaffAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        $model = new User;
        $profile = new Profile;
        $staffschool = new Teachers;
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            ///$model->usertype = '9';
            $model->usertype = $_POST['User']['type'];
            $model->school_id = $id;
            $model->username=$_POST['User']['email'];
            $model->email=$_POST['User']['email'];
            $model->userlevel=$_POST['Teachers']['TM_TH_UserType'];
            $profile->attributes = ((isset($_POST['Profile']) ? $_POST['Profile'] : array()));
            $staffschool->attributes = ((isset($_POST['Teachers']) ? $_POST['Teachers'] : array()));
            //$user->scenario='teacher';
            if ($model->validate() & $profile->validate()) {
                $model->activkey = UserModule::encrypting(microtime() . $model->password);
                $model->password = UserModule::encrypting($model->password);
                $model->createtime = time();
                $model->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
                $model->superuser = 0;
                $model->status = '0';
                if ($model->save()) {
                    $profile->user_id = $model->id;
                    $profile->save(false);
                    $staffschool->TM_TH_User_Id = $model->id;
                    $staffschool->TM_TH_Name = $profile->firstname . $profile->lastname;
                    $staffschool->save(false);
                    $password=$_POST['User']['password'];
                    $this->SendTeacherMail($model->id,$password);
                    $this->redirect(array('AddStandards', 'id' => $id,'master'=>$staffschool->TM_TH_User_Id));
                }
            }
        }
        $this->render('createTeacher', array(
            'model' => $model,
            'profile' => $profile,
            'staffschool' => $staffschool,
            'id' => $id
        ));
    }
    public function SendTeacherMail($id,$password)
    {
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$id));
        $user=User::model()->findByPk($teacher->TM_TH_User_Id);
        $userprofile=Profile::model()->findByPk($teacher->TM_TH_User_Id);
        $school=School::model()->findByPk($teacher->TM_TH_SchoolId);
        $adminEmail = Yii::app()->params['noreplayEmail'];
        $email=$user->email;
        $message="<p>Dear $userprofile->firstname,</p>
                    <p>You have been added as teacher to school: $school->TM_SCL_Name. Your login details are as below.</p>
                    <p><div style='border:solid 1px black;background-color: #E6E6FA;width:450px;height: 150px;padding: 20px;text-align: center;'>
                        <p><b>Username </b>: ".$user->username."</p>
                        <p><b>Password </b>: ".$password."</p>
                        <a href='www.tabbiemath.com'>www.tabbiemath.com</a>
                    </div></p>
                    <p>Once you login with these details, you can go into settings and change password at any time.</p>
                    <p>Regards,<br>TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($email);
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Teacher Registration ';
        $mail->Body = $message;

        return $mail->Send();
    }

    public function actionChangeStandard($id)
    {
        Yii::app()->session['standard'] = $id;
        $this->redirect(array('home'));
    }
    public function actionCreatequickhomework()
    {
        $this->layout = '//layouts/teacher';
        $model=new Schoolhomework();
		if(isset($_POST['Schoolhomework']))
		{
			$model->attributes=$_POST['Schoolhomework'];
			if($model->save())
				$this->redirect(array('Quickhomework','id'=>$model->TM_SCH_Id));
		}
        $this->render('createquickhomework',array('model'=>$model,'type'=>'quick'));
    }
    public function actionCreateCustomeHomework()
    {
        $this->layout = '//layouts/teacher';
        $model=new Customtemplate();
		if(isset($_POST['Customtemplate']))
		{
            $model->attributes=$_POST['Customtemplate'];
			if($model->save())
				$this->redirect(array('managecustomquestions','id'=>$model->TM_SCT_Id));
		}
        $this->render('customhomework',array('model'=>$model,'type'=>'custom'));
    }

    public function actionUpdateCustomeHomework($id)
    {
        $this->layout = '//layouts/teacher';
        $model=Customtemplate::model()->findByPk($id);
        if(isset($_POST['Customtemplate']))
		{
			$model->attributes=$_POST['Customtemplate'];
			if($model->save())
				$this->redirect(array('managecustomquestions','id'=>$model->TM_SCT_Id));
		}
        $this->render('customhomeworkupdate',array('model'=>$model));
    }

     public function actionListcustomehomework()
    {
        $userid = Yii::app()->user->id;
        $user=User::model()->find(array('condition'=>'id="'.$userid.'"'));
        $standard=Yii::app()->session['standard'];
        $blueprintschools = BlueprintSchool::model()->findAll(array('condition'=>"TM_BPS_School_Id='".$user->school_id."'AND TM_BPS_Status='0'"));
         $newid=array();
         foreach($blueprintschools as $key=>$blueprintschool){
          //$blueprint[] = Blueprints::model()->findByPk($blueprintschool->TM_BPS_Blueprint_Id);
             $blueprint[] = Blueprints::model()->find(array('condition'=>'TM_BP_Id='.$blueprintschool->TM_BPS_Blueprint_Id.' AND TM_BP_Standard_Id='.$standard.' '));
          $newid[] = $blueprint[$key]->TM_BP_Id;
         }
         $blueprints=new Blueprints('schoolsearch');
         $id = $newid;

        $this->layout = '//layouts/teacher';
        $model=new Customtemplate('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Mock']))
            $model->attributes=$_GET['Mock'];

        $this->render('listcustomehomework',array(
            'model'=>$model,
            'blueprints'=>$blueprints,
            'id'=>$id,
        ));
    }

     public function actionviewBlueprint($id)
    {
        $this->layout = '//layouts/teacher';
        // $bluePrintId = $id;
        $model = Blueprints::model()->find(array('condition'=>'TM_BP_Id='.$id));
        $chapters = Chapter::model()->findAll(array('condition'=>'TM_TP_Standard_Id='.$model->TM_BP_Standard_Id.' AND TM_TP_Status=0', 'order'=>'TM_TP_order ASC'));
        $marks = TmAvailableMarks::model()->findAll(array('condition'=>'TM_AV_MK_Standard_Id='.$model->TM_BP_Standard_Id.' AND status=0'));

        $this->render('viewBlueprint',array(
            'model'=>$model,
            'chapters'=>$chapters,
            'marks'=>$marks,
        ));

        // echo "success";exit;


    }

    public function actiondeleteHomework($id)
    {
        $model=Customtemplate::model()->findByPk($id);
        if($model->delete())
        {
            $questions=CustomtemplateQuestions::model()->deleteAll(array('condition'=>'TM_CTQ_Custom_Id='.$id));
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            if($model->TM_SCH_Type==1):
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listcustomehomework'));
            else:
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listquickhomework'));
            endif;

    }

    public function actionDeleteassignedhomework($id)
    {
        $model=Schoolhomework::model()->findByPk($id);
        if($model):
            if($model->delete())
            {
                $questions=HomeworkQuestions::model()->deleteAll(array('condition'=>'TM_HQ_Homework_Id='.$id));
                $studenthomeworks=StudentHomeworks::model()->deleteAll(array('condition'=>'TM_STHW_HomeWork_Id='.$id));
                $studentquestions=Studenthomeworkquestions::model()->deleteAll(array('condition'=>'TM_STHWQT_Mock_Id='.$id));
                $notifications=Notifications::model()->deleteAll(array('condition'=>"TM_NT_Item_Id='".$id."' AND TM_NT_Type IN ('HWAssign','HWReminder','HWMarking','HWSubmit')"));
    			$homeworkmarking=Homeworkmarking::model()->deleteAll(array('condition'=>'TM_HWM_Homework_Id='.$id));
            }
        endif;

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            if($model->TM_SCH_Type==1):
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listcustomehomework'));
            else:
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listquickhomework'));
            endif;

    }
    public function actionManagecustomquestions($id)
    {
        $this->layout = '//layouts/teacher';
        $model=Customtemplate::model()->findByPk($id);

        $homeworkquestions=CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$id."'", 'order'=>'TM_CTQ_Order ASC'));
        //print_r($homeworkquestions); exit;
        if(count($homeworkquestions)>0):
            $HQS='';
            foreach($homeworkquestions AS $key=>$HQ)
            {
                if($key=='0'):
                    $HQS.="'".$MQ->TM_CTQ_Question_Id."'";
                else:
                    $HQS.=",'".$MQ->TM_CTQ_Question_Id."'";
                endif;
            }
            $addedQuestion=$homeworkquestions;
            $condition="AND TM_QN_Id NOT IN (".$HQS.")";
        else:
            $addedQuestion='';
            $condition='';
        endif;
        //$questions=Questions::model()->findAll(array('condition'=>"TM_QaN_Publisher_Id='".$model->TM_MK_Publisher_Id."' AND TM_QN_Syllabus_Id='".$model->TM_MK_Syllabus_Id."'  AND TM_QN_Parent_Id='0' AND TM_QN_Standard_Id='".$model->TM_MK_Standard_Id."' AND TM_QN_Status='0' ".$condition));
        $this->render('managequestions',array(
            'model'=>$model,
            //'questions'=>$questions,
            'addedQuestion'=>$addedQuestion
        ));
    }
    public function actionGetquestions()
    {
        set_time_limit(500);
        $condition='';
        if($_POST['chapter']!=''):
            //$condition.=" AND TM_SQ_Chapter_Id='".$_POST['chapter']."'";
            $condition.=" AND TM_QN_Topic_Id='".$_POST['chapter']."'";
        endif;
        /*if($_POST['publisher']!=''):
            $condition.=" AND TM_SQ_Publisher_id='".$_POST['publisher']."'";
        endif;*/

        if($_POST['publisher']!=''):
            if($_POST['publisher']=='0'):
                $condition.=" AND TM_SQ_School_Id='".Yii::app()->session['school']."' AND TM_QN_School_Id='".Yii::app()->session['school']."' AND TM_SQ_Type=1";
                //$condition.=" AND TM_SQ_School_Id='".Yii::app()->session['school']."'";
            elseif($_POST['publisher']=='1'):
                $condition.=" AND TM_QN_School_Id='0'";
                //$condition.=" AND TM_SQ_School_Id='".Yii::app()->session['school']."'";
                //$condition.=" AND TM_QN_School_Id='0'";
            endif;
        else:
            $condition.=" AND TM_SQ_School_Id='".Yii::app()->session['school']."'";
        endif;
        if($_POST['topic']!=''):
            //$condition.=" AND TM_SQ_Topic_Id='".$_POST['topic']."'";
            $condition.=" AND TM_QN_Section_Id='".$_POST['topic']."'";
        endif;
        if($_POST['type']!=''):
            $condition.=" AND TM_SQ_Question_Type='".$_POST['type']."'";
        endif;

        if($_POST['difficulty']!=''):
            $condition.=" AND TM_QN_Dificulty_Id='".$_POST['difficulty']."'";
        endif;
        if($_POST['teacher']!=''):
            $condition.=" AND TM_QN_Teacher_Id='".$_POST['teacher']."'";
        endif;
        if($_POST['pattern']!=''):
            $condition.=" AND TM_QN_Pattern LIKE '%".$_POST['pattern']."%'";
        endif;

        if($_POST['reference']!=''):
            $condition.=" AND TM_QN_QuestionReff LIKE '%".$_POST['reference']."%'";
        endif;

         if($_POST['mark']!=''):
            $condition.=" AND TM_QN_Totalmarks LIKE '".$_POST['mark']."'";
        endif;

       if($_POST['skill']!=''):
            $condition.=" AND TM_QN_Skill REGEXP '".$_POST['skill']."'";
        endif;

        if($_POST['question_type'][0]>0):
            $qn_type_ids=array();
            $type_ids=$_POST['question_type'];
            foreach ($type_ids as $key => $value) {
                    $qstntypenexist=Questions::model()->findAll(array('condition'=>"TM_QN_Question_type REGEXP '".$value."'"));
                if(count($qstntypenexist)>0)
                {
                    foreach ($qstntypenexist as $key => $qtypexist) {

                    if(count($qtypexist)>0){

                            $qtypearr=explode(",",$qtypexist->TM_QN_Question_type);
                            $exist=0;
                            foreach($type_ids as $type_id)
                            {
                               if (in_array($type_id, $qtypearr))
                                {
                                    $exist++;
                                }
                            }
                        if($exist==count($type_ids))
                        {
                            if(in_array($qtypexist->TM_QN_Id,$qn_type_ids))
                            {

                            }
                            else
                            {
                              array_push($qn_type_ids,$qtypexist->TM_QN_Id);
                            }
                        }

                    }
                    }
                }
            }
               if(count($qn_type_ids)>0)
                {
                    $question_type_ids=implode(",", $qn_type_ids);
                }
                else
                {
                    $question_type_ids=0;

                }

        $condition.=" AND TM_QN_Id IN (".$question_type_ids.")";
        endif;
        if($_POST['question_info']!=''):
            $condition.=" AND TM_QN_Info LIKE '%".$_POST['question_info']."%'";
        endif;
        $homeworkquestions=CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$_POST['mock']."'"));
        if(count($homeworkquestions)>0):
            $HQS='';
            foreach($homeworkquestions AS $key=>$HQ)
            {
                if($key=='0'):
                    $HQS.=$HQ->TM_CTQ_Question_Id;
                else:
                    $HQS.=",".$HQ->TM_CTQ_Question_Id;
                endif;
            }
            $condition.=" AND TM_SQ_Question_Id NOT IN (".$HQS.")";
        endif;

		// var_dump(Yii::app()->session['standard']);
		//echo $condition;exit;
        ///$questions1=SchoolQuestions::model()->with('question')->findAll(array('condition'=>"TM_SQ_Standard_Id='".Yii::app()->session['standard']."' ".$condition." AND TM_SQ_ImportStatus=0 AND TM_QN_Status IN (0,2) ",'group'=>'TM_QN_Id'));
        //echo $condition;exit;
        $questions=SchoolQuestions::model()->with('question')->findAll(array('condition'=>"TM_QN_Standard_Id='".Yii::app()->session['standard']."' ".$condition." AND TM_SQ_ImportStatus=0 AND TM_QN_Status IN (0,2) ",'group'=>'TM_QN_Id'));
        //var_dump($questions);
        //return;
        //print_r(count($questions));exit;
        //AND TM_QN_Status='0'
		//echo count($questions);exit;
        $questionlist='';
        if(count($questions)>0):
		// var_dump($questions);exit;
            foreach($questions AS $question):
                    if($question->question->TM_QN_Totalmarks==0):
                        $totalmarks=$this->Getmarks($question->question->TM_QN_Id);
                    else:
                        $totalmarks=$question->question->TM_QN_Totalmarks;
                    endif;

                    $skill_ids=explode(",",$question->question->TM_QN_Skill);
                    $skill_names=array();
                    foreach ($skill_ids as $key => $skill_id) {
                    $skill_name = TmSkill::model()->findByPk($skill_id)->tm_skill_name;
                    array_push($skill_names,$skill_name);

                    }
                    $skills=implode(", ",$skill_names);
                    $type_ids=explode(",",$question->question->TM_QN_Question_type);
                    $type_names=array();
                    foreach ($type_ids as $key => $type_id) {
                    $type_name = QuestionType::model()->findByPk($type_id)->TM_Question_Type;
                    array_push($type_names,$type_name);

                    }
                    $types=implode(",",$type_names);
                $questionlist.='<tr id="addquestion'.$question->question->TM_QN_Id.'">
                            <td class="button-column">
                                <a class="previewquestion" title="Preview Question" data-id="'.base64_encode($question->question->TM_QN_Id).'" ><span class="glyphicon glyphicon-eye-open"></span></a>
                                <a class="addquestion" title="Add Question"  data-id="'.base64_encode($question->question->TM_QN_Id).'" data-element="addquestion'.$question->question->TM_QN_Id.'"><span class="glyphicon glyphicon-plus"></span></a>

                            </td>
                         <td>'.$totalmarks.'</td>
                         <td>'.Difficulty::model()->findByPk($question->question->TM_QN_Dificulty_Id)->TM_DF_Name.'</td>
                            <td>'.$question->question->TM_QN_Question.'</td>
                            <td>'.$skills.'</td>
                            <td>'.$types.'</td>
                            <td>'.$question->question->TM_QN_QuestionReff.'</td>
                            <td>'.$question->question->TM_QN_Pattern.'</td>

                        </tr>';
/*                            <td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                            <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>*/

            endforeach;
        else:
            $questionlist='<tr><td colspan="6">No Questions Found</td></tr>';
        endif;

        echo $questionlist;
    }
/*code by amall*/
    public function actionPreviewquestion()
    {
        $questionid=base64_decode($_POST['question']);
        $question=Questions::model()->findByPk($questionid);
        if($question->TM_QN_Type_Id!='5'):
            $datarow="<div class='row'><div class='col-md-12 col-lg-12' style='margin-bottom:10px '>
                    <div class='featured-article'>
                    <div class='col-md-8'>
                    <div class='block-title'>
                    <span>
                    ".$question->TM_QN_Question."
                    </blockquote>
                    </span>
                    </div></div>";
            if($question->TM_QN_Image!=''):
                $datarow.="<div class='col-md-4'>
                        <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=large&type=question&width=400&height=400'."' alt=''>
                        </div>";
            endif;
            $datarow.="</div><br>";
            $datarow.="<div class='col-md-12 col-lg-12'>";
            if($question->TM_QN_Type_Id=='1' || $question->TM_QN_Type_Id=='2' || $question->TM_QN_Type_Id=='3'):
                $datarow.="<ul class='media-list main-list'>";
                foreach($question->answers AS $answer):
                    $datarow.="<li class='media'>";
                    if($answer->TM_AR_Answer!='' & $answer->TM_AR_Image!=''):
                        $datarow.="<span class='pull-right' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span>";
                    endif;
                    $datarow.="<div class='media-body ptp25'>".$answer->TM_AR_Answer."</div>";
                    if($answer->TM_AR_Answer=='' & $answer->TM_AR_Image!=''):
                    $datarow.="<span class='pull-right' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span></li>";
                    endif;
                endforeach;
                $datarow.="<ul>";
            endif;
            $datarow.="</div></div>";
        elseif($question->TM_QN_Type_Id=='5' || $question->TM_QN_Type_Id=='7'):
            $datarow="<div class='row'><div class='col-md-12 col-lg-12' style='margin-bottom:10px '>
                    <div class='featured-article'>
                    <div class='col-md-8'>
                    <div class='block-title'>
                    <span>
                    ".$question->TM_QN_Question."
                    </blockquote>
                    </span>
                    </div></div>";
            if($question->TM_QN_Image!=''):
                $datarow.="<div class='col-md-4'>
                        <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$question->TM_QN_Image.'?size=large&type=question&width=400&height=400'."' alt=''>
                        </div>";
            endif;
            $datarow.="</div></div></div>";
            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
            $countchild=0;
            $childrow='';
            foreach ($questions AS $key=>$childquestion):
                $countchild=$key+1;
                $childrow.="<div class='row'><div class='col-md-12 col-lg-12' style='margin-bottom:10px '>
                    <div class='featured-article'>
                    <div class='col-md-8'>
                    <div class='block-title'>
                    <span>
                    ".$childquestion->TM_QN_Question."
                    </blockquote>
                    </span>
                    </div></div>";
                if($childquestion->TM_QN_Image!=''):
                    $childrow.="<div class='col-md-4'>
                        <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childquestion->TM_QN_Image.'?size=large&type=question&width=400&height=400'."' alt=''>
                        </div>";
                endif;
                $childrow.="</div>";
                $childrow.="<div class='col-md-12 col-lg-12'>";
                if($childquestion->TM_QN_Type_Id=='1' || $childquestion->TM_QN_Type_Id=='2' || $childquestion->TM_QN_Type_Id=='3'):
                    $childrow.="<ul class='media-list main-list parttest".$childquestion->TM_QN_Id."'>";
                    foreach($childquestion->answers AS $childanswer):
                        $childrow.="<li class='media'>";
                        if($childanswer->TM_AR_Answer!='' & $childanswer->TM_AR_Image!=''):
                            $childrow.="<span class='pull-right' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childanswer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span>";
                        endif;
                        $childrow.="<div class='media-body ptp25'>".$childanswer->TM_AR_Answer."</div>";
                        if($childanswer->TM_AR_Answer=='' & $childanswer->TM_AR_Image!=''):
                            $childrow.="<span class='pull-right' style='margin-right: 15px;'>
                                <img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$childanswer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>
                                </span>";
                        endif;
                        $childrow.="</li>";
                    endforeach;
                    $childrow.="</ul>";
                endif;
                $childrow.="</div></div></div>";
            endforeach;
            $datarow.=$childrow;
            //$datarow.="</div>";
        endif;
        $retarray=array('success'=>'yes','result'=>$datarow);
        echo json_encode($retarray);
    }
    /*ends*/

    public function actionAddquestion()
    {
        $mockid=base64_decode($_POST['mock']);
        $questionid=base64_decode($_POST['question']);
        $homeworkquestion=CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."' AND TM_CTQ_Question_Id='".$questionid."'"));
        $homeworkqstcount=count(CustomtemplateQuestions::model()->findAll(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."'")));
        if(count($homeworkquestion)==0)
        {
            $ordernumber=CustomtemplateQuestions::model()->find(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."'",'order'=>'TM_CTQ_Order DESC'));
            $order='';
            if(count($ordernumber)>0):
            $order=$ordernumber->TM_CTQ_Order+1;
            else:
                $order=1;
            endif;
            $question=Questions::model()->findByPk($questionid);
            $questionhomework=new CustomtemplateQuestions();
            $questionhomework->TM_CTQ_Custom_Id=$mockid;
            $questionhomework->TM_CTQ_Question_Id=$questionid;
            $questionhomework->TM_CTQ_Type=$question->TM_QN_Type_Id;
            $questionhomework->TM_CTQ_Order=$order;
            $questionhomework->save(false);

                    $skill_ids=explode(",",$question->TM_QN_Skill);
                    $skill_names=array();
                    foreach ($skill_ids as $key => $skill_id) {
                    $skill_name = TmSkill::model()->findByPk($skill_id)->tm_skill_name;
                    array_push($skill_names,$skill_name);

                    }
                    $skills=implode(", ",$skill_names);

                    $type_ids=explode(",",$question->TM_QN_Question_type);
                    $type_names=array();
                    foreach ($type_ids as $key => $type_id) {
                    $type_name = QuestionType::model()->findByPk($type_id)->TM_Question_Type;
                    array_push($type_names,$type_name);

                    }
                    $types=implode(",",$type_names);
            if($question['TM_QN_School_Id']!=0):
                $editbtn='<a class="editquestion" style="color: #848484 !important;" title="Edit Question" href="'.Yii::app()->createUrl('schoolQuestions/update',array("id"=>Yii::app()->session['school'],"question"=>$question->TM_QN_Id,"assignment"=>$mockid)).'"><span class="glyphicon glyphicon-pencil"></span></a>';
            else:
                $editbtn='<a class="editquestion" style="color: #848484 !important;" title="Edit Question" href="'.Yii::app()->createUrl('schoolQuestions/copyquestion',array("id"=>Yii::app()->session['school'],"assignment"=>$mockid,"question"=>$question->TM_QN_Id)).'"><span class="glyphicon glyphicon-pencil"></span></a>';
            endif;
            $returnrow='<tr id="removequestion'.$question->TM_QN_Id.'"class="order'.$questionhomework->TM_CTQ_Order.'qn" data-rawid="'.$question->TM_QN_Id.'">
                        <td>
                        <INPUT TYPE="image" id="orderlimkup'.$question->TM_QN_Id.'" class="order_link" data-info="up" SRC="'.Yii::app()->request->baseUrl.'/images/up.gif" ALT="SUBMIT" data-id="'.$questionhomework->TM_CTQ_Custom_Id.'" data-order="'.$questionhomework->TM_CTQ_Order.'" data-qnId="'.$question->TM_QN_Id.'">
                        <INPUT TYPE="image" id="orderlimkdown'.$question->TM_QN_Id.'" class="order_link" data-info="down" SRC="'.Yii::app()->request->baseUrl.'/images/down.gif" ALT="SUBMIT" data-id="'.$questionhomework->TM_CTQ_Custom_Id.'" data-order="'.$questionhomework->TM_CTQ_Order.'" data-qnId="'.$question->TM_QN_Id.'">
                        </td>
                        <td class="td-actions">
                            <a class="removequestion" style="color: #848484 !important;" title="Remove Question"  data-id="'.base64_encode($question->TM_QN_Id).'" data-element="removequestion'.$question->TM_QN_Id.'"><span class="glyphicon glyphicon-remove"></span></a>
                            <a href="javascript:void(0)" style="color: #848484 !important;" class="sectionheader" data-backdrop="static" data-toggle="modal" data-target="#SelectHeader" data-id="'.$mockid.'" data-value="'.$question->TM_QN_Id.'" ><span style="font-size: 13px;" class="glyphicon glyphicon-header"></span></a>
                            <label class="switch" title="MCQ">
                            <input  class="save" id="saveoptions" type="checkbox" data-id="'.$mockid.'" data-value="'.$question->TM_QN_Id.'" name="option" >
                            <span class="slider rounded"></span>
                            </label>
                            '.$editbtn.'
                        </td>
                        <td>'.$question->TM_QN_Question.'</td>
                        <td class="marks">'.$this->Getmarks($question->TM_QN_Id).'</td>
                        <td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                        <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>
                         <td>'.$skills.'</td>
                          <td>'.$types.'</td>
                        <td>'.$question->TM_QN_QuestionReff.'</td>
                        <td>'.$question->TM_QN_Pattern.'</td>
                    </tr>';
            /*<td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                        <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>*/
            $count=CustomtemplateQuestions::model()->count(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."'"));
            $retarray=array('success'=>'yes','insertrow'=>$returnrow,'count'=>$count);
            echo json_encode($retarray);
        }
        else
        {
            echo "Question Already Added";
        }
    }

    public function actionRemovequestion()
    {
        if(isset($_POST['mock']))
        {
            $mockid=base64_decode($_POST['mock']);
            $questionid=base64_decode($_POST['question']);
            $mockquestion=CustomtemplateQuestions::model()->find(array('condition'=>'TM_CTQ_Custom_Id='.$mockid.' AND TM_CTQ_Question_Id='.$questionid));
            $questionorder=$mockquestion->TM_CTQ_Order;
            if($mockquestion->delete(false))
            {
                $ordermock=CustomtemplateQuestions::model()->findAll(array('condition'=>'TM_CTQ_Custom_Id='.$mockid.' AND TM_CTQ_Order >'.$questionorder,'order'=>'TM_CTQ_Order ASC'));
                $ordermockcount=CustomtemplateQuestions::model()->count(array('condition'=>'TM_CTQ_Custom_Id='.$mockid));
                $neworder=$questionorder;
                 foreach($ordermock AS $order):
                        $order->TM_CTQ_Order=$neworder;
                        $order->save(false);
                        $neworder++;
                 endforeach;
                $retarray=array('success'=>'yes','count'=>$ordermockcount);
                echo json_encode($retarray);
            }
            else
            {
                $retarray=array('success'=>'no');
                echo json_encode($retarray);
            }
        }
    }

    public function actionQuickhomework()
    {
        $this->layout = '//layouts/teacher';
        $standard=Yii::app()->session['standard'];
        $syllabus=Yii::app()->session['syllabus'];
        $school=Yii::app()->session['school'];
        $standardnew=$_POST['standard'];
        $chapters = Chapter::model()->findAll(array('condition' => "TM_TP_Syllabus_Id='$syllabus' AND TM_TP_Standard_Id='$standard' AND TM_TP_Status=0",'order'=>'TM_TP_order ASC'));

        if (isset($_POST['name'])):


            $criteria = new CDbCriteria;
            $criteria->addInCondition('TM_TP_Id', $_POST['quickchapter']);
            $selectedchapters = Chapter::model()->findAll($criteria);
            $schoolhw=new Schoolhomework();
            $schoolhw->TM_SCH_Name=$_POST['name'];
            $schoolhw->TM_SCH_School_Id=$school;
            $schoolhw->TM_SCH_Standard_Id=$standardnew;
            $schoolhw->TM_SCH_Master_Stand_Id=$standard;
            $schoolhw->TM_SCH_Syllabus_Id=$syllabus;
            $schoolhw->TM_SCH_Type=0;
            $schoolhw->TM_SCH_Status=0;
            $schoolhw->TM_SCH_CreatedBy=Yii::app()->user->id;
            $schoolhw->TM_SCH_ShowSolution=$_POST['solution'];
            $schoolhw->TM_SCH_ShowMark=$_POST['mark'];
            $schoolhw->save(false);
            $count = 1;
            $totalquestions = 0;
            foreach ($selectedchapters AS $key => $chapter):

                if ($_POST['quickchaptertotal' . $chapter->TM_TP_Id] != '0'):
                    $quicklimits = $this->GetQuickLimits($_POST['quickchaptertotal' . $chapter->TM_TP_Id]);
                    $topics = '';
                    for ($i = 0; $i < count($_POST['quicktopic' . $chapter->TM_TP_Id]); $i++):
                        $topics = $topics . ($i == 0 ? $_POST['quicktopic' . $chapter->TM_TP_Id][$i] : "," . $_POST['quicktopic' . $chapter->TM_TP_Id][$i]);
                    endfor;
                    //$topics='"'.$topics.'"';
                    $totalquestions = $totalquestions + $_POST['quick' . $chapter->TM_TP_Id];
                    if ($quicklimits['basic'] != 0):
                        $Syllabus = $syllabus;
                        $Standard = $standard;
                        $Chapter = $chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $homework = $schoolhw->TM_SCH_Id;
                        $dificulty = '1';
                        $limit = $quicklimits['basic'];
                        $command = Yii::app()->db->createCommand("CALL NewSetHWQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Homework,:dificulty,:questioncount,:school,'0',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit", $limit);
                        $command->bindParam(":Homework", $homework);
                        $command->bindParam(":dificulty", $dificulty);
                        $command->bindParam(":questioncount", $count);
                        $command->bindParam(":school", $school);
                        $command->query();
                        $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                    if ($quicklimits['intermediate'] != 0):
                        $Syllabus = $syllabus;
                        $Standard = $standard;
                        $Chapter = $chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $homework = $schoolhw->TM_SCH_Id;
                        $dificulty = '2';
                        $limit = $quicklimits['intermediate'];
                        $command = Yii::app()->db->createCommand("CALL NewSetHWQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Homework,:dificulty,:questioncount,:school,'0',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit", $limit);
                        $command->bindParam(":Homework", $homework);
                        $command->bindParam(":dificulty", $dificulty);
                        $command->bindParam(":questioncount", $count);
                        $command->bindParam(":school", $school);
                        $command->query();
                        $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                    endif;
                    if ($quicklimits['advanced'] != 0):
                        $Syllabus = $syllabus;
                        $Standard = $standard;
                        $Chapter = $chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $homework = $schoolhw->TM_SCH_Id;
                        $dificulty = '3';
                        $limit = $quicklimits['advanced'];
                        $command = Yii::app()->db->createCommand("CALL NewSetHWQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Homework,:dificulty,:questioncount,:school,'0',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit", $limit);
                        $command->bindParam(":Homework", $homework);
                        $command->bindParam(":dificulty", $dificulty);
                        $command->bindParam(":questioncount", $count);
                        $command->bindParam(":school", $school);
                        $command->query();
                        $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                endif;
            endforeach;

            if ($count != '0'):
                $command = Yii::app()->db->createCommand("CALL SetHWQuestionsNum(:Homework)");
                $command->bindParam(":Homework", $schoolhw->TM_SCH_Id);
                $command->query();
                $this->PublishHomework($schoolhw->TM_SCH_Id,$_POST['assigngroups'],$_POST['duedate'],$_POST['comments'],$_POST['publishdate'],$_POST['type'],$_POST['solution']);
                $this->redirect(array('Home'));
            endif;

        endif;
        $this->render('quickhomework', array('chapters' => $chapters));
    }

    public function actionListquickhomework()
    {
        $this->layout = '//layouts/teacher';
        $model=new Schoolhomework('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Mock']))
            $model->attributes=$_GET['Mock'];
        $this->render('listquickhomework',array(
            'model'=>$model,
        ));
    }

    public function actionPublishHomework()
    {
        $school=Yii::app()->session['school'];
        $homeworkid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
		$homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                //$grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                $grpstudents=GroupStudents::model()->findAll(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'TM_GRP_Id='.$grpid.' AND b.status=1'));
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
                    if($_POST['publishdate']==date('Y-m-d')):
                        $notification=new Notifications();
                        $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                        $notification->TM_NT_Type='HWAssign';
                        $notification->TM_NT_Item_Id=$homeworkid;
                        $notification->TM_NT_Target_Id=Yii::app()->user->id;
                        $notification->TM_NT_Status='0';
                        $notification->save(false);
                    endif;
                    $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                    foreach($hwquestions AS $hwquestion):
                        $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                        $studhwqstns=New Studenthomeworkquestions();
                        $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                        $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                        $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                        $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                        $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                        $studhwqstns->save(false);
                        $questions=Questions::model()->findAllByAttributes(array(
                            'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                        ));
                        //print_r($questions);
                        if(count($questions)!=0):
                            foreach($questions AS $childqstn):
                                $studhwqstns=New Studenthomeworkquestions();
                                $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                $studhwqstns->save(false);
                            endforeach;
                        endif;
                    endforeach;
                    $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhomework->save(false);
                endforeach;
                $this->Addhomeworkgroup($homeworkid,$grpid);

            }
        endif;
            $homework = Schoolhomework::model()->findByPk($homeworkid);
            $homework->TM_SCH_Status='1';
            $homework->TM_SCH_DueDate=$_POST['dudate'];
            $homework->TM_SCH_Comments=$_POST['comments'];
            $homework->TM_SCH_PublishDate=$_POST['publishdate'];
            $homework->TM_SCH_PublishType=$_POST['type'];
            $homework->TM_SCH_ShowSolution=$_POST['solution'];
			$homework->TM_SCH_CreatedBy=Yii::app()->user->id;
            $homework->save(false);
    }
    public function GetQuickLimits($total)
    {
        $mode = $total % 3;
        if ($mode == 0):
            $count = $total / 3;
            return array('basic' => $count, 'intermediate' => $count, 'advanced' => $count);
        elseif ($mode == 1):
            $count = round($total / 3);
            return array('basic' => $count, 'intermediate' => $count + 1, 'advanced' => $count);
        elseif ($mode == 2):
            $count = $total / 3;
            return array('basic' => ceil($count), 'intermediate' => ceil($count), 'advanced' => floor($count));
        endif;

    }

    public function GetChapterTotal($chapter, $type)
    {
        $criteria = new CDbCriteria;
        //$criteria->condition = 'TM_QN_Topic_Id=:chapter AND TM_QN_Status=0 AND TM_QN_Parent_Id=0 AND TM_QE_Exam_Id=' . $type;
        $criteria->condition = 'TM_QN_Topic_Id=:chapter AND TM_QN_Parent_Id=0 AND TM_QE_Exam_Id=' . $type;
        $criteria->params = array(':chapter' => $chapter);
        $questions = Questions::model()->with('exams')->count($criteria);
        return $questions;
    }

    public function actionGetTopics()
    {
        $arr = explode(',', $_POST['topicids']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_TP_Id', $arr);
        $model = Chapter::model()->findAll($criteria);
        $topic = '';
        foreach ($model as $key => $value) {
            $topic = ($key == '0' ? $value->TM_TP_Name : $topic . ',' . $value->TM_TP_Name);
        }
        echo $topic;
    }

/*    public function actionGetGroups()
    {
        $groups=SchoolGroups::model()->findAll(array('condition' => "TM_SCL_Id='".$_POST['id']."'"));
        $count=count($groups);
        echo $count;
    }
*/

    public function actionGetGroups()
    {
        $groups=SchoolGroups::model()->findAll(array('condition' => "TM_SCL_Id='".$_POST['id']."'"));
        $count=count($groups);
        /*$paperquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='".$_POST['homeworkid']."' AND TM_HQ_Type IN(6,7)"));
        $papercount=count($paperquestions);
        $onlineqstns=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='".$_POST['homeworkid']."' AND TM_HQ_Type NOT IN(6,7)"));
        $onlinecount=count($onlineqstns);
        if($papercount!=0 & $onlinecount==0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' disabled>Online & Worksheet</label>";
        elseif($papercount!=0 & $onlinecount!=0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online & Worksheet</label>";
        else:
            $data="<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet Only</label><label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online & Worksheet</label>";
        endif;*/
        $schoolhw=Schoolhomework::model()->findByPk($_POST['homeworkid']);
        $ondate=$schoolhw->TM_SCH_PublishDate;
        $name=$schoolhw->TM_SCH_Name;
        $duedate=$schoolhw->TM_SCH_DueDate;
        $type=$schoolhw->TM_SCH_PublishType;
        if($type==1):
            $data="<label class='radio-inline'><input type='radio' name='type' value='2'>Online </label><label class='radio-inline'><input type='radio' class='showsolution' name='type' value='3' >Worksheet </label><label class='radio-inline'><input type='radio' name='type' value='1' checked='checked'>Worksheet (with options)</label>";
        elseif($type==3):
            $data="<label class='radio-inline'><input type='radio' name='type' value='2' >Online </label><label class='radio-inline' ><input type='radio' class='showsolution' name='type' value='3' checked='checked'>Worksheet</label><label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet with options </label>";
        else:
            $data="<label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online </label><label class='radio-inline'><input type='radio' class='showsolution' name='type' value='3' >Worksheet</label><label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet (with options)</label>";
        endif;
        $solution=$schoolhw->TM_SCH_ShowSolution;
        if($solution==1):
            $sol="<label class='radio-inline'><input type='radio' name='solution' value='1' checked='checked'>Yes</label><label class='radio-inline'><input type='radio' name='solution' value='0'>No</label>";
        else:
            $sol="<label class='radio-inline'><input type='radio' name='solution' value='1'>Yes</label><label class='radio-inline'><input type='radio' name='solution' value='0' checked='checked'>No</label>";
        endif;
        $comments=$schoolhw->TM_SCH_Comments;
        $arr=array('result' => $data,'count'=>$count,'name'=>$name,'ondate'=>date('Y-m-d'),'duedate'=>date("Y-m-d", strtotime('tomorrow')),'solution'=>$sol,'comments'=>$comments);
        echo json_encode($arr);
    }
   public function actionGetGroupsCustom()
    {
        $groups=SchoolGroups::model()->findAll(array('condition' => "TM_SCL_Id='".$_POST['id']."'"));
        $count=count($groups);
        $paperquestions=CustomtemplateQuestions::model()->findAll(array('condition' => "TM_CTQ_Custom_Id='".$_POST['homeworkid']."' AND TM_CTQ_Type IN(6,7)"));
        $papercount=count($paperquestions);
        $onlineqstns=CustomtemplateQuestions::model()->findAll(array('condition' => "TM_CTQ_Custom_Id='".$_POST['homeworkid']."' AND TM_CTQ_Type NOT IN(6,7)"));
        $onlinecount=count($onlineqstns);
        if($papercount!=0 & $onlinecount==0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='2' disabled>Online </label><label class='radio-inline'><input type='radio' class='showsolutio' name='type' value='3' >Worksheet</label><label class='radio-inline'><input type='radio' name='type' value='1' checked='checked'>Worksheet (with options)</label>";
        elseif($papercount!=0 & $onlinecount!=0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online </label><label class='radio-inline'><input type='radio' class='showsolutio' name='type' value='3' >Worksheet</label><label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet (with options)</label>";
        else:
            $data="<label class='radio-inline'><input type='radio' name='type' value='2' checked='checked'>Online </label><label class='radio-inline'><input type='radio' class='showsolutio' name='type' value='3' >Worksheet</label><label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet (with options)</label>";
        endif;
        //<option value='0'>Online only</option>
        $arr=array('result' => $data,'count'=>$count);
        echo json_encode($arr);
    }

    public function actionGetGroupsWorksheet()
    {
        $groups=SchoolGroups::model()->findAll(array('condition' => "TM_SCL_Id='".$_POST['id']."'"));
        $count=count($groups);
        $paperquestions=MockQuestions::model()->findAll(array('condition' => "TM_MQ_Mock_Id='".$_POST['homeworkid']."' AND TM_MQ_Type IN(6,7)"));
        $papercount=count($paperquestions);
        $onlineqstns=MockQuestions::model()->findAll(array('condition' => "TM_MQ_Mock_Id='".$_POST['homeworkid']."' AND TM_MQ_Type NOT IN(6,7)"));
        $onlinecount=count($onlineqstns);
        $total=$onlinecount+$papercount;
        if($papercount!=0 & $onlinecount!=0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='2' >Online </label><label class='radio-inline'><input type='radio' checked='checked' class='showsolutio' name='type' value='3' >Worksheet</label>";
			//<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet (with options)</label>
        elseif($papercount!=0 & $onlinecount==0):
            $data="<label class='radio-inline'><input type='radio' name='type' value='2' disabled>Online </label><label class='radio-inline'><input type='radio' checked='checked' class='showsolutio' name='type' value='3' >Worksheet</label>";
        else:
             $data="<label class='radio-inline'><input type='radio' name='type' value='2'>Online </label><label class='radio-inline'><input type='radio' class='showsolutio' name='type' value='3' checked='checked'>Worksheet</label>";
			 //<label class='radio-inline'><input type='radio' name='type' value='1'>Worksheet (with options)</label>
        endif;
        ///<option value='0'>Online only</option>
        $arr=array('result' => $data,'count'=>$count);
        echo json_encode($arr);
    }
    public function actionGroups()
    {
        //echo $_POST['standard'];exit;
        $id=Yii::app()->session['school'];
        if ($_POST['standard'] != '0'):
            $planmodel = SchoolPlan::model()->find(array('condition' => 'TM_SPN_StandardId=' . $_POST['standard'] . ' AND TM_SPN_SchoolId=' . Yii::app()->session['school']));
        else:
            $planmodel = SchoolPlan::model()->find(array('condition' => 'TM_SPN_StandardId=' . Yii::app()->session['standard'] . ' AND TM_SPN_SchoolId=' . $id));
        endif;
        $plan=$planmodel->TM_SPN_PlanId;
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_SCL_Id=' . $id . ' AND TM_SCL_PN_Id= '.$plan;
        $groups = SchoolGroups::model()->findAll($criteria);
        $groupsjson = '';
        $grouparray=array();
        if (count($groups) > 0):
            //$groupsjson .='{"id":0, "name":"All"}';
            foreach ($groups AS $key => $group):
                ///$groupstuents=GroupStudents::model()->count(array('condition'=>'TM_GRP_Id='.$group->TM_SCL_GRP_Id.''));
                $groupstuents=GroupStudents::model()->count(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'t.TM_GRP_Id='.$group->TM_SCL_GRP_Id.' AND b.status=1'));
                if($groupstuents>0)
                {
                    $groupsjson .= ($groupsjson == '' ? '{"id":' . $group->TM_SCL_GRP_Id . ', "name":"' . $group->TM_SCL_GRP_Name . ' "}' : ',{"id":' . $group->TM_SCL_GRP_Id . ', "name":"' . $group->TM_SCL_GRP_Name . ' "}');
                    $grouparray[]=array("id"=>$group->TM_SCL_GRP_Id,"name"=>$group->TM_SCL_GRP_Name);
                }
            endforeach;
        endif;
        echo json_encode($grouparray);
    }
    public function actionGetmarkers()
    {
        //$users=User::model()->count(array('join'=>'LEFT JOIN tm_homeworkmarking AS b ON TM_HWM_User_Id=t.id','condition'=>"usertype='11' AND school_id=".$_POST['id']." AND status=1 AND b.TM_HWM_Homework_Id=".$_POST['homework']));
        $users=User::model()->count(array('condition'=>"usertype IN (11,6,9,12) AND school_id=".$_POST['id']." AND status=1"));
        $data['count']=$users;
        if($users!==0):
            $usersassigned=User::model()->findAll(array('join'=>'LEFT JOIN tm_homeworkmarking AS b ON TM_HWM_User_Id=t.id','condition'=>"usertype IN (11,6,9,12) AND school_id=".$_POST['id']." AND status=1 AND b.TM_HWM_Homework_Id=".$_POST['homework']));
            $users='';
            foreach($usersassigned AS $key=>$assigned):
                $users.=($key=='0'?$assigned->profile->firstname.' '.$assigned->profile->lastname:','.$assigned->profile->firstname.' '.$assigned->profile->lastname);
            endforeach;
            $data['users']=$users;
        endif;
        echo json_encode($data);
    }
    public function Groups($id)
    {
        $plan=SchoolGroups::model()->GetPlanId('1');
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_SCL_Id=' . $id . ' AND TM_SCL_PN_Id= '.$plan;
        $groups = SchoolGroups::model()->findAll($criteria);
        $groupsjson = '';
        if (count($groups) > 0):
            //$groupsjson .='{"id":0, "name":"All"}';
            foreach ($groups AS $key => $group):
                //$groupstuents=GroupStudents::model()->count(array('condition'=>'TM_GRP_Id='.$group->TM_SCL_GRP_Id.''));
                $groupstuents=GroupStudents::model()->count(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'t.TM_GRP_Id='.$group->TM_SCL_GRP_Id.' AND b.status=1'));
                if($groupstuents>0)
                {
                    $groupsjson .= ($groupsjson == '' ? '{"id":' . $group->TM_SCL_GRP_Id . ', "name":"' . $group->TM_SCL_GRP_Name . ' "}' : ',{"id":' . $group->TM_SCL_GRP_Id . ', "name":"' . $group->TM_SCL_GRP_Name . ' "}');
                }
            endforeach;
        endif;
        return $groupsjson;
    }
    public function Markers($id)
    {
        $users=User::model()->findAll(array('condition'=>"usertype IN (11,6,9,12) AND school_id=".$id." AND status=1 AND id!=".Yii::app()->user->id,'limit'=>'25'));
        $groupsjson = '';
        if (count($users) > 0):
        foreach ($users AS $key => $user):
            $groupsjson .= ($groupsjson == '' ? '{"id":' . $user->id . ', "name":"' . $user->profile->firstname.' '.$user->profile->lastname. ' ('.$user->username.') "}' : ',{"id":' . $user->id . ', "name":"' . $user->profile->firstname.' '.$user->profile->lastname . ' ('.$user->username.') "}');
        endforeach;
        endif;
        return $groupsjson;
    }
    /** Teacher marking action **/
    public function actionMarkhomework($id)
    {
        $this->layout = '//layouts/teachermarkig';
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','limit'=>5,'order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $startedstudentscount=StudentHomeworks::model()->count(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        ///$notstartedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $notstartedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS users ON users.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5) AND users.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $homework=Schoolhomework::model()->findByPk($id);
        $this->render('teachermarking',array(
            'startedstudents'=>$startedstudents,
            'homework'=>$homework,
            'notstartedstudents'=>$notstartedstudents,
            'total'=>$startedstudentscount,
            'id'=>$id
        ));

    }

    public function HasComments($homework,$student)
    {
        $hwanswers=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_Student_Id='.$student.' AND TM_STHW_HomeWork_Id='.$homework));
        if(count($hwanswers)>0 && $hwanswers->TM_STHW_Comments_File!=''):
            return true;
        else:
            return false;
        endif;
    }

    public function actionGetComments()
    {
        $homeworks=StudentHomeworks::model()->findAll(array('condition'=>'TM_STHW_HomeWork_Id='.$_POST['homework'].' AND TM_STHW_Student_Id='.$_POST['student']));
        $html='';
        foreach($homeworks AS $homework):
            if($homework['TM_STHW_Comments_File']!=''):
                $html.='<div class="well" id="file'.$homework->TM_STHW_Id.'"><a href="'.Yii::app()->request->baseUrl.'/homework/'.$homework->TM_STHW_Comments_File.'" target="_blank"><span class="glyphicon glyphicon-file"></span>'.$homework->TM_STHW_Comments_File.'</a> <a class="deletecomment" data-value="'.$homework->TM_STHW_Id.'" title="delete" style="float:right"><span class="glyphicon glyphicon-trash"></span></a></div>';
            else:
                $html='<div class="well">No files found</div> ';
            endif;
        endforeach;
        if($html==''):
            $html='<div class="well">No files found</div> ';
        endif;
        echo $html;
    }

    public function actionUploadComments()
    {
        $hwid = $_POST['homeworkid'];
        $student = $_POST['studentid'];
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
        $criteria->params = array(':test' => $hwid, 'student' => $student);
        $homework=StudentHomeworks::model()->find($criteria);

        $target_path = "homework/";
        $filename = basename($_FILES['comments']['name']);
        $target_path = $target_path . $filename;
        if (move_uploaded_file($_FILES['comments']['tmp_name'], $target_path)) {
            $homework->TM_STHW_Comments_File = $filename;
        }
        if ($homework->save(false)):

            /*$homework=StudentHomeworks::model()->find($criteria);
            $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
            $homework->TM_STHW_Status = '6';
            $homework->save(false);*/
            $this->redirect(array('markhomework', 'id' => $hwid));
            //$this->redirect(Yii::app()->request->baseUrl."/teachers/markhomework/".$hwid);
        endif;
    }

    public function actiondeleteComment()
    {
        $homeworks=StudentHomeworks::model()->findByPk($_POST['worksheet']);
        $homeworks->TM_STHW_Comments_File='';
        $homeworks->save(false);
        echo "yes";

    }

    public function actionDownload()
    {
        if(isset($_REQUEST["file"])){
            // Get parameters
            $file = urldecode($_REQUEST["file"]); // Decode URL-encoded string
            $filepath = "homework/" . $file;

            // Process download
            if(file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                exit;
            }
        }
    }

    //code by amal
    public function actionSolution()
    {
        $student=$_POST['student'];
        $homework=$_POST['homework'];
        /*$hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $solution='';
        foreach($hwquestions AS $hwq):
            $question=Questions::model()->find(array('condition'=>'TM_QN_Id='.$hwq->TM_STHWQT_Question_Id));
            $solution.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4>$question->TM_QN_Solutions</li>";
        endforeach;
        echo $solution;*/
        $homeworkqstns=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $username=User::model()->findByPk(Yii::app()->user->id)->username;
        $homework=Schoolhomework::model()->findByPK($homework)->TM_SCH_Name;
        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>Homework : ".$homework."<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";
        foreach ($homeworkqstns AS $key => $homeworkqstn):
            $homeworkqstncount = $key + 1;
            $question = Questions::model()->findByPk($homeworkqstn->TM_STHWQT_Question_Id);
            //code by amal
            if($homeworkqstn->TM_STHWQT_Type!=5):
                if($homeworkqstn->TM_STHWQT_Type==4):
                    $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_STHWQT_Question_Id' "));
                    $marks=$answers->TM_AR_Marks;
                else:
                    $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_STHWQT_Question_Id' AND TM_AR_Correct='1' "));
                    $childtotmarks=0;
                    foreach($answers AS $answer):
                        $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                    endforeach;
                    $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                    $marks=$childtotmarks;
                endif;
            endif;
            if($homeworkqstn->TM_STHWQT_Type==5):
                $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_STHWQT_Question_Id' "));
                $childtotmarks=0;
                foreach($childqstns AS $childqstn):
                    //$childhwqstns = Questions::model()->findByPk($childqstn->TM_STHWQT_Question_Id);
                    if($childqstn->TM_QN_Type_Id==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                        $childtotmarks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                    endif;
                endforeach;
                $marks=$childtotmarks;
            endif;
            if($homeworkqstn->TM_STHWQT_Type==6):
                $marks=$question->TM_QN_Totalmarks;
            endif;
            //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $marks . " &nbsp;&nbsp;Ref : ".$question->TM_QN_QuestionReff."</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;

                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html .= "<tr><td colspan='3' style='text-decoration: underline;'>".$childquestion->TM_QN_Question." </td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            foreach($childquestion->answers AS $answer):
                                $html.="<tr><td>$answer->TM_AR_Answer</td></tr>";
                            endforeach;
                        endforeach;
                    endif;
                    $html.="<tr><td colspan='3'><b>Solution</b></td></tr><tr><td colspan='3'>$question->TM_QN_Solutions</td></tr>";
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $displaymark . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $html.="<tr><td colspan='3'><b>Solution</b></td></tr><tr><td colspan='3'>$question->TM_QN_Solutions</td></tr>";
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
        endforeach;
        $html .= "</table>";
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                <td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
                <td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
                <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                </tr></table>';

        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
        <td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Educational Solutions PVT Ltd.</span></td>
        </tr></table>');

        //echo $html;exit;
        ///$name=Yii::app()->request->baseUrl.'/homework/SOLUTION'.time().'.pdf';
        $name='SOLUTION'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output(Yii::app()->basePath.'/../homework/'.$name,'F');
        $answer='<object data="'.Yii::app()->request->baseUrl.'/homework/'.$name.'" type="application/pdf" width="100%" height="100%">
                </object>';
        echo $answer;
        //$mpdf->Output($name, 'F');
    }

    public function actionSolutionnew()
    {
        $student=$_GET['student'];
        $homework=$_GET['homework'];
        /*$hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $solution='';
        foreach($hwquestions AS $hwq):
            $question=Questions::model()->find(array('condition'=>'TM_QN_Id='.$hwq->TM_STHWQT_Question_Id));
            $solution.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4>$question->TM_QN_Solutions</li>";
        endforeach;
        echo $solution;*/
        $homeworkqstns=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $username=User::model()->findByPk(Yii::app()->user->id)->username;
        $homework=Schoolhomework::model()->findByPK($homework)->TM_SCH_Name;
        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>Homework : ".$homework."<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";
        foreach ($homeworkqstns AS $key => $homeworkqstn):
            $homeworkqstncount = $key + 1;
            $question = Questions::model()->findByPk($homeworkqstn->TM_STHWQT_Question_Id);
            //code by amal
            if($homeworkqstn->TM_STHWQT_Type!=5):
                if($homeworkqstn->TM_STHWQT_Type==4):
                    $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_STHWQT_Question_Id' "));
                    $marks=$answers->TM_AR_Marks;
                else:
                    $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_STHWQT_Question_Id' AND TM_AR_Correct='1' "));
                    $childtotmarks=0;
                    foreach($answers AS $answer):
                        $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                    endforeach;
                    $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                    $marks=$childtotmarks;
                endif;
            endif;
            if($homeworkqstn->TM_STHWQT_Type==5):
                $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_STHWQT_Question_Id' "));
                $childtotmarks=0;
                foreach($childqstns AS $childqstn):
                    //$childhwqstns = Questions::model()->findByPk($childqstn->TM_STHWQT_Question_Id);
                    if($childqstn->TM_QN_Type_Id==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                        $childtotmarks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                    endif;
                endforeach;
                $marks=$childtotmarks;
            endif;
            if($homeworkqstn->TM_STHWQT_Type==6):
                $marks=$question->TM_QN_Totalmarks;
            endif;
            //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $marks . " &nbsp;&nbsp;Ref : ".$question->TM_QN_QuestionReff."</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;

                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html .= "<tr><td colspan='3' style='text-decoration: underline;'>".$childquestion->TM_QN_Question." </td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            foreach($childquestion->answers AS $answer):
                                $html.="<tr><td>$answer->TM_AR_Answer</td></tr>";
                            endforeach;
                        endforeach;
                    endif;
                    $html.="<tr><td colspan='3'><b>Solution</b></td></tr><tr><td colspan='3'>$question->TM_QN_Solutions</td></tr>";
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $displaymark . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $html.="<tr><td colspan='3'><b>Solution</b></td></tr><tr><td colspan='3'>$question->TM_QN_Solutions</td></tr>";
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
        endforeach;
        $html .= "</table>";
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                <td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
                <td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
                <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                </tr></table>';

        // $mpdf = new mPDF();

        // $mpdf->SetHTMLHeader($header);
        // $mpdf->SetWatermarkText($username, .1);
        // $mpdf->showWatermarkText = true;

        // $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
        // <td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Educational Solutions PVT Ltd.</span></td>
        // </tr></table>');

        //echo $html;exit;
        ///$name=Yii::app()->request->baseUrl.'/homework/SOLUTION'.time().'.pdf';
        // $name='SOLUTION'.time().'.pdf';
        // $mpdf->WriteHTML($html);
        // $mpdf->Output(Yii::app()->basePath.'/../homework/'.$name,'F');
        // $answer='<object data="'.Yii::app()->request->baseUrl.'/homework/'.$name.'" type="application/pdf" width="100%" height="100%">
        //         </object>';
        // echo $answer;
        //$mpdf->Output($name, 'F');

        $this->render('printpdf',array('html'=>$html));
    }

public function actionAnswer()
    {
        $student=$_POST['student'];
        $homework=$_POST['homework'];
        $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $answer='<ul style="list-style-type: decimal;">';
        foreach($hwquestions AS $hwq):
            $studanswer='';
            $question=Questions::model()->find(array('condition'=>'TM_QN_Id='.$hwq->TM_STHWQT_Question_Id));
            if($hwq['TM_STHWQT_Type']==4):
                $studanswer=$hwq->TM_STHWQT_Answer;
                $correctans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$hwq->TM_STHWQT_Question_Id.''));
                $correctanswer=$correctans->TM_AR_Answer;
			elseif($hwq['TM_STHWQT_Type']==1):
                if($hwq['TM_STHWQT_Status']==2):
                    $studanswer='Skipped';
                else:
                    $studans=explode(',', $hwq->TM_STHWQT_Answer);
                    $studanswer='';
                    foreach($studans AS $studansw):
                        $studentanswer=Answers::model()->findByPk($studansw);
                        $studanswer.=$studentanswer->TM_AR_Answer.'<br>';
                    endforeach;
                endif;
            elseif($hwq['TM_STHWQT_Type']==5):
                $childquestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$hwq->TM_STHWQT_Question_Id'"));
                $childlist='<ul style="list-style-type: decimal;">';
                foreach ($childquestions AS $key => $childs):
                    $childanswer=Studenthomeworkquestions::model()->find(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Question_Id='.$childs->TM_QN_Id.' AND TM_STHWQT_Mock_Id='.$homework,))->TM_STHWQT_Answer;
                    if($childs->TM_QN_Type_Id==4):
                    	$childstudanswer=$childanswer;
                    	//$childcorrectans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$childs->TM_QN_Id.''));
                    	//$childcorrectanswer=$childcorrectans->TM_AR_Answer;
                    elseif($childs->TM_QN_Type_Id==1):
                        $childstudans=explode(',', $childanswer);
                        $childstudanswer='';
                        foreach($childstudans AS $childstudansw):
                            $studentanswer=Answers::model()->findByPk($childstudansw);
                            $childstudanswer.=$studentanswer->TM_AR_Answer.'<br>';
                        endforeach;
                    else:
                    	$childstudentanswer=Answers::model()->findByPk($childanswer);
                    	$childstudanswer=$childstudentanswer->TM_AR_Answer;
                    	$childcorrectans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$childs->TM_QN_Id.' AND TM_AR_Correct=1'));
                    	$childcorrectanswer=$childcorrectans->TM_AR_Answer;
                    endif;
                    $childlist.="<li>$childs->TM_QN_Question </li>".($childstudanswer==''?'<p>Not Answered</p>': '<p><b>Answer</b></p>'.$childstudanswer)."</li>";
				endforeach;
                $childlist.='</ul>';
			else:
                $studentanswer=Answers::model()->findByPk($hwq->TM_STHWQT_Answer);
                $studanswer=$studentanswer->TM_AR_Answer;
                $correctans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$hwq->TM_STHWQT_Question_Id.' AND TM_AR_Correct=1'));
                $correctanswer=$correctans->TM_AR_Answer;
            endif;
            if($hwq['TM_STHWQT_Type']==6 || $hwq['TM_STHWQT_Type']==7):
                $answer.="<li>$question->TM_QN_Question</li>";
            elseif($hwq['TM_STHWQT_Type']==5):
                $answer.="<li>$question->TM_QN_Question".$childlist."</li>";
            else:
                $answer.="<li>$question->TM_QN_Question".($studanswer==''?'<p>Not Answered</p>': '<p><b>Answer</b></p>'.$studanswer)."</li>";
            endif;
        endforeach;
        /*if(count($hwanswers)>0 ||$hwanswers->TM_STHWAR_Filename!=''):
            $answer='<object data="'.Yii::app()->request->baseUrl.'/homework/'.$hwanswers->TM_STHWAR_Filename.'" type="application/pdf" width="100%" height="100%">
                  <p>Click on the link if you face problem Viewing the <a href="'.Yii::app()->request->baseUrl.'/homework/'.$hwanswers->TM_STHWAR_Filename.'">to the PDF!file</a></p>
                </object>';
        else:
            $answer='<p>No Answer sheet uploaded</p>';
        endif;*/
        echo '</ul>'.$answer;
    }
    public function CheckAnswersheetCount($student,$homework)
    {
        $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        if(count($hwanswers)>0 && $hwanswers->TM_STHWAR_Filename!=''):
            return true;
        else:
            return false;
        endif;
    }
    public function GetAnswersheetCount($student,$homework)
    {
        $hwanswers=Studenthomeworkanswers::model()->findAll(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        return $hwanswers;
    }
    public function CheckAnswersheet($student,$homework)
    {
      $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        if(count($hwanswers)>0 ||$hwanswers->TM_STHWAR_Filename!=''):
            return Yii::app()->request->baseUrl.'/homework/'.$hwanswers->TM_STHWAR_Filename;
        else:
            return false;
        endif;
    }
    //endss
    public function actionSavemarks()
    {
        $student=$_POST["student"];
        $homework=$_POST["homework"];
        $totalmarks=0;
        $homeworkdata=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homework.' AND TM_STHW_Student_Id='.$student));
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));

        foreach($hwquestions AS $hwq):
            /*if($hwq->TM_STHWQT_Marks!=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id]):
                $hwq->TM_STHWQT_Marks=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id];
                $hwq->save(false);
            endif;*/
            $hwq->TM_STHWQT_Marks=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id];
            $totalmarks=$totalmarks+$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id];
            $hwq->save(false);
        endforeach;
		$homeworkdata->TM_STHW_Marks=$totalmarks;

		if($homeworkdata->TM_STHW_TotalMarks!=0):
            $percentage = ($totalmarks / $homeworkdata->TM_STHW_TotalMarks) * 100;
        else:
            $percentage=0;
        endif;
		$homeworkdata->TM_STHW_Percentage=$percentage;
		$homeworkdata->save(false);
        echo "saved";
    }
    public function actionMarkcomplete()
    {
        $studentvalue=$_POST["student"];
        $homeworkvalue=$_POST["homework"];
        $homework=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homeworkvalue.' AND TM_STHW_Student_Id='.$studentvalue));
        $homework->TM_STHW_Status='5';
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$studentvalue.' AND TM_STHWQT_Mock_Id='.$homeworkvalue,'order'=>'TM_STHWQT_Order ASC'));
		$totalmarks=0;
        foreach($hwquestions AS $hwq):
            if($hwq->TM_STHWQT_Marks!=$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id]):
                $hwq->TM_STHWQT_Marks=$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id];
                $hwq->save(false);
            endif;
			$totalmarks=$totalmarks+$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id];
        endforeach;
		$homework->TM_STHW_Marks=$totalmarks;
		if($homework->TM_STHW_TotalMarks!=0):
            $percentage = ($totalmarks / $homework->TM_STHW_TotalMarks) * 100;
        else:
            $percentage=0;
        endif;
		$homework->TM_STHW_Percentage=$percentage;
		$homework->save(false);
        echo "saved";
    }
    public function actioneditmarkcomplete()
    {
        $studentvalue=$_POST["student"];
        $homeworkvalue=$_POST["homework"];
        $homework=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homeworkvalue.' AND TM_STHW_Student_Id='.$studentvalue));
        $homework->TM_STHW_Status='4';
        $homework->save(false);
    }
    public function GetAverage($student,$homework)
    {
        $homework=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homework.' AND TM_STHW_Student_Id='.$student));
        return round($homework->TM_STHW_Percentage);
    }
    public function actionSaveStatus($id)
    {
        $model=Customtemplate::model()->findByPk($id);
        if($model->TM_SCT_Status==0):
            $model->TM_SCT_Status=1;
        else:
            $model->TM_SCT_Status=0;
        endif;
        $model->save(false);
        $this->redirect(array('managecustomquestions','id' => $id));

    }
    public function actionCompleteallmarking($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $homework=Schoolhomework::model()->findByPk($id);
        $homeworkgroups = Homeworkgroup::model()->findAll(array('condition' => "TM_HMG_Homework_Id='$id' "));
        $studhws=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_HomeWork_Id='$id' AND TM_STHW_Student_Id!='0'", "order" => "TM_STHW_Status DESC"));
        $wrkgrpids = '';
        $index = 0;
        foreach($homeworkgroups as $homeworkgroup):
            // $group = SchoolGroups::model()->findByPK($homeworkgroup->TM_HMG_Groupe_Id);
            if($index == 0)
                $wrkgrpids .= $homeworkgroup->TM_HMG_Groupe_Id;
            else
                $wrkgrpids .= ','.$homeworkgroup->TM_HMG_Groupe_Id;
            $index++;
        endforeach;
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));

         $notanswereded=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS user ON user.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.'  AND TM_STHW_Status NOT IN (4,5) AND user.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        foreach($startedstudents AS $started):
            $started->TM_STHW_Status='5';
			$hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$started->TM_STHW_Student_Id.' AND TM_STHWQT_Mock_Id='.$id,'order'=>'TM_STHWQT_Order ASC'));
			$totalmarks=0;
			foreach($hwquestions AS $hwq):
				$totalmarks=$totalmarks+$hwq->TM_STHWQT_Marks;
			endforeach;
			$started->TM_STHW_Marks=$totalmarks;
			if($started->TM_STHW_TotalMarks!=0):
				$percentage = ($totalmarks / $started->TM_STHW_TotalMarks) * 100;
			else:
				$percentage=0;
			endif;
			$started->TM_STHW_Percentage=$percentage;
            $started->save(false);
            $pointlevels = $this->AddPoints($started->TM_STHW_Marks, $id, 'Homework', $started->TM_STHW_Student_Id);
        endforeach;
        $homework->TM_SCH_Status='2';
        $homework->TM_SCH_CompletedOn=date('Y-m-d h:i:s');
        $homework->save(false);
        $assignedusers=Homeworkmarking::model()->findAll(array('condition'=>'TM_HWM_Homework_Id='.$id.''));
        foreach($assignedusers AS $assigned):
            $assigned->TM_HWM_Status=1;
            $assigned->save(false);
        endforeach;
        // single assesment report statistics
        $this->singleassesmentreport($id);
        // consolidated assignment report

        // check if exists
        $ifExist = AssessmentStatistics::model()->find(array('condition'=>'schoolhomework_id='.$id));
        if($ifExist)
            $assessmentStatistics = $ifExist;
        else
            $assessmentStatistics = new AssessmentStatistics();

        $assessmentStatistics->date = date('Y-m-d');
        $assessmentStatistics->group_ids = $wrkgrpids;
        $assessmentStatistics->teacher_id = $homework->TM_SCH_CreatedBy;
        $assessmentStatistics->schoolhomework_id = $id;
        $assessmentStatistics->homework_name = $homework->TM_SCH_Name;
        $assessmentStatistics->students_completed = 0;
        $assessmentStatistics->students_not_attempted = 0;
        $assessmentStatistics->average_score = 0;
        if($assessmentStatistics->save(false))
        {
            $notanswered = count($notanswereded);
            $assessmentStatistics->students_completed = count($startedstudents);
            $assessmentStatistics->students_not_attempted = $notanswered;
            $assessmentStatistics->save(false);
            // homeworkquestions
            $questions=HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$id));
            $totalMarks = 0;
            $totalScored = 0;
            if(count($questions) > 0)
            {
                foreach ($questions as $question) {
                    $totalAttempted = 0;
                    $totalCorrected = 0;
                    $totalstudentscorrect = [];
                    $questMaster = Questions::model()->findByPk($question->TM_HQ_Question_Id);
                    if ($questMaster->TM_QN_Type_Id != '5')
                    {
                     /*   $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$questMaster->TM_QN_Id."' AND TM_STHWQT_Student_Id!='0' AND TM_STHWQT_Marks='".$questMaster->TM_QN_Totalmarks."'"));*/


        $sql42 = "SELECT * FROM tm_studenthomeworkquestions AS a  INNER JOIN  tm_student_homeworks AS b ON b.TM_STHW_Student_Id=a.TM_STHWQT_Student_Id
                        WHERE a.TM_STHWQT_Mock_Id ='".$id."' AND b.TM_STHW_HomeWork_Id ='".$id."' AND a.TM_STHWQT_Question_Id = '".$questMaster->TM_QN_Id."' AND TM_STHWQT_Student_Id!=0 AND b.TM_STHW_Status IN (4,5) AND TM_STHWQT_Marks='".$questMaster->TM_QN_Totalmarks."'";

                        $command=$connection->createCommand($sql42);
                        $dataReader=$command->query();
                        $totalstudentscorrect=$dataReader->readAll();
                    }
                    else
                    {


                        $sql4 = "SELECT *,SUM(a.TM_STHWQT_Marks) AS totalGained FROM tm_studenthomeworkquestions AS a
                        LEFT JOIN  tm_question AS b ON b.TM_QN_Id=a.TM_STHWQT_Parent_Id
                        WHERE a.TM_STHWQT_Mock_Id ='".$id."' AND a.TM_STHWQT_Parent_Id = '".$questMaster->TM_QN_Id."' GROUP BY a.TM_STHWQT_Parent_Id,a.TM_STHWQT_Student_Id HAVING  totalGained = b.TM_QN_Totalmarks";

                        $command=$connection->createCommand($sql4);
                        $dataReader=$command->query();
                        $totalstudentscorrect=$dataReader->readAll();

                    }
                    $totalScored=0;
                    foreach ($startedstudents as $key => $startedstudent) {

                       $totalScored = $totalScored+$startedstudent->TM_STHW_Marks;
                    }

                    $totalMarks += $questMaster->TM_QN_Totalmarks * count($startedstudents);
                    //$totalScored += $questMaster->TM_QN_Totalmarks * count($totalstudentscorrect);
                    // consolidated report questions
                    // check if question already exists
                    $ifQExist = AssesmentStatisticsQuestions::model()->find(array('condition'=>'schoolhomework_id='.$id.' AND question_id ='.$question->TM_HQ_Question_Id.' AND assessment_statistics_id='.$assessmentStatistics->id));
                    if($ifQExist)
                        $consolidatedQuestion = $ifQExist;
                    else
                        $consolidatedQuestion = new AssesmentStatisticsQuestions();

                    $consolidatedQuestion->chapter_id = $questMaster->TM_QN_Topic_Id;
                    $consolidatedQuestion->topic_id = $questMaster->TM_QN_Section_Id;
                    $consolidatedQuestion->schoolhomework_id = $id;
                    $consolidatedQuestion->assessment_statistics_id = $assessmentStatistics->id;
                    $consolidatedQuestion->parent = 0;
                    $consolidatedQuestion->question_id = $question->TM_HQ_Question_Id;
                    $consolidatedQuestion->type = $question->TM_HQ_Type;
                    $consolidatedQuestion->total_students = count($studhws);
                    $consolidatedQuestion->total_attempted = count($startedstudents);
                    $consolidatedQuestion->total_corrected = count($totalstudentscorrect);
                    $consolidatedQuestion->save(false);
                }
            }
            if($totalMarks != 0){
                $assessmentStatistics->average_score = ($totalScored/$totalMarks)* 100;
                $assessmentStatistics->save(false);
            }
        }
        if(Yii::app()->user->isTeacher()):
            $this->redirect(array('home'));
        elseif(Yii::app()->user->isMarker()):
            $this->redirect(array('staffhome'));
        endif;

    }

    public function singleassesmentreport($id)
    {
        $ifExist = SingleHwStatistics::model()->find(array('condition'=>'TM_SAR_HW_Id='.$id));
        if($ifExist)
            $singleassesment = $ifExist;
        else
            $singleassesment = new SingleHwStatistics();

        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$id'",'order'=>'TM_HQ_Order ASC'));
        $totalquestions= count($hwquestions);
        $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$id' "));
        $totalMarks=$studhw->TM_STHW_TotalMarks;
        $studhws=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $totalanswered = count($studhws);
        $notanswered=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS user ON user.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.'  AND TM_STHW_Status NOT IN (4,5) AND user.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $totalnotanswered = count($notanswered);
        $singleassesment->TM_SAR_HW_Id=$id;
        $singleassesment->TM_SAR_Total_Questions=$totalquestions;
        $singleassesment->TM_SAR_Total_Marks=$totalMarks;
        $singleassesment->TM_SAR_Atempted=$totalanswered;
        $singleassesment->TM_SAR_Not_Attempted=$totalnotanswered;
        $singleassesment->save(false);
        if($totalanswered>0):
            foreach($studhws as $studhwdata):
                $studentdetails=Student::model()->find(array("condition"=>"TM_STU_User_Id='".$studhwdata->TM_STHW_Student_Id."'"));
                $studentname=$studentdetails->TM_STU_First_Name." ".$studentdetails->TM_STU_Last_Name;

                $actualpercentage = $studhwdata->TM_STHW_Percentage;
                if (strpos($actualpercentage, '.')!== false)
                 {
                    $finalpercentage = number_format($studhwdata->TM_STHW_Percentage,2);
                }
                else{
                    $finalpercentage = $actualpercentage;
                }
                $studentassesment = new SingleHwstudStatistics();
                $studentassesment->TM_SAR_STU_HW_Id=$id;
                $studentassesment->TM_SAR_STU_Student_Id=$studhwdata->TM_STHW_Student_Id;
                $studentassesment->TM_SAR_STU_Name=$studentname;
                $studentassesment->TM_SAR_STU_TotalMarks=$studhwdata->TM_STHW_Marks;
                $studentassesment->TM_SAR_STU_Percentage=$finalpercentage;
                $studentassesment->TM_SAR_STU_Status=1;
                $studentassesment->save(false);
            endforeach;
        endif;
        if($notanswered>0):
            foreach($notanswered as $notanswereddata):
                $studentdetails=Student::model()->find(array("condition"=>"TM_STU_User_Id='".$notanswereddata->TM_STHW_Student_Id."'"));
                $studentname=$studentdetails->TM_STU_First_Name." ".$studentdetails->TM_STU_Last_Name;
                $studentassesment = new SingleHwstudStatistics();
                $studentassesment->TM_SAR_STU_HW_Id=$id;
                $studentassesment->TM_SAR_STU_Student_Id=$notanswereddata->TM_STHW_Student_Id;
                $studentassesment->TM_SAR_STU_Name=$studentname;
                $studentassesment->TM_SAR_STU_TotalMarks=0;
                $studentassesment->TM_SAR_STU_Percentage=0;
                $studentassesment->TM_SAR_STU_Status=0;
                $studentassesment->save(false);
            endforeach;
        endif;

        $student_wise=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$id));
        foreach ($student_wise as $key => $student) {
        $studcomplete=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Student_Id='.$student->TM_STHWQT_Student_Id.' AND TM_STHW_Status IN (4,5)'));
        if($studcomplete)
        {
           $student_detail= new SingleHwquestStatistics();
           $student_detail->TM_SAR_QN_HW_Id=$id;
           $student_detail->TM_SAR_QN_Student_Id=$student->TM_STHWQT_Student_Id;
           $student_detail->TM_SAR_QN_Parent_Id=$student->TM_STHWQT_Parent_Id;
           $student_detail->TM_SAR_QN_Question_Id=$student->TM_STHWQT_Question_Id;
           $student_detail->TM_SAR_QN_Type=$student->TM_STHWQT_Type;
           $student_detail->TM_SAR_QN_Marks=$student->TM_STHWQT_Marks;
           $student_detail->save(false);
       }

        }

    }

    public function actionSingleassesmentreportbulk()
{

    $homeworks =  Schoolhomework::model()->findAll(['condition'=> 'TM_SCH_Status = 2 LIMIT 0,2']);
     $homeworkIDS = 'test';
    foreach ($homeworks as $homework)
    {
        $id =$homework->TM_SCH_Id;
        $homeworkIDS.= ','.$id;
        $ifExist = SingleHwStatistics::model()->find(array('condition'=>'TM_SAR_HW_Id='.$id));
        if($ifExist)
            $singleassesment = $ifExist;
        else
            $singleassesment = new SingleHwStatistics();

        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$id'",'order'=>'TM_HQ_Order ASC'));
        $totalquestions= count($hwquestions);
        $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$id' "));
        $totalMarks=$studhw->TM_STHW_TotalMarks;
        $studhws=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $totalanswered = count($studhws);
        $notanswered=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS user ON user.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.'  AND TM_STHW_Status NOT IN (4,5) AND user.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $totalnotanswered = count($notanswered);
        $singleassesment->TM_SAR_HW_Id=$id;
        $singleassesment->TM_SAR_Total_Questions=$totalquestions;
        $singleassesment->TM_SAR_Total_Marks=$totalMarks;
        $singleassesment->TM_SAR_Atempted=$totalanswered;
        $singleassesment->TM_SAR_Not_Attempted=$totalnotanswered;
        $singleassesment->save(false);
        if($totalanswered>0):
            foreach($studhws as $studhwdata):
                $studentdetails=Student::model()->find(array("condition"=>"TM_STU_User_Id='".$studhwdata->TM_STHW_Student_Id."'"));
                $studentname=$studentdetails->TM_STU_First_Name." ".$studentdetails->TM_STU_Last_Name;

                $actualpercentage = $studhwdata->TM_STHW_Percentage;
                if (strpos($actualpercentage, '.')!== false)
                 {
                    $finalpercentage = number_format($studhwdata->TM_STHW_Percentage,2);
                }
                else{
                    $finalpercentage = $actualpercentage;
                }
                $studentassesment = new SingleHwstudStatistics();
                $studentassesment->TM_SAR_STU_HW_Id=$id;
                $studentassesment->TM_SAR_STU_Student_Id=$studhwdata->TM_STHW_Student_Id;
                $studentassesment->TM_SAR_STU_Name=$studentname;
                $studentassesment->TM_SAR_STU_TotalMarks=$studhwdata->TM_STHW_Marks;
                $studentassesment->TM_SAR_STU_Percentage=$finalpercentage;
                $studentassesment->TM_SAR_STU_Status=1;
                $studentassesment->save(false);
            endforeach;
        endif;
        if($notanswered>0):
            foreach($notanswered as $notanswereddata):
                $studentdetails=Student::model()->find(array("condition"=>"TM_STU_User_Id='".$notanswereddata->TM_STHW_Student_Id."'"));
                $studentname=$studentdetails->TM_STU_First_Name." ".$studentdetails->TM_STU_Last_Name;
                $studentassesment = new SingleHwstudStatistics();
                $studentassesment->TM_SAR_STU_HW_Id=$id;
                $studentassesment->TM_SAR_STU_Student_Id=$notanswereddata->TM_STHW_Student_Id;
                $studentassesment->TM_SAR_STU_Name=$studentname;
                $studentassesment->TM_SAR_STU_TotalMarks=0;
                $studentassesment->TM_SAR_STU_Percentage=0;
                $studentassesment->TM_SAR_STU_Status=0;
                $studentassesment->save(false);
            endforeach;
        endif;

        $student_wise=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$id));
        foreach ($student_wise as $key => $student) {
            $studcomplete=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Student_Id='.$student->TM_STHWQT_Student_Id.' AND TM_STHW_Status IN (4,5)'));
            if($studcomplete)
            {
               $student_detail= new SingleHwquestStatistics();
               $student_detail->TM_SAR_QN_HW_Id=$id;
               $student_detail->TM_SAR_QN_Student_Id=$student->TM_STHWQT_Student_Id;
               $student_detail->TM_SAR_QN_Parent_Id=$student->TM_STHWQT_Parent_Id;
               $student_detail->TM_SAR_QN_Question_Id=$student->TM_STHWQT_Question_Id;
               $student_detail->TM_SAR_QN_Type=$student->TM_STHWQT_Type;
               $student_detail->TM_SAR_QN_Marks=$student->TM_STHWQT_Marks;
               $student_detail->save(false);
            }

        }
    }
     echo $homeworkIDS;exit;

}

    public function actionConsolidatedreportbulkdata()
    {
        $connection = CActiveRecord::getDbConnection();
        $homeworks =  Schoolhomework::model()->findAll(['condition'=> 'TM_SCH_Status = 2 LIMIT 500,100']);
        $homeworkIDS = 'test';
        foreach ($homeworks as $homework) {
            $id =$homework->TM_SCH_Id;
            $homeworkIDS .= ','.$id;

            $homeworkgroups = Homeworkgroup::model()->findAll(array('condition' => "TM_HMG_Homework_Id='$id' "));

            $studhws=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_HomeWork_Id='$id' AND TM_STHW_Student_Id!='0'", "order" => "TM_STHW_Status DESC"));

            $wrkgrpids = '';
            $index = 0;
            foreach($homeworkgroups as $homeworkgroup):
                // $group = SchoolGroups::model()->findByPK($homeworkgroup->TM_HMG_Groupe_Id);
                if($index == 0)
                    $wrkgrpids .= $homeworkgroup->TM_HMG_Groupe_Id;
                else
                    $wrkgrpids .= ','.$homeworkgroup->TM_HMG_Groupe_Id;
                $index++;
            endforeach;
            $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));

            $notanswereded=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS user ON user.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.'  AND TM_STHW_Status NOT IN (4,5) AND user.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
            $ifExist = AssessmentStatistics::model()->find(array('condition'=>'schoolhomework_id='.$id));
            if($ifExist)
                $assessmentStatistics = $ifExist;
            else
                $assessmentStatistics = new AssessmentStatistics();

            $assessmentStatistics->date = date('Y-m-d');
            $assessmentStatistics->group_ids = $wrkgrpids;
            $assessmentStatistics->teacher_id = $homework->TM_SCH_CreatedBy;
            $assessmentStatistics->schoolhomework_id = $id;
            $assessmentStatistics->homework_name = $homework->TM_SCH_Name;
            $assessmentStatistics->students_completed = 0;
            $assessmentStatistics->students_not_attempted = 0;
            $assessmentStatistics->average_score = 0;
            if($assessmentStatistics->save(false))
            {
                $notanswered = count($notanswereded);
                $assessmentStatistics->students_completed = count($startedstudents);
                $assessmentStatistics->students_not_attempted = $notanswered;
                $assessmentStatistics->save(false);
                // homeworkquestions
                $questions=HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$id));

                // var_dump($questions);exit;
                $totalMarks = 0;
                $totalScored = 0;
                if(count($questions) > 0)
                {
                    foreach ($questions as $question) {
                        $totalAttempted = 0;
                        $totalCorrected = 0;
                        $totalstudentscorrect = [];
                        $questMaster = Questions::model()->findByPk($question->TM_HQ_Question_Id);
                        if ($questMaster->TM_QN_Type_Id != '5')
                        {
                            $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$questMaster->TM_QN_Id."' AND TM_STHWQT_Student_Id!='0' AND TM_STHWQT_Marks='".$questMaster->TM_QN_Totalmarks."'"));

                        }
                        else
                        {


                            $sql4 = "SELECT *,SUM(a.TM_STHWQT_Marks) AS totalGained FROM tm_studenthomeworkquestions AS a
                            LEFT JOIN  tm_question AS b ON b.TM_QN_Id=a.TM_STHWQT_Parent_Id
                            WHERE a.TM_STHWQT_Mock_Id ='".$id."' AND a.TM_STHWQT_Parent_Id = '".$questMaster->TM_QN_Id."' GROUP BY a.TM_STHWQT_Parent_Id,a.TM_STHWQT_Student_Id HAVING  totalGained = b.TM_QN_Totalmarks";

                            $command=$connection->createCommand($sql4);
                            $dataReader=$command->query();
                            $totalstudentscorrect=$dataReader->readAll();

                        }

                        $totalMarks += $questMaster->TM_QN_Totalmarks * count($startedstudents);
                        $totalScored += $questMaster->TM_QN_Totalmarks * count($totalstudentscorrect);
                        // consolidated report questions
                        // check if question already exists
                        $ifQExist = AssesmentStatisticsQuestions::model()->find(array('condition'=>'schoolhomework_id='.$id.' AND question_id ='.$question->TM_HQ_Question_Id.' AND assessment_statistics_id='.$assessmentStatistics->id));
                        if($ifQExist)
                            $consolidatedQuestion = $ifQExist;
                        else
                            $consolidatedQuestion = new AssesmentStatisticsQuestions();

                        $consolidatedQuestion->chapter_id = $questMaster->TM_QN_Topic_Id;
                        $consolidatedQuestion->topic_id = $questMaster->TM_QN_Section_Id;
                        $consolidatedQuestion->schoolhomework_id = $id;
                        $consolidatedQuestion->assessment_statistics_id = $assessmentStatistics->id;
                        $consolidatedQuestion->parent = 0;
                        $consolidatedQuestion->question_id = $question->TM_HQ_Question_Id;
                        $consolidatedQuestion->type = $question->TM_HQ_Type;
                        $consolidatedQuestion->total_students = count($studhws);
                        $consolidatedQuestion->total_attempted = count($startedstudents);
                        $consolidatedQuestion->total_corrected = count($totalstudentscorrect);
                        $consolidatedQuestion->save(false);
                    }
                }
                if($totalMarks != 0){
                    $assessmentStatistics->average_score = ($totalScored/$totalMarks)* 100;
                    $assessmentStatistics->save(false);
                }

            }

            // $statisticsExist = AssessmentStatistics::model()->find(['condition'=> 'schoolhomework_id ='.$id.' ']);

            // $totalMarks = 0;
            // $totalScored = 0;

            // if(!$statisticsExist)
            // {
            //     $assessmentStatistics = new AssessmentStatistics();
            //     $assessmentStatistics->date = $homework->TM_SCH_CompletedOn;
            //     $assessmentStatistics->group_ids = $wrkgrpids;
            //     $assessmentStatistics->teacher_id = $homework->TM_SCH_CreatedBy;
            //     $assessmentStatistics->schoolhomework_id = $id;
            //     $assessmentStatistics->homework_name = $homework->TM_SCH_Name;
            //     $assessmentStatistics->students_completed = 0;
            //     $assessmentStatistics->students_not_attempted = 0;
            //     $assessmentStatistics->average_score = 0;
            //     if($assessmentStatistics->save(false))
            //     {

            //         $notanswered = count($studhws) - count($startedstudents);
            //         $assessmentStatistics->students_completed = count($startedstudents);
            //         $assessmentStatistics->students_not_attempted = $notanswered;
            //         $assessmentStatistics->save(false);



            //         // homeworkquestions
            //         $questions=HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$id));




            //         if(count($questions) > 0)
            //         {
            //             foreach ($questions as $question) {

            //                 $questMaster = Questions::model()->findByPk($question->TM_HQ_Question_Id);

            //                 if ($questMaster->TM_QN_Type_Id != '5')
            //                 {

            //                     $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$questMaster->TM_QN_Id."' AND TM_STHWQT_Student_Id!='0' AND TM_STHWQT_Marks='".$questMaster->TM_QN_Totalmarks."'"));

            //                     $totalMarks += $questMaster->TM_QN_Totalmarks * count($startedstudents);
            //                     $totalScored += $questMaster->TM_QN_Totalmarks * count($totalstudentscorrect);

            //                     // consolidated report questions
            //                     $consolidatedQuestion = new AssesmentStatisticsQuestions();
            //                     $consolidatedQuestion->chapter_id = $questMaster->TM_QN_Topic_Id;
            //                     $consolidatedQuestion->topic_id = $questMaster->TM_QN_Section_Id;
            //                     $consolidatedQuestion->schoolhomework_id = $id;
            //                     $consolidatedQuestion->assessment_statistics_id = $assessmentStatistics->id;
            //                     $consolidatedQuestion->parent = 0;
            //                     $consolidatedQuestion->question_id = $question->TM_HQ_Question_Id;
            //                     $consolidatedQuestion->type = $question->TM_HQ_Type;
            //                     $consolidatedQuestion->total_students = count($studhws);
            //                     $consolidatedQuestion->total_attempted = count($startedstudents);
            //                     $consolidatedQuestion->total_corrected = count($totalstudentscorrect);
            //                     $consolidatedQuestion->save(false);
            //                 }
            //                 else
            //                 {
            //                     $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $questMaster->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));

            //                     foreach ($questions AS $childquestion)
            //                     {

            //                         $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$childquestion->TM_QN_Id."' AND TM_STHWQT_Student_Id!='0' AND TM_STHWQT_Marks='".$childquestion->TM_QN_Totalmarks."'"));

            //                         $totalMarks += $childquestion->TM_QN_Totalmarks * count($startedstudents);
            //                         $totalScored += $childquestion->TM_QN_Totalmarks * count($totalstudentscorrect);

            //                         $consolidatedQuestion = new AssesmentStatisticsQuestions();
            //                         $consolidatedQuestion->chapter_id = $childquestion->TM_QN_Topic_Id;
            //                         $consolidatedQuestion->topic_id = $childquestion->TM_QN_Section_Id;
            //                         $consolidatedQuestion->schoolhomework_id = $id;
            //                         $consolidatedQuestion->assessment_statistics_id = $assessmentStatistics->id;
            //                         $consolidatedQuestion->parent = $questMaster->TM_QN_Id;
            //                         $consolidatedQuestion->question_id = $childquestion->TM_QN_Id;
            //                         $consolidatedQuestion->type = $childquestion->TM_QN_Type_Id;
            //                         $consolidatedQuestion->total_students = count($studhws);
            //                         $consolidatedQuestion->total_attempted = count($startedstudents);
            //                         $consolidatedQuestion->total_corrected = count($totalstudentscorrect);
            //                         $consolidatedQuestion->save(false);


            //                     }



            //                 }
            //             }

            //         }

            //         if($totalMarks != 0)
            //         {
            //             $assessmentStatistics->average_score = ($totalScored/$totalMarks)* 100;
            //             $assessmentStatistics->save(false);
            //         }


            //     }
            // }

        }

        echo $homeworkIDS;exit;
    }

    public function actionMovetohistory($id)
    {
        $homework=Schoolhomework::model()->findByPk($id);
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        foreach($startedstudents AS $started):
            $started->TM_STHW_Status='5';
            $started->save(false);
            $pointlevels = $this->AddPoints($started->TM_STHW_Marks, $id, 'Homework', $started->TM_STHW_Student_Id);
        endforeach;
        $homework->TM_SCH_Status='2';
        $homework->TM_SCH_CompletedOn=date('Y-m-d h:i:s');
        $homework->save(false);
        $assignedusers=Homeworkmarking::model()->findAll(array('condition'=>'TM_HWM_Homework_Id='.$id.''));
        foreach($assignedusers AS $assigned):
            $assigned->TM_HWM_Status=1;
            $assigned->save(false);
        endforeach;
        $this->Thematicmove($id);
        $this->singleassesmentreport($id);
        if(Yii::app()->user->isTeacher()):
            $this->redirect(array('home'));
        elseif(Yii::app()->user->isMarker()):
            $this->redirect(array('staffhome'));
        endif;
    }

    public function Thematicmove($id)
    {
        $homework=Schoolhomework::model()->findByPk($id);
        $homeworkgroups = Homeworkgroup::model()->findAll(array('condition' => "TM_HMG_Homework_Id='$id' "));
        $wrkgrpids = '';
        $index = 0;
        foreach($homeworkgroups as $homeworkgroup):
            // $group = SchoolGroups::model()->findByPK($homeworkgroup->TM_HMG_Groupe_Id);
            if($index == 0)
                $wrkgrpids .= $homeworkgroup->TM_HMG_Groupe_Id;
            else
                $wrkgrpids .= ','.$homeworkgroup->TM_HMG_Groupe_Id;
            $index++;
        endforeach;
        $studhws=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_HomeWork_Id='$id' AND TM_STHW_Student_Id!='0'", "order" => "TM_STHW_Status DESC"));

       $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));

        $notanswereded=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS user ON user.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.'  AND TM_STHW_Status NOT IN (4,5) AND user.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $connection = CActiveRecord::getDbConnection();
        $ifExist = AssessmentStatistics::model()->find(array('condition'=>'schoolhomework_id='.$id));
        if($ifExist)
            $assessmentStatistics = $ifExist;
        else
            $assessmentStatistics = new AssessmentStatistics();

        $assessmentStatistics->date = date('Y-m-d');
        $assessmentStatistics->group_ids = $wrkgrpids;
        $assessmentStatistics->teacher_id = $homework->TM_SCH_CreatedBy;
        $assessmentStatistics->schoolhomework_id = $id;
        $assessmentStatistics->homework_name = $homework->TM_SCH_Name;
        $assessmentStatistics->students_completed = 0;
        $assessmentStatistics->students_not_attempted = 0;
        $assessmentStatistics->average_score = 0;
        if($assessmentStatistics->save(false))
        {
            $notanswered = count($notanswereded);
            $assessmentStatistics->students_completed = count($startedstudents);
            $assessmentStatistics->students_not_attempted = $notanswered;
            $assessmentStatistics->save(false);
            // homeworkquestions
            $questions=HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$id));
            $totalMarks = 0;
            $totalScored = 0;
            if(count($questions) > 0)
            {
                foreach ($questions as $question) {
                    $totalAttempted = 0;
                    $totalCorrected = 0;
                    $totalstudentscorrect = [];
                    $questMaster = Questions::model()->findByPk($question->TM_HQ_Question_Id);
                    if ($questMaster->TM_QN_Type_Id != '5')
                    {
                       /* $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$questMaster->TM_QN_Id."' AND TM_STHWQT_Student_Id!='0' AND TM_STHWQT_Marks='".$questMaster->TM_QN_Totalmarks."'"));*/

                    $sql42 = "SELECT * FROM tm_studenthomeworkquestions AS a  INNER JOIN  tm_student_homeworks AS b ON b.TM_STHW_Student_Id=a.TM_STHWQT_Student_Id
                        WHERE a.TM_STHWQT_Mock_Id ='".$id."' AND b.TM_STHW_HomeWork_Id ='".$id."' AND a.TM_STHWQT_Question_Id = '".$questMaster->TM_QN_Id."' AND TM_STHWQT_Student_Id!=0 AND b.TM_STHW_Status IN (4,5) AND TM_STHWQT_Marks='".$questMaster->TM_QN_Totalmarks."'";

                        $command=$connection->createCommand($sql42);
                        $dataReader=$command->query();
                        $totalstudentscorrect=$dataReader->readAll();
                    }
                    else
                    {


                        $sql4 = "SELECT *,SUM(a.TM_STHWQT_Marks) AS totalGained FROM tm_studenthomeworkquestions AS a
                        LEFT JOIN  tm_question AS b ON b.TM_QN_Id=a.TM_STHWQT_Parent_Id
                        WHERE a.TM_STHWQT_Mock_Id ='".$id."' AND a.TM_STHWQT_Parent_Id = '".$questMaster->TM_QN_Id."' GROUP BY a.TM_STHWQT_Parent_Id,a.TM_STHWQT_Student_Id HAVING  totalGained = b.TM_QN_Totalmarks";

                        $command=$connection->createCommand($sql4);
                        $dataReader=$command->query();
                        $totalstudentscorrect=$dataReader->readAll();

                    }
                    $totalScored=0;
                    foreach ($startedstudents as $key => $startedstudent) {

                       $totalScored = $totalScored+$startedstudent->TM_STHW_Marks;
                    }

                    $totalMarks += $questMaster->TM_QN_Totalmarks * count($startedstudents);
                    //$totalScored += $questMaster->TM_QN_Totalmarks * count($totalstudentscorrect);
                    // consolidated report questions
                    // check if question already exists
                    $ifQExist = AssesmentStatisticsQuestions::model()->find(array('condition'=>'schoolhomework_id='.$id.' AND question_id ='.$question->TM_HQ_Question_Id.' AND assessment_statistics_id='.$assessmentStatistics->id));
                    if($ifQExist)
                        $consolidatedQuestion = $ifQExist;
                    else
                        $consolidatedQuestion = new AssesmentStatisticsQuestions();

                    $consolidatedQuestion->chapter_id = $questMaster->TM_QN_Topic_Id;
                    $consolidatedQuestion->topic_id = $questMaster->TM_QN_Section_Id;
                    $consolidatedQuestion->schoolhomework_id = $id;
                    $consolidatedQuestion->assessment_statistics_id = $assessmentStatistics->id;
                    $consolidatedQuestion->parent = 0;
                    $consolidatedQuestion->question_id = $question->TM_HQ_Question_Id;
                    $consolidatedQuestion->type = $question->TM_HQ_Type;
                    $consolidatedQuestion->total_students = count($studhws);
                    $consolidatedQuestion->total_attempted = count($startedstudents);
                    $consolidatedQuestion->total_corrected = count($totalstudentscorrect);
                    $consolidatedQuestion->save(false);
                }
            }
            if($totalMarks != 0){
                $assessmentStatistics->average_score = ($totalScored/$totalMarks)* 100;
                $assessmentStatistics->save(false);
            }
        }
    }
    public function actionSendreminder()
    {
        $notification=new Notifications();
        $notification->TM_NT_User=$_POST['student'];
        $notification->TM_NT_Type='HWReminder';
        $notification->TM_NT_Item_Id=$_POST['homework'];
        $notification->TM_NT_Target_Id=Yii::app()->user->id;
        $notification->TM_NT_Status='0';
        $notification->save(false);

        $user = Yii::app()->user->id;
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$user.''));
        $studentid=$_POST['student'];
        $hmewrkid=$_POST['homework'];

        $studentuser=User::model()->findByPk($studentid);
        $detail=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studentid.''));
        $homework=Schoolhomework::model()->findByPk($hmewrkid);
        $school=School::model()->findByPk($detail->TM_STU_School_Id);
        $parent_detail= User::model()->findByPk($detail->TM_STU_Parent_Id);
        $parentemail = $this->test_input($parent_detail->email);
        // check if e-mail address is well-formed
        if (filter_var($parentemail, FILTER_VALIDATE_EMAIL)) {
            ///echo "valid format";exit;
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->IsSMTP();
            //$mail->setFrom($sender, $senderName);
            $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
            $mail->SMTPAuth = true; // turn on SMTP authentication
            $mail->SMTPSecure = "tls";
            $mail->Port       = 587;
            $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
            $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
            //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
            $mail->From = 'services@tabbiemath.com';
            $mail->FromName = "TabbieMath";
            $mail->AddAddress($parent_detail->email);
            //$mail->AddAddress('amal@solminds.com');
            $mail->AddReplyTo("noreply@tabbiemath.com","noreply");
            $mail->IsHTML(true);
            $mail->Subject = "Homework reminder : ". $school->TM_SCL_Name."";
            $mail->Body = "<p>Dear ".$detail->TM_STU_First_Name."</p>
            <p>This is a reminder that ".$detail->TM_STU_First_Name." has an outstanding assignment to complete in TabbieMath.</p>
            <p>Name of assignment : ".$homework->TM_SCH_Name."</p>
            <p>Login to www.tabbiemath.com to complete the assignment.</p>
            <p>Username: ". $studentuser->username."<br>
            Password: ".$detail->TM_STU_Password."</p>
            <p>Regards,<br>".$teacher->TM_TH_Name."</p>";
            //echo $mail->Body;exit;
            $mail->Send();
            $message="yes";
        }
        else{
            $message="no";
        }
        echo $message;
        //echo "yes";
    }

    public function actionRemindall()
    {
        $user = Yii::app()->user->id;
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$user.''));
        $notstarted=StudentHomeworks::model()->findAll(array('condition'=>'TM_STHW_HomeWork_Id='.$_POST['homework'].' AND TM_STHW_Status NOT IN (4,5)'));
        $validstudarr=array();
        $invalidstudarr=array();
        foreach($notstarted AS $test):
            $notification=new Notifications();
            $notification->TM_NT_User=$test->TM_STHW_Student_Id;
            $notification->TM_NT_Type='HWReminder';
            $notification->TM_NT_Item_Id=$_POST['homework'];
            $notification->TM_NT_Target_Id=Yii::app()->user->id;
            $notification->TM_NT_Status='0';
            $notification->save(false);
            $hmewrkid=$_POST['homework'];
            $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$test->TM_STHW_Student_Id.''));
            $homework=Schoolhomework::model()->findByPk($hmewrkid);
            $school=School::model()->findByPk($student->TM_STU_School_Id);
            $parent_detail= user::model()->findByPk($student->TM_STU_Parent_Id);
            //echo $parent_detail->email."<br>";
            $parentemail = $this->test_input($parent_detail->email);
            $studentuser=User::model()->findByPk($student->TM_STU_User_Id);
            // check if e-mail address is well-formed
            if (filter_var($parentemail, FILTER_VALIDATE_EMAIL)) {
                Yii::import('application.extensions.phpmailer.JPhpMailer');
                $mail = new JPhpMailer;
                $mail->IsSMTP();
                $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
                $mail->SMTPAuth = true; // turn on SMTP authentication
                $mail->SMTPSecure = "tls";
                $mail->Port       = 587;
                $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
                $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; // SMTP password
                //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
                $mail->From = 'services@tabbiemath.com';
                $mail->FromName = "TabbieMath";
                //$mail->AddAddress('praveen@solminds.com');
                $mail->AddAddress($parent_detail->email);
                $mail->AddReplyTo("noreply@tabbiemath.com","noreply");
                $mail->IsHTML(true);
                $mail->Subject = "Homework reminder : ". $school->TM_SCL_Name."";
                $mail->Body = "<p>Dear ".$student->TM_STU_First_Name."</p>
                <p>This is a reminder that ".$student->TM_STU_First_Name." has an outstanding assignment to complete in TabbieMath.</p>
                <p>Name of assignment : ".$homework->TM_SCH_Name."</p>
                <p>Login to www.tabbiemath.com to complete the assignment.</p>
                <p>Username: ". $studentuser->username."<br>
                Password: ".$student->TM_STU_Password."</p>
                <p>Regards,<br>".$teacher->TM_TH_Name."</p>";
                $mail->Send();
                array_push($validstudarr,$test->TM_STHW_Student_Id);
            }
            else{
                array_push($invalidstudarr,$test->TM_STHW_Student_Id);
            }
        endforeach;
        //echo "yes";
        $arr=array('result'=>'yes','students'=>$validstudarr,'invalidstudents'=>$invalidstudarr);
        echo json_encode($arr);
        //$this->redirect(array('markhomework', 'id' =>$_POST['homework']));
    }

    public function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    public function GetQuestionColumnsCompleted($homework,$student)
    {
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $returnraw='';
        $count=1;
        foreach($hwquestions AS $hwq):
            $totalmarks=$this->Getmarks($hwq->TM_STHWQT_Question_Id);
            $checkclass='';
            /*$returnraw.='<td><figure class="thumbnail completed">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
                            <figcaption class="text-center">'.$hwq->TM_STHWQT_Marks.'</figcaption>
						</figure>
					</td>';*/
            if($hwq->TM_STHWQT_Type!='5'):
                $returnraw.='<td><figure class="thumbnail completed">
                                <figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
                                <figcaption class="text-center">'.$hwq->TM_STHWQT_Marks.'</figcaption>
                            </figure>
                        </td>';
            else:
                $childquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Parent_Id='.$hwq->TM_STHWQT_Question_Id.' '));
                $childreturnraw='';
                foreach($childquestions AS $key=>$childquestion):
                    $childreturnraw.='<td><figure class="thumbnail completed">
                                <figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'.'.($key + 1).'</figcaption>
                                <figcaption class="text-center">'.$childquestion->TM_STHWQT_Marks.'</figcaption>
                            </figure>
                        </td>';
                endforeach;
                $returnraw.=$childreturnraw;
            endif;
            $count++;
        endforeach;
        return $returnraw;

    }
    public function GetQuestionColumns($homework,$student)
    {

        $connection = CActiveRecord::getDbConnection();
        // $sql ="SELECT sum(TM_AR_Marks) AS mark FROM tm_answer WHERE TM_AR_Question_Id = '$questionid' AND TM_AR_Marks NOT IN(0)";
        //  $command=$connection->createCommand($sql);
        // $dataReader=$command->query();
        // $hwquestions=$dataReader->readAll();


        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $returnraw='';
        $count=1;
        foreach($hwquestions AS $hwq):
            $totalmarks=$this->Getmarks($hwq->TM_STHWQT_Question_Id);
            $checkclass='';
            if($hwq->TM_STHWQT_Type=='6' || $hwq->TM_STHWQT_Type=='7'):
                $checkclass='myred';
            elseif($hwq->TM_STHWQT_Marks==$totalmarks):
                $checkclass='mygreen';
            elseif(($hwq->TM_STHWQT_Type!='6' || $hwq->TM_STHWQT_Type!='7') & $hwq->TM_STHWQT_Marks!=$totalmarks):
                $checkclass='myyellow';
            endif;
            /*$returnraw.='<td><figure class="thumbnail '.$checkclass.'">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
							<span class="badge mybdg">'.$totalmarks.'</span>
                            <input type="hidden" id="totmarks'.$hwq->TM_STHWQT_Question_Id.'" value="'.$totalmarks.'">
                            <input type="hidden" class="error'.$student.'" id="error'.$student.$hwq->TM_STHWQT_Question_Id.'" value="0">
							<input class="form-control setmark'.$student.' mark" data-id="'.$hwq->TM_STHWQT_Question_Id.'" data-student="'.$student.'" id="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" name="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" value="'.$hwq->TM_STHWQT_Marks.'" type="text">
						</figure>
						<span id="markinfo'.$hwq->TM_STHWQT_Question_Id.'" style="color:red;display: none;"></span>
					</td>';*/
            if($hwq->TM_STHWQT_Type!='5'):
                $returnraw.='<td><figure class="thumbnail '.$checkclass.'">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
							<span class="badge mybdg">'.$totalmarks.'</span>
                            <input type="hidden" id="totmarks'.$hwq->TM_STHWQT_Question_Id.'" value="'.$totalmarks.'">
                            <input type="hidden" class="error'.$student.'" id="error'.$student.$hwq->TM_STHWQT_Question_Id.'" value="0">
							<input class="form-control setmark'.$student.' mark" data-id="'.$hwq->TM_STHWQT_Question_Id.'" data-student="'.$student.'" id="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" name="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" value="'.$hwq->TM_STHWQT_Marks.'" type="text">
						</figure>
						<span id="markinfo'.$hwq->TM_STHWQT_Question_Id.'" style="color:red;display: none;"></span>
					</td>';
            else:
                $childquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Parent_Id='.$hwq->TM_STHWQT_Question_Id.' '));
                $childreturnraw='';
                foreach($childquestions AS $key=>$childquestion):
                    // $questionType = Questions::model()->findByPk($childquestion->TM_STHWQT_Question_Id)->TM_QN_Type_Id;
                    $childtotalmarks=$this->ChildGetmarks($childquestion->TM_STHWQT_Question_Id);

                    // code by vivek - multipart text entry questions
                    // if($childquestion->TM_STHWQT_Marks != 0)
                        $childmarks=$childquestion->TM_STHWQT_Marks;
                    // else
                    // {
                    //     $questionType = Questions::model()->findByPk($childquestion->TM_STHWQT_Question_Id)->TM_QN_Type_Id;
                    //     if($questionType == 4)
                    //     {
                    //         $answers = Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$childquestion->TM_STHWQT_Question_Id));
                    //         $answeroption = explode(';', strtolower(strip_tags($answers->TM_AR_Answer)));
                    //         $useranswer = trim(strtolower($childquestion->TM_STHWQT_Answer));
                    //         $marked = 0;
                    //         $marks = 0;
                    //         for ($i = 0; $i < count($answeroption); $i++) {
                    //             $option = trim($answeroption[$i]);

                    //             //fix for < > issue
                    //             if($option=='&gt'):
                    //                 $newoption='&gt;';
                    //             elseif($option=='&lt'):
                    //                 $newoption='&lt;';
                    //             else:
                    //                 $newoption=$option;
                    //             endif;
                    //             if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0 && $marked ==0)
                    //             {
                    //                 $marks = $answers->TM_AR_Marks;
                    //                 $marked = 1;
                    //             }
                    //         }
                    //         $childmarks = $marks;
                    //     }
                    //     else
                    //     {
                    //         $answer=Answers::model()->findByPk($childquestion->TM_STHWQT_Answer);
                    //         if($answer->TM_AR_Correct==1):
                    //             $marks=$answer->TM_AR_Marks;
                    //         else:
                    //             $marks=0;
                    //         endif;
                    //         if($childquestion->TM_STHWQT_Marks!=0):
                    //             $childmarks=$childquestion->TM_STHWQT_Marks;
                    //         else:
                    //             $childmarks=$marks;
                    //         endif;
                    //     }
                    // }

                    if($childmarks==$childtotalmarks):
                        $checkclass='mygreen';
                    else:
                        $checkclass='myyellow';
                    endif;
                    $childreturnraw.='<td><figure class="thumbnail '.$checkclass.'">
                        <figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'.'.($key + 1).'</figcaption>
                        <span class="badge mybdg">'.$childtotalmarks.'</span>
                        <input type="hidden" id="totmarks'.$childquestion->TM_STHWQT_Question_Id.'" value="'.$childtotalmarks.'">
                        <input type="hidden" class="error'.$student.'" id="error'.$student.$childquestion->TM_STHWQT_Question_Id.'" value="0">
                        <input class="form-control setmark'.$student.' mark" data-id="'.$childquestion->TM_STHWQT_Question_Id.'" data-student="'.$student.'" id="setmark'.$student.$childquestion->TM_STHWQT_Question_Id.'" name="setmark'.$student.$childquestion->TM_STHWQT_Question_Id.'" value="'.$childmarks.'" type="text">
                    </figure>
                    <span id="markinfo'.$childquestion->TM_STHWQT_Question_Id.'" style="color:red;display: none;"></span>
                </td>';
                endforeach;
                $returnraw.=$childreturnraw;
            endif;
            $count++;
        endforeach;
        return $returnraw;

    }

    public function ChildGetmarks($questionid)
    {
        // $connection = CActiveRecord::getDbConnection();
        // $sql ="SELECT sum(TM_AR_Marks) AS mark FROM tm_answer WHERE TM_AR_Question_Id = '$questionid' AND TM_AR_Marks NOT IN(0)";
        //  $command=$connection->createCommand($sql);
        // $dataReader=$command->query();
        // $total=$dataReader->read();

        // return $total['mark'];
        $question = Questions::model()->findByPk($questionid);
        $total= $question->TM_QN_Totalmarks;
        return $total;

    }

    public function Getmarks($questionid)
    {
            $question = Questions::model()->findByPk($questionid);
            $total= $question->TM_QN_Totalmarks;
            return $total;
    }
    public function Updatemarks($qid)
    {
        $question = Questions::model()->findByPk($qid);
         if ($question->TM_QN_Parent_Id == '0'):
             if($question->TM_QN_Type_Id!='5' && $question->TM_QN_Type_Id!='6' && $question->TM_QN_Type_Id!='7'):
                 $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                 $total = 0;
                 foreach ($marks AS $key => $marksdisplay):
                     $total = $total + (float)$marksdisplay->TM_AR_Marks;
                 endforeach;
             elseif($question->TM_QN_Type_Id=='5'):
                 $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                 $total=0;
                 foreach ($childquestions AS $childquestion):
                     $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                     foreach ($marks AS $key => $marksdisplay):
                         $total = $total + (float)$marksdisplay->TM_AR_Marks;
                     endforeach;
                 endforeach;
             elseif($question->TM_QN_Type_Id=='6'):
                 $total=$question->TM_QN_Totalmarks;
             elseif($question->TM_QN_Type_Id=='7'):
                 $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                 $total=0;
                 foreach ($childquestions AS $childquestion):
                     $total = $total + (float)$childquestion->TM_QN_Totalmarks;
                 endforeach;
             endif;
         endif;
        return $total;
    }
    /** Teacher marking action ends **/
    //vivek - functio for bulk total mark update into table 'questions'
    public function actionAssignTotal()
    {
        set_time_limit(1000);
        //$from=$_GET['from'];
        $limit = 1000;
		$questions_=Questions::model()->findAll(array('condition'=>"TM_QN_Parent_Id=0 AND TM_QN_Totalmarks=0",'order' => 'TM_QN_Id ASC','limit' => $limit));

        if(count($questions_)>0){
					foreach($questions_ AS $question_){
                            $getmar=$this->Updatemarks($question_->TM_QN_Id);
                            Questions::model()->updateByPk($question_->TM_QN_Id, array(
                            'TM_QN_Totalmarks' => $getmar
                        ));
                         echo "Question Id=".$question_->TM_QN_Id." TotalMark=".$getmar."</br>";
					}
                    //var_dump("Total mark for".$limit."questions successfully updated.");
		}
   }

    /** assign home work for marking**/
    public function actionAssignmarking()
    {
        $school=Yii::app()->session['school'];
        $homeworkid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
        for($i=0;$i<count($groupid);$i++)
        {
            $homework=new Homeworkmarking();
            $homework->TM_HWM_Homework_Id=$homeworkid;
            $homework->TM_HWM_User_Id= $groupid[$i];
            $homework->TM_HWM_Assigned_On=date('Y-m-d');
            $homework->TM_HWM_Assigned_By=Yii::app()->user->id;
            $homework->TM_HWM_Status='0';
            $marker=Homeworkmarking::model()->find(array('condition' => "TM_HWM_Homework_Id='$homeworkid' AND TM_HWM_User_Id='$groupid[$i]' "));
            $count=count($marker);
            if($count==0)
            {
                $homework->save(false);
                $notification=new Notifications();
                $notification->TM_NT_User=$groupid[$i];
                $notification->TM_NT_Type='HWMarking';
                $notification->TM_NT_Item_Id=$homeworkid;
                $notification->TM_NT_Target_Id=Yii::app()->user->id;
                $notification->TM_NT_Status='0';
                $notification->save(false);
            }
        }
    }
    /** assign home work for marking ends **/
    /** staff function*/
    public function actionStaffhome()
    {
        $this->layout = '//layouts/teacher';
        $masterschool=UserModule::GetSchoolDetails();
        $model=new Homeworkmarking('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Mock']))
            $model->attributes=$_GET['Mock'];
        $this->render('markerhome',array('masterschool'=>$masterschool,'model'=>$model));
    }
    /** staff function*/
    /**
     * add points function
     */

    public function AddPoints($earnedmarks, $testid, $testtype, $user)
    {
        $studentpintcount = StudentPoints::model()->count(
            array('condition' => 'TM_STP_Student_Id=:student AND TM_STP_Test_Id=:test AND TM_STP_Test_Type=:type',
                'params' => array(':student' => $user, ':test' => $testid, 'type' => $testtype)
            ));
        if ($studentpintcount == 0) {
            $studentpoint = new StudentPoints();
            $studentpoint->TM_STP_Student_Id = $user;
            $studentpoint->TM_STP_Points = $earnedmarks;
            $studentpoint->TM_STP_Test_Id = $testid;
            $studentpoint->TM_STP_Test_Type = $testtype;
            $studentpoint->TM_STP_Standard_Id = Yii::app()->session['standard'];
            $studentpoint->save(false);
        }
        return true;
    }
 //code by amal
   public function actionPrintworksheet($id)
    {

        $schoolhomework=Schoolhomework::model()->findByPk($id);
        if($schoolhomework->TM_SCH_Worksheet==''):
            $homeworkqstns=HomeworkQuestions::model()->findAll(array(
                'condition' => 'TM_HQ_Homework_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_HQ_Order ASC'
            ));
            $username=User::model()->findByPk(Yii::app()->user->id)->username;
            $hwemoorkdata=Schoolhomework::model()->findByPK($id);
            $homework=$hwemoorkdata->TM_SCH_Name;
            $showoptionmaster=$hwemoorkdata->TM_SCH_PublishType;
            $html = "<style>
            p{
            margin-top: 0;
            margin-bottom: 0;
            }
            </style>
            <table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";
            //$html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>";
            $html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";
            $totalquestions=count($homeworkqstns);
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            $totalmarks=0;
            foreach ($homeworkqstns AS $key => $homeworkqstn):

                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_HQ_Question_Id);

                if($showoptionmaster==3):
                    if($question->TM_QN_Show_Option==1):

                        $showoption=true;
                    else:
                        $showoption=false;
                    endif;
                else:
                    $showoption=true;
                endif;
                //code by amal
                if($homeworkqstn->TM_HQ_Type!=5):
                    if($homeworkqstn->TM_HQ_Type==4):
                        $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_HQ_Question_Id' "));
                        $marks=$answer->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_HQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        //$childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_HQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_HQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            //$childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_HQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                if($homeworkqstn['TM_HQ_Header']!=''):
                    $html .= "<tr><td colspan=3 align='center'><u><b>".$homeworkqstn['TM_HQ_Header']."</b><u></td></tr>
                                <tr><td colspan=3 align='center'><i>".$homeworkqstn['TM_HQ_Section']."</i></td></tr><tr><td colspan=3 align='center' ></td></tr>";
                endif;

                    if ($question->TM_QN_Type_Id!='7'):
                        if($hwemoorkdata->TM_SCH_ShowMark==0){
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount ."</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>( " . $marks . " )</td></tr>";
                        }
                        else
                        {
                             $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount ."</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";

                        }


                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        if($homeworkqstn->TM_HQ_Type==6 || $homeworkqstn->TM_HQ_Type==4):
                            $showoption=false;
                        endif;
                        if($homeworkqstn->TM_HQ_Show_Options==1):
                            $showoption=true;
                        endif;
                        if($showoption):
                            foreach($question->answers AS $key=>$answer):
                                if($answer->TM_AR_Image!=''):
                                    $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=150&height=150'."' alt=''>";
                                else:
                                    $ansoptimg='';
                                endif;
                                $ans=$answer->TM_AR_Answer;
                                $find='<p>';
                                $pos = strpos($ans, $find);
                                if ($pos === false) {
                                    $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")".$answer->TM_AR_Answer."</p> $ansoptimg</td><td></td></tr>";
                                } else {
                                    $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p style="margin:0;">'.$alphabets[$key].') ', 0,3)." $ansoptimg</td><td></td></tr>";
                                }
                            endforeach;
                        endif;
                        if($question->TM_QN_Type_Id=='5'):
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            foreach ($questions AS $key=>$childquestion):

                                $html .= "<tr><td width='6%'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td></td></tr>";
                                if ($childquestion->TM_QN_Image != ''):
                                    $html .= '<tr><td width="6%"></td><td align="left" style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td></td></tr>';
                                endif;
                                // if($childquestion->TM_QN_Type_Id==4):
                                //     $showoption=false;
                                // endif;
                                if($childquestion->TM_QN_Type_Id != 4)
                                {
                                    if($showoption):
                                    foreach($childquestion->answers AS $key=>$answer):
                                        if($answer->TM_AR_Image!=''):
                                            $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=150&height=150'."' alt=''>";
                                        else:
                                            $ansoptimg='';
                                        endif;
                                        $ans=$answer->TM_AR_Answer;
                                        $find='<p>';
                                        $pos = strpos($ans, $find);
                                        if ($pos === false)
                                        {
                                            $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")".$answer->TM_AR_Answer."</p> $ansoptimg</td><td></td></tr>";
                                        }
                                        else
                                        {
                                            $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)." $ansoptimg</td><td></td></tr>";
                                        }
                                    endforeach;
                                endif;

                                }

                            endforeach;
                        endif;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    if ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $countpart = 1;
                        $childhtml = "";
                        foreach ($questions AS $childquestion):

                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                            $childhtml .="<tr><td width='6%' style='padding:0;'></td><td align='left' style='padding:0;'>Part." . $countpart."</td><td align='right' width='6%' style='padding:0;'></td></tr>";
                            $childhtml .="<tr><td width='6%' style='padding:0;'></td><td  style='padding:0;'>".$childquestion->TM_QN_Question."</td><td align='right' width='6%' style='padding:0;'></td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $childhtml .= '<tr><td align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            $countpart++;
                        endforeach;
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks: " . $displaymark . " ( &nbsp; )</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $html .= $childhtml;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3" style="padding:0;"><br></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    $totalmarks=$totalmarks+$marks;
            endforeach;
            $html .= "</table>";
            $schoolhw=Schoolhomework::model()->findByPK($id);
            $school=School::model()->findByPK($schoolhw->TM_SCH_School_Id);
            $standard=Standard::model()->findByPk($schoolhw->TM_SCH_Standard_Id)->TM_SD_Name;
            //$total=$this->GetMocktotal($id);
            $total=$totalmarks;
            $header.='<div>
            <table cellpadding="5" border="0" style="margin-bottom:10px; margin-top:0px; font-size:14px;font-family: STIXGeneral;" width="100%">';
               if($school->TM_SCL_Image!='')
                {
                $header.= '<tr><td colspan=4 align="center" style="padding:0;"><img src="'.Yii::app()->request->baseUrl.'/images/headers/'.$school->TM_SCL_Image.'" style="width:1200px;"> <hr style="height: 1px; border: 0;  display: block; background: transparent; width: 100%; border-top: solid 1px #9a9696;"></td></tr>';
               }
               else
               {
                 $header.='<tr><td colspan=4 align="center" style="padding:0;"><b>'.$school->TM_SCL_Name.'</b></td></tr>';
               }


                $header.='<tr><td colspan=4 align="center" style="padding:0; font-size:18px;"><b>'.$homework.'</b></td></tr>
                <tr>
                    <td width="20%" style="padding:0">Student Name :</td>
                    <td width="50%" style="padding:0"> </td>
                    <td width="20%" style="padding:0">Date :</td>
                    <td width="10%" style="padding:0"> '.date('d/m/Y').'</td>
                </tr>
                <tr>
                    <td width="20%" style="padding:0">Standard :</td>
                    <td width="50%" style="padding:0">  '.$standard.'</td>
                    <td width="20%" style="padding:0">Division : </td>
                    <td width="10%" style="padding:0"> </td>
                </tr>';

            if($schoolhw->TM_SCH_ShowMark==0)
            {
               $header.='<tr>
                    <td width="20%" style="padding:0">Total Marks :</td>
                    <td width="50%" style="padding:0"> '.$total.' </td>
                    <td width="20%" style="padding:0">Marks Scored :</td>
                    <td width="10%" style="padding:0"> </td>
                </tr>';

            }
                 $header.='<tr>
                    <td width="100%" colspan=4 >Instructions for Student : '.$schoolhw->TM_SCH_Comments.'</td>
                </tr>
            </table></div>';
            $html=$header.$html;

             $this->render('printpdf',array('html'=>$html));
             //$html=$header.$html;
             //$mpdf = new mPDF();
             //$name='WORKSHEET'.time().'.pdf';
            // $mpdf->WriteHTML($html);
             //$mpdf->Output($name, 'I');
            exit;
            /*$header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                    <td width="33%"><span style="font-weight: lighter; color: #afafaf; ">For use by '.$school->TM_SCL_Name.' only</span></td>
                    <td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
                    <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                    <td width="33%" align="center" style="font-weight: lighter; "></td>
                    </tr></table>';*/

            $mpdf = new mPDF();

            //$mpdf->SetHTMLHeader($header);
            $mpdf->SetWatermarkText($username, .1);
            $mpdf->showWatermarkText = false;

            $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
                <tr>
                    <td width="100%" colspan="2">
                        &copy; Copyright @ TabbieMe Ltd. All rights reserved .
                    </td>
                </tr>
                <tr>
                    <td width="80%" >
                        www.tabbiemath.com  - The one stop shop for Maths Revision.
                    </td>
                    <td align="right" >Page #{PAGENO}</td>
                </tr>
            </table>');

            //echo $html;exit;
            $name='WORKSHEET'.time().'.pdf';
            $mpdf->WriteHTML($html);
            $mpdf->Output($name, 'I');
        else:
            $this->redirect(Yii::app()->request->baseUrl."/worksheets/".$schoolhomework->TM_SCH_Worksheet);
        endif;

    }
    //ends
    /**
     * print solution
     */
     public function actionPrintsolution($id)
    {
        $hwrk=Schoolhomework::model()->findByPK($id)->TM_SCH_Solution;
        if($hwrk==''):
        $homeworkqstns=HomeworkQuestions::model()->findAll(array(
            'condition' => 'TM_HQ_Homework_Id=:test',
            'params' => array(':test' => $id),
            'order'=>'TM_HQ_Order ASC'
        ));
        $username=User::model()->findByPk(Yii::app()->user->id)->username;
        $homework=Schoolhomework::model()->findByPK($id)->TM_SCH_Name;
        $html = "<style>
            p{
            margin-top: 0;
            margin-bottom: 0;
            }
            </style>
            <table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";
        //$html .= "<tr><td colspan=3 align='center'><b><u>Homework : ".$homework."<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'></td></tr>";
        foreach ($homeworkqstns AS $key => $homeworkqstn):
            $homeworkqstncount = $key + 1;
            $question = Questions::model()->findByPk($homeworkqstn->TM_HQ_Question_Id);
            //print_r($question);exit;
                if ($question->TM_QN_Type_Id!='7'):
                    $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td colspan='3' >".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                    endif;
                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html .= "<tr><td width='6%'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                            endif;
                        endforeach;
                    endif;
                    $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>Sol.</b></td><td style='padding:0;'>".$question->TM_QN_Solutions."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                    if ($question->TM_QN_Solution_Image != ''):
                        $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        // $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>Part." . $countpart."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>(" . $displaymark . ")</td></tr>";
                    $html .= "<tr></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>Sol.</b></td><td style='padding:0;'>".$question->TM_QN_Solutions."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                    if ($question->TM_QN_Solution_Image != ''):
                        $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
        endforeach;
        $html .= "</table>";
        $schoolhw=Schoolhomework::model()->findByPK($id);
            $school=School::model()->findByPK($schoolhw->TM_SCH_School_Id);
            $standard=Standard::model()->findByPk($schoolhw->TM_SCH_Standard_Id)->TM_SD_Name;
            $total=$this->GetMocktotal($id);
            $header = '<div>
            <table cellpadding="5" border="0" style="margin-bottom:10px;font-size:14px;font-family: STIXGeneral;" width="100%">
                <tr><td colspan=4 align="center" style="padding:0;"><b>'.$school->TM_SCL_Name.'</b></td></tr>
                <tr><td colspan=4 align="center" style="padding:0;"><b>'.$homework.'</b></td></tr>

            </table></div>';
            $html=$header.$html;

            $this->render('printpdf',array('html'=>$html));
            exit;
       //  $mpdf = new mPDF();

       // // $mpdf->SetHTMLHeader($header);
       //  $mpdf->SetWatermarkText($username, .1);
       //  $mpdf->showWatermarkText = true;

       //  $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
       //       <tr>
       //           <td width="100%" colspan="2">
       //               &copy; Copyright @ TabbieMe Ltd. All rights reserved .
       //           </td>
       //       </tr>
       //       <tr>
       //           <td width="80%" >
       //               www.tabbiemath.com  - The one stop shop for Maths Revision.
       //           </td>
       //              <td align="right" >Page #{PAGENO}</td>
       //       </tr>
       //      </table>');

       //  //echo $html;exit;
       //  $name='HOMEWORKSOLUTION'.time().'.pdf';
       //  $mpdf->WriteHTML($html);
       //  $mpdf->Output($name, 'I');
        else:
            $this->redirect(Yii::app()->request->baseUrl."/worksheets/".$hwrk);
        endif;
    }
    public function actionPublishCustomHomework()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
        $standard=$_POST['publishto'];
        // change name of assessment while publishing
        if(isset($_POST['practiceName']))
        {
            $assessmentNewName = $_POST['practiceName'];
            if($assessmentNewName != '')
            {
                $template=Customtemplate::model()->findByPk($templateid);
                $template->TM_SCT_Name = $assessmentNewName;
                $template->save(false);


            }
        }
		$homeworkid=$this->InsertTemlapteQuestions($templateid,$standard);
        $homeworktotal=$this->Gethomeworktotal($homeworkid);
		if(count($groupid)> 0):

            for ($i = 0; $i < count($groupid); $i++) {

                $grpid=$groupid[$i];
                //$grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                $grpstudents=GroupStudents::model()->findAll(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'TM_GRP_Id='.$grpid.' AND b.status=1'));
                $this->Addhomeworkgroup($homeworkid,$grpid);
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];
                    $studhomework->TM_STHW_ShowMark=$_POST['showmark'];
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                        $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                        $studhomework->save(false);
                    }

                endforeach;
            }
        endif;
        $homework = Schoolhomework::model()->findByPk($homeworkid);
        $homework->TM_SCH_Status='1';
        $homework->TM_SCH_CreatedOn=date('Y-m-d');
        $homework->TM_SCH_DueDate=$_POST['dudate'];
        $homework->TM_SCH_Comments=$_POST['comments'];
        $homework->TM_SCH_PublishDate=$_POST['publishdate'];
        $homework->TM_SCH_PublishType=$_POST['type'];
        $homework->TM_SCH_ShowSolution=$_POST['solution'];
        $homework->TM_SCH_ShowMark=$_POST['showmark'];
		$homework->TM_SCH_CreatedBy=Yii::app()->user->id;
        if($homework->save(false)):
                echo "yes";
        endif;
    }
    public function actionRepublishhomework()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
    	$homeworkid=$this->InsertQuestions($templateid);
    	$homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                //$grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                $grpstudents=GroupStudents::model()->findAll(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'TM_GRP_Id='.$grpid.' AND b.status=1'));
                $this->Addhomeworkgroup($homeworkid,$grpid);
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
					$studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        $studhomework->save(false);
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                    }
                endforeach;
            }
        endif;
            $homework = Schoolhomework::model()->findByPk($homeworkid);
            $homework->TM_SCH_Status='1';
            $homework->TM_SCH_Name=$_POST['name'];
            $homework->TM_SCH_DueDate=$_POST['dudate'];
            $homework->TM_SCH_Comments=$_POST['comments'];
            $homework->TM_SCH_PublishDate=$_POST['publishdate'];
            $homework->TM_SCH_PublishType=$_POST['type'];
            $homework->TM_SCH_ShowSolution=$_POST['solution'];
            $homework->TM_SCH_CreatedBy=Yii::app()->user->id;
            $homework->save(false);
    }
	public function InsertTemlapteQuestions($homeworkid,$standard)
	{
			$template = Customtemplate::model()->findByPk($homeworkid);
			$schoolhomework=new Schoolhomework();
			$schoolhomework->TM_SCH_Name=$template->TM_SCT_Name;
			$schoolhomework->TM_SCH_Description=$template->TM_SCT_Description;
			$schoolhomework->TM_SCH_School_Id=$template->TM_SCT_School_Id;
			$schoolhomework->TM_SCH_Publisher_Id=$template->TM_SCT_Publisher_Id;
			$schoolhomework->TM_SCH_Syllabus_Id=$template->TM_SCT_Syllabus_Id;
			$schoolhomework->TM_SCH_Standard_Id=$standard;
			$schoolhomework->TM_SCH_Master_Stand_Id=$template->TM_SCT_Standard_Id;
			 if($template->TM_SCT_Type == 1)
                $schoolhomework->TM_SCH_Type='22';
            else
                $schoolhomework->TM_SCH_Type='1';
			$schoolhomework->TM_SCH_Status='1';
			$schoolhomework->TM_SCH_CreatedOn=$template->TM_SCT_CreatedOn;
			$schoolhomework->TM_SCH_CreatedBy=$template->TM_SCT_CreatedBy;
			$schoolhomework->save(false);
			$questions= CustomtemplateQuestions::model()->findAll(array('condition'=>'TM_CTQ_Custom_Id='.$homeworkid));
			foreach($questions AS $question):
				$homeworkquestion=new HomeworkQuestions();
				$homeworkquestion->TM_HQ_Homework_Id=$schoolhomework->TM_SCH_Id;
				$homeworkquestion->TM_HQ_Question_Id=$question->TM_CTQ_Question_Id;
				$homeworkquestion->TM_HQ_Type=$question->TM_CTQ_Type;
				$homeworkquestion->TM_HQ_Order=$question->TM_CTQ_Order;
                		$homeworkquestion->TM_HQ_Header=$question->TM_CTQ_Header;
                		$homeworkquestion->TM_HQ_Section=$question->TM_CTQ_Section;
				$homeworkquestion->TM_HQ_Show_Options=$question->TM_CTQ_Show_Options;
				$homeworkquestion->save(false);
			endforeach;
			return $schoolhomework->TM_SCH_Id;

	}
    public function InsertQuestions($homeworkid)
	{
			$template = Schoolhomework::model()->findByPk($homeworkid);
			$schoolhomework=new Schoolhomework();
			$schoolhomework->TM_SCH_Name=$template->TM_SCH_Name;
			$schoolhomework->TM_SCH_Description=$template->TM_SCH_Description;
			$schoolhomework->TM_SCH_School_Id=$template->TM_SCH_School_Id;
			$schoolhomework->TM_SCH_Publisher_Id=$template->TM_SCH_Publisher_Id;
			$schoolhomework->TM_SCH_Syllabus_Id=$template->TM_SCH_Syllabus_Id;
			$schoolhomework->TM_SCH_Standard_Id=$template->TM_SCH_Standard_Id;
			$schoolhomework->TM_SCH_Type=$template->TM_SCH_Type;
			$schoolhomework->TM_SCH_Status='1';
			$schoolhomework->TM_SCH_CreatedOn=$template->TM_SCH_CreatedOn;
			$schoolhomework->TM_SCH_CreatedBy=$template->TM_SCH_CreatedBy;
			$schoolhomework->save(false);
			$questions= HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$homeworkid));
			foreach($questions AS $question):
				$homeworkquestion=new HomeworkQuestions();
				$homeworkquestion->TM_HQ_Homework_Id=$schoolhomework->TM_SCH_Id;
				$homeworkquestion->TM_HQ_Question_Id=$question->TM_HQ_Question_Id;
				$homeworkquestion->TM_HQ_Type=$question->TM_HQ_Type;
				$homeworkquestion->TM_HQ_Order=$question->TM_HQ_Order;
				$homeworkquestion->save(false);
			endforeach;
			return $schoolhomework->TM_SCH_Id;

	}
    public function PublishHomework($homeworkid,$groupid,$duedate,$comments,$publishdate,$type,$solution)
    {
        $school=Yii::app()->session['school'];
        $homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                //$grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                $grpstudents=GroupStudents::model()->findAll(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'TM_GRP_Id='.$grpid.' AND b.status=1'));
                $this->Addhomeworkgroup($homeworkid,$grpid);
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal
                    if($publishdate==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$duedate;
                    $studhomework->TM_STHW_Comments=$comments;
                    $studhomework->TM_STHW_PublishDate=$publishdate;
                    $studhomework->TM_STHW_Type=$type;
                    $studhomework->TM_STHW_ShowSolution=$solution;
                    $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        $studhomework->save(false);
                        if($publishdate==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                    }
                endforeach;
            }
        endif;
            $homework = Schoolhomework::model()->findByPk($homeworkid);
            $homework->TM_SCH_Status='1';
            $homework->TM_SCH_DueDate=$duedate;
            $homework->TM_SCH_Comments=$comments;
            $homework->TM_SCH_PublishDate=$publishdate;
            $homework->TM_SCH_PublishType=$type;
            $homework->save(false);
    }

    /**
     * homework assessment
     */
    public function actionQuestionstatus($id)
    {
        $this->layout = '//layouts/teacher';
        $homework = Schoolhomework::model()->findByPk($id);
        ///$hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$id'",'order'=>'TM_HQ_Order ASC'));
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT TM_HQ_Homework_Id,TM_HQ_Question_Id,TM_HQ_Order FROM tm_homework_questions WHERE TM_HQ_Homework_Id='".$id."' ORDER BY TM_HQ_Order ASC";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $hwquestions=$dataReader->readAll();
        $studhws_attempt=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        //echo count($studhws_attempt);exit;
        $attstds='';
        foreach($studhws_attempt AS $key=>$studhws_atte):
            if($key==0):
                $attstds.=$studhws_atte['TM_STHW_Student_Id'];
            else:
                $attstds.=','.$studhws_atte['TM_STHW_Student_Id'];
            endif;
        endforeach;
        //print_r($hwquestions);
        $this->render('questionstatus',array('homework'=>$homework,'hwquestions'=>$hwquestions,'attstds'=>$attstds));
    }
    public function actionHomeworkstatus($id)
    {
        $this->layout = '//layouts/teacher';
        $homework = Schoolhomework::model()->findByPk($id);
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $notstartedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));

        //print_r($hwquestions);
        $this->render('homeworkstatus',array('homework'=>$homework,'startedstudents'=>$startedstudents,'notstartedstudents'=>$notstartedstudents));
    }
    public function GetIndividualquestionstatus($question,$homework)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT SUM(TM_STHWQT_Marks) AS studenttotal FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Question_Id='".$question."' AND TM_STHWQT_Mock_Id='".$homework."' AND TM_STHWQT_Marks!=0";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $studenttotal=$dataReader->read();
		$totalstudentscorrect=$studenttotal['studenttotal'];
        $sqltotal="SELECT (SUM(c.TM_AR_Marks)) AS totalmarks FROM tm_studenthomeworkquestions AS a,tm_question AS b,tm_answer AS c WHERE a.TM_STHWQT_Question_Id=b.TM_QN_Id AND c.TM_AR_Question_Id=a.TM_STHWQT_Question_Id AND a.TM_STHWQT_Mock_Id='".$homework."' AND a.TM_STHWQT_Question_Id='".$question."' AND c.TM_AR_Correct=1 AND a.TM_STHWQT_Student_Id!=0";
        $command=$connection->createCommand($sqltotal);
        $dataReader=$command->query();
        $questionttotal=$dataReader->read();
		$totalstudents=$questionttotal['totalmarks'];
		//$totalstudents=Studenthomeworkquestions::model()->count(array('condition'=>'TM_STHWQT_Question_Id='.$question.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Student_Id!=0'));
        //$totalstudentscorrect=Studenthomeworkquestions::model()->count(array('condition'=>'TM_STHWQT_Question_Id='.$question.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Marks!=0 AND TM_STHWQT_Student_Id!=0'));
        if($totalstudents!=0):
            if($totalstudentscorrect!=0 || $totalstudentscorrect!=''):
                $percentage = ($totalstudentscorrect / $totalstudents) * 100;
            else:
                $percentage = 0;
            endif;
        else:
              $percentage = 0;
        endif;
        return round($percentage);
    }

    public function GetIndividualpaperquestionstatus($question,$homework)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT SUM(TM_STHWQT_Marks) AS studenttotal FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Question_Id='".$question."' AND TM_STHWQT_Mock_Id='".$homework."' AND TM_STHWQT_Marks!=0";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $studenttotal=$dataReader->read();
        $totalstudentscorrect=$studenttotal['studenttotal'];
        $sqltotal="SELECT (SUM(b.TM_QN_Totalmarks)) AS totalmarks FROM tm_studenthomeworkquestions AS a,tm_question AS b WHERE a.TM_STHWQT_Question_Id=b.TM_QN_Id AND a.TM_STHWQT_Mock_Id='".$homework."' AND a.TM_STHWQT_Question_Id='".$question."' AND a.TM_STHWQT_Student_Id!=0";
        $command=$connection->createCommand($sqltotal);
        $dataReader=$command->query();
        $questionttotal=$dataReader->read();
        $totalstudents=$questionttotal['totalmarks'];
        ///echo $totalstudents;exit;
        //$totalstudents=Studenthomeworkquestions::model()->count(array('condition'=>'TM_STHWQT_Question_Id='.$question.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Student_Id!=0'));
        //$totalstudentscorrect=Studenthomeworkquestions::model()->count(array('condition'=>'TM_STHWQT_Question_Id='.$question.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Marks!=0 AND TM_STHWQT_Student_Id!=0'));
        if($totalstudents!=0):
            if($totalstudentscorrect!=0 || $totalstudentscorrect!=''):
                $percentage = ($totalstudentscorrect / $totalstudents) * 100;
            else:
                $percentage = 0;
            endif;
        else:
            $percentage = 0;
        endif;
        return round($percentage);
    }
    /**
     * Homework assessment ends
     */
    public function actionImportTeacher($id)
    {
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        $importlog=array();
        if(isset($_POST['submit'])):
            $file = fopen($_FILES['importcsv']['tmp_name'], "r");
            $count=1;
            $values=fgetcsv($file, 1000, ",");
            while(($values=fgetcsv($file, 1000, ","))!==FALSE)
            {
                if($values[0]!=''):
                    if($values[9]=='Teacher'):
                        $usertype=9;
                    else:
                        $usertype=12;
                    endif;
                    if($values[6]=='Active'):
                        $status=1;
                    else:
                        $status=0;
                    endif;
                    if($values[10]=='Yes'):
                        $questions=1;
                    else:
                        $questions=0;
                    endif;
                    if($values[11]=='Professional'):
                        $type=1;
                    else:
                        $type=0;
                    endif;
                    $user=User::model()->find(array(
                            'condition' => 'username=:name AND usertype IN (9,12)',
                            'params' => array(':name' => $values[2],)
                        )
                    );

                    if(User::model()->count('id=:value',array(':value'=>$user->id)))
                    {
                        $model=User::model()->findByPk($user->id);
                        $model->username=$values[2];
                        if($values[3]!=''):
                            $model->password = UserModule::encrypting($values[3]);
                        endif;
                        $model->email=$values[2];
                        $model->usertype = $usertype;
                        $model->status = $status;
                        $model->save(false);
                        $profile=Profile::model()->findByPk($model->id);
                        $profile->firstname = $values[0];
                        $profile->lastname = $values[1];
                        $profile->phonenumber = $values[8];
                        $profile->save(false);
                        $staffschool=Teachers::model()->find(array(
                                'condition' => 'TM_TH_User_Id=:id',
                                'params' => array(':id' => $model->id)
                            )
                        );
                        $staffschool->TM_TH_Name = $profile->firstname . $profile->lastname;
                        $staffschool->TM_TH_Manage_Questions = $questions;
                        $staffschool->TM_TH_UserType = $type;
                        $staffschool->save(false);
                        $criteria = new CDbCriteria;
                        $criteria->addCondition('TM_TE_ST_User_Id=' . $model->id);
                        $teacherstds = TeacherStandards::model()->deleteAll($criteria);
                        if($values[7]!='No standards assigned')
                        {
                            $standards = explode(',', $values[7]);
                            foreach($standards AS $standard):
                                $data=Standard::model()->find(array(
                                        'condition' => 'TM_SD_Name=:id',
                                        'params' => array(':id' => $standard)
                                    )
                                );
                                $tchrstds=New TeacherStandards();
                                $tchrstds->TM_TE_ST_Standard_Id=$data->TM_SD_Id;
                                $tchrstds->TM_TE_ST_User_Id=$model->id;
                                $tchrstds->TM_TE_ST_Teacher_Id=$staffschool->TM_TH_Id;
                                $tchrstds->save(false);
                            endforeach;
                        }
                        $importlog[$count]['teacher']='1';
                    }
                    else
                    {
                        if($values[3]==''):
                            $password=$this->generateRandomString();
                        else:
                            $password=$values[3];
                        endif;
                        $model = new User;
                        $profile = new Profile;
                        $staffschool = new Teachers;
                        $model->usertype = $usertype;
                        $model->school_id = $id;
                        $model->username=$values[2];
                        $model->email=$values[2];
                        $model->activkey = UserModule::encrypting(microtime() . $password);
                        $model->password = UserModule::encrypting($password);
                        $model->createtime = time();
                        $model->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
                        $model->superuser = 0;
                        $model->status = '0';
                        $importlog[$count]['status']=$this->GetImportitems($values);
                        if($importlog[$count]['status']==''):
                            $model->save(false);
                            $importlog[$count]['teacher']='1';
                            $profile->user_id = $model->id;
                            $profile->firstname = $values[0];
                            $profile->lastname = $values[1];
                            $profile->phonenumber = $values[8];
                            $profile->save(false);
                            $staffschool->TM_TH_User_Id = $model->id;
                            $staffschool->TM_TH_Name = $profile->firstname . $profile->lastname;
                            $staffschool->TM_TH_SchoolId = $id;
                            $staffschool->TM_TH_Manage_Questions = $questions;
                            $staffschool->TM_TH_UserType = $type;
                            $staffschool->TM_TH_CreatedBy=Yii::app()->user->id;
                            $staffschool->save(false);
                            //change by amal
                            if($values[7]!='No standards assigned' || $values[7]!='')
                            {
                                $standards = explode(',', $values[7]);
                                foreach($standards AS $standard):
                                    $data=Standard::model()->find(array(
                                            'condition' => 'TM_SD_Name=:id',
                                            'params' => array(':id' => $standard)
                                        )
                                    );
                                    $tchrstds=New TeacherStandards();
                                    $tchrstds->TM_TE_ST_Standard_Id=$data->TM_SD_Id;
                                    $tchrstds->TM_TE_ST_User_Id=$model->id;
                                    $tchrstds->TM_TE_ST_Teacher_Id=$staffschool->TM_TH_Id;
                                    $tchrstds->save(false);
                                endforeach;
                                $teacheruser=User::model()->find(array(
                                        'condition' => 'id=:teacherid',
                                        'params' => array(':teacherid' => $model->id,)
                                    )
                                );
                                $teacheruser->status='1';
                                $teacheruser->save(false);
                            }
                            //ends

                            $this->SendTeacherMail($model->id,$password);
                        else:
                            $importlog[$count]['teacher']='0';
                        endif;
                    }
                endif;
                $count++;
            }
        endif;
        $this->render('ImportTeacher', array(
            'id' => $id,
            'importlog'=>$importlog
        ));
    }
    public function GetImportitems($values)
    {
        $message="";
        /*if(User::model()->count('username=:value',array(':value'=>$values)))
        {
            $messagearray['message'].='Username \''.$values.'\' Exists<br>';
            $countarray=1;
        }*/
        if(count(array_filter($values))<8)
        {
            $message.='Invalid File Format ';
        }
        if(User::model()->count('email=:value',array(':value'=>$values[2])))
        {
            $message.='Email \''.$values[2].'\' Exists<br>';

        }

        return $message;
    }

    public function actionMockorder()
    {

        if (isset($_POST['id']))
        {
            $info=$_POST['info'];
            $id=$_POST['id'];
            $order=$_POST['order'];
            if (($_POST['info']=='up')):
                $modelold=CustomtemplateQuestions::model()->find(array(
                        'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order<:order ',
                        'limit' => 1,
                        'order'=>'TM_CTQ_Order DESC',
                        'params' => array(':id' => $id, ':order' => $order)
                    )
                );

                $model=CustomtemplateQuestions::model()->find(array(
                        'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order=:order',
                        'params' => array(':id' => $id, ':order' => $order)
                    )
                );
                $temp=$modelold->TM_CTQ_Order;
                $modelold->TM_CTQ_Order=$model->TM_CTQ_Order;
                $model->TM_CTQ_Order=$temp;
                $modelold->save(false);
                $model->save(false);
                echo "yes";
            else:
                    $modelold=CustomtemplateQuestions::model()->find(array(
                            'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order>:order ',
                            'limit' => 1,
                            'order'=>'TM_CTQ_Order ASC',
                            'params' => array(':id' => $id, ':order' => $order)
                        )
                    );

                    $model=CustomtemplateQuestions::model()->find(array(
                            'condition' => 'TM_CTQ_Custom_Id=:id AND  TM_CTQ_Order=:order',
                            'params' => array(':id' => $id, ':order' => $order)
                        )
                    );
                    $temp=$modelold->TM_CTQ_Order;
                    $modelold->TM_CTQ_Order=$model->TM_CTQ_Order;
                    $model->TM_CTQ_Order=$temp;
                    $modelold->save(false);
                    $model->save(false);
                    echo "yes";
            endif;

        }
    }
    public function actionEditHomework()
    {
        $this->layout = '//layouts/teacher';
        $homework = Schoolhomework::model()->findByPk($_POST['id']);

        $modaldiv='';

        $modaldiv .='

                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publish Date <span class="glyphicon glyphicon-calendar"></span></label>
                            <div class="col-sm-7">
                                <input type="date" id="publishdate" class="form-control" name="publishdate"
                                       value="';
        $datetime = new DateTime($homework->TM_SCH_PublishDate);
        $modaldiv .= $datetime->format('Y-m-d') . '">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Due On <span class="glyphicon glyphicon-calendar"></span></label>
                            <div class="col-sm-7">
                                <input type="date" id="duedate" class="form-control" name="duedate"
                                       value="';
        $datetime = new DateTime($homework->TM_SCH_DueDate);
        $modaldiv .= $datetime->format('Y-m-d').'">
</div>
</div>
</div>
<div class="row">
    <div class="form-group">
        <label class="col-sm-3 control-label">To be submitted</label>
        <div class="col-sm-7">';
        if ($homework->TM_SCH_PublishType == 2):

            $modaldiv .= '<input type="radio" class="type" name="type" value="2" checked="checked"/><span>Online </span>
                        <input type="radio" class="type" name="type" value="3"/><span>Worksheet </span>
                        <input type="radio" class="type" name="type" value="1"/><span>Worksheet (with options)</span>';
        elseif ($homework->TM_SCH_PublishType == 3):

            $modaldiv .= '<input type="radio" class="type" name="type" value="2" /><span>Online</span>
                        <input type="radio" class="type" name="type" value="3" checked="checked"/><span>Worksheet</span>
                        <input type="radio" class="type" name="type" value="1"/><span>Worksheet (with options)</span>';
        else:
            $modaldiv .= '<input type="radio" class="type" name="type" value="2"/><span>Online</span>
                        <input type="radio" class="type" name="type" value="3"/><span>Worksheet</span>
                        <input type="radio" class="type" name="type" checked="checked" value="1"/><span>Worksheet (with options)</span>';
        endif;
        $modaldiv .= '</div>
    </div>
</div>


<div class="row">
    <div class="form-group">
        <label class="col-sm-3 control-label">Show Solution</label>

        <div class="col-sm-7">';
        if ($homework->TM_SCH_ShowSolution == 1):
            $modaldiv .= '<input type="radio" class="showsolution" name="solution" value="1"
                       checked="checked"/><span>Yes</span>
                <input type="radio" class="showsolution" name="solution" value="0"/><span>No</span>';
        else:
            $modaldiv .= '<input type="radio" class="showsolution" name="solution" value="1"
                       /><span>Yes</span>
                <input type="radio" class="showsolution" name="solution" value="0" checked="checked"/><span>No</span>';

        endif;
        $modaldiv .= '</div>
    </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-3 control-label">Show Mark</label>

            <div class="col-sm-7">';
            if ($homework->TM_SCH_ShowMark == 0):
                $modaldiv .= '<input type="radio" class="showmarks" name="mark" value="0"
                           checked="checked"/><span>Yes</span>
                    <input type="radio" class="showmarks" name="mark" value="1"/><span>No</span>';
            else:
                $modaldiv .= '<input type="radio" class="showmarks" name="mark" value="0"
                           /><span>Yes</span>
                    <input type="radio" class="showmarks" name="mark" value="1" checked="checked"/><span>No</span>';

            endif;
            $modaldiv .= '</div>
        </div>
    </div>

<div class="row">
    <div class="form-group">
        <label class="col-sm-3 control-label">Comments</label>

        <div class="col-sm-7">
            <textarea name="comments" class="form-control"
                      id="comments">';
        $modaldiv .= $homework->TM_SCH_Comments . '</textarea>
        </div>
</div>
</div>
';


        //echo $modaldiv;
        $homeworkgroups=Homeworkgroup::model()->findAll(array('condition'=>'TM_HMG_Homework_Id='.$_POST['id'].' '));
        $grpids=array();
        foreach($homeworkgroups AS $key=>$homeworkgroup):
            array_push($grpids,$homeworkgroup['TM_HMG_Groupe_Id']);
        endforeach;
        $arr=array('result'=>$modaldiv,'grpid'=>$grpids);
        echo json_encode($arr);
        //$this->render('edithomework', array('homework'=>$homework));
    }
    public function actionchangehomeworkdetails()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['id'];
        $groupid=$_POST['nameids'];
    	$homeworkid=$_POST['id'];
    	$homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):
            Homeworkgroup::model()->deleteAll(array('condition'=>'TM_HMG_Homework_Id='.$homeworkid.' '));
            StudentHomeworks::model()->deleteAll(array('condition'=>'TM_STHW_HomeWork_Id='.$homeworkid.' '));
            Studenthomeworkquestions::model()->deleteAll(array('condition'=>'TM_STHWQT_Mock_Id='.$homeworkid.' '));
            Notifications::model()->deleteAll(array('condition'=>'TM_NT_Item_Id='.$homeworkid.' '));
            for ($i = 0; $i < count($groupid); $i++) {
                $grpid=$groupid[$i];
                //$grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                $grpstudents=GroupStudents::model()->findAll(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'TM_GRP_Id='.$grpid.' AND b.status=1'));
                $hwgrpcount=Homeworkgroup::model()->count(array('condition'=>'TM_HMG_Homework_Id='.$homeworkid.' AND TM_HMG_Groupe_Id='.$grpid.' '));
                if($hwgrpcount==0):
                    $this->Addhomeworkgroup($homeworkid,$grpid);
                endif;
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
					$studhomework->TM_STHW_TotalMarks=$homeworktotal;
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];
                    $studhomework->TM_STHW_ShowMark=$_POST['showmark'];
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        $studhomework->save(false);
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                    }
                endforeach;
            }
        endif;
		$homework = Schoolhomework::model()->findByPk($_POST['id']);
        $homework->TM_SCH_Status = '1';
        $homework->TM_SCH_DueDate = $_POST['dudate'];
        $homework->TM_SCH_Comments = $_POST['comments'];
        $homework->TM_SCH_PublishDate = $_POST['publishdate'];
        $homework->TM_SCH_PublishType = $_POST['type'];
        $homework->TM_SCH_ShowSolution = $_POST['solution'];
        $homework->TM_SCH_ShowMark = $_POST['showmark'];
        $homework->TM_SCH_Standard_Id=$_POST['publishto'];
        $homework->save(false);
        $studenthomeworks=StudentHomeworks::model()->findAll(array('condition'=>'TM_STHW_HomeWork_Id='.$_POST['id'].' '));
        foreach($studenthomeworks AS $studenthomework):
            $studenthomework->TM_STHW_DueDate = $_POST['dudate'];
            $studenthomework->TM_STHW_Comments = $_POST['comments'];
            $studenthomework->TM_STHW_PublishDate = $_POST['publishdate'];
            $studenthomework->TM_STHW_Type = $_POST['type'];
            $studenthomework->TM_STHW_ShowSolution = $_POST['solution'];
            $studenthomework->TM_STHW_ShowMark = $_POST['showmark'];
            $studenthomework->save(false);
        endforeach;
		echo "yes";
    }

    public function actionPasswordchange()
    {
        $this->layout = '//layouts/teacher';
        $model = new UserChangePassword;
        if (Yii::app()->user->id) {
            // ajax validator
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'changepassword-form') {
                echo UActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['UserChangePassword'])) {
                $model->attributes = $_POST['UserChangePassword'];
                if ($model->validate()) {
                    $new_password = User::model()->findbyPk(Yii::app()->user->id);
                    $new_password->password = UserModule::encrypting($model->password);
                    $new_password->activkey = UserModule::encrypting(microtime() . $model->password);
                    $new_password->save(false);
                    Yii::app()->user->setFlash('profileMessage', UserModule::t("New password is saved."));
                    $this->refresh();
                }
            }
            $this->render('changepassword', array('model' => $model));
        }
    }
    public function actionSubmitstudent()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
        $criteria->params = array(':test' => $_POST['homework'], 'student' => $_POST['student']);
        $homework=StudentHomeworks::model()->find($criteria);
        $papertotal=$homework->TM_STHW_TotalMarks;
        $paperquestions= Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$_POST['homework'].' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id='.$_POST['student']));
        foreach($paperquestions AS $question):
            $question->TM_STHWQT_Marks='0';
            $question->save(false);
        endforeach;
        $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
        $test->TM_STHW_Status = '4';
        $test->TM_STHW_TotalMarks = $papertotal;
        $test->save(false);
        echo "yes";
    }
    public function actionSubmitall($id)
    {
        $notstarted=StudentHomeworks::model()->findAll(array('condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)'));
        foreach($notstarted AS $test):
            $paperquestions= Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$id.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id='.$test->TM_STHW_Student_Id));
            foreach($paperquestions AS $question):
                $question->TM_STHWQT_Marks='0';
                $question->save(false);
            endforeach;
            $test->TM_STHW_Status = '4';
            $test->save(false);
        endforeach;
        $this->redirect(array('markhomework', 'id' =>$id));
    }
    public function Gethomeworktotal($homeworkid)
    {
        $questions= HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$homeworkid));
        $homeworktotal=0;
        foreach($questions AS $question):
            $question=Questions::model()->findByPk($question->TM_HQ_Question_Id);
            switch($question->TM_QN_Type_Id)
            {
                case 4:
                    $marks=Answers::model()->find(array('select'=>'TM_AR_Marks AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' '));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
                case 5:
                    $command = Yii::app()->db->createCommand();
                    $row = Yii::app()->db->createCommand(array(
                        'select' => array('SUM(TM_AR_Marks) AS totalmarks'),
                        'from' => 'tm_answer',
                        'where' => 'TM_AR_Question_Id IN (SELECT TM_QN_Id FROM 	tm_question WHERE TM_QN_Parent_Id='.$question->TM_QN_Id.')',
                    ))->queryRow();
                    $homeworktotal=$homeworktotal+$row['totalmarks'];
                    break;
                case 6:
                    //echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                case 7:
                    //echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                default:
                    $marks=Answers::model()->find(array('select'=>'SUM(TM_AR_Marks) AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' AND TM_AR_Correct=1'));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
            }

        endforeach;
        return $homeworktotal;
    }
    public function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    /**
     *
     * New teacher assessment report by karthik 08-12-2016
     *
     */
        public function actionAssessmentreport(){
        $this->layout = '//layouts/admincolumn2';
        $dataval=array();
        if(isset($_GET['search']))
        {
            $formvals=array();
            $formvals['date']=$_GET['date'];
            $formvals['school']=$_GET['school'];
            $formvals['schooltext']=$_GET['schooltext'];
            $formvals['teachername']=$_GET['teachername'];
            $formvals['plan']=$_GET['plan'];
            $formvals['fromdate']=$_GET['fromdate'];
            $formvals['todate']=$_GET['todate'];
            $connection = CActiveRecord::getDbConnection();
            $sqlcount="SELECT COUNT(*) AS total FROM tm_users AS a
                    LEFT JOIN  tm_profiles AS b ON b.user_id=a.id
                    LEFT JOIN  tm_school AS c ON c.TM_SCL_Id=a.school_id
                    LEFT JOIN  tm_schoolhomework AS d ON d.TM_SCH_CreatedBy=a.id
                    WHERE a.usertype IN (9,12)";
            $sql="SELECT a.id,a.email,b.lastname,b.firstname,b.phonenumber,c.TM_SCL_Name,d.TM_SCH_Id,d.TM_SCH_Type,d.TM_SCH_Status,d.TM_SCH_CreatedOn,d.TM_SCH_PublishDate,d.TM_SCH_PublishType,d.TM_SCH_ShowSolution,d.TM_SCH_DueDate,d.TM_SCH_Standard_Id
                    FROM tm_users AS a
                    LEFT JOIN  tm_profiles AS b ON b.user_id=a.id
                    LEFT JOIN  tm_school AS c ON c.TM_SCL_Id=a.school_id
                    LEFT JOIN  tm_schoolhomework AS d ON d.TM_SCH_CreatedBy=a.id
                    WHERE a.usertype IN (9,12)
                    ";
                    if($_GET['school']!==''):
                        $sql.=" AND a.school_id='".$_GET['school']."'";
                        $sqlcount.=" AND a.school_id='".$_GET['school']."'";
                    endif;
                    if($_GET['teachername']!==''):
                        $sql.=" AND (b.firstname LIKE '%".$_GET['teachername']."%' OR b.lastname LIKE '%".$_GET['teachername']."%' OR a.email LIKE '%".$_GET['teachername']."%')";
                        $sqlcount.=" AND (b.firstname LIKE '%".$_GET['teachername']."%' OR b.lastname LIKE '%".$_GET['teachername']."%' OR a.email LIKE '%".$_GET['teachername']."%')";;
                    endif;
                    if($_GET['date']=='before'):
                        if($_GET['fromdate']!=''):
                            $sql.=" AND d.TM_SCH_PublishDate < '".$_GET['fromdate']."' ";
                            $sqlcount.=" AND d.TM_SCH_PublishDate < '".$_GET['fromdate']."' ";
                        endif;
                    elseif($_GET['date']=='after'):
                        if($_GET['fromdate']!=''):
                            $sql.=" AND d.TM_SCH_PublishDate > '".$_GET['fromdate']."' ";
                            $sqlcount.=" AND d.TM_SCH_PublishDate > '".$_GET['fromdate']."' ";
                        endif;
                    else:
                        if($_GET['fromdate']!='' & $_GET['todate']!=''):
                            $sql.=" AND d.TM_SCH_PublishDate BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ";
                            $sqlcount.=" AND d.TM_SCH_PublishDate BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ";
                        endif;
                    endif;
                    $criteria='search=search&date='.$_GET['date'].'&school='.$_GET['school'].'&teachername='.$_GET['teachername'].'&fromdate='.$_GET['fromdate'].'&todate'.$_GET['todate'];
            $command=$connection->createCommand($sqlcount);
            $dataReader=$command->query();
            $datacount=$dataReader->read();
            $total_pages=$datacount['total'];
            $page = $_GET['page'];
            $limit = 50;
            if($page)
                $offset = ($page - 1) * $limit;             //first item to display on this page
            else
                $offset = 0;
            $sql.=" LIMIT $offset, $limit";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            if($_GET['page']==''):
                $count=1;
            else:
                $count=$_GET['page'];
            endif;
            $nopages=$this->Getpagination($criteria,$total_pages,$limit,$count,$_GET['page']);
        }
        $this->render('assessmentreport',array('formvals'=>$formvals,'dataval'=>$dataval,'nopages'=>$nopages));
    }
    public function GetCompletedAssessments($id)
    {
        $schoolid=Schoolhomework::model()->findByPk($id)->TM_SCH_School_Id;
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT COUNT(*) AS total FROM tm_student_homeworks AS a INNER JOIN tm_student AS b ON a.TM_STHW_Student_Id=b.TM_STU_User_Id  WHERE a.TM_STHW_HomeWork_Id='".$id."' AND a.TM_STHW_Status IN (4,5) AND (b.TM_STU_School=".$schoolid." OR b.TM_STU_School_Id=".$schoolid.") ";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $completed=$dataReader->read();
        return $completed['total'];
    }
    public function GetPendingAssessments($id)
    {
        $schoolid=Schoolhomework::model()->findByPk($id)->TM_SCH_School_Id;
        $connection = CActiveRecord::getDbConnection();
        //$sql="SELECT COUNT(*) AS total FROM tm_student_homeworks WHERE TM_STHW_HomeWork_Id='".$id."' AND TM_STHW_Status!='4'";
        $sql="SELECT COUNT(*) AS total FROM tm_student_homeworks AS a INNER JOIN tm_student AS b ON a.TM_STHW_Student_Id=b.TM_STU_User_Id INNER JOIN tm_users AS c ON a.TM_STHW_Student_Id=c.id WHERE a.TM_STHW_HomeWork_Id='".$id."' AND a.TM_STHW_Status NOT IN(4,5) AND (b.TM_STU_School=".$schoolid." OR b.TM_STU_School_Id=".$schoolid.") AND c.status=1";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $pending=$dataReader->read();
        return $pending['total'];
    }
    /*public function GetAssessmentstatus($id)
    {

    }*/
    public function GetQuestions($id)
    {
        $schoolhomework=Schoolhomework::model()->findByPk($id);
        if($schoolhomework->TM_SCH_Type!='3'):
            if($schoolhomework->TM_SCH_Video_Url!=''):
                return $schoolhomework->TM_SCH_Name;
            else:
            $connection = CActiveRecord::getDbConnection();
            $sql="SELECT q.TM_QN_QuestionReff
                    FROM tm_homework_questions AS hq
                    LEFT JOIN tm_question AS q ON hq.TM_HQ_Question_Id=q.TM_QN_Id
                    WHERE hq.TM_HQ_Homework_Id='".$id."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $questions=$dataReader->readAll();
            $hwquestion='';
            foreach($questions AS $key=>$question):
    			if($question['TM_QN_QuestionReff']!=''):
    				$hwquestion.=($key==0?'':' | ').$question['TM_QN_QuestionReff'];
    			endif;
            endforeach;
            return $hwquestion;
            endif;
        else:
            return $schoolhomework->TM_SCH_Name;
            /*if($schoolhomework->TM_SCH_Worksheet!=''):
                return $schoolhomework->TM_SCH_Worksheet;
            else:
                $connection = CActiveRecord::getDbConnection();
                $sql="SELECT q.TM_QN_QuestionReff
                        FROM tm_homework_questions AS hq
                        LEFT JOIN tm_question AS q ON hq.TM_HQ_Question_Id=q.TM_QN_Id
                        WHERE hq.TM_HQ_Homework_Id='".$id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $questions=$dataReader->readAll();
                $hwquestion='';
                foreach($questions AS $key=>$question):
        			if($question['TM_QN_QuestionReff']!=''):
        				$hwquestion.=($key==0?'':' | ').$question['TM_QN_QuestionReff'];
        			endif;
                endforeach;
                return $hwquestion;
            endif;*/
        endif;
    }
    public function GetStandard($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT TM_SD_Name FROM tm_standard WHERE TM_SD_Id='".$id."'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $standard=$dataReader->read();
        return $standard['TM_SD_Name'];
    }
    public function GetAssignedGroups($homework)
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_HMG_Groupe_Id,b.TM_SCL_GRP_Name FROM tm_homeworkgroup AS a LEFT JOIN tm_school_groups AS b ON a.TM_HMG_Groupe_Id=b.TM_SCL_GRP_Id WHERE a.TM_HMG_Homework_Id='".$homework."'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $groups=$dataReader->readAll();
        $groupnames='';
        foreach($groups AS $key=>$group):
            $groupnames.=($key==0?'':', ').$group['TM_SCL_GRP_Name'];
        endforeach;
        return $groupnames;

    }
    public function actionDownloadassessment(){
            $formvals=array();
            $formvals['date']=$_GET['date'];
            $formvals['school']=$_GET['school'];
            $formvals['schooltext']=$_GET['schooltext'];
            $formvals['teachername']=$_GET['teachername'];
            $formvals['plan']=$_GET['plan'];
            $formvals['fromdate']=$_GET['fromdate'];
            $formvals['todate']=$_GET['todate'];
            $connection = CActiveRecord::getDbConnection();
            $sql="SELECT a.id,a.email,b.lastname,b.firstname,b.phonenumber,c.TM_SCL_Name,d.TM_SCH_Id,d.TM_SCH_Type,d.TM_SCH_Status,d.TM_SCH_CreatedOn,d.TM_SCH_PublishDate,d.TM_SCH_PublishType,d.TM_SCH_ShowSolution,d.TM_SCH_DueDate,d.TM_SCH_Standard_Id
                    FROM tm_users AS a
                    LEFT JOIN  tm_profiles AS b ON b.user_id=a.id
                    LEFT JOIN  tm_school AS c ON c.TM_SCL_Id=a.school_id
                    LEFT JOIN  tm_schoolhomework AS d ON d.TM_SCH_CreatedBy=a.id
                    WHERE a.usertype IN (9,12)
                    ";
                    if($_GET['school']!==''):
                        $sql.=" AND a.school_id='".$_GET['school']."'";
                    endif;
                    if($_GET['teachername']!==''):
                        $sql.=" AND (b.firstname LIKE '%".$_GET['teachername']."%' OR b.lastname LIKE '%".$_GET['teachername']."%' OR a.email LIKE '%".$_GET['teachername']."%')";
                    endif;
                    if($_GET['date']=='before'):
                        if($_GET['fromdate']!=''):
                            $sql.=" AND d.TM_SCH_CreatedOn < '".$_GET['fromdate']."' ";
                        endif;
                    elseif($_GET['date']=='after'):
                        if($_GET['fromdate']!=''):
                            $sql.=" AND d.TM_SCH_CreatedOn > '".$_GET['fromdate']."' ";
                        endif;
                    else:
                        if($_GET['fromdate']!='' & $_GET['todate']!=''):
                            $sql.=" AND d.TM_SCH_CreatedOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ";
                        endif;
                    endif;
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=assessmentreport.csv');
            $output = fopen('php://output', 'w');
            $headers=array(
            "Date",
            "Teacher",
            "Email",
            "School",
            "Type",
            "Standard",
            "Submission",
            "Published On",
            "Completed",
            "Pending",
            "Status",
            "Groups",
            "Questions"
            );
            function cleanData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }
            fputcsv($output,$headers );
            if(count($dataval)>0):
                $flag = false;
                foreach($dataval AS $data):
                    switch ($data['TM_SCH_PublishType']) {
                        case 1:
                            $submission='Worksheet (with options)';
                            break;
                        case 2:
                            $submission='Online';
                            break;
                        case 3:
                            $submission='Worksheet';
                            break;
                    }
                    if($data['TM_SCH_Type']=='1'):
                        $assestype='Custom Practice';
                    elseif($data['TM_SCH_Type']=='3'):
                        $assestype='Worksheet';
                    elseif($data['TM_SCH_Type']=='33'):
                        $assestype='Resource';
                    elseif($data['TM_SCH_Type']=='22'):
                        $assestype='Blueprint';
                    else:
                        $assestype='Quick Practice';
                    endif;
                    $completed=$this->GetCompletedAssessments($data['TM_SCH_Id']);
                    $pending=$this->GetPendingAssessments($data['TM_SCH_Id']);
                    $row=array(date("d-m-Y", strtotime($data['TM_SCH_CreatedOn'])),$data['firstname'].' '.$data['lastname'],
                    $data['email'],$data['TM_SCL_Name'],$assestype,$this->GetStandard($data['TM_SCH_Standard_Id']),$submission,date("d-m-Y", strtotime($data['TM_SCH_PublishDate'])),
                    $completed,$pending,($data['TM_SCH_Status']=='2'?'Completed':'Open'),$this->GetAssignedGroups($data['TM_SCH_Id']),$this->GetQuestions($data['TM_SCH_Id']));

                    if(!$flag) {
                        // display field/column names as first row
                        fputcsv($output, $row);
                        $flag = true;
                    }
                    array_walk($row, 'cleanData');
                    fputcsv($output, $row);
                endforeach;
            endif;

    }
    public function Getpagination($criteria,$total_pages,$limit,$count,$currentpage)
    {
        $pages=ceil($total_pages / $limit);
        $nopages='';
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page):
                $class='class="active"';
            else:
                $class='';
            endif;
            $nopages.='<li><a  '.$class.' href="'.Yii::app()->request->baseUrl.'/teachers/assessmentreport?'.$criteria.'&page='.$i.'">'.$i.'</a></li>';
        }
        if($pages!=$currentpage):
            $nopages.='<li><a href="'.Yii::app()->request->baseUrl.'/teachers/assessmentreport?'.$criteria.'&page='.($count + 1).'">Next</a></li>';
        endif;
        return $nopages;
    }

    public function Getpaginationworksheet($criteria,$total_pages,$limit,$count,$currentpage)
    {
        $pages=ceil($total_pages / $limit);
        if($pages>0):
            if($count==1):
                $pclass="disabled";
            else:
                $pclass="";
            endif;
            if($currentpage!=1):
                $nopages='<li class="page-item '.$pclass.'">
                    <a class="page-link" aria-label="Previous" href="'.Yii::app()->request->baseUrl.'/teachers/WorksheetPrintReport?'.$criteria.'&page='.($count - 1).'">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span></a>
                    </li>';
            endif;
            for($i=1;$i<=$pages;$i++)
            {
                if($i==$currentpage):
                    $class='active';
                else:
                    $class='';
                endif;
                $nopages.='<li class="page-item '.$class.'"><a class="page-link"  href="'.Yii::app()->request->baseUrl.'/teachers/WorksheetPrintReport?'.$criteria.'&page='.$i.'">'.$i.'</a></li>';
            }
            if($pages!=$currentpage):
                $nopages.='<li class="page-item">
                        <a class="page-link" aria-label="Next" href="'.Yii::app()->request->baseUrl.'/teachers/WorksheetPrintReport?'.$criteria.'&page='.($count + 1).'">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span></a></li>';
            endif;
        else:
            $nopages='';
        endif;
        return $nopages;
    }
/**
 * Ends Here
 */
/**
 * function for infinite sccroll marking screen.
 */
    public function actionMarkinglist()
    {
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$_POST['homework'].' AND TM_STHW_Status IN (4,5)','offset' => $_POST['pagecount'],'limit'=>$_POST['limit'],'order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $content='';
        $script='';
        $count=$_POST['pagecount']+1;
        foreach($startedstudents AS $started):
            if($started->TM_STHW_Status=='4'):
            $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$started->TM_STHW_Student_Id));
            $content.='<li class="list-group-item">
                        <div class="row">
                            <div class="col-md-2 col-xs-6">
                            	<div class="doc" id="doc'.$started->TM_STHW_Student_Id.'">
                            		<div class="active item">
                            			<div class="carousel-info">
                            				<div class="pull-left">
                                            <span class="doc-name">'.$studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name.'</span>
                            					<div class="namebottom">
                            						<ul>
                                                    <li data-toggle="tooltip" data-placement="bottom" data-original-title="Reopen">
                                                            <a title="" class="reopen" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
                                                                <i style="font-size: 18px;" class="glyphicon glyphicon-remove"></i>
                                                            </a>
                                                        </li>
                            					       <li data-toggle="tooltip" data-placement="bottom" data-original-title="View Answer">
                            								<a title="" class="drop2 viewanswer" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
                            									<i class="fa fa-eye fa-2x"></i>
                            								</a>
                            							</li>';
    if($this->CheckAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id)):
        $hwanswers=$this->GetAnswersheetCount($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id);
        foreach($hwanswers AS $hwanswer):
        $ext = pathinfo($hwanswer['TM_STHWAR_Filename'], PATHINFO_EXTENSION);

       $content.='<li data-toggle="tooltip" data-placement="bottom" data-original-title="Download">';
        if($ext=='one'):
        $content.='<a href="'.Yii::app()->request->baseUrl."/teachers/download?file=".urlencode($hwanswer['TM_STHWAR_Filename']).'" title="" target="_blank" class="viewanswersheet" data-item="'.$hwanswer->TM_STHWAR_HW_Id.'" data-student="'.$hwanswer->TM_STHWAR_Student_Id.'">';
        else:
         $content.='<a href="'.Yii::app()->request->baseUrl."/homework/".$hwanswer['TM_STHWAR_Filename'].'" title="" target="_blank" class="viewanswersheet" data-item="'.$hwanswer->TM_STHWAR_HW_Id.'" data-student="'.$hwanswer->TM_STHWAR_Student_Id.'">';
        endif;
         $content.='<i class="fa fa-download fa-2x"></i>
				</a>
				</li>';
                                                            endforeach;
                                                        endif;



   $content.='<li data-toggle="tooltip" data-placement="bottom" data-original-title="Comments">';
      if($this->HasComments($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id)):
    $content.='<a href="javascript:void(0)" class="comments" title="Upload Comments" data-toggle="modal" data-target="#myModal" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
        <i class="fa fa-cloud-upload fa-2x"></i>
        </a>';
        else:
    $content.='<a href="javascript:void(0)" class="comments" title="Upload Comments" data-toggle="modal" data-target="#myModal" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
        <i style="color: #777777" class="fa fa-cloud-upload fa-2x"></i>
        </a>';
        endif;
    $content.='</li>';


                                                    if($this->HasWorksheeturl($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id)!='0'):
                                                        $homework_url=Studenthomeworkanswersurl::model()->find(array('condition'=>'TM_STHWARU_HW_Id='.$started->TM_STHW_HomeWork_Id.' AND TM_STHWARU_Student_Id='.$started->TM_STHW_Student_Id));
                                                        $homework_url->TM_STHWARU_Url;
                                                        $url='';
                                                        $search='https';
                                                        if(preg_match("/{$search}/i", $homework_url->TM_STHWARU_Url))
                                                        {
                                                           $url=$homework_url->TM_STHWARU_Url;

                                                        }
                                                        else
                                                        {
                                                             $url='https://'.$homework_url->TM_STHWARU_Url;
                                                        }

                                                $content.='<li><a href="'.$url.'" target="_blank" class="homeworkurl" title="Open Link"  data-value="'.$started->TM_STHW_HomeWork_Id.'"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a></li>';
                                                    endif;

														$criteria=new CDbCriteria;
														$criteria->condition='TM_HQ_Homework_Id='.$started->TM_STHW_HomeWork_Id.' AND TM_HQ_Type IN (6,7)';
														$totalpaper=HomeworkQuestions::model()->count($criteria);

                            							$content.='<li>
                            								<a title="" class="saveline" data-toggle="tooltip" data-placement="bottom" data-original-title="Save that line" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
                            									<i class="fa fa-floppy-o fa-2x"></i>
                            								</a>
                            							</li>
                            							<li>
                                                            <a title="" class="markcomplete" data-toggle="tooltip" data-placement="bottom" data-original-title="Mark it complete" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
                            									<i class="fa fa-check fa-2x"></i>
                            								</a>
                            							</li>
                            						</ul>
                            					</div>
                            				</div>
                            			</div>
                            		</div>
                            	</div>
                            </div>
                            <div class="col-md-1 col-xs-6">
                            </div>
                                <!--<div class="col-md-1 col-xs-6">
                                    <div class="btmy">
                                    <div id="pbar" class="progress-pie-chart gt-50" data-percent="0">
                                    <div class="ppc-progress">
                                    <div class="ppc-progress-fill" style="transform: rotate(316.8deg);"></div>
                                    </div>
                                    <div class="ppc-percents">
                                    <div class="pcc-percents-wrapper">
                                    <span>88%</span>
                                    </div>
                                    </div>
                                    </div>
                                    <progress style="display: none" id="progress_bar3" value="100" max="100"></progress>
                                    </div>
                                </div>-->
                            <div class="col-xs-12 col-md-9 col-lg-9">
                            	<div class="table-responsive">
                                    <form id="markingform'.$started->TM_STHW_Student_Id.'" method="post">
                                        <input type="hidden" name="student" id="student'.$started->TM_STHW_Student_Id.'" value="'.$started->TM_STHW_Student_Id.'"/>
                                        <input type="hidden" name="homework" id="homework'.$started->TM_STHW_Student_Id.'" value="'.$started->TM_STHW_HomeWork_Id.'"/>
                                		<table class="table tdnone">
                                			<tbody>
                                				<tr>
                                                    '.$this->GetQuestionColumns($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id).'
                                				</tr>
                                			</tbody>
                                		</table>
                                    </form>
                            	</div>
                            </div>
                        </div>
						<div class="row">
								<div class="my_wrapper drop_row solution'.$started->TM_STHW_Student_Id.'" style="height: 650px;">
								</div>
						</div>
						<div class="row">
								<div class="my_wrapper drop_row2 answer'.$started->TM_STHW_Student_Id.'">

								</div>
						</div>

                    </li>';
            elseif($started->TM_STHW_Status=='5'):
                    $studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$started->TM_STHW_Student_Id));
            $content.='<li class="list-group-item">
                                <div class="row">
        							<div class="col-md-2 col-xs-6">
        								<div class="doc">
        									<div class="active item">

        									  <div class="carousel-info">
        										<div class="pull-left">
        										  <span class="doc-name">'.$studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name.'</span>
        											<div class="namebottom">
        												<ul>
                            <li data-toggle="tooltip" data-placement="bottom" data-original-title="Reopen">
                                                            <a title="" class="reopen" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
                                                                <i style="font-size: 18px;" class="glyphicon glyphicon-remove"></i>
                                                            </a>
                                                        </li>
        													<li data-toggle="tooltip" data-placement="bottom" data-original-title="View Answer"><a title="" class="drop2 viewanswer" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'"><i class="fa fa-eye fa-2x"></i></a></li>';

                                                    if($this->HasWorksheeturl($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id)!='0'):
                                                        $homework_url=Studenthomeworkanswersurl::model()->find(array('condition'=>'TM_STHWARU_HW_Id='.$started->TM_STHW_HomeWork_Id.' AND TM_STHWARU_Student_Id='.$started->TM_STHW_Student_Id));
                                                        $homework_url->TM_STHWARU_Url;
                                                        $url='';
                                                        $search='https';
                                                        if(preg_match("/{$search}/i", $homework_url->TM_STHWARU_Url))
                                                        {
                                                           $url=$homework_url->TM_STHWARU_Url;

                                                        }
                                                        else
                                                        {
                                                             $url='https://'.$homework_url->TM_STHWARU_Url;
                                                        }

                                                $content.='<li><a href="'.$url.'" target="_blank" class="homeworkurl" title="Open Link"  data-value="'.$started->TM_STHW_HomeWork_Id.'"><span style="color:#ffa900;" class="glyphicon glyphicon-link"></span></a></li>';
                                                    endif;

                                                $content.='<li>
                                                            <a title="" class="unmarkcomplete" data-toggle="tooltip" data-placement="bottom" data-original-title="Edit Marking" data-item="'.$started->TM_STHW_HomeWork_Id.'" data-student="'.$started->TM_STHW_Student_Id.'">
                            									<i class="fa fa-check fa-2x"></i>
                            								</a>
                            							</li>
        												</ul>
        											</div>
        										</div>
        									  </div>
        									</div>
        								</div>
        							</div>



                               <div class="col-md-1 col-xs-6">
                                    <!--<div class="btmy">
                                    <div id="pbar" class="progress-pie-chart" data-percent="0">
                                    <div class="ppc-progress">
                                    <div class="ppc-progress-fill" ></div>
                                    </div>
                                    <div class="ppc-percents">
                                    <div class="pcc-percents-wrapper">
                                    <span>%</span>
                                    </div>
                                    </div>
                                    </div>
                                    <progress style="display: none" id="progress_bar" class="pieprogress"  value="0" max="'.$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).'"></progress>
                                    </div>-->
                                    <div id="'.'prog'.$count.'" class="containerdiv">
                                    	<div class="jCProgress" style="opacity: 1;">
                                    		<div class="percent" style="display: none;">'.$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).'</div>
                                    		<canvas width="55" height="55"></canvas>
                                    	</div>
                                    </div>';
                                    //$script.="var myplugin".$count.";myplugin".$count." = $('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).",loop : false,showPercent : true});";
                                    //$script.="$('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).",loop : false,showPercent : true});";
                                    if(Yii::app()->session['userlevel']==1):
                                        $script.="$('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50_new.png',speed: 10,PIStep : 0.05,limit: ".$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).",loop : false,showPercent : true});";
                                    else:
                                        $script.="$('#prog".$count."').cprogress({percent: -1,img1: '".Yii::app()->request->baseUrl."/images/c1_50.png',img2: '".Yii::app()->request->baseUrl."/images/c3_50.png',speed: 10,PIStep : 0.05,limit: ".$this->GetAverage($started->TM_STHW_Student_Id,$started->TM_STHW_HomeWork_Id).",loop : false,showPercent : true});";
                                    endif;

								$content.='</div>

        						<div class="col-xs-12 col-md-9 col-lg-9">
        							<div class="table-responsive">
        								<table class="table tdnone">
        									<tbody>
        										<tr>
                                                    '.$this->GetQuestionColumnsCompleted($started->TM_STHW_HomeWork_Id,$started->TM_STHW_Student_Id).'

        										</tr>
        									</tbody>
        								</table>
        							</div>
        						</div>




        						<!--<div class="pull-right btmy">
        							<button type="button" class="btn btn-warning btn-md">Save</button>
        						</div>-->

                                </div>

							<div class="row">
									<div class="my_wrapper drop_row">
										<ul class="lst_stl_my solution'.$started->TM_STHW_Student_Id.'" style="height: 650px;">

										</ul>
									</div>
							</div>
							<div class="row">
									<div class="my_wrapper drop_row2">
										<ul class="lst_stl_my answer'.$started->TM_STHW_Student_Id.'">

											</ul>
									</div>
							</div>
                            </li>';
            endif;
            $count++;
        endforeach;
        ///echo $content;
        $arr=array('html'=>$content,'percentage'=>$script);
        echo json_encode($arr);
    }
    public function Addhomeworkgroup($homeworkid,$groupid)
    {
        $homework =new Homeworkgroup();
        $homework->TM_HMG_Homework_Id=$homeworkid;
        $homework->TM_HMG_Groupe_Id=$groupid;
        $homework->save(false);
    }

    //worksheets
    public function actionListworksheets()
    {
        $this->layout = '//layouts/teacher';
        $model=new Mock('searchschool');
        $model->unsetAttributes();  // clear any default values
        $condition="";
        if(isset($_GET['Mock'])):
            $model->attributes=$_GET['Mock'];
            $model->schoolsearch=$_GET['Mock']['schoolsearch'];
            $pieces = explode(" ", $_GET['Mock']['TM_MK_Name']);
            //echo count($pieces);exit;
            if(count($pieces)>0):
                $condition .= "AND (";
                foreach($pieces as $key=>$piece)
                {
                    if($key !=0)
                        $condition .=" AND";
                    $condition .= " (TM_MK_Name LIKE '%".str_replace("'","''",$piece)."%' or TM_MK_Description LIKE '%".str_replace("'","''",$piece)."%' or TM_MK_Tags LIKE '%".str_replace("'","''",$piece)."%' or (n.firstname LIKE '".str_replace("'","''",$piece)."' OR n.lastname LIKE '".str_replace("'","''",$piece)."') ) ";
                }
                $condition .= ")";
            endif;

            /*$newstring = preg_replace('/\s+/', '|', $_GET['Mock']['TM_MK_Name']);
            if($newstring!='')
            {
                $condition=" AND (TM_MK_Name REGEXP '".$newstring."' OR TM_MK_Description REGEXP '".$newstring."' OR TM_MK_Tags REGEXP '".$newstring."' OR n.firstname REGEXP '".$newstring."' OR n.lastname REGEXP '".$newstring."' )";
            }*/
            /*if($pieces[0]!=''):
                $condition=" AND ( (TM_MK_Name LIKE '%".$pieces[0]."%' AND TM_MK_Name LIKE '%".$pieces[1]."%' AND TM_MK_Name LIKE '%".$pieces[2]."%') OR
                                    TM_MK_Description LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                                    TM_MK_Tags LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                                    (n.firstname LIKE '".$_GET['Mock']['TM_MK_Name']."' OR n.lastname LIKE '".$_GET['Mock']['TM_MK_Name']."') )";
            else:
                $condition=" AND (TM_MK_Name LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                            TM_MK_Description LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                            TM_MK_Tags LIKE '%".$_GET['Mock']['TM_MK_Name']."%' )";
            endif;*/
        endif;
        if(isset($_GET['tags'])):
            $condition=" AND (TM_MK_Tags REGEXP '".$_GET['tags']."' OR
                        TM_MK_Name LIKE '%".$_GET['tags']."%' OR
                        TM_MK_Description LIKE '%".$_GET['tags']."%')";
        endif;
        //echo $condition;
        $criteria = new CDbCriteria;
        //$criteria->join = 'INNER JOIN tm_mock_school AS m ON m.TM_MS_Mock_Id= TM_MK_Id INNER JOIN tm_profiles AS n ON n.user_id= TM_MK_CreatedBy';
        $criteria->join = 'INNER JOIN tm_mock_school AS m ON m.TM_MS_Mock_Id= TM_MK_Id LEFT JOIN tm_profiles AS n ON n.user_id= TM_MK_CreatedBy';
        $criteria->addCondition("TM_MK_Standard_Id='".Yii::app()->session['standard']."' AND TM_MK_Status=0 AND TM_MK_Availability != 0 AND TM_MK_Resource=0 AND m.TM_MS_School_Id='".Yii::app()->session['school']."'  $condition");
        $criteria->order = 'TM_MK_Sort_Order DESC';
        $mocks = Mock::model()->findAll($criteria);
        $mockheaders=MockTags::model()->findAll(array('condition'=>'TM_MT_Type=0 AND TM_MT_Standard_Id='.Yii::app()->session['standard'].' '));

        $this->render('listworksheets',array(
            'model'=>$model,
            'mocks'=>$mocks,
            'mockheaders'=>$mockheaders,
        ));
    }
    public function actionCreateworksheet()
    {
        $this->layout = '//layouts/teacher';
        $model=new Mock('teachercreation');
        $model->scenario = 'teachercreation';
        if(isset($_POST['Mock']))
        {
            $model->attributes=$_POST['Mock'];
            $model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
            $model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
            $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
            $model->TM_MK_Sort_Order = $this->GetSortorder($_POST['Mock']['TM_MK_Standard_Id']);
            $model->TM_MK_Link_Resource=$_POST['Mock']['TM_MK_Link_Resource'];
            if($model->save()):
                $insertarray=array();
                $insertarray[0]=array('TM_MS_Mock_Id'=>$model->TM_MK_Id,'TM_MS_School_Id'=>User::model()->findByPk(Yii::app()->user->id)->school_id);
                $builder=Yii::app()->db->schema->commandBuilder;
                $command=$builder->createMultipleInsertCommand('tm_mock_school',$insertarray );
                $command->execute();
                if($_FILES["Mock"]["name"]["TM_MK_Worksheet"]!=''):
                    $imageName = $_FILES["Mock"]["name"]["TM_MK_Worksheet"];
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $exten=explode('.', $imageName);
                    $uniqueName="Worksheet".$mockid.'.'.$exten[1];
                    $model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
                    $model->TM_MK_Worksheet->saveAs(Yii::app()->params['uploadPath'].$uniqueName);
                    $model->TM_MK_Worksheet = $uniqueName;
                endif;
                if($_FILES["Mock"]["name"]["TM_MK_Solution"]!=''):
                    $solutionimageName = $_FILES["Mock"]["name"]["TM_MK_Solution"];
                    $solexten=explode('.', $solutionimageName);
                    $uniqueName=$mockname.$mockid.'.'.$exten[1];
                    $uniquesolutionName="Worksheet".$mockid.'solution.'.$solexten[1];
                    $model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
                    $model->TM_MK_Solution->saveAs(Yii::app()->params['uploadPath'].$uniquesolutionName);
                    $model->TM_MK_Solution = $uniquesolutionName;
                endif;
                if($_FILES["Mock"]["name"]["TM_MK_Thumbnail"]!=''):
                    $thumbimageName = $_FILES["Mock"]["name"]["TM_MK_Thumbnail"];
                    $thumbexten=explode('.', $thumbimageName);
                    $uniquethumbName="Worksheet".$mockid.'thumbnail.'.$thumbexten[1];
                    $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
                    $model->TM_MK_Thumbnail->saveAs(Yii::app()->params['uploadPath'].$uniquethumbName);
                    $model->TM_MK_Thumbnail = $uniquethumbName;
                endif;
                $model->TM_MK_Availability='3';
                $model->save();
                $this->redirect(array('listworksheets'));
            endif;
        }
        $this->render('createworksheet',array('model'=>$model));
    }
    public function GetSortorder($standard)
	{
		$modelupper=Mock::model()->find(array(
				'condition' => 'TM_MK_Standard_Id=:standard',
				'limit' => 1,
				'order'=>'TM_MK_Sort_Order DESC',
				'params' => array(':standard'=>$standard)
			)
		);
		return $modelupper->TM_MK_Sort_Order+1;
	}

    public function actionUpdateworksheet($id)
    {
        $this->layout = '//layouts/teacher';
        $model=Mock::model()->findByPk($id);
        if(isset($_POST['Mock']))
        {
            $model->attributes=$_POST['Mock'];
            $model->TM_MK_Link_Resource=$_POST['Mock']['TM_MK_Link_Resource'];
            if($model->save()):
                $imageName = $_FILES["Mock"]["name"]["TM_MK_Worksheet"];
                $mockname=str_replace(' ', '', $model->TM_MK_Name);
                $mockid=str_replace(' ', '', $model->TM_MK_Id);
                $exten=explode('.', $imageName);
                if($_FILES["Mock"]["name"]["TM_MK_Worksheet"]!=''):
                    $imageName = $_FILES["Mock"]["name"]["TM_MK_Worksheet"];
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $exten=explode('.', $imageName);
                    $uniqueName="Worksheet".$mockid.'.'.$exten[1];
                    $model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
                    $model->TM_MK_Worksheet->saveAs(Yii::app()->params['uploadPath'].$uniqueName);
                    $model->TM_MK_Worksheet = $uniqueName;
                endif;
                if($_FILES["Mock"]["name"]["TM_MK_Solution"]!=''):
                    $solutionimageName = $_FILES["Mock"]["name"]["TM_MK_Solution"];
                    $solexten=explode('.', $solutionimageName);
                    $uniqueName=$mockname.$mockid.'.'.$exten[1];
                    $uniquesolutionName="Worksheet".$mockid.'solution.'.$solexten[1];
                    $model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
                    $model->TM_MK_Solution->saveAs(Yii::app()->params['uploadPath'].$uniquesolutionName);
                    $model->TM_MK_Solution = $uniquesolutionName;
                endif;
                $model->save(false);
                $this->redirect(array('listworksheets'));
            endif;
        }
        $this->render('worksheetupdate',array('model'=>$model));
    }
    public function actionDeleteworksheet($id)
    {
        $model=Mock::model()->findByPk($id);
        if($model->delete())
        {
            ///$questions=CustomtemplateQuestions::model()->deleteAll(array('condition'=>'TM_CTQ_Custom_Id='.$id));
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            if($model->TM_MK_Resource==1):
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listresources'));
            else:
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('listworksheets'));
            endif;

    }
    public function actionPublishworksheet()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
        $standard=$_POST['publishto'];
		$homeworkid=$this->InsertWorksheetQuestions($templateid,$standard);
        $homeworktotal=$this->Gethomeworktotal($homeworkid);
		if(count($groupid)> 0):

            for ($i = 0; $i < count($groupid); $i++) {

                $grpid=$groupid[$i];
                //$grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                $grpstudents=GroupStudents::model()->findAll(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'TM_GRP_Id='.$grpid.' AND b.status=1'));
                $this->Addhomeworkgroup($homeworkid,$grpid);
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];
                    $studhomework->TM_STHW_ShowMark=$_POST['showmark'];
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                        $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                        $studhomework->save(false);
                    }
                endforeach;
            }
        endif;
        $homework = Schoolhomework::model()->findByPk($homeworkid);
        $homework->TM_SCH_Status='1';
        $homework->TM_SCH_CreatedOn=date('Y-m-d');
        $homework->TM_SCH_DueDate=$_POST['dudate'];
        $homework->TM_SCH_Comments=$_POST['comments'];
        $homework->TM_SCH_PublishDate=$_POST['publishdate'];
        $homework->TM_SCH_PublishType=$_POST['type'];
        $homework->TM_SCH_ShowSolution=$_POST['solution'];
		$homework->TM_SCH_CreatedBy=Yii::app()->user->id;
		$homework->TM_SCH_CreatedOn=date('Y-m-d h:i:s');
        $homework->TM_SCH_ShowMark=$_POST['showmark'];
        if($homework->save(false)):
                echo "yes";
        endif;
    }
	public function InsertWorksheetQuestions($homeworkid,$standard)
	{
        $template = Mock::model()->findByPk($homeworkid);
        $schoolhomework=new Schoolhomework();
        $schoolhomework->TM_SCH_Name=$template->TM_MK_Name;
        $schoolhomework->TM_SCH_Description=$template->TM_MK_Description;
        $schoolhomework->TM_SCH_School_Id=User::model()->findByPk(Yii::app()->user->id)->school_id;
        //$schoolhomework->TM_SCH_Publisher_Id=$template->TM_SCT_Publisher_Id;
        //$schoolhomework->TM_SCH_Syllabus_Id=$template->TM_SCT_Syllabus_Id;
        $schoolhomework->TM_SCH_Standard_Id=$standard;
        $schoolhomework->TM_SCH_Master_Stand_Id=$template->TM_MK_Standard_Id;
        $schoolhomework->TM_SCH_Type='3';
        $schoolhomework->TM_SCH_Status='1';
        $schoolhomework->TM_SCH_CreatedOn=$template->TM_MK_CreatedOn;
        $schoolhomework->TM_SCH_CreatedBy=$template->TM_MK_CreatedBy;
        $schoolhomework->TM_SCH_Worksheet=$template->TM_MK_Worksheet;
        $schoolhomework->TM_SCH_Solution=$template->TM_MK_Worksheet;
        $schoolhomework->save(false);
        $questions= MockQuestions::model()->findAll(array('condition'=>'TM_MQ_Mock_Id='.$homeworkid));
        foreach($questions AS $question):
            $homeworkquestion=new HomeworkQuestions();
            $homeworkquestion->TM_HQ_Homework_Id=$schoolhomework->TM_SCH_Id;
            $homeworkquestion->TM_HQ_Question_Id=$question->TM_MQ_Question_Id;
            $homeworkquestion->TM_HQ_Type=$question->TM_MQ_Type;
            $homeworkquestion->TM_HQ_Order=$question->TM_MQ_Order;
            $homeworkquestion->TM_HQ_Header=$question->TM_MQ_Header;
            $homeworkquestion->TM_HQ_Section=$question->TM_MQ_Section;
	    $homeworkquestion->TM_HQ_Show_Options=$question->TM_MQ_Show_Options;
            $homeworkquestion->save(false);
        endforeach;
        return $schoolhomework->TM_SCH_Id;

	}
      public function actionPrintmock($id)
    {
        $schoolhomework=Mock::model()->findByPk($id);
        $user=User::model()->findByPk(Yii::app()->user->id);
        $report=New WorksheetprintReport();
        //$report->TM_WPR_Date=date("Y-m-d H:i:s");
        $report->TM_WPR_Username=$user->username;
        $profile=Profile::model()->findByPk(Yii::app()->user->id);
        $report->TM_WPR_FirstName=$profile['firstname'];
        $report->TM_WPR_LastName=$profile['lastname'];
        $report->TM_WPR_Standard=Yii::app()->session['standard'];
        $schoolid=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.Yii::app()->user->id.''))->TM_TH_SchoolId;
        $school=School::model()->findByPk($schoolid);
        $schoolname=$school['TM_SCL_Name'];
        $report->TM_WPR_School_id=$schoolid;
        $report->TM_WPR_School=$schoolname;
        $type=0;
        $location=$school['TM_SCL_City'];
        $report->TM_WPR_Type=$type;
        $report->TM_WPR_Location=$location;
        $hwemoorkdata=Mock::model()->findByPK($id);
        $homework=$hwemoorkdata->TM_MK_Name;
        $report->TM_WPR_Worksheetname=$homework;
        if($schoolhomework->TM_MK_Worksheet==''):
            $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $user=User::model()->findByPk(Yii::app()->user->id);
            $username=$user->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $school=$hwemoorkdata->TM_MK_Name;
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;
            $html = "<style>
            p{
            margin-top: 0;
            margin-bottom: 0;
            }
            </style>
            <table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";
            //$html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>";
            $html .= "<tr><td colspan=3 align='center'></td></tr>";
            $totalquestions=count($homeworkqstns);
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):

                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
                if($question->TM_QN_Show_Option==1):

                    $showoption=true;
                else:
                    $showoption=false;
                endif;
                ///$showoption=false;
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                if($homeworkqstn['TM_MQ_Header']!=''):
                    $html .= "<tr><td colspan=3 align='center'><u><b>".$homeworkqstn['TM_MQ_Header']."</b><u></td></tr>
                                <tr><td colspan=3 align='center'><i>".$homeworkqstn['TM_MQ_Section']."</i></td></tr><tr><td colspan=3 align='center' ></td></tr>";
                endif;
                //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td>";
                        $html .= "<td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>( " . $marks . " )</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        if($homeworkqstn->TM_MQ_Show_Options==1):
                            $showoption=true;
                        endif;

                        if($showoption):
                            foreach($question->answers AS $key=>$answer):
                                if($answer->TM_AR_Image!=''):
                                    $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=150&height=150'."' alt=''>";
                                else:
                                    $ansoptimg='';
                                endif;
                                $ans=$answer->TM_AR_Answer;
                                $find='<p>';
                                $pos = strpos($ans, $find);
                                if ($pos === false) {
                                    $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")".$answer->TM_AR_Answer."</p> $ansoptimg</td><td></td></tr>";
                                } else {
                                    // var_dump($answer->TM_AR_Answer);exit;
                                    $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p style="margin:0;">'.$alphabets[$key].')', 0,3)." $ansoptimg</td><td></td></tr>";
                                }
                            endforeach;
                        endif;
                        if($question->TM_QN_Type_Id=='5'):
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            foreach ($questions AS $key=>$childquestion):
                                $html .= "<tr><td width='6%'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td></td></tr>";
                                if ($childquestion->TM_QN_Image != ''):
                                    $html .= '<tr><td width="6%"></td><td align="left" style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td></td></tr>';
                                endif;


                                if($childquestion->TM_QN_Type_Id!=4){
                                    if($showoption):
                                        foreach($childquestion->answers AS $key=>$answer):
                                            if($answer->TM_AR_Image!=''):
                                                $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=150&height=150'."' alt=''>";
                                            else:
                                                $ansoptimg='';
                                            endif;
                                            $ans=$answer->TM_AR_Answer;
                                            $find='<p>';
                                            $pos = strpos($ans, $find);
                                            if ($pos === false)
                                            {
                                                $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")".$answer->TM_AR_Answer."</p> $ansoptimg</td><td></td></tr>";
                                            }
                                            else
                                            {
                                                $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)." $ansoptimg</td><td></td></tr>";
                                            }
                                        endforeach;
                                    endif;
                                }
                            endforeach;
                        endif;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3" style="padding:0;"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    if ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $countpart = 1;
                        $childhtml = "";
                        foreach ($questions AS $childquestion):
                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                            $childhtml .="<tr><td width='6%' style='padding:0;'></td><td align='left' style='padding:0;'>Part." . $countpart."</td><td align='right' width='6%' style='padding:0;'></td></tr>";
                            $childhtml .="<tr><td width='6%' style='padding:0;'></td><td  style='padding:0;'>".$childquestion->TM_QN_Question."</td><td align='right' width='6%' style='padding:0;'></td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $childhtml .= '<tr><td align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            $countpart++;
                        endforeach;
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'> (" . $displaymark . " )</td></tr>";
                        // $html .= "<tr><td colspan='3' style='padding:0;'>".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%"></td><td align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $html .= $childhtml;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3" style="padding:0;"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
            endforeach;
            $html .= "</table>";
            $schoolhw=Mock::model()->findByPK($id);
            $mockschl=MockSchool::model()->find(array('condition' => "TM_MS_Mock_Id='$id' "));
            $mockschool=School::model()->findByPk($user->school_id)->TM_SCL_Name;
            $schoolimage=School::model()->findByPk($user->school_id);
            $standard=Standard::model()->findByPk($schoolhw->TM_MK_Standard_Id)->TM_SD_Name;
            $total=$this->GetMocktotal($id);
            $header.= '
            <div class="first-div"><table cellpadding="5" border="0" style="margin-bottom:10px; margin-top: 15px; font-size:14px;font-family: STIXGeneral;" width="100%">';
               if($schoolimage->TM_SCL_Image!='')
                {

                  $header.= '<tr><td colspan=4 align="center" style="padding:0;"><img src="'.Yii::app()->request->baseUrl.'/images/headers/'.$schoolimage->TM_SCL_Image.'" style="width:1200px;" ><hr style="height: 1px; border: 0;  display: block; background: transparent; width: 100%; border-top: solid 1px #9a9696;"> </td></tr>';
                 }
                 else
                 {

                  $header.= '<tr><td colspan=4 align="center" style="padding:0;"><b>'.$mockschool.'</b></td></tr>';
                 }


                  $header.= '<tr><td colspan=4 align="center" style="padding:0; font-size:18px;"><b>'.$homework.'</b></td></tr>
                <tr>
                    <td width="20%" style="padding:0">Student Name :</td>
                    <td width="50%" style="padding:0"> </td>
                    <td width="20%" style="padding:0;">Date :</td>
                    <td width="10%" style="padding:0;"> '.date('d/m/Y').'</td>
                </tr>
                <tr>
                    <td width="20%" style="padding:0">Standard :</td>
                    <td width="50%" style="padding:0;">  '.$standard.'</td>
                    <td width="20%" style="padding:0">Division : </td>
                    <td width="10%" style="padding:0;"> </td>
                </tr>
                <tr>
                    <td width="20%" style="padding:0;">Total Marks :</td>
                    <td width="50%" style="padding:0;"> '.$total.' </td>
                    <td width="20%" style="padding:0;">Marks Scored :</td>
                    <td width="10%" style="padding:0;"> </td>
                </tr>

                <tr>
                    <td width="100%" colspan=4 >Instructions for Student : </td>
                </tr>
            </table></div>';
            $html=$header.$html;
            // echo $html;exit;

            $mpdf = new mPDF();
            $mpdf->SetDisplayMode('fullpage');
            //$mpdf->SetHTMLHeader($header);
            $mpdf->SetWatermarkText($username, .1);
            $mpdf->showWatermarkText = false;
            //$mpdf->showWatermarkImage = true;
            //$mpdf->SetWatermarkImage(Yii::app()->request->baseUrl . '/images/pdfbg.png', 0.15, 'F');
            $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter;margin-top:20px; ">
                <tr>
                    <td width="100%" colspan="2">
                        &copy; Copyright @ TabbieMe Ltd. All rights reserved .
                    </td>
                </tr>
                <tr>
                    <td width="80%" >
                        www.tabbiemath.com  - The one stop shop for Maths Revision.
                    </td>
                    <td align="right" >Page #{PAGENO}</td>
                </tr>
            </table>');

            $this->render('printpdf',array('html'=>$html));
            exit;
            // echo $html;exit;
            $report->save(false);
            $name='WORKSHEET'.time().'.pdf';
            $mpdf->WriteHTML($html);
            $mpdf->Output($name, 'I');
        else:
            $report->save(false);
            $this->redirect(Yii::app()->request->baseUrl."/worksheets/".$schoolhomework->TM_MK_Worksheet);
        endif;
    }
    public function actionPrintmocksolution($id)
    {
        $schoolhomework=Mock::model()->findByPk($id);
        if($schoolhomework->TM_MK_Solution==''):
        $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $user=User::model()->findByPk(Yii::app()->user->id);
            $username=$user->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;
            $html = "<style>
            p{
            margin-top: 0;
            margin-bottom: 0;
            }
            </style><table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";
            //$html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>";
            $html .= "<tr><td colspan=3 align='center'></td></tr>";
            $totalquestions=count($homeworkqstns);
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):

                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
                $showoption=false;
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>(" . $marks . ")</td></tr>";
                        $html .= "<tr></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%"" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        if($question->TM_QN_Type_Id=='5'):
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            foreach ($questions AS $key=>$childquestion):
                                $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                                if ($childquestion->TM_QN_Image != ''):
                                    $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                                endif;
                                if($showoption):
                                    foreach($childquestion->answers AS $key=>$answer):
                                        if($answer->TM_AR_Image!=''):
                                            $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                        else:
                                            $ansoptimg='';
                                        endif;
                                        $ans=$answer->TM_AR_Answer;
                                        $find='<p>';
                                        $pos = strpos($ans, $find);
                                        if ($pos === false)
                                        {
                                            $html.="<tr><td colspan='3'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
                                            <td align='right'>$ansoptimg</td></tr>";
                                        }
                                        else
                                        {
                                            $html.="<tr><td colspan='3'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td>
                                            <td align='right'>$ansoptimg</td></tr>";
                                        }
                                    endforeach;
                                endif;
                            endforeach;
                        endif;

                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>Sol. </b></td>";
                        $html .= "<td style='padding:0;'>".$question->TM_QN_Solutions."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    if ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $countpart = 1;
                        $childhtml = "";
                        foreach ($questions AS $childquestion):
                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                            $childhtml .="<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>Part." . $countpart."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                            $childhtml .="<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>".$childquestion->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $childhtml .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                            endif;
                            $countpart++;
                        endforeach;
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount ."</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>(" . $displaymark . ")</td></tr>";
                        // $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        $html .= $childhtml;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
            endforeach;
            $html .= "</table>";
            $schoolhw=Mock::model()->findByPK($id);
            $mockschl=MockSchool::model()->find(array('condition' => "TM_MS_Mock_Id='$id' "));
            $mockschool=School::model()->findByPk($user->school_id)->TM_SCL_Name;
            $standard=Standard::model()->findByPk($schoolhw->TM_MK_Standard_Id)->TM_SD_Name;
            $total=$this->GetMocktotal($id);
            $header = '<div ><table cellpadding="5" border="0" style="margin-bottom:10px;font-size:14px;font-family: STIXGeneral;" width="100%">
                <tr><td colspan=4 align="center" style="padding:0;"><b>'.$mockschool.'</b></td></tr>
                <tr><td colspan=4 align="center" style="padding:0;"><b>'.$homework.'</b></td></tr>
            </table></div>';
            $html=$header.$html;

            $this->render('printpdf',array('html'=>$html));
            exit;
            // $mpdf = new mPDF();
            // //$mpdf->SetHTMLHeader($header);
            // $mpdf->SetWatermarkText($username, .1);
            // $mpdf->showWatermarkText = false;
            // $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
            //  <tr>
            //      <td width="100%" colspan="2">
            //          &copy; Copyright @ TabbieMe Ltd. All rights reserved .
            //      </td>
            //  </tr>
            //  <tr>
            //      <td width="80%" >
            //          www.tabbiemath.com  - The one stop shop for Maths Revision.
            //      </td>
            //         <td align="right" >Page #{PAGENO}</td>
            //  </tr>
            // </table>');
            // //echo $html;exit;
            // $name='WORKSHEET'.time().'.pdf';
            // $mpdf->WriteHTML($html);
            // $mpdf->Output($name, 'I');
        else:
            $this->redirect(Yii::app()->request->baseUrl."/worksheets/".$schoolhomework->TM_MK_Solution);
        endif;
    }
    public function GetMocktotal($homeworkid)
    {
        $questions= MockQuestions::model()->findAll(array('condition'=>'TM_MQ_Mock_Id='.$homeworkid));
        $homeworktotal=0;
        foreach($questions AS $question):
            $question=Questions::model()->findByPk($question->TM_MQ_Question_Id);

            switch($question->TM_QN_Type_Id)
            {
                case 4:
                    $marks=Answers::model()->find(array('select'=>'TM_AR_Marks AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' '));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
                case 5:
                    $command = Yii::app()->db->createCommand();
                    $row = Yii::app()->db->createCommand(array(
                        'select' => array('SUM(TM_AR_Marks) AS totalmarks'),
                        'from' => 'tm_answer',
                        'where' => 'TM_AR_Question_Id IN (SELECT TM_QN_Id FROM 	tm_question WHERE TM_QN_Parent_Id='.$question->TM_QN_Id.')',
                    ))->queryRow();
                    $homeworktotal=$homeworktotal+$row['totalmarks'];
                    break;
                case 6:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                case 7:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                default:
                    $marks=Answers::model()->find(array('select'=>'SUM(TM_AR_Marks) AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' AND TM_AR_Correct=1'));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
            }

        endforeach;
        return $homeworktotal;
    }

    public function actionGetpaperqstns()
    {
        $paperonly=Questions::model()->findAll(array('condition'=>'TM_QN_Type_Id'));
        echo count($paperonly);exit;
    }

    public function actionWorksheetPrintReport()
    {
        $this->layout = '//layouts/admincolumn2';
        //$dataval=array();
        //$condition='TM_WPR_Standard='.Yii::app()->session['standard'].'';
        if(isset($_GET['search']))
        {
            $condition='TM_WPR_Type IN (0,1)';
            $formvals=array();
            if($_GET['date']=='before'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $condition.=" AND TM_WPR_Date < '".$_GET['fromdate']."' ";
                endif;
            elseif($_GET['date']=='after'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $condition.=" AND TM_WPR_Date > '".$_GET['fromdate']."' ";
                endif;
            else:
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!='' & $_GET['todate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $formvals['todate']=$_GET['todate'];
                    $condition.=" AND TM_WPR_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ";
                endif;
            endif;
            $formvals['schoolname']=$_GET['schoolname'];
            /*$formvals['fromdate']=$_GET['fromdate'];
            $formvals['todate']=$_GET['todate'];*/
            if($_GET['school']!=''):
                if($_GET['school']=='0'):
                    $formvals['school']=$_GET['school'];
                    $formvals['schooltext']=$_GET['schooltext'];
                    $condition.=" AND TM_WPR_School LIKE '%".$_GET['schooltext']."%' ";
                else:
                    $formvals['school']=$_GET['school'];
                    $condition.=" AND TM_WPR_School_id='".$_GET['school']."'";
                endif;
            endif;
            /*if($_GET['schoolname']!==''):
                $condition.=" AND TM_WPR_School LIKE '%".$_GET['schoolname']."%' ";
            endif;*/
            if($_GET['plan']!=''):
                $formvals['plan']=$_GET['plan'];
                $condition.=" AND TM_WPR_Standard =".$_GET['plan'];
            endif;
            $criteria='search=search&date='.$_GET['date'].'&school='.$_GET['school'].'&schooltext='.$_GET['schooltext'].'&plan='.$_GET['plan'].'&fromdate='.$_GET['fromdate'].'&todate='.$_GET['todate'];
            $datacount=WorksheetprintReport::model()->count(array('condition'=>$condition,'order'=>'TM_WPR_Id DESC'));
            $total_pages=$datacount;
            $limit = 10;
            if($_GET['page']==''):
                $count=1;
            else:
                $count=$_GET['page'];
            endif;
            //echo $condition;
            $page = $_GET['page'];
            if($page)
                $offset = ($page - 1) * $limit; 			//first item to display on this page
            else
                $offset = 0;

            $dataval=WorksheetprintReport::model()->findAll(array('condition'=>$condition,'order'=>'TM_WPR_Id DESC','limit' => $limit,'offset' => $offset));
            $nopages=$this->Getpaginationworksheet($criteria,$total_pages,$limit,$count,$_GET['page']);
        }

        $this->render('worksheetprintreport',array('formvals'=>$formvals,'dataval'=>$dataval,'nopages'=>$nopages));
    }

    public function actionViewworksheet()
    {
        $id=$_POST['worksheetid'];
        $schoolhomework=Mock::model()->findByPk($id);
        $user=User::model()->findByPk(Yii::app()->user->id);
        if($schoolhomework->TM_MK_Worksheet==''):
            $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $user=User::model()->findByPk(Yii::app()->user->id);
            $username=$user->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $school=$hwemoorkdata->TM_MK_Name;
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;
            $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
            //$html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>";
            //$html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";
            $totalquestions=count($homeworkqstns);
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):

                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
                if($question->TM_QN_Show_Option==1):

                    $showoption=true;
                else:
                    $showoption=false;
                endif;
                ///$showoption=false;
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                if ($question->TM_QN_Type_Id!='7'):
                    $html .= "<tr><td><div class='col-md-3'><span><b>Question " . $homeworkqstncount ."</b></span></div>
                    <div class='col-md-3 pull-right'><span class='pull-right' style='font-size: 14px;  padding-top: 1px;'>Marks:" . $marks . " ( &nbsp;&nbsp;&nbsp; )</span>
                    </div>
                    <div class='col-md-12 padnone'><div class='col-md-12'><p>".$question->TM_QN_Question."</p>
                    </div></div>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300">
                        </div>';
                    endif;
                    $html.="</td></tr>";
                    if($showoption):
                        foreach($question->answers AS $key=>$answer):
                            if($answer->TM_AR_Image!=''):
                                $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                            else:
                                $ansoptimg='';
                            endif;
                            $ans=$answer->TM_AR_Answer;
                            $find='<p>';
                            $pos = strpos($ans, $find);
                            if ($pos === false) {
                                $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</p></div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";
                                //$html.="<tr><td colspan='2'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td><td align='right'>$ansoptimg</td></tr>";
                            } else {
                                $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";

                            }
                        endforeach;
                    endif;
                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html.="<tr><td><div class='col-md-12 padnone'>
                                    <div class='col-md-12'><p>".$childquestion->TM_QN_Question."</p></div>
	                                </div>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html.='<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></div>';
                            endif;
                            $html.="</td></tr>";
                            if($showoption):
                                foreach($childquestion->answers AS $key=>$answer):
                                    if($answer->TM_AR_Image!=''):
                                        $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                    else:
                                        $ansoptimg='';
                                    endif;
                                    $ans=$answer->TM_AR_Answer;
                                    $find='<p>';
                                    $pos = strpos($ans, $find);
                                    if ($pos === false)
                                    {
                                        $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</p></div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";
                                    }
                                    else
                                    {
                                        $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";
                                    }
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        //$html .= '<tr ><td colspan="3"><br></td></tr>';
                        //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml.="<tr><td><div class='col-md-12 pull-left'>Part." . $countpart."</div>
                                        <div class='col-md-12'>".$childquestion->TM_QN_Question."</div>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml.='<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></div>';
                        endif;
                        $childhtml.="</td></tr>";
                        $countpart++;
                    endforeach;
                    $html.="<tr><td><div class='col-md-3'><span><b>Question " . $homeworkqstncount ."</b></span>
	                        </div>
	                        <div class='col-md-3 pull-right'>
		                    <span class='pull-right' style='font-size: 14px;  padding-top: 1px;'>Marks:" . $displaymark . " ( )</span>
	                        </div>
	                        <div class='col-md-12 padnone'>
                            <div class='col-md-12'><p>".$question->TM_QN_Question."</p>
		                    </div>
	                        </div>";
                    if ($question->TM_QN_Image != ''):
                        $html.='<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></div>';
                    endif;
                    $html.="</td></tr>";
                    $html .= $childhtml;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        //$html .= '<tr ><td colspan="3"><br></td></tr>';
                        //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                    endif;
                endif;
            endforeach;
            $html .= "</table></body></html>";
        endif;
        $print="<a href='".Yii::app()->createUrl('teachers/printmock',array('id'=>$id))."' class='btn btn-warning btn-xs' target='_blank'>
                        <span style='padding-right: 10px;'>Print</span>
                        <i class='fa fa-print fa-lg'></i></a>";
        $arr=array('result' => $html,'print'=>$print);
        echo json_encode($arr);
    }

    public function actionListresources()
    {
        $this->layout = '//layouts/teacher';
        $model=new Mock('searchschool');
        $model->unsetAttributes();  // clear any default values
        $condition="";
        if(isset($_GET['Mock'])):
            $model->attributes=$_GET['Mock'];
            $model->schoolsearch=$_GET['Mock']['schoolsearch'];
            $pieces = explode(" ", $_GET['Mock']['TM_MK_Name']);
            //echo count($pieces);
            if($pieces[0]!=''):
                $condition=" AND ( (TM_MK_Name LIKE '%".$pieces[0]."%' AND TM_MK_Name LIKE '%".$pieces[1]."%' AND TM_MK_Name LIKE '%".$pieces[2]."%') OR
                                    TM_MK_Description LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                                    TM_MK_Tags LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                                    (n.firstname LIKE '".$_GET['Mock']['TM_MK_Name']."' OR n.lastname LIKE '".$_GET['Mock']['TM_MK_Name']."') )";
            else:
                $condition=" AND (TM_MK_Name LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                            TM_MK_Description LIKE '%".$_GET['Mock']['TM_MK_Name']."%' OR
                            TM_MK_Tags LIKE '%".$_GET['Mock']['TM_MK_Name']."%' )";
            endif;
        endif;
        if(isset($_GET['tags'])):
            $condition=" AND (TM_MK_Tags REGEXP '".$_GET['tags']."' OR
                        TM_MK_Name LIKE '%".$_GET['tags']."%' OR
                        TM_MK_Description LIKE '%".$_GET['tags']."%')";
        endif;
        //echo $condition;
        $criteria = new CDbCriteria;
        $criteria->join = 'INNER JOIN tm_mock_school AS m ON m.TM_MS_Mock_Id= TM_MK_Id INNER JOIN tm_profiles AS n ON n.user_id= TM_MK_CreatedBy';
        $criteria->addCondition("TM_MK_Standard_Id='".Yii::app()->session['standard']."' AND TM_MK_Status=0 AND TM_MK_Availability != 0 AND TM_MK_Resource=1 AND m.TM_MS_School_Id='".Yii::app()->session['school']."'  $condition");
        $criteria->order = 'TM_MK_Sort_Order DESC';
        $mocks = Mock::model()->findAll($criteria);
        $mockheaders=MockTags::model()->findAll(array('condition'=>'TM_MT_Type=1 AND TM_MT_Standard_Id='.Yii::app()->session['standard'].' '));

        $this->render('listresources',array(
            'model'=>$model,
            'mocks'=>$mocks,
            'mockheaders'=>$mockheaders,
        ));
    }

    public function actionCreateresource()
    {
        $this->layout = '//layouts/teacher';
        $model=new Mock('teachercreation');
        $model->scenario = 'resourcecreation';
        if(isset($_POST['Mock']))
        {

            $model->attributes=$_POST['Mock'];
            $model->TM_MK_Resource=1;
            //$model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
            //$model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
            $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
            $model->TM_MK_Link_Resource=$_POST['Mock']['TM_MK_Link_Resource'];
            if($model->save()):
                $insertarray=array();
                $insertarray[0]=array('TM_MS_Mock_Id'=>$model->TM_MK_Id,'TM_MS_School_Id'=>User::model()->findByPk(Yii::app()->user->id)->school_id);
                $builder=Yii::app()->db->schema->commandBuilder;
                $command=$builder->createMultipleInsertCommand('tm_mock_school',$insertarray );
                $command->execute();
                /*if($_FILES["Mock"]["name"]["TM_MK_Worksheet"]!=''):
                    $imageName = $_FILES["Mock"]["name"]["TM_MK_Worksheet"];
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $exten=explode('.', $imageName);
                    $uniqueName="Worksheet".$mockid.'.'.$exten[1];
                    $model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
                    $model->TM_MK_Worksheet->saveAs(Yii::app()->params['uploadPath'].$uniqueName);
                    $model->TM_MK_Worksheet = $uniqueName;
                endif;
                if($_FILES["Mock"]["name"]["TM_MK_Solution"]!=''):
                    $solutionimageName = $_FILES["Mock"]["name"]["TM_MK_Solution"];
                    $solexten=explode('.', $solutionimageName);
                    $uniqueName=$mockname.$mockid.'.'.$exten[1];
                    $uniquesolutionName="Worksheet".$mockid.'solution.'.$solexten[1];
                    $model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
                    $model->TM_MK_Solution->saveAs(Yii::app()->params['uploadPath'].$uniquesolutionName);
                    $model->TM_MK_Solution = $uniquesolutionName;
                endif;*/#
        if(isset($_POST['Mock']['TM_MK_Video_Url']))
        {

                $rx =
                    '~
                    ^(?:https?://)?
                    (?:www[.])?
                    (?:youtube[.]com/watch[?]v=|youtu[.]be/)
                    ([^&]{11})~x';
                    $youtube_url = $_POST['Mock']['TM_MK_Video_Url'];
                    $has_match = preg_match($rx, $youtube_url, $matches);

                    if($matches[0]!='')
                            {
                                $video_id = explode("=", $youtube_url);
                                $video_id = $video_id[1];
                                $video_char = explode("&", $video_id);
                                if(count($video_char)>1)
                                {
                                   $video_id=$video_char[0];
                                }
                                $thumbnail_url='https://img.youtube.com/vi/'.$video_id.'/hqdefault.jpg';
                                $model->TM_MK_Thumbnail = $thumbnail_url;
                            }

        }
                if($_FILES["Mock"]["name"]["TM_MK_Thumbnail"]!=''):
                    $thumbimageName = $_FILES["Mock"]["name"]["TM_MK_Thumbnail"];
                    $thumbexten=explode('.', $thumbimageName);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $uniquethumbName="Resource".$mockid.'thumbnail.'.$thumbexten[1];
                    $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
                    $model->TM_MK_Thumbnail->saveAs(Yii::app()->params['uploadPath'].$uniquethumbName);
                    $model->TM_MK_Thumbnail = $uniquethumbName;
                endif;
                $model->TM_MK_Availability='3';

                $model->save();
                $this->redirect(array('listresources'));
            endif;
        }
        $this->render('createresource',array('model'=>$model));
    }

    public function actionUpdateresource($id)
    {
        $this->layout = '//layouts/teacher';
        $model=Mock::model()->findByPk($id);
        if(isset($_POST['Mock']))
        {
            $model->attributes=$_POST['Mock'];
            $model->TM_MK_Link_Resource=$_POST['Mock']['TM_MK_Link_Resource'];
            if($model->save()):
        if(isset($_POST['Mock']['TM_MK_Video_Url']))
        {

                $rx =
                    '~
                    ^(?:https?://)?
                    (?:www[.])?
                    (?:youtube[.]com/watch[?]v=|youtu[.]be/)
                    ([^&]{11})~x';
                    $youtube_url = $_POST['Mock']['TM_MK_Video_Url'];
                    $has_match = preg_match($rx, $youtube_url, $matches);

                    if($matches[0]!='')
                            {
                                $video_id = explode("=", $youtube_url);
                                $video_id = $video_id[1];
                                $video_char = explode("&", $video_id);
                                if(count($video_char)>1)
                                {
                                   $video_id=$video_char[0];
                                }
                                $thumbnail_url='https://img.youtube.com/vi/'.$video_id.'/hqdefault.jpg';
                                $model->TM_MK_Thumbnail = $thumbnail_url;
                            }

        }
                if($_FILES["Mock"]["name"]["TM_MK_Thumbnail"]!=''):
                    $imageName = $_FILES["Mock"]["name"]["TM_MK_Thumbnail"];
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $exten=explode('.', $imageName);
                    $uniquethumbName="Resource".$mockid.'thumbnail.'.$exten[1];
                    $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
                    $model->TM_MK_Thumbnail->saveAs(Yii::app()->params['uploadPath'].$uniquethumbName);
                    $model->TM_MK_Thumbnail = $uniquethumbName;
                endif;
                $model->save(false);
                $this->redirect(array('listresources'));
            endif;
        }
        $this->render('resourceupdate',array('model'=>$model));
    }

    public function actionPublishresource()
    {
        $school=Yii::app()->session['school'];
        $templateid=$_POST['homeworkid'];
        $groupid=$_POST['nameids'];
        $standard=$_POST['publishto'];
        $homeworkid=$this->InsertResourceQuestions($templateid,$standard);
        $homeworktotal=$this->Gethomeworktotal($homeworkid);
        if(count($groupid)> 0):

            for ($i = 0; $i < count($groupid); $i++) {

                $grpid=$groupid[$i];
                //$grpstudents=GroupStudents::model()->findAll(array('condition' => "TM_GRP_Id='$grpid'"));
                $grpstudents=GroupStudents::model()->findAll(array('join'=>'LEFT JOIN tm_users AS b ON TM_GRP_STUId=b.id','condition'=>'TM_GRP_Id='.$grpid.' AND b.status=1'));
                $this->Addhomeworkgroup($homeworkid,$grpid);
                foreach($grpstudents AS $grpstudent):
                    $studhomework=New StudentHomeworks();
                    $studhomework->TM_STHW_Student_Id=$grpstudent->TM_GRP_STUId;
                    $studhomework->TM_STHW_HomeWork_Id=$homeworkid;
                    $studhomework->TM_STHW_Plan_Id=$grpstudent->TM_GRP_PN_Id;
                    //code by amal
                    if($_POST['publishdate']==date('Y-m-d')):
                        $studhomework->TM_STHW_Status=0;
                    else:
                        $studhomework->TM_STHW_Status=7;
                    endif;
                    //ends
                    $studhomework->TM_STHW_Assigned_By=Yii::app()->user->id;
                    $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                    $studhomework->TM_STHW_DueDate=$_POST['dudate'];
                    $studhomework->TM_STHW_Comments=$_POST['comments'];
                    $studhomework->TM_STHW_PublishDate=$_POST['publishdate'];
                    $studhomework->TM_STHW_Type=$_POST['type'];
                    $studhomework->TM_STHW_ShowSolution=$_POST['solution'];
                    $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$homeworkid' AND TM_STHW_Student_Id='$grpstudent->TM_GRP_STUId' "));
                    $count=count($studhw);
                    if($count==0)
                    {
                        if($_POST['publishdate']==date('Y-m-d')):
                            $notification=new Notifications();
                            $notification->TM_NT_User=$grpstudent->TM_GRP_STUId;
                            $notification->TM_NT_Type='HWAssign';
                            $notification->TM_NT_Item_Id=$homeworkid;
                            $notification->TM_NT_Target_Id=Yii::app()->user->id;
                            $notification->TM_NT_Status='0';
                            $notification->save(false);
                        endif;
                        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$homeworkid'"));
                        foreach($hwquestions AS $hwquestion):
                            $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                            $studhwqstns=New Studenthomeworkquestions();
                            $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                            $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                            $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                            $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                            $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                            $studhwqstns->save(false);
                            $questions=Questions::model()->findAllByAttributes(array(
                                'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                            ));
                            //print_r($questions);
                            if(count($questions)!=0):
                                foreach($questions AS $childqstn):
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$homeworkid;
                                    $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$grpstudent->TM_GRP_STUId;
                                    $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                    $studhwqstns->save(false);
                                endforeach;
                            endif;
                        endforeach;
                        $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                        $studhomework->save(false);
                    }
                endforeach;
            }
        endif;
        $homework = Schoolhomework::model()->findByPk($homeworkid);
        $homework->TM_SCH_Status='1';
        $homework->TM_SCH_CreatedOn=date('Y-m-d');
        $homework->TM_SCH_DueDate=$_POST['dudate'];
        $homework->TM_SCH_Comments=$_POST['comments'];
        $homework->TM_SCH_PublishDate=$_POST['publishdate'];
        $homework->TM_SCH_PublishType=$_POST['type'];
        $homework->TM_SCH_ShowSolution=$_POST['solution'];
        $homework->TM_SCH_CreatedBy=Yii::app()->user->id;
        if($homework->save(false)):
            echo "yes";
        endif;
    }

    public function InsertResourceQuestions($homeworkid,$standard)
    {
        $template = Mock::model()->findByPk($homeworkid);
        //print_r($template);exit;
        $schoolhomework=new Schoolhomework();
        $schoolhomework->TM_SCH_Name=$template->TM_MK_Name;
        $schoolhomework->TM_SCH_Description=$template->TM_MK_Description;
        $schoolhomework->TM_SCH_School_Id=User::model()->findByPk(Yii::app()->user->id)->school_id;
        //$schoolhomework->TM_SCH_Publisher_Id=$template->TM_SCT_Publisher_Id;
        //$schoolhomework->TM_SCH_Syllabus_Id=$template->TM_SCT_Syllabus_Id;
        $schoolhomework->TM_SCH_Standard_Id=$standard;
        $schoolhomework->TM_SCH_Master_Stand_Id=$template->TM_MK_Standard_Id;
        $schoolhomework->TM_SCH_Type='33';
        $schoolhomework->TM_SCH_Status='1';
        $schoolhomework->TM_SCH_CreatedOn=$template->TM_MK_CreatedOn;
        $schoolhomework->TM_SCH_CreatedBy=$template->TM_MK_CreatedBy;
        $schoolhomework->TM_SCH_Worksheet=$template->TM_MK_Worksheet;
        $schoolhomework->TM_SCH_Solution=$template->TM_MK_Worksheet;
        $schoolhomework->TM_SCH_Video_Url=$template->TM_MK_Video_Url;
        $schoolhomework->save(false);
        $questions= MockQuestions::model()->findAll(array('condition'=>'TM_MQ_Mock_Id='.$homeworkid));
        foreach($questions AS $question):
            $homeworkquestion=new HomeworkQuestions();
            $homeworkquestion->TM_HQ_Homework_Id=$schoolhomework->TM_SCH_Id;
            $homeworkquestion->TM_HQ_Question_Id=$question->TM_MQ_Question_Id;
            $homeworkquestion->TM_HQ_Type=$question->TM_MQ_Type;
            $homeworkquestion->TM_HQ_Order=$question->TM_MQ_Order;
            $homeworkquestion->save(false);
        endforeach;
        return $schoolhomework->TM_SCH_Id;

    }

    public function actionPrintresource()
    {
        $id=$_POST['worksheetid'];
        $report=New WorksheetprintReport();
        //$report->TM_WPR_Date=date("Y-m-d H:i:s");
        $user=User::model()->findByPk(Yii::app()->user->id);
        $report->TM_WPR_Username=$user->username;
        $profile=Profile::model()->findByPk(Yii::app()->user->id);
        $report->TM_WPR_FirstName=$profile['firstname'];
        $report->TM_WPR_LastName=$profile['lastname'];
        $report->TM_WPR_Standard=Yii::app()->session['standard'];
        $schoolid=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.Yii::app()->user->id.''))->TM_TH_SchoolId;
        $school=School::model()->findByPk($schoolid);
        $schoolname=$school['TM_SCL_Name'];
        $report->TM_WPR_School_id=$schoolid;
        $report->TM_WPR_School=$schoolname;
        $type=0;
        $location=$school['TM_SCL_City'];
        $report->TM_WPR_Type=$type;
        $report->TM_WPR_Location=$location;
        $hwemoorkdata=Mock::model()->findByPK($id);
        $homework=$hwemoorkdata->TM_MK_Name;
        $report->TM_WPR_Worksheetname=$homework;
        $report->save(false);
    }
    public function actionDownloadworksheet()
    {
        $formvals=array();
        $formvals['date']=$_GET['date'];
        $formvals['school']=$_GET['school'];
        $formvals['schooltext']=$_GET['schooltext'];
        $formvals['plan']=$_GET['plan'];
        $formvals['fromdate']=$_GET['fromdate'];
        $formvals['todate']=$_GET['todate'];
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT * FROM tm_worksheetprint_report WHERE TM_WPR_Type IN (0,1)";

        if($_GET['date']=='before'):
            if($_GET['fromdate']!=''):
                $sql.=" AND TM_WPR_Date < '".$_GET['fromdate']."' ";
            endif;
        elseif($_GET['date']=='after'):
            if($_GET['fromdate']!=''):
                $sql.=" AND TM_WPR_Date > '".$_GET['fromdate']."' ";
            endif;
        else:
            if($_GET['fromdate']!='' & $_GET['todate']!=''):
                $sql.=" AND TM_WPR_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ";
            endif;
        endif;

        if($_GET['school']!=''):
            $sql.=" AND TM_WPR_School_id='".$_GET['school']."'";
        endif;
        if($_GET['plan']!=''):
            $sql.=" AND TM_WPR_Standard =".$_GET['plan'];
        endif;
        $sql.=" ORDER BY TM_WPR_Date DESC";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataval=$dataReader->readAll();
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=worksheetreport.csv');
        $output = fopen('php://output', 'w');
        $headers=array(
            "Date & Time",
            "Username",
            "First Name",
            "Last Name",
            "Standard",
            "School",
            "Teacher/Student",
            "Location",
            "Worksheet name"
        );
        function cleanData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
        }
        fputcsv($output,$headers );
        if(count($dataval)>0):
            $flag = false;
            foreach($dataval AS $data):
                if($data['TM_WPR_Type']==0):
                    $type="Teacher";
                else:
                    $type="Student";
                endif;
                $standard=Standard::model()->findByPk($data['TM_WPR_Standard'])->TM_SD_Name;


                $row=array(date("d-m-Y h:i A", strtotime($data['TM_WPR_Date'])),$data['TM_WPR_Username'],$data['TM_WPR_FirstName'],
                    $data['TM_WPR_LastName'],$standard,$data['TM_WPR_School'],$type,$data['TM_WPR_Location'],$data['TM_WPR_Worksheetname']);

                if(!$flag) {
                    // display field/column names as first row
                    fputcsv($output, $row);
                    $flag = true;
                }
                array_walk($row, 'cleanData');
                fputcsv($output, $row);
            endforeach;
        endif;

    }
     //code to generate blueprint
    public function actionGenerateblueprint($id)
    {
        $school=Yii::app()->session['school'];
        $blueprint=Blueprints::model()->findByPk($id);
        $schoolhw=new Customtemplate();
        $schoolhw->TM_SCT_Name=$blueprint['TM_BP_Name'];
        $schoolhw->TM_SCT_School_Id=$school;
        $schoolhw->TM_SCT_Standard_Id=$blueprint['TM_BP_Standard_Id'];
        $schoolhw->TM_SCT_Syllabus_Id=$blueprint['TM_BP_Syllabus_Id'];
        $schoolhw->TM_SCT_CreatedBy=Yii::app()->user->id;
        $schoolhw->TM_SCT_Privacy=1;
        $schoolhw->TM_SCT_Type=1;
        $schoolhw->TM_SCT_Blueprint_Id=$id;
        $schoolhw->save(false);
        $bpchapters=blueprintTemplate::model()->findAll(array('condition'=>'TM_BPT_Blueprint_Id='.$id.' ','order'=>'TM_BPT_Chapter_Id ASC'));
        $count = 1;
        foreach($bpchapters AS $bpchapter):
            $marktype=TmAvailableMarks::model()->findByPk($bpchapter['TM_BPT_Mark_type'])->TM_AV_MK_Mark;
            $bptopics=BlueprintTopics::model()->findAll(array('condition'=>'TM_BP_TP_Blueprint_Id='.$id.' AND TM_BP_TP_Chapter_Id='.$bpchapter['TM_BPT_Chapter_Id'].' AND TM_BP_TP_Template_Id='.$bpchapter['TM_BPT_Id'].''));
            $seltopics='';
            foreach($bptopics AS $key=>$bptopic):
                if($key==0):
                    $seltopics.=$bptopic['TM_BP_TP_Topic_Id'];
                else:
                    $seltopics.=",".$bptopic['TM_BP_TP_Topic_Id'];
                endif;
            endforeach;
            //echo "chapterid: ".$bpchapter['TM_BPT_Chapter_Id']." topics: ".$bptopic['TM_BP_TP_Topic_Id']." type: ".$marktype." count: ".$bpchapter['TM_BPT_No_questions']."<br>";
            //echo $seltopics."<br>";
            $Syllabus = $blueprint['TM_BP_Syllabus_Id'];
            $Standard = $blueprint['TM_BP_Standard_Id'];
            $Chapter = $bpchapter['TM_BPT_Chapter_Id'];
            $limit = $bpchapter['TM_BPT_No_questions'];
            $homework = $schoolhw->TM_SCT_Id;
            //echo "sy: ".$Syllabus."st: ".$Standard."ch: ".$Chapter."top: ".$seltopics."mark: ".$marktype;exit;
            $command = Yii::app()->db->createCommand("CALL NewSetBPQuestions(:Syllabus,:Standard,:Chapter,'" . $seltopics . "','" . $marktype . "',:limit,:Homework,:questioncount,:school,'0',@out)");
            $command->bindParam(":Syllabus", $Syllabus);
            $command->bindParam(":Standard", $Standard);
            $command->bindParam(":Chapter", $Chapter);
            $command->bindParam(":limit", $limit);
            $command->bindParam(":Homework", $homework);
            $command->bindParam(":questioncount", $count);
            $command->bindParam(":school", $school);
            $command->query();
            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
        endforeach;
        if ($count != '0'):
            $command = Yii::app()->db->createCommand("CALL SetBPQuestionsNum(:Homework)");
            $command->bindParam(":Homework", $schoolhw->TM_SCT_Id);
            $command->query();
        endif;
        $this->redirect(array('managecustomquestions', 'id' => $schoolhw->TM_SCT_Id));
    }

    public function actionDiff()
    {
        /*$connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_STHW_Student_Id FROM tm_student_homeworks AS a INNER JOIN tm_student AS b ON a.TM_STHW_Student_Id=b.TM_STU_User_Id INNER JOIN tm_users AS c ON a.TM_STHW_Student_Id=c.id WHERE a.TM_STHW_HomeWork_Id='2194' AND a.TM_STHW_Status!='4' AND (b.TM_STU_School=20 OR b.TM_STU_School_Id=20) AND c.status=1";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $pending=$dataReader->readAll();
        $pendingarr=array();
        foreach($pending AS $item):
            array_push($pendingarr,$item['TM_STHW_Student_Id']);
        endforeach;
        $sql="SELECT a.TM_STHW_Student_Id FROM tm_student_homeworks AS a INNER JOIN tm_student AS b ON a.TM_STHW_Student_Id=b.TM_STU_User_Id INNER JOIN tm_users AS c ON a.TM_STHW_Student_Id=c.id WHERE a.TM_STHW_HomeWork_Id='2178' AND a.TM_STHW_Status!='4' AND (b.TM_STU_School=20 OR b.TM_STU_School_Id=20) AND c.status=1";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $pending1=$dataReader->readAll();
        $pendingarr1=array();
        foreach($pending1 AS $item1):
            array_push($pendingarr1,$item1['TM_STHW_Student_Id']);
        endforeach;
        $result = array_diff($pendingarr,$pendingarr1);
        foreach($result AS $value):
            echo "studID: ".$value."<br>";
        endforeach;*/
    }

    /* ASSIGNMENT Report by Akhil */

    public function actionAssessmentreportpdf($id)
    {
        $hwemoorkdata = Schoolhomework::model()->findByPK($id);
        $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => "TM_HQ_Homework_Id='$id'",'order'=>'TM_HQ_Order ASC'));
        $studhw=StudentHomeworks::model()->find(array('condition' => "TM_STHW_HomeWork_Id='$id' "));
        $staffschool=Teachers::model()->find(array('condition' => "TM_TH_User_Id='$studhw->TM_STHW_Assigned_By' "));
        $homework = $hwemoorkdata->TM_SCH_Name;
        $homeworkgroups = Homeworkgroup::model()->findAll(array('condition' => "TM_HMG_Homework_Id='$id' "));
        //$studhws=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_HomeWork_Id='$id' AND TM_STHW_Student_Id!='0'", "order" => "TM_STHW_Status DESC"));

       /* $studhws=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));*/
        //$studhws_attempt=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_HomeWork_Id='$id' AND TM_STHW_Student_Id!='0' AND TM_STHW_Status!='0'", "order" => "TM_STHW_Status ASC"));
        //$studhws_notattempt=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_HomeWork_Id='$id' AND TM_STHW_Student_Id!='0' AND TM_STHW_Status='0'", "order" => "TM_STHW_Status DESC"));

      /*  $studhws_attempt=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));*/
        $studhws_attempt=SingleHwstudStatistics::model()->findAll(array('condition'=>'TM_SAR_STU_HW_Id='.$id.' AND TM_SAR_STU_Status=1'));
        //echo count($studhws_attempt);exit;
        $attstds.='';
        foreach($studhws_attempt AS $key=>$studhws_atte):
            if($key==0):
                $attstds.=$studhws_atte['TM_SAR_STU_Student_Id'];
            else:
                $attstds.=','.$studhws_atte['TM_SAR_STU_Student_Id'];
            endif;
        endforeach;
       /* $studhws_notattempt=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS user ON user.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5) AND user.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));  */
         $studhws_notattempt=SingleHwstudStatistics::model()->findAll(array('condition'=>'TM_SAR_STU_HW_Id='.$id.' AND TM_SAR_STU_Status=0'));
        $wrkgrpids = array();
        foreach($homeworkgroups as $homeworkgroup):
         $group = SchoolGroups::model()->findByPK($homeworkgroup->TM_HMG_Groupe_Id);
         $wrkgrpids[] = $group->TM_SCL_GRP_Name;
        endforeach;
        $string = rtrim(implode(',', $wrkgrpids), ',');
        $publishdate = Yii::app()->dateFormatter->format("dd-MM-y",strtotime($hwemoorkdata->TM_SCH_PublishDate));
        $finishdate = Yii::app()->dateFormatter->format("dd-MM-y",strtotime($hwemoorkdata->TM_SCH_CompletedOn));
        $schoolname = School::model()->findByPK($hwemoorkdata->TM_SCH_School_Id)->TM_SCL_Name;

        $html="<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b>".$schoolname."</b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'><b>".$homework."</b></td></tr><br><br></table>";

        /* First Table starts */
      /*  $totalanswered = count($studhws);
       // $notanswered = count($studhws) - $totalanswered;
        $notanswered=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id INNER JOIN tm_users AS user ON user.id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.'  AND TM_STHW_Status NOT IN (4,5) AND user.status=1','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC')); */
        $singleHwstatics= SingleHwStatistics::model()->find(array('condition' => "TM_SAR_HW_Id ='".$id."' "));


        $html .= "<table cellspacing='1' cellpadding='10' border='2' width='100%' style='background:black;font-family:lucidasansregular;border:1px solid #dadada;'>
                  <tr>
                  <td style='background:#fff;'>Assignment Set Date: ".$publishdate."</td>
                  <td style='background:#fff;'>Assignment completion date: ".$finishdate."</td>
                  </tr>
                  <tr>
                  <td style='background:#fff;'>Teacher: ".$staffschool->TM_TH_Name."</td>
                  <td style='background:#fff;'>Published to : ".$string."</td>
                  </tr>
                  <tr>
                  <td style='background:#fff;'>Total No of Questions: ".$singleHwstatics->TM_SAR_Total_Questions."</td>
                  <td style='background:#fff;'>Total Marks: ".$singleHwstatics->TM_SAR_Total_Marks."</td>
                  </tr>
                  <tr>
                  <td style='background:#fff;'>No of students attempted: ".$singleHwstatics->TM_SAR_Atempted."</td>
                  <td style='background:#fff;'>No of students not attempted: ".$singleHwstatics->TM_SAR_Not_Attempted."</td>
                  </tr>
                  </table><br><br>";

        /* First Table ends */

        /* Second Table starts */

        $html .= "<tr><td colspan=3 align='left'><b>ASSIGNMENT STATISTICS</b></td></tr><br><br>";

        $html .= "<table class='middle-table' cellspacing='2' cellpadding='10' border='2' width='100%' style='background:black;font-family:lucidasansregular;border:2px solid #dadada;'>
                  <tr>
                  <td style='background:#fff;width: 3%;'></td>
                  <td style='background:#fff;width: 2%;'><b style='font-size: 10px;'>Total Marks</b></td>
                  <td style='background:#fff;width: 2%;'><b style='font-size: 10px;'>%</b></td>";
        $count=1;
        foreach($hwquestions as $hwquestion):
            $question=Questions::model()->findByPk($hwquestion['TM_HQ_Question_Id']);
            if ($question->TM_QN_Type_Id != '5'):
                $html .= "<td style='background:#fff;width: 1%;'><b style='font-size: 10px;'>Qn ". $count ."</b></td>";
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                $multicount = 1;
                foreach ($questions AS $childquestion):
                    $html .= "<td style='background:#fff;width: 1%;'><b style='font-size: 10px;'>Qn ". $count.'.'.$multicount ."</b></td>";
                    $multicount++;
                endforeach;
            endif;
        $count++;
        endforeach;
        $html .= "</tr>";

        $html .= "<tr>
                  <td style='background:#fff;text-align:center;' colspan='3'></td>";
        foreach($hwquestions as $hwquestion):
            $question=Questions::model()->findByPk($hwquestion['TM_HQ_Question_Id']);
            if ($question->TM_QN_Type_Id != '5'):
                $html .= "<td style='background:#fff;width: 1%;'><b style='font-size: 10px;'>".$question->TM_QN_Totalmarks."</b></td>";
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                $multicount = 1;
                foreach ($questions AS $childquestion):
                    $html .= "<td style='background:#fff;width: 1%;'><b style='font-size: 10px;'>".$childquestion->TM_QN_Totalmarks."</b></td>";
                endforeach;
            endif;
        endforeach;

        $html .= "</tr>";

        foreach($studhws_attempt as $studhw):
            $studentdetails=Student::model()->find(array("condition"=>"TM_STU_User_Id='".$studhw->TM_SAR_STU_Student_Id."'"));
             /*$studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studhw->TM_STHW_Student_Id.'AND TM_STU_Status = "0"')); */
             if($studentdetails):
             $actualpercentage = $studhw->TM_SAR_STU_Percentage;
             if (strpos($actualpercentage, '.')!== false)
             {
                $finalpercentage = number_format($studhw->TM_SAR_STU_Percentage,2);
             }
             else{
                $finalpercentage = $actualpercentage;
             }
             $html .= "<tr>
                       <td style='background:#fff;font-size: 10px;'>".$studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name."</td>";
             //$paperquestions= Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_STHW_Student_Id."' AND TM_STHWQT_Type != '0'", "order" => "TM_STHWQT_Order ASC"));
             //echo count($paperquestions); exit;
            //$answered=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_STHW_Student_Id."' AND TM_STHWQT_Status='1' AND TM_STHWQT_Marks != '0'", "order" => "TM_STHWQT_Order ASC"));
            $answered=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_SAR_STU_Student_Id."'", "order" => "TM_STHWQT_Order ASC"));
            $totalquestions=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_SAR_STU_Student_Id."'", "order" => "TM_STHWQT_Order ASC"));
            //if($answered){
                $html .= "<td style='background:#fff;font-size: 10px;'>".$studhw->TM_SAR_STU_TotalMarks."</td>";
                if($finalpercentage!=''){
                 $html .= "<td style='background:#fff;font-size: 10px;'>".$finalpercentage."%</td>";
                }
                else{
                 $html .= "<td style='background:#fff;font-size: 10px;'>0%</td>";
                }
                foreach($hwquestions as $paperquestion):
                    //$question1=Studenthomeworkquestions::model()->findByPk($paperquestion->TM_STHWQT_Id);
                   /* $question1=Studenthomeworkquestions::model()->find(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_SAR_STU_Student_Id."' AND TM_STHWQT_Question_Id='".$paperquestion->TM_HQ_Question_Id."' AND TM_STHWQT_Type != '0'", "order" => "TM_STHWQT_Order ASC"));*/
                   $question1=SingleHwquestStatistics::model()->find(array("condition"=>"TM_SAR_QN_HW_Id='".$id."' AND TM_SAR_QN_Student_Id='".$studhw->TM_SAR_STU_Student_Id."' AND TM_SAR_QN_Question_Id='".$paperquestion->TM_HQ_Question_Id."'"));

                        if ($question1->TM_SAR_QN_Type != '5'){
                            $html .= "<td style='background:#fff;font-size: 10px;width: 1%;'>".$question1['TM_SAR_QN_Marks']."</td>";
                        }
                        else{
                           /* $questions1 = Studenthomeworkquestions::model()->findAll(array("condition" => "TM_STHWQT_Parent_Id ='".$question1->TM_STHWQT_Question_Id."' AND TM_STHWQT_Student_Id='".$question1->TM_STHWQT_Student_Id."' AND TM_STHWQT_Mock_Id='".$question1->TM_STHWQT_Mock_Id."'", "order" => "TM_STHWQT_Id ASC"));*/

                           $questions1=SingleHwquestStatistics::model()->findAll(array("condition" => "TM_SAR_QN_Parent_Id ='".$question1->TM_SAR_QN_Question_Id."' AND TM_SAR_QN_Student_Id='".$question1->TM_SAR_QN_Student_Id."' AND TM_SAR_QN_HW_Id='".$question1->TM_SAR_QN_HW_Id."'"));

                            foreach ($questions1 AS $childquestion1):
                                $html .= "<td style='background:#fff;font-size: 10px;width: 1%;'>".$childquestion1['TM_SAR_QN_Marks']."</td>";
                            endforeach;
                        }

                endforeach;
            /*}
            else{
                $multipart=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_STHW_Student_Id."' AND TM_STHWQT_Type = '0'", "group" => "TM_STHWQT_Parent_Id"));
                $colspan = count($totalquestions) - count($multipart) + 2;
                $html .= "<td style='background:#fff;text-align:center;' colspan='".$colspan."'>NA</td>";
            } */
            endif;
        endforeach;

        $html .= "</tr>";
        $totalquestionscount=0;
        $multipartcount=0;
        foreach($studhws_notattempt as $studhw):

                /*$studentdetails=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studhw->TM_STHW_Student_Id.'AND TM_STU_Status = "0"'));*/
                $studentdetails=Student::model()->find(array("condition"=>"TM_STU_User_Id='".$studhw->TM_SAR_STU_Student_Id."'"));
                if($studentdetails):
                 $actualpercentage = $studhw->TM_SAR_STU_Percentage;
                 if (strpos($actualpercentage, '.')!== false)
                 {
                    $finalpercentage = number_format($studhw->TM_SAR_STU_Percentage,2);
                 }
                 else{
                    $finalpercentage = $actualpercentage;
                 }
                 $html .= "<tr><td style='background:#fff;font-size: 10px;'>".$studentdetails->TM_STU_First_Name.' '.$studentdetails->TM_STU_Last_Name."</td>";
                $totalquestions=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_SAR_STU_Student_Id."'", "order" => "TM_STHWQT_Order ASC"));
                if(count($totalquestions)>0)
                {
                    if($totalquestionscount==0)
                    {
                        $totalquestionscount=count($totalquestions);
                    }

                }

                $multipart=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_SAR_STU_Student_Id."' AND TM_STHWQT_Type = '0'", "group" => "TM_STHWQT_Parent_Id"));
                 if(count($multipart)>0)
                 {
                    if($multipartcount==0)
                    {
                         $multipartcount=count($multipart);
                    }

                 }

                $colspan = $totalquestionscount - $multipartcount + 2;
                $html .= "<td style='background:#fff;text-align:center;' colspan='".$colspan."'>NA</td>";
                endif;
        endforeach;

        $html .= "</tr></table>";

        /* Second Table ends */

        /* Third Table starts */

        $html .= "<table id='footer' width='100%' style='page-break-before: always;font-size: 12px;font-family: STIXGeneral;display: none'>
                   <tr>
                      <td width='100%' style='vertical-align: top;padding:0;'>
                        <p>© Copyright @ TabbieMe Ltd. All rights reserved .</p>
                      </td>
                      <td style='vertical-align: top;padding:0;'>
                        <p>www.tabbiemath.com</p>
                      </td>
                  </tr>
                </table>";
        $html .="<br><table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b>".$schoolname."</b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'><b>".$homework."</b></td></tr><br><br></table>";
        $html .= "<table cellspacing='1' cellpadding='10' border='2' width='100%' style='background:black;font-family:lucidasansregular;border:1px solid #dadada;'>
                  <tr>
                  <td style='background:#fff;'>Assignment Set Date: ".$publishdate."</td>
                  <td style='background:#fff;'>Assignment completion date: ".$finishdate."</td>
                  </tr>
                  <tr>
                  <td style='background:#fff;'>Teacher: ".$staffschool->TM_TH_Name."</td>
                  <td style='background:#fff;'>Published to : ".$string."</td>
                  </tr>
                  <tr>
                  <td style='background:#fff;'>Total No of Questions: ".count($hwquestions)."</td>
                  <td style='background:#fff;'>Total Marks: ".$singleHwstatics->TM_SAR_Total_Marks."</td>
                  </tr>
                  <tr>
                  <td style='background:#fff;'>No of students attempted: ".$singleHwstatics->TM_SAR_Atempted."</td>
                  <td style='background:#fff;'>No of students not attempted: ".$singleHwstatics->TM_SAR_Not_Attempted."</td>
                  </tr>
                  </table><br><br>";
        $html .= "<tr><td colspan=3 align='left'><b>QUESTION STATISTICS</b></td></tr><br><br>";

        $html .= "<table cellspacing='2' cellpadding='10' border='2' width='100%' style='background:black;font-family:lucidasansregular;border:2px solid #dadada;'>
                  <tr>
                  <td style='background:#fff;'></td>
                  <td style='background:#fff;'><b style='font-size: 14px;'>Chapter Name</b></td>
                  <td style='background:#fff;'><b style='font-size: 14px;'>Topic Name</b></td>
                  <td style='background:#fff;'><b style='font-size: 14px;'>No of students getting qn right</b></td>
                  <td style='background:#fff;'><b style='font-size: 14px;'>% students getting question right</b></td></tr>";

        $count1=1;
        //echo $attstds;exit;
        foreach($hwquestions as $hwquestion):
            $question=Questions::model()->findByPk($hwquestion['TM_HQ_Question_Id']);
            $totalstudents = count($studhws);
            if ($question->TM_QN_Type_Id != '5'):
                $question_statistics=AssesmentStatisticsQuestions::model()->find(array('condition'=>'schoolhomework_id='.$id.' AND question_id ='.$hwquestion['TM_HQ_Question_Id'].''));


                $chapter = Chapter::model()->findByPk($question_statistics->chapter_id);
                $topic = Topic::model()->findByPk($question_statistics->topic_id);
                //$percentage = $this->GetIndividualquestionstatusnew($hwquestion->TM_HQ_Question_Id,$hwquestion->TM_HQ_Homework_Id,$attstds);
                if($question_statistics->total_attempted>0){
                $percent=($question_statistics->total_corrected/$question_statistics->total_attempted)*100;
                if (strpos($percent, '.')!== false)
                 {
                    $percentage=number_format($percent,2);
                 }
                 else
                 {
                    $percentage=$percent;
                 }

                }

                //$totalstudentscorrect = ($percentage / 100) * $totalstudents;
                //$totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$question->TM_QN_Id."' AND TM_STHWQT_Student_Id!='0' AND TM_STHWQT_Marks='".$question->TM_QN_Totalmarks."'"));
/*                if($attstds!='')
                {
                    $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$question->TM_QN_Id."' AND TM_STHWQT_Student_Id IN (".$attstds.") AND TM_STHWQT_Marks='".$question->TM_QN_Totalmarks."'"));
                }*/
                //$percentage = (count($totalstudentscorrect) / $totalanswered) * 100;
                $html .= "<tr>
                          <td style='background:#fff;'>Question ". $count1 ."</td>
                          <td style='background:#fff;'>".$chapter->TM_TP_Name."</td>
                          <td style='background:#fff;'>".$topic->TM_SN_Name."</td>
                          <td style='background:#fff;'>".$question_statistics->total_corrected."</td>
                          <td style='background:#fff;'>".$percentage."%</td>
                          </tr>";
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                $multicount1 = 1;
                foreach ($questions AS $childquestion):
                    $chapter = Chapter::model()->findByPk($childquestion->TM_QN_Topic_Id);
                    $topic = Topic::model()->findByPk($childquestion->TM_QN_Section_Id);

                    $percentage = $this->GetIndividualquestionstatusnew($childquestion->TM_QN_Id,$hwquestion->TM_HQ_Homework_Id,$attstds);
                    //$totalstudentscorrect = ($percentage / 100) * $totalstudents;
                    if($attstds!='')
                    {
                         $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Question_Id='".$childquestion->TM_QN_Id."' AND TM_STHWQT_Student_Id IN (".$attstds.") AND TM_STHWQT_Marks='".$childquestion->TM_QN_Totalmarks."'"));
                    }
                    else
                    {
                        $totalstudentscorrect=0;
                    }

                    //$percentage = (count($totalstudentscorrect) / $totalanswered) * 100;
                    $html .= "<tr>
                              <td style='background:#fff;'>Question ". $count1.'.'.$multicount1 ."</td>
                              <td style='background:#fff;'>".$chapter->TM_TP_Name."</td>
                              <td style='background:#fff;'>".$topic->TM_SN_Name."</td>
                              <td style='background:#fff;'>".count($totalstudentscorrect)."</td>
                              <td style='background:#fff;'>".$percentage."%</td>
                              </tr>";
                    $multicount1++;
                endforeach;
            endif;
        $count1++;
        endforeach;

        $html .= "</table>";

        /* Third Table ends */

        //$html .= "</table>";
        $this->render('reportpdfview',array('html'=>$html));
        //echo $html; exit;
        //$mpdf = new mPDF();
        //$name='ASSIGNMENT_Report_'.time().'.pdf';
        //$mpdf->WriteHTML($html);
        //$mpdf->Output($name, 'I');
    }

    public function GetIndividualquestionstatusnew($question,$homework,$attstds)
    {
        $selectedquestion = Questions::model()->findByPk($question);
        $connection = CActiveRecord::getDbConnection();
        //$sql="SELECT SUM(TM_STHWQT_Marks) AS studenttotal FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Question_Id='".$question."' AND TM_STHWQT_Mock_Id='".$homework."' AND TM_STHWQT_Marks='".$selectedquestion->TM_QN_Totalmarks."' AND TM_STHWQT_Status='1'";
        $sql="SELECT SUM(TM_STHWQT_Marks) AS studenttotal FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Question_Id='".$question."' AND TM_STHWQT_Mock_Id='".$homework."' AND TM_STHWQT_Marks='".$selectedquestion->TM_QN_Totalmarks."'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $studenttotal=$dataReader->read();
		//$totalstudentscorrect=$studenttotal['studenttotal'];
        //$studhws=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_HomeWork_Id='$homework' AND TM_STHW_Student_Id!='0' "));
        $studhws=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id="'.$homework.'" AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        //$totalstudents= $selectedquestion->TM_QN_Totalmarks * count($studhws);

        $totalstudents = 0;
        foreach($studhws as $studhw):
          $detail=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studhw->TM_STHW_Student_Id.''));
          if($detail):
              //$answered=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$id."' AND TM_STHWQT_Student_Id='".$studhw->TM_STHW_Student_Id."' AND TM_STHWQT_Status='1' AND TM_STHWQT_Marks != '0'", "order" => "TM_STHWQT_Order ASC"));
              $answered=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$homework."' AND TM_STHWQT_Student_Id='".$studhw->TM_STHW_Student_Id."'", "order" => "TM_STHWQT_Order ASC"));
              if($answered){
                $totalstudents++;
              }
          endif;
        endforeach;
        if($attstds!='')
        {
            $totalstudentscorrect=Studenthomeworkquestions::model()->findAll(array("condition"=>"TM_STHWQT_Mock_Id='".$homework."' AND TM_STHWQT_Question_Id='".$question."' AND TM_STHWQT_Student_Id IN (".$attstds.") AND TM_STHWQT_Marks='".$selectedquestion->TM_QN_Totalmarks."'"));
        }
        if($totalstudents!=0):
            if($totalstudentscorrect!=0 || $totalstudentscorrect!=''):
                //$percentage = ($totalstudentscorrect / $totalstudents) * 100;
                  $percentage = (count($totalstudentscorrect) / $totalstudents) * 100;
            else:
                $percentage = 0;
            endif;
        else:
              $percentage = 0;
        endif;
        return round($percentage);
    }

    public function actionCorrectingCompletedStatus()
    {
        $studhws=StudentHomeworks::model()->findAll(array('condition' => "TM_STHW_Student_Id!='0' AND TM_STHW_Status!='0' Limit 20500,500 "));

        foreach($studhws as $studhw){
           $question=Studenthomeworkquestions::model()->find(array("condition"=>"TM_STHWQT_Mock_Id='".$studhw->TM_STHW_HomeWork_Id."' AND TM_STHWQT_Student_Id='".$studhw->TM_STHW_Student_Id."'"));
           if($question){
           $question->TM_STHWQT_Status = 1;
           $question->save(false);
           }
        }
    }

    public function actionAddSection()
    {
        $mockid=$_POST['practiceid'];
        $questionid=$_POST['qnids'];
        $questionhomework=CustomtemplateQuestions::model()->find(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."' AND TM_CTQ_Question_Id='".$questionid."' "));
        $questionhomework->TM_CTQ_Section=$_POST['section'];
        $questionhomework->TM_CTQ_Header=$_POST['header'];
        if($questionhomework->save(false)):
            echo "yes";
        endif;
    }

    public function actionGetHeaderCustom()
    {
        $mockid=$_POST['homeworkid'];
        $questionid=$_POST['questionid'];
        $questionhomework=CustomtemplateQuestions::model()->find(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."' AND TM_CTQ_Question_Id='".$questionid."' "));
        $header=$questionhomework['TM_CTQ_Header'];
        $section=$questionhomework['TM_CTQ_Section'];
        $data=array('header'=>$header,'section'=>$section);
        echo json_encode($data);
    }

    public function actionDeleteheader()
    {
        $mockid=$_POST['homeworkid'];
        $questionid=$_POST['questionid'];
        $questionhomework=CustomtemplateQuestions::model()->find(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."' AND TM_CTQ_Question_Id='".$questionid."' "));
        $questionhomework->TM_CTQ_Header='';
        $questionhomework->TM_CTQ_Section='';
        $questionhomework->save(false);
        $data=array($res='success');
        echo json_encode($data);

    }
    public function actionQstnoptions()
    {
        $mockid=$_POST['practiceid'];
        $questionid=$_POST['qnids'];
        $questionoptions=CustomtemplateQuestions::model()->find(array('condition'=>"TM_CTQ_Custom_Id='".$mockid."' AND TM_CTQ_Question_Id='".$questionid."' "));
        $questionoptions->TM_CTQ_Show_Options=$_POST['option'];
        $questionoptions->save(false);
        $data=array($res='success');
        echo json_encode($data);

    }
    public function actionLinkresources()
    {
        $this->layout = '//layouts/teacher';
        $model=new Mock('searchschool');
        $worksheet_id=$_GET['resource'];
        $condition="";
        if(isset($_POST['Mock']['TM_MK_Name'])){
         $condition=" AND (TM_MK_Tags REGEXP '".$_POST['Mock']['TM_MK_Name']."' OR
                        TM_MK_Name LIKE '%".$_POST['Mock']['TM_MK_Name']."%' OR
                        TM_MK_Description LIKE '%".$_POST['Mock']['TM_MK_Name']."%')";
        }
        else
        {
              $condition="AND TM_MK_Tags REGEXP '".$worksheet_id."'";
        }
        if(isset($_POST['tags'])):
         $condition=" AND (TM_MK_Tags REGEXP '".$_POST['tags']."' OR
                        TM_MK_Name LIKE '%".$_POST['tags']."%' OR
                        TM_MK_Description LIKE '%".$_POST['tags']."%')";
        endif;
         $criteria = new CDbCriteria;
        $criteria->addCondition("TM_MK_Standard_Id='".Yii::app()->session['standard']."'    $condition");
        $criteria->order = 'TM_MK_Sort_Order DESC';
        $mocks = Mock::model()->findAll($criteria);
       /* $mocks=Mock::model()->findAll(array('condition'=>'TM_MK_Tags REGEXP '.$worksheet_id.' AND TM_MK_Standard_Id='.Yii::app()->session['standard'].''));*/
        $mockheaders=MockTags::model()->findAll(array('condition'=>'TM_MT_Type=1 AND TM_MT_Standard_Id='.Yii::app()->session['standard'].' '));

            $this->render('wrksheetresource',array(
            'mocks'=>$mocks,
            'mockheaders'=>$mockheaders,
            'model'=>$model,
        ));

    }

      public function actionLinkworksheet()
    {
        $this->layout = '//layouts/teacher';
        $worksheet_id=$_GET['worksheet'];
        $model=new Mock('searchschool');
        $model->unsetAttributes();
         $condition='';
        if(isset($_POST['Mock']['TM_MK_Name']))
        {

              $condition=" AND (TM_MK_Tags REGEXP '".$_POST['Mock']['TM_MK_Name']."' OR
                        TM_MK_Name LIKE '%".$_POST['Mock']['TM_MK_Name']."%' OR
                        TM_MK_Description LIKE '%".$_POST['Mock']['TM_MK_Name']."%')";
        }
        else
        {
             $condition="AND TM_MK_Tags REGEXP '".$worksheet_id."'";
        }
        /*$mocks=Mock::model()->findAll(array('condition'=>'TM_MK_Tags REGEXP '.$worksheet_id.' AND TM_MK_Standard_Id='.Yii::app()->session['standard'].''));*/

        if(isset($_POST['tags'])):
            $condition=" AND (TM_MK_Tags REGEXP '".$_POST['tags']."' OR
                        TM_MK_Name LIKE '%".$_POST['tags']."%' OR
                        TM_MK_Description LIKE '%".$_POST['tags']."%')";
        endif;

        $criteria = new CDbCriteria;
         $criteria->join = 'INNER JOIN tm_mock_school AS m ON m.TM_MS_Mock_Id= TM_MK_Id LEFT JOIN tm_profiles AS n ON n.user_id= TM_MK_CreatedBy';
        $criteria->addCondition("TM_MK_Standard_Id='".Yii::app()->session['standard']."' AND TM_MK_Status=0 AND TM_MK_Availability != 0 AND TM_MK_Resource=0 AND m.TM_MS_School_Id='".Yii::app()->session['school']."'  $condition");

        //$criteria->addCondition("TM_MK_Standard_Id='".Yii::app()->session['standard']."'   $condition");
         $criteria->order = 'TM_MK_Sort_Order DESC';
        $mocks = Mock::model()->findAll($criteria);
        $mockheaders=MockTags::model()->findAll(array('condition'=>'TM_MT_Type=0 AND TM_MT_Standard_Id='.Yii::app()->session['standard'].' '));
            $this->render('rsrceworksheet',array(
            'mocks'=>$mocks,
            'mockheaders'=>$mockheaders,
            'model'=>$model,
        ));

    }

    public function actionStudentreopen()
    {
        $homewrkid=$_POST['homeworid'];
        $studentid=$_POST['studentid'];
        $studhwqws=Studenthomeworkquestions::model()->findAll(array('condition' => "TM_STHWQT_Student_Id='".$studentid."' AND TM_STHWQT_Mock_Id='".$homewrkid."'"));
      foreach ($studhwqws as $key => $studhwqw) {
          $studhwqw->TM_STHWQT_Answer='';
          $studhwqw->TM_STHWQT_Marks=0;
          $studhwqw->TM_STHWQT_Status=0;
          $studhwqw->save(false);
      }
       $studhws=StudentHomeworks::model()->find(array('condition' => "TM_STHW_Student_Id='".$studentid."' AND TM_STHW_HomeWork_Id='".$homewrkid."'"));
        $studhws->TM_STHW_Status=0;
        $studhws->TM_STHW_Percentage='';
        $studhws->TM_STHW_Marks=0;
        if($studhws->save(false))
        {
           $stud_detail=Student::model()->find(array('condition' => "TM_STU_User_Id='".$studentid."'"));
           $schoolhomework=Schoolhomework::model()->find(array('condition' => "TM_SCH_Id='".$homewrkid."'"));
           $teacherprofile=Profile::model()->findByPk(Yii::app()->user->id);
           $teachermail=User::model()->findByPk(Yii::app()->user->id);
           $parent_detail= User::model()->findByPk($stud_detail->TM_STU_Parent_Id);
           //print_r($teachermail); exit;
           $adminEmail = Yii::app()->params['noreplayEmail'];
            $message="<p>Dear ".$stud_detail->TM_STU_First_Name.' '.$stud_detail->TM_STU_Last_Name.",</p>
                    <p>This is to let you know that assignment <b>".$schoolhomework->TM_SCH_Name."</b> has been  reopened for you. Please complete as instructed.</p>
                    <p>Regards,<br>".$teacherprofile->firstname.' '.$teacherprofile->lastname."</p>";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($parent_detail->email);
        $mail->AddCC($teachermail->email, "Teacher");
        $mail->AddBCC("support@tabbiemath.com");
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'Assignment Reopened';
        $mail->Body = $message;
        $mail->Send();

        }

    }

    public function actionEditscreen()
    {
        if(isset($_POST['create']))
        {
            $model = new Imagetag();
            $model->assignment_id=$_GET['assignment'];
            $model->pic_id=$_GET['name'];
            $model->comments=$_POST['comment'];
            $model->pic_x=$_POST['imagex'];
            $model->pic_y=$_POST['imagey'];
            $model->save(false);
        }
        if(isset($_POST['update']))
        {
           $model=Imagetag::model()->findByPk($_POST['commentid']);
           $model->comments=$_POST['commentedited'];
           $model->save(false);
        }

        if(isset($_POST['delete']))
        {
            if($_POST['delete']==1)
            {
                 $model=Imagetag::model()->findByPk($_POST['commentid']);
                 $model->delete();
            }
          
        }
        $this->render('imgedit');
    }

    public function actionSavetag()
    {


    }

    public function actionGetdata()
    {
         $model=Imagetag::model()->findByPk($_POST['id']);
         echo $model->comments;
    }
    public function actionEdittag()
    {
       $model=Imagetag::model()->findByPk($_POST['commentid']);
       $model->comments=$_POST['commentedited'];
       $model->save(false);

    }

        public function HasWorksheeturl($homework,$student)
    {
        $count=Studenthomeworkanswersurl::model()->count(array('condition'=>'TM_STHWARU_HW_Id='.$homework.' AND TM_STHWARU_Url!="" AND TM_STHWARU_Student_Id='.$student));

        return $count;
    }


}
