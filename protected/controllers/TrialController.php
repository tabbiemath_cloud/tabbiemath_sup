<?php

class TrialController extends Controller

{
    public $layout='//layouts/main';
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'accessControl', // perform access control for CRUD operations

        );
    }

    public function accessRules()
    {
        return array(
            /*array('deny', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('GetSyllabus','home','Getstandard','Revision','getTopics','starttest','RevisionTestTrial','Getpdf','RevisionTest','TestComplete','GetSolution','Showpaper','RedoComplete'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudent()'
            ),
*/
            array('allow',  // deny all users
                'users'=>array('*'),
            )
        );
    }
    public function beforeAction($action)
    {
           if (!Yii::app()->user->isGuest):
                $this->redirect(Yii::app()->createUrl('user/logout',array('referrer'=>'trial')));
           else:
                return true;
           endif;
           //something code right here if user valided
    }
    
	public function actionHome()
	{
        if(isset($_POST['startTrial'])):

            Yii::app()->session['syllabus'] =$_POST['syllabusdrop'];
            Yii::app()->session['standard'] =$_POST['standard'];
            $this->redirect(array('revision'));
            endif;
        $publishers=Publishers::model()->findAll();
		$this->render('home',array('publishers'=> $publishers));
	}
 public function actionRevision()
{
    $syllabus=Yii::app()->session['syllabus'];
    $standard=Yii::app()->session['standard'];


    $chapters=Chapter::model()->findAll(array('condition'=>"TM_TP_Syllabus_Id='$syllabus' AND TM_TP_Standard_Id='$standard'"));

    if(isset($_POST['startTest'])):


        if($_POST['testtype']=='Difficulty'):

            $criteria = new CDbCriteria;
            $criteria->addInCondition('TM_TP_Id' ,$_POST['chapter'] );
            $selectedchapters = Chapter::model()->findAll($criteria);
            $StudentTestTrial= new StudentTestTrial();
            $StudentTestTrial->TM_TR_STU_TT_Student_Id=Yii::app()->getSession()->getSessionId();
            $StudentTestTrial->TM_TR_STU_TT_Publisher_Id=3;
            $StudentTestTrial->TM_TR_STU_TT_Type='0';
            $StudentTestTrial->TM_TR_STU_TT_Mode='0';
            $StudentTestTrial->save(false);
            $count=1;
            $totalquestions=0;
            foreach($selectedchapters AS $key=>$chapter):
                if($_POST['chaptertotal'.$chapter->TM_TP_Id]!='0'):
                    $testchapter=new StudentTestChaptersTrial();
                    $testchapter->TM_TR_STC_Student_Id=Yii::app()->getSession()->getSessionId();
                    $testchapter->TM_TR_STC_Test_Id=$StudentTestTrial->TM_TR_STU_TT_Id;
                    $testchapter->TM_TR_STC_Chapter_Id=$chapter->TM_TP_Id;
                    $testchapter->save(false);
                    $topics='';
                    for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                        $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                    endfor;


                    //$topics='"'.$topics.'"';
                    $totalquestions=$totalquestions+$_POST['basic'.$chapter->TM_TP_Id]+$_POST['inter'.$chapter->TM_TP_Id]+$_POST['adv'.$chapter->TM_TP_Id];
                    if($_POST['basic'.$chapter->TM_TP_Id]!='0'):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;

                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='1';
                        $limit=$_POST['basic'.$chapter->TM_TP_Id];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;

                    if($_POST['inter'.$chapter->TM_TP_Id]!='0'):
                        $$Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='2';
                        $limit=$_POST['inter'.$chapter->TM_TP_Id];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                    if($_POST['adv'.$chapter->TM_TP_Id]!='0'):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='3';
                        $limit=$_POST['adv'.$chapter->TM_TP_Id];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                endif;
            endforeach;
            if($count!='0'):
                $command = Yii::app()->db->createCommand("CALL SetTrialQuestionsNum(:Test)");
                $command->bindParam(":Test", $StudentTestTrial->TM_TR_STU_TT_Id);
                $command->query();
                $selectedquestions=base64_encode('Remove_'.base64_encode($totalquestions));
                $addedquestions=base64_encode('Remove_'.base64_encode($count-1));
                $this->redirect(array('starttest','id'=>$StudentTestTrial->TM_TR_STU_TT_Id,'inque'=>$selectedquestions,'outque'=>$addedquestions));
            endif;
        else:
            $criteria = new CDbCriteria;
            $criteria->addInCondition('TM_TP_Id' ,$_POST['quickchapter'] );
            $selectedchapters = Chapter::model()->findAll($criteria);
            $StudentTestTrial= new StudentTestTrial();
            $StudentTestTrial->TM_TR_STU_TT_Student_Id=Yii::app()->getSession()->getSessionId();
            $StudentTestTrial->TM_TR_STU_TT_Publisher_Id=3;
            $StudentTestTrial->TM_TR_STU_TT_Type='0';
            $StudentTestTrial->TM_TR_STU_TT_Mode='1';
            $StudentTestTrial->save(false);
            $count=1;
            $totalquestions=0;
            foreach($selectedchapters AS $key=>$chapter):
                if($_POST['quickchaptertotal'.$chapter->TM_TP_Id]!='0'):
                    $testchapter=new StudentTestChaptersTrial();
                    $testchapter->TM_TR_STC_Student_Id=Yii::app()->getSession()->getSessionId();
                    $testchapter->TM_TR_STC_Test_Id=$StudentTestTrial->TM_TR_STU_TT_Id;
                    $testchapter->TM_TR_STC_Chapter_Id=$chapter->TM_TP_Id;
                    $testchapter->save(false);
                    $quicklimits=$this->GetQuickLimits($_POST['quickchaptertotal'.$chapter->TM_TP_Id]);
                    $topics='';
                    for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                        $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                    endfor;
                    //$topics='"'.$topics.'"';
                    $totalquestions=$totalquestions+$_POST['quick'.$chapter->TM_TP_Id];
                    if($quicklimits['basic']!=0):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='1';
                        $limit=$quicklimits['basic'];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                    endif;
                    if($quicklimits['intermediate']!=0):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='2';
                        $limit=$quicklimits['intermediate'];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                    endif;
                    if($quicklimits['advanced']!=0):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='2';
                        $limit=$quicklimits['advanced'];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                    endif;
                endif;
            endforeach;
            if($count!='0'):
                $command = Yii::app()->db->createCommand("CALL SetTrialQuestionsNum(:Test)");
                $command->bindParam(":Test", $StudentTestTrial->TM_TR_STU_TT_Id);
                $command->query();

                $selectedquestions=base64_encode('Remove_'.base64_encode($totalquestions));
                $addedquestions=base64_encode('Remove_'.base64_encode($count-1));
                $this->redirect(array('starttest','id'=>$StudentTestTrial->TM_TR_STU_TT_Id,'inque'=>$selectedquestions,'outque'=>$addedquestions));
            endif;
        endif;
    //$this->redirect(array('RevisionDifficulty','id'=>$StudentTestTrial->TM_TR_STU_TT_Id));
    elseif(isset($_POST['addTest'])):
        if($_POST['testtype']=='Difficulty'):
            $criteria = new CDbCriteria;
            $criteria->addInCondition('TM_TP_Id' ,$_POST['chapter'] );
            $selectedchapters = Chapter::model()->findAll($criteria);
            $StudentTestTrial= new StudentTestTrial();
            $StudentTestTrial->TM_TR_STU_TT_Student_Id=Yii::app()->getSession()->getSessionId();
            $StudentTestTrial->TM_TR_STU_TT_Publisher_Id=3;
            $StudentTestTrial->TM_TR_STU_TT_Type='0';
            $StudentTestTrial->TM_TR_STU_TT_Mode='0';
            $StudentTestTrial->save(false);
            $count=1;
            $totalquestions=0;
            foreach($selectedchapters AS $key=>$chapter):
                if($_POST['chaptertotal'.$chapter->TM_TP_Id]!='0'):
                    $testchapter=new StudentTestChaptersTrial();
                    $testchapter->TM_TR_STC_Student_Id=Yii::app()->getSession()->getSessionId();
                    $testchapter->TM_TR_STC_Test_Id=$StudentTestTrial->TM_TR_STU_TT_Id;
                    $testchapter->TM_TR_STC_Chapter_Id=$chapter->TM_TP_Id;
                    $testchapter->save(false);
                    $topics='';
                    for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                        $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                    endfor;
                    //$topics='"'.$topics.'"';
                    $totalquestions=$totalquestions+$_POST['basic'.$chapter->TM_TP_Id]+$_POST['inter'.$chapter->TM_TP_Id]+$_POST['adv'.$chapter->TM_TP_Id];
                    if($_POST['basic'.$key]!='0'):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='1';
                        $limit=$_POST['basic'.$chapter->TM_TP_Id];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                    if($_POST['inter'.$chapter->TM_TP_Id]!='0'):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='2';
                        $limit=$_POST['inter'.$chapter->TM_TP_Id];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                    if($_POST['adv'.$chapter->TM_TP_Id]!='0'):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='3';
                        $limit=$_POST['adv'.$chapter->TM_TP_Id];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                endif;
            endforeach;
            if($count!='0'):
                $command = Yii::app()->db->createCommand("CALL SetTrialQuestionsNum(:Test)");
                $command->bindParam(":Test", $StudentTestTrial->TM_TR_STU_TT_Id);
                $command->query();
                $this->redirect(array('home'));
            endif;
        else:
            $criteria = new CDbCriteria;
            $criteria->addInCondition('TM_TP_Id' ,$_POST['quickchapter'] );
            $selectedchapters = Chapter::model()->findAll($criteria);
            $StudentTestTrial= new StudentTestTrial();
            $StudentTestTrial->TM_TR_STU_TT_Student_Id=Yii::app()->getSession()->getSessionId();
            $StudentTestTrial->TM_TR_STU_TT_Publisher_Id=3;
            $StudentTestTrial->TM_TR_STU_TT_Type='0';
            $StudentTestTrial->TM_TR_STU_TT_Mode='1';
            $StudentTestTrial->save(false);
            $count=1;
            $totalquestions=0;
            foreach($selectedchapters AS $key=>$chapter):
                if($_POST['quickchaptertotal'.$key]!='0'):
                    $testchapter=new StudentTestChaptersTrial();
                    $testchapter->TM_TR_STC_Student_Id=Yii::app()->getSession()->getSessionId();
                    $testchapter->TM_TR_STC_Test_Id=$StudentTestTrial->TM_TR_STU_TT_Id;
                    $testchapter->TM_TR_STC_Chapter_Id=$chapter->TM_TP_Id;
                    $testchapter->save(false);
                    $quicklimits=$this->GetQuickLimits($_POST['quickchaptertotal'.$key]);
                    $topics='';
                    for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                        $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                    endfor;
                    //$topics='"'.$topics.'"';
                    $totalquestions=$totalquestions+$_POST['quick'.$chapter->TM_TP_Id];;
                    if($quicklimits['basic']!=0):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='1';
                        $limit=$quicklimits['basic'];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,1,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                    if($quicklimits['intermediate']!=0):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='2';
                        $limit=$quicklimits['intermediate'];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,1,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                    if($quicklimits['advanced']!=0):
                        $Syllabus= $chapter->TM_TP_Syllabus_Id;
                        $Standard= $chapter->TM_TP_Standard_Id;
                        $Chapter=$chapter->TM_TP_Id;
                        //$Topic=$topics;
                        $Test=$StudentTestTrial->TM_TR_STU_TT_Id;
                        $Student=Yii::app()->getSession()->getSessionId();
                        $dificulty='2';
                        $limit=$quicklimits['advanced'];
                        $command = Yii::app()->db->createCommand("CALL GetRevisionQuestionsTrial(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,1,@out)");
                        $command->bindParam(":Syllabus", $Syllabus);
                        $command->bindParam(":Standard", $Standard);
                        $command->bindParam(":Chapter", $Chapter);
                        $command->bindParam(":limit",$limit);
                        $command->bindParam(":Test",$Test);
                        $command->bindParam(":Student",$Student);
                        $command->bindParam(":dificulty",$dificulty);
                        $command->bindParam(":questioncount",$count);
                        $command->query();
                        $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                    endif;
                endif;
            endforeach;
            if($count!='0'):
                $command = Yii::app()->db->createCommand("CALL SetTrialQuestionsNum(:Test)");
                $command->bindParam(":Test", $StudentTestTrial->TM_TR_STU_TT_Id);
                $command->query();
                $this->redirect(array('home'));
            endif;
        endif;
    endif;

    $this->render('revision',array('chapters'=>$chapters));
}
	// Uncomment the following methods and override them if needed
    public function actionGetstandard()
    {
        if(isset($_POST['id']))
        {
            $data=Standard::model()->with('plan')->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0' AND TM_PN_Visibility=0",
                array(':syllabus'=>(int) $_POST['id']));

            $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
            /*              if(count($data)!='1'):
                            echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
                          endif;*/
            echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }
        }
    }

    public function actionGetSyllabus(){

        if(isset($_POST['id']))
          {
              $data=Standard::model()->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0'",
                  array(':syllabus'=>(int) $_POST['id']));

              $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
/*              if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
              endif;*/
        echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
        foreach($data as $value=>$name)
        {
            echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
        }
    }
    }
    public function actionGetTopics()
    {
        $arr = explode(',', $_POST['topicids']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_TP_Id' ,$arr );
        $model = Chapter::model()->findAll($criteria);

        $topic='';
        foreach ($model as $key=>$value) {
            $topic=($key=='0'?$value->TM_TP_Name:$topic.','.$value->TM_TP_Name);
        }
        echo $topic;

    }
    public function GetQuickLimits($total)
    {
        $mode=$total%3;
        if($mode==0):
            $count=$total/3;
            return array('basic'=>$count,'intermediate'=>$count,'advanced'=>$count);
        elseif($mode==1):
            $count=round($total/3);
            return array('basic'=>$count,'intermediate'=>$count+1,'advanced'=>$count);
        elseif($mode==2):
            $count=$total/3;
            return array('basic'=>ceil($count),'intermediate'=>ceil($count),'advanced'=>floor($count));
        endif;

    }
    public function actionStarttest($id)
    {
        $test=StudentTestTrial::model()->findByPk($id);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id='.$id.' AND TM_TR_STU_QN_Type NOT IN (6,7)';
        $totalonline=count(StudentQuestionsTrial::model()->findAll($criteria));
        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id='.$id.' AND TM_TR_STU_QN_Type IN (6,7)';
        $totalpaper=count(StudentQuestionsTrial::model()->findAll($criteria));
        $totalquestions=$totalonline+$totalpaper;
        //$selectedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['inque'])));
        //$addedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['outque'])));
        $this->render('starttest',array('id'=>$id,'test'=>$test,'totalonline'=>$totalonline,'totalpaper'=>$totalpaper,'totalquestions'=>$totalquestions));

    }
public function actionRevisionTestTrial($id)
{
    if(isset($_POST['action']['GoNext']))
    {

        if($_POST['QuestionType']!='5'):
            $criteria=new CDbCriteria;
            $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
            $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
            $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
            if($_POST['QuestionType']=='1'):
                $answer=implode(',',$_POST['answer']);
                $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
            elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                $answer=$_POST['answer'];
                $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
            elseif($_POST['QuestionType']=='4'):
                $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'];
            endif;
            $selectedquestion->TM_TR_STU_QN_Flag='1';
            $selectedquestion->save(false);
        else:
            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
            foreach($questions AS $child):
                $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                    array('TM_TR_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_TR_STU_QN_Test_Id'=>$id,'TM_TR_STU_QN_Student_Id'=>Yii::app()->getSession()->getSessionId())
                );
                if(count($childquestion)=='1'):
                    $selectedquestion=$childquestion;
                else:
                    $selectedquestion=new StudentQuestionsTrial();
                endif;
                $selectedquestion->TM_TR_STU_QN_Test_Id=$id;
                $selectedquestion->TM_TR_STU_QN_Student_Id=Yii::app()->getSession()->getSessionId();
                $selectedquestion->TM_TR_STU_QN_Question_Id=$child->TM_QN_Id;
                if($child->TM_QN_Type_Id=='1'):
                    $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                    $answer=$_POST['answer'.$child->TM_QN_Id];
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($child->TM_QN_Type_Id=='4'):
                    $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                endif;
                $selectedquestion->TM_TR_STU_QN_Parent_Id=$_POST['QuestionId'];
                $selectedquestion->TM_TR_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endforeach;
            $criteria=new CDbCriteria;
            $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
            $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
            $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
            $selectedquestion->TM_TR_STU_QN_Flag='1';
            $selectedquestion->save(false);
        endif;
        $questionid=StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id'=>$id),
            array(
                'condition'=>'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7) AND  TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Number>:tableid',
                'limit'=>1,
                'order'=>'TM_TR_STU_QN_Number ASC, TM_TR_STU_QN_Flag ASC',
                'params'=>array(':student'=>Yii::app()->getSession()->getSessionId(),':tableid'=>$_POST['QuestionNumber'])
            )
        );
    }
    if(isset($_POST['action']['GetPrevios'])):
        if($_POST['QuestionType']!='5'):
            if(isset($_POST['answer'])):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
                $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_TR_STU_QN_Flag='1';
                $selectedquestion->save(false);
            else:
                $criteria=new CDbCriteria;
                $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
                $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
                $selectedquestion->TM_TR_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endif;
        else:
            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
            foreach($questions AS $child):
                if(isset($_POST['answer'.$child->TM_QN_Id])):
                    $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                        array('TM_TR_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_TR_STU_QN_Test_Id'=>$id,'TM_TR_STU_QN_Student_Id'=>Yii::app()->getSession()->getSessionId())
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new StudentQuestionsTrial();
                    endif;
                    $selectedquestion->TM_TR_STU_QN_Test_Id=$id;
                    $selectedquestion->TM_TR_STU_QN_Student_Id=Yii::app()->getSession()->getSessionId();
                    $selectedquestion->TM_TR_STU_QN_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_TR_STU_QN_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endif;
            endforeach;
            $criteria=new CDbCriteria;
            $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
            $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
            $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
            $selectedquestion->TM_TR_STU_QN_Flag='1';
            $selectedquestion->save(false);
        endif;
        $questionid=StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id'=>$id),
            array(
                'condition'=>'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Flag IN (0,1,2) AND TM_TR_STU_QN_Type NOT IN (6,7) AND TM_TR_STU_QN_Number<:tableid AND TM_TR_STU_QN_Parent_Id=0',
                'limit'=>1,
                'order'=>'TM_TR_STU_QN_Number DESC, TM_TR_STU_QN_Flag ASC',
                'params'=>array(':student'=>Yii::app()->getSession()->getSessionId(),':tableid'=>$_POST['QuestionNumber'])
            )
        );
    endif;
    if(isset($_POST['action']['DoLater'])):
        if($_POST['QuestionType']!='5'):
            if(isset($_POST['answer'])):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
                $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_TR_STU_QN_Flag='2';
                $selectedquestion->save(false);
            else:
                $criteria=new CDbCriteria;
                $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
                $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
                $selectedquestion->TM_TR_STU_QN_Flag='2';
                $selectedquestion->save(false);
            endif;
        else:
            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
            foreach($questions AS $child):
                if(isset($_POST['answer'.$child->TM_QN_Id])):
                    $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                        array('TM_TR_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_TR_STU_QN_Test_Id'=>$id,'TM_TR_STU_QN_Student_Id'=>Yii::app()->getSession()->getSessionId())
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new StudentQuestionsTrial();
                    endif;
                    $selectedquestion->TM_TR_STU_QN_Test_Id=$id;
                    $selectedquestion->TM_TR_STU_QN_Student_Id=Yii::app()->getSession()->getSessionId();
                    $selectedquestion->TM_TR_STU_QN_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_TR_STU_QN_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endif;
            endforeach;

            $criteria=new CDbCriteria;
            $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
            $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
            $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
            $selectedquestion->TM_TR_STU_QN_Flag='2';
            $selectedquestion->save(false);
        endif;
        $questionid=StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id'=>$id),
            array(
                'condition'=>'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7) AND  TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Number>:tableid',
                'limit'=>1,
                'order'=>'TM_TR_STU_QN_Number ASC, TM_TR_STU_QN_Flag ASC',
                'params'=>array(':student'=>Yii::app()->getSession()->getSessionId(),':tableid'=>$_POST['QuestionNumber'])
            )
        );
    endif;
    if (isset($_POST['action']['Skipcomplete'])):
        if ($_POST['QuestionType'] != '5'):
            if (isset($_POST['answer'])):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_TR_STU_QN_Flag = '2';
                $selectedquestion->save(false);
            else:
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                $selectedquestion->TM_TR_STU_QN_Flag = '2';
                $selectedquestion->save(false);
            endif;
        else:
            $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
            foreach ($questions AS $child):
                if (isset($_POST['answer' . $child->TM_QN_Id])):
                    $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                        array('TM_TR_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_TR_STU_QN_Test_Id' => $id, 'TM_TR_STU_QN_Student_Id' => Yii::app()->getSession()->getSessionId())
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new StudentQuestionsTrial();
                    endif;
                    $selectedquestion->TM_TR_STU_QN_Test_Id = $id;
                    $selectedquestion->TM_TR_STU_QN_Student_Id = Yii::app()->getSession()->getSessionId();
                    $selectedquestion->TM_TR_STU_QN_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_TR_STU_QN_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endif;
            endforeach;
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
            $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
            $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
            $selectedquestion->TM_TR_STU_QN_Flag = '2';
            $selectedquestion->save(false);
        endif;
        $questionid = StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id' => $id),
            array(
                'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7) AND  TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Number>:tableid',
                'limit' => 1,
                'order' => 'TM_TR_STU_QN_Number ASC, TM_TR_STU_QN_Flag ASC',
                'params' => array(':student' => Yii::app()->getSession()->getSessionId(), ':tableid' => $_POST['QuestionNumber'])
            )
        );
        if ($this->GetPaperCount($id) != 0):
            $test = StudentTestTrial::model()->findByPk($id);
            $test->TM_TR_STU_TT_Status = '2';
            $test->save(false);
            $this->redirect(array('showpaper', 'id' => $id));
        else:
            $test = StudentTestTrial::model()->findByPk($id);
            $test->TM_TR_STU_TT_Status = '1';
            $test->save(false);
            $this->redirect(array('TestComplete', 'id' => $id));
        endif;
    endif;
    if(isset($_POST['action']['Complete']))
    {

        if($_POST['QuestionType']!='5'):
            $criteria=new CDbCriteria;
            $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
            $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
            $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
            if($_POST['QuestionType']=='1'):
                $answer=implode(',',$_POST['answer']);
                $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
            elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                $answer=$_POST['answer'];
                $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
            elseif($_POST['QuestionType']=='4'):
                $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'];
            endif;
            $selectedquestion->TM_TR_STU_QN_Flag='1';
            $selectedquestion->save(false);
        else:
            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
            foreach($questions AS $child):
                $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                    array('TM_TR_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_TR_STU_QN_Test_Id'=>$id,'TM_TR_STU_QN_Student_Id'=>Yii::app()->getSession()->getSessionId())
                );
                if(count($childquestion)=='1'):
                    $selectedquestion=$childquestion;
                else:
                    $selectedquestion=new StudentQuestionsTrial();
                endif;
                $selectedquestion->TM_TR_STU_QN_Test_Id=$id;
                $selectedquestion->TM_TR_STU_QN_Student_Id=Yii::app()->getSession()->getSessionId();
                $selectedquestion->TM_TR_STU_QN_Question_Id=$child->TM_QN_Id;
                if($child->TM_QN_Type_Id=='1'):
                    $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                    $answer=$_POST['answer'.$child->TM_QN_Id];
                    $selectedquestion->TM_TR_STU_QN_Answer_Id=$answer;
                elseif($child->TM_QN_Type_Id=='4'):
                    $selectedquestion->TM_TR_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                endif;
                $selectedquestion->TM_TR_STU_QN_Parent_Id=$_POST['QuestionId'];
                $selectedquestion->save(false);
            endforeach;
            $criteria=new CDbCriteria;
            $criteria->condition='TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
            $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id);
            $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
            $selectedquestion->TM_TR_STU_QN_Flag='1';
            $selectedquestion->save(false);
        endif;
        if($this->GetPaperCount($id)!=0):
            $test=StudentTestTrial::model()->findByPk($id);
            $test->TM_TR_STU_TT_Status='2';
            $test->save(false);
            $this->redirect(array('showpaper','id'=>$id));
        else:
            $test=StudentTestTrial::model()->findByPk($id);
            $test->TM_TR_STU_TT_Status='1';
            $test->save(false);
            $this->redirect(array('TestComplete','id'=>$id));
        endif;
        //$this->redirect(array('TestComplete','id'=>$id));
    }
    if(!isset($_POST['action'])):
        if(isset($_GET['questionnum'])):
            $questionid=StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Flag IN (0,1,2) AND TM_TR_STU_QN_Type NOT IN (6,7)  AND TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Number='.str_replace("question_","",base64_decode($_GET['questionnum'])),
                    'limit'=>1,
                    'order'=>'TM_TR_STU_QN_Number ASC, TM_TR_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->getSession()->getSessionId())
                )
            );
        else:
            $questionid=StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Flag IN (0) AND TM_TR_STU_QN_Type NOT IN (6,7)  AND TM_TR_STU_QN_Parent_Id=0',
                    'limit'=>1,
                    'order'=>'TM_TR_STU_QN_Number ASC, TM_TR_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->getSession()->getSessionId())
                )
            );

        endif;
    endif;


    if(count($questionid)):

        $question=Questions::model()->findByPk($questionid->TM_TR_STU_QN_Question_Id);

        $this->renderPartial('showquestion',array('testid'=>$id,'question'=>$question,'questionid'=>$questionid));
    else:
      


        $this->redirect(array('revision', 'syllabus' => $questionid->TM_QN_Syllabus_Id,'standard'=> $questionid->TM_QN_Standard_Id));


    endif;
}
    public function OptionHint($type,$question)
    {
        $returnval=array();
        if($type=='1'):
            $corectans=Answers::model()->count(
                array(
                    'condition'=>'TM_AR_Question_Id=:question AND TM_AR_Correct=:correct',
                    'params'=>array(':question'=>$question,':correct'=>'1')
                ));
            $returnval['hint']='<span class="optionhint">Select any '.$corectans.' options</span>';
            $returnval['correctoptions']=$corectans;
        elseif($type=='2'):
            $returnval['hint']='<span class="optionhint">Select an option</span>';
            $returnval['correctoptions']='1';
        elseif($type=='3'):
            $returnval['hint']='<span class="optionhint">Select either True Or False</span>';
            $returnval['correctoptions']='1';
        elseif($type=='4'):
            $returnval['hint']='<span class="optionhint">Enter Your Answer</span>';
            $returnval['correctoptions']='0';
        elseif($type=='5'):
            $returnval['hint']='<span class="optionhint">Answer the following questions</span>';
            $returnval['correctoptions']='0';
        endif;
        return $returnval;
    }
    public function GetTestQuestions($id,$question)
    {

        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Type NOT IN (6,7)';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_TR_STU_QN_Number ASC';
        $selectedquestion=StudentQuestionsTrial::model()->findAll($criteria);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Type IN (6,7)';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_TR_STU_QN_Number ASC';
        $papercount=StudentQuestionsTrial::model()->count($criteria);
        $pagination='';
        foreach($selectedquestion AS $key=>$testquestion):
            $count=$key+1;
            $class='';
            if($testquestion->TM_TR_STU_QN_Question_Id==$question)
            {
                $questioncount=$count;
            }
            if($testquestion->TM_TR_STU_QN_Type=='6' || $testquestion->TM_TR_STU_QN_Type=='7')
            {
                $pagination.='<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">'.$count.'</a></li>';
            }
            else
            {
                $title='';
                $class='';
                if($testquestion->TM_TR_STU_QN_Flag=='0')
                {
                    $class=($testquestion->TM_TR_STU_QN_Question_Id==$question?'class="active"':'');
                    $title=($testquestion->TM_TR_STU_QN_Question_Id==$question?'Current Question':'');
                }
                elseif($testquestion->TM_TR_STU_QN_Flag=='1')
                {
                    $class=($testquestion->TM_TR_STU_QN_Question_Id==$question?'class="active"':'class="answered"');
                    $title=($testquestion->TM_TR_STU_QN_Question_Id==$question?'Current Question':'Already Answered');
                }
                elseif($testquestion->TM_TR_STU_QN_Flag=='2')
                {
                    $class=($testquestion->TM_TR_STU_QN_Question_Id==$question?'class="active"':'class="skiped"');
                    $title=($testquestion->TM_TR_STU_QN_Question_Id==$question?'Current Question':'Skipped Question');
                }
                $pagination.='<li '.$class.' ><a data-toggle="tooltip" data-placement="bottom" title="'.$title.'" href="'.($testquestion->TM_TR_STU_QN_Question_Id==$question?'javascript:void(0)':Yii::app()->createUrl('trial/RevisionTestTrial',array('id'=>$id,'questionnum'=>base64_encode('question_'.$testquestion->TM_TR_STU_QN_Number)))).'">'.$count.'</a></li>';
            }
        endforeach;


        return array('pagination'=>$pagination,'questioncount'=>$questioncount,'totalcount'=>$count,'papercount'=>$papercount);
    }
    public function GetChildAnswer($type,$test,$question)
    {

        $questionid=StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id'=>$test,'TM_TR_STU_QN_Student_Id'=>Yii::app()->getSession()->getSessionId(),'TM_TR_STU_QN_Question_Id'=>$question));
        if(count($questionid)>0):
            if($type=='1'):
                if($questionid->TM_TR_STU_QN_Answer_Id!=''):
                    return explode(',',$questionid->TM_TR_STU_QN_Answer_Id);
                else:
                    return array();
                endif;
            else:
                return $questionid->TM_TR_STU_QN_Answer;
            endif;
        else:
            return array();
        endif;
    }
    public function doLatercount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Flag=:flag';
        $criteria->params=array(':test'=>$id,':flag'=>'2');
        $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
        return count($selectedquestion);
    }

    public function actionTestComplete($id)
    {

			$results = StudentQuestionsTrial::model()->findAll(
				array(
					'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Type NOT IN(6,7)',
					"order" => 'TM_TR_STU_QN_Number ASC',
					'params' => array(':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id)
				)
			);
            $totalmarks = 0;
            $earnedmarks = 0;
            $result = '';
            $marks = 0;
            foreach ($results AS $exam):
                $question = Questions::model()->findByPk($exam->TM_TR_STU_QN_Question_Id);
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5'):
                        $displaymark = 0;
                        $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marksdisp AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;

                        endforeach;
                        $marks = 0;
                        if ($question->TM_QN_Type_Id != '4'):
                            if ($exam->TM_TR_STU_QN_Answer_Id != ''):

                                $studentanswer = explode(',', $exam->TM_TR_STU_QN_Answer_Id);
                                foreach ($question->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++) {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($exam->TM_TR_STU_QN_Answer != ''):
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_TR_STU_QN_Answer . '</div></li></ul>';
                                foreach ($question->answers AS $ans):
                                    $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer = trim(strtolower($exam->TM_TR_STU_QN_Answer));
                                    for ($i = 0; $i < count($answeroption); $i++) {
                                        $option = trim($answeroption[$i]);
                                        if (strcmp($useranswer, $option) == 0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($question->TM_QN_Parent_Id == 0):
                            $result .= '<tr><td><div class="col-md-4">';
                            if ($marks == '0'):
                                $exam->TM_TR_STU_QN_Redo_Flag = '1';
                                $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';

                            elseif ($marks < $displaymark & $marks != '0'):
                                $exam->TM_TR_STU_QN_Redo_Flag = '1';
                                $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_TR_STU_QN_Number . '</span>';
                            else:
                                $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';
                            endif;

                            $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_TR_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? ' fa fa-flag sty' : ' fa fa-flag stynone') . ' newtest" data-toggle="tooltip" data-placement="left" title="' . ($marks != $displaymark ? '  Unflag question.' : '  Flag question.') . '"></span></a>      Mark: ' . $marks . '/' . $displaymark . '</div>
                            <div class="col-md-3 pull-right">
                            <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                            //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                            $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                            $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $result .= '<div class="col-md-12 Itemtest clickable-row showalert' . $question->TM_QN_Id . '" >

								<div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2 mailadd" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                              </div>
                                 <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id=""  hidden >Your message has been sent to admin
                                </div>  </div>
                             </td></tr>';

                            //glyphicon glyphicon-star sty
                        endif;

                        $exam->TM_TR_STU_QN_Mark = $marks;
                        $exam->save(false);
                        $earnedmarks = $earnedmarks + $marks;

                    else:
                        $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $displaymark = 0;
                        foreach ($marksQuestions AS $key => $formarks):

                            $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                            foreach ($gettingmarks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;



                        endforeach;

                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $count = 1;
                        $childresult = '';
                        $childearnedmarks = 0;
                        foreach ($questions AS $childquestion):
                            $childresults = StudentQuestionsTrial::model()->find(
                                array(
                                    'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Question_Id=:questionid',
                                    'params' => array(':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                                )
                            );
                            $marks = 0;
                            if ($childquestion->TM_QN_Type_Id != '4'):
                                if ($childresults->TM_TR_STU_QN_Answer_Id != ''):

                                    $studentanswer = explode(',', $childresults->TM_TR_STU_QN_Answer_Id);
                                    foreach ($childquestion->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    if (count($studentanswer) > 0):
                                        $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                        for ($i = 0; $i < count($studentanswer); $i++) {
                                            $answer = Answers::model()->findByPk($studentanswer[$i]);
                                            $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                        }
                                        $correctanswer .= '</ul>';
                                    endif;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                                endif;
                            else:
                                if ($childresults->TM_TR_STU_QN_Answer != ''):
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_TR_STU_QN_Answer . '</li></ul>';
                                    foreach ($childquestion->answers AS $ans):
                                        $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                        $useranswer = trim(strtolower($childresults->TM_TR_STU_QN_Answer));
                                        for ($i = 0; $i < count($answeroption); $i++) {
                                            $option = trim($answeroption[$i]);
                                            if (strcmp($useranswer, $option) == 0):
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            endif;
                                        }
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                                endif;

                            endif;
                            if ($childquestion->TM_QN_Parent_Id != 0):
                                $childresult .= '

                            <div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($childquestion->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                                $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                                $childresult .= '';
                                //glyphicon glyphicon-star sty
                            endif;


                            $earnedmarks = $earnedmarks + $marks;
                            $childearnedmarks = $childearnedmarks + $marks;
                            $count++;
                        endforeach;
                        $marks = $childearnedmarks;
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_TR_STU_QN_Redo_Flag = '1';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';
                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_TR_STU_QN_Redo_Flag = '1';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_TR_STU_QN_Number . '</span>';
                        else:
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';
                        endif;

                        $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_TR_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? ' fa fa-flag sty' : ' fa fa-flag stynone') . ' newtest" data-toggle="tooltip" data-placement="left" title="' . ($marks != $displaymark ? '  Unflag question.' : '  Flag question.') . '"></span></a>    Mark: ' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right">
                            <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= $childresult . '</td></tr>';
                        $result .= '<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div>

                            </div></div>
</td></tr>';
                        $exam->TM_TR_STU_QN_Mark = $marks;
                        $exam->save(false);
                    endif;


                endif;

            endforeach;
		$percentage = ($earnedmarks / $totalmarks) * 100;
        $test = StudentTestTrial::model()->findByPk($id);
        $test->TM_TR_STU_TT_Mark = $earnedmarks;
        $test->TM_TR_STU_TT_TotalMarks = $totalmarks;
        $test->TM_TR_STU_TT_Percentage = round($percentage, 1);
        $test->save(false);
        $this->render('showresult', array('test' => $test, 'testresult' => $result));       
    }
    
    public function actionGetpdf($id)
    {
        $paperquestions = $this->GetPaperQuestions($id);
        $totalquestions=count($paperquestions);
        $nak = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->getSession()->getSessionId() . "'"))->TM_STU_First_Name;
        $html = "<table cellpadding='5' border='0' width='100%'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>PAPER QUESTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";



        foreach ($paperquestions AS $key => $paperquestion):

            $question = Questions::model()->findByPk($paperquestion->TM_TR_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):

                if ($question->TM_QN_Type_Id != '7'):
                    $displaymark = 0;
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marks AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $paperquestion->TM_TR_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%'>Marks: ".$question->TM_QN_Totalmarks;
                    $html .= "</td><td align='right'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";


                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;



                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml="";
                    foreach ($questions AS $childquestion):
                        $displaymark=$displaymark+$childquestion->TM_QN_Totalmarks;
                        $childhtml .= "<tr><td width='5%'>Part." . $countpart;
                        $childhtml .= "</td><td colspan='2' width='95%'>";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;

                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $paperquestion->TM_TR_STU_QN_Number;
                    $html .= "</b></td><td width='15%'>Marks: ".$displaymark;
                    $html .= "</td><td  align='right'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";

                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .=$childhtml;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;
            endif;
        endforeach;
        $html .= "</table>";
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->getSession()->getSessionId() . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->getSession()->getSessionId() . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->getSession()->getSessionId())->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; font-style: italic;"><tr>
<td width="33%"><span style="font-weight: lighter; font-style: italic;">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right;font-weight: lighter; ">' . $firstname . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; font-style: italic;"></span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right;font-weight: lighter; ">' . $username . '</td>
</tr></table>';


        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText(Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->getSession()->getSessionId() . "'"))->TM_STU_First_Name);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; font-style: italic;"><tr>
<td width="100%" align="center"><span style="font-weight: lighter; font-style: italic;" align="center">Disclaimer:This is the property ofTabbie Me Educational Solutions PVT Ltd.</span></td>
</tr></table>
');


        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf', 'D');

    }
    public function actionGetSolution()
    {
        if (isset($_POST['testid'])):
            $results = StudentQuestionsTrial::model()->findAll(
                array(
                    'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test',
                    "order" => 'TM_TR_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->getSession()->getSessionId(), ':test' => $_POST['testid'])
                )
            );
            $resulthtml = '';
            foreach ($results AS $result):
                $question = Questions::model()->findByPk($result->TM_TR_STU_QN_Question_Id);
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '6' && $question->TM_QN_Type_Id != '7'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span> <b>Question ' . $result->TM_TR_STU_QN_Number . '</b></span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                    </div><div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                            $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                              <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>

                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '6'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                        <span> <b>Question ' . $result->TM_TR_STU_QN_Number . '</b></span>
                        <span class="pull-right">Marks: ' . $question->TM_QN_Totalmarks . '</span>
                        </div>
                        <div class="col-md-3 pull-right ">
                       <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                        <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                        </div><div class="col-md-12 padnone">
                        <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                            $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                                  <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                               </div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                               </div>
                               </div>
                                <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                    </div></div>

                            </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $displaymark = $displaymark + $chilquestions->TM_QN_Totalmarks;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question ' . $result->TM_TR_STU_QN_Number . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                            $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '5'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $displaymark = 0;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question ' . $result->TM_TR_STU_QN_Number . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                            $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';

                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function GetPaperCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Type IN (:type) AND TM_TR_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id,':type'=>'6,7');
        $selectedquestion=StudentQuestionsTrial::model()->find($criteria);
        return count($selectedquestion);
    }
    public function actionShowpaper($id)

    {

        $connection = CActiveRecord::getDbConnection();
        if(isset($_POST['coomplete']))
        {
            $nonpaperquestions=$this->GetNonPaperQuestions($id);
            $countnon=count($nonpaperquestions);

            if($countnon!=0): {
                $test = StudentTestTrial::model()->findByPk($id);
                $test->TM_TR_STU_TT_Status = '1';
                $test->save(false);
                $this->redirect(array('TestComplete', 'id' => $id));
            }
            else: {
                $test = StudentTestTrial::model()->findByPk($id);
                $test->TM_TR_STU_TT_Status = '1';
                $test->save(false);
                $studenttests = StudentTestTrial::model()->findAll(array('condition' => "TM_TR_STU_TT_Student_Id='" . Yii::app()->getSession()->getSessionId() . "' AND TM_TR_STU_TT_Status!='1'"));
                $this->redirect(array('home'));
            }

            endif;
        }

        $paperquestions=$this->GetPaperQuestions($id);


        $this->render('showpaperquestion',array('id'=>$id,'paperquestions'=>$paperquestions));

    }
    public function GetPaperQuestions($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Type IN (6,7) AND TM_TR_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_TR_STU_QN_Number ASC';
        $selectedquestion=StudentQuestionsTrial::model()->findAll($criteria);
        return $selectedquestion;
    }
    public function GetNonPaperQuestions($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Type NOT IN (6,7) AND TM_TR_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_TR_STU_QN_Number ASC';
        $nonpaperquestions=StudentQuestionsTrial::model()->findAll($criteria);
        return $nonpaperquestions;
    }
    public function ResultMessage($percentage)
    {
        $hundred=array('Congratulations. All hail the Math Master! ','Wow ! Time to Celebrate..','High - five !! Now, thats what I call a score !');
        $ninety=array('Excellent score ! Well done.','Excellent.. Next time its 100% !','Excellent Score .. Keep it up !');
        $seventyfive=array('Well done, but the 90\'s suit you better !','Not a bad score.. Well done, you !','Keep it up ! Keep it better !');
        $sixty=array('Decent effort, But you can do better !',' Well done you ! But you can do better !','A decent effort. Aim for higher !');
        $fifty=array('Not bad, but brush up a little more..','Do revise and try again !','Practice Makes Perfect. Try again !');
        $belowforty=array('Sorry.. do brush up and try again ! ','Hmm.. do we need some more practice ?','Practice Makes Perfect. Try again !');
        if($percentage=='100'):
            $rand=array_rand($hundred);
            return $hundred[$rand];
        elseif($percentage<=99 & $percentage>=90):
            $rand=array_rand($ninety);
            return $ninety[$rand];
        elseif($percentage<=89 & $percentage>=75):
            $rand=array_rand($seventyfive);
            return $seventyfive[$rand];
        elseif($percentage<=74 & $percentage>=60):
            $rand=array_rand($sixty);
            return $sixty[$rand];
        elseif($percentage<=59 & $percentage>=40):
            $rand=array_rand($fifty);
            return $fifty[$rand];
        elseif($percentage<40):
            $rand=array_rand($belowforty);
            return $belowforty[$rand];
        endif;
        //elseif():
    }
    public function actionHistory()
    {


        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_TR_STU_TT_Student_Id=:id AND TM_TR_STU_TT_Status=1');
        $criteria->params=array(':id'=>Yii::app()->getSession()->getSessionId());
        $totaltests = StudentTestTrial::model()->findAll($criteria);

        $this->render('viewhistory',array('totaltests'=>$totaltests));
    }
    public function GetRedoCount($test)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7) AND TM_TR_STU_QN_Redo_Flag=1';
        $criteria->params = array(':test' => $test,':student'=>Yii::app()->getSession()->getSessionId());
        $redoquestions = StudentQuestionsTrial::model()->count($criteria);
        return $redoquestions;
    }
    public function GetChapterTotalTrial($chapter)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_QN_Topic_Id=:chapter AND TM_QN_Status=0 AND TM_QN_Parent_Id=0';
        $criteria->params=array(':chapter'=>$chapter);
        $questions=Questions::model()->with(array('exams'=>array('select'=>false,'joinType'=>'INNER JOIN','condition'=>'exams.TM_QE_Exam_Id=5')))->count($criteria);
        return $questions;
    }
    public function actionTrialRedo($id)
    {

        $mode = ($_GET['redo']==''?$_POST['redo']:$_GET['redo']);
        $test=StudentTestTrial::model()->findByPk($id);
        if($test->TM_TR_STU_TT_Status=='1'):
            $test->TM_TR_STU_TT_Status='2';
            $test->TM_TR_STU_TT_Redo_Action=$mode;
            if ($mode == 'all'):
                $redoquestions=StudentQuestionsTrial::model()->findAll(array('condition'=>'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test','params'=>array(':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id)));
            else:
                $redoquestions=StudentQuestionsTrial::model()->findAll(array('condition'=>'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Redo_Flag=1','params'=>array(':student'=>Yii::app()->getSession()->getSessionId(),':test'=>$id)));
            endif;
            foreach($redoquestions AS $rdq):
                $question=Questions::model()->findByPk($rdq->TM_TR_STU_QN_Question_Id);
                if($question->TM_QN_Type_Id=='5'):
                    $childquestion=StudentQuestionsTrial::model()->findAll(array('condition'=>'TM_TR_STU_QN_Parent_Id=:parent','params'=>array(':parent'=>$rdq->TM_TR_STU_QN_Question_Id)));
                    foreach($childquestion AS $child):
                        $child->TM_TR_STU_QN_Answer_Id='';
                        $child->TM_TR_STU_QN_Answer='';
                        $child->save(false);
                    endforeach;
                endif;
                $rdq->TM_TR_STU_QN_Answer_Id='';
                $rdq->TM_TR_STU_QN_Answer='';
                $rdq->TM_TR_STU_QN_Mark='';
                $rdq->TM_TR_STU_QN_Flag='0';
                $rdq->TM_TR_STU_QN_Redo_Flag='1';
                $rdq->TM_TR_STU_QN_RedoStatus='0';
                $rdq->save(false);
            endforeach;
            $test->save(false);
        endif;
        $condition='';

        if (isset($_POST['action']['GoNext'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                            array('TM_TR_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_TR_STU_QN_Test_Id' => $id, 'TM_TR_STU_QN_Student_Id' => Yii::app()->getSession()->getSessionId())
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestionsTrial();
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_TR_STU_QN_Student_Id = Yii::app()->getSession()->getSessionId();
                        $selectedquestion->TM_TR_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $condition=array(
                'condition'=>'TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7) AND  TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Redo_Flag=1 AND TM_TR_STU_QN_RedoStatus=0 AND TM_TR_STU_QN_Number>:tableid',
                'limit'=>1,
                'order'=>'TM_TR_STU_QN_Number ASC, TM_TR_STU_QN_RedoStatus ASC',
                'params'=>array(':test'=>$id,':student'=>Yii::app()->getSession()->getSessionId(),':tableid'=>$_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['GetPrevios'])){
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                            array('TM_TR_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_TR_STU_QN_Test_Id' => $id, 'TM_TR_STU_QN_Student_Id' => Yii::app()->getSession()->getSessionId())
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestionsTrial();
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_TR_STU_QN_Student_Id = Yii::app()->getSession()->getSessionId();
                        $selectedquestion->TM_TR_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $condition=array(
                'condition'=>'TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7) AND TM_TR_STU_QN_Redo_Flag=1 AND TM_TR_STU_QN_Number <:tableid AND TM_TR_STU_QN_Parent_Id=0',
                'limit'=>1,
                'order'=>'TM_TR_STU_QN_Number DESC',
                'params'=>array(':test'=>$id,':student'=>Yii::app()->getSession()->getSessionId(),':tableid'=>$_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['DoLater'])){
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                            array('TM_TR_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_TR_STU_QN_Test_Id' => $id, 'TM_TR_STU_QN_Student_Id' => Yii::app()->getSession()->getSessionId())
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestionsTrial();
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_TR_STU_QN_Student_Id = Yii::app()->getSession()->getSessionId();
                        $selectedquestion->TM_TR_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                $selectedquestion->TM_TR_STU_QN_RedoStatus = '2';
                $selectedquestion->save(false);
            endif;
            $condition=array(
                'condition'=>'TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7) AND  TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Redo_Flag=1 AND TM_TR_STU_QN_Number>:tableid',
                'limit'=>1,
                'order'=>'TM_TR_STU_QN_Number ASC, TM_TR_STU_QN_RedoStatus ASC',
                'params'=>array(':test'=>$id,':student'=>Yii::app()->getSession()->getSessionId(),':tableid'=>$_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['Complete'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                    $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                    $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestionsTrial::model()->findByAttributes(
                            array('TM_TR_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_TR_STU_QN_Test_Id' => $id, 'TM_TR_STU_QN_Student_Id' => Yii::app()->getSession()->getSessionId())
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestionsTrial();
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_TR_STU_QN_Student_Id = Yii::app()->getSession()->getSessionId();
                        $selectedquestion->TM_TR_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_TR_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_TR_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_TR_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_TR_STU_QN_Question_Id=:question AND TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id);
                $selectedquestion = StudentQuestionsTrial::model()->find($criteria);
                $selectedquestion->TM_TR_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $test = StudentTestTrial::model()->findByPk($id);
            $test->TM_TR_STU_TT_Status = '1';
            $test->save(false);
            $this->redirect(array('RedoComplete', 'id' => $id,'mode'=>$mode));
        }
        if (isset($_GET['questionnum'])&& $condition==''):
            $questionid = StudentQuestionsTrial::model()->findByAttributes(array('TM_TR_STU_QN_Test_Id' => $id),
                array(
                    'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Type NOT IN (6,7)  AND TM_TR_STU_QN_Redo_Flag=1 AND TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Number=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                    'limit' => 1,
                    'order' => 'TM_TR_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->getSession()->getSessionId())
                )
            );
        else:

            if($condition==''):
                $condition=array(
                    'condition' => 'TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Student_Id=:student  AND TM_TR_STU_QN_Type NOT IN (6,7)  AND TM_TR_STU_QN_Redo_Flag=1 AND TM_TR_STU_QN_RedoStatus=0 AND TM_TR_STU_QN_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_TR_STU_QN_Number ASC',
                    'params' => array(':test'=>$id,':student' => Yii::app()->getSession()->getSessionId())
                );

            endif;

            $questionid = StudentQuestionsTrial::model()->find($condition);

        endif;
        if (count($questionid)):
            $question = Questions::model()->findByPk($questionid->TM_TR_STU_QN_Question_Id);

            $this->renderPartial('showquestionredo', array('testid' => $id, 'question' => $question, 'questionid' => $questionid, 'mode' => $mode));
        else:
            //$this->redirect(array('revision'));
        endif;

    }
    public function GetRedoTestQuestions($id, $question, $mode)
    {
        if ($mode == 'flagged'):
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0 AND TM_TR_STU_QN_Redo_Flag=1 AND TM_TR_STU_QN_Type NOT IN (6,7)';
            $criteria->params = array(':test' => $id);
            $criteria->order = 'TM_TR_STU_QN_Number ASC';
            $selectedquestion = StudentQuestionsTrial::model()->findAll($criteria);
            $pagination = '';
            foreach ($selectedquestion AS $key => $testquestion):
                $count = $key + 1;
                $class = '';
                if ($testquestion->TM_TR_STU_QN_Question_Id == $question) {
                    $questioncount = $count;
                }
                if ($testquestion->TM_TR_STU_QN_Type == '6' || $testquestion->TM_TR_STU_QN_Type == '7') {
                    $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
                }
                else
                {
                    $title = '';
                    $class = '';
                    if ($testquestion->TM_TR_STU_QN_RedoStatus == '0') {
                        $class = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'class="active"' : '');
                        $title = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'Current Question' : '');
                    }
                    elseif ($testquestion->TM_TR_STU_QN_RedoStatus == '1')
                    {
                        $class = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'class="active"' : 'class="answered"');
                        $title = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'Current Question' : 'Already Answered');
                    }
                    elseif ($testquestion->TM_TR_STU_QN_RedoStatus == '2')
                    {
                        $class = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                        $title = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                    }
                    $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('trial/trialredo', array('id' => $id,'redo'=>$mode, 'questionnum' => base64_encode('question_' . $testquestion->TM_TR_STU_QN_Number)))) . '">' . $count . '</a></li>';
                }


            endforeach;


            return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count);


        else:
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Parent_Id=0  AND TM_TR_STU_QN_Type NOT IN (6,7)';
            $criteria->params = array(':test' => $id);
            $criteria->order = 'TM_TR_STU_QN_Number ASC';
            $selectedquestion = StudentQuestionsTrial::model()->findAll($criteria);
            $pagination = '';
            foreach ($selectedquestion AS $key => $testquestion):
                $count = $key + 1;
                $class = '';
                if ($testquestion->TM_TR_STU_QN_Question_Id == $question) {
                    $questioncount = $count;
                }
                if ($testquestion->TM_TR_STU_QN_Type == '6' || $testquestion->TM_TR_STU_QN_Type == '7') {
                    $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
                }
                else
                {
                    $title = '';
                    $class = '';
                    if ($testquestion->TM_TR_STU_QN_RedoStatus == '0') {
                        $class = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'class="active"' : '');
                        $title = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'Current Question' : '');
                    }
                    elseif ($testquestion->TM_TR_STU_QN_RedoStatus == '1')
                    {
                        $class = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'class="active"' : 'class="answered"');
                        $title = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'Current Question' : 'Already Answered');
                    }
                    elseif ($testquestion->TM_TR_STU_QN_RedoStatus == '2')
                    {
                        $class = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                        $title = ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                    }
                    $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_TR_STU_QN_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('trial/trialredo', array('id' => $id,'redo'=>$mode,  'questionnum' => base64_encode('question_' . $testquestion->TM_TR_STU_QN_Number)))) . '">' . $count . '</a></li>';
                }
            endforeach;
            return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count);

        endif;
    }
    public function actionRedoComplete($id)
    {
        $results = StudentQuestionsTrial::model()->findAll(
            array(
                'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Type NOT IN(6,7) AND TM_TR_STU_QN_Redo_Flag=1',
                "order" => 'TM_TR_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id)
            )
        );

        $totalmarks = 0;
        $earnedmarks = 0;
        $result = '';
        $resultsolution = '';
        $marks=0;
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_TR_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == 0):
                if ($question->TM_QN_Type_Id != '5'):
                    $displaymark = 0;
                    $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marksdisp AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;

                    endforeach;
                    $marks = 0;
                    if ($question->TM_QN_Type_Id != '4' & $exam->TM_TR_STU_QN_Flag != '2'):
                        if ($exam->TM_TR_STU_QN_Answer_Id != ''):

                            $studentanswer = explode(',', $exam->TM_TR_STU_QN_Answer_Id);
                            foreach ($question->answers AS $ans):
                                if ($ans->TM_AR_Correct == '1'):
                                    if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    }
                                endif;
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh>';
                                for ($i = 0; $i < count($studentanswer); $i++)
                                {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);
                                    $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($exam->TM_TR_STU_QN_Answer != ''):
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_TR_STU_QN_Answer . '</div></li></ul>';
                            foreach ($question->answers AS $ans):
                                $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                $useranswer=  trim(strtolower($exam->TM_TR_STU_QN_Answer));
                                for($i=0;$i<count($answeroption);$i++)
                                {
                                    $option= trim($answeroption[$i]);
                                    if(strcmp($useranswer,$option)==0):
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    endif;
                                }
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;

                    endif;
                    if ($question->TM_QN_Parent_Id == 0):
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_TR_STU_QN_Redo_Flag = '1';
                            $exam->TM_TR_STU_QN_Flag = '0';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';

                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_TR_STU_QN_Redo_Flag = '1';
                            $exam->TM_TR_STU_QN_Flag = '0';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_TR_STU_QN_Number . '</span>';
                        else:
                            $exam->TM_TR_STU_QN_Redo_Flag = '0';
                            $exam->TM_TR_STU_QN_Flag = '1';
                            $exam->TM_TR_STU_QN_RedoStatus = '0';
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';
                        endif;

                        $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_TR_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? 'fa fa-flag sty' : 'fa fa-flag stynone') . ' newtest"  data-toggle="tooltip" data-placement="left" title=" Flag question." ></span></a>      Mark: ' . $marks . '/' . $displaymark . '</div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-8"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                        $result .= '<div class="col-md-12 Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2 mailadd" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                 <div class="col-md-12 mailSendCompletetest' . $question->TM_QN_Id . '" id="" hidden >Your Message Has Been Sent To Admin
                                </div>
                            </div></div>
                             </td></tr>';

                        //glyphicon glyphicon-star sty
                    endif;

                    $exam->TM_TR_STU_QN_Mark = $marks;
                    $exam->TM_TR_STU_QN_RedoStatus = 0;
                    $exam->save(false);
                    $earnedmarks = $earnedmarks + $marks;

                else:
                    $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                    $displaymark = 0;
                    foreach ($marksQuestions AS $key => $formarks):

                        $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($gettingmarks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                    endforeach;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childresult='';
                    $childearnedmarks=0;
                    $childresulthtml='';
                    foreach ($questions AS $childquestion):
                        $childresults = StudentQuestionsTrial::model()->find(
                            array(
                                'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test AND TM_TR_STU_QN_Question_Id=:questionid',
                                'params' => array(':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                            )
                        );
                        $marks = 0;
                        if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_TR_STU_QN_Flag != '2'):
                            if ($childresults->TM_TR_STU_QN_Answer_Id != ''):

                                $studentanswer = explode(',', $childresults->TM_TR_STU_QN_Answer_Id);
                                foreach ($childquestion->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++)
                                    {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($childresults->TM_TR_STU_QN_Answer != ''):
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_TR_STU_QN_Answer . '</li></ul>';
                                foreach ($childquestion->answers AS $ans):
                                    $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer=  trim(strtolower($childresults->TM_TR_STU_QN_Answer));
                                    for($i=0;$i<count($answeroption);$i++)
                                    {
                                        $option= trim($answeroption[$i]);
                                        if(strcmp($useranswer,$option)==0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($childquestion->TM_QN_Parent_Id != 0):
                            $childresult .= '

                            <div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                            $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $childresult .= '';
                            //glyphicon glyphicon-star sty
                        endif;

                        $childresulthtml .='<div class="col-md-12 ">Part :'.$count.'</div>';
                        $childresulthtml .='<div class="col-md-12 padnone">';
                        $childresulthtml .= '<div class="col-md-' . ($childquestion->TM_QN_Image != '' ? '10' : '12') . '">' . $childquestion->TM_QN_Question . '</div>' . ($childquestion->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') ;
                        $childresulthtml .='</div>';
                        $earnedmarks = $earnedmarks + $marks;
                        $childearnedmarks = $childearnedmarks + $marks;
                        $count++;
                    endforeach;


                    $marks=$childearnedmarks;
                    $result .= '<tr><td><div class="col-md-4">';
                    if ($marks == '0'):
                        $exam->TM_TR_STU_QN_Redo_Flag = '1';
                        $exam->TM_TR_STU_QN_Flag = '0';
                        $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';
                    elseif ($marks < $displaymark & $marks != '0'):
                        $exam->TM_TR_STU_QN_Redo_Flag = '1';
                        $exam->TM_TR_STU_QN_Flag = '0';
                        $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_TR_STU_QN_Number . '</span>';
                    else:
                        $exam->TM_TR_STU_QN_Redo_Flag = '0';
                        $exam->TM_TR_STU_QN_Flag = '1';
                        $exam->TM_TR_STU_QN_RedoStatus = '0';
                        $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_TR_STU_QN_Number . '</span>';
                    endif;

                    $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_TR_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? 'fa fa-flag sty' : 'fa fa-flag stynone') . ' newtest"  data-toggle="tooltip" data-placement="left" title=" Flag question." ></span></a>    Mark: ' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                    //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                    $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-8"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                    $result .= $childresult.'</td></tr>';
                    $result .='<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-12 mailSendCompletetest' . $question->TM_QN_Id . '" id="" hidden >Your Message Has Been Sent To Admin
                                </div>

                            </div></div>
                            </td></tr>';
                    $exam->TM_TR_STU_QN_Mark = $marks;
                    $exam->save(false);
                endif;


            endif;

        endforeach;

        $test = StudentTest::model()->findByPk($id);
        $test->TM_STU_TT_Status='1';
        $test->save(false);

        $this->render('showresultredo', array('test' => $test, 'testresult' => $result,'solution'=>$resultsolution));
    }
    public function actionGetpdfAnswer($id)
    {
        $results = StudentQuestionsTrial::model()->findAll(
            array(
                'condition' => 'TM_TR_STU_QN_Student_Id=:student AND TM_TR_STU_QN_Test_Id=:test ',
                "order" => 'TM_TR_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->getSession()->getSessionId(), ':test' => $id)
            )
        );
        $totalquestions = count($results);
        $html = "<table cellpadding='5' border='0' width='100%'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>REVISION SOLUTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";


        foreach ($results AS $key => $result):
            $count = $key + 1;
            $question = Questions::model()->findByPk($result->TM_TR_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Type_Id == '1' || $question->TM_QN_Type_Id == '2' || $question->TM_QN_Type_Id == '3' || $question->TM_QN_Type_Id == '4'):
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_TR_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='80%' align='right'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";


                    $html .= "<tr><td colspan='3' >";


                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '6'):
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_TR_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $question->TM_QN_Totalmarks;
                    $html .= "</td><td width='80%' align='right'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";


                    $html .= "<tr><td colspan='3' >";


                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '5'):

                    $displaymark = 0;


                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childhtml = '';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;

                        $childhtml .= "<tr><td colspan='3'><b>";
                        $childhtml .= "Part " . $count;
                        $childhtml .= "</b></td></tr><tr><td colspan='3'>";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;


                        $count++;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_TR_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='80%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td width='5%'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):

                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childhtml = '';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));


                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;


                        $childhtml .= "<tr><td colspan='3'><b>";
                        $childhtml .= "Part " . $count;
                        $childhtml .= "</b></td></tr><tr><td colspan='3'>";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;


                        $count++;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_TR_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='80%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td width='5%'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;


            endif;
        endforeach;
        $html .= "</table>";

        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf;">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $firstname . ' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $username . '</td>
</tr></table>';


        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);

        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style="font-weight: lighter; color: #afafaf;" align="center">Disclaimer : This is the property of Tabbie Me Educational Solutions PVT Ltd.</span></td>

</tr></table>
');

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf', 'D');


    }
}