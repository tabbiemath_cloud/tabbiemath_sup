<?php

class QuestionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
			'postOnly + Deleteimage', // we only allow deletion via POST request
			//'postOnly + copy', // we only allow copy via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),*/
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','admin','delete','create','update','GetTopicList','copy','importcsv','changeStatus','deleteimage','uploadimage','deleteimagetemp','uploadimagetemp','GetTypeForm','Deletechildquestion','exportquestions','importtosyllabus','exporttosyllabus','exportsyllabuscsv','Setoptions','FindAvailableMarks','updatemultiparmark','Getskill','Getqtype','Standardlist'),
				'users'=>UserModule::getQuestionAdmins(),
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('ChangeStatus','Getskill','Getqtype'),
                'users'=>array('@'),
                'expression'=>'UserModule::isTeacherAdmin()'
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
    
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$availability=Questions::model()->getExams($id);
        $this->render('view',array(
			'model'=>$this->loadModel($id),
			'availability'=>$availability,
		));
	}
    public function actionCopy($id)
    {
        if(isset($id)):
            $question=Questions::model()->findByPk($id);
            $type=$question->TM_QN_Type_Id;
            $newquestion= new Questions();
            $newquestion->attributes=$question->attributes;
            $newquestion->TM_QN_Id=NULL;
            $newquestion->TM_QN_Question=$question->TM_QN_Question;
            $newquestion->TM_QN_Option_Count=$question->TM_QN_Option_Count;
            $newquestion->TM_QN_CreatedBy=Yii::app()->user->id;
            $newquestion->TM_QN_CreatedOn=date('Y-m-d');
            $newquestion->TM_QN_UpdatedBy='0';
            $newquestion->TM_QN_UpdatedOn='0000-00-00';
            switch($type)
            {
                case '5':
                    $questionparts=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                    if($newquestion->save(false)):
                        $qstexams=Questions::model()->getExams($question->TM_QN_Id);
                        foreach($qstexams AS $exam):
                            $exams=New QuestionExams();
                            $exams->TM_QE_Exam_Id=$exam['Id'];
                            $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                            $exams->save(false);
                        endforeach;
                        foreach($questionparts AS $parts):
                            //$parts=Questions::model()->findByPk($parts->TM_QN_Question);
                            $newpart= new Questions();
                            $newpart->attributes=$parts->attributes;
                            $newpart->TM_QN_Id=NULL;
                            $newpart->TM_QN_Question=$parts->TM_QN_Question;
                            $newpart->TM_QN_Parent_Id=$newquestion->TM_QN_Id;
                            if($newpart->save(false)):
                                foreach($parts->answers AS $ans):
                                    $answer=Answers::model()->findByPk($ans->TM_AR_Id);
                                    $newanswer=new Answers();
                                    $newanswer->attributes=$answer->attributes;
                                    $newanswer->TM_AR_Id=NULL;
                                    $newanswer->TM_AR_Answer=$answer->TM_AR_Answer;
                                    $newanswer->TM_AR_Question_Id=$newpart->TM_QN_Id;
                                    $newanswer->save(false);
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    break;
                case '6':
                    $newquestion->save(false);
                    break;
                case '7':
                    if($newquestion->save(false)):
                        $questionparts=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach($questionparts AS $parts):
                            $newpart= new Questions();
                            $newpart->attributes=$parts->attributes;
                            $newpart->TM_QN_Id=NULL;
                            $newpart->TM_QN_Question=$parts->TM_QN_Question;
                            $newpart->TM_QN_Parent_Id=$newquestion->TM_QN_Id;
                            $newpart->save(false);
                        endforeach;
                    endif;
                    break;
                default:
                    if($newquestion->save(false)):
                        $qstexams=Questions::model()->getExams($question->TM_QN_Id);
                        foreach($qstexams AS $exam):
                            $exams=New QuestionExams();
                            $exams->TM_QE_Exam_Id=$exam['Id'];
                            $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                            $exams->save(false);
                        endforeach;
                        foreach($question->answers AS $ans):
                            $answer=Answers::model()->findByPk($ans->TM_AR_Id);
                            $newanswer=new Answers();
                            $newanswer->attributes=$answer->attributes;
                            $newanswer->TM_AR_Id=NULL;
                            $newanswer->TM_AR_Answer=$answer->TM_AR_Answer;
                            $newanswer->TM_AR_Question_Id=$newquestion->TM_QN_Id;
                            $newanswer->save(false);
                        endforeach;
                    endif;

            }
            $schools=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_SchoolId)','condition'=>'TM_SPN_Publisher_Id='.$newquestion->TM_QN_Publisher_Id.' AND TM_SPN_StandardId='.$newquestion->TM_QN_Standard_Id));
            foreach($schools AS $school):
                $scquestion=new SchoolQuestions();
                $scquestion->TM_SQ_School_Id=$school->TM_SPN_SchoolId;
                $scquestion->TM_SQ_Question_Type=$newquestion->TM_QN_Type_Id;
                $scquestion->TM_SQ_Question_Id=$newquestion->TM_QN_Id;
                $scquestion->TM_SQ_Standard_Id=$newquestion->TM_QN_Standard_Id;
                $scquestion->TM_SQ_Chapter_Id=$newquestion->TM_QN_Topic_Id;
                $scquestion->TM_SQ_Topic_Id=$newquestion->TM_QN_Section_Id;
                $scquestion->TM_SQ_Syllabus_Id=$newquestion->TM_QN_Syllabus_Id;
                $scquestion->TM_SQ_Publisher_id=$newquestion->TM_QN_Publisher_Id;
                $scquestion->TM_SQ_Type='1';
                $scquestion->TM_SQ_QuestionReff=$newquestion->TM_QN_QuestionReff;
                $scquestion->TM_SQ_Status='0';
                $scquestion->save(false);
            endforeach;             
            $this->redirect(array('update','id'=>$newquestion->TM_QN_Id,'action'=>'copy'));
        endif;
    }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Questions;
        $model->scenario = 'Create';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Questions']))
		{

            //print_r($_POST['Questions']); exit;
            $model->attributes=$_POST['Questions'];
            if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionchoice'];

                $model->TM_QN_Option_Count=$_POST['Questions']['TM_QN_Option_Countchoice'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='3')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTF'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;

            }
            elseif($model->TM_QN_Type_Id=='4')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTEXT'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='5')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionpart'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='6')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionPaperOnly'];
                $model->TM_QN_Totalmarks=$_POST['Papermarks'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='7')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionMPPaperOnly'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            $tempimagesol=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'solution',':section'=>'main')));
            if($tempimagesol->image!=''):
                $model->TM_QN_Solution_Image=$tempimagesol->image;
            endif; 
          
            if(isset($_POST['Questions']['TM_QN_Skill']))
            {
                 $skill_ids=$_POST['Questions']['TM_QN_Skill'];
                 $skill_id= implode(",",$skill_ids);
                 $model->TM_QN_Skill=$skill_id;

            }
            if(isset($_POST['Questions']['TM_QN_Question_type']))
            {
                    $type_ids=$_POST['Questions']['TM_QN_Question_type'];
                    $type_id=implode(",",$type_ids);
                    $model->TM_QN_Question_type=$type_id;
            }
            if(isset($_POST['Questions']['TM_QN_Info']))
            {
                 $model->TM_QN_Info=$_POST['Questions']['TM_QN_Info'];
            }
           
            $model->TM_QN_CreatedBy=Yii::app()->user->id;
            /*$model->TM_QN_UpdatedBy=Yii::app()->user->id;
            $model->TM_QN_UpdatedOn=date('Y-m-d');*/
			if($model->save())
            {
                // $num_correct_ans=0;

                // insert into question_exam table
                for($i=0;$i<count($_POST['Questions']['availability']);$i++)
                {
                    $exams=New QuestionExams();
                    $exams->TM_QE_Exam_Id=$_POST['Questions']['availability'][$i];
                    $exams->TM_QE_Question_Id=$model->TM_QN_Id;
                    $exams->save(false);
                }
                // insert into answers table
                $total=0;
                if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
                {

                    for($i=0;$i<$model->TM_QN_Option_Count;$i++)
                    {

                        $identifier=$i+1;
                        $answer=new Answers();
                        $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                        $answer->TM_AR_Answer=$_POST['answer'][$i];
                        if(isset($_POST['correctanswer'.$identifier]) & $model->TM_QN_Type_Id=='1')
                        {
                            $answer->TM_AR_Correct=$_POST['correctanswer'.$identifier];
                            $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                            
                            $total+=$_POST['marks'.$identifier];
                           
                            

                        }
                        elseif($model->TM_QN_Type_Id=='2' & isset($_POST['correctanswer']))
                        {
                            if($_POST['correctanswer']==$identifier)
                            {
                                $answer->TM_AR_Correct='1';
                                $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                $total+= $_POST['marks'.$identifier];                           }
                            else
                            {
                                $answer->TM_AR_Correct='0';
                                $answer->TM_AR_Marks='0';
                            }

                        }
                        $tempimageanswer=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier)));
                        if($tempimageanswer->image!=''):
                            $answer->TM_AR_Image=$tempimageanswer->image;
                        endif;
                        $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                        $answer->TM_AR_Status='0';
                        $answer->save(false);
                        
                    }
        
                        

                }
                elseif($model->TM_QN_Type_Id=='3')
                {
                    $answer=new Answers();
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='True';
                    if($_POST['correctanswerTF']=='True')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                        $total+=$_POST['marksTF'];

                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                    $answer=new Answers();
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='False';
                    if($_POST['correctanswerTF']=='False')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                        $total+=$_POST['marksTF'];
                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='4')
                {
                    $answer=new Answers();
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer=$_POST['answeroptions'];
                    $answer->TM_AR_Marks=$_POST['marksTEXT'];
                    $total+=$_POST['marksTEXT'];
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='5')
                {
                    for($i=0;$i<$_POST['multipleqstnum'];$i++)
                    {
                        $identifier=$i+1;
                        $question=new Questions();
                        $question->TM_QN_Parent_Id=$model->TM_QN_Id;
                        $question->TM_QN_Publisher_Id=$model->TM_QN_Publisher_Id;
                        $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                        $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                        $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                        $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                        $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                        $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;
                        $question->TM_QN_Type_Id=$_POST['multypartType'][$i];
                        $question->TM_QN_Question=$_POST['multypartQuestions'][$i];
                        $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                        if($tempimagechild->image!=''):
                            $question->TM_QN_Image=$tempimagechild->image;
                        endif;

                        if($_POST['multypartType'][$i]=='1' | $_POST['multypartType'][$i]=='2')
                        {
                            $question->TM_QN_Option_Count=$model->TM_QN_Option_Count;
                        }
                        elseif($_POST['multypartType'][$i]=='4')
                        {
                            $id=$i+1;
                            if(isset($_POST['Editor'.$id])):
                                $question->TM_QN_Editor=$_POST['Editor'.$id];
                            endif;
                        }
                        $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                        $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                        $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                        $question->TM_QN_Status=$model->TM_QN_Status;
                        if($question->save(false))
                        {
                            $multQuesTotalMark = 0;
                            $identifier=$i+1;
                            if($_POST['multypartType'][$i]=='1')
                            {

                                for($j=0;$j<$_POST['multypartOptions'.$identifier];$j++)
                                {
                                    $multiidenti=$j+1;
                                    $answer=new Answers();
                                    $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                    $answer->TM_AR_Answer=$_POST['answer'.$identifier][$j];
                                    if(isset($_POST['correctanswer'.$identifier.$multiidenti]))
                                    {
                                        $answer->TM_AR_Correct=$_POST['correctanswer'.$identifier.$multiidenti];
                                        $answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
                                        $total+=$_POST['marks'.$identifier.$multiidenti];
                                        $multQuesTotalMark += $_POST['marks'.$identifier.$multiidenti];
                                    }
                                    $tempimagechildans=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier.$multiidenti)));
                                    if($tempimagechildans->image!=''):
                                        $answer->TM_AR_Image=$tempimagechildans->image;
                                    endif;


                                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                    $answer->TM_AR_Status='0';
                                    $answer->save(false);
                                }
                            }

                            if($_POST['multypartType'][$i]=='2')
                            {

                                for($j=0;$j<$_POST['multypartOptions'.$identifier];$j++)
                                {
                                    $multiidenti=$j+1;
                                    $answer=new Answers();
                                    $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                    $answer->TM_AR_Answer=$_POST['answer'.$identifier][$j];
                                    if($_POST['correctanswer'.$identifier]==$multiidenti)
                                    {
                                        $answer->TM_AR_Correct='1';
                                        $answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
                                        $total+=$_POST['marks'.$identifier.$multiidenti];
                                        $multQuesTotalMark += $_POST['marks'.$identifier.$multiidenti];
                                    }
                                    $tempimagechildans=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier.$multiidenti)));
                                    if($tempimagechildans->image!=''):
                                        $answer->TM_AR_Image=$tempimagechildans->image;
                                    endif;


                                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                    $answer->TM_AR_Status='0';
                                    $answer->save(false);
                                }
                            }                            
                            elseif($_POST['multypartType'][$i]=='3')
                            {
                                $answer=new Answers();
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='True';
                                if($_POST['correctanswer'.$identifier]=='True')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                     $multQuesTotalMark += $_POST['marks'.$identifier];
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                                $answer=new Answers();
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='False';
                                if($_POST['correctanswer'.$identifier]=='False')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                    $multQuesTotalMark += $_POST['marks'.$identifier];
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                            elseif($_POST['multypartType'][$i]=='4')
                            {
                                $identifier=$i+1;
                                $answer=new Answers();
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer=$_POST['answeroptions'.$identifier];
                                $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                $total+=$_POST['marks'.$identifier];
                                $multQuesTotalMark += $_POST['marks'.$identifier];
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                            $question->TM_QN_Totalmarks = $multQuesTotalMark;
                            $question->save(false);
                        }


                    }
                }
                elseif($model->TM_QN_Type_Id=='6')
                {
                    $total+=$_POST['Papermarks'];

                }
                elseif($model->TM_QN_Type_Id=='7')
                {
                   for($i=0;$i<$_POST['PoMPqstnum'];$i++)
                   {
                       $identifier=$i+1;
                       $question=new Questions();
                       $question->TM_QN_Parent_Id=$model->TM_QN_Id;
                       $question->TM_QN_Publisher_Id=$model->TM_QN_Publisher_Id;
                       $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                       $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                       $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                       $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                       $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                       $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;
                       $question->TM_QN_Type_Id=$model->TM_QN_Type_Id;
                       $question->TM_QN_Question=$_POST['PapermultypartQuestions'][$i];
                       $question->TM_QN_Solutions=$model->TM_QN_Solutions;
                       $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                       $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                       $question->TM_QN_Totalmarks=$_POST['PaperMPmarks'][$i];
                       $total+=$_POST['PaperMPmarks'][$i];
                       $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                       $question->TM_QN_Status=$model->TM_QN_Status;

                       $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                       if($tempimagechild->image!=''):
                           $question->TM_QN_Image=$tempimagechild->image;
                       endif;
                        $question->save(false);
                   }
                }
                
                //update the totalMark into table questions
                Questions::model()->updateByPk($model->TM_QN_Id, array(
                'TM_QN_Totalmarks' => $total
                ));

                $this->Updateavailablemarks();
                
                     //----------------------------------------        
                $schools=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_SchoolId)','condition'=>'TM_SPN_Publisher_Id='.$model->TM_QN_Publisher_Id.' AND TM_SPN_StandardId='.$model->TM_QN_Standard_Id));
                foreach($schools AS $school):
                    $scquestion=new SchoolQuestions();
                    $scquestion->TM_SQ_School_Id=$school->TM_SPN_SchoolId;
                    $scquestion->TM_SQ_Question_Type=$model->TM_QN_Type_Id;
                    $scquestion->TM_SQ_Question_Id=$model->TM_QN_Id;
                    $scquestion->TM_SQ_Standard_Id=$model->TM_QN_Standard_Id;
                    $scquestion->TM_SQ_Chapter_Id=$model->TM_QN_Topic_Id;
                    $scquestion->TM_SQ_Topic_Id=$model->TM_QN_Section_Id;
                    $scquestion->TM_SQ_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                    $scquestion->TM_SQ_Publisher_id=$model->TM_QN_Publisher_Id;
                    $scquestion->TM_SQ_Type='1';
                    $scquestion->TM_SQ_QuestionReff=$model->TM_QN_QuestionReff;
                    $scquestion->TM_SQ_Status='0';
                    $scquestion->save(false);
                endforeach;                
                $criteria = new CDbCriteria;
                $criteria->addCondition('tempid='.$_POST['imagetempid']);
                $tempdelete = Imagetemp::model()->deleteAll($criteria);
                $this->redirect(array('view','id'=>$model->TM_QN_Id));

            }

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
     public function actionImportcsv()
    {
        $importlog=array();
        if(isset($_POST['submit'])):
 
            $total=0;

            if($_POST['Type']=='1' || $_POST['Type']=='2' || $_POST['Type']=='3'):
                $file = fopen($_FILES['importcsv']['tmp_name'], "r");
                $count=0;

                while(! feof($file))
                {
                    $values=fgetcsv($file);
                    if($count!='0' & $values[0]!=''):
                        $availability=explode(',',$values[7]);
                        $statusarray=array(
                                '0' => ('Active'),
                                '1' => ('Inactive'),
                                '2' => ('Review'),
                                '3' => ('Flagged for review'),
                                '4' => ('Reviewed - Correct'),
                                '5' => ('Reviewed - Incorrect'),
                                '6' => ('Image'),
                                '7' => ('editor'),
                                '8' => ('Doubt'),
                            );
                        $status=array_search($values[10], $statusarray);
                        $question= new Questions();
                        $question->TM_QN_Publisher_Id=(Publishers::model()->exists('TM_PR_Name=:value',array(':value'=>$values[0]))?Publishers::model()->findByAttributes(array('TM_PR_Name'=>$values[0]))->TM_PR_Id:'');
                        $question->TM_QN_Syllabus_Id=(Syllabus::model()->exists('TM_SB_Name=:value',array(':value'=>$values[1]))?Syllabus::model()->findByAttributes(array('TM_SB_Name'=>$values[1]))->TM_SB_Id:'');
                        $question->TM_QN_Standard_Id=(Standard::model()->exists('TM_SD_Name=:value',array(':value'=>$values[2]))?Standard::model()->findByAttributes(array('TM_SD_Name'=>$values[2]))->TM_SD_Id:'');
                        $question->TM_QN_Subject_Id=1;
                        $question->TM_QN_Topic_Id=(Chapter::model()->exists('TM_TP_Name=:value',array(':value'=>$values[3]))?Chapter::model()->findByAttributes(array('TM_TP_Name'=>$values[3]))->TM_TP_Id:'');
                        $question->TM_QN_Section_Id=(Topic::model()->exists('TM_SN_Name=:value',array(':value'=>$values[4]))?Topic::model()->findByAttributes(array('TM_SN_Name'=>$values[4],'TM_SN_Topic_Id'=>$question->TM_QN_Topic_Id))->TM_SN_Id:'');
                        $question->TM_QN_Dificulty_Id=(Difficulty::model()->exists('TM_DF_Name=:value',array(':value'=>$values[5]))?Difficulty::model()->findByAttributes(array('TM_DF_Name'=>$values[5]))->TM_DF_Id:'');
                        $question->TM_QN_Type_Id=(Types::model()->exists('TM_TP_Name=:value',array(':value'=>$values[6]))?Types::model()->findByAttributes(array('TM_TP_Name'=>$values[6]))->TM_TP_Id:'');
                        $question->TM_QN_Teacher_Id=(Teachers::model()->exists('TM_TH_Name=:value',array(':value'=>$values[8]))?Teachers::model()->findByAttributes(array('TM_TH_Name'=>$values[8]))->TM_TH_Id:'');
                        $question->TM_QN_Pattern=$values[9];
                        $question->TM_QN_Status=$status;
                        $question->TM_QN_QuestionReff=$values[11];
                        $question->TM_QN_Question=$values[12];
                        $question->TM_QN_Option_Count=$values[13];
                        $question->TM_QN_Solutions=$values[14];
                        $question->TM_QN_CreatedBy=Yii::app()->user->id;
                        $importlog[$count]['status']=$this->GetImportitems($values);
                        if($question->save(false)):                    
                            $schools=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_SchoolId)','condition'=>'TM_SPN_Publisher_Id='.$question->TM_QN_Publisher_Id.' AND TM_SPN_StandardId='.$question->TM_QN_Standard_Id));
                            foreach($schools AS $school):
                                $scquestion=new SchoolQuestions();
                                $scquestion->TM_SQ_School_Id=$school->TM_SPN_SchoolId;
                                $scquestion->TM_SQ_Question_Type=$question->TM_QN_Type_Id;
                                $scquestion->TM_SQ_Question_Id=$question->TM_QN_Id;
                                $scquestion->TM_SQ_Standard_Id=$question->TM_QN_Standard_Id;
                                $scquestion->TM_SQ_Chapter_Id=$question->TM_QN_Topic_Id;
                                $scquestion->TM_SQ_Topic_Id=$question->TM_QN_Section_Id;
                                $scquestion->TM_SQ_Syllabus_Id=$question->TM_QN_Syllabus_Id;
                                $scquestion->TM_SQ_Publisher_id=$question->TM_QN_Publisher_Id;
                                $scquestion->TM_SQ_Type='1';
                                $scquestion->TM_SQ_QuestionReff=$question->TM_QN_QuestionReff;
                                $scquestion->TM_SQ_Status='0';
                                $scquestion->save(false);
                            endforeach;                                                                                                       
                            $importlog[$count]['question']='1';
                            $correctanswer=explode(',',$values[20]);
                            $marks=explode(',',$values[21]);
                            for($j=0;$j<count($availability);$j++)
                            {
                                if(Exams::model()->exists('TM_EX_Name=:value',array(':value'=>$availability[$j]))):
                                    $exams=New QuestionExams();
                                    $exams->TM_QE_Exam_Id=Exams::model()->findByAttributes(array('TM_EX_Name'=>$availability[$j]))->TM_EX_Id;
                                    $exams->TM_QE_Question_Id=$question->TM_QN_Id;
                                    $exams->save(false);
                                else:
                                    $importlog[$count]['status']=$importlog[$count]['status']."Exam '".$availability[$j]."' Not Found";
                                endif;
                            }
                            for($i=0;$i<$values[13];$i++):
                                $countanswer=$i+1;
                                $index=14+$countanswer;
                                $answer=new Answers();
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer=$values[$index];
                                if(in_array($countanswer,$correctanswer))
                                {
                                    $answer->TM_AR_Correct='1';
                                }
                                if($marks[$i]!='0')
                                {
                                    $answer->TM_AR_Marks=$marks[$i];
                                    $total+=$marks[$i];
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                if($answer->save(false)):
                                    $importlog[$count]['answer']='1';
                                else:
                                    $importlog[$count]['answer']='0';
                                endif;
                            endfor;
                        else:
                            $importlog[$count]['question']='0';
                        endif;
                    endif;
                    $count++;
                }
            elseif($_POST['Type']=='4'):
                $file = fopen($_FILES['importcsv']['tmp_name'], "r");
                $count=0;
                while(! feof($file))
                {
                    $values=fgetcsv($file);
                    if($count!='0' & $values[0]!=''):

                        $availability=explode(',',$values[7]);
                        $statusarray=array(
                            '0' => ('Active'),
                            '1' => ('Inactive'),
                            '2' => ('Review'),
                            '3' => ('Flagged for review'),
                            '4' => ('Reviewed - Correct'),
                            '5' => ('Reviewed - Incorrect'),
                            '6' => ('Image'),
                            '7' => ('editor'),
                            '8' => ('Doubt'),
                        );
                        $status=array_search($values[10], $statusarray);
                        $question= new Questions();
                        $question->TM_QN_Publisher_Id=(Publishers::model()->exists('TM_PR_Name=:value',array(':value'=>$values[0]))?Publishers::model()->findByAttributes(array('TM_PR_Name'=>$values[0]))->TM_PR_Id:'');
                        $question->TM_QN_Syllabus_Id=(Syllabus::model()->exists('TM_SB_Name=:value',array(':value'=>$values[1]))?Syllabus::model()->findByAttributes(array('TM_SB_Name'=>$values[1]))->TM_SB_Id:'');
                        $question->TM_QN_Standard_Id=(Standard::model()->exists('TM_SD_Name=:value',array(':value'=>$values[2]))?Standard::model()->findByAttributes(array('TM_SD_Name'=>$values[2]))->TM_SD_Id:'');
                        $question->TM_QN_Subject_Id=1;
                        $question->TM_QN_Topic_Id=(Chapter::model()->exists('TM_TP_Name=:value',array(':value'=>$values[3]))?Chapter::model()->findByAttributes(array('TM_TP_Name'=>$values[3]))->TM_TP_Id:'');
                        $question->TM_QN_Section_Id=(Topic::model()->exists('TM_SN_Name=:value',array(':value'=>$values[4]))?Topic::model()->findByAttributes(array('TM_SN_Name'=>$values[4]))->TM_SN_Id:'');
                        $question->TM_QN_Dificulty_Id=(Difficulty::model()->exists('TM_DF_Name=:value',array(':value'=>$values[5]))?Difficulty::model()->findByAttributes(array('TM_DF_Name'=>$values[5]))->TM_DF_Id:'');
                        $question->TM_QN_Type_Id=(Types::model()->exists('TM_TP_Name=:value',array(':value'=>$values[6]))?Types::model()->findByAttributes(array('TM_TP_Name'=>$values[6]))->TM_TP_Id:'');
                        $question->TM_QN_Teacher_Id=(Teachers::model()->exists('TM_TH_Name=:value',array(':value'=>$values[8]))?Teachers::model()->findByAttributes(array('TM_TH_Name'=>$values[8]))->TM_TH_Id:'');
                        $question->TM_QN_Pattern=$values[9];
                        $question->TM_QN_Status=$status;
                        $question->TM_QN_QuestionReff=$values[11];
                        $question->TM_QN_Question=$values[12];
                        $question->TM_QN_Editor=$values[15];
                        $question->TM_QN_Solutions=$values[16];
                        $question->TM_QN_CreatedBy=Yii::app()->user->id;
                        $importlog[$count]['status']=$this->GetImportitems($values);
                        if($question->save(false)):
                            $schools=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_SchoolId)','condition'=>'TM_SPN_Publisher_Id='.$question->TM_QN_Publisher_Id.' AND TM_SPN_StandardId='.$question->TM_QN_Standard_Id));
                            foreach($schools AS $school):
                                $scquestion=new SchoolQuestions();
                                $scquestion->TM_SQ_School_Id=$school->TM_SPN_SchoolId;
                                $scquestion->TM_SQ_Question_Type=$question->TM_QN_Type_Id;
                                $scquestion->TM_SQ_Question_Id=$question->TM_QN_Id;
                                $scquestion->TM_SQ_Standard_Id=$question->TM_QN_Standard_Id;
                                $scquestion->TM_SQ_Chapter_Id=$question->TM_QN_Topic_Id;
                                $scquestion->TM_SQ_Topic_Id=$question->TM_QN_Section_Id;
                                $scquestion->TM_SQ_Syllabus_Id=$question->TM_QN_Syllabus_Id;
                                $scquestion->TM_SQ_Publisher_id=$question->TM_QN_Publisher_Id;
                                $scquestion->TM_SQ_Type='1';
                                $scquestion->TM_SQ_QuestionReff=$question->TM_QN_QuestionReff;
                                $scquestion->TM_SQ_Status='0';
                                $scquestion->save(false);
                            endforeach;                         
                            $importlog[$count]['question']='1';
                            for($j=0;$j<count($availability);$j++)
                            {
                                if(Exams::model()->exists('TM_EX_Name=:value',array(':value'=>$availability[$j]))):
                                    $exams=New QuestionExams();
                                    $exams->TM_QE_Exam_Id=Exams::model()->findByAttributes(array('TM_EX_Name'=>$availability[$j]))->TM_EX_Id;
                                    $exams->TM_QE_Question_Id=$question->TM_QN_Id;
                                    $exams->save(false);
                                else:
                                    $importlog[$count]['status']=$importlog[$count]['status']."Exam '".$availability[$j]."' Not Found";
                                endif;
                            }
                            $answer=new Answers();
                            $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                            $answer->TM_AR_Answer=$values[13];
                            $answer->TM_AR_Marks=$values[14];
                            $total+=$values[14];
                            $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                            $answer->TM_AR_Status='0';
                            if($answer->save(false)):
                                $importlog[$count]['answer']='1';
                            else:
                                $importlog[$count]['answer']='0';
                            endif;
                        else:
                            $importlog[$count]['question']='0';
                        endif;
                    endif;
                    $count++;
                }
            elseif($_POST['Type']=='5'):
            elseif($_POST['Type']=='6'):
                $file = fopen($_FILES['importcsv']['tmp_name'], "r");
                $count=0;
                while(! feof($file))
                {
                    $values=fgetcsv($file);
                    if($count!='0' & $values[0]!=''):

                        $availability=explode(',',$values[7]);
                        $statusarray=array(
                            '0' => ('Active'),
                            '1' => ('Inactive'),
                            '2' => ('Review'),
                            '3' => ('Flagged for review'),
                            '4' => ('Reviewed - Correct'),
                            '5' => ('Reviewed - Incorrect'),
                            '6' => ('Image'),
                            '7' => ('editor'),
                            '8' => ('Doubt'),
                        );
                        $status=array_search($values[10], $statusarray);
                        $question= new Questions();
                        $question->TM_QN_Publisher_Id=(Publishers::model()->exists('TM_PR_Name=:value',array(':value'=>$values[0]))?Publishers::model()->findByAttributes(array('TM_PR_Name'=>$values[0]))->TM_PR_Id:'');
                        $question->TM_QN_Syllabus_Id=(Syllabus::model()->exists('TM_SB_Name=:value',array(':value'=>$values[1]))?Syllabus::model()->findByAttributes(array('TM_SB_Name'=>$values[1]))->TM_SB_Id:'');
                        $question->TM_QN_Standard_Id=(Standard::model()->exists('TM_SD_Name=:value',array(':value'=>$values[2]))?Standard::model()->findByAttributes(array('TM_SD_Name'=>$values[2]))->TM_SD_Id:'');
                        $question->TM_QN_Subject_Id=1;
                        $question->TM_QN_Topic_Id=(Chapter::model()->exists('TM_TP_Name=:value',array(':value'=>$values[3]))?Chapter::model()->findByAttributes(array('TM_TP_Name'=>$values[3]))->TM_TP_Id:'');
                        $question->TM_QN_Section_Id=(Topic::model()->exists('TM_SN_Name=:value',array(':value'=>$values[4]))?Topic::model()->findByAttributes(array('TM_SN_Name'=>$values[4]))->TM_SN_Id:'');
                        $question->TM_QN_Dificulty_Id=(Difficulty::model()->exists('TM_DF_Name=:value',array(':value'=>$values[5]))?Difficulty::model()->findByAttributes(array('TM_DF_Name'=>$values[5]))->TM_DF_Id:'');
                        $question->TM_QN_Type_Id=(Types::model()->exists('TM_TP_Name=:value',array(':value'=>$values[6]))?Types::model()->findByAttributes(array('TM_TP_Name'=>$values[6]))->TM_TP_Id:'');
                        $question->TM_QN_Teacher_Id=(Teachers::model()->exists('TM_TH_Name=:value',array(':value'=>$values[8]))?Teachers::model()->findByAttributes(array('TM_TH_Name'=>$values[8]))->TM_TH_Id:'');
                        $question->TM_QN_Pattern=$values[9];
                        $question->TM_QN_Status=$status;
                        $question->TM_QN_QuestionReff=$values[11];
                        $question->TM_QN_Question=$values[12];
                        $question->TM_QN_Totalmarks=$values[13];
                        $total+=$values[13];
                        $question->TM_QN_Solutions=$values[14];
                        $question->TM_QN_CreatedBy=Yii::app()->user->id;
                        $importlog[$count]['status']=$this->GetImportitems($values);
                        if($question->save(false)):
                            $schools=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_SchoolId)','condition'=>'TM_SPN_Publisher_Id='.$question->TM_QN_Publisher_Id.' AND TM_SPN_StandardId='.$question->TM_QN_Standard_Id));
                            foreach($schools AS $school):
                                $scquestion=new SchoolQuestions();
                                $scquestion->TM_SQ_School_Id=$school->TM_SPN_SchoolId;
                                $scquestion->TM_SQ_Question_Type=$question->TM_QN_Type_Id;
                                $scquestion->TM_SQ_Question_Id=$question->TM_QN_Id;
                                $scquestion->TM_SQ_Standard_Id=$question->TM_QN_Standard_Id;
                                $scquestion->TM_SQ_Chapter_Id=$question->TM_QN_Topic_Id;
                                $scquestion->TM_SQ_Topic_Id=$question->TM_QN_Section_Id;
                                $scquestion->TM_SQ_Syllabus_Id=$question->TM_QN_Syllabus_Id;
                                $scquestion->TM_SQ_Publisher_id=$question->TM_QN_Publisher_Id;
                                $scquestion->TM_SQ_Type='1';
                                $scquestion->TM_SQ_QuestionReff=$question->TM_QN_QuestionReff;
                                $scquestion->TM_SQ_Status='0';
                                $scquestion->save(false);
                            endforeach;                         
                            $importlog[$count]['question']='1';
                            for($j=0;$j<count($availability);$j++)
                            {
                                if(Exams::model()->exists('TM_EX_Name=:value',array(':value'=>$availability[$j]))):
                                    $exams=New QuestionExams();
                                    $exams->TM_QE_Exam_Id=Exams::model()->findByAttributes(array('TM_EX_Name'=>$availability[$j]))->TM_EX_Id;
                                    $exams->TM_QE_Question_Id=$question->TM_QN_Id;
                                    $exams->save(false);
                                else:
                                    $importlog[$count]['status']=$importlog[$count]['status']."Exam '".$availability[$j]."' Not Found";
                                endif;
                            }
                        else:
                            $importlog[$count]['question']='0';
                        endif;
                    endif;
                    $count++;
                }
            elseif($_POST['Type']=='7'):
                $file = fopen($_FILES['importcsv']['tmp_name'], "r");
                $count=0;
                while(! feof($file))
                {
                    $values=fgetcsv($file);
                    if($count!='0' & $values[0]!=''):

                        $availability=explode(',',$values[7]);
                        $statusarray=array(
                            '0' => ('Active'),
                            '1' => ('Inactive'),
                            '2' => ('Review'),
                            '3' => ('Flagged for review'),
                            '4' => ('Reviewed - Correct'),
                            '5' => ('Reviewed - Incorrect'),
                            '6' => ('Image'),
                            '7' => ('editor'),
                            '8' => ('Doubt'),
                        );
                        $status=array_search($values[10], $statusarray);
                        $question= new Questions();
                        $question->TM_QN_Publisher_Id=(Publishers::model()->exists('TM_PR_Name=:value',array(':value'=>$values[0]))?Publishers::model()->findByAttributes(array('TM_PR_Name'=>$values[0]))->TM_PR_Id:'');
                        $question->TM_QN_Syllabus_Id=(Syllabus::model()->exists('TM_SB_Name=:value',array(':value'=>$values[1]))?Syllabus::model()->findByAttributes(array('TM_SB_Name'=>$values[1]))->TM_SB_Id:'');
                        $question->TM_QN_Standard_Id=(Standard::model()->exists('TM_SD_Name=:value',array(':value'=>$values[2]))?Standard::model()->findByAttributes(array('TM_SD_Name'=>$values[2]))->TM_SD_Id:'');
                        $question->TM_QN_Subject_Id=1;
                        $question->TM_QN_Topic_Id=(Chapter::model()->exists('TM_TP_Name=:value',array(':value'=>$values[3]))?Chapter::model()->findByAttributes(array('TM_TP_Name'=>$values[3]))->TM_TP_Id:'');
                        $question->TM_QN_Section_Id=(Topic::model()->exists('TM_SN_Name=:value',array(':value'=>$values[4]))?Topic::model()->findByAttributes(array('TM_SN_Name'=>$values[4]))->TM_SN_Id:'');
                        $question->TM_QN_Dificulty_Id=(Difficulty::model()->exists('TM_DF_Name=:value',array(':value'=>$values[5]))?Difficulty::model()->findByAttributes(array('TM_DF_Name'=>$values[5]))->TM_DF_Id:'');
                        $question->TM_QN_Type_Id=(Types::model()->exists('TM_TP_Name=:value',array(':value'=>$values[6]))?Types::model()->findByAttributes(array('TM_TP_Name'=>$values[6]))->TM_TP_Id:'');
                        $question->TM_QN_Teacher_Id=(Teachers::model()->exists('TM_TH_Name=:value',array(':value'=>$values[8]))?Teachers::model()->findByAttributes(array('TM_TH_Name'=>$values[8]))->TM_TH_Id:'');
                        $question->TM_QN_Pattern=$values[9];
                        $question->TM_QN_Status=$status;
                        $question->TM_QN_QuestionReff=$values[11];
                        $question->TM_QN_Question=$values[12];
                        $question->TM_QN_Solutions=$values[19];
                        $question->TM_QN_CreatedBy=Yii::app()->user->id;
                        $marks=explode(',',$values[18]);
                        $importlog[$count]['status']=$this->GetImportitems($values);
                        if($question->save(false)):
                            $schools=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_SchoolId)','condition'=>'TM_SPN_Publisher_Id='.$question->TM_QN_Publisher_Id.' AND TM_SPN_StandardId='.$question->TM_QN_Standard_Id));
                            foreach($schools AS $school):
                                $scquestion=new SchoolQuestions();
                                $scquestion->TM_SQ_School_Id=$school->TM_SPN_SchoolId;
                                $scquestion->TM_SQ_Question_Type=$question->TM_QN_Type_Id;
                                $scquestion->TM_SQ_Question_Id=$question->TM_QN_Id;
                                $scquestion->TM_SQ_Standard_Id=$question->TM_QN_Standard_Id;
                                $scquestion->TM_SQ_Chapter_Id=$question->TM_QN_Topic_Id;
                                $scquestion->TM_SQ_Topic_Id=$question->TM_QN_Section_Id;
                                $scquestion->TM_SQ_Syllabus_Id=$question->TM_QN_Syllabus_Id;
                                $scquestion->TM_SQ_Publisher_id=$question->TM_QN_Publisher_Id;
                                $scquestion->TM_SQ_Type='1';
                                $scquestion->TM_SQ_QuestionReff=$question->TM_QN_QuestionReff;
                                $scquestion->TM_SQ_Status='0';
                                $scquestion->save(false);
                            endforeach;                          
                            $importlog[$count]['question']='1';
                            for($j=0;$j<count($availability);$j++)
                            {
                                if(Exams::model()->exists('TM_EX_Name=:value',array(':value'=>$availability[$j]))):
                                    $exams=New QuestionExams();
                                    $exams->TM_QE_Exam_Id=Exams::model()->findByAttributes(array('TM_EX_Name'=>$availability[$j]))->TM_EX_Id;
                                    $exams->TM_QE_Question_Id=$question->TM_QN_Id;
                                    $exams->save(false);
                                else:
                                    $importlog[$count]['status']=$importlog[$count]['status']."Exam '".$availability[$j]."' Not Found";
                                endif;
                            }
                            for($i=0;$i<$values[13];$i++)
                            {
                                $countanswer=$i+1;
                                $index=13+$countanswer;
                                $subquestion= new Questions();
                                $subquestion->TM_QN_Parent_Id=$question->TM_QN_Id;
                                $subquestion->TM_QN_Publisher_Id=(Publishers::model()->exists('TM_PR_Name=:value',array(':value'=>$values[0]))?Publishers::model()->findByAttributes(array('TM_PR_Name'=>$values[0]))->TM_PR_Id:'');
                                $subquestion->TM_QN_Syllabus_Id=(Syllabus::model()->exists('TM_SB_Name=:value',array(':value'=>$values[1]))?Syllabus::model()->findByAttributes(array('TM_SB_Name'=>$values[1]))->TM_SB_Id:'');
                                $subquestion->TM_QN_Standard_Id=(Standard::model()->exists('TM_SD_Name=:value',array(':value'=>$values[2]))?Standard::model()->findByAttributes(array('TM_SD_Name'=>$values[2]))->TM_SD_Id:'');
                                $subquestion->TM_QN_Subject_Id=1;
                                $subquestion->TM_QN_Topic_Id=(Chapter::model()->exists('TM_TP_Name=:value',array(':value'=>$values[3]))?Chapter::model()->findByAttributes(array('TM_TP_Name'=>$values[3]))->TM_TP_Id:'');
                                $subquestion->TM_QN_Section_Id=(Topic::model()->exists('TM_SN_Name=:value',array(':value'=>$values[4]))?Topic::model()->findByAttributes(array('TM_SN_Name'=>$values[4]))->TM_SN_Id:'');
                                $subquestion->TM_QN_Dificulty_Id=(Difficulty::model()->exists('TM_DF_Name=:value',array(':value'=>$values[5]))?Difficulty::model()->findByAttributes(array('TM_DF_Name'=>$values[5]))->TM_DF_Id:'');
                                $subquestion->TM_QN_Type_Id=(Types::model()->exists('TM_TP_Name=:value',array(':value'=>$values[6]))?Types::model()->findByAttributes(array('TM_TP_Name'=>$values[6]))->TM_TP_Id:'');
                                $subquestion->TM_QN_Teacher_Id=(Teachers::model()->exists('TM_TH_Name=:value',array(':value'=>$values[8]))?Teachers::model()->findByAttributes(array('TM_TH_Name'=>$values[8]))->TM_TH_Id:'');
                                $subquestion->TM_QN_Pattern=$values[9];
                                $subquestion->TM_QN_Status=$status;
                                $subquestion->TM_QN_QuestionReff=$values[11];
                                $subquestion->TM_QN_Question=$values[$index];
                                $subquestion->TM_QN_Totalmarks=$marks[$i];
                                $total+=$marks[$i];
                                $question->TM_QN_CreatedBy=Yii::app()->user->id;
                                $subquestion->save(false);
                            }
                        else:
                            $importlog[$count]['question']='1';
                        endif;
                    endif;
                    $count++;
                }
            endif;

            //update total mark into table questions
            Questions::model()->updateByPk($question->TM_QN_Id, array(
            'TM_QN_Totalmarks' => $total
            ));

             $this->Updateavailablemarks();
            //-------------------
        endif;
        $this->render('importcsv',array('importlog'=>$importlog));
    }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->scenario = 'Update';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
        $oldcount=$model->TM_QN_Option_Count;
        $oldtype=$model->TM_QN_Type_Id;
		if(isset($_POST['Questions']))
		{
			$model->attributes=$_POST['Questions'];
            if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
            {

                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionchoice'];
                $model->TM_QN_Option_Count=$_POST['Questions']['TM_QN_Option_Countchoice'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='3')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTF'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='4')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTEXT'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='5')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionpart'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;

            }
            elseif($model->TM_QN_Type_Id=='6')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionPaperOnly'];
                $model->TM_QN_Totalmarks=$_POST['Papermarks'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='7')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionMPPaperOnly'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }


            $tempimagesol=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'solution',':section'=>'main')));
            if($tempimagesol->image!=''):
                $model->TM_QN_Solution_Image=$tempimagesol->image;
            endif;
            $model->TM_QN_UpdatedOn=date('Y-m-d');
            $model->TM_QN_UpdatedBy=Yii::app()->user->id;
            if(isset($_POST['Questions']['TM_QN_Skill']))
            {
                $skill_ids=$_POST['Questions']['TM_QN_Skill'];
                $skill_id= implode(",",$skill_ids);
                $model->TM_QN_Skill=$skill_id;

            }
            if(isset($_POST['Questions']['TM_QN_Question_type']))
            {
                $type_ids=$_POST['Questions']['TM_QN_Question_type'];
                $type_id=implode(",",$type_ids);
                $model->TM_QN_Question_type=$type_id;
            }
            if(isset($_POST['Questions']['TM_QN_Info']))
            {
                $model->TM_QN_Info=$_POST['Questions']['TM_QN_Info'];
            }
            
			if($model->save())
            {
                $connection = CActiveRecord::getDbConnection();
                $sql="DELETE FROM tm_question_exams WHERE TM_QE_Question_Id='".$model->TM_QN_Id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();

                for($i=0;$i<count($_POST['Questions']['availability']);$i++)
                {
                    $exams=New QuestionExams();
                    $exams->TM_QE_Exam_Id=$_POST['Questions']['availability'][$i];
                    $exams->TM_QE_Question_Id=$model->TM_QN_Id;
                    $exams->save(false);
                }
                // insert into answers table
                $total=0;
                if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
                {
                    $answerid=array();
                    for($i=0;$i<$model->TM_QN_Option_Count;$i++)
                    {

                        $identifier=$i+1;
                        if(isset($_POST['answerid'.$identifier])):
                            $answer=Answers::model()->findByPk($_POST['answerid'.$identifier]);
                        else:
                            $answer=new Answers();
                        endif;
                        $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                        $answer->TM_AR_Answer=$_POST['answer'][$i];
                        switch ($model->TM_QN_Type_Id) {
                            case "1":
                                if(isset($_POST['correctanswer'.$identifier]))
                                {
                                    $answer->TM_AR_Correct=$_POST['correctanswer'.$identifier];
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                break;
                            case "2":
                                if($_POST['correctanswer']==$identifier)
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                break;
                        }


                        $tempimageanswer=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier)));
                        if($tempimageanswer->image!=''):
                            $answer->TM_AR_Image=$tempimageanswer->image;
                        endif;
                        $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                        $answer->TM_AR_Status='0';
                        $answer->save(false);
                        $answerid[$i]=$answer->TM_AR_Id;
                    }
                    $criteria = new CDbCriteria;
                    $criteria->addNotInCondition('TM_AR_Id' ,$answerid );
                    $criteria->addCondition('TM_AR_Question_Id='.$model->TM_QN_Id);
                    $deleanswers = Answers::model()->findAll($criteria);
                    foreach ($deleanswers as $value) {
                        $value->delete();
                    }
                }
                elseif($model->TM_QN_Type_Id=='3')
                {
                    $answer=Answers::model()->findByPk($_POST['answeridTF1']);
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='True';
                    if($_POST['correctanswerTF']=='True')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                        $total+=$_POST['marksTF'];
                    }
                    else
                    {
                        $answer->TM_AR_Correct='0';
                        $answer->TM_AR_Marks='0';
                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                    $answer=Answers::model()->findByPk($_POST['answeridTF2']);
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='False';
                    if($_POST['correctanswerTF']=='False')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                        $total+=$_POST['marksTF'];
                    }

                    else
                    {
                        $answer->TM_AR_Correct='0';
                        $answer->TM_AR_Marks='0';
                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='4')
                {
                    $answer=Answers::model()->findByPk($_POST['answeridTEXT']);
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer=$_POST['answeroptions'];
                    $answer->TM_AR_Marks=$_POST['marksTEXT'];
                    $total+=$_POST['marksTEXT'];
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='5')
                {
                    $childquestions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$id."'"));
                    $countchild=count($childquestions);                    
					for($i=0;$i<$_POST['multipleqstnum'];$i++)
                    {
                        
                        $identifier=$i+1;                        
                        $question=Questions::model()->findByPk($_POST['multyquestionid'.$identifier]);                        
						 if(isset($_POST['multyquestionid'.$identifier])):
                            $question=Questions::model()->findByPk($_POST['multyquestionid'.$identifier]);
                        else:
                            $question=new Questions();
							$question->TM_QN_Type_Id=$_POST['multypartType'.$identifier];
                        endif;                                               
                        $question->TM_QN_Parent_Id=$model->TM_QN_Id;
                        $question->TM_QN_Publisher_Id=$model->TM_QN_Publisher_Id;
                        $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                        $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                        $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                        $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                        $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                        $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;

                        $question->TM_QN_Question=$_POST['multypartQuestions'.$identifier];

                        $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                        if($tempimagechild->image!=''):
                            $question->TM_QN_Image=$tempimagechild->image;
                        endif;
						if(!isset($_POST['multyquestionid'.$identifier])):
							if($_POST['multypartType'.$identifier]=='1' | $_POST['multypartType'.$identifier]=='2')
							{
								$question->TM_QN_Option_Count=$model->TM_QN_Option_Count;
							}
							elseif($_POST['multypartType'.$identifier]=='4')
							{
								$id=$i+1;
								if(isset($_POST['Editor'.$id])):
									$question->TM_QN_Editor=$_POST['Editor'.$id];
								endif;
							}
						endif;
                        $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                        $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                        $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                        $question->TM_QN_Status=$model->TM_QN_Status;
                        if($question->save(false))
                        {
                            $multQuesTotalMark = 0;
							if(isset($_POST['multyquestionid'.$identifier])):
								$type=$question->TM_QN_Type_Id;
								$parts=count($question->answers);
							else:
								$type=$_POST['multypartType'.$identifier];
								$parts=$_POST['multypartOptions'.$identifier];
							endif;
                            if($type=='1' | $type=='2')
                            {

								for($j=0;$j<$parts;$j++)
                                {

                                    $multiidenti=$j+1;  
                                                            
									if(isset($_POST['answeridOPT'.$identifier.$multiidenti]))
									{
										$answer=Answers::model()->findByPk($_POST['answeridOPT'.$identifier.$multiidenti]);
									}
									else
									{
										$answer=new Answers();
									}

                                    $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                    $answer->TM_AR_Answer=$_POST['answer'.$identifier][$j];   
                                                                  
									if($type=='1')
									{
										if(isset($_POST['correctanswer'.$identifier.$multiidenti]))
										{

											$answer->TM_AR_Correct=$_POST['correctanswer'.$identifier.$multiidenti];
											$answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
                                            $total+=$_POST['marks'.$identifier.$multiidenti];
                                            $multQuesTotalMark += $_POST['marks'.$identifier.$multiidenti];
										}
										else
										{
											$answer->TM_AR_Correct='0';
											$answer->TM_AR_Marks='0';
										}
									}
									else
									{
										if($_POST['correctanswer'.$identifier]==$answer->TM_AR_Id || $_POST['correctanswer'.$identifier]==$multiidenti)
										{

											$answer->TM_AR_Correct='1';
											$answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
                                            $total+=$_POST['marks'.$identifier.$multiidenti];
                                            $multQuesTotalMark += $_POST['marks'.$identifier.$multiidenti];
										}
										else
										{
											$answer->TM_AR_Correct='0';
											$answer->TM_AR_Marks='0';
										}
									}
                                    $tempimagechildans=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier.$multiidenti)));
                                    if($tempimagechildans->image!=''):
                                        $answer->TM_AR_Image=$tempimagechildans->image;
                                    endif;
                                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                    $answer->TM_AR_Status='0';
                                    $answer->save(false);
                                }
                                
                            }
                            elseif($type=='3')
                            {
								if(isset($_POST['answeridTF'.$identifier.'2']))
								{
									$answer=Answers::model()->findByPk($_POST['answeridTF'.$identifier.'1']);
								}
								else
								{
									$answer=new Answers();
								}
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='True';
                                if($_POST['correctanswer'.$identifier]=='True')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                    $multQuesTotalMark += $_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
								if(isset($_POST['answeridTF'.$identifier.'2']))
								{
									$answer=Answers::model()->findByPk($_POST['answeridTF'.$identifier.'2']);
								}
								else
								{
									$answer=new Answers();
								}
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='False';
                                if($_POST['correctanswer'.$identifier]=='False')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                    $multQuesTotalMark += $_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                            elseif($type=='4')
                            {
                                $identifier=$i+1;
								if(isset($_POST['answeridTEXT'.$identifier]))
								{
									$answer=Answers::model()->findByPk($_POST['answeridTEXT'.$identifier]);
								}
								else
								{
									$answer=new Answers();
								}
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer=$_POST['answeroptions'.$identifier];
                                $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                $total+=$_POST['marks'.$identifier];
                                $multQuesTotalMark += $_POST['marks'.$identifier];
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                            $question->TM_QN_Totalmarks = $multQuesTotalMark;
                            $question->save(false);
                        }
                  


                    }                                         
                    /*if($countchild>$_POST['multipleqstnum']):
                        $deletecount=$countchild-$_POST['multipleqstnum'];
                     foreach($childquestions AS $childquestion):
    
                         if($deletecount<0){
    
                             $post=Questions::model()->findByPk($childquestion->TM_QN_Id ); // assuming there is a post whose ID is 10
                             $post->delete();
    
                         }
                        $deletecount--;

                     endforeach;

                    endif;*/

                }
                elseif($model->TM_QN_Type_Id=='6'){
                    $total+=$_POST['Papermarks'];
                }
                elseif($model->TM_QN_Type_Id=='7')
                {
                    $childquestions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$id."'"));
                    $countchild=count($childquestions);


                    if($countchild>$_POST['PoMPqstnum']):
                        $deletecount=$countchild-$_POST['PoMPqstnum'];


                        foreach($childquestions AS $childquestion):

                            if($deletecount<=0){

                                $post=Questions::model()->findByPk($childquestion->TM_QN_Id ); // assuming there is a post whose ID is 10
                                $post->delete();


                            }

                            $deletecount--;

                        endforeach;

                    endif;
                    for($i=0;$i<$_POST['PoMPqstnum'];$i++)
                    {
                        $identifier=$i+1;
                        if(isset($_POST['papermultyquestionid'.$identifier])):
                            $question=Questions::model()->findByPk($_POST['papermultyquestionid'.$identifier]);
                        else:
                            $question=new Questions();
                        endif;
                        $question->TM_QN_Parent_Id=$model->TM_QN_Id;
                        $question->TM_QN_Publisher_Id=$model->TM_QN_Publisher_Id;
                        $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                        $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                        $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                        $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                        $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                        $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;
                        $question->TM_QN_Type_Id=$model->TM_QN_Type_Id;
                        $question->TM_QN_Question=$_POST['PapermultypartQuestions'][$i];
                        $question->TM_QN_Solutions=$model->TM_QN_Solutions;
                        $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                        $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                        $question->TM_QN_Totalmarks=$_POST['PaperMPmarks'][$i];
                        $total+=$_POST['PaperMPmarks'][$i];
                        $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                        $question->TM_QN_Status=$model->TM_QN_Status;
                        $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                        if($tempimagechild->image!=''):
                            $question->TM_QN_Image=$tempimagechild->image;
                        endif;
                        $question->save(false);

                    }
                }
                //update the totalMark into table questions
                Questions::model()->updateByPk($model->TM_QN_Id, array(
                'TM_QN_Totalmarks' => $total
                ));

                $this->Updateavailablemarks();

                $this->redirect(array('view','id'=>$model->TM_QN_Id));
            }

		}

		$this->render('update',array(
			'model'=>$model,
			'availability'=>Questions::model()->getExams($id),

		));
	}
    public function actionDeletechildquestion()
    {
       if(isset($_POST['question'])):
        $question=Questions::model()->findByPk($_POST['question']);
        $parent=$question->TM_QN_Parent_Id;
        $answers=Answers::model()->findAll(array('condition'=>'TM_AR_Question_Id='.$_POST['question']));
        if($question->delete()):
            if(count($answers)>0):
                foreach($answers AS $answer):
                    $answer->delete();
                endforeach;
            endif;
            echo "yes";
        endif;
        
       endif; 
        
    }
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $question=$this->loadModel($id);	           
        $mockquestions=MockQuestions::model()->findAll(array('condition'=>'TM_MQ_Question_Id='.$id));
        $question->TM_QN_Status='111';
        if($question->save(false)):
            foreach($mockquestions AS $questionmock):
                 $mockid=$questionmock->TM_MQ_Mock_Id;
                 $questionorder=$questionmock->TM_MQ_Order;
                 $questionmock->delete();
                 $ordermock=MockQuestions::model()->findAll(array('condition'=>'TM_MQ_Mock_Id='.$mockid.' AND TM_MQ_Order >'.$questionorder,'order'=>'TM_MQ_Order ASC'));
                 $neworder=$questionorder;
                 foreach($ordermock AS $order):
                        $order->TM_MQ_Order=$neworder;
                        $order->save(false);
                        $neworder++;
                 endforeach;                                       
            endforeach; 
            SchoolQuestions::model()->deleteAll(array('condition'=>'TM_SQ_Question_Id='.$id.''));
        endif;                           

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Questions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Questions('search');
/*		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Questions']))
			$model->attributes=$_GET['Questions'];*/

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    public function actionChangeStatus()
    {
        $arr = explode(',', $_POST['theIds']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_QN_Id' ,$arr );
        $model = Questions::model()->findAll($criteria);
        foreach ($model as $value) {
            //update Order`s Status
            $value->TM_QN_Status = $_POST['status'];
            $value->update();
        }
    }
    public function actionDeleteimage()
    {
        if(isset($_POST['id'])):
            if($_POST['type']=='Question')
            {
                $question=$this->loadModel($_POST['id']);
                $image=$question->TM_QN_Image;
                // if(unlink(Yii::app()->basePath."/../images/questionimages/".$image) & unlink(Yii::app()->basePath."/../images/questionimages/thumbs/".$image) )
                // {
                    $question->TM_QN_Image='';
                    $question->save(false);
                    echo "yes";
               // }
            }
            elseif($_POST['type']=='answer')
            {
                $answer=Answers::model()->findByPk($_POST['id']);
                $image=$answer->TM_AR_Image;
                // if(unlink(Yii::app()->basePath."/../images/answerimages/".$image) & unlink(Yii::app()->basePath."/../images/answerimages/thumbs/".$image) )
                // {
                    $answer->TM_AR_Image='';
                    $answer->save(false);
                    echo "yes";
                //}
            }
            elseif($_POST['type']=='solution')
            {
                $question=$this->loadModel($_POST['id']);
                $image=$question->TM_QN_Solution_Image;
                // if(unlink(Yii::app()->basePath."/../images/solutionimages/".$image) & unlink(Yii::app()->basePath."/../images/solutionimages/thumbs/".$image) )
                // {
                    $question->TM_QN_Solution_Image='';
                    $question->save(false);
                    echo "yes";
                //}
            }
        endif;
    }
    public function actionDeleteimagetemp()
    {
        if(isset($_POST['imagetempid'])):
            $imagetemp=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
            if($_POST['type']=='question')
            {
                if(unlink(Yii::app()->basePath."/../images/questionimages/".$imagetemp->image) & unlink(Yii::app()->basePath."/../images/questionimages/thumbs/".$imagetemp->image) )
                {
                    Imagetemp::model()->deleteAll(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
                    echo "yes";
                }
            }
            elseif($_POST['type']=='answer')
            {
                if(unlink(Yii::app()->basePath."/../images/answerimages/".$imagetemp->image) & unlink(Yii::app()->basePath."/../images/answerimages/thumbs/".$imagetemp->image) )
                {
                    Imagetemp::model()->deleteAll(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
                    echo "yes";
                }
            }
            elseif($_POST['type']=='solution')
            {
                if(unlink(Yii::app()->basePath."/../images/solutionimages/".$imagetemp->image) & unlink(Yii::app()->basePath."/../images/solutionimages/thumbs/".$imagetemp->image) )
                {
                    Imagetemp::model()->deleteAll(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
                    echo "yes";
                }
            }
        endif;
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Questions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Questions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    public function actionGetTopicList()
    {
        if(isset($_POST['id']))
        {
            $data=Topic::model()->findAll("TM_SN_Topic_Id=:chapter AND TM_SN_Status='0' ORDER BY TM_SN_order ASC",
                array(':chapter'=>(int) $_POST['id']));
            $data=CHtml::listData($data,'TM_SN_Id','TM_SN_Name');
            echo CHtml::tag('option',array('value'=>''),'Select Topic',true);
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$value),CHtml::encode($name),true);
            }
        }
    }
    public function GetImportitems($values)
    {
        $message='';

        if(!Publishers::model()->exists('TM_PR_Name=:value',array(':value'=>$values[0])))
        {
            $message.='Publisher \''.$values[0].'\' Not Found<br>';
        }
        if(!Syllabus::model()->exists('TM_SB_Name=:value',array(':value'=>$values[1])))
        {
            $message.='Syllabus \''.$values[1].'\' Not Found<br>';
        }
        if(!Standard::model()->exists('TM_SD_Name=:value',array(':value'=>$values[2])))
        {
            $message.='Standard \''.$values[2].'\' Not Found<br>';
        }
        if(!Chapter::model()->exists('TM_TP_Name=:value',array(':value'=>$values[3])))
        {
            $message.='Chapter \''.$values[3].'\' Not Found<br>';
        }
        if(!Topic::model()->exists('TM_SN_Name=:value',array(':value'=>$values[4])))
        {
            $message.='Topic \''.$values[4].'\' Not Found<br>';
        }
        if(!Difficulty::model()->exists('TM_DF_Name=:value',array(':value'=>$values[5])))
        {
            $message.='Difficulty \''.$values[5].'\' Not Found<br>';
        }
        if(!Types::model()->exists('TM_TP_Name=:value',array(':value'=>$values[6])))
        {
            $message.='Type \''.$values[6].'\' Not Found<br>';
        }
        if(!Teachers::model()->exists('TM_TH_Name=:value',array(':value'=>$values[8])))
        {
            $message.='Teachers \''.$values[8].'\' Not Found<br>';
        }
        return $message;
    }
	/**
	 * Performs the AJAX validation.
	 * @param Questions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='questions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionUploadimageTemp()
    {
        $fieldname=$_POST['imagefieldname'];
        $type=$_POST['imagetype'];
        $section=$_POST['imagesection'];
        $tempid=$_POST['imagetempid'];
        $tempimagecount=Imagetemp::model()->count(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        if($tempimagecount>0):
            $imagetemp=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        else:
            $imagetemp=new Imagetemp();
        endif;
        $imagetemp->type=$type;
        $imagetemp->section=$section;
        $imagetemp->tempid=$tempid;
        $path='';
        if($_FILES[$fieldname]["error"]!=4)
        {
            $temp = explode(".",$_FILES[$fieldname]["name"]);

            $imagename=md5($_FILES[$fieldname]['name'].rand(999,9999)). '.' .end($temp);
            switch($type){
                case 'question':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/questionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/questionimages/thumbs/'.$imagename);
                    }
                    $path='questionimages';
                break;
                case 'answer':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/answerimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/answerimages/thumbs/'.$imagename);
                    }
                    $path='answerimages';
                break;
                case 'solution':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/solutionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/solutionimages/thumbs/'.$imagename);
                    }
                    $path='solutionimages';
                break;
            }
            $imagetemp->image=$imagename;
            $imagetemp->save(false);
            echo Yii::app()->request->baseUrl."/images/".$path."/thumbs/" . $imagename;
        }
    }


    public function actionUploadimage()
    {
        $fieldname=$_POST['imagefieldname'];
        $type=$_POST['imagetype'];
        $section=$_POST['imagesection'];
        $tempid=$_POST['imagetempid'];
        $tempimagecount=Imagetemp::model()->count(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        if($tempimagecount>0):
            $imagetemp=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        else:
            $imagetemp=new Imagetemp();
        endif;
        $imagetemp->type=$type;
        $imagetemp->section=$section;
        $imagetemp->tempid=$tempid;
        $path='';
        if($_FILES[$fieldname]["error"]!=4)
        {
            $temp = explode(".",$_FILES[$fieldname]["name"]);

            $imagename=md5($_FILES[$fieldname]['name'].rand(999,9999)). '.' .end($temp);
            switch($type){
                case 'question':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/questionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/questionimages/thumbs/'.$imagename);
                    }
                    $path='questionimages';
                    break;
                case 'answer':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/answerimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/answerimages/thumbs/'.$imagename);
                    }
                    $path='answerimages';
                    break;
                case 'solution':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/solutionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/solutionimages/thumbs/'.$imagename);
                    }
                    $path='solutionimages';
                    break;
            }
            $imagetemp->image=$imagename;
            $imagetemp->save(false);
            echo Yii::app()->request->baseUrl."/images/".$path."/thumbs/" . $imagename;
        }
    }
    public function actionGetTypeForm()
    {
        if($_POST['type']=='1'):
            $typpe='checkbox';
        else:
            $typpe='radio';
        endif;
        $this->renderPartial('_multypleoption',array('type'=>$typpe));
    }
    public function actionExportquestions()
    {
        if(isset($_POST['theIds'])):              
            $questioncount=Questions::model()->count(array('condition'=>'TM_QN_Id IN ('.$_POST['theIds'].')'));
            $filename=time().'_questions.csv';       
            $output = fopen(Yii::app()->basePath.'/../importfiles/'.$filename, 'w');                
            fputcsv($output, array(
                "Id",
                "Syllabus",
                "Standard",
                "Chapter",
                "Topic",
                "Reference",
                "Pattern",
                "Status",
                "New Syllabus",
                "New Standard",
                "New Chapter",
                "New Topic",
                "New Reference",
                "New Pattern",
                "New Status"
            ));            
            if($questioncount>0):
                $questions=Questions::model()->findAll(array('condition'=>'TM_QN_Id IN ('.$_POST['theIds'].')'));                
                foreach($questions AS $question):
                    $syllabus=Syllabus::model()->findByPk($question->TM_QN_Syllabus_Id)->TM_SB_Name;
                    $Standard=Standard::model()->findByPk($question->TM_QN_Standard_Id)->TM_SD_Name;
                    $Chapter=Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name;
                    $topic=Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name;
                    $status=Questions::itemAlias("QuestionStatus",$question->TM_QN_Status);
                    $itemrow=array(
                        $question->TM_QN_Id,
                        $syllabus,
                        $Standard,
                        $Chapter,
                        $topic,
                        $question->TM_QN_QuestionReff,
                        $question->TM_QN_Pattern,
                        $status,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""                        
                    );
                    fputcsv($output, $itemrow);
                endforeach;
            endif;                         
            fclose($output);
            echo Yii::app()->request->baseUrl."/questions/exportquestions/file/" . $filename;                
        endif;
        if($_GET['file']!=''):
            header('Content-Description: File Transfer');
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$_GET['file']);
            readfile(Yii::app()->basePath.'/../importfiles/'.$_GET['file']);                    
        endif;        
                    
    }
    public function actionExporttosyllabus()
    {
        $syllabus=Syllabus::model()->findAll(array("condition"=>"TM_SB_Status = '0'",'order' => 'TM_SB_Name'));        
        $this->render('exporttosyllabus',array('syllabus'=>$syllabus));
                 
    }
    public function actionExportsyllabuscsv()
    {
        if($_POST['syllabus']):
            $condition='TM_QN_Parent_Id=0 AND TM_QN_Syllabus_Id='.$_POST['syllabus'];
            $condition1='a.TM_QN_Parent_Id=0 AND a.TM_QN_Syllabus_Id='.$_POST['syllabus'];
            if($_POST['standard']!=''):
                $condition.=' AND TM_QN_Standard_Id='.$_POST['standard'];
                $condition1.=' AND a.TM_QN_Standard_Id='.$_POST['standard'];
            endif;
            if($_POST['chapter']!=''):
                $condition.=' AND TM_QN_Topic_Id='.$_POST['chapter'];
                $condition1.=' AND a.TM_QN_Topic_Id='.$_POST['chapter'];
            endif;
            if($_POST['topic']!=''):
                $condition.=' AND TM_QN_Section_Id='.$_POST['topic'];
                $condition1.=' AND a.TM_QN_Section_Id='.$_POST['topic'];
            endif;
            $questioncount=Questions::model()->count(array('condition'=>$condition));
            $filename=time().'_questions.csv';
            //$output = fopen(Yii::app()->basePath.'/../importfiles/'.$filename, 'w');

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$filename);
            $output = fopen('php://output', 'w');
            fputcsv($output, array(
                "Id",
                "Syllabus",
                "Standard",
                "Chapter",
                "Topic",
                "Reference",
                "Pattern",
                "Difficulty",
                "Print Option",
                "Availability",
                "Status",
                "Correct Answer",
                "Mark",
                "Skills",
                "Additional Info",
                "Notes",
                "Type",
                "New Syllabus",
                "New Standard",
                "New Chapter",
                "New Topic",
                "New Reference",
                "New Pattern",
                "New Difficulty",
                "New Print Option",
                "New Availability",
                "New Status",
                "New Correct Answer",
                "New Mark",
                "New Skill",
                "New Additional Info",
                "New Notes"
            ));
            if($questioncount>0):
                //$questions=Questions::model()->findAll(array('condition'=>$condition));
                $connection = CActiveRecord::getDbConnection();
                $sql="SELECT a.TM_QN_Id,a.TM_QN_Show_Option,a.TM_QN_Status,a.TM_QN_Type_Id,a.TM_QN_Totalmarks,a.TM_QN_QuestionReff,a.TM_QN_Pattern,a.TM_QN_Skill,a.TM_QN_Question_type,a.TM_QN_Info,
                      b.TM_SB_Name AS syllabus,c.TM_SD_Name AS Standard,d.TM_TP_Name AS Chapter,e.TM_SN_Name AS topic,
                      f.TM_DF_Name AS difficulty,g.TM_TP_Name AS type, h.tm_skill_name AS skill, i.TM_Question_Type AS qtype
                      FROM tm_question AS a
                      INNER JOIN tm_syllabus AS b ON a.TM_QN_Syllabus_Id=b.TM_SB_Id
                      INNER JOIN tm_standard AS c ON a.TM_QN_Standard_Id=c.TM_SD_Id
                      INNER JOIN tm_topic AS d ON a.TM_QN_Topic_Id=d.TM_TP_Id
                      INNER JOIN tm_section AS e ON a.TM_QN_Section_Id=e.TM_SN_Id
                      INNER JOIN tm_difficulty AS f ON a.TM_QN_Dificulty_Id=f.TM_DF_Id
                      INNER JOIN tm_types AS g ON a.TM_QN_Type_Id=g.TM_TP_Id
                      LEFT JOIN tm_skill AS h ON a.TM_QN_Skill=h.id
                      LEFT JOIN tm_question_type AS i ON a.TM_QN_Question_type=i.id
                      WHERE ".$condition1." ";
                //echo $sql;exit;
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $questions=$dataReader->readAll(); //print_r($questions); exit;
                foreach($questions AS $question):
                    $sql1="SELECT TM_QE_Exam_Id FROM tm_question_exams WHERE TM_QE_Question_Id=".$question['TM_QN_Id']." ";
                    $command=$connection->createCommand($sql1);
                    $dataReader=$command->query();
                    $availability=$dataReader->readAll();
                    $questionavailability='';
                    foreach($availability AS $key=>$avail):
                        $availabilityname=Exams::model()->findByPk($avail['TM_QE_Exam_Id'])->TM_EX_Name;
                        $questionavailability.=($key=='0'?'':':').$availabilityname;
                    endforeach;
                    $status=Questions::itemAlias("QuestionStatus",$question['TM_QN_Status']);
                    $printoption=($question['TM_QN_Show_Option']=='0'?'Hide':'Show');
                    if($question['TM_QN_Type_Id']=='1' || $question['TM_QN_Type_Id']=='2' || $question['TM_QN_Type_Id']=='3'):
                        $sql2="SELECT TM_AR_Correct,TM_AR_Marks FROM tm_answer WHERE TM_AR_Question_Id=".$question['TM_QN_Id']." ";
                        $command=$connection->createCommand($sql2);
                        $dataReader=$command->query();
                        $answers=$dataReader->readAll();
                        $correctanswer='';
                        $mark='';
                        foreach($answers AS $pkey=>$answer):
                            $correctanswer.=($answer['TM_AR_Correct']=='0'?'No':'Yes').";";
                            $mark.=$answer['TM_AR_Marks'].";";
                        endforeach;
                    elseif($question['TM_QN_Type_Id']=='4'):
                        $sql3="SELECT TM_AR_Marks FROM tm_answer WHERE TM_AR_Question_Id=".$question['TM_QN_Id']." ";
                        $command=$connection->createCommand($sql3);
                        $dataReader=$command->query();
                        $answers=$dataReader->read();
                        $correctanswer="Yes;";
                        $mark=$answers['TM_AR_Marks'].";";
                    elseif($question['TM_QN_Type_Id']=='5'):
                        $sql4="SELECT TM_QN_Type_Id,TM_QN_Id FROM tm_question WHERE TM_QN_Parent_Id=".$question['TM_QN_Id']." ";
                        $command=$connection->createCommand($sql4);
                        $dataReader=$command->query();
                        $childquestions=$dataReader->readAll();
                        $correctanswer="";
                        $mark="";
                        foreach($childquestions AS $childkey=>$childquestion):
                            if($childquestion['TM_QN_Type_Id']=='1' || $childquestion['TM_QN_Type_Id']=='2' || $childquestion['TM_QN_Type_Id']=='3'):
                                $sql5="SELECT TM_AR_Correct,TM_AR_Marks FROM tm_answer WHERE TM_AR_Question_Id=".$childquestion['TM_QN_Id']." ";
                                $command=$connection->createCommand($sql5);
                                $dataReader=$command->query();
                                $childanswers=$dataReader->readAll(); 
                                $childcorrect="";
                                $childmark='';
                                foreach($childanswers AS $key1=>$childanswer):
                                    $childcorrect.=($childanswer['TM_AR_Correct']=='0'?'No':'Yes').":";
                                    $childmark.=$childanswer['TM_AR_Marks'].":";
                                endforeach;
                            elseif($childquestion['TM_QN_Type_Id']=='4'):
                                $sql6="SELECT TM_AR_Marks FROM tm_answer WHERE TM_AR_Question_Id=".$childquestion['TM_QN_Id']." ";
                                $command=$connection->createCommand($sql6);
                                $dataReader=$command->query();
                                $childanswers=$dataReader->read();
                                $childcorrect="Yes:";
                                $childmark=$childanswers['TM_AR_Marks'].":";
                            endif;
                            $correctanswer.=$childcorrect.";";
                            $mark.=$childmark.";";
                        endforeach;
                    elseif($question['TM_QN_Type_Id']=='6'):
                        $correctanswer="Yes;";
                        $mark=$question['TM_QN_Totalmarks'].";";
                    else:
                        $sql7="SELECT TM_QN_Type_Id,TM_QN_Id,TM_QN_Totalmarks FROM tm_question WHERE TM_QN_Parent_Id=".$question['TM_QN_Id']." ";
                        $command=$connection->createCommand($sql7);
                        $dataReader=$command->query();
                        $paperchildquestions=$dataReader->readAll();
                        $correctanswer="";
                        $mark="";
                        foreach ($paperchildquestions as $key => $papervalue)
                        { 
                          $correctanswer.="Yes;";
                          $mark.=$papervalue['TM_QN_Totalmarks'].";";
                        }
                        
                    endif;
                    $skills='';

                    if($question['TM_QN_Skill']!='')
                    {    
                    $skills_array=array();
                    $skill_arrs=explode(",",$question['TM_QN_Skill']);

                    foreach ($skill_arrs as $key => $skill_arr) {
                    $sql8="SELECT tm_skill_name FROM  tm_skill WHERE id=".$skill_arr."";
                                $command=$connection->createCommand($sql8);
                                $dataReader=$command->query();
                                $question_skill=$dataReader->read();
                                array_push($skills_array,$question_skill['tm_skill_name']);
                    }

                    $skills=implode(";",$skills_array);
                }
                $types='';
                 if($question['TM_QN_Question_type']!='')
                   {
                    $type_array=array();
                    $type_arrs=explode(",",$question['TM_QN_Question_type']);
                    foreach ($type_arrs as $key => $type_arr) {
                        if($type_arr!=''):
                            $sql9="SELECT TM_Question_Type FROM  tm_question_type WHERE id=".$type_arr."";
                            $command=$connection->createCommand($sql9);
                            $dataReader=$command->query();
                            $question_type=$dataReader->read();
                            array_push($type_array,$question_type['TM_Question_Type']);
                        endif;
                    }
                    $types=implode(";",$type_array);
                }

                     $itemrow=array(
                        $question['TM_QN_Id'],//0
                        $question['syllabus'],//1
                        $question['Standard'],//2
                        $question['Chapter'],//3
                        $question['topic'],//4
                        $question['TM_QN_QuestionReff'],//5
                        $question['TM_QN_Pattern'],//6
                        $question['difficulty'],//7
                        $printoption,//8
                        $questionavailability,//9
                        $status,//10
                        $correctanswer,//11
                        $mark,//12
                        $skills,//13
                        $types,//14
                        $question['TM_QN_Info'],//15
                        $question['type'],//16
                        "",//17syllabus
                        "",//18standard
                        "",//19chapter
                        "",//20topic
                        "",//21Reff
                        "",//22pattern
                        "",//23difficulty
                        "",//24print
                        "",//25availability
                        "",//26status
                        "",//27canswer
                        "",//28mark
                        "",//29Skills
                        "",//30types
                        "",//31qinfo

                    );

                    fputcsv($output, $itemrow);
                endforeach; 
                
            endif;
        endif;
    }
    public function actionImporttosyllabus()
    {
        set_time_limit(500); 
        $importlog=array();
        if(isset($_POST['submit'])):
            $file = fopen($_FILES['importcsv']['tmp_name'], "r");
                $count=0;
                while(! feof($file))
                {
                    $values=fgetcsv($file);                   
                    if($count!='0' & $values[0]!='' & $count<500):
                      
                      
                        $importlog[$count]['status']='';
                        $questionid=$values[0]; 
                        //
                        if($values[17]!='')//syllabus
                        { 
                            // $syllabus = $values[12];
                            $newsyllabusidcount=Syllabus::model()->find(array('condition'=>'TM_SB_Name=:syllabus','params'=>array(':syllabus'=>$values[17])));
                        	if($newsyllabusidcount>0):
                        		$syllabus_id=Syllabus::model()->find(array('condition'=>'TM_SB_Name=:syllabus','params'=>array(':syllabus'=>$values[17])))->TM_SB_Id;
                            endif;
                                              
                        } else{

                        // $syllabus = $values[1];
                        $syllabus_id=Syllabus::model()->find(array('condition'=>'TM_SB_Name=:syllabus','params'=>array(':syllabus'=>$values[1])))->TM_SB_Id;
                        
                        }



                        // if($values[13]!='')://Standard
                        // 		$newstandardidcount=Standard::model()->count(array('TM_SD_Syllabus_Id'=>$syllabus_id),array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[13])));
                        // 		if($newstandardidcount>0):
                        //         $newstandardid = Standard::model()->findByAttributes(array('TM_SD_Syllabus_Id'=>$syllabus_id),array('condition'=>'TM_SD_Name=:standard', 'params'=>array(':standard'=>$values[13])))->TM_SD_Id;
                        // 			// $newstandardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[13])))->TM_SD_Id;
                        			
                        //             $question->TM_QN_Standard_Id=$newstandardid; 
                        // 		else:
                        // 			$importlog[$count]['status']=$importlog[$count]['status'].'New Standard "'.$values[13].'" Not Found!';                                                                                                                           
                        // 		endif;                                
                        // endif;

                        // if($values[14]!='')://chapter
                        // 		$newchapteridcount=Chapter::model()->count(array('TM_TP_Standard_Id'=>$newstandardid),array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[14])));
                        // 		if($newchapteridcount>0):
                        //             $newchapterid = Chapter::model()->findByAttributes(array('TM_TP_Standard_Id'=>$newstandardid),array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[14])))->TM_TP_Id;
                        // 			// $newchapterid=Chapter::model()->find(array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[14])))->TM_TP_Id;
                        // 			$question->TM_QN_Topic_Id=$newchapterid;
                        // 		else:
                        // 			$importlog[$count]['status']=$importlog[$count]['status'].'New Chapter "'.$values[14].'" Not Found!';
                        // 		endif;
                        // endif;
                        // if($values[15]!='')://topic
                        // 		$newtopicidcount=Topic::model()->count(array('TM_SN_Topic_Id'=>$newchapterid),array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[15])));
                        // 		if($newtopicidcount>0):
                        // 			$newtopicid = Topic::model()->findByAttributes(array('TM_SN_Topic_Id'=>$newchapterid),array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[15])))->TM_SN_Id;
                        //             // $newtopicid=Topic::model()->find(array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[15])))->TM_SN_Id;
                        // 			$question->TM_QN_Section_Id=$newtopicid;
                        // 		else:
                        // 			$importlog[$count]['status']=$importlog[$count]['status'].'New Topic "'.$values[15].'" Not Found!';
                        // 		endif;
                        // 	endif;


                    
                        // 

                        $syllabusid=Syllabus::model()->find(array('condition'=>'TM_SB_Name=:syllabus','params'=>array(':syllabus'=>$values[1])))->TM_SB_Id;
                        $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[2])))->TM_SD_Id;
                        $chapterid=Chapter::model()->find(array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[3])))->TM_TP_Id;
                        $topicid=Topic::model()->find(array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[4])))->TM_SN_Id;
                        $refference=$values[5];
                        $pattern=$values[6];
                        $question_info=$values[15];
                        $difficulty=Difficulty::model()->find(array('condition'=>'TM_DF_Name=:diffi','params'=>array(':diffi'=>$values[7])))->TM_DF_Id;                        
                        /*$availability=$values[9];
                        $questionavailability='';
                        foreach($availability AS $key=>$avail):
                            $availabilityname=Exams::model()->findByPk($avail->TM_QE_Exam_Id)->TM_EX_Name;
                            $questionavailability.=($key=='0'?'':':').$availabilityname;
                        endforeach;*/
                        $printoption=($values[8]=='Hide'?'0':'1');                        
                        $statusid=Questionstatusmaster::model()->find(array('condition'=>'TM_QSM_Name=:status','params'=>array(':status'=>$values[10])))->TM_QSM_Id;                                                                                                                                
                        $question=Questions::model()->find(array('condition'=>'TM_QN_Id=:questionid','params'=>array(':questionid'=>$questionid)));
                        // if($values[13]!='')://Standard
                        // 		$newstandardidcount=Standard::model()->count(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[13])));
                        // 		if($newstandardidcount>0):
                        // 			$newstandardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[13])))->TM_SD_Id;
                        // 			$question->TM_QN_Standard_Id=$newstandardid; 
                        // 		else:
                        // 			$importlog[$count]['status']=$importlog[$count]['status'].'New Standard "'.$values[13].'" Not Found!';                                                                                                                           
                        // 		endif;                                
                        // 	endif;
                        // 	if($values[14]!='')://chapter
                        // 		$newchapteridcount=Chapter::model()->count(array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[14])));
                        // 		if($newchapteridcount>0):
                        // 			$newchapterid=Chapter::model()->find(array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[14])))->TM_TP_Id;
                        // 			$question->TM_QN_Topic_Id=$newchapterid;
                        // 		else:
                        // 			$importlog[$count]['status']=$importlog[$count]['status'].'New Chapter "'.$values[14].'" Not Found!';
                        // 		endif;
                        // 	endif;
                        // 	if($values[15]!='')://topic
                        // 		$newtopicidcount=Topic::model()->count(array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[15])));
                        // 		if($newtopicidcount>0):
                        // 			$newtopicid=Topic::model()->find(array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[15])))->TM_SN_Id;
                        // 			$question->TM_QN_Section_Id=$newtopicid;
                        // 		else:
                        // 			$importlog[$count]['status']=$importlog[$count]['status'].'New Topic "'.$values[15].'" Not Found!';
                        // 		endif;
                        // 	endif;
                        
                        // vivek - code

                        if($values[29]!='')//skill
                        { 
                            //echo $values[29]; exit;
                            $skill_arrays=explode(";",$values[29]);
                            $skill_ids=array();
                           foreach($skill_arrays as $skills_array)
                           { 
                             
                                 if($values[18]!='')
                                 {
                                       $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[18])))->TM_SD_Id;
                                        $newskillcount=TmSkill::model()->find(array('condition'=>'tm_skill_name=:skill  AND tm_standard_id=:standard_id' ,'params'=>array(':skill'=>$skills_array,':standard_id'=>$standardid)));
                                 }
                                 else
                                 {
                                    $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[2])))->TM_SD_Id;
                                    $newskillcount=TmSkill::model()->find(array('condition'=>'tm_skill_name=:skill  AND tm_standard_id=:standard_id','params'=>array(':skill'=>$skills_array,':standard_id'=>$standardid)));
                                 }
                                 
                                if($newskillcount>0 && $values[18]!=''):
                                     $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[18])))->TM_SD_Id;
                                    $skill_id=TmSkill::model()->find(array('condition'=>'tm_skill_name=:skill  AND tm_standard_id=:standard_id','params'=>array(':skill'=>$skills_array,':standard_id'=>$standardid)))->id;
                                    array_push($skill_ids,$skill_id);
                                else:
                                      $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[2])))->TM_SD_Id;
                                     $skill_id=TmSkill::model()->find(array('condition'=>'tm_skill_name=:skill  AND tm_standard_id=:standard_id','params'=>array(':skill'=>$skills_array,':standard_id'=>$standardid)))->id;
                                    array_push($skill_ids,$skill_id);
                                endif; 


                            }

                             $skills=implode(",",$skill_ids);
                             $question->TM_QN_Skill=$skills;

                        } else{

                            $skill_arrays=explode(";",$values[13]);
                          
                            $skill_ids=array();
                            foreach($skill_arrays as $skills_array)
                            {
                              if($values[18]!='')
                               {
                                $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[18])))->TM_SD_Id;
                               }
                               else
                               {
                                $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[2])))->TM_SD_Id;
                               }
                                $skill_idarr=TmSkill::model()->find(array('condition'=>'tm_skill_name=:skill  AND tm_standard_id=:standard_id','params'=>array(':skill'=>$skills_array,':standard_id'=>$standardid)))->id;
                                array_push($skill_ids,$skill_idarr);
                            }
                                $skill_id=implode(",",$skill_ids);
                                
                                 $question->TM_QN_Skill=$skill_id;
                        }

                        if($values[30]!='')//type
                        { 
                            $type_arrays=explode(";",$values[30]);
                            $qtype_ids=array();
                            foreach ($type_arrays as $key => $type_array) {
                            if($values[18]!='')
                            {    
                                $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[18])))->TM_SD_Id;
                            }
                            else
                            {
                                $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[2])))->TM_SD_Id;

                            }
                               $newqtypecount=QuestionType::model()->find(array('condition'=>'TM_Question_Type=:qtype AND tm_standard_id=:standard_id','params'=>array(':qtype'=>$type_array,':standard_id'=>$standardid)));

                                if($newqtypecount>0 && $values[18]!=''):
                                   $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[18])))->TM_SD_Id;
                                    $qtype_id=QuestionType::model()->find(array('condition'=>'TM_Question_Type=:qtype AND tm_standard_id=:standard_id','params'=>array(':qtype'=>$type_array,':standard_id'=>$standardid)))->id;
                                else:
                                     $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[2])))->TM_SD_Id;
                                     $qtype_id=QuestionType::model()->find(array('condition'=>'TM_Question_Type=:qtype AND tm_standard_id=:standard_id','params'=>array(':qtype'=>$type_array,':standard_id'=>$standardid)))->id; 
                                     
                                endif;
                                 array_push($qtype_ids,$qtype_id);
                            }
                                $question_types=implode(",",$qtype_ids);
                                $question->TM_QN_Question_type=$question_types;
                        } else{

                        $type_arrays=explode(";",$values[14]);
                        $type_ids_arr=array();
                            foreach($type_arrays as $type_array)
                            {
                                $standardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[2])))->TM_SD_Id;
                                 $qtype_ids=QuestionType::model()->find(array('condition'=>'TM_Question_Type=:qtype AND tm_standard_id=:standard_id','params'=>array(':qtype'=>$type_array,':standard_id'=>$standardid)))->id;
                                array_push($type_ids_arr,$qtype_ids);
                            }
                                $qtype_id=implode(",",$type_ids_arr);
                        // $syllabus = $values[1];
                      
                         $question->TM_QN_Question_type=$qtype_id;
                        }

                           if($values[18]!='')://Standard
                        		$newstandardidcount=Standard::model()->findByAttributes(array('TM_SD_Syllabus_Id'=>$syllabus_id),array('condition'=>'TM_SD_Name=:standard', 'params'=>array(':standard'=>$values[18])));
                        		if($newstandardidcount>0):
                                $newstandardid = Standard::model()->findByAttributes(array('TM_SD_Syllabus_Id'=>$syllabus_id),array('condition'=>'TM_SD_Name=:standard', 'params'=>array(':standard'=>$values[18])))->TM_SD_Id;
                        			// $newstandardid=Standard::model()->find(array('condition'=>'TM_SD_Name=:standard','params'=>array(':standard'=>$values[13])))->TM_SD_Id;
                        			
                                    $question->TM_QN_Standard_Id=$newstandardid; 
                        		else:
                        			$importlog[$count]['status']=$importlog[$count]['status'].'New Standard "'.$values[18].'" Not Found!';
                        		endif;                                
                        endif;

                        if($values[19]!='')://chapter
                        		$newchapteridcount=Chapter::model()->findByAttributes(array('TM_TP_Standard_Id'=>$newstandardid),array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[19])));
                        		if($newchapteridcount>0):
                                    $newchapterid = Chapter::model()->findByAttributes(array('TM_TP_Standard_Id'=>$newstandardid),array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[19])))->TM_TP_Id;
                        			// $newchapterid=Chapter::model()->find(array('condition'=>'TM_TP_Name=:chapter','params'=>array(':chapter'=>$values[14])))->TM_TP_Id;
                        			$question->TM_QN_Topic_Id=$newchapterid;
                        		else:
                        			$importlog[$count]['status']=$importlog[$count]['status'].'New Chapter "'.$values[19].'" Not Found!';
                        		endif;
                        endif;

                        if($values[20]!='')://topic
                        		$newtopicidcount=Topic::model()->findByAttributes(array('TM_SN_Topic_Id'=>$newchapterid),array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[20])));
                        		if($newtopicidcount>0):
                        			$newtopicid = Topic::model()->findByAttributes(array('TM_SN_Topic_Id'=>$newchapterid),array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[20])))->TM_SN_Id;
                                    // $newtopicid=Topic::model()->find(array('condition'=>'TM_SN_Name=:topic','params'=>array(':topic'=>$values[15])))->TM_SN_Id;
                        			$question->TM_QN_Section_Id=$newtopicid;
                        		else:
                        			$importlog[$count]['status']=$importlog[$count]['status'].'New Topic "'.$values[20].'" Not Found!';
                        		endif;
                        endif;
                            //vivek - code - ends

                        	if($values[21]!='')://refference
                        		$newrefference=$values[21];
                        		$question->TM_QN_QuestionReff=$newrefference;
                        	endif;
                        	if($values[22]!='')://pattern
                        		$newpattern=$values[22];
                        		$question->TM_QN_Pattern=$newpattern;                          
                        	endif; 

                            if($values[31]!='')://info
                                $newinfo=$values[31];
                                $question->TM_QN_Info=$newinfo;
                            else:
                                $question->TM_QN_Info=$values[15];
                            endif;
                        	if($values[23]!='')://Difficulty
                        		$newdifficcount=Difficulty::model()->count(array('condition'=>'TM_DF_Name=:diff','params'=>array(':diff'=>$values[23])));
                        		if($newdifficcount>0):
                        			$newdifficid=Difficulty::model()->find(array('condition'=>'TM_DF_Name=:diff','params'=>array(':diff'=>$values[23])))->TM_DF_Id;
                        			$question->TM_QN_Dificulty_Id=$newdifficid;
                        		else:
                        			$importlog[$count]['status']=$importlog[$count]['status'].'New Difficulty "'.$values[23].'" Not Found!';
                        		endif;
                        	endif;  
                        	if($values[24]!=''):
                        		if($values[24]=='Show'):
                                    $newoption='1';
									$question->TM_QN_Show_Option='1';
								elseif($values[24]=='Hide'):
                                    $newoption='0';
									$question->TM_QN_Show_Option='0';
								endif;
                        	endif;
                        	if($values[25]!=''):
                        		$availability=explode(':',$values[25]);
                                $newavailability=array();
                                for($i=0;$i<count($availability);$i++){
                                    $newavailability[$i]=Exams::model()->find(array('condition'=>'TM_EX_Name=:exam','params'=>array(':exam'=>$availability[$i])))->TM_EX_Id;    
                                }
                        	endif;

                        	if($values[26]!='')://Status
                        		if($values[26]!='Delete'):
                        			$newstatusidcount=Questionstatusmaster::model()->count(array('condition'=>'TM_QSM_Name=:status','params'=>array(':status'=>$values[26])));
                        			if($newstatusidcount>0):
                        				$newstatusid=Questionstatusmaster::model()->find(array('condition'=>'TM_QSM_Name=:status','params'=>array(':status'=>$values[26])))->TM_QSM_Id;
                        				$question->TM_QN_Status=$newstatusid;
                        			else:
                        				$importlog[$count]['status']=$importlog[$count]['status'].'New Question Status "'.$values[26].'" Not Found!';
                        			endif;
                        		else:
                        			$question->TM_QN_Status='111';
                        		endif;                            
                        	endif;

                        if($values[17]!='' & $values[26]!='Delete'):
                        		// $newsyllabusidcount=Syllabus::model()->find(array('condition'=>'TM_SB_Name=:syllabus','params'=>array(':syllabus'=>$values[12])));
                        		if($newsyllabusidcount>0):
                        		// 	$newsyllabusid=Syllabus::model()->find(array('condition'=>'TM_SB_Name=:syllabus','params'=>array(':syllabus'=>$values[12])))->TM_SB_Id;                                                                   
                        			if($this->Copysyllabusquestions($question->TM_QN_Id,$syllabus_id,$newstandardid,$newchapterid,$newtopicid,$newrefference,$newpattern,$newdifficid,$newoption,$newavailability,$newstatusid,$values[27],$values[28],$skill_id,$qtype_id,$newinfo)):
                                        $importlog[$count]['question']='1';
                                    else:
                                        $importlog[$count]['status']=$importlog[$count]['status'].'Failed to create new question!';
                                        $importlog[$count]['question']='0';
                        			endif;
                        		else:
                        			$importlog[$count]['status']=$importlog[$count]['status'].'New Syllabus "'.$values[17].'" Not Found!';
                                    $importlog[$count]['question']='0';
                        		endif; 
                        else:                                                                                    	
                        	if($importlog[$count]['status']==''):
                                $importlog[$count]['question']='1';
                                $question->TM_QN_UpdatedOn=date('Y-m-d');
                        		$question->save(false);
                                if($values[27]!='' & $values[28]!=''):
                                    if($question['TM_QN_Type_Id']==5):
                                        $partquestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id='.$question['TM_QN_Id'].''));
                                        $totalMark = 0;
                                        foreach($partquestions AS $key=>$partquestion):
                                            $childanswers=Answers::model()->findAll(array('condition'=>'TM_AR_Question_Id='.$partquestion['TM_QN_Id'].' '));
                                            $multQuesTotalMark = 0;
                                            foreach($childanswers AS $key1=>$childanswer):
                                                $nopartsans=explode(';',$values[27]);
                                                $correctanswer=explode(':',$nopartsans[$key]);
                                                $nopartsmark=explode(';',$values[28]);
                                                $marks=explode(':',$nopartsmark[$key]);
                                                //print_r($correctanswer);
                                                if($correctanswer[$key1]!='No')
                                                {
                                                    $childanswer->TM_AR_Correct='1';
                                                    if($marks[$key1]!='0')
                                                    {
                                                        $totalMark += $marks[$key1];
                                                        $multQuesTotalMark += $marks[$key1];

                                                    }
                                                }
                                                else
                                                {
                                                    $childanswer->TM_AR_Correct='0';
                                                }
                                                ///echo $marks[$key1];
                                                if($marks[$key1]!='0')
                                                {
                                                    $childanswer->TM_AR_Marks=$marks[$key1];
                                                }
                                                else
                                                {
                                                    $childanswer->TM_AR_Marks='0';
                                                }
                                                $childanswer->save(false);
                                            endforeach;
                                            $partquestion->TM_QN_Totalmarks = $multQuesTotalMark;
                                            $partquestion->save(false);
                                            
                                        endforeach;
                                        $updateMark=Questions::model()->findByPk($question['TM_QN_Id']);
                                        $updateMark->TM_QN_Totalmarks = $totalMark;
                                        $updateMark->save(false);
                                    elseif($question['TM_QN_Type_Id']==7):
                                        ///$paperquestion=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id='.$question['TM_QN_Id'].''));
                                        $marks=explode(';',$values[28]);
                                        $partquestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id='.$question['TM_QN_Id'].''));
                                        foreach($partquestions AS $key5 => $partquestion):
                                            $partquestion->TM_QN_Totalmarks=$marks[$key5];
                                            $partquestion->save(false);
                                        endforeach;
                                    elseif($question['TM_QN_Type_Id']==6):
                                        $marks=explode(';',$values[28]);
                                        $question->TM_QN_Totalmarks=$marks[0];
                                        $question->save(false);
                                    else:
                                        $correctanswer=explode(';',$values[27]);
                                        $marks=explode(';',$values[28]);
                                        $answers=Answers::model()->findAll(array('condition'=>'TM_AR_Question_Id='.$question['TM_QN_Id'].' '));
                                        $totalMarks = 0;
                                        foreach($answers AS $key=>$answer):
                                            if($correctanswer[$key]!='No')
                                            {
                                                $answer->TM_AR_Correct='1';
                                                if($marks[$key]!='0')
                                                {
                                                    $totalMarks += $marks[$key];
                                                }
                                            }
                                            else
                                            {
                                                $answer->TM_AR_Correct='0';
                                            }
                                            if($marks[$key]!='0')
                                            {
                                                $answer->TM_AR_Marks=$marks[$key];
                                            }
                                            else
                                            {
                                                $answer->TM_AR_Marks='0';
                                            }
                                            $answer->save(false);
                                        endforeach;
                                        $updateMark=Questions::model()->findByPk($question['TM_QN_Id']);
                                        $updateMark->TM_QN_Totalmarks = $totalMarks;
                                        $updateMark->save(false);
                                    endif;
                                endif;
                                if($newavailability!=''):
                                    $connection = CActiveRecord::getDbConnection();
                                    $sql="DELETE FROM tm_question_exams WHERE TM_QE_Question_Id='".$question->TM_QN_Id."'";
                                    $command=$connection->createCommand($sql);
                                    $dataReader=$command->query();
                    
                                    for($i=0;$i<count($newavailability);$i++)
                                    {
                                        $exams=New QuestionExams();
                                        $exams->TM_QE_Exam_Id=$newavailability[$i];
                                        $exams->TM_QE_Question_Id=$question->TM_QN_Id;
                                        $exams->save(false);
                                    }
                                endif;                                
                            else:
                                $importlog[$count]['question']='0';
                        	endif;                              
                        endif;                                                
                                             
                    endif;
                    
                    $count++;                    
                }                            
        endif;   
        $this->Updateavailablemarks();     
        $this->render('importsyllabus',array('importlog'=>$importlog));        
    }
    public function Copysyllabusquestions($id,$syllabus,$standard,$chapter,$topic,$refference,$pattern,$difficid,$showoption,$newavailability,$status,$answer,$mark,$skill,$qtype,$qinfo)
    {
        if(isset($id) & $syllabus!='' & $standard!='' & $chapter!='' & $topic!='' & $refference !='' & $pattern!=''  & $difficid!='' & $status!='' & $showoption!='' & $status!=''):
            $question=Questions::model()->findByPk($id);
            $type=$question->TM_QN_Type_Id;
            $newquestion= new Questions();
            $newquestion->attributes=$question->attributes;
            $newquestion->TM_QN_Id=NULL;
            $newquestion->TM_QN_Question=$question->TM_QN_Question;
            $newquestion->TM_QN_Option_Count=$question->TM_QN_Option_Count;
            $newquestion->TM_QN_Syllabus_Id=$syllabus;
            $newquestion->TM_QN_Standard_Id=$standard;
            $newquestion->TM_QN_Topic_Id=$chapter;
            $newquestion->TM_QN_Section_Id=$topic;
            $newquestion->TM_QN_Pattern=$pattern;
            $newquestion->TM_QN_Dificulty_Id=$difficid;
            $newquestion->TM_QN_Show_Option=$showoption;
            $newquestion->TM_QN_Totalmarks=$question->TM_QN_Totalmarks;
            $newquestion->TM_QN_QuestionReff=$refference;
            $newquestion->TM_QN_CreatedBy=Yii::app()->user->id;
            $newquestion->TM_QN_CreatedOn=date('Y-m-d');
            $newquestion->TM_QN_UpdatedBy='0';
            $newquestion->TM_QN_UpdatedOn='0000-00-00';//date('Y-m-d');
            $newquestion->TM_QN_Status=$status;
            $newquestion->TM_QN_Skill=$skill;
            $newquestion->TM_QN_Question_type=$qtype;
            $newquestion->TM_QN_Info=$qinfo;
            //print_r($newquestion);exit();
            switch($type)
            {
                case '5':
                    $questionparts=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                    if($newquestion->save(false)):
                        if($newavailability!=''):
                            for($i=0;$i<count($newavailability);$i++)
                            {
                                $exams=New QuestionExams();
                                $exams->TM_QE_Exam_Id=$newavailability[$i];
                                $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                                $exams->save(false);
                            }                         
                        else:
                            $qstexams=Questions::model()->getExams($question->TM_QN_Id);
                            foreach($qstexams AS $exam):
                                $exams=New QuestionExams();
                                $exams->TM_QE_Exam_Id=$exam['Id'];
                                $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                                $exams->save(false);
                            endforeach;
                        endif;
                        foreach($questionparts AS $key=>$parts):
                            //$parts=Questions::model()->findByPk($parts->TM_QN_Question);
                            $newpart= new Questions();
                            $newpart->attributes=$parts->attributes;
                            $newpart->TM_QN_Id=NULL;
                            $newpart->TM_QN_Question=$parts->TM_QN_Question;
                            $newpart->TM_QN_Option_Count=$parts->TM_QN_Option_Count;
                            $newpart->TM_QN_Parent_Id=$newquestion->TM_QN_Id;
                            $newpart->TM_QN_Syllabus_Id=$syllabus;
                            $newpart->TM_QN_Standard_Id=$standard;
                            $newpart->TM_QN_Topic_Id=$chapter;
                            $newpart->TM_QN_Section_Id=$topic;
                            $newpart->TM_QN_Pattern=$pattern;
                            $newpart->TM_QN_QuestionReff=$refference;
                            $newpart->TM_QN_Skill=$skill;
                            $newpart->TM_QN_Question_type=$qtype;
                            $newpart->TM_QN_Info=$qinfo;                            
                            if($newpart->save(false)):
                                $totalMark = 0;
                                foreach($parts->answers AS $key1=>$ans):
                                    //$answer=Answers::model()->findByPk($ans->TM_AR_Id);
                                    $answerdata=Answers::model()->findByPk($ans->TM_AR_Id);
                                    $newanswer=new Answers();
                                    $newanswer->attributes=$answerdata->attributes;
                                    $newanswer->TM_AR_Id=NULL;
                                    $newanswer->TM_AR_Answer=$answerdata->TM_AR_Answer;
                                    $newanswer->TM_AR_Question_Id=$newpart->TM_QN_Id;
                                    $nopartsans=explode(';',$answer);
                                    $correctanswer=explode(':',$nopartsans[$key]);
                                    $nopartsmark=explode(';',$mark);
                                    $marks=explode(':',$nopartsmark[$key]);
                                    if($correctanswer[$key1]!='No')
                                    {
                                        $newanswer->TM_AR_Correct='1';
                                        if($marks[$key1]!='0')
                                        {
                                            $totalMark += $marks[$key1];
                                        }
                                    }
                                    //echo $marks[$key1];
                                    if($marks[$key1]!='0')
                                    {
                                        $newanswer->TM_AR_Marks=$marks[$key1];
                                    }
                                    $newanswer->TM_AR_CreatedBy=$answerdata->TM_AR_CreatedBy;
                                    $newanswer->TM_AR_Status=$answerdata->TM_AR_Status;
                                    $newanswer->save(false);
                                endforeach;
                                $updateMark=Questions::model()->findByPk($newpart->TM_QN_Id);
                                $updateMark->TM_QN_Totalmarks = $totalMark;
                                $updateMark->save(false);
                            endif;
                        endforeach;
                    endif;
                    break;
                case '6':
                    $newquestion->save(false);
                    $updatenew=Questions::model()->findByPk($newquestion->TM_QN_Id);
                    $marks=explode(';',$mark);
                    $updatenew->TM_QN_Totalmarks=$marks[0];
                    $updatenew->save(false);
                        if($newavailability!=''):
                            for($i=0;$i<count($newavailability);$i++)
                            {
                                $exams=New QuestionExams();
                                $exams->TM_QE_Exam_Id=$newavailability[$i];
                                $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                                $exams->save(false);
                            }                                                 
                        endif;                    
                    break;
                case '7':
                    if($newquestion->save(false)):
                        $questionparts=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        $marks=explode(';',$mark);
                        foreach($questionparts AS $key3=>$parts):
                            $newpart= new Questions();
                            $newpart->attributes=$parts->attributes;
                            $newpart->TM_QN_Id=NULL;
                            $newpart->TM_QN_Question=$parts->TM_QN_Question;
                            $newpart->TM_QN_Parent_Id=$newquestion->TM_QN_Id;
                            $newpart->TM_QN_Syllabus_Id=$syllabus;
                            $newpart->TM_QN_Standard_Id=$standard;
                            $newpart->TM_QN_Topic_Id=$chapter;
                            $newpart->TM_QN_Section_Id=$topic;
                            $newpart->TM_QN_Pattern=$pattern;
                            $newpart->TM_QN_QuestionReff=$refference;
                            $newpart->TM_QN_Totalmarks = $marks[$key3];
                            $newpart->TM_QN_Skill=$skill;
                            $newpart->TM_QN_Question_type=$qtype;
                            $newpart->TM_QN_Info=$qinfo;   
                            $newpart->save(false);
                        endforeach;
                    endif;
                    break;
                default:
                    if($newquestion->save(false)):
                        if($newavailability!=''):
                            for($i=0;$i<count($newavailability);$i++)
                            {
                                $exams=New QuestionExams();
                                $exams->TM_QE_Exam_Id=$newavailability[$i];
                                $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                                $exams->save(false);
                            }                         
                        else:
                            $qstexams=Questions::model()->getExams($question->TM_QN_Id);
                            foreach($qstexams AS $exam):
                                $exams=New QuestionExams();
                                $exams->TM_QE_Exam_Id=$exam['Id'];
                                $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                                $exams->save(false);
                            endforeach;
                        endif;
                        $correctanswer=explode(';',$answer);
                        $marks=explode(';',$mark);
                        $totalMark = 0;
                        foreach($question->answers AS $key=>$ans):
                            $answerdata=Answers::model()->findByPk($ans->TM_AR_Id);
                            $newanswer=new Answers();
                            $newanswer->attributes=$answerdata->attributes;
                            $newanswer->TM_AR_Id=NULL;
                            $newanswer->TM_AR_Answer=$answerdata->TM_AR_Answer;
                            $newanswer->TM_AR_Question_Id=$newquestion->TM_QN_Id;
                            if($correctanswer[$key]!='No')
                            {
                                $newanswer->TM_AR_Correct='1';
                                if($marks[$key]!='0')
                                {
                                    $totalMark += $marks[$key];
                                }
                            }
                            if($marks[$key]!='0')
                            {
                                $newanswer->TM_AR_Marks=$marks[$key];
                            }
                            $newanswer->TM_AR_CreatedBy=$answerdata->TM_AR_CreatedBy;
                            $newanswer->TM_AR_Status=$answerdata->TM_AR_Status;
                            $newanswer->save(false);
                        endforeach;
                        $updateMarks=Questions::model()->findByPk($question->TM_QN_Id);
                        $updateMarks->TM_QN_Totalmarks = $totalMark;
                        $updateMarks->save(false);
                    endif;

            }
            $schools=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_SchoolId)','condition'=>'TM_SPN_Publisher_Id='.$newquestion->TM_QN_Publisher_Id.' AND TM_SPN_StandardId='.$newquestion->TM_QN_Standard_Id));
            foreach($schools AS $school):
                $scquestion=new SchoolQuestions();
                $scquestion->TM_SQ_School_Id=$school->TM_SPN_SchoolId;
                $scquestion->TM_SQ_Question_Type=$newquestion->TM_QN_Type_Id;
                $scquestion->TM_SQ_Question_Id=$newquestion->TM_QN_Id;
                $scquestion->TM_SQ_Standard_Id=$newquestion->TM_QN_Standard_Id;
                $scquestion->TM_SQ_Chapter_Id=$newquestion->TM_QN_Topic_Id;
                $scquestion->TM_SQ_Topic_Id=$newquestion->TM_QN_Section_Id;
                $scquestion->TM_SQ_Syllabus_Id=$newquestion->TM_QN_Syllabus_Id;
                $scquestion->TM_SQ_Publisher_id=$newquestion->TM_QN_Publisher_Id;
                $scquestion->TM_SQ_Type='1';
                $scquestion->TM_SQ_QuestionReff=$newquestion->TM_QN_QuestionReff;
                $scquestion->TM_SQ_Status='0';
                $scquestion->save(false);
            endforeach;
            return true;
        else:
            return false;                         
        endif;
    }

    public function Updateavailablemarks()
    {
        ini_set('memory_limit', '512M');
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT * FROM tm_standard WHERE TM_SD_Status=0";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $standards = $dataReader->readAll();
        foreach($standards as $standard)
        {
            $sql1="UPDATE tm_available_marks SET status=1 WHERE TM_AV_MK_Standard_Id=".$standard['TM_SD_Id']."";
            $command=$connection->createCommand($sql1);
            $dataReader=$command->query();
            $sql="SELECT * FROM tm_question WHERE TM_QN_Standard_Id=".$standard['TM_SD_Id']." AND TM_QN_Parent_Id=0 AND TM_QN_Status=0 GROUP BY TM_QN_Totalmarks";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $questions = $dataReader->readAll();
            foreach($questions as $question)
            {
                //    var_dump($question);exit;
                $mark = $question['TM_QN_Totalmarks'];
                if($mark != 0)
                {
                    /// $markExists = TmAvailableMarks::model()->findAll(array('condition'=>'TM_AV_MK_Standard_Id='.$standard->TM_SD_Id.' AND TM_AV_MK_Mark='.$mark));

                    $sql1="SELECT * FROM tm_available_marks WHERE TM_AV_MK_Standard_Id=".$standard['TM_SD_Id']." AND TM_AV_MK_Mark=".$mark;
                    $command=$connection->createCommand($sql1);
                    $dataReader=$command->query();
                    $markExists = $dataReader->readAll();

                    if(count($markExists)==0)
                    {

                        $availableMark = new TmAvailableMarks();
                        $availableMark->TM_AV_MK_Standard_Id = $standard['TM_SD_Id'];
                        $availableMark->TM_AV_MK_Mark = $mark;
                        $availableMark->save(false);
                    }
                    else
                    {
                        $sql1="UPDATE tm_available_marks SET status=0 WHERE TM_AV_MK_Standard_Id=".$standard['TM_SD_Id']." AND TM_AV_MK_Mark=".$mark;
                        $command=$connection->createCommand($sql1);
                        $dataReader=$command->query();

                    }

                }
            }
        }
    }

     public function actionFindAvailableMarks()
    {
        // $standards = Standard::model()->findAll();
        ini_set('memory_limit', '512M');
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT * FROM tm_standard WHERE TM_SD_Status=0";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $standards = $dataReader->readAll();
        // var_dump($standards);
        foreach($standards as $standard)
        {
            // var_dump($standard);exit;
            // $questions = Questions::model()->findAll(array('condition'=>'TM_QN_Standard_Id='.$standard->TM_SD_Id));

            $sql="SELECT * FROM tm_question WHERE TM_QN_Standard_Id=".$standard['TM_SD_Id']." AND TM_QN_Parent_Id=0 AND TM_QN_Status=0 GROUP BY TM_QN_Totalmarks";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $questions = $dataReader->readAll();

            $sql1="UPDATE tm_available_marks SET status=1 WHERE TM_AV_MK_Standard_Id=".$standard['TM_SD_Id']."";
            $command=$connection->createCommand($sql1);
            $dataReader=$command->query();

            foreach($questions as $question)
            {
                //    var_dump($question);exit;
                $mark = $question['TM_QN_Totalmarks'];
                if($mark != 0)
                {
                    /// $markExists = TmAvailableMarks::model()->findAll(array('condition'=>'TM_AV_MK_Standard_Id='.$standard->TM_SD_Id.' AND TM_AV_MK_Mark='.$mark));

                    $sql1="SELECT * FROM tm_available_marks WHERE TM_AV_MK_Standard_Id=".$standard['TM_SD_Id']." AND TM_AV_MK_Mark=".$mark;
                    $command=$connection->createCommand($sql1);
                    $dataReader=$command->query();
                    $markExists = $dataReader->readAll();

                    if(count($markExists)==0)
                    {

                        $availableMark = new TmAvailableMarks();
                        $availableMark->TM_AV_MK_Standard_Id = $standard['TM_SD_Id'];
                        $availableMark->TM_AV_MK_Mark = $mark;
                        $availableMark->save(false);
                    }
                    else
                    {
                        $sql1="UPDATE tm_available_marks SET status=0 WHERE TM_AV_MK_Standard_Id=".$standard['TM_SD_Id']." AND TM_AV_MK_Mark=".$mark;
                        $command=$connection->createCommand($sql1);
                        $dataReader=$command->query();

                    }

                }
            }

        }
        echo "successfully updated";exit;

    }

    public function actionUpdateMultiparMark()
    {

        ini_set('memory_limit', '512M');


        $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id !=0",'offset' => $_GET['from'],'limit'=>500, 'order'=>'TM_QN_Id ASC'));
        // var_dump($childquestions);
        // exit;
        foreach($childquestions as $childquestion)
        {

            if($childquestion->TM_QN_Type_Id ==4)
            {    

                $connection = CActiveRecord::getDbConnection();
                $sql ="SELECT sum(TM_AR_Marks) AS mark FROM tm_answer WHERE TM_AR_Question_Id = '".$childquestion["TM_QN_Id"]."'";
                 $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $total=$dataReader->read();
            }
            else
            {

                $connection = CActiveRecord::getDbConnection();
                $sql ="SELECT sum(TM_AR_Marks) AS mark FROM tm_answer WHERE TM_AR_Question_Id = '".$childquestion["TM_QN_Id"]."' AND TM_AR_Correct=1";
                 $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $total=$dataReader->read();


            }

            // var_dump($total['mark']);
            // exit;
          
            if($total['mark'] != 0 && $total['mark'] != NULL && $total['mark'] != '')
            {
                $childquestion->TM_QN_Totalmarks = $total['mark'];
                $childquestion->save(false);
                // var_dump($childquestion);
                // echo $childquestion->TM_QN_Id;
                // echo $total['mark'];
                // exit;
            }
            
        }

        echo 'success';
    }

    public function actionGetSkill()
    {
       if(isset($_POST['id']))
       {
          $data=TmSkill::model()->findAll("tm_standard_id=:skill ORDER BY id ASC",
                array(':skill'=>(int) $_POST['id']));
            //$data=CHtml::listData($data,'tm_standard_id','tm_skill_name');
           // echo CHtml::tag('option',array('value'=>''),'Select Skill',true);
            $datarows.='<option value="">Select Skill</option>';
            foreach($data as $name)
            {
                
                $datarows.='<option value='.$name["id"].'>'.$name["tm_skill_name"].'</option>';
            } 
            $arr = array('result' => $datarows,'error'=>'No');
            echo json_encode($arr);
           
       }


    }

    public function actionGetqtype()
    {
       if(isset($_POST['id']))
       {
          $data=QuestionType::model()->findAll("tm_standard_id=:qtype ORDER BY id ASC",
                array(':qtype'=>(int) $_POST['id']));
            //$data=CHtml::listData($data,'tm_standard_id','tm_skill_name');
           // echo CHtml::tag('option',array('value'=>''),'Select Skill',true);
            $datarows.='<option value="" >Select</option>';
            foreach($data as $name)
            {
                
                $datarows.='<option value='.$name["id"].'>'.$name["TM_Question_Type"].'</option>';
            } 
            $arr = array('result' => $datarows,'error'=>'No');
            echo json_encode($arr);
           
       }


    }

    public function actionStandardlist()
    {
          if(isset($_POST['id']))
          {
              $data=Standard::model()->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0'",
                  array(':syllabus'=>(int) $_POST['id']));

              $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
/*              if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
              endif;*/
              echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
              foreach($data as $value=>$name)
              {
                  echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
              }
          }
    }

    public function actionSetoptions()
    {
        /*$questions=Questions::model()->findAll(array('condition'=>'TM_QN_Type_Id=2 AND TM_QN_Option_Count=0','limit'=>'500'));
        echo "Count : ".count($questions);
        foreach($questions AS $key=>$question):
            $answers=Answers::model()->findAll(array('condition'=>'TM_AR_Question_Id='.$question['TM_QN_Id'].' '));
            echo $key." Questionid : ".$question['TM_QN_Id']." Answer : ".count($answers)."<br>";
            $optcount=count($answers);
            $question->TM_QN_Option_Count=$optcount;
            $question->save(false);
        endforeach;*/
        /*$questions=Questions::model()->findAll(array('condition'=>'TM_QN_Type_Id=5 '));
        foreach($questions AS $question):
            $questions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id='.$question['TM_QN_Id'].' AND TM_QN_Type_Id=1 AND TM_QN_Option_Count=0','limit'=>'500'));
            if(count($questions) > 0):
            echo " Questionid : ".$question['TM_QN_Id']." Count : ".count($questions)."<br>";
            endif;
        endforeach;*/
    }
}
