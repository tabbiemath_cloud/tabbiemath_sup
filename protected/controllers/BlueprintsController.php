<?php

class BlueprintsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin','delete','duplicate'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'GetStandard', 'GetSecondPart','GetSecondPartEdit', 'Save','UpdateBluePrint', 'SaveNumberOfQuestion','UpdateNumberOfQuestion','SaveTopic', 'UpdateTopic','ChangeStatus','ChangeStatusInactive','Blueprintschools'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
			$this->layout = '//layouts/schoolcolumn2';
		endif;
		$model = $this->loadModel($id);
		 $chapters = Chapter::model()->findAll(array('condition'=>'TM_TP_Standard_Id='.$model->TM_BP_Standard_Id.' AND TM_TP_Status=0', 'order'=>'TM_TP_order ASC'));
        $marks = TmAvailableMarks::model()->findAll(array('condition'=>'TM_AV_MK_Standard_Id='.$model->TM_BP_Standard_Id.' AND status=0'));

		$this->render('view',array(
			'model'=>$model,
			'chapters'=>$chapters,
            'marks'=>$marks,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
			$this->layout = '//layouts/schoolcolumn2';
		endif;
		$model=new Blueprints;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Blueprints']))
		{
			$model->attributes=$_POST['Blueprints'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_BP_Id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
			$this->layout = '//layouts/schoolcolumn2';
		endif;
		///echo $id;exit;
		$model=$this->loadModel($id);
		$selectedSchools = BlueprintSchool::model()->findAll(array('condition'=>'TM_BPS_Status=0 AND TM_BPS_Blueprint_Id='.$id));
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Blueprints']))
		{
			$model->attributes=$_POST['Blueprints'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_BP_Id));
		}

		$this->render('update',array(
			'model'=>$model,
			'schools'=>$selectedSchools,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//echo $id;exit;
		$uid = Yii::app()->user->id;
		$user=User::model()->find(array('condition'=>'id="'.$uid.'"'));
		$blueprint = Blueprints::model()->findByPk($id);
		if($uid==$blueprint->TM_BP_CreatedBy || Yii::app()->user->isAdmin()){
			
		  	$this->loadModel($id)->delete();
			
		}
		else{
			$userSchool = $user->school_id;
		  	$blueprintschool = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='.$id.' AND TM_BPS_School_Id='.$userSchool));
		  	$blueprintschool->TM_BPS_Status = 1;
		  	$blueprintschool->save(false);
			
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionChangeStatus($id)
	{
		$uid = Yii::app()->user->id;
		$user=User::model()->find(array('condition'=>'id="'.$uid.'"'));
		$userSchool = $user->school_id;
	  	$blueprintschool = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='.$id.' AND TM_BPS_School_Id='.$userSchool));
	  	$blueprintschool->TM_BPS_Status = 1;
	  	$blueprintschool->save(false);

	  	if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionChangeStatusInactive($id)
	{
		$uid = Yii::app()->user->id;
		$user=User::model()->find(array('condition'=>'id="'.$uid.'"'));
		$userSchool = $user->school_id;
	  	$blueprintschool = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='.$id.' AND TM_BPS_School_Id='.$userSchool));
	  	$blueprintschool->TM_BPS_Status = 0;
	  	$blueprintschool->save(false);

	  	if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

	}
	
	public function actionDuplicate($id)
	{
		$userid = Yii::app()->user->id;
        $user=User::model()->find(array('condition'=>'id="'.$userid.'"'));
		$oldblueprint = Blueprints::model()->findByPk($id);
		
		$bluePrint = new Blueprints();
		$bluePrint->TM_BP_Name = $oldblueprint->TM_BP_Name;
		$bluePrint->TM_BP_Description = $oldblueprint->TM_BP_Description;
		$bluePrint->TM_BP_Syllabus_Id = $oldblueprint->TM_BP_Syllabus_Id;
		$bluePrint->TM_BP_Standard_Id = $oldblueprint->TM_BP_Standard_Id;
		$bluePrint->TM_BP_Totalmarks = $oldblueprint->TM_BP_Totalmarks;
		$bluePrint->TM_BP_Status = $oldblueprint->TM_BP_Status;
		$bluePrint->TM_BP_CreatedBy = $userid;
		if($bluePrint->save(false))
		{
			$assignedSchools = new BlueprintSchool();
			$assignedSchools->TM_BPS_Blueprint_Id = $bluePrint->TM_BP_Id;
			$assignedSchools->TM_BPS_School_Id = $user->school_id;
			$assignedSchools->TM_BPS_Status = 0;
			$assignedSchools->save(false);
			
			$templates = blueprintTemplate::model()->findAll(array('condition'=>'TM_BPT_Blueprint_Id='.$id));
			foreach($templates as $template){
		        $topics = BlueprintTopics::model()->findAll(array('condition'=>'TM_BP_TP_Template_Id='.$template->TM_BPT_Id));				
			  	$blueprintTemplate = new blueprintTemplate();
				$blueprintTemplate->TM_BPT_Blueprint_Id = $bluePrint->TM_BP_Id;
				$blueprintTemplate->TM_BPT_Chapter_Id = $template->TM_BPT_Chapter_Id;
				$blueprintTemplate->TM_BPT_Mark_type = $template->TM_BPT_Mark_type;
				$blueprintTemplate->TM_BPT_No_questions = $template->TM_BPT_No_questions;
				
				if($blueprintTemplate->save(false)){
					foreach($topics as $topic){
                        $blueprintTopic = new BlueprintTopics();
						$blueprintTopic->TM_BP_TP_Template_Id = $blueprintTemplate->TM_BPT_Id;
						$blueprintTopic->TM_BP_TP_Chapter_Id = $topic->TM_BP_TP_Chapter_Id;
						$blueprintTopic->TM_BP_TP_Topic_Id = $topic->TM_BP_TP_Topic_Id;
						$blueprintTopic->save(false);
					}					
				}
			}
		}
		$this->redirect(array('update','id'=>$bluePrint->TM_BP_Id));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Blueprints');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
			$this->layout = '//layouts/schoolcolumn2';
		endif;
		$userid = Yii::app()->user->id;
        $user=User::model()->find(array('condition'=>'id="'.$userid.'"'));
		if(Yii::app()->user->isAdmin()){
			///echo "yes";exit;
			$model=new Blueprints('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Blueprints']))
				$model->attributes=$_GET['Blueprints'];
		}
		else if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()){
		 $blueprintschools = BlueprintSchool::model()->findAll(array('condition'=>"TM_BPS_School_Id='".$user->school_id."'"));
		 $newid=array();
		 foreach($blueprintschools as $key=>$blueprintschool){
		  $blueprint[] = Blueprints::model()->findByPk($blueprintschool->TM_BPS_Blueprint_Id);
		  $newid[] = $blueprint[$key]->TM_BP_Id;
		 }
		 ///$value = implode(',', $newid);
		 $model=new Blueprints('schoolsearch');
			if(isset($_GET['Blueprints']))
				$model->attributes=$_GET['Blueprints'];
		 $id = $newid;
		// print_r($id); exit;
	   }
		$this->render('admin',array(
			'model'=>$model,
			'id'=>$id,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Blueprints the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Blueprints::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionGetStandard()
	{
		$syllabus = $_POST['syllabus'];
		$standards = Standard::model()->findAll(array('condition'=>'TM_SD_Syllabus_Id='.$syllabus));
		$html ='';
		if(count($standards) > 0)
		{
			foreach($standards as $standard)
			{
				$selected = '';
				if(isset($_POST['selecteStandard']) && $_POST['selecteStandard'] ==  $standard->TM_SD_Id)
					$selected = 'selected';
				$html .='<option value='.$standard->TM_SD_Id.' '.$selected.'>'.$standard->TM_SD_Name.'</option>';
			}
		}
		$array = array('result'=>$html, 'error' => 'No');
		echo json_encode($array);
	}

public function actionGetSecondPart()
	{
		$standard = $_POST['standard'];
		// echo $standard;
		$chapters = Chapter::model()->findAll(array('condition'=>'TM_TP_Standard_Id='.$standard.' AND TM_TP_Status=0', 'order'=>'TM_TP_order ASC'));
		$marks = TmAvailableMarks::model()->findAll(array('condition'=>'TM_AV_MK_Standard_Id='.$standard.' AND status=0'));
		$html ='';
		if(count($chapters) > 0)
		{

		$html .='<table style="width:100%;box-sizing: border-box;table-layout: fixed;">
    	  		<tr>
		    	<th style="width:250px;"></th>';
			if(count($marks) > 0)
			{
				foreach ($marks as $mark)
				{
                    ///$html .='<td>'.$mark->TM_AV_MK_Mark.'</td>';
                   $html .='<th style="width:100px;">'.$mark->TM_AV_MK_Mark.'-Mark</th>
                   <input type="hidden" name="avMarkIds[]" value ='.$mark->TM_AV_MK_Id.' >';
				}
			}

		$html .='</tr>
		';


		$html .='<tr>
		<th style="width:250px;">
		Total no of questions: <span id="totalNoOfQuest"></span><br>
		Total marks: <span id="totalMark"></span>
		</th>';
			if(count($marks) > 0)
			{
				foreach ($marks as $mark)
				{
                    ///$html .='<td>'.$mark->TM_AV_MK_Mark.'</td>';
                   $html .='<th style="text-align:center;width:100px;"><span id="QuestionsInRow"></span><br>
                   <span id="markInRow"></span>
                   </th>';
				}
			}

		$html .='</tr>
		';
			foreach($chapters AS $chapter)
			{
				$html .='<tr style="background-color:#f6f6f6;border-bottom: 2px solid #d7d7d7;"><td style="padding-left: 10px;width:250px;">'.$chapter->TM_TP_Name.'</td>
					<input type="hidden" name="avChapters[]" value='.$chapter->TM_TP_Id.'>
				';
				foreach ($marks as $mark)
				{
					$html .='<td style="padding: 10px;width:100px;"><input type="text" class="allowedNumbers" data-chapter="'.$chapter->TM_TP_Id.'" data-mark="'.$mark->TM_AV_MK_Mark.'" data-id="'.$mark->TM_AV_MK_Id.'" name="allowedNumbers'.$chapter->TM_TP_Id.'-'.$mark->TM_AV_MK_Id.'" style="width: 40px;border: 1px solid #ff990045;text-align:center;">
                        <button type="button" style="width: 22px;
												    height: 22px;
												    padding: 6px 0;
												    border-radius: 15px;
												    text-align: center;
												    font-size: 8px;
												    line-height: 1.428571429;" class="btn btn-default btn-circle topic-toggable" id="quicktoggleheader1" data-target="#quickfeature-'.$mark->TM_AV_MK_Id.'-'.$chapter->TM_TP_Id.'" aria-expanded="true">
                            <i class="fa fa-chevron-down" style=""></i>
                        </button></td>';	
					
				}
			  	$html .='</tr><tr style="border-bottom: 2px solid #d7d7d7;"><td colspan=15>';
				foreach ($marks as $mark)
				{
					$html .='<div style="display:none" class="topics-toggle-list" id="quickfeature-'.$mark->TM_AV_MK_Id.'-'.$chapter->TM_TP_Id.'"
							 aria-expanded="true">
							<div class="row">
								<div class="col-md-12" style="padding-top: 17px;">
									<button type="button" data-chapter='.$chapter->TM_TP_Id.' data-id='.$mark->TM_AV_MK_Id.' class="btn btn-xs uncheckall" style="margin-left: 15px;background-color:white;" id="uncheckall"
											href="javascript:void(0)">
										Clear all
									</button>
									<button type="button" data-chapter='.$chapter->TM_TP_Id.' data-id='.$mark->TM_AV_MK_Id.' class="btn btn-xs checkall" style="margin-left: 15px;background-color:white;" id="checkall"
											href="javascript:void(0)">
										Select all
									</button>
								</div>
							<div class="col-xs-12 col-md-4">
							<ul class="listnew" style="list-style: none;">';
					$topics = Topic::model()->findAll(array('condition'=>'	TM_SN_Topic_Id='.$chapter->TM_TP_Id));
					if(count($topics) > 0)
					{
					
						foreach ($topics as $topic){

							$html .='<li>
										<input type="checkbox" class="inputtopic-'.$mark->TM_AV_MK_Id.'-'.$chapter->TM_TP_Id.' inputtp-'.$mark->TM_AV_MK_Id.'"
												name="quicktopic' . $chapter->TM_TP_Id .'-'.$mark->TM_AV_MK_Id. '[]"
												checked="checked"
												value="'.$topic->TM_SN_Id.'">
											'.$topic->TM_SN_Name.'
									</li>';

						} 
					}                     
										
								$html .='</ul>
							</div>
						</div>
					</div>';
				}
				$html .='</td></tr>';
			}
			$html .='</table>';
		}
		$array = array('result'=>$html, 'error' => 'No');
		echo json_encode($array);
	}

	public function actionGetSecondPartEdit()
	{
		$standard = $_POST['standard'];
		$bluePrintId = $_POST['bluePrintId'];
		// echo $standard;
		$chapters = Chapter::model()->findAll(array('condition'=>'TM_TP_Standard_Id='.$standard.' AND TM_TP_Status=0', 'order'=>'TM_TP_order ASC'));
		$marks = TmAvailableMarks::model()->findAll(array('condition'=>'TM_AV_MK_Standard_Id='.$standard.' AND status=0'));
		// var_dump($marks);exit;
		$html ='';
		if(count($chapters) > 0)
		{

		$html .='<table style="width:100%;box-sizing: border-box;table-layout: fixed;">
				<input type="hidden" name="bluePrintId" value="'.$bluePrintId.'">
    	  		<tr>
		    	<th style="width:250px;"></th>';
			if(count($marks) > 0)
			{
				foreach ($marks as $mark)
				{
                    //$html .='<td>'.$mark->TM_AV_MK_Mark.'</td>';
                   $html .='<th style="width:100px;">'.$mark->TM_AV_MK_Mark.'-Mark</th>
                   <input type="hidden" name="avMarkIds[]" value ='.$mark->TM_AV_MK_Id.' >
                   ';
				}
			}
		$html .='</tr>';



		$html .='<tr>
		<th style="width:250px;">
		Total no of questions: <span id="totalNoOfQuest"></span><br>
		Total marks: <span id="totalMark"></span>
		</th>';
			if(count($marks) > 0)
			{
				foreach ($marks as $mark)
				{
                    ///$html .='<td>'.$mark->TM_AV_MK_Mark.'</td>';
                   $html .='<th style="text-align:center;width:100px;"><span id="QuestionsInRow"></span><br>
                   <span id="markInRow"></span>
                   </th>';
				}
			}

		$html .='</tr>
		';

		
			foreach($chapters AS $chapter)
			{
				$html .='<tr style="background-color:#f6f6f6;border-bottom: 2px solid #d7d7d7;"><td style="padding-left: 10px;width:250px;">'.$chapter->TM_TP_Name.'</td>
				<input type="hidden" name="avChapters[]" value='.$chapter->TM_TP_Id.'>';
				foreach ($marks as $mark)
				{
					$value = blueprintTemplate::model()->find(array('condition'=>'TM_BPT_Blueprint_Id='.$bluePrintId.' AND TM_BPT_Chapter_Id='.$chapter->TM_TP_Id.' AND TM_BPT_Mark_type='.$mark->TM_AV_MK_Id));	
					// var_dump($value);exit
					// $html .='<div class="col-md-1"><input type="number" class="allowedNumbers" data-valueid="'.$value->TM_BPT_Id.'" value="'.$value->TM_BPT_No_questions.'" data-chapter="'.$chapter->TM_TP_Id.'" data-id="'.$mark->TM_AV_MK_Id.'" name="allowedNumbers" style="width: 40px">
     //                    <button type="button" class="btn btn-default btn-circle" id="quicktoggleheader1" data-toggle="collapse" data-target="#quickfeature-'.$mark->TM_AV_MK_Id.'-'.$chapter->TM_TP_Id.'" aria-expanded="true">
     //                        <i class="fa fa-chevron-down"></i>
     //                    </button>
					// </div>';

					$html .='<td style="padding: 10px;width:100px;"><input type="text" class="allowedNumbers" data-valueid="'.$value->TM_BPT_Id.'" value="'.$value->TM_BPT_No_questions.'" data-chapter="'.$chapter->TM_TP_Id.'" data-mark="'.$mark->TM_AV_MK_Mark.'" data-id="'.$mark->TM_AV_MK_Id.'" name="allowedNumbers'.$chapter->TM_TP_Id.'-'.$mark->TM_AV_MK_Id.'" style="width: 40px;border: 1px solid #ff990045;text-align:center;">

						<input type="hidden" name="templateId'.$chapter->TM_TP_Id.'-'.$mark->TM_AV_MK_Id.'" value="'.$value->TM_BPT_Id.'">
                        <button type="button" style="width: 22px;
    height: 22px;
    padding: 6px 0;
    border-radius: 15px;
    text-align: center;
    font-size: 8px;
    line-height: 1.428571429;" class="btn btn-default btn-circle topic-toggable" id="quicktoggleheader1" data-target="#quickfeature-'.$mark->TM_AV_MK_Id.'-'.$chapter->TM_TP_Id.'" aria-expanded="true">
                            <i class="fa fa-chevron-down" style=""></i>
                        </button></td>';	
					
					
					
				}
				 $html .='</tr><tr style="border-bottom: 2px solid #d7d7d7;"><td colspan=15>';
				foreach ($marks as $mark)
				{
					$html .='<div style="display:none;" id="quickfeature-'.$mark->TM_AV_MK_Id.'-'.$chapter->TM_TP_Id.'"
							class="topics-toggle-list" aria-expanded="true">
						<div class="row">
							<div class="col-md-12" style="padding-top: 17px;">
								<button type="button" data-chapter='.$chapter->TM_TP_Id.' data-id='.$mark->TM_AV_MK_Id.' class="btn btn-xs uncheckall" style="margin-left: 15px;background-color:white;" id="uncheckall"
										href="javascript:void(0)">
									Clear all
								</button>
								<button type="button" data-chapter='.$chapter->TM_TP_Id.' data-id='.$mark->TM_AV_MK_Id.' class="btn btn-xs checkall inputtp-'.$mark->TM_AV_MK_Id.'" style="margin-left: 15px;background-color:white;" id="checkall"
										href="javascript:void(0)">
									Select all
								</button>
							</div>
							<div class="col-xs-12 col-md-4">
							<ul class="listnew" style="list-style: none;">';
					$topics = Topic::model()->findAll(array('condition'=>'	TM_SN_Topic_Id='.$chapter->TM_TP_Id));
					if(count($topics) > 0)
					{
						$templateId = blueprintTemplate::model()->find(array('condition'=>'TM_BPT_Blueprint_Id='.$bluePrintId.' AND TM_BPT_Chapter_Id='.$chapter->TM_TP_Id.' AND TM_BPT_Mark_type='.$mark->TM_AV_MK_Id));
						foreach ($topics as $topic){
							// echo 'TM_BP_TP_Template_Id='.$templateId->TM_BPT_Id.' AND TM_BP_TP_Chapter_Id='.$chapter->TM_TP_Id.' AND TM_BP_TP_Topic_Id='.$topic->TM_SN_Id; 
							// exit;
							if($templateId->TM_BPT_Id !='')
							{
								$isTopicExist =  BlueprintTopics::model()->find(array('condition'=>'TM_BP_TP_Template_Id='.$templateId->TM_BPT_Id.' AND TM_BP_TP_Chapter_Id='.$chapter->TM_TP_Id.' AND TM_BP_TP_Topic_Id='.$topic->TM_SN_Id));
								if($isTopicExist)
									$checked = 'checked="checked"';
								else
									$checked = '';
							}
							else
								$checked = 'checked="checked"';
							$html .='<li>
										<input type="checkbox" class="inputtopic-'.$mark->TM_AV_MK_Id.'-'.$chapter->TM_TP_Id.' inputtp-'.$mark->TM_AV_MK_Id.'"
												name="quicktopic' . $chapter->TM_TP_Id .'-'.$mark->TM_AV_MK_Id. '[]" 
												'.$checked.'
												value="'.$topic->TM_SN_Id.'">
											'.$topic->TM_SN_Name.'
										</li>';

						} 
					}                     
										
								$html .='</ul>
							</div>
						</div>
					</div>';
				}
				$html .='</td></tr>';
			}
			$html .='</table>';
		}
		$array = array('result'=>$html, 'error' => 'No');
		echo json_encode($array);
	}
	public function actionSave()
	{
		// print_r($_POST['schools']);exit;
		//echo $_POST['schools']['0']; exit;
		$bluePrint = new Blueprints();
		$bluePrint->TM_BP_Name = $_POST['name'];
		$bluePrint->TM_BP_Description = $_POST['description'];
		$bluePrint->TM_BP_Syllabus_Id = $_POST['syllabus'];
		$bluePrint->TM_BP_Standard_Id = $_POST['standard'];
		$bluePrint->TM_BP_Status = $_POST['status'];
		$bluePrint->TM_BP_Availability = $_POST['availability'];
		$bluePrint->TM_BP_CreatedBy = Yii::app()->user->id;
		if($bluePrint->save(false))
		{
			if($_POST['availability'] != 0) {
				// var_dump($_POST['schools']);exit;
				if(!isset($_POST['schools']) && isset($_POST['school_id'])) {
					$assignedSchools = new BlueprintSchool();
					$assignedSchools->TM_BPS_Blueprint_Id = $bluePrint->TM_BP_Id;
					$assignedSchools->TM_BPS_School_Id = $_POST['school_id'];
					$assignedSchools->TM_BPS_Status = 0;
					$assignedSchools->save(false);
				}
				else
				{
					if(count($_POST['schools'])>1){
						foreach($_POST['schools'] AS $school)
						{
							$assignedSchools = new BlueprintSchool();
							$assignedSchools->TM_BPS_Blueprint_Id = $bluePrint->TM_BP_Id;
							$assignedSchools->TM_BPS_School_Id = $school;
							$assignedSchools->TM_BPS_Status = 0;
							$assignedSchools->save(false);
						}
					}
					else{
						$assignedSchools = new BlueprintSchool();
						$assignedSchools->TM_BPS_Blueprint_Id = $bluePrint->TM_BP_Id;
						$assignedSchools->TM_BPS_School_Id = $_POST['schools'][0];
						$assignedSchools->TM_BPS_Status = 0;
						$assignedSchools->save(false);
					}
				}
			}
			foreach ($_POST['avChapters'] as $chapter) {
				foreach ($_POST['avMarkIds'] as $mark) {
					// // echo 'allowedNumbers'.$chapter.'-'.$mark;
					// // echo $_POST['allowedNumbers'.$chapter.'-'.$mark];

					// exit;
					if($_POST['allowedNumbers'.$chapter.'-'.$mark] != null)
					{

						$blueprintTemplate = new blueprintTemplate();
						$blueprintTemplate->TM_BPT_Blueprint_Id = $bluePrint->TM_BP_Id;
						$blueprintTemplate->TM_BPT_Chapter_Id = $chapter;
						$blueprintTemplate->TM_BPT_Mark_type = $mark;
						$blueprintTemplate->TM_BPT_No_questions = $_POST['allowedNumbers'.$chapter.'-'.$mark];
						if($blueprintTemplate->save(false))
						{
							foreach ($_POST['quicktopic'.$chapter.'-'.$mark] as $topic) {

								$blueprintTopic = new BlueprintTopics();
								$blueprintTopic->TM_BP_TP_Template_Id = $blueprintTemplate->TM_BPT_Id;
								$blueprintTopic->TM_BP_TP_Chapter_Id = $chapter;
								$blueprintTopic->TM_BP_TP_Topic_Id = $topic;
								$blueprintTopic->TM_BP_TP_Blueprint_Id = $bluePrint->TM_BP_Id;
								$blueprintTopic->save(false);
							}
						}
					}
				}

			}




			$array = array('bluePrintId'=>$bluePrint->TM_BP_Id, 'error' => 'No');
			echo json_encode($array);
		}

	}
	public function actionUpdateBluePrint()
	{
		// print_r($_POST['schools']);exit;
		$bluePrint = $this->loadModel($_POST['bluePrintId']);
		$bluePrint->TM_BP_Name = $_POST['name'];
		$bluePrint->TM_BP_Description = $_POST['description'];
		$bluePrint->TM_BP_Syllabus_Id = $_POST['syllabus'];
		$bluePrint->TM_BP_Standard_Id = $_POST['standard'];
		$bluePrint->TM_BP_Status = $_POST['status'];
		$bluePrint->TM_BP_Availability = $_POST['availability'];
		// $bluePrint->TM_BP_CreatedBy = Yii::app()->user->id;
		if($bluePrint->save(false))
		{
			// echo "success";exit;
			if($_POST['availability'] == 0){

				BlueprintSchool::model()->deleteAll(array('condition'=>'TM_BPS_Blueprint_Id='.$_POST['bluePrintId']));
			}
			else
			{
				if(isset($_POST['schools']) && $_POST['schools']!=''){
					//echo count($_POST['schools']);exit;
					if(count($_POST['schools'])>0){
						//echo "yes";exit;
						//print_r($_POST['schools']);exit;
						$assignedSchools = BlueprintSchool::model()->findAll(array('condition'=>'TM_BPS_Blueprint_Id='.$_POST['bluePrintId']));
						foreach($assignedSchools AS $assignedSchool):
							$assignedSchool->TM_BPS_Status = 1;
							$assignedSchool->save(false);
						endforeach;
						foreach($_POST['schools'] AS $school)
						{
							//echo "yes";exit;
							$isExist = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='.$_POST['bluePrintId'].' AND TM_BPS_School_Id='.$school));
							if(!$isExist)
							{
								$assignedSchools = new BlueprintSchool();
								$assignedSchools->TM_BPS_Blueprint_Id = $bluePrint->TM_BP_Id;
								$assignedSchools->TM_BPS_School_Id = $school;
								$assignedSchools->TM_BPS_Status = 0;
								$assignedSchools->save(false);
							}
							else
							{
								if($isExist->TM_BPS_Status == 1)
								{
									$isExist->TM_BPS_Status = 0;
									$isExist->save(false);
								}
							}
						}
					}
					else{
						//echo $_POST['schools'];exit;
						$isExist = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='.$_POST['bluePrintId'].' AND TM_BPS_School_Id='.$_POST['schools']));
						if(!$isExist)
						{
							$assignedSchools = new BlueprintSchool();
							$assignedSchools->TM_BPS_Blueprint_Id = $bluePrint->TM_BP_Id;
							$assignedSchools->TM_BPS_School_Id = $_POST['schools'];
							$assignedSchools->TM_BPS_Status = 0;
							$assignedSchools->save(false);
						}
						else
						{
							if($isExist->TM_BPS_Status == 1)
							{
								$isExist->TM_BPS_Status = 0;
								$isExist->save(false);
							}
						}

					}
				}


				else{

					$assignedSchools = BlueprintSchool::model()->find(array('condition'=>'TM_BPS_Blueprint_Id='.$_POST['bluePrintId']));
					$assignedSchools->TM_BPS_Status = 1;
					$assignedSchools->save(false);
				}
			}


			foreach ($_POST['avChapters'] as $chapter) {
				foreach ($_POST['avMarkIds'] as $mark) {
					if($_POST['allowedNumbers'.$chapter.'-'.$mark] != null)
					{
						$templateId = $_POST['templateId'.$chapter.'-'.$mark];

						if($templateId != null)
							$blueprintTemplate = blueprintTemplate::model()->findByPk($templateId);
						else
							$blueprintTemplate = new blueprintTemplate();

						$blueprintTemplate->TM_BPT_Blueprint_Id = $bluePrint->TM_BP_Id;
						$blueprintTemplate->TM_BPT_Chapter_Id = $chapter;
						$blueprintTemplate->TM_BPT_Mark_type = $mark;
						$blueprintTemplate->TM_BPT_No_questions = $_POST['allowedNumbers'.$chapter.'-'.$mark];

						if($blueprintTemplate->save(false))
						{

							BlueprintTopics::model()->deleteAll(array('condition'=>'TM_BP_TP_Template_Id='.$blueprintTemplate->TM_BPT_Id.' AND TM_BP_TP_Chapter_Id='.$chapter.' AND TM_BP_TP_Blueprint_Id='.$bluePrint->TM_BP_Id));

							foreach ($_POST['quicktopic'.$chapter.'-'.$mark] as $topic) {
								$blueprintTopic = new BlueprintTopics();
								$blueprintTopic->TM_BP_TP_Template_Id = $blueprintTemplate->TM_BPT_Id;
								$blueprintTopic->TM_BP_TP_Chapter_Id = $chapter;
								$blueprintTopic->TM_BP_TP_Topic_Id = $topic;
								$blueprintTopic->TM_BP_TP_Blueprint_Id = $bluePrint->TM_BP_Id;
								$blueprintTopic->save(false);
							}
						}
					}
					else{
						$templateId = $_POST['templateId'.$chapter.'-'.$mark];
						//echo $templateId;exit;
						if($templateId != null):
							$blueprintTemplate = blueprintTemplate::model()->findByPk($templateId);
							$blueprintTemplate->delete();
						endif;
					}
				}

			}




			$array = array('bluePrintId'=>$bluePrint->TM_BP_Id, 'error' => 'No');
			echo json_encode($array);
		}

	}
	public function actionSaveNumberOfQuestion()
	{
		//echo "test";exit;
		$blueprintTemplate = new blueprintTemplate();
		$blueprintTemplate->TM_BPT_Blueprint_Id = $_POST['bluePrintId'];
		$blueprintTemplate->TM_BPT_Chapter_Id = $_POST['chapterId'];
		$blueprintTemplate->TM_BPT_Mark_type = $_POST['markId'];
		$blueprintTemplate->TM_BPT_No_questions = $_POST['noOfQuestions'];
		if($blueprintTemplate->save(false))
		{
			$array = array('templateId'=>$blueprintTemplate->TM_BPT_Id,'bluePrintId'=>$_POST['bluePrintId'],'chapterId'=>$_POST['chapterId'],'markId' => $_POST['markId'],  'error' => 'No');
			echo json_encode($array);
		}
		
		
	}
	public function actionUpdateNumberOfQuestion()
	{
		if($_POST['valueId'] != '')
			$blueprintTemplate = blueprintTemplate::model()->findByPk($_POST['valueId']);
		else
			$blueprintTemplate = new blueprintTemplate();
		$blueprintTemplate->TM_BPT_Blueprint_Id = $_POST['bluePrintId'];
		$blueprintTemplate->TM_BPT_Chapter_Id = $_POST['chapterId'];
		$blueprintTemplate->TM_BPT_Mark_type = $_POST['markId'];
		$blueprintTemplate->TM_BPT_No_questions = $_POST['noOfQuestions'];
		if($blueprintTemplate->save(false))
		{
			$array = array('templateId'=>$blueprintTemplate->TM_BPT_Id,'bluePrintId'=>$_POST['bluePrintId'],'chapterId'=>$_POST['chapterId'],'markId' => $_POST['markId'],  'error' => 'No');
			echo json_encode($array);
		}
	}

	public function actionSaveTopic()
	{
		$blueprintTopic = new BlueprintTopics();
		$blueprintTopic->TM_BP_TP_Template_Id = $_POST['templateId'];
		$blueprintTopic->TM_BP_TP_Chapter_Id = $_POST['chapterId'];
		$blueprintTopic->TM_BP_TP_Topic_Id = $_POST['topic'];
		$blueprintTopic->TM_BP_TP_Blueprint_Id = $_POST['bluePrintId'];
		if($blueprintTopic->save(false))
		{
			$array = array('error'=> 'No');
			echo json_encode($array);
		}
				
	}
	public function actionUpdateTopic()
	{
		$isExist = BlueprintTopics::model()->find(array('condition'=>'TM_BP_TP_Template_Id='.$_POST['templateId'].' AND TM_BP_TP_Chapter_Id='.$_POST['chapterId'].' AND TM_BP_TP_Topic_Id='.$_POST['topic']));
		if(!$isExist)
		{
			$blueprintTopic = new BlueprintTopics();
			$blueprintTopic->TM_BP_TP_Template_Id = $_POST['templateId'];
			$blueprintTopic->TM_BP_TP_Chapter_Id = $_POST['chapterId'];
			$blueprintTopic->TM_BP_TP_Topic_Id = $_POST['topic'];
			$blueprintTopic->TM_BP_TP_Blueprint_Id = $_POST['bluePrintId'];
			if($blueprintTopic->save(false))
				$array = array('error'=> 'No');
			else
				$array = array('error'=> 'Yes');
		}
		else
		{
			$array = array('error'=> 'No');
		}
		echo json_encode($array);
				
	}

	public function actionBlueprintschools()
	{
		$blueprints=Blueprints::model()->findAll(array('condition'=>'TM_BP_Availability=3'));
		//echo count($blueprints);
		foreach($blueprints AS $blueprint):
			$bpschools=BlueprintSchool::model()->findAll(array('condition'=>'TM_BPS_Blueprint_Id='.$blueprint['TM_BP_Id'].' '));
			$school=School::model()->findByPk($bpschools[0]['TM_BPS_School_Id'])->TM_SCL_Name;
			echo "bpid: ".$blueprint['TM_BP_Id']." count: ".count($bpschools)."<br>";
		endforeach;
		//echo "updated";
	}

	/**
	 * Performs the AJAX validation.
	 * @param Blueprints $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='blueprints-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
