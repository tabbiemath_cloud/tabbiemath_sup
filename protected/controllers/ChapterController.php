<?php

class ChapterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('GetstandardQuestion'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','create','update','admin','delete','Getstandard','Order'),
                'users'=>UserModule::getSuperAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
    public function actionGetstandard()
    {
          if(isset($_POST['id']))
          {
              $data=Standard::model()->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0'",
                  array(':syllabus'=>(int) $_POST['id']));

              $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
/*              if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
              endif;*/
              echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
              foreach($data as $value=>$name)
              {
                  echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
              }
          }
    }
    public function actionGetstandardQuestion()
    {
          if(isset($_POST['id']))
          {
                    $data=Standard::model()->with(array(
                        'admins'=>array(
                            // we don't want to select posts
                            'select'=>false,
                            // but want to get only users with published posts
                            'joinType'=>'LEFT JOIN',
                            'condition'=>'admins.TM_SA_Admin_Id='.Yii::app()->user->id,
                        ),
                    ))->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0'",array(':syllabus'=>(int) $_POST['id']));                
                $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
/*              if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
              endif;*/
                echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
                foreach($data as $value=>$name)
                {
                  echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                }
          }
    }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Chapter;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Chapter']))
		{
			$model->attributes=$_POST['Chapter'];
			if($model->save()){
                $criteria = new CDbCriteria;
                $criteria->order='TM_TP_order DESC';
                $order=Chapter::model()->find($criteria)->TM_TP_order;
                $model->TM_TP_order=$order+1;
                $model->save(false);
                $this->redirect(array('view','id'=>$model->TM_TP_Id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Chapter']))
		{
			$model->attributes=$_POST['Chapter'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_TP_Id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Chapter');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Chapter('search');
/*		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Chapter']))
			$model->attributes=$_GET['Chapter'];*/

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Chapter the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Chapter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Chapter $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='chapter-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actions()
    {
        return array(
            'order' => array(
                'class' => 'ext.OrderColumn.OrderAction',
                'modelClass' => 'Chapter',
                'pkName'  => 'TM_TP_Id',
            ),
        );
    }    
}
