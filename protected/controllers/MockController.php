<?php

class MockController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('Printmock','Printmocksolution','deletefile','Sortorder','Consolidatedreport','Schoolplan'),
				'users'=>array('@'),
			),
			/*array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),*/
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','Managequestions','create','update','index','view','getquestions','Addquestion','Removequestion','MockOrder','SaveStatus','Reordermock','Tags','Createtags','Viewtags','Updatetags','Deletetags','Exportworksheet','Exportworksheetcsv','Importworksheet','Importworksheetcsv','GetOptions','Qstnoptions','AddSection','GetHeaderCustom','Deleteheader'),
                'users'=>UserModule::getQuestionAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate()
    {
        $model=new Mock;
        $model->scenario = 'Insert';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Mock']))
        { 
            $model->attributes=$_POST['Mock'];
            $model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
            $model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
            $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
            /*if($model->TM_MK_Availability=='3'):
                $model->TM_MK_Availability='1';
            endif;*/
            $model->TM_MK_Sort_Order = $this->GetSortorder($_POST['Mock']['TM_MK_Standard_Id'],$_POST['Mock']['TM_MK_Resource']);
            $model->TM_MK_Resource=$_POST['Mock']['TM_MK_Resource'];
            $model->TM_MK_Link_Resource=$_POST['Mock']['TM_MK_Link_Resource'];
            if($model->save()):
                if($model->TM_MK_Availability!='0' && $model->TM_MK_Availability!=''):
                    $insertarray=array();
                    for($i=0;$i<count($model->schools);$i++):
                        $insertarray[$i]=array('TM_MS_Mock_Id'=>$model->TM_MK_Id,'TM_MS_School_Id'=>$model->schools[$i]);
                    endfor;
                    $builder=Yii::app()->db->schema->commandBuilder;
                    $command=$builder->createMultipleInsertCommand('tm_mock_school',$insertarray );
                    $command->execute();
                endif;
                $imageName = $_FILES["Mock"]["name"]["TM_MK_Worksheet"];
                if($imageName!=''):
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $exten=explode('.', $imageName);
                    $uniqueName="Worksheet".$mockid.'.'.$exten[1];
                    $model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
                    $model->TM_MK_Worksheet->saveAs(Yii::app()->params['uploadPath'].$uniqueName);
                    $model->TM_MK_Worksheet = $uniqueName;
                endif;
                $solutionimageName = $_FILES["Mock"]["name"]["TM_MK_Solution"];
                if($solutionimageName!=''):
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $solexten=explode('.', $imageName);
                    $uniqueName=$mockname.$mockid.'.'.$exten[1];
                    $uniquesolutionName="Worksheet".$mockid.'solution.'.$solexten[1];
                    $model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
                    $model->TM_MK_Solution->saveAs(Yii::app()->params['uploadPath'].$uniquesolutionName);
                    $model->TM_MK_Solution = $uniquesolutionName;
                endif;

                if(isset($_POST['Mock']['TM_MK_Video_Url']))
                { 

                        $rx = 
                            '~ 
                            ^(?:https?://)?                         
                            (?:www[.])?                            
                            (?:youtube[.]com/watch[?]v=|youtu[.]be/) 
                            ([^&]{11})~x'; 
                            $youtube_url = $_POST['Mock']['TM_MK_Video_Url'];
                            $has_match = preg_match($rx, $youtube_url, $matches);

                            if($matches[0]!='')
                                    {
                                        $video_id = explode("?v=", $youtube_url);
                                        $video_id = $video_id[1]; 
                                        $thumbnail_url='https://img.youtube.com/vi/'.$video_id.'/hqdefault.jpg';
                                        $model->TM_MK_Thumbnail = $thumbnail_url;
                                    } 
                                   
                }
                $thumbnailimageName = $_FILES["Mock"]["name"]["TM_MK_Thumbnail"];
                if($thumbnailimageName!=''):
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $thumbexten=explode('.', $imageName);
                    $uniquethumbName="Worksheet".$mockid.'thumbnail.'.$thumbexten[1];
                    $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
                    $model->TM_MK_Thumbnail->saveAs(Yii::app()->params['uploadPath'].$uniquethumbName);
                    $model->TM_MK_Thumbnail = $uniquethumbName;
                endif;
                $model->save();
                $this->redirect(array('managequestions','id'=>$model->TM_MK_Id));
            endif;
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }
	public function GetSortorder($standard,$type)
	{
		$modelupper=Mock::model()->find(array(
				'condition' => 'TM_MK_Standard_Id=:standard AND TM_MK_Resource=:type ',
				'limit' => 1,
				'order'=>'TM_MK_Sort_Order DESC',
				'params' => array(':standard'=>$standard,':type'=>$type)
			)
		); 
		return $modelupper->TM_MK_Sort_Order+1;		
	}
    public function actionManagequestions($id)
    {
        $model=$this->loadModel($id);
        $mockquestions=MockQuestions::model()->findAll(array('condition'=>"TM_MQ_Mock_Id='".$id."'",'order'=>'TM_MQ_Order ASC'));
        if(count($mockquestions)>0):
            $MQS='';
            foreach($mockquestions AS $key=>$MQ)
            {
                if($key=='0'):
                    $MQS.="'".$MQ->TM_MQ_Question_Id."'";
                else:
                    $MQS.=",'".$MQ->TM_MQ_Question_Id."'";
                endif;
            }
            $addedQuestion=$mockquestions;
            $condition="AND TM_QN_Id NOT IN (".$MQS.")";
        else:
            $addedQuestion='';
            $condition='';
        endif;
        //$questions=Questions::model()->findAll(array('condition'=>"TM_QN_Publisher_Id='".$model->TM_MK_Publisher_Id."' AND TM_QN_Syllabus_Id='".$model->TM_MK_Syllabus_Id."'  AND TM_QN_Parent_Id='0' AND TM_QN_Standard_Id='".$model->TM_MK_Standard_Id."' AND TM_QN_Status='0' ".$condition));
        $mockqstcount=count(MockQuestions::model()->findAll(array('condition'=>"TM_MQ_Mock_Id='".$id."'")));
        $this->render('managequestions',array(
            'model'=>$model,
            //'questions'=>$questions,
            'addedQuestion'=>$addedQuestion,
            'questioncount'=>$mockqstcount
        ));
    }
    public function actionGetquestions()
    {
        $condition='';
        if($_POST['chapter']!=''):
            $condition.=" AND TM_QN_Topic_Id='".$_POST['chapter']."'";
        endif;
        if($_POST['topic']!=''):
            $condition.=" AND TM_QN_Section_Id='".$_POST['topic']."'";
        endif;
        if($_POST['type']!=''):
            $condition.=" AND TM_QN_Type_Id='".$_POST['type']."'";
        endif;
        if($_POST['reference']!=''): 
            $condition.=" AND TM_QN_QuestionReff LIKE '%".$_POST['reference']."%'";
        endif;
        if($_POST['pattern']!=''):
            $condition.=" AND TM_QN_Pattern LIKE '%".$_POST['pattern']."%'";
        endif;
        if($_POST['dificulty']!=''):
            $condition.=" AND TM_QN_Dificulty_Id = '".$_POST['dificulty']."'";
        endif;
        if($_POST['status']!=''):
            $condition.=" AND TM_QN_Status = '".$_POST['status']."'";
        endif;
        if($_POST['mark']!=''):
            $condition.=" AND TM_QN_Totalmarks LIKE '".$_POST['mark']."'";
        endif; 

        if($_POST['skills']!=''):

          $condition.=" AND TM_QN_Skill REGEXP '".$_POST['skills']."'";
        endif; 
        if($_POST['question_type']!=''):

            $qn_type_ids=array();
            $type_ids=$_POST['question_type'];
            foreach ($type_ids as $key => $value) {

                $qstntypenexist=Questions::model()->findAll(array('condition'=>"TM_QN_Question_type REGEXP '".$value."'"));

                if(count($qstntypenexist)>0)
                {
                    foreach ($qstntypenexist as $keys => $qtypexist) {
                    if(count($qtypexist)>0){
                            $qtypearr=explode(",",$qtypexist->TM_QN_Question_type);
                            $exist=0;
                            foreach($type_ids as $type_id)
                            {
                               if (in_array($type_id, $qtypearr))
                                { 
                                    $exist++;
                                }
                            }
                           if($exist==count($type_ids))
                           {
                            if(in_array($qtypexist->TM_QN_Id,$qn_type_ids))
                            {
                              
                            }
                            else
                            {
                              array_push($qn_type_ids,$qtypexist->TM_QN_Id);
                            }
                           }
                    }
                    }
                }
            }

           if(count($qn_type_ids)>0)
            {
                $question_type_ids=implode(",", $qn_type_ids);
            }
            else
            {

                $question_type_ids=0;
            }

            $condition.=" AND TM_QN_Id IN (".$question_type_ids.")";
        endif; 
        if($_POST['question_info']!=''):
            $condition.=" AND TM_QN_Info LIKE '".$_POST['question_info']."'";
        endif; 
        $mockquestions=MockQuestions::model()->findAll(array('condition'=>"TM_MQ_Mock_Id='".$_POST['mock']."'"));
        if(count($mockquestions)>0):
            $MQS='';
            foreach($mockquestions AS $key=>$MQ)
            {
                if($key=='0'):
                    $MQS.="'".$MQ->TM_MQ_Question_Id."'";
                else:
                    $MQS.=",'".$MQ->TM_MQ_Question_Id."'";
                endif;
            }
            $condition.=" AND TM_QN_Id NOT IN (".$MQS.")";
        endif;
        $questions=Questions::model()->with(array('exams'=>array('select'=>false,'joinType'=>'INNER JOIN','condition'=>'exams.TM_QE_Exam_Id=2')))->findAll(array('condition'=>"TM_QN_Publisher_Id='".$_POST['publisher']."' AND TM_QN_Syllabus_Id='".$_POST['syllabus']."'  AND TM_QN_Standard_Id='".$_POST['standard']."'  AND TM_QN_Parent_Id='0' AND TM_QN_Status!='111' ".$condition.""));
        // AND TM_QN_Status='0'
      // print_r($questions); exit;
        $questionlist='';
        if(count($questions)>0):
            foreach($questions AS $question):
                $types='';
                $skills='';
                    if(isset($question->TM_QN_Skill))
                    {
                        $skill_ids=explode(",",$question->TM_QN_Skill);
                        $skill_names=array();
                        foreach ($skill_ids as $key => $skill_id) {
                        $skill_name = TmSkill::model()->findByPk($skill_id)->tm_skill_name;
                        array_push($skill_names,$skill_name);

                        }
                        $skills=implode(", ",$skill_names);
                    }
                    if(isset($question->TM_QN_Question_type))
                    {
                        $type_ids=explode(",",$question->TM_QN_Question_type);
                        $type_names=array();
                        foreach ($type_ids as $key => $type_id) {
                        $type_name = QuestionType::model()->findByPk($type_id)->TM_Question_Type; 
                        array_push($type_names,$type_name);

                        }
                        $types=implode(",",$type_names);
                    }
                $questionlist.='<tr id="addquestion'.$question->TM_QN_Id.'">
                            <td class="td-actions">
                                <button class="btn btn-warning btn-block addquestion" data-id="'.base64_encode($question->TM_QN_Id).'" data-element="addquestion'.$question->TM_QN_Id.'">Add</button>
                            </td>
							<td>'.$question->TM_QN_Id.'</td>
                            <td>'.$question->TM_QN_Question.'</td>
                            <td>'.Types::model()->findByPk($question->TM_QN_Type_Id)->TM_TP_Name.'</td>

                             <td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                              <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>
                              <td>'.$skills.'</td>
                              <td>'.$types.'</td>
                            <td>'.$question->TM_QN_QuestionReff.'</td>
                            <td>'.$question->TM_QN_Pattern.'</td>
                            
                        </tr>';
/*                            <td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                            <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>*/
            endforeach;
        else:
            $questionlist='<tr><td colspan="6">No Questions Found</td></tr>';
        endif;
        echo $questionlist;
    }
    public function actionSaveStatus($id)
    {
        $model=Mock::model()->findByPk($id);
        if($model->TM_MK_Status==0):
            $model->TM_MK_Status=1;
        else:
            $model->TM_MK_Status=0;
        endif;
        $model->save(false);
        $this->redirect(array('view','id' => $id));

    }
    public function actionAddquestion()
    {
        $mockid=base64_decode($_POST['mock']);
        $questionid=base64_decode($_POST['question']);

        $mockquestion=MockQuestions::model()->findAll(array('condition'=>"TM_MQ_Mock_Id='".$mockid."' AND TM_MQ_Question_Id='".$questionid."'"));
        $mockqstcount=count(MockQuestions::model()->findAll(array('condition'=>"TM_MQ_Mock_Id='".$mockid."'")));         
        if(count($mockquestion)==0)
        {
            $ordernumber=MockQuestions::model()->find(array('condition'=>"TM_MQ_Mock_Id='".$mockid."'",'order'=>'TM_MQ_Order DESC'));
            $order='';
            if(count($ordernumber)>0):
            $order=$ordernumber->TM_MQ_Order+1;
            else:
                $order=1;
            endif;
            $question=Questions::model()->findByPk($questionid);
            $questionmock=new MockQuestions();
            $questionmock->TM_MQ_Mock_Id=$mockid;
            $questionmock->TM_MQ_Question_Id=$questionid;
            $questionmock->TM_MQ_Type=$question->TM_QN_Type_Id;
            $questionmock->TM_MQ_Order=$order;
            $questionmock->save(false);

            $skill_ids=explode(",",$question->TM_QN_Skill);
            $skill_names=array();
            foreach ($skill_ids as $key => $skill_id) {
            $skill_name = TmSkill::model()->findByPk($skill_id)->tm_skill_name;
            array_push($skill_names,$skill_name);

            }
            $skills=implode(", ",$skill_names);

            $type_ids=explode(",",$question->TM_QN_Question_type);
            $type_names=array();
            foreach ($type_ids as $key => $type_id) {
            $type_name = QuestionType::model()->findByPk($type_id)->TM_Question_Type; 
            array_push($type_names,$type_name);

            }
            $types=implode(",",$type_names);
            $returnrow='<tr id="removequestion'.$question->TM_QN_Id.'"class="order'.$questionmock->TM_MQ_Order.'qn" data-rawid="'.$question->TM_QN_Id.'">
                        <td>
                            <INPUT TYPE="image" id="orderlimkup'.$question->TM_QN_Id.'" class="order_link" data-info="up" SRC="'.Yii::app()->request->baseUrl.'/images/up.gif" ALT="SUBMIT" data-id="'.$questionmock->TM_MQ_Mock_Id.'" data-order="'.$questionmock->TM_MQ_Order.'" data-qnId="'.$question->TM_QN_Id.'">
                            <INPUT TYPE="image" id="orderlimkdown'.$question->TM_QN_Id.'" class="order_link" data-info="down" SRC="'.Yii::app()->request->baseUrl.'/images/down.gif" ALT="SUBMIT" data-id="'.$questionmock->TM_MQ_Mock_Id.'" data-order="'.$questionmock->TM_MQ_Order.'" data-qnId="'.$question->TM_QN_Id.'">
                        </td>
                        <td class="td-actions">
                            <a class="removequestion" data-id="'.base64_encode($question->TM_QN_Id).'" data-element="removequestion'.$question->TM_QN_Id.'"><span style="color: #848484; cursor: pointer; " class="glyphicon glyphicon-remove"></span></a>
                            <a href="javascript:void(0)" style="color: #848484 !important;" class="sectionheader" data-backdrop="static" data-toggle="modal" data-target="#SelectHeader" data-id="'.$questionmock->TM_MQ_Mock_Id.'" data-value="'.$question->TM_QN_Id.'" ><span style="font-size: 13px;" class="glyphicon glyphicon-header"></span></a>
			    <label class="switch" title="MCQ">
                            <input class="save" id="saveoptions" type="checkbox" data-id="'. $questionmock->TM_MQ_Mock_Id.'" data-value="'.$question->TM_QN_Id.'" name="option" >
                            <span class="slider round"></span>
                            </label>
                        </td>
                        <td>'.$question->TM_QN_Id.'</td>
						<td>'.$question->TM_QN_Question.'</td>
                        <td class="marks">'.$this->Getmarks($question->TM_QN_Id).'</td>
                        <td>'.Types::model()->findByPk($question->TM_QN_Type_Id)->TM_TP_Name.'</td>
                        <td>'.Difficulty::model()->findByPk($question->TM_QN_Dificulty_Id)->TM_DF_Name.'</td>
                        <td>'.$question->TM_QN_QuestionReff.'</td>
                        <td>'.$question->TM_QN_Pattern.'</td>
                        <td>'.$skills.'</td>
                        <td>'.$types.'</td>
			</tr>';
            /*<td>'.Chapter::model()->findByPk($question->TM_QN_Topic_Id)->TM_TP_Name.'</td>
                        <td>'.Topic::model()->findByPk($question->TM_QN_Section_Id)->TM_SN_Name.'</td>*/
            $mockqstcountnew=$mockqstcount+1;
            $marks=$this->Getmarks($question->TM_QN_Id);                       
            $retarray=array('success'=>'yes','insertrow'=>$returnrow,'count'=>$mockqstcount,'total'=>$mockqstcountnew);
            echo json_encode($retarray);
        }
        else
        {
            echo "Question Already Added";
        }
    }
    public function actionRemovequestion()
    {
        if(isset($_POST['mock']))
        {
            $mockid=base64_decode($_POST['mock']);
            $questionid=base64_decode($_POST['question']);
            $mockquestion=MockQuestions::model()->find(array('condition'=>'TM_MQ_Mock_Id='.$mockid.' AND TM_MQ_Question_Id='.$questionid));
            $questionorder=$mockquestion->TM_MQ_Order;
            if($mockquestion):
                if($mockquestion->delete(false))
                {
                    $ordermock=MockQuestions::model()->findAll(array('condition'=>'TM_MQ_Mock_Id='.$mockid.' AND TM_MQ_Order >'.$questionorder,'order'=>'TM_MQ_Order ASC'));
                    $neworder=$questionorder;
                     foreach($ordermock AS $order):
                            $order->TM_MQ_Order=$neworder;
                            $order->save(false);
                            $neworder++;
                     endforeach;                                  
                    $mockqstcount=MockQuestions::model()->count(array('condition'=>"TM_MQ_Mock_Id='".$mockid."'"));
                    $retarray=array('success'=>'yes','count'=>$mockqstcount);
                    echo json_encode($retarray);
                }
                else
                {
                    $retarray=array('success'=>'no');
                    echo json_encode($retarray);
                }
            endif;
        }
    }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $model->scenario = 'Insert';
        if($model->TM_MK_Availability!=0):
            $connection = CActiveRecord::getDbConnection();
            $sql="SELECT TM_MS_School_Id FROM tm_mock_school WHERE TM_MS_Mock_Id=$id";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $mockschool=$dataReader->readAll();
            $mockschools=array();
            foreach($mockschool AS $key=>$ms):
                $mockschools[$ms['TM_MS_School_Id']]=array('selected' => 'selected');
            endforeach;
        else:
            $mockschools=array();
        endif;
        $oldavail=$model->TM_MK_Availability;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Mock']))
        {
            $model->attributes=$_POST['Mock'];
            $model->TM_MK_Link_Resource=$_POST['Mock']['TM_MK_Link_Resource'];
            //print_r($_POST['Mock']);exit;
            if($model->save()):
                //echo "asd";exit;
                $connection = CActiveRecord::getDbConnection();
                $sql="DELETE FROM tm_mock_school WHERE TM_MS_Mock_Id=$model->TM_MK_Id";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                if($model->TM_MK_Availability!='0'):
                    $insertarray=array();
                    for($i=0;$i<count($model->schools);$i++):
                        $insertarray[$i]=array('TM_MS_Mock_Id'=>$model->TM_MK_Id,'TM_MS_School_Id'=>$model->schools[$i]);
                    endfor;
                    $builder=Yii::app()->db->schema->commandBuilder;
                    $command=$builder->createMultipleInsertCommand('tm_mock_school',$insertarray );
                    $command->execute();
                endif;
                $imageName = $_FILES["Mock"]["name"]["TM_MK_Worksheet"];
                if($imageName!=''):
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $exten=explode('.', $imageName);
                    $uniqueName="Worksheet".$mockid.'.'.$exten[1];
                    $model->TM_MK_Worksheet=CUploadedFile::getInstance($model,'TM_MK_Worksheet');
                    $model->TM_MK_Worksheet->saveAs(Yii::app()->params['uploadPath'].$uniqueName);
                    $model->TM_MK_Worksheet = $uniqueName;
                endif;
                $solutionimageName = $_FILES["Mock"]["name"]["TM_MK_Solution"];
                if($solutionimageName!=''):
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $solexten=explode('.', $solutionimageName);
                    $uniqueName=$mockname.$mockid.'.'.$exten[1];
                    $uniquesolutionName="Worksheet".$mockid.'solution.'.$solexten[1];
                    $model->TM_MK_Solution=CUploadedFile::getInstance($model,'TM_MK_Solution');
                    $model->TM_MK_Solution->saveAs(Yii::app()->params['uploadPath'].$uniquesolutionName);
                    $model->TM_MK_Solution = $uniquesolutionName;
                endif;

                if(isset($_POST['Mock']['TM_MK_Video_Url']))
                { 

                        $rx = 
                            '~ 
                            ^(?:https?://)?                         
                            (?:www[.])?                            
                            (?:youtube[.]com/watch[?]v=|youtu[.]be/) 
                            ([^&]{11})~x'; 
                            $youtube_url = $_POST['Mock']['TM_MK_Video_Url'];
                            $has_match = preg_match($rx, $youtube_url, $matches);

                            if($matches[0]!='')
                                {
                                    $video_id = explode("?v=", $youtube_url);
                                    $video_id = $video_id[1]; 
                                    $thumbnail_url='https://img.youtube.com/vi/'.$video_id.'/hqdefault.jpg';
                                    $model->TM_MK_Thumbnail = $thumbnail_url;
                                } 
                                   
                }
                $thumbimageName = $_FILES["Mock"]["name"]["TM_MK_Thumbnail"];
                if($thumbimageName!=''):
                    $mockname=str_replace(' ', '', $model->TM_MK_Name);
                    $mockid=str_replace(' ', '', $model->TM_MK_Id);
                    $thumbexten=explode('.', $thumbimageName);
                    $uniquethumbName="Worksheet".$mockid.'thumbnail.'.$thumbexten[1];
                    $model->TM_MK_Thumbnail=CUploadedFile::getInstance($model,'TM_MK_Thumbnail');
                    $model->TM_MK_Thumbnail->saveAs(Yii::app()->params['uploadPath'].$uniquethumbName);
                    $model->TM_MK_Thumbnail = $uniquethumbName;
                endif;
                $model->save(false);
                $this->redirect(array('view','id'=>$model->TM_MK_Id));
            endif;
        }

        $this->render('update',array(
            'model'=>$model,
            'mockschools'=>$mockschools
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$mock=$this->loadModel($id);
        $ordermocks=Mock::model()->findAll(array(
                        'condition' => 'TM_MK_Sort_Order>:order AND TM_MK_Standard_Id=:standard',                        
                        'order'=>'TM_MK_Sort_Order ASC',
                        'params' => array(':order' => $mock->TM_MK_Sort_Order,':standard'=>$mock->TM_MK_Standard_Id)
                    )
                );
        $sortorder=$mock->TM_MK_Sort_Order;
        foreach($ordermocks AS $order):
            $order->TM_MK_Sort_Order=$sortorder;
            $order->save(false);
            $sortorder++;
        endforeach;
        $mock->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Mock');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Mock('search');
		/*$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Mock']))
			$model->attributes=$_GET['Mock'];*/

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    public function actionTags()
    {
        $model=new MockTags('search');
        /*$model->unsetAttributes();  // clear any default values
        if(isset($_GET['Mock']))
            $model->attributes=$_GET['Mock'];*/

        $this->render('tags',array(
            'model'=>$model,
        ));
    }
    public function actionCreatetags()
    {
        $model=new MockTags();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['MockTags']))
        {
            $model->attributes=$_POST['MockTags'];
            $model->TM_MT_Type=$_POST['MockTags']['TM_MT_Type'];
            $standard=$_POST['MockTags']['TM_MT_Standard_Id'];
            $syllabus=Standard::model()->findByPk($standard);
            $model->TM_MT_Syllabus_Id=$syllabus['TM_SD_Syllabus_Id'];
            if($model->save())
                $this->redirect(array('viewtags','id'=>$model->TM_MT_Id));
        }

        $this->render('createtags',array(
            'model'=>$model,
        ));
    }
    public function actionViewtags($id)
    {
        $this->render('viewtags',array(
            'model'=>$this->loadModelTags($id),
        ));
    }
    public function actionUpdatetags($id)
    {
        $model=$this->loadModelTags($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['MockTags']))
        {
            $model->attributes=$_POST['MockTags'];
            $standard=$_POST['MockTags']['TM_MT_Standard_Id'];
            $syllabus=Standard::model()->findByPk($standard);
            $model->TM_MT_Syllabus_Id=$syllabus['TM_SD_Syllabus_Id'];
            if($model->save())
                $this->redirect(array('viewtags','id'=>$model->TM_MT_Id));
        }

        $this->render('updatetags',array(
            'model'=>$model,
        ));
    }
    public function actionDeletetags($id)
    {
        $this->loadModelTags($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('tags'));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Mock the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Mock::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function loadModelTags($id)
    {
        $model=MockTags::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

	/**
	 * Performs the AJAX validation.
	 * @param Mock $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mock-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionMockOrder()
    {
    
        if (isset($_POST['id']))
        {
            $info=$_POST['info'];
            $id=$_POST['id'];
            $order=$_POST['order'];
            if (($_POST['info']=='up')):
                $modelold=MockQuestions::model()->find(array(
                        'condition' => 'TM_MQ_Mock_Id=:id AND  TM_MQ_Order<:order ',
                        'limit' => 1,
                        'order'=>'TM_MQ_Order DESC',
                        'params' => array(':id' => $id, ':order' => $order)
                    )
                );
                
                $model=MockQuestions::model()->find(array(
                        'condition' => 'TM_MQ_Mock_Id=:id AND  TM_MQ_Order=:order',
                        'params' => array(':id' => $id, ':order' => $order)
                    )
                );
                $temp=$modelold->TM_MQ_Order;
                $modelold->TM_MQ_Order=$model->TM_MQ_Order;
                $model->TM_MQ_Order=$temp;
                $modelold->save(false);
                $model->save(false);
                echo "yes";
            else:
                    $modelold=MockQuestions::model()->find(array(
                            'condition' => 'TM_MQ_Mock_Id=:id AND  TM_MQ_Order>:order ',
                            'limit' => 1,
                            'order'=>'TM_MQ_Order ASC',
                            'params' => array(':id' => $id, ':order' => $order)
                        )
                    );
    
                    $model=MockQuestions::model()->find(array(
                            'condition' => 'TM_MQ_Mock_Id=:id AND  TM_MQ_Order=:order',
                            'params' => array(':id' => $id, ':order' => $order)
                        )
                    );
                    $temp=$modelold->TM_MQ_Order;
                    $modelold->TM_MQ_Order=$model->TM_MQ_Order;
                    $model->TM_MQ_Order=$temp;
                    $modelold->save(false);
                    $model->save(false);
                    echo "yes";
            endif;

        }
    }
    public function actionReordermock()
    {
    
        if (isset($_POST['id']))
        {
            $info=$_POST['info'];
            $id=$_POST['id'];
            $order=$_POST['order'];
            if (($_POST['info']=='up')):
				$neworder=$order+1;
                $model=Mock::model()->findByPk($id); 
                $modelupper=Mock::model()->find(array(
                        'condition' => 'TM_MK_Sort_Order>:order AND TM_MK_Standard_Id=:standard AND TM_MK_Resource=:resource',
                        'limit' => 1,
                        'order'=>'TM_MK_Sort_Order ASC',
                        'params' => array(':order' => $order,':standard'=>$model->TM_MK_Standard_Id,':resource'=>$model->TM_MK_Resource)
                    )
                );  					
                if($modelupper):    
					$model->TM_MK_Sort_Order=$modelupper->TM_MK_Sort_Order;                
					$model->save(false);
                    $modelupper->TM_MK_Sort_Order=$order;    
                    $modelupper->save(false);
	
					echo "yes";					
				else:
					echo "no";					
                endif;                                
            else:
				$neworder=$order-1;
                $model=Mock::model()->findByPk($id); 
                $modelupper=Mock::model()->find(array(
                        'condition' => 'TM_MK_Sort_Order<:order AND TM_MK_Standard_Id=:standard AND TM_MK_Resource=:resource',
                        'limit' => 1,
                        'order'=>'TM_MK_Sort_Order DESC',
                        'params' => array(':order' => $order,':standard'=>$model->TM_MK_Standard_Id,':resource'=>$model->TM_MK_Resource)
                    )
                ); 	
				//echo $modelupper->TM_MK_Sort_Order; exit;
				//echo $order; exit;
                if($modelupper):
					$model->TM_MK_Sort_Order=$modelupper->TM_MK_Sort_Order;
					$model->save(false);		
                    $modelupper->TM_MK_Sort_Order=$order;    
                    $modelupper->save(false); 					                								
					echo "yes";					
				else:
					echo "no";					
                endif;  
            endif;

        }
    }
    public function actionPrintmock($id)
    {       
        $schoolhomework=Mock::model()->findByPk($id);        
        if($schoolhomework->TM_MK_Worksheet==''):
            $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $username=User::model()->findByPk(Yii::app()->user->id)->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;  
            // $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
             $html = "<style>
            p{
            margin-top: 0;
            margin-bottom: 0;
            }
            </style>
            <table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";

            $html .= "<tr style='vertical-align: top;'><td width='6%' style='vertical-align: top;padding:0;padding-bottom: 40px;'></td><td style='padding:0;' align='center'><u><b>".$homework."</b><u></td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>"; 
            $html .= "<tr><td colspan=3 align='center'></td></tr>";
            $totalquestions=count($homeworkqstns);         
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):
    
                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
                $showoption=false;
                if($question->TM_QN_Show_Option==1):
                    $showoption=true;
                else:
                    $showoption=false;
                endif;                       
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                if($homeworkqstn['TM_MQ_Header']!=''):
                    $html .= "<tr><td colspan=3 align='center'><u><b>".$homeworkqstn['TM_MQ_Header']."</b><u></td></tr>
                                <tr><td colspan=3 align='center'><i>".$homeworkqstn['TM_MQ_Section']."</i></td></tr><tr><td colspan=3 align='center' ></td></tr>";
                endif;
                //ends
                        if ($question->TM_QN_Type_Id!='7'):
                            $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td>";
                            $html .= "<td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>( " . $marks . " )</td></tr>";
                            if ($question->TM_QN_Image != ''):
                                $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                            endif;
                            if($homeworkqstn->TM_MQ_Show_Options==1):
                                $showoption=true;
                            endif;
                            if($showoption):
                                foreach($question->answers AS $key=>$answer):
                                    if($answer->TM_AR_Image!=''):
                                        $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                    else:
                                        $ansoptimg='';
                                    endif;
                                    $ans=$answer->TM_AR_Answer;
                                    $find='<p>';
                                    $pos = strpos($ans, $find);
                                     if ($pos === false) {
                                        $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")".$answer->TM_AR_Answer."</p> $ansoptimg</td><td></td></tr>";
                                    } else {
                                        // var_dump($answer->TM_AR_Answer);exit;
                                        $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p style="margin:0;">'.$alphabets[$key].')', 0,3)." $ansoptimg</td><td></td></tr>";
                                    }
                                endforeach;
                            endif;
                            if($question->TM_QN_Type_Id=='5'):
                                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                                foreach ($questions AS $key=>$childquestion):
                                   $html .= "<tr><td width='6%'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td></td></tr>";
                                    if ($childquestion->TM_QN_Image != ''):
                                         $html .= '<tr><td width="6%"></td><td align="left" style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td></td></tr>';
                                    endif;
                                    // if($childquestion->TM_QN_Type_Id==4):
                                    //     $showoption=false;
                                    // endif;
                                    // multipart text entry showing answers
                                    if($childquestion->TM_QN_Type_Id!=4){
                                        if($showoption):
                                            foreach($childquestion->answers AS $key=>$answer):
                                                if($answer->TM_AR_Image!=''):
                                                    $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                                else:
                                                    $ansoptimg='';
                                                endif;
                                                $ans=$answer->TM_AR_Answer;
                                                $find='<p>';
                                                $pos = strpos($ans, $find);
                                                if ($pos === false)
                                                {
                                                    $html.="<tr><td></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")</p>".$answer->TM_AR_Answer."</td>
                                                    <td align='right'>$ansoptimg</td></tr>";
                                                }
                                                else
                                                {
                                                   $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."$ansoptimg</td><td></td></tr>";
                                                }
                                            endforeach;
                                        endif;
                                    }
                                    
                                endforeach;
                            endif;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    if ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $countpart = 1;
                        $childhtml = "";
                        foreach ($questions AS $childquestion):
                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                            $childhtml .="<tr><td width='6%' style='padding:0;'></td><td align='left' style='padding:0;'>Part." . $countpart."</td><td align='right' width='6%' style='padding:0;'></td></tr>";
                            $childhtml .="<tr><td width='6%' style='padding:0;'></td><td  style='padding:0;'>".$childquestion->TM_QN_Question."</td><td align='right' width='6%' style='padding:0;'></td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                               $childhtml .= '<tr><td align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            $countpart++;
                        endforeach;
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'> (" . $displaymark . " )</td></tr>";
                        // $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%"></td><td align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $html .= $childhtml;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"><br></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />                    
                        endif;
                    endif;
            endforeach;
            $html .= "</table>";
            $schoolhw=Mock::model()->findByPK($id);
            //$school=School::model()->findByPK($schoolhw->TM_SCH_School_Id);
            $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));
             $userschool=User::model()->findByPk(Yii::app()->user->id);
             $school=School::model()->findByPk($userschool->school_id);
            $header.= '<div style=""><table width="100%" style="vertical-align: bottom;font-size:14px;font-family: STIXGeneral; color: #000000; font-weight: lighter; margin-bottom:10px; margin-top:15px;"><tr>
                    <td width="20%"><span style="font-weight: lighter; ">Student Name :</span></td>
                    <td width="50%" align="left" style="font-weight: lighter; ">'.$student->TM_STU_First_Name.'</td>
                    <td width="20%"><span style="font-weight: lighter; ">Date : </span></td>
                    <td width="10%" align="center" style="font-weight: lighter; ">'.date('d/m/Y').'</td>
                    </tr></table></div>';
            $html=$header.$html;

             $this->render('printpdf',array('html'=>$html));
            exit;
            // $mpdf = new mPDF();
            // 
            // //$mpdf->SetHTMLHeader($header);
            // $mpdf->SetWatermarkText($username, .1);
            // $mpdf->showWatermarkText = false;
    
            // $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
            //  <tr>
            //      <td width="100%" colspan="2">
            //          &copy; Copyright @ TabbieMe Ltd. All rights reserved .
            //      </td>
            //  </tr>
            //  <tr>
            //      <td width="80%" >
            //          www.tabbiemath.com  - The one stop shop for Maths Revision.
            //      </td>
            //         <td align="right" >Page #{PAGENO}</td>
            //  </tr>
            // </table>');
    
            // //echo $html;exit;
            // $name='WORKSHEET'.time().'.pdf';
            // $mpdf->WriteHTML($html);
            // $mpdf->Output($name, 'I');
        else:
            $this->redirect(Yii::app()->request->baseUrl."/worksheets/".$schoolhomework->TM_MK_Worksheet);
        endif;

    }    
     public function actionPrintmocksolution($id)
    {        
            $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $username=User::model()->findByPk(Yii::app()->user->id)->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;  
            $html = "<style>
            p{
            margin-top: 0;
            margin-bottom: 0;
            }
            </style>
            <table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";
            $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;text-align: center;'><u><b>".$homework."</b><u></td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>"; 
            $html .= "<tr><td colspan=3 align='center'></td></tr>";
            $totalquestions=count($homeworkqstns);         
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):
    
                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
                $showoption=false;
                if($question->TM_QN_Show_Option==1):
                    $showoption=true;
                else:
                    $showoption=false;
                endif;                      
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>(" . $marks . ")</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%"" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;                                              
                        if($question->TM_QN_Type_Id=='5'):
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            foreach ($questions AS $key=>$childquestion):
                                $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                                if ($childquestion->TM_QN_Image != ''):
                                     $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                                endif;
                                // multipart text entry showing answers
                                // if($childquestion->TM_QN_Type_Id!=4)
                                // {
                                    if($showoption):
                                        foreach($childquestion->answers AS $key=>$answer):
                                            if($answer->TM_AR_Image!=''):
                                                $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                            else:
                                                $ansoptimg='';
                                            endif;
                                            $ans=$answer->TM_AR_Answer;
                                            $find='<p>';
                                            $pos = strpos($ans, $find);
                                            if ($pos === false)
                                            {
                                                $html.="<tr><td colspan='3'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
                                                <td align='right'>$ansoptimg</td></tr>";
                                            }
                                            else
                                            {
                                                $html.="<tr><td colspan='3'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td>
                                                <td align='right'>$ansoptimg</td></tr>";
                                            }
                                        endforeach;
                                    endif;
                                // }
                            endforeach;
                        endif;

                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>Sol. </b></td>";
                       $html .= "<td style='padding:0;'>".$question->TM_QN_Solutions."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                           $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;                          
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    if ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $countpart = 1;
                        $childhtml = "";
                        foreach ($questions AS $childquestion):
                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                            $childhtml .="<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>Part." . $countpart."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                             $childhtml .="<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>".$childquestion->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                 $childhtml .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                            endif;
                            $countpart++;
                        endforeach;
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount ."</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>(" . $displaymark . ")</td></tr>";
                        // $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                              $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        $html .= $childhtml;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />                    
                        endif;
                    endif;
            endforeach;
            $html .= "</table>";
            $schoolhw=Mock::model()->findByPK($id);
            $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));
            $header = '<div><table width="100%" style="vertical-align: bottom; font-size:14px;font-family: STIXGeneral; color: #000000; font-weight: lighter; "><tr>
                    <td width="20%"><span style="font-weight: lighter; ">Student Name :</span></td>
                    <td width="50%" align="center" style="font-weight: lighter; ">'.$student->TM_STU_First_Name.'</td>
                    <td width="20%"><span style="font-weight: lighter; ">Date : </span></td>
                    <td width="10%" align="center" style="font-weight: lighter; ">'.date('d/m/Y').'</td>
                    </tr></table></div>';
            $html=$header.$html;

            $this->render('printpdf',array('html'=>$html));
            // exit;
            // $mpdf = new mPDF();
            
            // //$mpdf->SetHTMLHeader($header);;
            // $mpdf->SetWatermarkText($username, .1);
            // $mpdf->showWatermarkText = false;
    
            // $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
            //  <tr>
            //      <td width="100%" colspan="2">
            //          &copy; Copyright @ TabbieMe Ltd. All rights reserved .
            //      </td>
            //  </tr>
            //  <tr>
            //      <td width="80%" >
            //          www.tabbiemath.com  - The one stop shop for Maths Revision.
            //      </td>
            //         <td align="right" >Page #{PAGENO}</td>
            //  </tr>
            // </table>');
    
            // //echo $html;exit;
            // $name='WORKSHEET'.time().'.pdf';
            // $mpdf->WriteHTML($html);
            // $mpdf->Output($name, 'I');
    } 
    public function actionDeletefile()
    {
        if(isset($_POST['mock'])):
            $mock=Mock::model()->findByPk($_POST['mock']);
            if($_POST['type']=='worksheet'):
                unlink(Yii::app()->params['uploadPath'].$mock->TM_MK_Worksheet);
                $mock->TM_MK_Worksheet='';
            elseif($_POST['type']=='solution'):
                unlink(Yii::app()->params['uploadPath'].$mock->TM_MK_Solution);
                $mock->TM_MK_Solution='';
            elseif(strpos($mock->TM_MK_Thumbnail, 'youtube') > 0):
                $mock->TM_MK_Thumbnail='';
               else:
                unlink(Yii::app()->params['uploadPath'].$mock->TM_MK_Thumbnail);
                $mock->TM_MK_Thumbnail='';
            endif;
            $mock->save(false);
            echo 'yes';
        else:
            echo 'No';
        endif;
    }
    public function Getmarks($questionid)
    {
            $question = Questions::model()->findByPk($questionid);
            if ($question->TM_QN_Parent_Id == '0'):
                if($question->TM_QN_Type_Id!='5' && $question->TM_QN_Type_Id!='6' && $question->TM_QN_Type_Id!='7'):
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                    $total = 0;
                    foreach ($marks AS $key => $marksdisplay):
                        $total = $total + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                elseif($question->TM_QN_Type_Id=='5'):
                    $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                    $total=0;                    
                    foreach ($childquestions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($marks AS $key => $marksdisplay):
                            $total = $total + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                    endforeach;
                elseif($question->TM_QN_Type_Id=='6'):
                    $total=$question->TM_QN_Totalmarks;
                elseif($question->TM_QN_Type_Id=='7'):
                    $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                    $total=0;                    
                    foreach ($childquestions AS $childquestion):
                        $total = $total + (float)$childquestion->TM_QN_Totalmarks;
                    endforeach;
                endif;
            endif;
            return $total;
    }
	
	public function actionExportworksheet()
    {
        $syllabus=Syllabus::model()->findAll(array("condition"=>"TM_SB_Status = '0'",'order' => 'TM_SB_Name'));
        $this->render('exportworksheet',array('syllabus'=>$syllabus));
    }

    public function actionExportworksheetcsv()
    {
        if($_POST['syllabus']):
            $condition='a.TM_MK_Syllabus_Id='.$_POST['syllabus'];
            if($_POST['standard']!=''):
                $condition.=' AND a.TM_MK_Standard_Id='.$_POST['standard'];
            endif;
        endif;
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_MK_Id,a.TM_MK_Name,a.TM_MK_Description,a.TM_MK_Tags,a.TM_MK_Thumbnail,a.TM_MK_Status,a.TM_MK_Availability,a.TM_MK_Sort_Order,a.TM_MK_Resource,a.TM_MK_Link_Resource,a.TM_MK_Video_Url,";
        $sql.="b.TM_PR_Name,c.TM_SB_Name,d.TM_SD_Name ";
        $sql.="FROM tm_mock AS a LEFT JOIN tm_publisher AS b ON a.TM_MK_Publisher_Id=b.TM_PR_Id ";
        $sql.="LEFT JOIN tm_syllabus AS c ON a.TM_MK_Syllabus_Id=c.TM_SB_Id ";
        $sql.="LEFT JOIN tm_standard AS d ON a.TM_MK_Standard_Id=d.TM_SD_Id ";
        $sql.="WHERE ".$condition." ORDER BY a.TM_MK_Sort_Order DESC";
        //echo $sql;exit;
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataval=$dataReader->readAll();
        //print_r($dataval); exit;
        $filename=time().'_worksheets.csv';
        //$output = fopen(Yii::app()->basePath.'/../importfiles/'.$filename, 'w');
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);
        $output = fopen('php://output', 'w');
        fputcsv($output, array(
            "ID",
			"Delete",
            "Name",
            "Description",
            "Publisher",
            "Syllabus",
            "Standard",
            "Status",
            "Type", 
            "Availability",
            "Tags",
            "Thumbnail Image",
            "Sort Order",
            "Questions",
            "Link to resource/worksheet",
        ));
        if(count($dataval)>0):
            foreach($dataval AS $item):
                if($item['TM_MK_Status']==0):
                    $status="Active";
                else:
                    $status="Not Active";
                endif;
                if($item['TM_MK_Availability']==0):
                    $avail="Student";
                elseif($item['TM_MK_Availability']==1):
                    $avail="School - All";
                elseif($item['TM_MK_Availability']==2):
                    $avail="Both";
                elseif($item['TM_MK_Availability']==3):
                    $avail="School - Specific";
                elseif($item['TM_MK_Availability']==4):
                    $avail="Both - Specific Schools";
                else:
                    $avail="";
                endif;
		        $delete = "No";
                $mockquestions=MockQuestions::model()->findAll(array('condition'=>"TM_MQ_Mock_Id='".$item['TM_MK_Id']."'",'order'=>'TM_MQ_Order ASC'));
                $questions='';
                if($item['TM_MK_Resource']==0){
                foreach($mockquestions AS $key=>$mockquestion):
                    $question=Questions::model()->findByPk($mockquestion->TM_MQ_Question_Id);
                    if($key==0):
                        $questions.= $question['TM_QN_Id'] .":". $question['TM_QN_QuestionReff'];
                    else:
                        $questions.=",".$question['TM_QN_Id'].":".$question['TM_QN_QuestionReff'];
                    endif;
                endforeach;
            }
            else
            {

                $questions.=$item['TM_MK_Video_Url'];
            }
                $type='';
                if($item['TM_MK_Resource']==0)
                {
                    $type='Worksheet';
                }
                elseif($item['TM_MK_Resource']==1)
                {
                    $type='Resource';
                }
                $link='';
                if($item['TM_MK_Link_Resource']==0)
                {
                    $link='No';
                }
                else
                {
                    $link='Yes';
                }
                if (strpos($item['TM_MK_Thumbnail'], 'youtube') > 0)

                {
                    $thumbnail='';
                }
                else
                {
                    $thumbnail= $item['TM_MK_Thumbnail'];
                }
                $itemrow=array(
                    $item['TM_MK_Id'],//0
					$delete,//1
                    $item['TM_MK_Name'],//2
                    $item['TM_MK_Description'],//3
                    $item['TM_PR_Name'],//4
                    $item['TM_SB_Name'],//5
                    $item['TM_SD_Name'],//6
                    $status,//7
                    $type,//8
                    $avail,//9
                    $item['TM_MK_Tags'],//10
                    $thumbnail,//11
                    $item['TM_MK_Sort_Order'],//12
                    $questions,//13
                    $link,//14
					
                );
                fputcsv($output, $itemrow);
            endforeach;
        endif;
    }
	
	public function actionImportworksheet()
    {
        $connection = CActiveRecord::getDbConnection();

        $syllabus=Syllabus::model()->findAll(array("condition"=>"TM_SB_Status = '0'",'order' => 'TM_SB_Name'));
        $importlog=array();
        if(isset($_POST["submit"])){
            if($_FILES["importworksheetcsv"]["size"] > 0)
            {
                $file = fopen($_FILES['importworksheetcsv']['tmp_name'], "r");

                $values=fgetcsv($file);
                $count=1;
                while (($values = fgetcsv($file)) !== FALSE)
                { 
                    $model=Mock::model()->findByPk($values[0]);
                    if(count($model)>0){
                      if($values[1]=="No"): 
                        $model->TM_MK_Name=$values[2];
                        $model->TM_MK_Description=$values[3];
                        $publishers= Publishers::model()->find(array('condition'=>"TM_PR_Name='".$values[4]."' AND TM_PR_Status=0 "));
                        if(count($publishers)>0):
                            $model->TM_MK_Publisher_Id=$publishers['TM_PR_Id'];
                        else:
                            $importlog[$count]['status']="Mock ID:".$values[0]." Publisher '".$values[4]."' Not Found";
                            $importlog[$count]['worksheet']='1';
                        endif;
                            $syllabus= Syllabus::model()->find(array('condition'=>"TM_SB_Name='".$values[5]."' AND TM_SB_Status=0 "));
                            if(count($syllabus)>0):
                                $model->TM_MK_Syllabus_Id=$syllabus['TM_SB_Id'];
                                $standard= Standard::model()->find(array('condition'=>"TM_SD_Syllabus_Id='".$syllabus['TM_SB_Id']."' AND TM_SD_Name='".$values[6]."' AND TM_SD_Status=0 "));
                                if(count($standard)>0):
                                    $model->TM_MK_Standard_Id=$standard['TM_SD_Id'];
                                    if($values[7]=="Active"):
                                        $status= 0;
                                    else:
                                        $status= 1;
                                    endif;
                                    $model->TM_MK_Status=$status;
                                    if($values[9]=='Student'):
                                        $avail= 0;
                                    elseif($values[9]=='School - All'):
                                        $avail= 1;
                                    elseif($values[9]=='Both'):
                                        $avail= 2;
                                    elseif($values[9]=='School - Specific'):
                                        $avail= 3;
                                    elseif($values[9]=='Both - Specific Schools'):
                                        $avail= 4;
                                    endif;
                                    
                                    if($values[14]=='Yes')
                                    {
                                        $linktype=1;
                                    } 
                                    else
                                    {
                                        $linktype=0;
                                    }
                                    if($values[8]=='Resource')
                                    {
                                        $type=1;
                                        $created=Yii::app()->user->id;

                                    }
                                    else
                                    {
                                        $type=0;
                                        $created=0;
                                    }
                                    $model->TM_MK_CreatedBy=$created;
                                    $model->TM_MK_Resource=$type;
                                    $model->TM_MK_Availability=$avail;
                                    $model->TM_MK_Tags=$values[10];
                                    $model->TM_MK_Sort_Order=$values[12];
                                    $model->TM_MK_Link_Resource=$linktype;
                if($values[8]=='Resource' && $values[13]!='')
                { 

                        $rx = 
                            '~ 
                            ^(?:https?://)?                         
                            (?:www[.])?                            
                            (?:youtube[.]com/watch[?]v=|youtu[.]be/) 
                            ([^&]{11})~x'; 
                            $youtube_url = $values[13];
                            $has_match = preg_match($rx, $youtube_url, $matches);

                            if($matches[0]!='')
                                    {
                                        $video_id = explode("=", $youtube_url);
                                        $video_id = $video_id[1]; 
                                        $video_char = explode("&", $video_id);
                                        if(count($video_char)>1)
                                        {
                                           $video_id=$video_char[0];
                                        }
                                        $thumbnail_url='https://img.youtube.com/vi/'.$video_id.'/hqdefault.jpg';

                                        if($values[11]=='')
                                        {
                                          $model->TM_MK_Thumbnail=$thumbnail_url;
                                        }
                                        else
                                        {
                                           $model->TM_MK_Thumbnail=$values[11];
                                        }
                                        $model->TM_MK_Video_Url=$values[13];
                                    }
                                    else
                                    {
                                        $model->TM_MK_Thumbnail=$values[11];
                                        $model->TM_MK_Video_Url=$values[13];
                                    }            
                }
                else
                {
                    $model->TM_MK_Thumbnail=$values[11];
                    $model->TM_MK_Video_Url=$values[13];
                }
                                                                 
                                    if($model->save(false)):
                                        $importlog[$count]['worksheet']='1';
                                        if($model->TM_MK_Availability=='1' or $model->TM_MK_Availability=='2'):
                                            
                                            $sql="DELETE FROM tm_mock_school WHERE TM_MS_Mock_Id=$model->TM_MK_Id";
                                            $command=$connection->createCommand($sql);
                                            $dataReader=$command->query();
                                            $insertarray=array();
                                            $schools = School::model()->findAll();
                                            for($i=0;$i<count($schools);$i++):
                                                $insertarray[$i]=array('TM_MS_Mock_Id'=>$values[0],'TM_MS_School_Id'=>$schools[$i]['TM_SCL_Id']);
                                            endfor;
                                            $builder=Yii::app()->db->schema->commandBuilder;
                                            $command=$builder->createMultipleInsertCommand('tm_mock_school',$insertarray );
                                            $command->execute();
                                        endif;
                                        
                                        $questref=explode(',', $values[13]);
                                        $a1=array();
                                       
                                        
                                        $sortCount = 0;


                                        $sql="DELETE FROM tm_mock_questions WHERE TM_MQ_Mock_Id='".$model['TM_MK_Id']."'";
                                        $command=$connection->createCommand($sql);
                                        $dataReader=$command->query();
                                            
                        
                                        // echo $model['TM_MK_Id'];exit;

                                        for($j=0;$j<count($questref);$j++)
                                        {

                                            $questid = explode(':', $questref[$j]);
                                            //echo $questid[0]; exit;
                                            $question=Questions::model()->find(array('condition'=>"TM_QN_Id='".$questid[0]."'"));
                                            if(count($question)>0):
                                                $sortCount++;

                                               // $mockquestions=MockQuestions::model()->find(array('condition'=>"TM_MQ_Mock_Id='".$model['TM_MK_Id']."' AND TM_MQ_Question_Id='".$questid[0]."'"));
                                               // $mockquestions1=MockQuestions::model()->findAll(array('condition'=>"TM_MQ_Mock_Id='".$model['TM_MK_Id']."'"));
                                               // $countmockquestions = count($mockquestions1);
                                               // if(count($mockquestions)==0):
                                                $questionmock=new MockQuestions();
                                                $questionmock->TM_MQ_Mock_Id=$model['TM_MK_Id'];
                                                    $questionmock->TM_MQ_Question_Id=$question['TM_QN_Id'];
                                                    $questionmock->TM_MQ_Type=$question->TM_QN_Type_Id;
                                                    $questionmock->TM_MQ_Order=$sortCount;
                                                    $questionmock->save(false);
                                               // endif;
                                            endif;
                                            // array_push($a1,$questid[0]);
                                        }
                                       
                                    else:
                                        $importlog[$count]['worksheet']='0';
                                    endif;
                                else:
                                  $importlog[$count]['status']="Mock ID:".$values[0]." Standard '".$values[6]."' Not Found";
                                  $importlog[$count]['worksheet']='0';
                                endif;
                            else:
                              $importlog[$count]['status']="Mock ID:".$values[0]." Syllabus '".$values[5]."' Not Found";
                              $importlog[$count]['worksheet']='0';
                            endif;
                      else:
                            $ordermocks=Mock::model()->findAll(array(
                                    'condition' => 'TM_MK_Sort_Order>:order AND TM_MK_Standard_Id=:standard',                        
                                    'order'=>'TM_MK_Sort_Order ASC',
                                    'params' => array(':order' => $model->TM_MK_Sort_Order,':standard'=>$model->TM_MK_Standard_Id)
                                )
                            );
                            $sortorder=$model->TM_MK_Sort_Order;
                            foreach($ordermocks AS $order):
                                $order->TM_MK_Sort_Order=$sortorder;
                                $order->save(false);
                                $sortorder++;
                            endforeach;
                            $model->delete();
                            $importlog[$count]['status']="Mock ID:".$values[0]." Deleted Successfully";
                            $importlog[$count]['worksheet']='1';
                      endif;
                    }
                    else{
                      if($values[1]=="No"):
                        $model=new Mock;
                        $model->TM_MK_Name=$values[2];
                        $model->TM_MK_Description=$values[3];
                        $publishers= Publishers::model()->find(array('condition'=>"TM_PR_Name='".$values[4]."' AND TM_PR_Status=0 "));
                        if(count($publishers)>0):
                            $model->TM_MK_Publisher_Id=$publishers['TM_PR_Id'];
                        endif;
                        $syllabus= Syllabus::model()->find(array('condition'=>"TM_SB_Name='".$values[5]."' AND TM_SB_Status=0 "));
                            if(count($syllabus)>0):
                                $model->TM_MK_Syllabus_Id=$syllabus['TM_SB_Id'];
                                $standard= Standard::model()->find(array('condition'=>"TM_SD_Syllabus_Id='".$syllabus['TM_SB_Id']."' AND TM_SD_Name='".$values[6]."' AND TM_SD_Status=0 "));
                                if(count($standard)>0):
                                    $model->TM_MK_Standard_Id=$standard['TM_SD_Id'];
                                    if($values[7]=="Active"):
                                        $status= 0;
                                    else:
                                        $status= 1;
                                    endif;
                                    $model->TM_MK_Status=$status;
                                    if($values[9]=='Student'):
                                        $avail= 0;
                                    elseif($values[9]=='School - All'):
                                        $avail= 1;
                                    elseif($values[9]=='Both'):
                                        $avail= 2;
                                    elseif($values[9]=='School - Specific'):
                                        $avail= 3;
                                    elseif($values[9]=='Both - Specific Schools'):
                                        $avail= 4;
                                    endif;
                                    if($values[14]=='Yes')
                                    {
                                        $linktype=1;
                                    } 
                                    else
                                    {
                                        $linktype=0;
                                    }
                                    if($values[8]=='Resource')
                                    {
                                        $type=1;
                                        $created=Yii::app()->user->id;

                                    }
                                    else
                                    {
                                        $type=0;
                                        $created=0;
                                    }
                                    $model->TM_MK_CreatedBy=$created;
                                    $model->TM_MK_Resource=$type;
                                    $model->TM_MK_Availability=$avail;
                                    $model->TM_MK_Tags=$values[10];
                                    //$model->TM_MK_Thumbnail=$values[11];
                                    $model->TM_MK_Sort_Order=$values[12];
                                    $model->TM_MK_Link_Resource=$linktype;
                if($values[8]=='Resource' && $values[13]!='')
                { 

                        $rx = 
                            '~ 
                            ^(?:https?://)?                         
                            (?:www[.])?                            
                            (?:youtube[.]com/watch[?]v=|youtu[.]be/) 
                            ([^&]{11})~x'; 
                            $youtube_url = $values[13];
                            $has_match = preg_match($rx, $youtube_url, $matches);

                            if($matches[0]!='')
                                    {
                                        $video_id = explode("=", $youtube_url);
                                        $video_id = $video_id[1]; 
                                        $video_char = explode("&", $video_id);
                                        if(count($video_char)>1)
                                        {
                                           $video_id=$video_char[0];
                                        }
                                        $thumbnail_url='https://img.youtube.com/vi/'.$video_id.'/hqdefault.jpg';
                                         $model->TM_MK_Thumbnail=$thumbnail_url;
                                        $model->TM_MK_Video_Url=$values[13];
                                    } 
                                    elseif(strpos($values[11], 'youtube'))
                                    { 
                                        $model->TM_MK_Thumbnail='';
                                        $model->TM_MK_Video_Url=$values[13];
                                    } 
                                    else
                                    {
                                        $model->TM_MK_Thumbnail=$values[11];
                                        $model->TM_MK_Video_Url=$values[13];
                                    } 

                                   
                }
                else
                {
                    $model->TM_MK_Thumbnail=$values[11];
                    $model->TM_MK_Video_Url=$values[13];
                }                                   
                                    if($model->save(false)):
                                        if($model->TM_MK_Availability=='1' || $model->TM_MK_Availability=='2'):
                                            $insertarray=array();
                                            $schools = School::model()->findAll();
                                            for($i=0;$i<count($schools);$i++):
                                                $insertarray[$i]=array('TM_MS_Mock_Id'=>$model['TM_MK_Id'],'TM_MS_School_Id'=>$schools[$i]['TM_SCL_Id']);
                                            endfor;
                                            $builder=Yii::app()->db->schema->commandBuilder;
                                            $command=$builder->createMultipleInsertCommand('tm_mock_school',$insertarray );
                                            $command->execute();
                                        endif;
                                        $questref=explode(',', $values[13]);
                                        for($j=0;$j<count($questref);$j++)
                                        {
                                              $questid = explode(':', $questref[$j]);
                                              $question=Questions::model()->find(array('condition'=>"TM_QN_Syllabus_Id='".$model['TM_MK_Syllabus_Id']."' AND TM_QN_Standard_Id='".$model['TM_MK_Standard_Id']."' AND TM_QN_Id='".$questid[0]."' "));
                                                if(count($question)>0):
                                                    $questionmock=new MockQuestions();
                                                    $questionmock->TM_MQ_Mock_Id=$model['TM_MK_Id'];
                                                    $questionmock->TM_MQ_Question_Id=$question->TM_QN_Id;
                                                    $questionmock->TM_MQ_Type=$question->TM_QN_Type_Id;
                                                    $questionmock->TM_MQ_Order=($j+1);
                                                    $questionmock->save(false);
                                                endif;
                                        }
                                      $importlog[$count]['worksheet']='1';
                                    endif;
                                endif;
                            endif;
                      endif;
                    }
                    $count++;
                }
            }
        }
        $this->render('importworksheet',array('importlog'=>$importlog));
    }

    //consolidatedreport
    public function actionConsolidatedreport()
    {
        // $this->layout = '//layouts/admincolumn2';
        if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
            $this->layout = '//layouts/schoolcolumn2';
        endif;
        $connection = CActiveRecord::getDbConnection();
        $dataval=array();
        if(isset($_GET['search']))
        {

            $frDate=date_create($_GET['fromdate']);
            $frDateFormated = date_format($frDate,"Y-m-d");

            $toDate=date_create($_GET['todate']);
            $toDateFormated = date_format($toDate,"Y-m-d");

            // echo $frDateFormated; exit;

            $formvals=array();
            $formvals['date']=$_GET['date'];
            $formvals['school']=$_GET['school'];
            $formvals['schooltext']=$_GET['schooltext'];
            $formvals['teachername']=$_GET['teachername'];
            $formvals['plan']=$_GET['plan'];
            $formvals['fromdate']=$frDateFormated;
            $formvals['todate']=$toDateFormated;

            $plan = Plan::model()->findByPk($_GET['plan']);
            $standard=Standard::model()->findByPk($plan->TM_PN_Standard_Id);

            $school=School::model()->findByPk($_GET['school']);
            $condition = '';

            if($_GET['teachername'] != '')
                $condition ="AND a.teacher_id ='".$_GET['teachername']."' ";


            $sql="SELECT * FROM assessment_statistics AS a
             LEFT JOIN  tm_schoolhomework AS b ON b.TM_SCH_Id=a.schoolhomework_id
             WHERE b.TM_SCH_PublishDate >= '".$frDateFormated."' AND b.TM_SCH_PublishDate <= '".$toDateFormated."' AND b.TM_SCH_School_Id = '".$_GET['school']."' AND b.TM_SCH_Standard_Id = '".$standard->TM_SD_Id."' AND b.TM_SCH_Status IN(5,2) ".$condition." ORDER BY DATE(TM_SCH_PublishDate) DESC";
            ///echo $sql;exit;
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();

            $header = "<h3 style='text-align:center;'>".$school->TM_SCL_Name."</h3>";

            $html2 = '<br><h4 style="text-align:center;">THEMATIC REPORT</h4>
                    <table class="commonTab" style="width:100%;font-size:12px;"><thead><tr><th> Published
Date</th><th>Completed Date</th><th>Group</th><th>Teacher</th><th>Name of assignment</th><th>No of students completed</th><th>No of students not attempted</th><th>Average Score %</th></tr></thead><tbody>';
            $totalStudents = 0;
            $attemptedStudents = 0;
            $totalAvgScore = 0;
            $ids = '';
            if(count($dataval) > 0)
            {
                $count = 0;
                foreach ($dataval as $data) {

                    $groupNames = '';
                    if($count == 0)
                        $ids .= $data['id'];
                    else
                        $ids .= ','.$data['id'];

                    if($data['group_ids'] != '')
                    {
                        $str_arr = explode (",", $data['group_ids']);
                        if(count($str_arr) > 0)
                        {
                            $num = 0;
                            foreach ($str_arr as $group) {
                                $groupName = SchoolGroups::model()->findByPK($group)->TM_SCL_GRP_Name;
                                if($num == 0)
                                    $groupNames .= $groupName;
                                else
                                    $groupNames .= ','.$groupName;
                                $num++;
                            }
                        }
                    }

                    $count++;
                    $teacher = Teachers::model()->find(['condition' => "TM_TH_User_Id='".$data['teacher_id']."' "]);

                    $html2 .= '<tr>
                        <td>'.date_format(date_create($data['TM_SCH_PublishDate']),"d/m/Y").'</td>
                        <td>'.date_format(date_create($data['TM_SCH_CompletedOn']),"d/m/Y").'</td>
                        <td>'.$groupNames.'</td>
                        <td>'.$teacher->TM_TH_Name.'</td>
                        <td><a target="_blank" style="text-decoration: none;color:black;" href="'.Yii::app()->request->baseUrl.'/teachers/assessmentreportpdf/'.$data['TM_SCH_Id'].'">'.$data['TM_SCH_Name'].'</a></td>
                        <td style="text-align:center;">'.$data['students_completed'].'</td>
                        <td style="text-align:center;">'.$data['students_not_attempted'].'</td>
                        <td style="text-align:center;">'.round($data['average_score'],2).'%</td>
                        </tr>';
                    $totalStudents += $data['students_completed'] + $data['students_not_attempted'];
                    $attemptedStudents += $data['students_completed'];
                    $totalAvgScore += $data['average_score'];
                }

                $html2 .= '</tbody></table>
                <pagebreak>';
                if($attemptedStudents>0 && $totalStudents>0)
                {
                    $averageAttempted = ($attemptedStudents/$totalStudents)*100;
                }
                $html .= '<style>
                            .commonTab{
                                border-collapse: collapse;
                            }
                            .commonTab,.commonTab th,.commonTab td {
                              border: 1px solid black;
                            }
                            .backgroundGrey{
                                background-color:lightgrey;
                            }
                        </style>';
                // sub level

                $sql2="SELECT * FROM assesment_statistics_questions AS a
                 LEFT JOIN  tm_topic AS b ON b.TM_TP_Id=a.chapter_id
                 WHERE a.assessment_statistics_id IN (".$ids.") GROUP BY a.chapter_id";

                $command=$connection->createCommand($sql2);
                $dataReader=$command->query();
                $chapters=$dataReader->readAll();

                $totalUniqQuestions = 0;
                $html3 = '';

                if(count($chapters) > 0)
                {
                    foreach ($chapters as $chapter) {

                        $totalChapterQuestions = 0;
                        $basicChapterQuesCount = 0;
                        $interChapterQuesCount = 0;
                        $advanceChapterQuestCount = 0;

                        $averageScoreBasicSum = 0;
                        $basicCount = 0;
                        $averageScoreInterSum = 0;
                        $interCount = 0;
                        $averageScoreAdvSum = 0;
                        $advCount = 0;
                        $averageScoreSum = 0;

                        $chapterHtml = '';
                        $topicHtml = '';

                        $sql3="SELECT * FROM assesment_statistics_questions AS a
                         LEFT JOIN  tm_section AS b ON b.TM_SN_Id=a.topic_id
                         WHERE a.assessment_statistics_id IN (".$ids.") AND a.chapter_id = '".$chapter['TM_TP_Id']."' GROUP BY a.topic_id";

                        $command=$connection->createCommand($sql3);
                        $dataReader=$command->query();
                        $topics=$dataReader->readAll();



                        if(count($topics) > 0)
                        {
                            foreach ($topics as $topic) {

                                $sql4="SELECT *,SUM(total_attempted) AS total_attempted, SUM(total_corrected) AS total_corrected FROM assesment_statistics_questions AS a
                                 LEFT JOIN  tm_question AS b ON b.TM_QN_Id=a.question_id
                                 WHERE a.assessment_statistics_id IN (".$ids.") AND a.chapter_id = '".$chapter['TM_TP_Id']."' AND a.topic_id = '".$topic['topic_id']."' GROUP BY question_id";
                                $command=$connection->createCommand($sql4);
                                $dataReader=$command->query();
                                $questions=$dataReader->readAll();
                                $takenQuestion = [];
                                $totalTopicQuestions = 0;

                                $basicQuesCount = 0;
                                $interQuesCount = 0;
                                $advanceQuestCount = 0;

                                $averageBasic = 0;
                                $countBasic = 0;

                                $averageInter = 0;
                                $countInter = 0;

                                $averageAdv = 0;
                                $countAdv = 0;

                                $totalAvg = 0;


                                if(count($questions) > 0)
                                {
                                    

                                    foreach ($questions as $question) {

                                        $attemptTot = $question['total_attempted'] * $question['TM_QN_Totalmarks'];

                                        $correctTot = $question['total_corrected'] * $question['TM_QN_Totalmarks'];

                                        $average = 0;
                                        if($attemptTot != 0)
                                            $average = ($correctTot/$attemptTot)*100;


                                        if (!in_array($question['question_id'], $takenQuestion) && !in_array($question['parent'], $takenQuestion))
                                        {

                                            $totalTopicQuestions++;

                                            if($question['TM_QN_Dificulty_Id'] == 1){
                                                $averageBasic += $average;
                                                $countBasic++;
                                                $basicQuesCount++;
                                            }
                                            elseif($question['TM_QN_Dificulty_Id'] == 2){
                                                $averageInter += $average;
                                                // echo $attemptTot.'-'.$correctTot.'<br>';
                                                $countInter++;
                                                $interQuesCount++;
                                            }
                                            elseif($question['TM_QN_Dificulty_Id'] == 3){
                                                $averageAdv += $average;
                                                $countAdv++;
                                                $advanceQuestCount++;
                                            }

                                            $totalAvg += $average;

                                            array_push($takenQuestion,$question['question_id']);
                                        }
                                    }
                                }

                                $averageScoreBasic = 0;
                                $averageScoreInter = 0;
                                $averageScoreAdv = 0;
                                $averageScore = 0;

                                if($countBasic > 0){
                                    $averageScoreBasic = $averageBasic/$countBasic;
                                    $basicCount++;
                                }
                                if($countInter > 0){
                                    $averageScoreInter = $averageInter/$countInter;
                                    $interCount++;
                                }
                                if($countAdv > 0){
                                    $averageScoreAdv = $averageAdv/$countAdv;
                                    $advCount++;
                                }
                                if(count($questions) > 0)

                                    $averageScore = $totalAvg/count($questions);
                                $averageScoreBasicSum += $averageScoreBasic;
                                $averageScoreInterSum += $averageScoreInter;
                                $averageScoreAdvSum += $averageScoreAdv;
                                $averageScoreSum += $averageScore;

                                $totalChapterQuestions += $totalTopicQuestions;
                                $totalUniqQuestions += $totalTopicQuestions;
                                $basicChapterQuesCount += $basicQuesCount;
                                $interChapterQuesCount += $interQuesCount;
                                $advanceChapterQuestCount += $advanceQuestCount;

                                $topicHtml .= '<tr>
                                   <td></td>
                                   <td>'.$topic['TM_SN_Name'].'</td>
                                   <td style="text-align:center;">'.$totalTopicQuestions.'</td>
                                   <td style="text-align:center;">'.$basicQuesCount.'</td>
                                   <td style="text-align:center;">'.$interQuesCount.'</td>
                                   <td style="text-align:center;">'.$advanceQuestCount.'</td>
                                   <td style="text-align:center;">'.round($averageScore, 2).'%</td>';
                                if($countBasic > 0){
                                    $topicHtml .= '<td style="text-align:center;">'.round($averageScoreBasic, 2).'%</td>';
                                }
                                else {
                                    $topicHtml .= '<td style="text-align:center;">-</td>';
                                }
                                if($countInter > 0){
                                    $topicHtml .= '<td style="text-align:center;">'.round($averageScoreInter, 2).'%</td>';
                                }
                                else{
                                    $topicHtml .= '<td style="text-align:center;">-</td>';
                                }
                                if($countAdv > 0){
                                    $topicHtml .= '<td style="text-align:center;">'.round($averageScoreAdv, 2).'%</td>';
                                }
                                else{
                                    $topicHtml .= '<td style="text-align:center;">-</td>';
                                }
                                $topicHtml .= '</tr>';
                            }

                            $chapterHtml .= '<tr>
                                   <td class="backgroundGrey">'.$chapter['TM_TP_Name'].'</td>
                                   <td class="backgroundGrey"></td>
                                   <td class="backgroundGrey" style="text-align:center;"><b>'.$totalChapterQuestions.'</b></td>
                                   <td class="backgroundGrey" style="text-align:center;"><b>'.$basicChapterQuesCount.'</b></td>
                                   <td class="backgroundGrey" style="text-align:center;"><b>'.$interChapterQuesCount.'</b></td>
                                   <td class="backgroundGrey" style="text-align:center;"><b>'.$advanceChapterQuestCount.'</b></td>
                                   <td class="backgroundGrey" style="text-align:center;"><b>'.round($averageScoreSum/count($topics),2).'%</b></td>';

                            if($basicCount != 0){
                                $chapterHtml .= '<td class="backgroundGrey" style="text-align:center;"><b>'.round($averageScoreBasicSum/$basicCount,2).'%</b></td>';
                            }
                            else {
                                $chapterHtml .= '<td class="backgroundGrey" style="text-align:center;"><b>-</b></td>';
                            }
                            if($interCount != 0)
                            {
                                $chapterHtml .='<td class="backgroundGrey" style="text-align:center;"><b>'.round($averageScoreInterSum/$interCount,2).'%</b></td>';
                            }
                            else {
                                $chapterHtml .= '<td class="backgroundGrey" style="text-align:center;"><b>-</b></td>';
                            }

                            if($advCount != 0){
                                $chapterHtml .='<td class="backgroundGrey" style="text-align:center;"><b>'.round($averageScoreAdvSum/$advCount,2).'%</b></td>
                                </tr>';
                            }
                            else
                            {
                                $chapterHtml .='<td class="backgroundGrey" style="text-align:center;"><b>-</b></td>
                                </tr>';
                            }

                        }

                        $html3 .= $chapterHtml.$topicHtml;
                    }
                }

                $html1 = '<br>
                        <table style="width:100%;font-size:13px;">
                        <tbody>
                        <tr>
                        <td><b>Standard : </b>'.$standard->TM_SD_Name.'</td>
                        <td style="text-align:right;"><strong>Date Range : </strong>'.date_format(date_create($_GET['fromdate']),"d/m/Y").' - '.date_format(date_create($_GET['todate']),"d/m/Y").' </td></tr>
                        <tr>
                        <td><strong>Total No of assignments set : </strong>'.count($dataval).'</td>
                        <td></td>
                        </tr>
                        <tr><td><strong>Average score from all assignments : </strong>'.round($totalAvgScore / count($dataval),2).'%</td>
                        <td style="text-align:right;"><strong>Total no of questions attempted : </strong>'.$totalUniqQuestions.'</td>
                        </tr>
                        </tbody>
                        </table>';
                $html .= $html1.$html2.$html1;
                $html .= '<br>
                    <h4 style="text-align:center;">CHAPTER & TOPIC DISTRIBUTION</h4>
                <table class="commonTab" style="width:100%;font-size:12px;">
                <thead>
                <tr>
                    <th>Chapter</th>
                    <th>Topic Name</th>
                    <th colspan="4">NO of questions set from various assignments</th>
                    <th colspan="4">Average Score (%)</th>
                </tr>
                <tr>
                   <th></th>
                   <th></th>
                   <th>Total</th>
                   <th>Basic</th>
                   <th>Int</th>
                   <th>Adv</th>
                   <th></th>
                   <th>Basic</th>
                   <th>Int</th>
                   <th>Adv</th>
                </tr>
                </thead><tbody>';
                $html .= $html3;

            }

            $html .= '</tbody></table>';

            $mpdf = new mPDF();
            $mpdf->falseBoldWeight = 4;
            $mpdf->SetHTMLHeader($header);
            // $mpdf->SetWatermarkText($username, .1);
            // $mpdf->showWatermarkText = false;

            $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
             <tr>
                 <td width="100%" colspan="2">
                     &copy; Copyright @ TabbieMe Ltd. All rights reserved .
                 </td>
             </tr>
             <tr>
                 <td width="80%" >
                     www.tabbiemath.com  - The one stop shop for Maths Revision.
                 </td>
                    <td align="right" >Page #{PAGENO}</td>
             </tr>
            </table>');

            //echo $html;exit;
            $name='Report-'.date("d/m/Y").'.pdf';
            $mpdf->WriteHTML($html);
            $mpdf->Output($name, 'I');
        }
        $this->render('consolidated',array(
            // 'model'=>$model,
        ));
    }
    public function actionSchoolplan()
    {
        $schoolid = $_POST['id'];
        $schoolplans= SchoolPlan::model()->findAll(array('condition' => 'TM_SPN_SchoolId=' .$schoolid.' AND TM_SPN_Status=0'));
        $planslist ='<option value="">Select Class</option>';
        foreach($schoolplans as $schoolplan):
            $plan = Plan::model()->findByPk($schoolplan->TM_SPN_PlanId);
            $planslist .= "<option value='".$plan->TM_PN_Id."'>".$plan->TM_PN_Name."</option>";
        endforeach;
        $teachers = Teachers::model()->findAll(['condition' => 'TM_TH_SchoolId='.$schoolid.' AND TM_TH_Status=0']);
        $teacherslist ='<option value="">Select Teacher</option>';
        foreach($teachers as $teacher):
            $teacherslist .= "<option value='".$teacher->TM_TH_User_Id."'>".$teacher->TM_TH_Name."</option>";
        endforeach;
        $array=array('success'=>'yes','planslist'=>$planslist,'teacherslist'=>$teacherslist);
        echo json_encode($array);
    }

    /*public function actionSortorder()
    {
        //$mocks=Mock::model()->findAll(array('condition'=>'TM_MK_Publisher_Id=3 AND TM_MK_Syllabus_Id=4 AND TM_MK_Standard_Id=10 AND TM_MK_Resource=0','order'=>'TM_MK_Sort_Order ASC'));
        $mocks=Mock::model()->findAll(array('condition'=>'TM_MK_Standard_Id=10 AND TM_MK_Resource=1','order'=>'TM_MK_Sort_Order ASC'));
        echo count($mocks);
        foreach($mocks AS $key=>$mock):
            $mock->TM_MK_Sort_Order=($key + 1);
            $mock->save(false);
            echo "Mockid : ".$mock['TM_MK_Id']." Mock Name : ".$mock['TM_MK_Name']." Sortorder : ".$mock['TM_MK_Sort_Order']."<br>";
        endforeach;
    }*/
    public function actionAddSection()
    {
        $mockid=$_POST['practiceid'];
        $questionid=$_POST['qnids'];
        $questionhomework=MockQuestions::model()->find(array('condition'=>"TM_MQ_Mock_Id='".$mockid."' AND TM_MQ_Question_Id='".$questionid."' "));
        $questionhomework->TM_MQ_Section=$_POST['section'];
        $questionhomework->TM_MQ_Header=$_POST['header'];
        if($questionhomework->save(false)):
            echo "yes";
        endif;
    }
    public function actionGetHeaderCustom()
    {
        $mockid=$_POST['homeworkid'];
        $questionid=$_POST['questionid'];
        $questionhomework=MockQuestions::model()->find(array('condition'=>"TM_MQ_Mock_Id='".$mockid."' AND TM_MQ_Question_Id='".$questionid."' "));
        $header=$questionhomework['TM_MQ_Header'];
        $section=$questionhomework['TM_MQ_Section'];
        $data=array('header'=>$header,'section'=>$section);
        echo json_encode($data);
    }
    public function actionDeleteheader()
    {
        $mockid=$_POST['homeworkid'];
        $questionid=$_POST['questionid'];
        $questionhomework=MockQuestions::model()->find(array('condition'=>"TM_MQ_Mock_Id='".$mockid."' AND TM_MQ_Question_Id='".$questionid."' "));
        $questionhomework->TM_MQ_Header='';
        $questionhomework->TM_MQ_Section='';
        $questionhomework->save(false);
        $data=array($res='success');
        echo json_encode($data);

    }

    public function actionQstnoptions()
    {
        $mockid=$_POST['practiceid'];
        $questionid=$_POST['qnids'];
        $questionoptions=MockQuestions::model()->find(array('condition'=>"TM_MQ_Mock_Id='".$mockid."' AND TM_MQ_Question_Id='".$questionid."' "));
        $questionoptions->TM_MQ_Show_Options=$_POST['option'];
        $questionoptions->save(false);
        $data=array($res='success');
        echo json_encode($data);

    }


}
