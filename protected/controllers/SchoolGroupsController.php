<?php

class SchoolGroupsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','ManageGroups','AddGroups','Addstudents','Viewgroup','Editgroup','Deletegroup'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
    public function actionManageGroups($id)
    {
        $teacheradmin=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin() || $teacheradmin=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn2';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';
            $teacher=1;
        endif;
        $model=new SchoolGroups('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['SchoolGroups']))
            $model->attributes=$_GET['SchoolGroups'];
        $this->render('grouplist', array(
            'id' => $id,
            'model' => $model,
            'teacher' => $teacher,
        ));
    }

    public function actionViewgroup($id)
    {
        if(Yii::app()->user->isSchoolAdmin($id)):
            $this->layout = '//layouts/schoolcolumn2';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';                            
        endif;
        if(isset($_GET['group'])):
            $school=$id;
            $schoolgroup=SchoolGroups::model()->find(array('condition'=>'TM_SCL_Id='.$school.' AND TM_SCL_GRP_Id='.$_GET['group']));
            $this->render('groupview',array(
                'school'=>$school,
                'schoolgroup'=>$schoolgroup
            ));
        else:
            $this->redirect(array('admin','id'=>$id));
        endif;
    }

   public function actionEditgroup($id)
    {
        $teacheradmin=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin($id) || $teacheradmin=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn2';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';                            
        endif;
        if(isset($_GET['group'])):
            $school=$id;
            //$schoolgroup=SchoolGroups::model()->find(array('condition'=>'TM_SCL_Id='.$school.' AND TM_SCL_GRP_Id='.$_GET['group']));
            $model = $this->loadModel($_GET['group']);
            if(isset($_POST['SchoolGroups']))
            {
                $model->attributes=$_POST['SchoolGroups'];
                $model->TM_SCL_GRP_Name=$_POST['SchoolGroups']['TM_SCL_GRP_Name'];
                //$model->TM_SCL_Id=$id;
                $model->TM_SCL_SD_Id=$_POST['Standard'];
                $plan=SchoolPlan::model()->findByAttributes(array('TM_SPN_StandardId'=>$_POST['Standard']));
                $model->TM_SCL_PN_Id=$plan['TM_SPN_PlanId'];
                //print_r($model->TM_SCL_PN_Id);exit;
                if($model->save())
                    $this->redirect(array('addstudents','id'=>$id,'group'=>$_GET['group']));
            }
            $this->render('editgroup',array(
                'school'=>$school,
                'model'=>$model
            ));
        else:
            $this->redirect(array('admin','id'=>$id));
        endif;
    }

    public function actionDeletegroup($id)
    {
            // we only allow deletion via POST request
            $groupid=$_GET['group'];
            $schoolgroups=SchoolGroups::model()->find(array("condition"=>"TM_SCL_GRP_Id = $groupid"));
            $schoolgroups->delete();
            $connection = CActiveRecord::getDbConnection();
            $sql="DELETE FROM tm_group_students WHERE TM_GRP_Id='$groupid'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_POST['ajax']))
                $this->redirect(array('manageGroups','id'=>$id));     
    }

    public function actionAddGroups($id)
    {
        $teacheradmin=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin() || $teacheradmin=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn2';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';                                 
        endif;
        $model = new SchoolGroups();
        if(isset($_POST['SchoolGroups']))
        {
            $model->attributes=$_POST['SchoolGroups'];
            $model->TM_SCL_GRP_Name=$_POST['SchoolGroups']['TM_SCL_GRP_Name'];
            $model->TM_SCL_Id=$id;
            $model->TM_SCL_SD_Id=$_POST['Standard'];
            $plan=SchoolPlan::model()->findByAttributes(array('TM_SPN_StandardId'=>$_POST['Standard'],'TM_SPN_Status'=>0));
            ///print_r($plan);exit;
            $model->TM_SCL_PN_Id=$plan['TM_SPN_PlanId'];
            //print_r($model->TM_SCL_PN_Id);exit;
            if($model->save())
                $this->redirect(array('addstudents','id'=>$id,'group'=>$model->TM_SCL_GRP_Id));
        }
        $this->render('addgroups', array('id' => $id,'model' => $model,));
    }

    public function actionAddstudents($id)
    {
        $teacheradmin=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin() || $teacheradmin=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn2';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';
        endif;        
        $connection = CActiveRecord::getDbConnection();
        $groupid=$_GET['group'];
        $model=$this->loadModel($groupid);
        $schoolid=$id;
        // $grpstudents=GroupStudents::model()->findAll(array("condition"=>"TM_GRP_Id = $groupid"));
        // $addedstudents='';
        // foreach($grpstudents AS $key=>$grpst):
        //     $addedstudents.=($key=='0'?$grpst->TM_GRP_STUId:','.$grpst->TM_GRP_STUId);
        // endforeach;

         $sql="SELECT * FROM tm_group_students AS a INNER JOIN tm_users AS c on a.TM_GRP_STUId=c.id WHERE a.TM_GRP_Id='".$groupid."' AND c.status = 1 ";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $grpstudents=$dataReader->readAll();

        $addedstudents='';
        foreach($grpstudents AS $key=>$grpst):
            $addedstudents.=($key=='0'?$grpst['TM_GRP_STUId']:','.$grpst['TM_GRP_STUId']);
        endforeach;
        
        //$students=Student::model()->findAll(array("condition"=>"TM_STU_School_Id = $schoolid"));
        $sql="SELECT a.TM_STU_Id,a.TM_STU_First_Name,a.TM_STU_Last_Name FROM tm_student AS a LEFT JOIN tm_student_plan AS b ON b.TM_SPN_StudentId=a.TM_STU_User_Id INNER JOIN tm_users AS c on a.TM_STU_User_Id=c.id WHERE  b.TM_SPN_PlanId='".$model->TM_SCL_PN_Id."' AND c.school_id='$id' AND c.status = 1 ";
        if($addedstudents!=''):
            $sql.=" AND a.TM_STU_User_Id NOT IN(".$addedstudents.")";
        endif;
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $students=$dataReader->readAll();
        if(!empty($_POST['students']))
        {
            $studid=explode(',', $_POST['students']);
            for($i=0;$i<count($studid);$i++)
            {
                $group=New GroupStudents();
                $group->TM_GRP_Id=$groupid;
                $group->TM_GRP_SCL_Id=$id;
                $student=Student::model()->findByAttributes(array('TM_STU_Id'=>$studid[$i]));
                $group->TM_GRP_STUId=$student['TM_STU_User_Id'];
                $group->TM_GRP_STUName=$student['TM_STU_First_Name'].' '.$student['TM_STU_Last_Name'];
                $group->TM_GRP_SD_Id=$model->TM_SCL_SD_Id;
                $group->TM_GRP_PN_Id=$model->TM_SCL_PN_Id;
                $group->save(false);
                $homeworks=Homeworkgroup::model()->findAll(array('condition'=>'TM_HMG_Groupe_Id='.$groupid.' '));
                if(count($homeworks)>0):
                    foreach($homeworks AS $homework):
                        $schhomeworks=Schoolhomework::model()->find(array('condition'=>'TM_SCH_Id='.$homework['TM_HMG_Homework_Id'].' AND TM_SCH_Status=1'));
                        if(count($schhomeworks)>0):
                            $studenthw=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$schhomeworks['TM_SCH_Id'].''));
                            $studhomework=New StudentHomeworks();
                            $studhomework->TM_STHW_Student_Id=$student['TM_STU_User_Id'];
                            $studhomework->TM_STHW_HomeWork_Id=$schhomeworks['TM_SCH_Id'];
                            $studhomework->TM_STHW_Plan_Id=$studenthw['TM_STHW_Plan_Id'];
                            /*if($studenthw['TM_STHW_PublishDate']==date('Y-m-d')):
                                $studhomework->TM_STHW_Status=0;
                            else:
                                $studhomework->TM_STHW_Status=7;
                            endif;*/
                            if(date('Y-m-d')>=$studenthw['TM_STHW_PublishDate']):
                                $studhomework->TM_STHW_Status=0;
                            else:
                                $studhomework->TM_STHW_Status=7;
                            endif;
                            $studhomework->TM_STHW_Assigned_By=$studenthw['TM_STHW_Assigned_By'];
                            $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                            $studhomework->TM_STHW_DueDate=$studenthw['TM_STHW_DueDate'];
                            $studhomework->TM_STHW_Comments=$studenthw['TM_STHW_Comments'];
                            $studhomework->TM_STHW_PublishDate=$studenthw['TM_STHW_PublishDate'];
                            $studhomework->TM_STHW_Type=$studenthw['TM_STHW_Type'];
                            $studhomework->TM_STHW_ShowSolution=$studenthw['TM_STHW_ShowSolution'];
                            $studhw=StudentHomeworks::model()->find(array('condition' => 'TM_STHW_HomeWork_Id='.$schhomeworks['TM_SCH_Id'].' AND TM_STHW_Student_Id='.$student['TM_STU_User_Id'].' '));
                            $count=count($studhw);
                            ///echo "success ".$groupid." student: ".$studid[$i]." count: ".$count;
                            if($count==0)
                            {
                                if(date('Y-m-d')>=$studenthw['TM_STHW_PublishDate']):
                                    $notification=new Notifications();
                                    $notification->TM_NT_User=$student['TM_STU_User_Id'];
                                    $notification->TM_NT_Type='HWAssign';
                                    $notification->TM_NT_Item_Id=$schhomeworks['TM_SCH_Id'];
                                    $notification->TM_NT_Target_Id=$studenthw['TM_STHW_Assigned_By'];
                                    $notification->TM_NT_Status='0';
                                    $notification->save(false);
                                endif;
                                $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => 'TM_HQ_Homework_Id='.$schhomeworks['TM_SCH_Id'].' '));
                                foreach($hwquestions AS $hwquestion):
                                    $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                                    $studhwqstns=New Studenthomeworkquestions();
                                    $studhwqstns->TM_STHWQT_Mock_Id=$schhomeworks['TM_SCH_Id'];
                                    $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                                    $studhwqstns->TM_STHWQT_Student_Id=$student['TM_STU_User_Id'];
                                    $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                                    $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                                    $studhwqstns->save(false);
                                    $questions=Questions::model()->findAllByAttributes(array(
                                        'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                                    ));
                                    ///print_r($questions);
                                    if(count($questions)!=0):
                                        foreach($questions AS $childqstn):
                                            $studhwqstns=New Studenthomeworkquestions();
                                            $studhwqstns->TM_STHWQT_Mock_Id=$schhomeworks['TM_SCH_Id'];
                                            $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                            $studhwqstns->TM_STHWQT_Student_Id=$student['TM_STU_User_Id'];
                                            $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                            $studhwqstns->save(false);
                                        endforeach;
                                    endif;
                                endforeach;
                                $homeworktotal=$this->Gethomeworktotal($schhomeworks['TM_SCH_Id']);
                                $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                                $studhomework->save(false);
                            }

                        endif;
                    endforeach;
                endif;
            }
            $this->redirect(array('manageGroups','id'=>$id));
        }
        if(!empty($_POST['grpstudents']))
        {
            $studid=explode(',', $_POST['grpstudents']);
            for($i=0;$i<count($studid);$i++)
            {
                $grpstudent=GroupStudents::model()->findByAttributes(array('TM_GRP_STUId'=>$studid[$i]));
                $grpstudent->delete(false);
                $homeworks=Homeworkgroup::model()->findAll(array('condition'=>'TM_HMG_Groupe_Id='.$groupid.' '));
                if(count($homeworks)>0):
                    foreach($homeworks AS $homework):
                        StudentHomeworks::model()->deleteAll(array('condition' => 'TM_STHW_HomeWork_Id='.$homework['TM_HMG_Homework_Id'].' AND TM_STHW_Student_Id='.$studid[$i].' '));
                        Studenthomeworkquestions::model()->deleteAll(array('condition' => 'TM_STHWQT_Mock_Id='.$homework['TM_HMG_Homework_Id'].' AND TM_STHWQT_Student_Id='.$studid[$i].' '));
                    endforeach;
                endif;
            }
            $this->redirect(array('manageGroups','id'=>$id));
        }        
        $this->render('students',array(
            'model'=>$model,
            'students'=>$students,
            'grpstudents'=>$grpstudents,
        ));
    }
    public function Gethomeworktotal($homeworkid)
    {
        $questions= HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$homeworkid));
        $homeworktotal=0;
        foreach($questions AS $question):
            $question=Questions::model()->findByPk($question->TM_HQ_Question_Id);
            switch($question->TM_QN_Type_Id)
            {
                case 4:
                    $marks=Answers::model()->find(array('select'=>'TM_AR_Marks AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' '));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
                case 5:
                    $command = Yii::app()->db->createCommand();
                    $row = Yii::app()->db->createCommand(array(
                        'select' => array('SUM(TM_AR_Marks) AS totalmarks'),
                        'from' => 'tm_answer',
                        'where' => 'TM_AR_Question_Id IN (SELECT TM_QN_Id FROM 	tm_question WHERE TM_QN_Parent_Id='.$question->TM_QN_Id.')',
                    ))->queryRow();
                    $homeworktotal=$homeworktotal+$row['totalmarks'];
                    break;
                case 6:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                case 7:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                default:
                    $marks=Answers::model()->find(array('select'=>'SUM(TM_AR_Marks) AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' AND TM_AR_Correct=1'));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
            }
        endforeach;
        return $homeworktotal;
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SchoolGroups;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SchoolGroups']))
		{
			$model->attributes=$_POST['SchoolGroups'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_SCL_GRP_Id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SchoolGroups']))
		{
			$model->attributes=$_POST['SchoolGroups'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_SCL_GRP_Id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($group)
	{
		$this->loadModel($group)->delete();
        $connection = CActiveRecord::getDbConnection();
        $sql="DELETE FROM tm_group_students WHERE TM_GRP_Id='$group'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $school=$_GET['id'];
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('manageGroups','id'=>$school));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SchoolGroups');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SchoolGroups('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SchoolGroups']))
			$model->attributes=$_GET['SchoolGroups'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SchoolGroups the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SchoolGroups::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SchoolGroups $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='school-groups-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
