<?php

class ProfessionalController extends Controller
{
    public $layout='//layouts/prologincolumn1';
    public $defaultAction = 'login';

    public function actionLogin()
    {
        if (Yii::app()->user->isGuest) {
            $model=new UserLogin;
            /// collect user input data
            if(isset($_POST['UserLogin']))
            {
                $model->attributes=$_POST['UserLogin'];
                // validate user input and redirect to previous page if valid
                if($model->validate()) {
                    //print_r(Yii::app()->controller->module->returnUrlStudent);exit;
                    if (strpos(Yii::app()->user->returnUrl,'/index.php')!==false)
                        //echo "success1";exit;
                        if(Yii::app()->user->isStudent()):
                            $this->SetPro();
                            $this->SetPlan();
                            $this->lastViset();
                            $this->redirect(array('/home'));
                        elseif(Yii::app()->user->isSchoolAdmin()):
                            $this->SetTeacherPro();
                            $this->lastViset();
                            $this->redirect(array('/schoolhome'));
                        elseif(Yii::app()->user->isTeacher()):
                            $this->SetTeacherPro();
                            $this->lastViset();
                            $this->SetStandard();
                            $this->redirect(array('/teacherhome'));
                        else:
                            $this->redirect(array('/professional/logout'));
                        endif;
                    else
                    //echo "success2";exit;
                        if(Yii::app()->user->isStudent()):
                            $this->SetPro();
                            $this->SetPlan();
                            $this->lastViset();
                            $this->redirect(array('/home'));
                        elseif(Yii::app()->user->isSchoolAdmin()):
                            $this->SetTeacherPro();
                            $this->lastViset();
                            $this->redirect(array('/schoolhome'));
                        elseif(Yii::app()->user->isTeacher()):
                            $this->SetTeacherPro();
                            $this->lastViset();
                            $this->SetStandard();
                            $this->redirect(array('/teacherhome'));
                        else:
                            $this->redirect(array('/professional/logout'));
                        endif;
                }
            }
            // display the login form
            $this->render('/professional/login',array('model'=>$model));
        } else
        {
            ///print_r(Yii::app()->controller->module->returnUrlStudent);exit;
            if(Yii::app()->user->isStudent()):
                $this->SetPro();
                $this->SetPlan();
                $this->lastViset();
                $this->redirect(array('/home'));
            elseif(Yii::app()->user->isSchoolAdmin()):
                $this->SetTeacherPro();
                $this->lastViset();
                $this->redirect(array('/schoolhome'));
            elseif(Yii::app()->user->isTeacher()):
                $this->SetTeacherPro();
                $this->lastViset();
                $this->SetStandard();
                $this->redirect(array('/teacherhome'));
            else:
                $this->redirect(array('/professional/logout'));
            endif;
        }
    }

    public function actionLogout()
    {
        
        Yii::app()->user->logout();
        $this->redirect(array('/professional/login'));
    }

    public function lastViset() {
        $lastVisit = User::model()->findByPk(Yii::app()->user->id);
        $lastVisit->lastvisit = time();
        $lastVisit->save(false);
    }
    private function SetType()
    {
        Yii::app()->session['usertype']=User::model()->findbyPk(Yii::app()->user->id)->usertype;
    }
    private function SetPro()
    {
        Yii::app()->session['userlevel']=User::model()->findbyPk(Yii::app()->user->id)->userlevel;
    }
    private function SetTeacherPro()
    {
        Yii::app()->session['userlevel']=Teachers::model()->find(array('condition' => 'TM_TH_User_Id='.Yii::app()->user->id))->TM_TH_UserType;
    }
    private function SetPlan()
    {
        $plan=StudentPlan::model()->find(array('condition'=>'TM_SPN_StudentId='.Yii::app()->user->id,'limit'=>1,'order'=>'TM_SPN_CreatedOn DESC'));
        $masterplan=Plan::model()->findByPk($plan->TM_SPN_PlanId);

        $modules=Planmodules::model()->findAll(array('condition'=>'TM_PM_Plan_Id='.$plan->TM_SPN_PlanId));
        Yii::app()->session['plan'] = $plan->TM_SPN_PlanId;
        Yii::app()->session['publisher'] = $plan->TM_SPN_Publisher_Id;
        Yii::app()->session['syllabus'] = $plan->TM_SPN_SyllabusId;
        Yii::app()->session['standard'] = $plan->TM_SPN_StandardId;
        Yii::app()->session['subject'] = $plan->TM_SPN_SubjectId;
        if($masterplan->TM_PN_Type=='1'):
            Yii::app()->session['school'] = true;
        else:
            Yii::app()->session['school'] = false;
            foreach($modules AS $module):
                switch($module->TM_PM_Module)
                {
                    case "1":
                        Yii::app()->session['quick'] = true;
                        break;
                    case "2":
                        Yii::app()->session['difficulty'] = true;
                        break;
                    case "3":
                        Yii::app()->session['challenge'] = true;
                        break;
                    case "4":
                        Yii::app()->session['mock'] = true;
                        break;
                    case "5":
                        Yii::app()->session['resource'] = true;
                        break;
                    case "6":
                        Yii::app()->session['blueprint'] = true;
                        break;
                }
            endforeach;
        endif;
    }
    private function SetStandard()
    {
        $standards=TeacherStandards::model()->find(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id,'order'=>'TM_TE_ST_Id ASC'));
        $user=User::model()->findByPk(Yii::app()->user->id);
        $syllabus=Standard::model()->find(array('condition'=>'TM_SD_Id='.$standards->TM_TE_ST_Standard_Id));
        Yii::app()->session['standard'] = $standards->TM_TE_ST_Standard_Id;
        Yii::app()->session['school'] = $user->school_id;
        Yii::app()->session['syllabus'] = $syllabus->TM_SD_Syllabus_Id;
        Yii::app()->session['mode'] = "Teacher";
    }
}