<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/7/2015
 * Time: 12:37 PM
 */

class StudentController extends Controller {

    public $layout='//layouts/main';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + deletetest,deletechallenge,Cancelchallenge,Deletemock', // mewe only allow deletion via POST request
            //'postOnly + copy', // we only allow copy via POST request
        );
    }
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('home','Revision','RevisionTest','getTopics','TestComplete','deletetest','starttest','GetSolution','Challenge','showpaper','Getpdf','SendMail','Mock','Deletemock','startmock','Mocktest','MockComplete','GetmockSolution','History','GetpdfAnswer','Invitebuddies','ChallengeGetSolution','Redo','RevisionRedo','GetchallengepdfAnswer','RedoComplete','GetRedoSolution','ChangePlan','Leaderboard','MockRedo','showmockpaper','SendMailReminders','GetChallengernames'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudent()'
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Startchallenge','ChallengeTest','challengeresult','deletechallenge','Cancelchallenge','Acceptchallenge'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudentChalange($_GET["id"])'
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            )
        );
    }


    public function actionHome()
    {
        if(Yii::app()->session['quick'] || Yii::app()->session['difficulty']):
            $studenttests=StudentTest::model()->findAll(array('condition'=>"TM_STU_TT_Student_Id='".Yii::app()->user->id."' AND TM_STU_TT_Status NOT IN (1,2) AND TM_STU_TT_Plan='".Yii::app()->session['plan']."'"));
        endif;
        if(Yii::app()->session['mock']):
            $studentmocks=StudentMocks::model()->with('mocks')->findAll(array('condition'=>"TM_STMK_Student_Id='".Yii::app()->user->id."' AND TM_STMK_Status =0 AND TM_MK_Status='0' AND  TM_STMK_Plan_Id='".Yii::app()->session['plan']."'"));
        endif;
        if(Yii::app()->session['challenge']):
            $studentshallenges=Chalangerecepient::model()->with('challenge')->findAll(array('condition'=>"TM_CE_RE_Recepient_Id='".Yii::app()->user->id."' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0' AND TM_CL_Plan='".Yii::app()->session['plan']."'"));
        endif;
        $this->render('userhome',array('studenttests'=>$studenttests,'challenges'=>$studentshallenges,'studentmocks'=>$studentmocks));
    }
    public function actionRevision()
    {
        $plan=$this->GetStudentDet();
        $chapters=Chapter::model()->findAll(array('condition'=>"TM_TP_Syllabus_Id='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'"));
        if(isset($_POST['startTest'])):

            if($_POST['testtype']=='Difficulty'):
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['chapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Plan=Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode='0';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['chaptertotal'.$chapter->TM_TP_Id]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $topics='';

                        for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        echo $topics;
                        exit;

                        $totalquestions=$totalquestions+$_POST['basic'.$chapter->TM_TP_Id]+$_POST['inter'.$chapter->TM_TP_Id]+$_POST['adv'.$chapter->TM_TP_Id];
                        if($_POST['basic'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$_POST['basic'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['inter'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$_POST['inter'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['adv'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='3';
                            $limit=$_POST['adv'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $selectedquestions=base64_encode('Remove_'.base64_encode($totalquestions));
                    $addedquestions=base64_encode('Remove_'.base64_encode($count-1));
                    $this->redirect(array('starttest','id'=>$StudentTest->TM_STU_TT_Id,'inque'=>$selectedquestions,'outque'=>$addedquestions));
                endif;
            else:
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['quickchapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Plan=Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode='1';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['quickchaptertotal'.$chapter->TM_TP_Id]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $quicklimits=$this->GetQuickLimits($_POST['quickchaptertotal'.$chapter->TM_TP_Id]);
                        $topics='';
                        for($i=0;$i<count($_POST['quicktopic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['quicktopic'.$chapter->TM_TP_Id][$i]:",".$_POST['quicktopic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions=$totalquestions+$_POST['quick'.$chapter->TM_TP_Id];
                        if($quicklimits['basic']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$quicklimits['basic'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if($quicklimits['intermediate']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['intermediate'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if($quicklimits['advanced']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['advanced'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $selectedquestions=base64_encode('Remove_'.base64_encode($totalquestions));
                    $addedquestions=base64_encode('Remove_'.base64_encode($count-1));
                    $this->redirect(array('starttest','id'=>$StudentTest->TM_STU_TT_Id,'inque'=>$selectedquestions,'outque'=>$addedquestions));
                endif;
            endif;
            //$this->redirect(array('RevisionDifficulty','id'=>$StudentTest->TM_STU_TT_Id));
        elseif(isset($_POST['addTest'])):
            if($_POST['testtype']=='Difficulty'):
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['chapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Plan=Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode='0';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['chaptertotal'.$chapter->TM_TP_Id]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $topics='';

                        for($i=0;$i<count($_POST['topic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['topic'.$chapter->TM_TP_Id][$i]:",".$_POST['topic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';

                        $totalquestions=$totalquestions+$_POST['basic'.$chapter->TM_TP_Id]+$_POST['inter'.$chapter->TM_TP_Id]+$_POST['adv'.$chapter->TM_TP_Id];
                        if($_POST['basic'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$_POST['basic'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['inter'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$_POST['inter'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if($_POST['adv'.$chapter->TM_TP_Id]!='0'):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='3';
                            $limit=$_POST['adv'.$chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $this->redirect(array('home'));
                endif;
            else:
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id' ,$_POST['quickchapter'] );
                $criteria->addInCondition('TM_TP_Id' ,$_POST['quickchapter'] );
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest= new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id=Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id=$_POST['publisher'];
                $StudentTest->TM_STU_TT_Type='0';
                $StudentTest->TM_STU_TT_Plan=Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode='1';
                $StudentTest->save(false);
                $count=1;
                $totalquestions=0;
                foreach($selectedchapters AS $key=>$chapter):
                    if($_POST['quickchaptertotal'.$chapter->TM_TP_Id]!='0'):
                        $testchapter=new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id=Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id=$StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id=$chapter->TM_TP_Id;
                        $testchapter->save();
                        $quicklimits=$this->GetQuickLimits($_POST['quickchaptertotal'.$chapter->TM_TP_Id]);
                        $topics='';
                        for($i=0;$i<count($_POST['quicktopic'.$chapter->TM_TP_Id]);$i++):
                            $topics=$topics.($i==0?$_POST['quicktopic'.$chapter->TM_TP_Id][$i]:",".$_POST['quicktopic'.$chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions=$totalquestions+$_POST['quick'.$chapter->TM_TP_Id];
                        if($quicklimits['basic']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='1';
                            $limit=$quicklimits['basic'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if($quicklimits['intermediate']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['intermediate'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if($quicklimits['advanced']!=0):
                            $Syllabus= $plan->TM_SPN_SyllabusId;
                            $Standard= $plan->TM_SPN_StandardId;
                            $Chapter=$chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test=$StudentTest->TM_STU_TT_Id;
                            $Student=Yii::app()->user->id;
                            $dificulty='2';
                            $limit=$quicklimits['advanced'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'".$topics."',:limit,:Test,:Student,:dificulty,:questioncount,'1',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit",$limit);
                            $command->bindParam(":Test",$Test);
                            $command->bindParam(":Student",$Student);
                            $command->bindParam(":dificulty",$dificulty);
                            $command->bindParam(":questioncount",$count);
                            $command->query();
                            $count=$count+Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                    endif;
                endforeach;
                if($count!='0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $this->redirect(array('home'));
                endif;
            endif;
        endif;

        $this->render('revision',array('plan'=>$plan,'chapters'=>$chapters));
    }
    public function actionStarttest($id)
    {
        $test=StudentTest::model()->findByPk($id);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id='.$id.' AND TM_STU_QN_Type NOT IN (6,7)';
        $totalonline=count(StudentQuestions::model()->findAll($criteria));
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id='.$id.' AND TM_STU_QN_Type IN (6,7)';
        $totalpaper=count(StudentQuestions::model()->findAll($criteria));
        $totalquestions=$totalonline+$totalpaper;
        //$selectedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['inque'])));
        //$addedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['outque'])));
        $this->render('starttest',array('id'=>$id,'test'=>$test,'totalonline'=>$totalonline,'totalpaper'=>$totalpaper,'totalquestions'=>$totalquestions));

    }
    public function actionStartchallenge($id)
    {
        $test=Challenges::model()->findByPk($id);
        $this->render('startchallenge',array('id'=>$id,'test'=>$test));

    }
    public function actionRevisionTest($id)
    {
        if(isset($_POST['action']['GoNext']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = StudentQuestions::model()->findByAttributes(
                        array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new StudentQuestions();
                    endif;
                    $selectedquestion->TM_STU_QN_Test_Id=$id;
                    $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->TM_STU_QN_Flag='1';
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endif;
            $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                )
            );
        }
        if(isset($_POST['action']['GetPrevios'])):
            if($_POST['QuestionType']!='5'):
                if(isset($_POST['answer'])):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    if($_POST['QuestionType']=='1'):
                        $answer=implode(',',$_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                        $answer=$_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                else:
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    if(isset($_POST['answer'.$child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                        );
                        if(count($childquestion)=='1'):
                            $selectedquestion=$childquestion;
                        else:
                            $selectedquestion=new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id=$id;
                        $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                        if($child->TM_QN_Type_Id=='1'):
                            $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                            $answer=$_POST['answer'.$child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='4'):
                            $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endif;
            $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0,1,2) AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Number<:tableid AND TM_STU_QN_Parent_Id=0',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number DESC, TM_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                )
            );
        endif;
        if(isset($_POST['action']['DoLater'])):
            if($_POST['QuestionType']!='5'):
                if(isset($_POST['answer'])):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    if($_POST['QuestionType']=='1'):
                        $answer=implode(',',$_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                        $answer=$_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($_POST['QuestionType']=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                else:
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag='2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    if(isset($_POST['answer'.$child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                        );
                        if(count($childquestion)=='1'):
                            $selectedquestion=$childquestion;
                        else:
                            $selectedquestion=new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id=$id;
                        $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                        if($child->TM_QN_Type_Id=='1'):
                            $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                            $answer=$_POST['answer'.$child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                        elseif($child->TM_QN_Type_Id=='4'):
                            $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='2';
                $selectedquestion->save(false);
            endif;
            $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                array(
                    'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                )
            );
        endif;
        if (isset($_POST['action']['Skipcomplete'])):
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag = '2';
                $selectedquestion->save(false);
            endif;
            $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                )
            );
            if ($this->GetPaperCount($id) != 0):
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '2';
                $test->save(false);
                $this->redirect(array('showpaper', 'id' => $id));
            else:
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '1';
                $test->save(false);
                $this->redirect(array('TestComplete', 'id' => $id));
            endif;
        endif;
        if(isset($_POST['action']['Complete']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STU_QN_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = StudentQuestions::model()->findByAttributes(
                        array('TM_STU_QN_Question_Id'=>$child->TM_QN_Id,'TM_STU_QN_Test_Id'=>$id,'TM_STU_QN_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new StudentQuestions();
                    endif;
                    $selectedquestion->TM_STU_QN_Test_Id=$id;
                    $selectedquestion->TM_STU_QN_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STU_QN_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STU_QN_Answer_Id=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STU_QN_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STU_QN_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag='1';
                $selectedquestion->save(false);
            endif;
            if($this->GetPaperCount($id)!=0):
                $test=StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status='2';
                $test->save(false);
                $this->redirect(array('showpaper','id'=>$id));
            else:
                $test=StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status='1';
                $test->save(false);
                $this->redirect(array('TestComplete','id'=>$id));
            endif;
            //$this->redirect(array('TestComplete','id'=>$id));
        }
        if(!isset($_POST['action'])):
            if(isset($_GET['questionnum'])):
                $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                    array(
                        'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0,1,2) AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number='.str_replace("question_","",base64_decode($_GET['questionnum'])),
                        'limit'=>1,
                        'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            else:
                $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$id),
                    array(
                        'condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0) AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Parent_Id=0',
                        'limit'=>1,
                        'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            endif;
        endif;
        if(count($questionid)):
            $question=Questions::model()->findByPk($questionid->TM_STU_QN_Question_Id);
            $this->renderPartial('showquestion',array('testid'=>$id,'question'=>$question,'questionid'=>$questionid));
        else:
            $this->redirect(array('revision'));
        endif;
    }


    public function actionTestComplete($id)
    {
        $results = StudentQuestions::model()->findAll(
            array(
                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7)',
                "order" => 'TM_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $totalmarks = 0;
        $earnedmarks = 0;
        $result = '';
        $marks=0;
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == 0):
                if ($question->TM_QN_Type_Id != '5'):
                    $displaymark = 0;
                    $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marksdisp AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;

                    endforeach;
                    $marks = 0;
                    if ($question->TM_QN_Type_Id != '4' & $exam->TM_STU_QN_Flag != '2'):
                        if ($exam->TM_STU_QN_Answer_Id != ''):

                            $studentanswer = explode(',', $exam->TM_STU_QN_Answer_Id);
                            foreach ($question->answers AS $ans):
                                if ($ans->TM_AR_Correct == '1'):
                                    if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    }
                                endif;
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh>';
                                for ($i = 0; $i < count($studentanswer); $i++)
                                {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);
                                    $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($exam->TM_STU_QN_Answer != ''):
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_STU_QN_Answer . '</div></li></ul>';
                            foreach ($question->answers AS $ans):
                                $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                $useranswer=  trim(strtolower($exam->TM_STU_QN_Answer));
                                for($i=0;$i<count($answeroption);$i++)
                                {
                                    $option= trim($answeroption[$i]);
                                    if(strcmp($useranswer,$option)==0):
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    endif;
                                }
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;

                    endif;
                    if ($question->TM_QN_Parent_Id == 0):
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';

                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STU_QN_Number . '</span>';
                        else:
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                        endif;

                        $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? ' fa fa-flag sty' : ' fa fa-flag stynone') . ' newtest" data-toggle="tooltip" data-placement="left" title=" Flag question."></span></a>      Mark: ' . $marks . '/' . $displaymark . '</div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                        $result .= '<div class="col-md-12 Itemtest clickable-row showalert' . $question->TM_QN_Id . '" >

								<div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2 mailadd" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                              </div>
                                 <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id=""  hidden >Your Message Has Been Sent To Admin
                                </div>  </div>
                             </td></tr>';

                        //glyphicon glyphicon-star sty
                    endif;

                    $exam->TM_STU_QN_Mark = $marks;
                    $exam->save(false);
                    $earnedmarks = $earnedmarks + $marks;

                else:
                    $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                    $displaymark = 0;
                    foreach ($marksQuestions AS $key => $formarks):

                        $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($gettingmarks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;



                    endforeach;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childresult='';
                    $childearnedmarks=0;
                    foreach ($questions AS $childquestion):
                        $childresults = StudentQuestions::model()->find(
                            array(
                                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Question_Id=:questionid',
                                'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                            )
                        );
                        $marks = 0;
                        if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_STU_QN_Flag != '2'):
                            if ($childresults->TM_STU_QN_Answer_Id != ''):

                                $studentanswer = explode(',', $childresults->TM_STU_QN_Answer_Id);
                                foreach ($childquestion->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++)
                                    {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($childresults->TM_STU_QN_Answer != ''):
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_STU_QN_Answer . '</li></ul>';
                                foreach ($childquestion->answers AS $ans):
                                    $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer=  trim(strtolower($childresults->TM_STU_QN_Answer));
                                    for($i=0;$i<count($answeroption);$i++)
                                    {
                                        $option= trim($answeroption[$i]);
                                        if(strcmp($useranswer,$option)==0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($childquestion->TM_QN_Parent_Id != 0):
                            $childresult .= '

                            <div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                            $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $childresult .= '';
                            //glyphicon glyphicon-star sty
                        endif;


                        $earnedmarks = $earnedmarks + $marks;
                        $childearnedmarks = $childearnedmarks + $marks;
                        $count++;
                    endforeach;
                    $marks=$childearnedmarks;
                    $result .= '<tr><td><div class="col-md-4">';
                    if ($marks == '0'):
                        $exam->TM_STU_QN_Redo_Flag = '1';
                        $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                    elseif ($marks < $displaymark & $marks != '0'):
                        $exam->TM_STU_QN_Redo_Flag = '1';
                        $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STU_QN_Number . '</span>';
                    else:
                        $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                    endif;

                    $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? ' fa fa-flag sty' : ' fa fa-flag stynone') . ' newtest" data-toggle="tooltip" data-placement="left" title=" Flag question."></span></a>    Mark: ' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                    //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                    $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                    $result .= $childresult.'</td></tr>';
                    $result .='<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div>

                            </div></div>
</td></tr>';
                    $exam->TM_STU_QN_Mark = $marks;
                    $exam->save(false);
                endif;


            endif;

        endforeach;
        $percentage = ($earnedmarks / $totalmarks) * 100;
        $test = StudentTest::model()->findByPk($id);
        $test->TM_STU_TT_Mark = $earnedmarks;
        $test->TM_STU_TT_TotalMarks = $totalmarks;
        $test->TM_STU_TT_Percentage = round($percentage, 1);
        $test->save(false);
        $pointlevels=$this->AddPoints($earnedmarks,$id,'Revision',Yii::app()->user->id);
        $this->render('showresult', array('test' => $test, 'testresult' => $result,'pointlevels'=>$pointlevels));
    }
    public function actionRedoComplete($id)
    {
        $results = StudentQuestions::model()->findAll(
            array(
                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7) AND TM_STU_QN_Redo_Flag=1',
                "order" => 'TM_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $totalmarks = 0;
        $earnedmarks = 0;
        $result = '';
        $resultsolution = '';
        $marks=0;
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == 0):
                if ($question->TM_QN_Type_Id != '5'):
                    $displaymark = 0;
                    $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marksdisp AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;

                    endforeach;
                    $marks = 0;
                    if ($question->TM_QN_Type_Id != '4' & $exam->TM_STU_QN_Flag != '2'):
                        if ($exam->TM_STU_QN_Answer_Id != ''):

                            $studentanswer = explode(',', $exam->TM_STU_QN_Answer_Id);
                            foreach ($question->answers AS $ans):
                                if ($ans->TM_AR_Correct == '1'):
                                    if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    }
                                endif;
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh>';
                                for ($i = 0; $i < count($studentanswer); $i++)
                                {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);
                                    $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($exam->TM_STU_QN_Answer != ''):
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_STU_QN_Answer . '</div></li></ul>';
                            foreach ($question->answers AS $ans):
                                $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                $useranswer=  trim(strtolower($exam->TM_STU_QN_Answer));
                                for($i=0;$i<count($answeroption);$i++)
                                {
                                    $option= trim($answeroption[$i]);
                                    if(strcmp($useranswer,$option)==0):
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    endif;
                                }
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;

                    endif;
                    if ($question->TM_QN_Parent_Id == 0):
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $exam->TM_STU_QN_Flag = '0';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';

                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $exam->TM_STU_QN_Flag = '0';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STU_QN_Number . '</span>';
                        else:
                            $exam->TM_STU_QN_Redo_Flag = '0';
                            $exam->TM_STU_QN_Flag = '1';
                            $exam->TM_STU_QN_RedoStatus = '0';
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                        endif;

                        $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? 'fa fa-flag sty' : 'fa fa-flag stynone') . ' newtest"  data-toggle="tooltip" data-placement="left" title=" Flag question." ></span></a>      Mark: ' . $marks . '/' . $displaymark . '</div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-8"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                        $result .= '<div class="col-md-12 Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2 mailadd" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                 <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div>
                            </div></div>
                             </td></tr>';

                        //glyphicon glyphicon-star sty
                    endif;

                    $exam->TM_STU_QN_Mark = $marks;
                    $exam->TM_STU_QN_RedoStatus = 0;
                    $exam->save(false);
                    $earnedmarks = $earnedmarks + $marks;

                else:
                    $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                    $displaymark = 0;
                    foreach ($marksQuestions AS $key => $formarks):

                        $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($gettingmarks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                    endforeach;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childresult='';
                    $childearnedmarks=0;
                    $childresulthtml='';
                    foreach ($questions AS $childquestion):
                        $childresults = StudentQuestions::model()->find(
                            array(
                                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Question_Id=:questionid',
                                'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                            )
                        );
                        $marks = 0;
                        if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_STU_QN_Flag != '2'):
                            if ($childresults->TM_STU_QN_Answer_Id != ''):

                                $studentanswer = explode(',', $childresults->TM_STU_QN_Answer_Id);
                                foreach ($childquestion->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++)
                                    {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($childresults->TM_STU_QN_Answer != ''):
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_STU_QN_Answer . '</li></ul>';
                                foreach ($childquestion->answers AS $ans):
                                    $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer=  trim(strtolower($childresults->TM_STU_QN_Answer));
                                    for($i=0;$i<count($answeroption);$i++)
                                    {
                                        $option= trim($answeroption[$i]);
                                        if(strcmp($useranswer,$option)==0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($childquestion->TM_QN_Parent_Id != 0):
                            $childresult .= '

                            <div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                            $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $childresult .= '';
                            //glyphicon glyphicon-star sty
                        endif;

                        $childresulthtml .='<div class="col-md-12 ">Part :'.$count.'</div>';
                        $childresulthtml .='<div class="col-md-12 padnone">';
                        $childresulthtml .= '<div class="col-md-' . ($childquestion->TM_QN_Image != '' ? '10' : '12') . '">' . $childquestion->TM_QN_Question . '</div>' . ($childquestion->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') ;
                        $childresulthtml .='</div>';
                        $earnedmarks = $earnedmarks + $marks;
                        $childearnedmarks = $childearnedmarks + $marks;
                        $count++;
                    endforeach;


                    $marks=$childearnedmarks;
                    $result .= '<tr><td><div class="col-md-4">';
                    if ($marks == '0'):
                        $exam->TM_STU_QN_Redo_Flag = '1';
                        $exam->TM_STU_QN_Flag = '0';
                        $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                    elseif ($marks < $displaymark & $marks != '0'):
                        $exam->TM_STU_QN_Redo_Flag = '1';
                        $exam->TM_STU_QN_Flag = '0';
                        $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STU_QN_Number . '</span>';
                    else:
                        $exam->TM_STU_QN_Redo_Flag = '0';
                        $exam->TM_STU_QN_Flag = '1';
                        $exam->TM_STU_QN_RedoStatus = '0';
                        $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                    endif;

                    $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? 'fa fa-flag sty' : 'fa fa-flag stynone') . ' newtest"  data-toggle="tooltip" data-placement="left" title=" Flag question." ></span></a>    Mark: ' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                    //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                    $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-8"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                    $result .= $childresult.'</td></tr>';
                    $result .='<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div>

                            </div></div>
                            </td></tr>';
                    $exam->TM_STU_QN_Mark = $marks;
                    $exam->save(false);
                endif;


            endif;

        endforeach;

        $test = StudentTest::model()->findByPk($id);
        $test->TM_STU_TT_Status='1';
        $test->save(false);

        $this->render('showresultredo', array('test' => $test, 'testresult' => $result,'solution'=>$resultsolution));
    }
    public function actionGetpdf($id)
    {
        $paperquestions = $this->GetPaperQuestions($id);
        $totalquestions=count($paperquestions);
        $nak = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;

        $html = "<table cellpadding='5' border='0' width='100%'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>PAPER QUESTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";



        foreach ($paperquestions AS $key => $paperquestion):

            $question = Questions::model()->findByPk($paperquestion->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):

                if ($question->TM_QN_Type_Id != '7'):
                    $displaymark = 0;
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marks AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $paperquestion->TM_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%'>Marks: ".$question->TM_QN_Totalmarks;
                    $html .= "</td><td align='right'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";


                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;



                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml="";
                    foreach ($questions AS $childquestion):
                        $displaymark=$displaymark+$childquestion->TM_QN_Totalmarks;
                        $childhtml .= "<tr><td colspan='3' width='5%'>Part." . $countpart;
                        $childhtml .= "</td><tr><td colspan='3' >";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";


                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question." . $paperquestion->TM_STU_QN_Number;
                    $html .= "</b></td><td width='15%'>Marks: ".$displaymark;
                    $html .= "</td><td  align='right'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";

                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .=$childhtml;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;
            endif;
        endforeach;
        $html .= "</table>";
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;  color: #afafaf; ">' . $firstname.' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf; ">' . $username . '</td>
</tr></table>';


        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username,.1);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">Disclaimer:This is the property ofTabbie Me Educational Solutions PVT Ltd.</span></td>
</tr></table>
');


        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf', 'D');

    }
    public function actionGetSolution()
    {
        if (isset($_POST['testid'])):
            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $_POST['testid'])
                )
            );
            $resulthtml = '';
            foreach ($results AS $result):
                $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '7'   ):
                        $resulthtml.='<tr><td><div class="col-md-3">
                    <span> <b>Question'.$result->TM_STU_QN_Number.'</b></span>
                    <span class="pull-right">Marks: '.$displaymark.'</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="'. $question->TM_QN_Id.'" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :'.$question->TM_QN_QuestionReff.'</span>
                    </div><div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .=  '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .='<div class="col-md-12 padnone">';

                        if($question->TM_QN_Solutions!=''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .='</div>';


                        $resulthtml.= '<div class="col-md-12 Itemtest clickable-row ">
                              <div class="sendmail suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comment"  id="comment" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div></div>

                        </td></tr>';
                    else:
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti=1;
                        $displaymark = 0;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;
                            $childresulthtml .='<div class="col-md-12 ">Part :'.$countmulti.'</div>';
                            $childresulthtml .='<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') ;
                            $childresulthtml .='</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question '.$result->TM_STU_QN_Number.'</b> </span>
                    <span class="pull-right">Marks: '.$displaymark.'</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="'. $question->TM_QN_Id.'" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :'.$question->TM_QN_QuestionReff.'</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .='<div class="col-md-12 padnone">';
                        if($question->TM_QN_Solutions!=''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .='</div>';

                        $resulthtml.= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comment"  id="comment" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div></div>
                        </td></tr>';

                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function actionGetmockSolution()
    {
        if(isset($_POST['testid'])):
            $results=MockQuestions::model()->findAll(
                array(
                    'condition'=>'TM_MQ_Mock_Id=:test',
                    'params'=>array(':test'=>$_POST['testid'])
                )
            );
            $resulthtml='';
            $count=0;
            foreach ($results AS $key=>$result):
                $count=$key+1;
                $question = Questions::model()->findByPk($result->TM_MQ_Question_Id);
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '7'   ):
                        $resulthtml.='<tr><td><div class="col-md-3">
                    <span> <b>Question'.$count.'</b></span>
                    <span class="pull-right">Marks: '.$displaymark.'</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="'. $question->TM_QN_Id.'" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :'.$question->TM_QN_QuestionReff.'</span>
                    </div><div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .=  '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .='<div class="col-md-12 padnone">';

                        if($question->TM_QN_Solutions!=''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .='</div>';


                        $resulthtml.= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comment" hidden/></div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden ></div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div>

                        </td></tr>';
                    else:
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $displaymark = 0;
                        $countmulti=1;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):
                            //$resulthtml .= '<tr><td>';
                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;

                            $childresulthtml .='<div class="col-md-12 ">Part :'.$countmulti.'</div>';
                            $childresulthtml .='<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') ;
                            $childresulthtml .='</div>';
                            $countmulti++;
                        endforeach;
                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question '.$count.'</b> </span>
                    <span class="pull-right">Marks: '.$displaymark.'</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="'. $question->TM_QN_Id.'" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :'.$question->TM_QN_QuestionReff.'</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .='<div class="col-md-12 padnone">';
                        if($question->TM_QN_Solutions!=''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .='</div>';

                        $resulthtml.= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comment" hidden/></div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden ></div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div>
                        </td></tr>';


                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function actionShowpaper($id)

    {

        $connection = CActiveRecord::getDbConnection();
        if(isset($_POST['coomplete']))
        {
            $nonpaperquestions=$this->GetNonPaperQuestions($id);
            $countnon=count($nonpaperquestions);

            if($countnon!=0): {
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '1';
                $test->save(false);
                $this->redirect(array('TestComplete', 'id' => $id));
            }
            else: {
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '1';
                $test->save(false);
                $studenttests = StudentTest::model()->findAll(array('condition' => "TM_STU_TT_Student_Id='" . Yii::app()->user->id . "' AND TM_STU_TT_Status!='1'"));
                $studentshallenges = Chalangerecepient::model()->with('challenge')->findAll(array('condition' => "TM_CE_RE_Recepient_Id='" . Yii::app()->user->id . "' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0'"));
                $studentmocks = StudentMocks::model()->with('mocks')->findAll(array('condition' => "TM_STMK_Student_Id='" . Yii::app()->user->id . "' AND TM_STMK_Status =0 AND TM_MK_Status='0'"));
                $this->render('userhome', array('studenttests' => $studenttests, 'challenges' => $studentshallenges, 'studentmocks' => $studentmocks));
            }

            endif;
        }

        $paperquestions=$this->GetPaperQuestions($id);


        $this->render('showpaperquestion',array('id'=>$id,'paperquestions'=>$paperquestions));

    }

    public function GetStudentDet()
    {
        $user=Yii::app()->user->id;
        $plan=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_Publisher_Id=:publisher AND TM_SPN_PlanId=:plan AND TM_SPN_SyllabusId=:syllabus AND TM_SPN_StandardId=:standard AND TM_SPN_SubjectId=:subject AND TM_SPN_StudentId=:user',
                        'params'=>array(':publisher'=>Yii::app()->session['publisher'],':plan'=>Yii::app()->session['plan'],':syllabus'=>Yii::app()->session['syllabus'],':standard'=>Yii::app()->session['standard'],':subject'=>Yii::app()->session['subject'],':user'=>$user)));
        return $plan[0];
    }
    public function actionGetChallengernames()
    {

        $arr =  $_POST['nameids'];
            $name='';




                if(count($arr)==1){
                    $name = User::model()->findbyPk($arr[0])->username;

        }
        else{
            $name='';
            for($i=0;$i<=count($arr);$i++){
                $name.=User::model()->findbyPk($arr[$i])->username.",";
            }
        }

        echo $name;
    }
    public function actionGetTopics()
    {
        $arr = explode(',', $_POST['topicids']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_TP_Id' ,$arr );
        $model = Chapter::model()->findAll($criteria);
        $topic='';
        foreach ($model as $key=>$value) {
            $topic=($key=='0'?$value->TM_TP_Name:$topic.','.$value->TM_TP_Name);
        }
        echo $topic;
    }
    public function GetPaperQuestions($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type IN (6,7) AND TM_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STU_QN_Number ASC';
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        return $selectedquestion;
    }
    public function GetNonPaperQuestions($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STU_QN_Number ASC';
        $nonpaperquestions=StudentQuestions::model()->findAll($criteria);
        return $nonpaperquestions;
    }
    public function GetPaperQuestionsMock($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type IN (6,7) AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Student_Id=:student';
        $criteria->params=array(':test'=>$id,'student'=>Yii::app()->user->id);
        $criteria->order='TM_STMKQT_Id ASC';
        $selectedquestion=Studentmockquestions::model()->findAll($criteria);
        return $selectedquestion;
    }
    public function OptionHint($type,$question)
    {
        $returnval=array();
        if($type=='1'):
            $corectans=Answers::model()->count(
                array(
                    'condition'=>'TM_AR_Question_Id=:question AND TM_AR_Correct=:correct',
                    'params'=>array(':question'=>$question,':correct'=>'1')
                ));
            $returnval['hint']='<span class="optionhint">Select any '.$corectans.' options</span>';
            $returnval['correctoptions']=$corectans;
        elseif($type=='2'):
            $returnval['hint']='<span class="optionhint">Select an option</span>';
            $returnval['correctoptions']='1';
        elseif($type=='3'):
            $returnval['hint']='<span class="optionhint">Select either True Or False</span>';
            $returnval['correctoptions']='1';
        elseif($type=='4'):
            $returnval['hint']='<span class="optionhint">Enter Your Answer</span>';
            $returnval['correctoptions']='0';
        elseif($type=='5'):
            $returnval['hint']='<span class="optionhint">Answer the following questions</span>';
            $returnval['correctoptions']='0';
        endif;
        return $returnval;
    }

    public function actionDeletetest($id)
    {
        if(StudentTest::model()->findByPk($id)->delete())
        {
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STC_Test_Id='.$id);
            $model = StudentTestChapters::model()->deleteAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_QN_Test_Id='.$id);
            $model = StudentQuestions::model()->deleteAll($criteria);
            echo "yes";
        }
        else
        {
            echo "no";
        }
    }
    public function actionDeletechallenge($id)
    {
        $challenge=Challenges::model()->findByPk($id);
        if($challenge->TM_CL_Chalanger_Id==Yii::app()->user->id):

                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CL_QS_Chalange_Id='.$id);
                $model = Chalangequestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CEUQ_Challenge='.$id);
                $model = Chalangeuserquestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CE_CP_Challenge_Id='.$id);
                $model = ChallengeChapters::model()->deleteAll($criteria);
                $challenge->TM_CL_Status='1';
                $challenge->save(false);
                echo "yes";
        else:
                echo "no";
        endif;
    }
    public function actionCancelchallenge($id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status='4';
        if($model->save(false)):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id);
            $model = Chalangeuserquestions::model()->deleteAll($criteria);
            echo "yes";
        else:
            echo "no";
        endif;
    }
    public function actionAcceptchallenge($id)
    {

        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status='1';
        $return='';
        if($model->save(false)):
            $return.='<td data-title="Date">'.($model->challenge->TM_CL_Chalanger_Id==Yii::app()->user->id?"Me":$this->GetChallenger($model->challenge->TM_CL_Chalanger_Id)).'</td>';
            $return.='<td data-title="Publisher">'.date("d-m-Y", strtotime($model->challenge->TM_CL_Created_On)).'</td>';
            $return.='<td data-title="Division">'.$this->GetChallengeInvitees($model->challenge->TM_CL_Id,Yii::app()->user->id).'</td>';
            $return.='<td data-title="Link">';
            $return.="<a href='".Yii::app()->createUrl('student/startchallenge', array('id' => $id))."' class='btn btn-warning'>".(Student::model()->GetChallengeStatus($id,Yii::app()->user->id)>0?'Continue':'Start')."</a>";
            $return.='</td>';
            $return.='<td data-title="Completed">';
            $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                'student/cancelchallenge/'.$id,
                array (
                    'type'=>'POST',
                    'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$id.'").remove();} }'
                ),
                array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$id)
            );
            $return.='</td>';
        echo $return;
        else:
            echo "no";
        endif;
    }
    public function actionChallenge()
    {
        $plan=$this->GetStudentDet();
        $mainchapters=Chapter::model()->findAll(array('condition'=>"TM_TP_Syllabus_Id='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'"));
        if(isset($_POST['Challenge'])):
           if(count($_POST['chapters'])>0):
                $chapters='';
                $topics='';
                $challenge= new Challenges();
                $challenge->TM_CL_Chalanger_Id=Yii::app()->user->id;
                $challenge->TM_CL_Plan=Yii::app()->session['plan'];
                $challenge->TM_CL_QuestionCount=$_POST['Challenge']['questions'];
                $challenge->save(false);
                $recepchalange= new Chalangerecepient();
                $recepchalange->TM_CE_RE_Chalange_Id=$challenge->TM_CL_Id;
                $recepchalange->TM_CE_RE_Recepient_Id=Yii::app()->user->id;
                $recepchalange->TM_CE_RE_Status='1';
                $recepchalange->save(false);
               if(count($_POST['invitebuddies'])>0):
                   for($i=0;$i<count($_POST['invitebuddies']);$i++):
                       $recepchalange= new Chalangerecepient();
                       $recepchalange->TM_CE_RE_Chalange_Id=$challenge->TM_CL_Id;
                       $recepchalange->TM_CE_RE_Recepient_Id=$_POST['invitebuddies'][$i];
                       if($recepchalange->save(false)):
                           $this->SendMailChallengeInvite($_POST['invitebuddies'][$i]);
                           endif;
                       $notification=new Notifications();
                       $notification->TM_NT_User= $_POST['invitebuddies'][$i];
                       $notification->TM_NT_Type='Challenge';
                       $notification->TM_NT_Target_Id=Yii::app()->user->Id;
                       $notification->TM_NT_Status='0';
                       $notification->save(false);
                   endfor;
               endif;
                for($i=0;$i<count($_POST['chapters']);$i++):
                    $chapters=$chapters.($i==0?$_POST['chapters'][$i]:",".$_POST['chapters'][$i]);
                    $chapterid=$_POST['chapters'][$i];
                    $challengechapter= new ChallengeChapters();
                    $challengechapter->TM_CE_CP_Challenge_Id=$challenge->TM_CL_Id;
                    $challengechapter->TM_CE_CP_Chapter_Id=$chapterid;
                    $challengechapter->save(false);
                    for($j=0;$j<count($_POST['topic'.$chapterid]);$j++):
                        $topics=$topics.($i==0 & $j==0?$_POST['topic'.$chapterid][$j]:",".$_POST['topic'.$chapterid][$j]);
                    endfor;
                endfor;
               $Syllabus= $plan->TM_SPN_SyllabusId;
               $Standard= $plan->TM_SPN_StandardId;
               //$Topic=$topics;
               $Test=$challenge->TM_CL_Id;
               $Student=Yii::app()->user->id;
               $limit=$challenge->TM_CL_QuestionCount;
               //set questions for basic
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,1,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $basiccount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                //set questions for intermediate
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,2,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $intercount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
               //set questions for advanced
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,3,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $advancecount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
            endif;
            $this->redirect(array('home'));
        endif;
        $this->render('challenge',array('plan'=>$plan,'chapters'=>$mainchapters));
    }
    public function actionChallengeTest($id)
    {
        $challengecount=Challenges::model()->findByPk($id)->TM_CL_QuestionCount;
        if(isset($_POST['action']['GoNext'])):
                    //change challenge status to started
                    $criteria = new CDbCriteria;
                    $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
                    $model = Chalangerecepient::model()->find($criteria);
                    if($model->TM_CE_RE_Status=='1'):
                        $model->TM_CE_RE_Status='2';
                        $model->save(false);
                    endif;
                    $chuserquest=Chalangeuserquestions::model()->findByPk($_POST['ChallengeQuestionId']);
                    $masterquestion=Questions::model()->findByPk($_POST['QuestionId']);
                    $difficulty=$chuserquest->TM_CEUQ_Difficulty;
                        if($difficulty=='1'):
                            $marks='5';
                        elseif($difficulty=='2'):
                            $marks='8';
                        elseif($difficulty=='3'):
                            $marks='12';
                        endif;
                        if($_POST['QuestionType']=='1'):
                            $answer=implode(',',$_POST['answer']);
                            $chuserquest->TM_CEUQ_Answer_Id=$answer;
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                if($ans->TM_AR_Correct=='1'):
                                    $correctcount++;
                                    if (array_search($ans->TM_AR_Id, $_POST['answer'])!==false) {
                                        $usercorrect++;
                                    }
                                endif;
                            endforeach;
                            if($correctcount==$usercorrect):
                                switch ($difficulty) {
                                    case "1";
                                        $chuserquest->TM_CEUQ_Marks='5';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        $chuserquest->TM_CEUQ_Marks='8';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                        break;
                                    case "3";
                                            $chuserquest->TM_CEUQ_Marks='12';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                        break;
                                }
                            else:
                                switch ($difficulty) {
                                    case "1";
                                        if($this->NextDiffCount($id,'1')>0):
                                            $nextdifficulty='1';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                }
                            endif;
                        elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                            $answer=$_POST['answer'];
                            $answerarray=array($_POST['answer']);
                            $chuserquest->TM_CEUQ_Answer_Id=$answer;
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                if($ans->TM_AR_Correct=='1'):
                                    $correctcount++;
                                    if (array_search($ans->TM_AR_Id, $answerarray)!==false) {
                                        $usercorrect++;
                                    }
                                endif;
                            endforeach;
                            if($correctcount==$usercorrect):
                                switch ($difficulty) {
                                    case "1";
                                        $chuserquest->TM_CEUQ_Marks='5';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        $chuserquest->TM_CEUQ_Marks='8';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                }
                            else:
                                switch ($difficulty) {
                                    case "1";
                                        if($this->NextDiffCount($id,'1')>0):
                                            $nextdifficulty='1';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                }
                            endif;
                        elseif($_POST['QuestionType']=='4'):
                            $chuserquest->TM_CEUQ_Answer_Id=$_POST['answer'];
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                $useranswer=  trim(strtolower($_POST['answer']));
                                for($i=0;$i<count($answeroption);$i++)
                                {
                                   $option= trim($answeroption[$i]);
                                    if(strcmp($useranswer,$option)==0):
                                        $usercorrect++;
                                    endif;
                                }
                            endforeach;
                            if($usercorrect!=0):
                                switch ($difficulty) {
                                    case "1";
                                        $chuserquest->TM_CEUQ_Marks='5';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        $chuserquest->TM_CEUQ_Marks='8';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                }
                            else:
                                switch ($difficulty) {
                                    case "1";
                                        if($this->NextDiffCount($id,'1')>0):
                                            $nextdifficulty='1';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                }
                            endif;
                        else:
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$masterquestion->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            $totalmarks=0;
                            $marks=0;
                            foreach($questions AS $child):
                                $selectedquestion = Chalangeuserquestions::model()->find(
                                    array('condition'=>'TM_CEUQ_Question_Id=:parent AND TM_CEUQ_Challenge=:challenge AND TM_CEUQ_User_Id=:student',
                                    'params'=>array(':parent'=>$child->TM_QN_Id,':challenge'=>$id,':student'=>Yii::app()->user->id))
                                );

                                if($child->TM_QN_Type_Id=='1'):
                                    $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                                    $selectedquestion->TM_CEUQ_Answer_Id=$answer;
                                    foreach ($child->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if (array_search($ans->TM_AR_Id, $answer) !== false) {
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                                    $answer=$_POST['answer'.$child->TM_QN_Id];
                                    $selectedquestion->TM_CEUQ_Answer_Id=$answer;
                                    foreach ($child->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if ($ans->TM_AR_Id==$answer){
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;

                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;

                                elseif($child->TM_QN_Type_Id=='4'):
                                    $selectedquestion->TM_CEUQ_Answer_Id=$_POST['answer'.$child->TM_QN_Id];
                                    foreach ($child->answers AS $ans):

                                        $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                        $useranswer=  trim(strtolower($selectedquestion->TM_CEUQ_Answer_Id));
                                        for($i=0;$i<count($answeroption);$i++)
                                        {
                                            $option= trim($answeroption[$i]);
                                            if(strcmp($useranswer,$option)==0):
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            endif;
                                        }
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;

                                    endforeach;
                                endif;
                                $selectedquestion->save(false);
                            endforeach;
                            if($totalmarks==$marks):
                                    switch ($difficulty) {
                                        case "1";
                                            $chuserquest->TM_CEUQ_Marks='5';
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                if($this->NextDiffCount($id,'3')>0):
                                                    $nextdifficulty='3';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                        case "2";
                                            $chuserquest->TM_CEUQ_Marks='8';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                        case "3";
                                            $chuserquest->TM_CEUQ_Marks='12';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                    }
                            else:
                                    switch ($difficulty) {
                                        case "1";
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='3';
                                                endif;
                                            endif;
                                            break;
                                        case "2";
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                if($this->NextDiffCount($id,'3')>0):
                                                    $nextdifficulty='3';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                        case "3";
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                if($this->NextDiffCount($id,'1')>0):
                                                    $nextdifficulty='1';
                                                else:
                                                    $nextdifficulty='3';
                                                endif;
                                            endif;
                                            break;
                                    }
                            endif;
                        endif;
                $chuserquest->TM_CEUQ_Status='1';
                $chuserquest->save(false);
                if($challengecount=='10' & $chuserquest->TM_CEUQ_Iteration<='5'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;

                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount=='15' & $chuserquest->TM_CEUQ_Iteration<='10'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount=='20' & $chuserquest->TM_CEUQ_Iteration<='15'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);

                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount=='25' & $chuserquest->TM_CEUQ_Iteration<='20'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount==$chuserquest->TM_CEUQ_Iteration):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CE_RE_Chalange_Id=:test AND TM_CE_RE_Recepient_Id=:user';
                    $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
                    $nextquestions=Chalangerecepient::model()->find($criteria);
                    $nextquestions->TM_CE_RE_Status='3';
                    $nextquestions->save(false);
                    $this->redirect(array('challengeresult','id'=>$id));
                endif;

        endif;
        if(Student::model()->GetChallengeStatus($id,Yii::app()->user->id)==0):
            // insert for basic 3 questions
            $difficulty=1;
            $easylimit=3;
            $iteration=0;
            $easycount=$this->GetChallengeCount($id,$difficulty,$easylimit);
            if($easycount < $easylimit):
                $interiteration=$this->ChallengeAddQuestions($id,$difficulty,$easycount,Yii::app()->user->id,$iteration);
                $difficulty=2;
                $newlimit=2+$easycount;
            else:

                $interiteration=$this->ChallengeAddQuestions($id,$difficulty,$easylimit,Yii::app()->user->id,$iteration);
                $difficulty=2;
                $newlimit=2;
            endif;

            // insert $interlimit intermediate 2 questions
            $intercount=$this->GetChallengeCount($id,$difficulty,$newlimit);

            if($intercount<$newlimit):
                $advanceiteration=$this->ChallengeAddQuestions($id,$difficulty,$intercount,Yii::app()->user->id,$interiteration);
                $difficulty=3;
                $newlimit=2-$intercount;
            else:
                $advanceiteration=$this->ChallengeAddQuestions($id,$difficulty,$newlimit,Yii::app()->user->id,$interiteration);
                $difficulty=3;
                $newlimit=0;
            endif;
            // if not enough intermediate insert from advance
            if($newlimit!=0):
                $advancecount=$this->GetChallengeCount($id,$difficulty,$newlimit);
                if($advancecount < $newlimit):
                    $iteration=$this->ChallengeAddQuestions($id,$difficulty,$newlimit,Yii::app()->user->id,$advanceiteration);
                else:
                    $iteration=$this->ChallengeAddQuestions($id,$difficulty,$newlimit,Yii::app()->user->id,$advanceiteration);
                endif;
            endif;
        endif;
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CEUQ_Challenge=:test AND TM_CEUQ_Status=0 AND TM_CEUQ_User_Id=:user AND TM_CEUQ_Parent_Id=0';
        $criteria->limit='1';
        $criteria->order='TM_CEUQ_Iteration ASC';
        $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
        $question=Chalangeuserquestions::model()->find($criteria);
        $this->renderPartial('challengequestion',array('testid'=>$id,'challengequestion'=>$question,'question'=>$question->question,'challengecount'=>$challengecount));

    }
    public function actionChallengeresult($id)
    {
        $challengecount=Challenges::model()->findByPk($id)->TM_CL_QuestionCount;
        if($challengecount=='10'):
            $examtotalmarks='79';
        elseif($challengecount=='15'):
            $examtotalmarks='139';
        elseif($challengecount=='20'):
            $examtotalmarks='199';
        elseif($challengecount=='25'):
            $examtotalmarks='199';
        endif;
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_CEUQ_Marks) AS total');
        $criteria->condition='TM_CEUQ_Challenge=:test AND TM_CEUQ_User_Id=:user';
        $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
        $studenttotal=Chalangeuserquestions::model()->find($criteria);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $recepient = Chalangerecepient::model()->find($criteria);
        $recepient->TM_CE_RE_ObtainedMarks=$studenttotal->total;
        $points=$studenttotal->total;
        $recepient->save(false);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id);
        $criteria->order='TM_CE_RE_ObtainedMarks DESC';
        $participants = Chalangerecepient::model()->findAll($criteria);
        $recepients=count($participants);
        //echo $recepients;exit;
        $userids=array();
        foreach($participants AS $party):
            $userids[]=$party->TM_CE_RE_Recepient_Id;
        endforeach;


        $results = Chalangeuserquestions::model()->findAll(
            array(
                'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Parent_Id=0 ',
                "order" => 'TM_CEUQ_Id ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $earnedmarks = 0;
        $result = '';
        $qstnnum=1;
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_CEUQ_Question_Id);
            $displaymark = 0;
            if($exam->TM_CEUQ_Difficulty==1):
                $displaymark=5;
            elseif($exam->TM_CEUQ_Difficulty==2):
                $displaymark=8;
            elseif($exam->TM_CEUQ_Difficulty==3):
                $displaymark=12;
            endif;
            if ($question->TM_QN_Type_Id != '5'):
                $marks = 0;
                if ($question->TM_QN_Type_Id != '4' ):
                    $challengeans=Chalangeuserquestions::model()->find(array('condition'=>"TM_CEUQ_Challenge='$id' AND TM_CEUQ_Question_Id='$exam->TM_CEUQ_Question_Id'"));
                    $studentanswer = explode(',', $challengeans->TM_CEUQ_Answer_Id);
                    if (count($studentanswer) > 0):
                        $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh>';
                        for ($i = 0; $i < count($studentanswer); $i++)
                        {
                            $answer = Answers::model()->findByPk($studentanswer[$i]);
                            $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=200&height=200" alt=""></div>' : '') . '</li>';
                        }
                        $correctanswer .= '</ul>';
                    else:
                        $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh><li class="list-group-item1" style="display:flex ;">Question Skipped</li></ul>';
                    endif;
                else:
                    if ($exam->TM_CEUQ_Answer_Id != ''):
                        $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh><li class="list-group-item1" style="display:flex ;"><div class="col-md-12">' . $exam->TM_CEUQ_Answer_Id . '</div></li></ul>';
                    else:
                        $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh><li class="list-group-item1" style="display:flex ;">Question Skipped</li></ul>';
                    endif;
                endif;
                $result .= '<tr><td style="background-color: white;"><div class="col-md-3">';
                if($exam->TM_CEUQ_Marks=='0'):
                    $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question.' . $qstnnum++.'</span>';
                /*elseif($marks<$displaymark & $marks!='0'):
                    $result .= '<span class="clrr"><img src="'.Yii::app()->request->baseUrl.'/images/rw.png"></i>  Question.' .$qstnnum++.'</span>';*/
                else:
                    $result .= '<span ><i class="fa fa fa-check"></i>  Question.' .$qstnnum++.'</span>';
                endif;

                $result .= ' <span class="pull-right">Mark: '.$exam->TM_CEUQ_Marks.'/' . $displaymark . '</span></div>
                            <div class="col-md-4 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="'.$question->TM_QN_Id.'" style="font-size:12px;margin-left: 42%;color: rgb(50, 169, 255);">Report Problem</a>
                            <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:' . $question->TM_QN_QuestionReff . '</span></div>';

                //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>'.($question->TM_QN_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>':'').'</li></div>';
                $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                $result .= '<div class="col-md-12 Itemtest clickable-row ">
                            <div class="sendmailtest suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                            </div>
                            <div class="col-md-10 alert alert-success alertpadding alertpullright" id="mailSendCompletetest" style="display: none;">Your Message Has Been Sent To Admin</div>
                            </div>
                             </td></tr>';

            //glyphicon glyphicon-star sty
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));

                $count = 1;
                $studentmarks=0;
                $totalmarks=0;
                $childresult='';
                foreach ($questions AS $childquestion):
                    $childresults = Chalangeuserquestions::model()->find(
                        array(
                            'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Question_Id=:questionid',
                            'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                        )
                    );
                    if ($childquestion->TM_QN_Type_Id != '4'):
                        if ($childresults->TM_CEUQ_Answer_Id != ''):
                            $studentanswer = explode(',', $childresults->TM_CEUQ_Answer_Id);
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                for ($i = 0; $i < count($studentanswer); $i++)
                                {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);
                                    $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=200&height=200" alt=""></div>' : '') . '</li></ul>';

                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($childresults->TM_CEUQ_Answer_Id != ''):
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_CEUQ_Answer_Id . '</li></ul>';
                        else:
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                        endif;

                    endif;

                    $childresult .= '<div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '')) . '</li></div>';
                    $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                    //glyphicon glyphicon-star sty
                    $count++;
                endforeach;
                $result .= '<tr><td style="background-color: white;"><div class="col-md-3">';
                if($exam->TM_CEUQ_Marks=='0'):
                    $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question.' . $qstnnum++.'</span>';
                /*elseif($marks<$displaymark & $marks!='0'):
                    $result .= '<span class="clrr"><img src="'.Yii::app()->request->baseUrl.'/images/rw.png"></i>  Question.' .$qstnnum++.'</span>';*/
                else:
                    $result .= '<span ><i class="fa fa fa-check"></i>  Question.' .$qstnnum++.'</span>';
                endif;
                $result .= ' <span class="pull-right">Mark: '.$exam->TM_CEUQ_Marks.'/' . $displaymark . '</span></div>
                            <div class="col-md-4 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="'.$question->TM_QN_Id.'" style="font-size:12px;margin-left: 42%;color: rgb(50, 169, 255);">Report Problem</a>
                            <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:' . $question->TM_QN_QuestionReff . '</span></div>';

                //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>'.($question->TM_QN_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>':'').'</li></div>';
                $result .=$childresult;
                $result .= '<div class="col-md-12 Itemtest clickable-row ">
                            <div class="sendmailtest suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                            </div>
                            <div class="col-md-10 alert alert-success alertpadding alertpullright" id="mailSendCompletetest" style="display: none;">Your Message Has Been Sent To Admin</div>
                            </div>
                             </td></tr>';
            endif;
        endforeach;
        $pointlevels=$this->AddPoints($studenttotal->total,$id,'Challenge',Yii::app()->user->id);
        $this->render('challengeresult',array('totalmarks'=>$examtotalmarks,'studenttotal'=>$studenttotal->total,'points'=>$points,'participants'=>$participants,'recepients'=>$recepients,'userids'=>$userids,'testresult' => $result,'pointlevels'=>$pointlevels,'testid'=>$id));
    }
    public function actionChallengeGetSolution()
    {
        if(isset($_POST['challengeid'])):
            $results=Chalangeuserquestions::model()->findAll(
                array(
                    'condition'=>'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test',
                    'params'=>array(':student'=>Yii::app()->user->id,':test'=>$_POST['challengeid'])
                )
            );
            $resulthtml='';
            $count=1;
            foreach($results AS $result):
                if($result->TM_CEUQ_Difficulty==1):
                    $mark=5;
                elseif($result->TM_CEUQ_Difficulty==2):
                    $mark=8;
                elseif($result->TM_CEUQ_Difficulty==3):
                    $mark=12;
                endif;
                $question=Questions::model()->findByPk($result->TM_CEUQ_Question_Id);
                $resulthtml.='<tr><td style="background-color: white;"><div class="col-md-3">
                    <span style="font-weight: 700;"> Question '.$count++.'</span>
                    <span class="pull-right">Marks : '.$mark.'</span>
                    </div>
                    <div class="col-md-3 pull-right">
                    <a href="javascript:void(0)" class="showSolutionItem" data-reff="'.$result->TM_CEUQ_Question_Id.'" style="font-size:12px;color: rgb(50, 169, 255);">Report Problem</a>
                    <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:'.$question->TM_QN_QuestionReff.'</span>
                    </div>';
                $resulthtml.='<div class="col-md-12">
                <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Question . '</div>'.($question->TM_QN_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>':'').'</div>';

                $resulthtml.='<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div>
                <div class="col-md-12">

                <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Solutions . '</div>'.($question->TM_QN_Solution_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>':'').'</div>';

                //$resulthtml.='<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div><div class="col-md-12">'.$question->TM_QN_Solutions.'</div>';

                $resulthtml.= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestion'.$result->TM_CEUQ_Question_Id.'" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments'.$question->TM_QN_Id.'"name="comments"  id="comment" hidden/></div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden ></div>
                           </div><div class="col-md-10 alert alert-success alertpadding alertpullright" id="mailSendComplete" style="display: none;">Your Message Has Been Sent To Admin</div></div>

                        </td></tr>';
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function GetPaperCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type IN (:type) AND TM_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id,':type'=>'6,7');
        $selectedquestion=StudentQuestions::model()->find($criteria);
        return count($selectedquestion);
    }
    public function GetPaperCountMock($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type IN (:type) AND TM_STMKQT_Parent_Id=0';
        $criteria->params=array(':test'=>$id,':type'=>'6,7');
        $selectedquestion=Studentmockquestions::model()->find($criteria);
        return count($selectedquestion);
    }
    public function GetQuestionCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id);
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        return count($selectedquestion);
    }
    public function GetChallengeQuestionCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_Id=:test ';
        $criteria->params=array(':test'=>$id);
        $selectedquestion=Challenges::model()->findAll($criteria);
        //$questioncount=$selectedquestion->TM_CL_QuestionCount;
        return $selectedquestion->TM_CL_QuestionCount;
    }

    public function doLatercount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Flag=:flag';
        $criteria->params=array(':test'=>$id,':flag'=>'2');
        $selectedquestion=StudentQuestions::model()->find($criteria);
        return count($selectedquestion);
    }
    public function GetTestQuestions($id,$question)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Type NOT IN (6,7)';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STU_QN_Number ASC';
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Type IN (6,7)';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STU_QN_Number ASC';
        $papercount=StudentQuestions::model()->count($criteria);
        $pagination='';
        foreach($selectedquestion AS $key=>$testquestion):
            $count=$key+1;
            $class='';
            if($testquestion->TM_STU_QN_Question_Id==$question)
            {
                $questioncount=$count;
            }
            if($testquestion->TM_STU_QN_Type=='6' || $testquestion->TM_STU_QN_Type=='7')
            {
                $pagination.='<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">'.$count.'</a></li>';
            }
            else
            {
                $title='';
                $class='';
                if($testquestion->TM_STU_QN_Flag=='0')
                {
                    $class=($testquestion->TM_STU_QN_Question_Id==$question?'class="active"':'');
                    $title=($testquestion->TM_STU_QN_Question_Id==$question?'Current Question':'');
                }
                elseif($testquestion->TM_STU_QN_Flag=='1')
                {
                    $class=($testquestion->TM_STU_QN_Question_Id==$question?'class="active"':'class="answered"');
                    $title=($testquestion->TM_STU_QN_Question_Id==$question?'Current Question':'Already Answered');
                }
                elseif($testquestion->TM_STU_QN_Flag=='2')
                {
                    $class=($testquestion->TM_STU_QN_Question_Id==$question?'class="active"':'class="skiped"');
                    $title=($testquestion->TM_STU_QN_Question_Id==$question?'Current Question':'Skipped Question');
                }
                $pagination.='<li '.$class.' ><a data-toggle="tooltip" data-placement="bottom" title="'.$title.'" href="'.($testquestion->TM_STU_QN_Question_Id==$question?'javascript:void(0)':Yii::app()->createUrl('student/RevisionTest',array('id'=>$id,'questionnum'=>base64_encode('question_'.$testquestion->TM_STU_QN_Number)))).'">'.$count.'</a></li>';
            }
        endforeach;
        return array('pagination'=>$pagination,'questioncount'=>$questioncount,'totalcount'=>$count,'papercount'=>$papercount);
    }
    public function GetChildAnswer($type,$test,$question)
    {

        $questionid=StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id'=>$test,'TM_STU_QN_Student_Id'=>Yii::app()->user->id,'TM_STU_QN_Question_Id'=>$question));
        if(count($questionid)>0):
            if($type=='1'):
                if($questionid->TM_STU_QN_Answer_Id!=''):
                    return explode(',',$questionid->TM_STU_QN_Answer_Id);
                else:
                    return array();
                endif;
            else:
                return $questionid->TM_STU_QN_Answer;
            endif;
        else:
            return array();
        endif;
    }
    public function GetChildAnswerMock($type,$test,$question)
    {

        $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$test,'TM_STMKQT_Student_Id'=>Yii::app()->user->id,'TM_STMKQT_Question_Id'=>$question));
        if(count($questionid)>0):
            if($type=='1'):
                if($questionid->TM_STU_QN_Answer_Id!=''):
                    return explode(',',$questionid->TM_STU_QN_Answer_Id);
                else:
                    return array();
                endif;
            else:
                return $questionid->TM_STU_QN_Answer;
            endif;
        else:
            return array();
        endif;
    }
    public function GetQuickLimits($total)
    {
        $mode=$total%3;
        if($mode==0):
            $count=$total/3;
            return array('basic'=>$count,'intermediate'=>$count,'advanced'=>$count);
        elseif($mode==1):
            $count=round($total/3);
            return array('basic'=>$count,'intermediate'=>$count+1,'advanced'=>$count);
        elseif($mode==2):
            $count=$total/3;
            return array('basic'=>ceil($count),'intermediate'=>ceil($count),'advanced'=>floor($count));
        endif;

    }
    public function ResultMessage($percentage)
    {
        $hundred=array('Congratulations. All hail the Math Master! ','Wow ! Time to Celebrate..','High - five !! Now, thats what I call a score !');
        $ninety=array('Excellent score ! Well done.','Excellent.. Next time its 100% !','Excellent Score .. Keep it up !');
        $seventyfive=array('Well done, but the 90\'s suit you better !','Not a bad score.. Well done, you !','Keep it up ! Keep it better !');
        $sixty=array('Decent effort, But you can do better !',' Well done you ! But you can do better !','A decent effort. Aim for higher !');
        $fifty=array('Not bad, but brush up a little more..','Do revise and try again !','Practice Makes Perfect. Try again !');
        $belowforty=array('Sorry.. do brush up and try again ! ','Hmm.. do we need some more practice ?','Practice Makes Perfect. Try again !');
        if($percentage=='100'):
            $rand=array_rand($hundred);
            return $hundred[$rand];
        elseif($percentage<=99 & $percentage>=90):
            $rand=array_rand($ninety);
            return $ninety[$rand];
        elseif($percentage<=89 & $percentage>=75):
            $rand=array_rand($seventyfive);
            return $seventyfive[$rand];
        elseif($percentage<=74 & $percentage>=60):
            $rand=array_rand($sixty);
            return $sixty[$rand];
        elseif($percentage<=59 & $percentage>=40):
            $rand=array_rand($fifty);
            return $fifty[$rand];
        elseif($percentage<40):
            $rand=array_rand($belowforty);
            return $belowforty[$rand];
        endif;
        //elseif():
    }
    public function GetBuddies($user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_BD_Connection_Id='.Yii::app()->user->id.' AND TM_BD_Status=0';
        $users=Buddies::model()->findAll($criteria);
        $usersjson='';
        foreach($users AS $key=>$student):
            $studentplan=StudentPlan::model()->count(array('condition'=>'TM_SPN_PlanId=:plan AND TM_SPN_StudentId=:student','params'=>array(':plan'=>Yii::app()->session['plan'],':student'=>$student->TM_BD_Student_Id)));
            if($studentplan>0):
                $user=User::model()->findByPk($student->TM_BD_Student_Id);
                $usersjson.=($usersjson==''?'{"id":'.$user->id.', "name":"'.$user->username.'"}':',{"id":'.$user->id.', "name":"'.$user->username.'"}');
            endif;
        endforeach;
        return $usersjson;

    }
    public function GetChallengeInvitees($challenge,$user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Recepient_Id!='.$user;
        $users=Chalangerecepient::model()->findAll($criteria);
        $usersjson='Me';
        if(count($users)>0):
            $othercount=count($users);
            $others=" + <a href='javascript:void(0);' class='hover' data-id='$challenge' data-tooltip='tooltip$challenge' style='color: #FFA900;'>".$othercount." Others"."
            <div class='tooltiptable' id='tooltip$challenge' style='display: none; width: 400px;'>
            <a href='javascript:void(0);'class='cls' id='close'>x</a><div class='row'><div class='col-md-12'><ul class='list-group'>".$this->GetChallengeBuddies($challenge)."</ul></div></div>
            </div></a>";
        else:
            $others='';
        endif;
        /*foreach($users AS $key=>$user):
            if($key<3):
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username;
                $usersjson.=','.$challengeuser;
            endif;
        endforeach;*/
        return $usersjson.$others;

    }
    public function GetChallengeInviteesForResults($challenge,$user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Recepient_Id!='.$user;
        $users=Chalangerecepient::model()->findAll($criteria);
        $usersjson='';
        if(count($users)>0):
            $othercount=count($users);
            $others="  <a href='javascript:void(0);' class='hover' data-id='$challenge' data-tooltip='tooltip$challenge' style='color: #FFA900;'>View Stats
            <div class='tooltiptable' id='tooltip$challenge' style='display: none; width: 400px; text-align: left'>
            <a href='javascript:void(0);'class='cls' id='close'>x</a><div class='row' ><div class='col-md-12'><ul class='list-group'>".$this->GetChallengeBuddies($challenge)."</ul></div></div>
            </div></a>";
        else:
            $others='';
        endif;
        /*foreach($users AS $key=>$user):
            if($key<3):
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username;
                $usersjson.=','.$challengeuser;
            endif;
        endforeach;*/
        return $usersjson.$others;

    }
    public function GetChallengeBuddies($challenge)
    {
        $userid=Yii::app()->user->id;
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge;
        $criteria->order='TM_CE_RE_ObtainedMarks DESC';
        $users=Chalangerecepient::model()->findAll($criteria);
        $buddies='';
        $userids=array();
        foreach($users AS $party):
            $userids[]=$party->TM_CE_RE_Recepient_Id;
        endforeach;
        foreach($users AS $key=>$user):
            if($user->TM_CE_RE_Status=='0'):
                $status= "Not Started</br><a class='reminder  rem".$user->TM_CE_RE_Recepient_Id."'data-id='$user->TM_CE_RE_Recepient_Id' style='color: #FFA900;'>Send Reminder</a></br><span class='remsend".$user->TM_CE_RE_Recepient_Id."' hidden style='background-color:rgb(255,255,0)'>Reminder sent</span>";
            elseif($user->TM_CE_RE_Status=='1'):
                $status= "Not Started</br><a class='reminder  rem".$user->TM_CE_RE_Recepient_Id."' data-id='$user->TM_CE_RE_Recepient_Id' style='color: #FFA900;'>Send Reminder</a></br><span class='remsend".$user->TM_CE_RE_Recepient_Id."' hidden style='background-color:rgb(255,255,0)'>Reminder sent</span>";
            elseif($user->TM_CE_RE_Status=='2'):
                $status= "Not Completed";
            elseif($user->TM_CE_RE_Status=='3'):
                $status= $this->GetChallengePosition($userids, $user->TM_CE_RE_Recepient_Id);
            elseif($user->TM_CE_RE_Status=='4'):
                $status= "NA";
            endif;
            if($user->TM_CE_RE_Recepient_Id==$userid):
                $challengeuser='Me';
            else:
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username."(".Student::model()->find(array('condition' => "TM_STU_User_Id='" . $user->TM_CE_RE_Recepient_Id . "'"))->TM_STU_First_Name." ".Student::model()->find(array('condition' => "TM_STU_User_Id='" . $user->TM_CE_RE_Recepient_Id . "'"))->TM_STU_Last_Name. ")";
            endif;

            $buddies.="<li class='list-group-item padding5'>
            <img src='http://lifestyle.iloveindia.com/lounge/images/oblong-face-hairstyles.jpg' class='img-circle tab_imgsmall'>
            <span>$challengeuser</span>
            <span style='margin-left: 35px;position: absolute;right: 0; margin: 2px;'>$status</span>


            </li>";
        endforeach;
        return $buddies;

    }
    public function GetChallenger($id)
    {
        return Student::model()->find(array('condition' => "TM_STU_User_Id='" .$id . "'"))->TM_STU_First_Name." ".Student::model()->find(array('condition' => "TM_STU_User_Id='" . $id . "'"))->TM_STU_Last_Name ;
    }
    public function GetChallangeInviteStatus($challenge)
    {
        $user=Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Recepient_Id!='.$user.' AND TM_CE_RE_Status IN (1,2)');
        $model = Chalangerecepient::model()->count($criteria);
        return $model;
    }
    public function getChallengeLinks($id,$user)
    {
        $challenge=Challenges::model()->findByPk($id);
        $return='';
        if($challenge->TM_CL_Chalanger_Id==$user):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge->TM_CL_Id.' AND TM_CE_RE_Recepient_Id='.$user);
            $recepient = Chalangerecepient::model()->find($criteria);
            $return.='<td data-title="Link"><span class="pull-right">';
                if($recepient->TM_CE_RE_Status=='1'):
                    $return.="<a href='".Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Start</a>";
                elseif($recepient->TM_CE_RE_Status=='2'):
                    $return.="<a href='".Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Continue</a>";
                endif;
            $return.='</span></td>';
            $return.='<td data-title="Completed"><span class="pull-right">';
                if($this->GetChallangeInviteStatus($id)>0):
                    $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                        Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?')
                    );
                else:
                    $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                        Yii::app()->request->baseUrl.'/student/deletechallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to delete this Challenge?')
                    );
                endif;
            $return.='</span></td>';
        else:
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge->TM_CL_Id.' AND TM_CE_RE_Recepient_Id='.$user);
            $recepient = Chalangerecepient::model()->find($criteria);

            if($recepient->TM_CE_RE_Status=='0'):
                $return.='<td data-title="Link">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/right.png','',array('class'=>'hvr-buzz-out','title'=>'Accept Challenge')),
                    Yii::app()->request->baseUrl.'/student/acceptchallenge/'.$challenge->TM_CL_Id,
                    array (
                        'type'=>'POST',
                        'success'=>'function(html){ if(html!="no"){$("#challengerow'.$challenge->TM_CL_Id.'").html(html);} }'
                    )
                );
                $return.='</td>';
                $return.='<td></td>';
                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out','title'=>'Cancel Challenge')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                    array (
                        'type'=>'POST',
                        'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                    ),
                    array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='1'):
                $return.='<td></td>';
                $return.='<td data-title="Link">';
                $return.="<a href='".Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Start</a>";
                $return.='</td>';
                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                    );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='2'):
                $return.='<td></td>';
                $return.='<td data-title="Link">';
                $return.="<a href='".Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Continue</a>";
                $return.='</td>';

                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                    );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='3'):
            elseif($recepient->TM_CE_RE_Status=='4'):
            endif;
        endif;
        return $return;
    }
    public function NextDiffCount($id,$nextdifficulty)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge="'.$id.'" AND TM_CEUQ_User_Id="'.Yii::app()->user->id.'")';
        $criteria->limit='1';
        $criteria->order='RAND()';
        $criteria->params=array(':test'=>$id);
        return Chalangequestions::model()->count($criteria);

    }
    public function GetChallengePosition($users,$id)
    {
        $position=array_keys($users, $id);
        $position1=$position[0]+1;

        if($position1=='1'):
            return $position1.'<sup>st</sup><span class="fa fa-trophy size2 brd_r2"></span>';
        elseif($position1=='2'):
            return $position1.'<sup>nd</sup>';
        elseif($position1=='3'):
            return $position1.'<sup>rd</sup>';
        else:
            return $position1.'<sup>th</sup>';
        endif;
    }
    public function actionSendMail()

    {
        $questionId = $_POST["questionId"];
        $comments = $_POST["comments"];

        $question=Questions::model()->findByPk($questionId);
        $sendQuestion= $question->TM_QN_Question;
        $sendReff= $question->TM_QN_QuestionReff;
        $userId=Yii::app()->user->getId();
        $userDet=User::model()->findByPk($userId);
        $studentDet=Student::model()->findAll(array('condition' => "TM_STU_User_Id='$userId'"));

        $content='<p>A problem has been reported by '.$studentDet->TM_STU_First_Name.$studentDet->TM_STU_Last_Name.'('.$userDet->username.'),</p><p>'.$comments.'</p><p> Thank You</p>';
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;

        $mail->IsSMTP();
        $mail->Host = "smtpout.secureserver.net"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;
        $mail->Username = "services@tabbiemath.com"; // SMTP username
        $mail->Password = "Ta88ieMe"; // SMTP password
        $mail->SetFrom('services@tabbiemath.com', 'TabbieMe');
        $mail->Subject = 'Problem reported Ref:'.$sendReff;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($content);
        $mail->AddAddress('vigil@solminds.com', 'Rema');
        //$mail->AddAddress('karthik@solminds.com', 'Karthik');



        if(!$mail->Send()) {

            echo "Mailer Error: " . $mail->ErrorInfo;

        } else {


            return success;

        }
    }

    /*Mock Related functions*/
    public function actionMock()
    {
        $plan=$this->GetStudentDet();
        $criteria=new CDbCriteria;
        $criteria->condition='TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status';
        // AND TM_MK_Id NOT IN ( SELECT TM_STMK_Mock_Id FROM tm_student_mocks WHERE TM_STMK_Student_Id='.Yii::app()->user->id.')
        $criteria->params=array(':publisher'=>'3',':syllabus'=>$plan->TM_SPN_SyllabusId,':standard'=>$plan->TM_SPN_StandardId,':status'=>'0');
        $mocks=Mock::model()->findAll($criteria);
        $this->render('mocks',array('mocks'=>$mocks));
    }
    public function actionStartmock($id)
    {
        $test=Mock::model()->findByPk($id);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_MQ_Mock_Id='.$id.' AND TM_MQ_Type NOT IN (6,7)';
        $totalonline=MockQuestions::model()->count($criteria);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_MQ_Mock_Id='.$id.' AND TM_MQ_Type IN (6,7)';
        $totalpaper=MockQuestions::model()->count($criteria);
        $totalquestions=$totalonline+$totalpaper;
        if(isset($_POST['startTest'])):
            $criteria=new CDbCriteria;
            $criteria->condition='TM_MQ_Mock_Id=:mock';
            $criteria->params=array(':mock'=>$id);

            $mockquestions=MockQuestions::model()->findAll($criteria);
            $studentmock=new StudentMocks();
            $studentmock->TM_STMK_Student_Id=Yii::app()->user->id;
            $studentmock->TM_STMK_Mock_Id=$id;
            $studentmock->TM_STMK_Status='0';
            $studentmock->TM_STMK_Plan_Id=Yii::app()->session['plan'];
            $studentmock->save(false);
            foreach($mockquestions AS $key=>$mockquestion):
                $studentmockquestion=new Studentmockquestions();
                $studentmockquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                $studentmockquestion->TM_STMKQT_Mock_Id=$studentmock->TM_STMK_Id;
                $studentmockquestion->TM_STMKQT_Type=$mockquestion->TM_MQ_Type;
                $studentmockquestion->TM_STMKQT_Question_Id=$mockquestion->TM_MQ_Question_Id;
                $studentmockquestion->TM_STMKQT_Status=='0';
                $studentmockquestion->save(false);
            endforeach;
            if($totalquestions==$totalpaper):
                $this->redirect(array('Showmockpaper','id'=>$studentmock->TM_STMK_Id));
            else:
                $this->redirect(array('Mocktest','id'=>$studentmock->TM_STMK_Id));
            endif;

        endif;

        $this->render('startmock',array('id'=>$id,'test'=>$test,'totalquestions'=>$totalquestions,'totalonline'=>$totalonline,'totalpaper'=>$totalpaper));
    }
    public function actionMocktest($id)
    {
        if(isset($_POST['action']['GoNext']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STMKQT_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id'=>$child->TM_QN_Id,'TM_STMKQT_Mock_Id'=>$id,'TM_STMKQT_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id=$id;
                    $selectedquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STMKQT_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            endif;
            $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                array(
                    'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Id>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STMKQT_Id ASC, TM_STMKQT_Status ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionTableid'])
                )
            );
        }
        if(isset($_POST['action']['GetPrevios'])):

            $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                array(
                    'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0,1,2) AND TM_STMKQT_Type NOT IN (6,7) AND TM_STMKQT_Id<:tableid AND TM_STMKQT_Parent_Id=0',
                    'limit'=>1,
                    'order'=>'TM_STMKQT_Id DESC, TM_STMKQT_Status ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionTableid'])
                )
            );
        endif;
        if(isset($_POST['action']['DoLater'])):
            if($_POST['QuestionType']!='5'):
                if(isset($_POST['answer'])):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=Studentmockquestions::model()->find($criteria);
                    if($_POST['QuestionType']=='1'):
                        $answer=implode(',',$_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                        $answer=$_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($_POST['QuestionType']=='4'):
                        $selectedquestion->TM_STMKQT_Answer=$_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_Status='2';
                    $selectedquestion->save(false);
                else:
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                    $selectedquestion=Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_Status='2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    if(isset($_POST['answer'.$child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id'=>$child->TM_QN_Id,'TM_STMKQT_Mock_Id'=>$id,'TM_STMKQT_Student_Id'=>Yii::app()->user->id)
                        );
                        if(count($childquestion)=='1'):
                            $selectedquestion=$childquestion;
                        else:
                            $selectedquestion=new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id=$id;
                        $selectedquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id=$child->TM_QN_Id;
                        if($child->TM_QN_Type_Id=='1'):
                            $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer=$answer;
                        elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                            $answer=$_POST['answer'.$child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer=$answer;
                        elseif($child->TM_QN_Type_Id=='4'):
                            $selectedquestion->TM_STMKQT_Answer=$_POST['answer'.$child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id=$_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status='2';
                $selectedquestion->save(false);
            endif;
            $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                array(
                    'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Id>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STMKQT_Id ASC, TM_STMKQT_Status ASC',
                    'params'=>array(':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionTableid'])
                )
            );
        endif;

        if(isset($_POST['action']['Complete']))
        {

            if($_POST['QuestionType']!='5'):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                if($_POST['QuestionType']=='1'):
                    $answer=implode(',',$_POST['answer']);
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                    $answer=$_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer=$answer;
                elseif($_POST['QuestionType']=='4'):
                    $selectedquestion->TM_STMKQT_Answer=$_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            else:
                $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$_POST['QuestionId']."'","order"=>"TM_QN_Id ASC"));
                foreach($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id'=>$child->TM_QN_Id,'TM_STMKQT_Mock_Id'=>$id,'TM_STMKQT_Student_Id'=>Yii::app()->user->id)
                    );
                    if(count($childquestion)=='1'):
                        $selectedquestion=$childquestion;
                    else:
                        $selectedquestion=new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id=$id;
                    $selectedquestion->TM_STMKQT_Student_Id=Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id=$child->TM_QN_Id;
                    if($child->TM_QN_Type_Id=='1'):
                        $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                        $answer=$_POST['answer'.$child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer=$answer;
                    elseif($child->TM_QN_Type_Id=='4'):
                        $selectedquestion->TM_STMKQT_Answer=$_POST['answer'.$child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id=$_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params=array(':question'=>$_POST['QuestionId'],':student'=>Yii::app()->user->id,':test'=>$id);
                $selectedquestion=Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status='1';
                $selectedquestion->save(false);
            endif;
            if($this->GetPaperCountMock($id)!=0):
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMK_Mock_Id=:mock AND TM_STMK_Student_Id=:student';
                $criteria->params=array(':mock'=>$id,':student'=>Yii::app()->user->id);
                $test=StudentMocks::model()->find($id);
                $test->TM_STMK_Status='1';
                $test->save(false);
                $this->redirect(array('showmockpaper','id'=>$id));
            else:
                $criteria=new CDbCriteria;
                $criteria->condition='TM_STMK_Mock_Id=:mock AND TM_STMK_Student_Id=:student';
                $criteria->params=array(':mock'=>$id,':student'=>Yii::app()->user->id);
                $test=StudentMocks::model()->find($id);
                $test->TM_STMK_Status='1';
                $test->save(false);
                $this->redirect(array('mockcomplete','id'=>$id));
            endif;
            //$this->redirect(array('TestComplete','id'=>$id));
        }
        if(!isset($_POST['action'])):
            if(isset($_GET['questionnum'])):
                $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                    array(
                        'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0,1,2) AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Question_Id='.str_replace("question_","",base64_decode($_GET['questionnum'])),
                        'limit'=>1,
                        'order'=>'TM_STMKQT_Question_Id ASC, TM_STMKQT_Status ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            else:
                $questionid=Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id'=>$id),
                    array(
                        'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0) AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Parent_Id=0',
                        'limit'=>1,
                        'order'=>'TM_STMKQT_Question_Id ASC, TM_STMKQT_Status ASC',
                        'params'=>array(':student'=>Yii::app()->user->id)
                    )
                );
            endif;
        endif;
        if(count($questionid)):
            $question=Questions::model()->findByPk($questionid->TM_STMKQT_Question_Id);
            $this->renderPartial('mocktest',array('testid'=>$id,'question'=>$question,'questionid'=>$questionid));
        else:
            $this->redirect(array('mock'));
        endif;
    }
    public function actionMockComplete($id)
    {
        $results=Studentmockquestions::model()->findAll(
            array(
                'condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type NOT IN(6,7)',
                'params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)
            )
        );
        $totalmarks = 0;
        $earnedmarks = 0;
        $result = '';
        $count=0;
        foreach ($results AS $key=>$exam):
            $count=$key+1;
            $question = Questions::model()->findByPk($exam->TM_STMKQT_Question_Id);
            if ($question->TM_QN_Parent_Id == 0):
                if ($question->TM_QN_Type_Id != '5'):
                    $displaymark = 0;
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marks AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                    $marks = 0;
                    if ($question->TM_QN_Type_Id != '4' & $exam->TM_STMKQT_Flag != '2'):
                        if ($exam->TM_STMKQT_Answer != ''):

                            $studentanswer = explode(',', $exam->TM_STMKQT_Answer);
                            foreach ($question->answers AS $ans):
                                if ($ans->TM_AR_Correct == '1'):
                                    if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    }
                                endif;
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh>';
                                for ($i = 0; $i < count($studentanswer); $i++)
                                {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);

                                    $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($exam->TM_STMKQT_Answer != ''):

                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_STMKQT_Answer . '</div></li></ul>';
                            foreach ($question->answers AS $ans):
                                $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                $useranswer=  trim(strtolower($exam->TM_STMKQT_Answer));
                                for($i=0;$i<count($answeroption);$i++)
                                {
                                    $option= trim($answeroption[$i]);
                                    if(strcmp($useranswer,$option)==0):
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    endif;
                                }
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;

                    endif;
                    if ($question->TM_QN_Parent_Id == 0):
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_STMKQT_Redo_Flag = 1;
                            $result .='<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $count . '</span>';

                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_STMKQT_Redo_Flag = 1;
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $count . '</span>';
                        else:
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question '  . $count . '</span>';
                        endif;




                        $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STMKQT_Mock_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? 'fa fa-flag sty' : 'fa fa-flag stynone') . ' newtest"data-toggle="tooltip" data-placement="left" title=" Flag question."></span></a>      Marks:' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                             <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';

                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                        $result .= '<div class="col-md-12 Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2 mailadd" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                 <div class="col-md-10 alert alert-success mailSendCompletetest' . $question->TM_QN_Id . ' alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div>
                            </div></div>
                             </td></tr>';

                        //glyphicon glyphicon-star sty
                    endif;
                    $exam->TM_STMKQT_Marks = $marks;
                    $exam->save(false);
                    $earnedmarks = $earnedmarks + $marks;

                else:
                    $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                    $displaymark = 0;
                    foreach ($marksQuestions AS $key => $formarks):

                        $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($gettingmarks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                    endforeach;
                    $result .= '<tr><td><div class="col-md-4">';

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childresult="";

                    foreach ($questions AS $childquestion):
                        $childresults = Studentmockquestions::model()->find(
                            array(
                                'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Question_Id=:questionid',
                                'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                            )
                        );
                        $marks = 0;
                        if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_STMKQT_Flag != '2'):
                            if ($childresults->TM_STMKQT_Answer != ''):

                                $studentanswer = explode(',', $childresults->TM_STMKQT_Answer);
                                foreach ($childquestion->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++)
                                    {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($childresults->TM_STMKQT_Answer != ''):
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_STMKQT_Answer . '</li></ul>';
                                foreach ($childquestion->answers AS $ans):
                                    $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer=  trim(strtolower($childresults->TM_STMKQT_Answer));
                                    for($i=0;$i<count($answeroption);$i++)
                                    {
                                        $option= trim($answeroption[$i]);
                                        if(strcmp($useranswer,$option)==0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($childquestion->TM_QN_Parent_Id != 0):
                            $childresult .= '<tr><td>

                             <div class="col-md-12"><p>Part ' . $countpart . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                            $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $childresult .= '';
                            //glyphicon glyphicon-star sty
                        endif;

/*

                        $exam->TM_STMKQT_Marks = $marks;
                        $exam->save(false);*/
                        $earnedmarks = $earnedmarks + $marks;

                        $countpart++;
                    endforeach;
                    if ($marks == '0'):
                        $exam->TM_STMKQT_Redo_Flag = 1;
                        $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $count . '</span>';
                    elseif ($marks < $displaymark & $marks != '0'):
                        $exam->TM_STMKQT_Redo_Flag = 1;
                        $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $count. '</span>';
                    else:
                        $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $count. '</span>';
                    endif;
                    $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STMKQT_Mock_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? ' fa fa-flag sty' : ' fa fa-flag stynone') . ' newtest" data-toggle="tooltip" data-placement="left" title=" Flag question."></span></a>    Mark: ' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                    //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                    $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                    $result .= $childresult.'</td></tr>';
                    $result .='<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-10 alert alert-success mailSendCompletetest' . $question->TM_QN_Id . ' alertpadding alertpullright" id="" hidden >Your Message Has Been Sent To Admin
                                </div>

                            </div></div>
</td></tr>';
                endif;


            endif;

        endforeach;
        $percentage = ($earnedmarks / $totalmarks) * 100;
        $test = StudentMocks::model()->findByPk(array($id));
        $test->TM_STMK_Marks = $earnedmarks;
        $test->TM_STMK_TotalMarks = $totalmarks;
        $test->TM_STMK_Percentage = round($percentage, 1);
        $test->TM_STMK_Status = 1;
        $test->save(false);
        $pointlevels=$this->AddPoints($earnedmarks,$id,'Mock',Yii::app()->user->id);
        $this->render('showmockresult', array('test' => $test, 'testresult' => $result,'pointlevels'=>$pointlevels));
    }
    public function actionDeletemock($id)
    {

        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMK_Mock_Id=:mockid AND TM_STMK_Student_Id=:studentid';
        $criteria->params=array(':mockid'=>$id,':studentid'=>Yii::app()->user->id);
        $studentmock=StudentMocks::model()->find($criteria);
        $return='';
        $mock=Mock::model()->findByPk($id);
        $return.='<td>'.$mock->TM_MK_Name.'</td>';
        $return.='<td>'.$mock->TM_MK_Description.'</td>';
        $return.='<td>'.$mock->TM_MK_CreatedOn.'</td>';
        $return.='<td colspan="2"><a href="'.Yii::app()->createUrl('student/startmock', array('id' => $mock->TM_MK_Id)).'" class="btn btn-warning">Start Mock</a></td>';

        if($studentmock->delete())
        {
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMKQT_Student_Id='.Yii::app()->user->id.' AND TM_STMKQT_Mock_Id='.$id);
            $model = Studentmockquestions::model()->deleteAll($criteria);
            echo $return;
        }
        else
        {
            echo "no";
        }
    }
    public function GetMockQuestions($id,$question)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Type NOT IN (6,7)';
        $criteria->params=array(':test'=>$id);
        $criteria->order='TM_STMKQT_Id ASC';
        $selectedquestion=Studentmockquestions::model()->findAll($criteria);
        $pagination='';
        foreach($selectedquestion AS $key=>$testquestion):
            $count=$key+1;
            $class='';
            if($testquestion->TM_STMKQT_Question_Id==$question)
            {
                $questioncount=$count;
            }
            if($testquestion->TM_STMKQT_Type=='6' || $testquestion->TM_STMKQT_Type=='7')
            {
                $pagination.='<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">'.$count.'</a></li>';
            }
            else
            {
                $title='';
                $class='';
                if($testquestion->TM_STMKQT_Status=='0')
                {
                    $class=($testquestion->TM_STMKQT_Question_Id==$question?'class="active"':'');
                    $title=($testquestion->TM_STMKQT_Question_Id==$question?'Current Question':'');
                }
                elseif($testquestion->TM_STMKQT_Status=='1')
                {
                    $class=($testquestion->TM_STMKQT_Question_Id==$question?'class="active"':'class="answered"');
                    $title=($testquestion->TM_STMKQT_Question_Id==$question?'Current Question':'Already Answered');
                }
                elseif($testquestion->TM_STMKQT_Status=='2')
                {
                    $class=($testquestion->TM_STMKQT_Question_Id==$question?'class="active"':'class="skiped"');
                    $title=($testquestion->TM_STMKQT_Question_Id==$question?'Current Question':'Skipped Question');
                }
                $pagination.='<li '.$class.' ><a data-toggle="tooltip" data-placement="bottom" title="'.$title.'" href="'.($testquestion->TM_STMKQT_Question_Id==$question?'javascript:void(0)':Yii::app()->createUrl('student/mocktest',array('id'=>$id,'questionnum'=>base64_encode('question_'.$testquestion->TM_STMKQT_Question_Id)))).'">'.$count.'</a></li>';
            }
        endforeach;
        return array('pagination'=>$pagination,'questioncount'=>$questioncount,'totalcount'=>$count);
    }


    // view history

    public function actionHistory(){

        if(Yii::app()->session['challenge']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Recepient_Id='.Yii::app()->user->id.' AND TM_CE_RE_Status=3 AND TM_CL_Plan='.Yii::app()->session['plan']);
            $challenges = Chalangerecepient::model()->with('challenge')->findAll($criteria);
        endif;
        if(Yii::app()->session['quick'] || Yii::app()->session['difficulty']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_TT_Student_Id='.Yii::app()->user->id.' AND TM_STU_TT_Status IN (1,2) AND TM_STU_TT_Plan='.Yii::app()->session['plan']);
            $totaltests = StudentTest::model()->findAll($criteria);
        endif;
        if(Yii::app()->session['mock']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMK_Student_Id='.Yii::app()->user->id.' AND TM_STMK_Status IN (1,2) AND TM_MK_Status=0 AND  TM_STMK_Plan_Id='.Yii::app()->session['plan']);
            $totalmocks = StudentMocks::model()->with('mocks')->findAll($criteria);
        endif;
        $this->render('viewhistory',array('challenges'=>$challenges,'totaltests'=>$totaltests,'totalmocks'=>$totalmocks));
    }
    // view history ends
    public function GetTotal($id)
    {
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_AR_Marks) AS totalmarks');
        $criteria->condition='TM_AR_Question_Id=:question';
        $criteria->params=array(':question'=>$id);
        $totalmarks=Answers::model()->find($criteria);
        return $totalmarks->totalmarks;
    }
    public function actionGetpdfAnswer($id)
    {
        $results = StudentQuestions::model()->findAll(
            array(
                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test ',
                "order" => 'TM_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $totalquestions=count($results);
        $html = "<table cellpadding='5' border='0' width='100%'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>REVISION SOLUTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";





        foreach ($results AS $key => $result):
            $count = $key + 1;
            $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Type_Id == '1' || $question->TM_QN_Type_Id == '2' || $question->TM_QN_Type_Id == '3' || $question->TM_QN_Type_Id == '4'):
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='80%' align='right'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";


                    $html .= "<tr><td colspan='3' >";


                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td colspan='3'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';


                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ( $question->TM_QN_Type_Id == '6'):
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $question->TM_QN_Totalmarks;
                    $html .= "</td><td width='80%' align='right'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";


                    $html .= "<tr><td colspan='3' >";


                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td colspan='3'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';


                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '5' ):

                    $displaymark = 0;



                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childhtml='';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;

                        $childhtml .= "<tr><td colspan='3'><b>";
                        $childhtml .= "Part " . $count;
                        $childhtml .= "</b></td></tr><tr><td colspan='3'>";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;




                        $count++;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='80%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .=$childhtml;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td width='5%'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';



                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    ;
                    $displaymark = 0;



                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childhtml='';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));


                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;


                        $childhtml .= "<tr><td colspan='3'><b>";
                        $childhtml .= "Part " . $count;
                        $childhtml .= "</b></td></tr><tr><td colspan='3'>";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;



                        $count++;
                    endforeach;
                    $html .= "<tr><td width='5%'><b>";
                    $html .= "Question" . $result->TM_STU_QN_Number . ".";
                    $html .= "</b></td><td width='15%' align='left'>Marks: ";
                    $html .= $question->TM_QN_Totalmarks;
                    $html .= "</td><td width='80%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .=$childhtml;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td width='5%'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';



                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;


            endif;
        endforeach;
        $html .= "</table>";

        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf;">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $firstname.' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $username . '</td>
</tr></table>';


        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);

        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style="font-weight: lighter; color: #afafaf;" align="center">Disclaimer:This is the property ofTabbie Me Educational Solutions PVT Ltd.</span></td>

</tr></table>
');

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf', 'D');


    }
    public function actionInvitebuddies()
    {
        $user=Yii::app()->user->id;
        if(count($_POST['invitebuddies'])>0):
            for($i=0;$i<count($_POST['invitebuddies']);$i++):
                $recepchalange= new Chalangerecepient();
                $recepchalange->TM_CE_RE_Chalange_Id=$_POST['challengeid'];
                $recepchalange->TM_CE_RE_Recepient_Id=$_POST['invitebuddies'][$i];
                if($recepchalange->save(false)):
                    $this->SendMailChallengeInvite($_POST['invitebuddies'][$i]);
                    endif;
                $notification=new Notifications();
                $notification->TM_NT_User= $_POST['invitebuddies'][$i];
                $notification->TM_NT_Type='Challenge';
                $notification->TM_NT_Target_Id=Yii::app()->user->Id;
                $notification->TM_NT_Status='0';
                $notification->save(false);
            endfor;
            $criteria=new CDbCriteria;
            $criteria->condition='TM_CE_RE_Chalange_Id='.$_POST['challengeid'].' AND TM_CE_RE_Recepient_Id!='.$user;
            $users=Chalangerecepient::model()->findAll($criteria);
            $usersjson='Me';
            if(count($users)>0):
                $othercount=count($users);
                $others=" + <a href='javascript:void(0);' class='hover' data-id='".$_POST['challengeid']."' data-tooltip='tooltip".$_POST['challengeid']."' style='color: #FFA900;' >".$othercount." Others"."
            <div class='tooltiptable' id='tooltip".$_POST['challengeid']."' style='display: none; width: 400px;'>
            <a href='javascript:void(0);'class='cls' id='close'>x</a><div class='row'><div class='col-xs-6 col-md-12'><ul class='list-group'>".$this->GetChallengeBuddies($_POST['challengeid'])."</ul></div></div>
            </div></a>";
            else:
                $others='';
            endif;
            echo $usersjson.$others;
        else:
            echo "no";
        endif;
    }
    public function actionShowmockpaper($id)
    {
        if(isset($_POST['coomplete']))
        {

            $test=StudentMocks::model()->find(array('condition'=>'TM_STMK_Mock_Id=:mock AND TM_STMK_Student_Id=:student','params'=>array(':mock'=>$id,':student'=>Yii::app()->user->Id)));
            $test->TM_STMK_Status='1';
            $test->save(false);
            $this->redirect(array('mockcomplete','id'=>$id));

        }
        $paperquestions=$this->GetPaperQuestionsMock($id);
        $this->render('showpaperquestionmock',array('id'=>$id,'paperquestions'=>$paperquestions));
    }
    public function actionRedo()
    {

        if (isset($_POST['deleteflag'])):
            $questionId = $_POST['questonid'];
            $questionType = $_POST['type'];
            $testId = $_POST['testid'];
                $redoStudent = StudentQuestions::model()->find(array('condition' => "TM_STU_QN_Question_Id='$questionId'AND TM_STU_QN_Test_Id='$testId'"));
                $redoStudent->TM_STU_QN_Redo_Flag = $_POST['deleteflag'];
                if($_POST['deleteflag']=='1'):
                    $redoStudent->TM_STU_QN_Flag = 0;
                else:
                    $redoStudent->TM_STU_QN_Flag = 1;
                endif;
                $redoStudent->save(false);
        endif;
    }

    public function actionRevisionRedo($id)
    {
        $mode = ($_GET['redo']==''?$_POST['redo']:$_GET['redo']);
        $test=StudentTest::model()->findByPk($id);
        if($test->TM_STU_TT_Status=='1'):
            $test->TM_STU_TT_Status='2';
            $test->TM_STU_TT_Redo_Action=$mode;
            if ($mode == 'all'):
                $redoquestions=StudentQuestions::model()->findAll(array('condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test','params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)));
            else:
                $redoquestions=StudentQuestions::model()->findAll(array('condition'=>'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Redo_Flag=1','params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)));
            endif;
            foreach($redoquestions AS $rdq):
                $question=Questions::model()->findByPk($rdq->TM_STU_QN_Question_Id);
                if($question->TM_QN_Type_Id=='5'):
                    $childquestion=StudentQuestions::model()->findAll(array('condition'=>'TM_STU_QN_Parent_Id=:parent','params'=>array(':parent'=>$rdq->TM_STU_QN_Question_Id)));
                    foreach($childquestion AS $child):
                        $child->TM_STU_QN_Answer_Id='';
                        $child->TM_STU_QN_Answer='';
                        $child->save(false);
                    endforeach;
                endif;
                $rdq->TM_STU_QN_Answer_Id='';
                $rdq->TM_STU_QN_Answer='';
                $rdq->TM_STU_QN_Mark='';
                $rdq->TM_STU_QN_Flag='0';
                $rdq->TM_STU_QN_Redo_Flag='1';
                $rdq->TM_STU_QN_RedoStatus='0';
                $rdq->save(false);
            endforeach;
            $test->save(false);
        endif;
        $condition='';

            if (isset($_POST['action']['GoNext'])) {
                if ($_POST['QuestionType'] != '5'):
                    if (isset($_POST['answer'])):
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        if ($_POST['QuestionType'] == '1'):
                            $answer = implode(',', $_POST['answer']);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                            $answer = $_POST['answer'];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                        endif;
                        $selectedquestion->TM_STU_QN_RedoStatus = '1';
                        $selectedquestion->save(false);
                    else:
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        $selectedquestion->TM_STU_QN_RedoStatus = '1';
                        $selectedquestion->save(false);
                    endif;
                else:
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                    foreach ($questions AS $child):
                        if (isset($_POST['answer' . $child->TM_QN_Id])):
                            $childquestion = StudentQuestions::model()->findByAttributes(
                                array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                            );
                            if (count($childquestion) == '1'):
                                $selectedquestion = $childquestion;
                            else:
                                $selectedquestion = new StudentQuestions();
                            endif;
                            $selectedquestion->TM_STU_QN_Test_Id = $id;
                            $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                            $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                            if ($child->TM_QN_Type_Id == '1'):
                                $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                                $answer = $_POST['answer' . $child->TM_QN_Id];
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '4'):
                                $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                            endif;
                            $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                            $selectedquestion->save(false);
                        endif;
                    endforeach;
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
                $condition=array(
                    'condition'=>'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_RedoStatus=0 AND TM_STU_QN_Number>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                    'params'=>array(':test'=>$id,':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                );
            }
            if (isset($_POST['action']['GetPrevios'])){
                if ($_POST['QuestionType'] != '5'):
                    if (isset($_POST['answer'])):
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        if ($_POST['QuestionType'] == '1'):
                            $answer = implode(',', $_POST['answer']);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                            $answer = $_POST['answer'];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                        endif;
                        $selectedquestion->TM_STU_QN_RedoStatus = '1';
                        $selectedquestion->save(false);
                    else:
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        $selectedquestion->TM_STU_QN_RedoStatus = '1';
                        $selectedquestion->save(false);
                    endif;
                else:
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                    foreach ($questions AS $child):
                        if (isset($_POST['answer' . $child->TM_QN_Id])):
                            $childquestion = StudentQuestions::model()->findByAttributes(
                                array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                            );
                            if (count($childquestion) == '1'):
                                $selectedquestion = $childquestion;
                            else:
                                $selectedquestion = new StudentQuestions();
                            endif;
                            $selectedquestion->TM_STU_QN_Test_Id = $id;
                            $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                            $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                            if ($child->TM_QN_Type_Id == '1'):
                                $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                                $answer = $_POST['answer' . $child->TM_QN_Id];
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '4'):
                                $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                            endif;
                            $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                            $selectedquestion->save(false);
                        endif;
                    endforeach;
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
                $condition=array(
                    'condition'=>'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Number <:tableid AND TM_STU_QN_Parent_Id=0',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number DESC',
                    'params'=>array(':test'=>$id,':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                );
            }
            if (isset($_POST['action']['DoLater'])){
                if ($_POST['QuestionType'] != '5'):
                    if (isset($_POST['answer'])):
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        if ($_POST['QuestionType'] == '1'):
                            $answer = implode(',', $_POST['answer']);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                            $answer = $_POST['answer'];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                        endif;
                        $selectedquestion->TM_STU_QN_RedoStatus = '2';
                        $selectedquestion->save(false);
                    else:
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        $selectedquestion->TM_STU_QN_RedoStatus = '2';
                        $selectedquestion->save(false);
                    endif;
                else:
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                    foreach ($questions AS $child):
                        if (isset($_POST['answer' . $child->TM_QN_Id])):
                            $childquestion = StudentQuestions::model()->findByAttributes(
                                array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                            );
                            if (count($childquestion) == '1'):
                                $selectedquestion = $childquestion;
                            else:
                                $selectedquestion = new StudentQuestions();
                            endif;
                            $selectedquestion->TM_STU_QN_Test_Id = $id;
                            $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                            $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                            if ($child->TM_QN_Type_Id == '1'):
                                $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                                $answer = $_POST['answer' . $child->TM_QN_Id];
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '4'):
                                $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                            endif;
                            $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                            $selectedquestion->save(false);
                        endif;
                    endforeach;
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '2';
                    $selectedquestion->save(false);
                endif;
                $condition=array(
                    'condition'=>'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Number>:tableid',
                    'limit'=>1,
                    'order'=>'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                    'params'=>array(':test'=>$id,':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
                );
            }
            if (isset($_POST['action']['Complete'])) {
                if ($_POST['QuestionType'] != '5'):
                    if (isset($_POST['answer'])):
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        if ($_POST['QuestionType'] == '1'):
                            $answer = implode(',', $_POST['answer']);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                            $answer = $_POST['answer'];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($_POST['QuestionType'] == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                        endif;
                        $selectedquestion->TM_STU_QN_RedoStatus = '1';
                        $selectedquestion->save(false);
                    else:
                        $criteria = new CDbCriteria;
                        $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                        $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                        $selectedquestion = StudentQuestions::model()->find($criteria);
                        $selectedquestion->TM_STU_QN_RedoStatus = '1';
                        $selectedquestion->save(false);
                    endif;
                else:
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                    foreach ($questions AS $child):
                        if (isset($_POST['answer' . $child->TM_QN_Id])):
                            $childquestion = StudentQuestions::model()->findByAttributes(
                                array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                            );
                            if (count($childquestion) == '1'):
                                $selectedquestion = $childquestion;
                            else:
                                $selectedquestion = new StudentQuestions();
                            endif;
                            $selectedquestion->TM_STU_QN_Test_Id = $id;
                            $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                            $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                            if ($child->TM_QN_Type_Id == '1'):
                                $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                                $answer = $_POST['answer' . $child->TM_QN_Id];
                                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                            elseif ($child->TM_QN_Type_Id == '4'):
                                $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                            endif;
                            $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                            $selectedquestion->save(false);
                        endif;
                    endforeach;
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '1';
                $test->save(false);
                $this->redirect(array('RedoComplete', 'id' => $id,'mode'=>$mode));
            }
            if (isset($_GET['questionnum'])&& $condition==''):
                $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                    array(
                        'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                        'limit' => 1,
                        'order' => 'TM_STU_QN_Number ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            else:

                if($condition==''):
                    $condition=array(
                        'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student  AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_RedoStatus=0 AND TM_STU_QN_Parent_Id=0',
                        'limit' => 1,
                        'order' => 'TM_STU_QN_Number ASC',
                        'params' => array(':test'=>$id,':student' => Yii::app()->user->id)
                    );

                endif;
                $questionid = StudentQuestions::model()->find($condition);

            endif;
            if (count($questionid)):
                $question = Questions::model()->findByPk($questionid->TM_STU_QN_Question_Id);

                $this->renderPartial('showquestionredo', array('testid' => $id, 'question' => $question, 'questionid' => $questionid, 'mode' => $mode));
            else:
                //$this->redirect(array('revision'));
            endif;

    }
    public function actionMockRedo($id)
    {
        $mode = ($_GET['redo']==''?$_POST['redo']:$_GET['redo']);
        $test=StudentMocks::model()->findByPk($id);
        if($test->TM_STMK_Status=='1'):
            $test->TM_STMK_Status='2';
            $test->TM_STMK_Redo_Action=$mode;
            if ($mode == 'all'):
                $redoquestions=Studentmockquestions::model()->findAll(array('condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test','params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)));
            else:
                $redoquestions=Studentmockquestions::model()->findAll(array('condition'=>'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Redo_Flag=1','params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)));
            endif;
            foreach($redoquestions AS $rdq):
                $question=Questions::model()->findByPk($rdq->TM_STMKQT_Question_Id);
                if($question->TM_QN_Type_Id=='5'):
                    $childquestion=Studentmockquestions::model()->findAll(array('condition'=>'TM_STMKQT_Parent_Id=:parent','params'=>array(':parent'=>$rdq->TM_STMKQT_Question_Id)));
                    foreach($childquestion AS $child):
                        $child->TM_STMKQT_Answer='';
                        $child->save(false);
                    endforeach;endif;
                $rdq->TM_STMKQT_Answer='';
                $rdq->TM_STMKQT_Marks='';
                $rdq->TM_STMKQT_Flag='0';
                $rdq->TM_STMKQT_Redo_Flag='1';
                $rdq->TM_STMKQT_RedoStatus='0';
                $rdq->save(false);
            endforeach;
            $test->save(false);
        endif;
        $condition='';

        if (isset($_POST['action']['GoNext'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $condition=array(
                'condition'=>'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_RedoStatus=0 AND TM_STMKQT_Number>:tableid',
                'limit'=>1,
                'order'=>'TM_STMKQT_Number ASC, TM_STMKQT_RedoStatus ASC',
                'params'=>array(':test'=>$id,':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['GetPrevios'])){
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $condition=array(
                'condition'=>'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_Number <:tableid AND TM_STMKQT_Parent_Id=0',
                'limit'=>1,
                'order'=>'TM_STMKQT_Number DESC',
                'params'=>array(':test'=>$id,':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['DoLater'])){
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '2';
                $selectedquestion->save(false);
            endif;
            $condition=array(
                'condition'=>'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_Number>:tableid',
                'limit'=>1,
                'order'=>'TM_STMKQT_Number ASC, TM_STMKQT_RedoStatus ASC',
                'params'=>array(':test'=>$id,':student'=>Yii::app()->user->id,':tableid'=>$_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['Complete'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $test = StudentTest::model()->findByPk($id);
            $test->TM_STU_TT_Status = '1';
            $test->save(false);
            $this->redirect(array('RedoComplete', 'id' => $id,'mode'=>$mode));
        }
        if (isset($_GET['questionnum'])&& $condition==''):
            $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Number=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id)
                )
            );
        else:

            if($condition==''):
                $condition=array(
                    'condition' => 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student  AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_RedoStatus=0 AND TM_STMKQT_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Number ASC',
                    'params' => array(':test'=>$id,':student' => Yii::app()->user->id)
                );

            endif;
            $questionid = Studentmockquestions::model()->find($condition);

        endif;
        if (count($questionid)):
            $question = Questions::model()->findByPk($questionid->TM_STMKQT_Question_Id);

            $this->renderPartial('showquestionredo', array('testid' => $id, 'question' => $question, 'questionid' => $questionid, 'mode' => $mode));
        else:
            //$this->redirect(array('revision'));
        endif;

    }

    public function GetRedoTestQuestions($id, $question, $mode)
    {
        if ($mode == 'flagged'):
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Type NOT IN (6,7)';
            $criteria->params = array(':test' => $id);
            $criteria->order = 'TM_STU_QN_Number ASC';
            $selectedquestion = StudentQuestions::model()->findAll($criteria);
            $pagination = '';
            foreach ($selectedquestion AS $key => $testquestion):
                $count = $key + 1;
                $class = '';
                if ($testquestion->TM_STU_QN_Question_Id == $question) {
                    $questioncount = $count;
                }
                if ($testquestion->TM_STU_QN_Type == '6' || $testquestion->TM_STU_QN_Type == '7') {
                    $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
                }
                else
                {
                    $title = '';
                    $class = '';
                    if ($testquestion->TM_STU_QN_RedoStatus == '0') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : '');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : '');
                    }
                    elseif ($testquestion->TM_STU_QN_RedoStatus == '1')
                    {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="answered"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Already Answered');
                    }
                    elseif ($testquestion->TM_STU_QN_RedoStatus == '2')
                    {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                    }
                    $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_STU_QN_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('student/RevisionRedo', array('id' => $id,'redo'=>$mode, 'questionnum' => base64_encode('question_' . $testquestion->TM_STU_QN_Number)))) . '">' . $count . '</a></li>';
                }


            endforeach;


            return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count);


       else:
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0  AND TM_STU_QN_Type NOT IN (6,7)';
            $criteria->params = array(':test' => $id);
            $criteria->order = 'TM_STU_QN_Number ASC';
            $selectedquestion = StudentQuestions::model()->findAll($criteria);
            $pagination = '';
            foreach ($selectedquestion AS $key => $testquestion):
                $count = $key + 1;
                $class = '';
                if ($testquestion->TM_STU_QN_Question_Id == $question) {
                    $questioncount = $count;
                }
                if ($testquestion->TM_STU_QN_Type == '6' || $testquestion->TM_STU_QN_Type == '7') {
                    $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
                }
                else
                {
                    $title = '';
                    $class = '';
                    if ($testquestion->TM_STU_QN_RedoStatus == '0') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : '');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : '');
                    }
                    elseif ($testquestion->TM_STU_QN_RedoStatus == '1')
                    {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="answered"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Already Answered');
                    }
                    elseif ($testquestion->TM_STU_QN_RedoStatus == '2')
                    {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                    }
                    $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_STU_QN_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('student/RevisionRedo', array('id' => $id,'redo'=>$mode,  'questionnum' => base64_encode('question_' . $testquestion->TM_STU_QN_Number)))) . '">' . $count . '</a></li>';
                }
            endforeach;
            return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count);

        endif;
    }
	public function GetRedoCount($test)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Redo_Flag=1';
        $criteria->params = array(':test' => $test,':student'=>Yii::app()->user->id);
        $redoquestions = StudentQuestions::model()->count($criteria);
        return $redoquestions;
    }
	public function GetMockRedoCount($test)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND TM_STMKQT_Redo_Flag=1';
        $criteria->params = array(':test' => $test,':student'=>Yii::app()->user->id);
        $redoquestions = Studentmockquestions::model()->count($criteria);
        return $redoquestions;
    }
    public function actionGetchallengepdfAnswer($id)
    {
        $results=Chalangeuserquestions::model()->findAll(
            array(
                'condition'=>'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test',
                "order"=>'TM_CEUQ_Id ASC',
                'params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)
            )
        );
        $html="<table cellpadding='5' border='0' width='100%'>";
        $html.="<tr><td colspan=3 align='center'><b>Challenge</b></td></tr>";
        $html.="<tr><td colspan=3 align='center'>". date("d/m/Y")."</td></tr>";
        $number=1;
        foreach($results AS $key=>$result):
            if($result->TM_CEUQ_Difficulty==1):
                $mark=5;
            elseif($result->TM_CEUQ_Difficulty==2):
                $mark=8;
            elseif($result->TM_CEUQ_Difficulty==3):
                $mark=12;
            endif;
            $count=$key+1;
            $question=Questions::model()->findByPk($result->TM_CEUQ_Question_Id);
            if( $question->TM_QN_Parent_Id=='0' ):
                if($question->TM_QN_Type_Id=='1' || $question->TM_QN_Type_Id=='2' || $question->TM_QN_Type_Id=='3'|| $question->TM_QN_Type_Id=='4'):
                    $html.="<tr><td  colspan=2 ><b>Question ".$number++."</b> Marks : ".$mark."</td><td align='right'>Reference : ".$question->TM_QN_QuestionReff."</td></tr>";
                    $html.="<tr>";
                    $html.="<td colspan=2 >";
                    $html.=$question->TM_QN_Question;
                    $html.="</td></tr>";
                    if($question->TM_QN_Image!=''):
                        $html.="<tr><td colspan='3' align='right'><img src='". Yii::app()->request->baseUrl . "/site/Imagerender/id/".$question->TM_QN_Image."?size=thumbs&type=question&width=100&height=100'></td></tr>";
                    endif;
                    $html.="<tr><td colspan='2'>Sol:<br>";
                    $html.=$question->TM_QN_Solutions;
                    $html.="</td></tr>";
                    if($question->TM_QN_Solution_Image!=''):
                        $html.="<tr><td colspan='3' align='right'><img src='". Yii::app()->request->baseUrl . "/site/Imagerender/id/".$question->TM_QN_Solution_Image."?size=thumbs&type=solution&width=100&height=100'></td></tr>";
                    endif;
                    $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                endif;
                if($question->TM_QN_Type_Id=='5'):

                    $html.="<tr><td >";
                    $html.=$result->TM_CEUQ_Question_Id.".";
                    $html.="</td><td >";
                    $html.=$question->TM_QN_Question;
                    $html.="</td></tr>";
                    $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                    $count=1;
                    foreach ($questions AS $childquestion):

                        $html.="<tr><td >";
                        $html.="Part ".$count;
                        $html.="</td><td >";
                        $html.=$childquestion->TM_QN_Question;
                        $html.="</td></tr>";
                        if($childquestion->TM_QN_Image!=''):
                            $html.="<tr><td colspan='2'><img src=". Yii::app()->request->baseUrl."/site/Imagerender/id/".$childquestion->TM_QN_Image." alt="." height="."42"." width="."42"."></td></tr>";
                        endif;

                        $html.="<tr><td >";
                        $html.="Sol:</td><td width='95%'>";

                        $html.=$childquestion->TM_QN_Solutions;
                        $html.="</td></tr>";
                        if($childquestion->TM_QN_Solution_Image!=''):
                            $html.="<tr><td colspan='2'><img src=". Yii::app()->request->baseUrl."/site/Imagerender/id/".$childquestion->TM_QN_Solution_Image." alt="." height="."42"." width="."42"."></td></tr>";
                        endif;
                        $count++;
                    endforeach;
                endif;


            endif;
        endforeach;
        $html.="</table>";
        //echo $html;exit;


        $mpdf=new mPDF();
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf;">' . $firstname . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right; font-weight: lighter; color: #afafaf;">' . $username . '</td>
</tr></table>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username,.1);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
        <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"><tr>
        <!--<td width="33%"><span style="font-weight: bold; font-style: italic;">{DATE j-m-Y}</span></td>-->
        <td width="33%" align="center" style="font-weight: bold; color: #afafaf;">Disclaimer: This is the property of TabbieMe Educational Solutions Pvt Ltd.</td>
        <!--<td width="33%" style="text-align: right; ">Tabbie Me Revision</td>-->
        </tr></table>');

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf','D');


    }
    public function GetChallengeCount($challenge,$difficulty,$limit)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=:difficulty';
        $criteria->limit=$limit;
        $criteria->order='RAND()';
        $criteria->params=array(':test'=>$challenge,':difficulty'=>$difficulty);
        $questions=Chalangequestions::model()->findAll($criteria);
        return count($questions);
    }
    public function ChallengeAddQuestions($challenge,$difficulty,$limit,$user,$iteration)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=:difficulty';
        $criteria->limit=$limit;
        $criteria->order='RAND()';
        $criteria->params=array(':test'=>$challenge,':difficulty'=>$difficulty);
        $questions=Chalangequestions::model()->findAll($criteria);
        $count=$iteration;
        foreach($questions AS $key=>$question):
            $masterquestion=Questions::model()->findByPk($question->TM_CL_QS_Question_Id);
            $count++;
            $userquestion= new Chalangeuserquestions();
            $userquestion->TM_CEUQ_Challenge=$challenge;
            $userquestion->TM_CEUQ_Question_Id=$question->TM_CL_QS_Question_Id;
            $userquestion->TM_CEUQ_Difficulty=$question->TM_CL_QS_Difficulty;
            $userquestion->TM_CEUQ_User_Id=$user;
            $userquestion->TM_CEUQ_Iteration=$count;
            $userquestion->save(false);
            if($masterquestion->TM_QN_Type_Id=='5'):
                $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$question->TM_CL_QS_Question_Id)));
                    foreach($childQuestions AS $child):
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$challenge;
                        $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                        $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                        $userquestion->TM_CEUQ_User_Id=$user;
                        $userquestion->TM_CEUQ_Parent_Id=$question->TM_CL_QS_Question_Id;
                        $userquestion->save(false);
                    endforeach;
            endif;
        endforeach;
        return $count;
    }
    public function GetChapterTotal($chapter)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_QN_Topic_Id=:chapter AND TM_QN_Status=0 AND TM_QN_Parent_Id=0';
        $criteria->params=array(':chapter'=>$chapter);
        $questions=Questions::model()->count($criteria);
        return $questions;
    }
    public function actionChangePlan($id)
    {
        $plan=StudentPlan::model()->find(array('condition'=>'TM_SPN_StudentId='.Yii::app()->user->id.' AND TM_SPN_PlanId='.$id,'limit'=>1,'order'=>'TM_SPN_CreatedOn DESC'));
        $modules=Planmodules::model()->findAll(array('condition'=>'TM_PM_Plan_Id='.$plan->TM_SPN_PlanId));
        Yii::app()->session['plan'] = $plan->TM_SPN_PlanId;
        Yii::app()->session['publisher'] = $plan->TM_SPN_Publisher_Id;
        Yii::app()->session['syllabus'] = $plan->TM_SPN_SyllabusId;
        Yii::app()->session['standard'] = $plan->TM_SPN_StandardId;
        Yii::app()->session['subject'] = $plan->TM_SPN_SubjectId;
        Yii::app()->session['quick']=false;
        Yii::app()->session['difficulty']=false;
        Yii::app()->session['challenge']=false;
        Yii::app()->session['mock']=false;
        foreach($modules AS $module):
            switch($module->TM_PM_Module)
            {
                case "1":
                    Yii::app()->session['quick'] = true;
                    break;
                case "2":
                    Yii::app()->session['difficulty'] = true;
                    break;
                case "3":
                    Yii::app()->session['challenge'] = true;
                    break;
                case "4":
                    Yii::app()->session['mock'] = true;
                    break;
            }
        endforeach;
        $this->redirect(array('home'));
    }
    public function actionLeaderboard()
    {

        if(isset($_GET['startdate'])):
            $dates=$this->GetWeek($_GET['startdate']);
        else:
            $dates=$this->GetWeek(date('Y-m-d'));
        endif;
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_STP_Points) AS totalpoints,TM_STP_Student_Id,TM_STP_Date');
        $criteria->condition="TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '".$dates[0]."' AND '".$dates[1]."'";
        $criteria->order='totalpoints DESC';
        $criteria->group='TM_STP_Student_Id';
        $criteria->limit='5';
        $criteria->params=array(':standard'=>Yii::app()->session['standard']);
        $studenttotal=StudentPoints::model()->findAll($criteria);
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_STP_Points) AS totalpoints');
        $criteria->condition="TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '".$dates[0]."' AND '".$dates[1]."' AND TM_STP_Student_Id=".Yii::app()->user->id;
        $criteria->order='totalpoints DESC';
        $criteria->group='TM_STP_Student_Id';
        $criteria->params=array(':standard'=>Yii::app()->session['standard']);
        $usertotal=StudentPoints::model()->find($criteria);
        $this->render('leaderboard',array('studenttotal'=>$studenttotal,'usertotal'=>$usertotal,'dates'=>$dates));
    }
    public function AddPoints($earnedmarks,$testid,$testtype,$user)
    {
        $studentpintcount=StudentPoints::model()->count(
                        array('condition'=>'TM_STP_Student_Id=:student AND TM_STP_Test_Id=:test AND TM_STP_Test_Type=:type',
                            'params'=>array(':student'=>$user,':test'=>$testid,'type'=>$testtype)
                        ));
        if($studentpintcount==0)
        {
            $studentpoint=new StudentPoints();
            $studentpoint->TM_STP_Student_Id=$user;
            $studentpoint->TM_STP_Points=$earnedmarks;
            $studentpoint->TM_STP_Test_Id=$testid;
            $studentpoint->TM_STP_Test_Type=$testtype;
            $studentpoint->TM_STP_Standard_Id=Yii::app()->session['standard'];
            $studentpoint->save(false);
        }
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_STP_Points) AS totalpoints');
        $criteria->condition='TM_STP_Student_Id=:student AND TM_STP_Standard_Id=:standard';
        $criteria->params=array(':student'=>$user,':standard'=>Yii::app()->session['standard']);
        $studenttotal=StudentPoints::model()->find($criteria);
        $returnarray=array('total'=>$studenttotal->totalpoints);
        $levels=Levels::model()->findAll(array('condition'=>'TM_LV_Status=0'));
        $levelcount=Studentlevels::model()->count(array('condition'=>'TM_STL_Student_Id='.$user));
        foreach($levels AS $level):
            if($studenttotal->totalpoints >= $level->TM_LV_Minpoint & $studenttotal->totalpoints<$level->TM_LV_MaxPoint):
                $studentlevelcount=Studentlevels::model()->count(array('condition'=>'TM_STL_Student_Id='.$user.' AND TM_STL_Level_Id='.$level->TM_LV_Id));
                if($studentlevelcount==0):
                    $addlevel=new Studentlevels();
                    $addlevel->TM_STL_Level_Id=$level->TM_LV_Id;
                    $addlevel->TM_STL_Student_Id=$user;
                    $addlevel->TM_STL_Order=$levelcount+1;
                    $addlevel->save(false);
                    $returnarray['newlevel']=true;
                    $notification=new Notifications();
                    $notification->TM_NT_User=$user;
                    $notification->TM_NT_Type='Level';
                    $notification->TM_NT_Target_Id=$level->TM_LV_Id;
                    $notification->TM_NT_Status='0';
                    $notification->save(false);
                else:
                    $returnarray['newlevel']=false;
                endif;
                $returnarray['studentlevel']=$level->TM_LV_Id;
            endif;
        endforeach;
        return $returnarray;
    }
    public function GetWeek($week)
    {
        $ts = strtotime($week);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday midnight', $ts);
        $previous_week = strtotime("-1 week +1 day",$ts);
        $previous_week = strtotime("last sunday midnight",$previous_week);
        $next_week = strtotime("+1 week +1 day",$ts);
        $next_week = strtotime("last sunday midnight",$next_week);
        return array(date('Y-m-d', $start),date('Y-m-d', strtotime('next saturday', $start)),date('F Y', $start),date('Y-m-d', $previous_week),date('Y-m-d', $next_week));
    }
    public function actionGetpdfMockAnswer($id)
    {

        $results = MockQuestions::model()->findAll(
            array(
                'condition' => 'TM_MQ_Mock_Id=:test ',

                'params' => array(':test' => $id)
            )
        );

        $totalquestions=count($results);


        $html = "<table cellpadding='5' border='0' width='100%'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>MOCK SOLUTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";





        foreach ($results AS $key => $result):
            $count = $key + 1;
            $question = Questions::model()->findByPk($result->TM_MQ_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Type_Id == '1' || $question->TM_QN_Type_Id == '2' || $question->TM_QN_Type_Id == '3' || $question->TM_QN_Type_Id == '4'):
                    $html .= "<tr><td width='5%'>";
                    $html .= "Question" .$count. ".";
                    $html .= "</td><td width='15%' align='left'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='80%' align='right'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";


                    $html .= "<tr><td colspan='3' ><b>";


                    $html .= $question->TM_QN_Question;
                    $html .= "</b></td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td colspan='3'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';


                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ( $question->TM_QN_Type_Id == '6'):
                    $html .= "<tr><td width='5%'>";
                    $html .= "Question" .$count. ".";
                    $html .= "</td><td width='15%' align='left'>Marks: ";
                    $html .= $question->TM_QN_Totalmarks;
                    $html .= "</td><td width='80%' align='right'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";


                    $html .= "<tr><td colspan='3' ><b>";


                    $html .= $question->TM_QN_Question;
                    $html .= "</b></td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td colspan='3'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';


                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '5' ):
                    ;
                    $displaymark = 0;



                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml='';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;

                        $childhtml .= "<tr><td colspan='3'>";
                        $childhtml .= "Part " . $countpart;
                        $childhtml .= "</td></tr><tr><td colspan='3'>";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;



                        $count++;
                    endforeach;
                    $html .= "<tr><td width='5%'>";
                    $html .= "Question" .$count. ".";
                    $html .= "</td><td width='15%' align='left'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='80%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .=$childhtml;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td width='5%'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';



                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    ;
                    $displaymark = 0;



                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml='';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;

                        $childhtml .= "<tr><td colspan='3'>";
                        $childhtml .= "Part " . $countpart;
                        $childhtml .= "</td></tr><tr><td colspan='3'>";
                        $childhtml .= $childquestion->TM_QN_Question;
                        $childhtml .= "</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;



                        $count++;
                    endforeach;
                    $html .= "<tr><td width='5%'>";
                    $html .= "Question" .$count. ".";
                    $html .= "</td><td width='15%' align='left'>Marks: ";
                    $html .= $question->TM_QN_Totalmarks;
                    $html .= "</td><td width='80%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .=$childhtml;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td width='5%'>";
                        $html .= "Ans:</td></tr><tr><td colspan='3'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';



                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;


            endif;
        endforeach;
        $html .= "</table>";

        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-size: small"><body style="font-family: serif;  color: #afafaf;">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $firstname.' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter;"></td>
<td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf; ">' . $username . '</td>
</tr></table>';


        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);

        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style="font-weight: lighter; color: #afafaf;" align="center">Disclaimer:This is the property ofTabbie Me Educational Solutions PVT Ltd.</span></td>

</tr></table>
');

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf', 'D');


    }
    public function actionSendMailReminders()

    {
        $recepientid = $_POST["recepientid"];



        $userId=Yii::app()->user->getId();
        $userDet=User::model()->findByPk($userId);
        $recepiendet=User::model()->findByPk($recepientid);
        $studentDet=Student::model()->find(array('condition' => "TM_STU_User_Id='$recepientid'"));
        $parentDet=User::model()->findByPk($studentDet->TM_STU_Parent_Id);
        $content='<p>Dear '.Student::model()->find(array('condition' => "TM_STU_User_Id='" . $recepientid . "'"))->TM_STU_First_Name.'</p>

                  <p>Hey, I have sent you a challenge. </p>
                  <p>Do accept it and crack it ! Good luck ! </p>

                   <p>Cheers, '.Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name.' </p>';
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Port = 465;
        $mail->IsHTML(true);
        $mail->SMTPSecure = "ssl";
        $mail->Username = 'vigil@solminds.com';
        $mail->Password = 'jimbruttan123';
        $mail->SetFrom('vigil@solminds.com', 'TabbieMe');
        $mail->Subject = 'TabbieMath: Reminder for Challenge';
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($content);
        $mail->AddAddress($parentDet->email, '');
        //$mail->AddAddress('karthik@solminds.com', 'Karthik');



        if(!$mail->Send()) {

            echo "Mailer Error: " . $mail->ErrorInfo;

        } else {


            return success;

        }
    }
    public function SendMailChallengeInvite($id)

    {




        $userId=Yii::app()->user->getId();
        $userDet=User::model()->findByPk($userId);
        $recepiendet=User::model()->findByPk($id);
        $studentDet=Student::model()->find(array('condition' => "TM_STU_User_Id='$id'"));
        $parentDet=User::model()->findByPk($studentDet->TM_STU_Parent_Id);
        $content='<p>Dear '.Student::model()->find(array('condition' => "TM_STU_User_Id='" . $id . "'"))->TM_STU_First_Name.'</p>

<p>You are invited to a challenge by '.Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name.'</p>
<p>Login to TabbieMath and accept the challenge</p>
<p>'.Yii::app()->createUrl('user/login').'</p>

<p>Thank you</p>
<p>Support. </p>';
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;

        $mail->IsSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Port = 465;
        $mail->IsHTML(true);
        $mail->SMTPSecure = "ssl";
        $mail->Username = 'vigil@solminds.com';
        $mail->Password = 'jimbruttan123';
        $mail->SetFrom('vigil@solminds.com', 'TabbieMe');
        $mail->Subject = 'Accept the Challenge';
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($content);
        $mail->AddAddress($parentDet->email, '');
        //$mail->AddAddress('karthik@solminds.com', 'Karthik');



        if(!$mail->Send()) {

            echo "Mailer Error: " . $mail->ErrorInfo;

        } else {


            return success;

        }
    }
}