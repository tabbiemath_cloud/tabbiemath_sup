<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/7/2015
 * Time: 12:37 PM
 */

class ChallengeController extends Controller {

    public $layout='//layouts/main';

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Challenge','SendMail','Startchallenge','GetpdfAnswer','Invitebuddies','ChallengeGetSolution','GetchallengepdfAnswer'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudent()'
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('ChallengeTest','challengeresult','deletechallenge','Cancelchallenge','Acceptchallenge'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudentChalange($_GET["id"])'
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + deletetest,deletechallenge,Cancelchallenge,Deletemock', // mewe only allow deletion via POST request
            //'postOnly + copy', // we only allow copy via POST request
        );
    }


    public function actionStartchallenge($id)
    {
        $test=Challenges::model()->findByPk($id);
        $this->render('startchallenge',array('id'=>$id,'test'=>$test));

    }

    public function GetStudentDet()
    {
        $user=Yii::app()->user->id;
        $plan=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId='.$user));
        return $plan[0];
    }
    public function actionGetTopics()
    {
        $arr = explode(',', $_POST['topicids']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_TP_Id' ,$arr );
        $model = Chapter::model()->findAll($criteria);
        $topic='';
        foreach ($model as $key=>$value) {
            $topic=($key=='0'?$value->TM_TP_Name:$topic.','.$value->TM_TP_Name);
        }
        echo $topic;
    }
    public function OptionHint($type,$question)
    {
        $returnval=array();
        if($type=='1'):
            $corectans=Answers::model()->count(
                array(
                    'condition'=>'TM_AR_Question_Id=:question AND TM_AR_Correct=:correct',
                    'params'=>array(':question'=>$question,':correct'=>'1')
                ));
            $returnval['hint']='<span class="optionhint">Select any '.$corectans.' options</span>';
            $returnval['correctoptions']=$corectans;
        elseif($type=='2'):
            $returnval['hint']='<span class="optionhint">Select an option</span>';
            $returnval['correctoptions']='1';
        elseif($type=='3'):
            $returnval['hint']='<span class="optionhint">Select either True Or False</span>';
            $returnval['correctoptions']='1';
        elseif($type=='4'):
            $returnval['hint']='<span class="optionhint">Enter Your Answer</span>';
            $returnval['correctoptions']='0';
        elseif($type=='5'):
            $returnval['hint']='<span class="optionhint">Answer the following questions</span>';
            $returnval['correctoptions']='0';
        endif;
        return $returnval;
    }


    public function actionDeletechallenge($id)
    {
        $challenge=Challenges::model()->findByPk($id);
        if($challenge->TM_CL_Chalanger_Id==Yii::app()->user->id):

                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CL_QS_Chalange_Id='.$id);
                $model = Chalangequestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CEUQ_Challenge='.$id);
                $model = Chalangeuserquestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CE_CP_Challenge_Id='.$id);
                $model = ChallengeChapters::model()->deleteAll($criteria);
                $challenge->TM_CL_Status='1';
                $challenge->save(false);
                echo "yes";
        else:
                echo "no";
        endif;
    }
    public function actionCancelchallenge($id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status='4';
        if($model->save(false)):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id);
            $model = Chalangeuserquestions::model()->deleteAll($criteria);
            echo "yes";
        else:
            echo "no";
        endif;
    }
    public function actionAcceptchallenge($id)
    {

        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status='1';
        $return='';
        if($model->save(false)):
            $return.='<td data-title="Date">'.($model->challenge->TM_CL_Chalanger_Id==Yii::app()->user->id?"Me":$this->GetChallenger($model->challenge->TM_CL_Chalanger_Id)).'</td>';
            $return.='<td data-title="Publisher">'.date("d-m-Y", strtotime($model->challenge->TM_CL_Created_On)).'</td>';
            $return.='<td data-title="Division">'.$this->GetChallengeInvitees($model->challenge->TM_CL_Id,Yii::app()->user->id).'</td>';
            $return.='<td data-title="Link">';
            $return.="<a href='".Yii::app()->createUrl('student/startchallenge', array('id' => $id))."' class='btn btn-warning'>".(Student::model()->GetChallengeStatus($id,Yii::app()->user->id)>0?'Continue':'Start')."</a>";
            $return.='</td>';
            $return.='<td data-title="Completed">';
            $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                'student/cancelchallenge/'.$id,
                array (
                    'type'=>'POST',
                    'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$id.'").remove();} }'
                ),
                array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$id)
            );
            $return.='</td>';
        echo $return;
        else:
            echo "no";
        endif;
    }
    public function actionChallenge()
    {
        $plan=$this->GetStudentDet();
        $mainchapters=Chapter::model()->findAll(array('condition'=>"TM_TP_Syllabus_Id='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'"));
        if(isset($_POST['Challenge'])):
           if(count($_POST['chapters'])>0):
                $chapters='';
                $topics='';
                $challenge= new Challenges();
                $challenge->TM_CL_Chalanger_Id=Yii::app()->user->id;
                $challenge->TM_CL_QuestionCount=$_POST['Challenge']['questions'];
                $challenge->save(false);
                $recepchalange= new Chalangerecepient();
                $recepchalange->TM_CE_RE_Chalange_Id=$challenge->TM_CL_Id;
                $recepchalange->TM_CE_RE_Recepient_Id=Yii::app()->user->id;
                $recepchalange->TM_CE_RE_Status='1';
                $recepchalange->save(false);
               if(count($_POST['invitebuddies'])>0):
                   for($i=0;$i<count($_POST['invitebuddies']);$i++):
                       $recepchalange= new Chalangerecepient();
                       $recepchalange->TM_CE_RE_Chalange_Id=$challenge->TM_CL_Id;
                       $recepchalange->TM_CE_RE_Recepient_Id=$_POST['invitebuddies'][$i];
                       $recepchalange->save(false);
                   endfor;
               endif;
                for($i=0;$i<count($_POST['chapters']);$i++):
                    $chapters=$chapters.($i==0?$_POST['chapters'][$i]:",".$_POST['chapters'][$i]);
                    $chapterid=$_POST['chapters'][$i];
                    $challengechapter= new ChallengeChapters();
                    $challengechapter->TM_CE_CP_Challenge_Id=$challenge->TM_CL_Id;
                    $challengechapter->TM_CE_CP_Chapter_Id=$chapterid;
                    $challengechapter->save(false);
                    for($j=0;$j<count($_POST['topic'.$chapterid]);$j++):
                        $topics=$topics.($i==0 & $j==0?$_POST['topic'.$chapterid][$j]:",".$_POST['topic'.$chapterid][$j]);
                    endfor;
                endfor;
               $Syllabus= $plan->TM_SPN_SyllabusId;
               $Standard= $plan->TM_SPN_StandardId;
               //$Topic=$topics;
               $Test=$challenge->TM_CL_Id;
               $Student=Yii::app()->user->id;
               $limit=$challenge->TM_CL_QuestionCount;
               //set questions for basic
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,1,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $basiccount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                //set questions for intermediate
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,2,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $intercount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
               //set questions for advanced
               $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'".$chapters."','".$topics."',:limit,:Test,3,@out)");
               $command->bindParam(":Syllabus", $Syllabus);
               $command->bindParam(":Standard", $Standard);
               $command->bindParam(":limit",$limit);
               $command->bindParam(":Test",$Test);
               $command->query();
               $advancecount=Yii::app()->db->createCommand("select @out as result;")->queryScalar();
            endif;
            $this->redirect(array('home'));
        endif;
        $this->render('challenge',array('plan'=>$plan,'chapters'=>$mainchapters));
    }
    public function actionChallengeTest($id)
    {
        $challengecount=Challenges::model()->findByPk($id)->TM_CL_QuestionCount;
        if(isset($_POST['action']['GoNext'])):
                    //change challenge status to started
                    $criteria = new CDbCriteria;
                    $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
                    $model = Chalangerecepient::model()->find($criteria);
                    if($model->TM_CE_RE_Status=='1'):
                        $model->TM_CE_RE_Status='2';
                        $model->save(false);
                    endif;
                    $chuserquest=Chalangeuserquestions::model()->findByPk($_POST['ChallengeQuestionId']);
                    $masterquestion=Questions::model()->findByPk($_POST['QuestionId']);
                    $difficulty=$chuserquest->TM_CEUQ_Difficulty;
                        if($difficulty=='1'):
                            $marks='5';
                        elseif($difficulty=='2'):
                            $marks='8';
                        elseif($difficulty=='3'):
                            $marks='12';
                        endif;
                        if($_POST['QuestionType']=='1'):
                            $answer=implode(',',$_POST['answer']);
                            $chuserquest->TM_CEUQ_Answer_Id=$answer;
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                if($ans->TM_AR_Correct=='1'):
                                    $correctcount++;
                                    if (array_search($ans->TM_AR_Id, $_POST['answer'])!==false) {
                                        $usercorrect++;
                                    }
                                endif;
                            endforeach;
                            if($correctcount==$usercorrect):
                                switch ($difficulty) {
                                    case "1";
                                        $chuserquest->TM_CEUQ_Marks='5';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        $chuserquest->TM_CEUQ_Marks='8';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                        break;
                                    case "3";
                                            $chuserquest->TM_CEUQ_Marks='12';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                        break;
                                }
                            else:
                                switch ($difficulty) {
                                    case "1";
                                        if($this->NextDiffCount($id,'1')>0):
                                            $nextdifficulty='1';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                }
                            endif;
                        elseif($_POST['QuestionType']=='2' || $_POST['QuestionType']=='3'):
                            $answer=$_POST['answer'];
                            $answerarray=array($_POST['answer']);
                            $chuserquest->TM_CEUQ_Answer_Id=$answer;
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                if($ans->TM_AR_Correct=='1'):
                                    $correctcount++;
                                    if (array_search($ans->TM_AR_Id, $answerarray)!==false) {
                                        $usercorrect++;
                                    }
                                endif;
                            endforeach;
                            if($correctcount==$usercorrect):
                                switch ($difficulty) {
                                    case "1";
                                        $chuserquest->TM_CEUQ_Marks='5';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        $chuserquest->TM_CEUQ_Marks='8';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                }
                            else:
                                switch ($difficulty) {
                                    case "1";
                                        if($this->NextDiffCount($id,'1')>0):
                                            $nextdifficulty='1';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                }
                            endif;
                        elseif($_POST['QuestionType']=='4'):
                            $chuserquest->TM_CEUQ_Answer_Id=$_POST['answer'];
                            $correctcount=0;
                            $usercorrect=0;
                            foreach($masterquestion->answers AS $ans):
                                $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                $useranswer=  trim(strtolower($_POST['answer']));
                                for($i=0;$i<count($answeroption);$i++)
                                {
                                   $option= trim($answeroption[$i]);
                                    if(strcmp($useranswer,$option)==0):
                                        $usercorrect++;
                                    endif;
                                }
                            endforeach;
                            if($usercorrect!=0):
                                switch ($difficulty) {
                                    case "1";
                                        $chuserquest->TM_CEUQ_Marks='5';
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        $chuserquest->TM_CEUQ_Marks='8';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        $chuserquest->TM_CEUQ_Marks='12';
                                        if($this->NextDiffCount($id,'3')>0):
                                            $nextdifficulty='3';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                }
                            else:
                                switch ($difficulty) {
                                    case "1";
                                        if($this->NextDiffCount($id,'1')>0):
                                            $nextdifficulty='1';
                                        else:
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                    case "2";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                $nextdifficulty='1';
                                            endif;
                                        endif;
                                        break;
                                    case "3";
                                        if($this->NextDiffCount($id,'2')>0):
                                            $nextdifficulty='2';
                                        else:
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                $nextdifficulty='3';
                                            endif;
                                        endif;
                                        break;
                                }
                            endif;
                        else:
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$masterquestion->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            $totalmarks=0;
                            $marks=0;
                            foreach($questions AS $child):
                                $selectedquestion = Chalangeuserquestions::model()->find(
                                    array('condition'=>'TM_CEUQ_Question_Id=:parent AND TM_CEUQ_Challenge=:challenge AND TM_CEUQ_User_Id=:student',
                                    'params'=>array(':parent'=>$child->TM_QN_Id,':challenge'=>$id,':student'=>Yii::app()->user->id))
                                );

                                if($child->TM_QN_Type_Id=='1'):
                                    $answer=implode(',',$_POST['answer'.$child->TM_QN_Id]);
                                    $selectedquestion->TM_CEUQ_Answer_Id=$answer;
                                    foreach ($child->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if (array_search($ans->TM_AR_Id, $answer) !== false) {
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                elseif($child->TM_QN_Type_Id=='2' || $child->TM_QN_Type_Id=='3'):
                                    $answer=$_POST['answer'.$child->TM_QN_Id];
                                    $selectedquestion->TM_CEUQ_Answer_Id=$answer;
                                    foreach ($child->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if ($ans->TM_AR_Id==$answer){
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;

                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;

                                elseif($child->TM_QN_Type_Id=='4'):
                                    $selectedquestion->TM_CEUQ_Answer_Id=$_POST['answer'.$child->TM_QN_Id];
                                    foreach ($child->answers AS $ans):

                                        $answeroption=explode(';',strtolower(strip_tags($ans->TM_AR_Answer)));
                                        $useranswer=  trim(strtolower($selectedquestion->TM_CEUQ_Answer_Id));
                                        for($i=0;$i<count($answeroption);$i++)
                                        {
                                            $option= trim($answeroption[$i]);
                                            if(strcmp($useranswer,$option)==0):
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            endif;
                                        }
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;

                                    endforeach;
                                endif;
                                $selectedquestion->save(false);
                            endforeach;
                            if($totalmarks==$marks):
                                    switch ($difficulty) {
                                        case "1";
                                            $chuserquest->TM_CEUQ_Marks='5';
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                if($this->NextDiffCount($id,'3')>0):
                                                    $nextdifficulty='3';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                        case "2";
                                            $chuserquest->TM_CEUQ_Marks='8';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                        case "3";
                                            $chuserquest->TM_CEUQ_Marks='12';
                                            if($this->NextDiffCount($id,'3')>0):
                                                $nextdifficulty='3';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                    }
                            else:
                                    switch ($difficulty) {
                                        case "1";
                                            if($this->NextDiffCount($id,'1')>0):
                                                $nextdifficulty='1';
                                            else:
                                                if($this->NextDiffCount($id,'2')>0):
                                                    $nextdifficulty='2';
                                                else:
                                                    $nextdifficulty='3';
                                                endif;
                                            endif;
                                            break;
                                        case "2";
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                if($this->NextDiffCount($id,'3')>0):
                                                    $nextdifficulty='3';
                                                else:
                                                    $nextdifficulty='1';
                                                endif;
                                            endif;
                                            break;
                                        case "3";
                                            if($this->NextDiffCount($id,'2')>0):
                                                $nextdifficulty='2';
                                            else:
                                                if($this->NextDiffCount($id,'1')>0):
                                                    $nextdifficulty='1';
                                                else:
                                                    $nextdifficulty='3';
                                                endif;
                                            endif;
                                            break;
                                    }
                            endif;
                        endif;
                $chuserquest->TM_CEUQ_Status='1';
                $chuserquest->save(false);
                if($challengecount=='10' & $chuserquest->TM_CEUQ_Iteration<='5'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;

                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount=='15' & $chuserquest->TM_CEUQ_Iteration<='10'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount=='20' & $chuserquest->TM_CEUQ_Iteration<='15'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);

                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount=='25' & $chuserquest->TM_CEUQ_Iteration<='20'):
                    $nextcount=$chuserquest->TM_CEUQ_Iteration+5;
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge='.$id.' AND TM_CEUQ_User_Id='.Yii::app()->user->id.')';
                    $criteria->limit='1';
                    $criteria->order='RAND()';
                    $criteria->params=array(':test'=>$id);
                    $nextquestions=Chalangequestions::model()->find($criteria);
                    if(count($nextquestions)>0):
                        $masterquestion=Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$id;
                        $userquestion->TM_CEUQ_Question_Id=$nextquestions->TM_CL_QS_Question_Id;
                        $userquestion->TM_CEUQ_Difficulty=$nextquestions->TM_CL_QS_Difficulty;
                        $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                        $userquestion->TM_CEUQ_Iteration=$nextcount;
                        $userquestion->save(false);
                        if($masterquestion->TM_QN_Type_Id=='5'):
                            $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$nextquestions->TM_CL_QS_Question_Id)));
                            foreach($childQuestions AS $child):
                                $userquestion= new Chalangeuserquestions();
                                $userquestion->TM_CEUQ_Challenge=$id;
                                $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                                $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                                $userquestion->TM_CEUQ_User_Id=Yii::app()->user->id;
                                $userquestion->TM_CEUQ_Parent_Id=$nextquestions->TM_CL_QS_Question_Id;
                                $userquestion->save(false);
                            endforeach;
                        endif;
                    endif;
                elseif($challengecount==$chuserquest->TM_CEUQ_Iteration):
                    $criteria=new CDbCriteria;
                    $criteria->condition='TM_CE_RE_Chalange_Id=:test AND TM_CE_RE_Recepient_Id=:user';
                    $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
                    $nextquestions=Chalangerecepient::model()->find($criteria);
                    $nextquestions->TM_CE_RE_Status='3';
                    $nextquestions->save(false);
                    $this->redirect(array('challengeresult','id'=>$id));
                endif;

        endif;
        if(Student::model()->GetChallengeStatus($id,Yii::app()->user->id)==0):
            // insert for basic 3 questions
            $difficulty=1;
            $easylimit=3;
            $iteration=0;
            $easycount=$this->GetChallengeCount($id,$difficulty,$easylimit);
            if($easycount < $easylimit):
                $interiteration=$this->ChallengeAddQuestions($id,$difficulty,$easycount,Yii::app()->user->id,$iteration);
                $difficulty=2;
                $newlimit=2+$easycount;
            else:

                $interiteration=$this->ChallengeAddQuestions($id,$difficulty,$easylimit,Yii::app()->user->id,$iteration);
                $difficulty=2;
                $newlimit=2;
            endif;

            // insert $interlimit intermediate 2 questions
            $intercount=$this->GetChallengeCount($id,$difficulty,$newlimit);

            if($intercount<$newlimit):
                $advanceiteration=$this->ChallengeAddQuestions($id,$difficulty,$intercount,Yii::app()->user->id,$interiteration);
                $difficulty=3;
                $newlimit=2-$intercount;
            else:
                $advanceiteration=$this->ChallengeAddQuestions($id,$difficulty,$newlimit,Yii::app()->user->id,$interiteration);
                $difficulty=3;
                $newlimit=0;
            endif;
            // if not enough intermediate insert from advance
            if($newlimit!=0):
                $advancecount=$this->GetChallengeCount($id,$difficulty,$newlimit);
                if($advancecount < $newlimit):
                    $iteration=$this->ChallengeAddQuestions($id,$difficulty,$newlimit,Yii::app()->user->id,$advanceiteration);
                else:
                    $iteration=$this->ChallengeAddQuestions($id,$difficulty,$newlimit,Yii::app()->user->id,$advanceiteration);
                endif;
            endif;
        endif;
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CEUQ_Challenge=:test AND TM_CEUQ_Status=0 AND TM_CEUQ_User_Id=:user AND TM_CEUQ_Parent_Id=0';
        $criteria->limit='1';
        $criteria->order='TM_CEUQ_Iteration ASC';
        $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
        $question=Chalangeuserquestions::model()->find($criteria);
        $this->renderPartial('challengequestion',array('testid'=>$id,'challengequestion'=>$question,'question'=>$question->question,'challengecount'=>$challengecount));

    }
    public function actionChallengeresult($id)
    {
        $challengecount=Challenges::model()->findByPk($id)->TM_CL_QuestionCount;
        if($challengecount=='10'):
            $examtotalmarks='79';
        elseif($challengecount=='15'):
            $examtotalmarks='139';
        elseif($challengecount=='20'):
            $examtotalmarks='199';
        elseif($challengecount=='25'):
            $examtotalmarks='199';
        endif;
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_CEUQ_Marks) AS total');
        $criteria->condition='TM_CEUQ_Challenge=:test AND TM_CEUQ_User_Id=:user';
        $criteria->params=array(':test'=>$id,':user'=>Yii::app()->user->id);
        $studenttotal=Chalangeuserquestions::model()->find($criteria);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id.' AND TM_CE_RE_Recepient_Id='.Yii::app()->user->id);
        $recepient = Chalangerecepient::model()->find($criteria);
        $recepient->TM_CE_RE_ObtainedMarks=$studenttotal->total;
        $points=$studenttotal->total;
        $recepient->save(false);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$id);
        $criteria->order='TM_CE_RE_ObtainedMarks DESC';
        $participants = Chalangerecepient::model()->findAll($criteria);
        $recepients=count($participants);
        //echo $recepients;exit;
        $userids=array();
        foreach($participants AS $party):
            $userids[]=$party->TM_CE_RE_Recepient_Id;
        endforeach;


        $results = Chalangeuserquestions::model()->findAll(
            array(
                'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Parent_Id=0 ',
                "order" => 'TM_CEUQ_Id ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $earnedmarks = 0;
        $result = '';
        $qstnnum=1;
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_CEUQ_Question_Id);
                $displaymark = 0;
                if($exam->TM_CEUQ_Difficulty==1):
                    $displaymark=5;
                elseif($exam->TM_CEUQ_Difficulty==2):
                    $displaymark=8;
                elseif($exam->TM_CEUQ_Difficulty==3):
                    $displaymark=12;
                endif;
                if ($question->TM_QN_Type_Id != '5'):
                    $marks = 0;
                    if ($question->TM_QN_Type_Id != '4' ):
                        $challengeans=Chalangeuserquestions::model()->find(array('condition'=>"TM_CEUQ_Challenge='$id' AND TM_CEUQ_Question_Id='$exam->TM_CEUQ_Question_Id'"));
                        $studentanswer = explode(',', $challengeans->TM_CEUQ_Answer_Id);
                        if (count($studentanswer) > 0):
                            $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh>';
                            for ($i = 0; $i < count($studentanswer); $i++)
                            {
                                $answer = Answers::model()->findByPk($studentanswer[$i]);
                                $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=200&height=200" alt=""></div>' : '') . '</li>';
                            }
                            $correctanswer .= '</ul>';
                        else:
                            $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh><li class="list-group-item1" style="display:flex ;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($exam->TM_CEUQ_Answer_Id != ''):
                            $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh><li class="list-group-item1" style="display:flex ;"><div class="col-md-12">' . $exam->TM_CEUQ_Answer_Id . '</div></li></ul>';
                        else:
                            $correctanswer = '<ul class="list-group" ><lh>Your Answer:</lh><li class="list-group-item1" style="display:flex ;">Question Skipped</li></ul>';
                        endif;
                    endif;
                        $result .= '<tr><td style="background-color: white;"><div class="col-md-3">';
                        if($exam->TM_CEUQ_Marks=='0'):
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question.' . $qstnnum++.'</span>';
                        /*elseif($marks<$displaymark & $marks!='0'):
                            $result .= '<span class="clrr"><img src="'.Yii::app()->request->baseUrl.'/images/rw.png"></i>  Question.' .$qstnnum++.'</span>';*/
                        else:
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question.' .$qstnnum++.'</span>';
                        endif;

                        $result .= ' <span class="pull-right">Mark: '.$exam->TM_CEUQ_Marks.'/' . $displaymark . '</span></div>
                            <div class="col-md-4 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="'.$question->TM_QN_Id.'" style="font-size:12px;margin-left: 42%;color: rgb(50, 169, 255);">Report Problem</a>
                            <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>'.($question->TM_QN_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>':'').'</li></div>';
                        $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                        $result .= '<div class="col-md-12 Itemtest clickable-row ">
                            <div class="sendmailtest suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                            </div>
                            <div class="col-md-12" id="mailSendCompletetest" style="display: none;">Your Message Has Been Sent To Admin</div>
                            </div>
                             </td></tr>';

                        //glyphicon glyphicon-star sty
                else:
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));

                    $count = 1;
                    $studentmarks=0;
                    $totalmarks=0;
                    $childresult='';
                    foreach ($questions AS $childquestion):
                        $childresults = Chalangeuserquestions::model()->find(
                            array(
                                'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Question_Id=:questionid',
                                'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                            )
                        );
                            if ($childquestion->TM_QN_Type_Id != '4'):
                                if ($childresults->TM_CEUQ_Answer_Id != ''):
                                    $studentanswer = explode(',', $childresults->TM_CEUQ_Answer_Id);
                                    if (count($studentanswer) > 0):
                                        $correctanswer = '<ul class="list-group"><lh>Your Answer:</lh>';
                                        for ($i = 0; $i < count($studentanswer); $i++)
                                        {
                                            $answer = Answers::model()->findByPk($studentanswer[$i]);
                                            $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=200&height=200" alt=""></div>' : '') . '</li></ul>';

                                        }
                                        $correctanswer .= '</ul>';
                                    endif;
                                else:
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                                endif;
                            else:
                                if ($childresults->TM_CEUQ_Answer_Id != ''):
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_CEUQ_Answer_Id . '</li></ul>';
                                else:
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                                endif;

                            endif;

                                $childresult .= '<div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '')) . '</li></div>';
                                    $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                                //glyphicon glyphicon-star sty
                        $count++;
                    endforeach;
                    $result .= '<tr><td style="background-color: white;"><div class="col-md-3">';
                    if($exam->TM_CEUQ_Marks=='0'):
                        $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question.' . $qstnnum++.'</span>';
                    /*elseif($marks<$displaymark & $marks!='0'):
                        $result .= '<span class="clrr"><img src="'.Yii::app()->request->baseUrl.'/images/rw.png"></i>  Question.' .$qstnnum++.'</span>';*/
                    else:
                        $result .= '<span ><i class="fa fa fa-check"></i>  Question.' .$qstnnum++.'</span>';
                    endif;
                    $result .= ' <span class="pull-right">Mark: '.$exam->TM_CEUQ_Marks.'/' . $displaymark . '</span></div>
                            <div class="col-md-4 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="'.$question->TM_QN_Id.'" style="font-size:12px;margin-left: 42%;color: rgb(50, 169, 255);">Report Problem</a>
                            <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:' . $question->TM_QN_QuestionReff . '</span></div>';

                    //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                    $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>'.($question->TM_QN_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>':'').'</li></div>';
                    $result .=$childresult;
                    $result .= '<div class="col-md-12 Itemtest clickable-row ">
                            <div class="sendmailtest suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                            </div>
                            <div class="col-md-12" id="mailSendCompletetest" style="display: none;">Your Message Has Been Sent To Admin</div>
                            </div>
                             </td></tr>';
                endif;
        endforeach;
        $this->render('challengeresult',array('totalmarks'=>$examtotalmarks,'studenttotal'=>$studenttotal->total,'points'=>$points,'participants'=>$participants,'recepients'=>$recepients,'userids'=>$userids,'testresult' => $result));
    }
    public function actionChallengeGetSolution()
    {
        if(isset($_POST['challengeid'])):
            $results=Chalangeuserquestions::model()->findAll(
                array(
                    'condition'=>'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test',
                    'params'=>array(':student'=>Yii::app()->user->id,':test'=>$_POST['challengeid'])
                )
            );
            $resulthtml='';
            $count=1;
            foreach($results AS $result):
                if($result->TM_CEUQ_Difficulty==1):
                    $mark=5;
                elseif($result->TM_CEUQ_Difficulty==2):
                    $mark=8;
                elseif($result->TM_CEUQ_Difficulty==3):
                    $mark=12;
                endif;
                $question=Questions::model()->findByPk($result->TM_CEUQ_Question_Id);
                if($question->TM_QN_Type_Id!='5'):
                    $resulthtml.='<tr><td style="background-color: white;"><div class="col-md-3">
                        <span style="font-weight: 700;"> Question '.$count++.'</span>
                        <span class="pull-right">Marks : '.$mark.'</span>
                        </div>
                        <div class="col-md-3 pull-right">
                        <a href="javascript:void(0)" class="showSolutionItem" data-reff="'.$result->TM_CEUQ_Question_Id.'" style="font-size:12px;color: rgb(50, 169, 255);">Report Problem</a>
                        <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:'.$question->TM_QN_QuestionReff.'</span>
                        </div>';
                    $resulthtml.='<div class="col-md-12">
                    <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Question . '</div>'.($question->TM_QN_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>':'').'</div>';

                    $resulthtml.='<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div>
                    <div class="col-md-12">

                    <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Solutions . '</div>'.($question->TM_QN_Solution_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>':'').'</div>';

                    //$resulthtml.='<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div><div class="col-md-12">'.$question->TM_QN_Solutions.'</div>';

                    $resulthtml.= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestion'.$result->TM_CEUQ_Question_Id.'" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" hidden/></div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden ></div>
                               </div><div class="col-md-12" id="mailSendComplete" style="display: none;">Your Message Has Been Sent To Admin</div></div>

                            </td></tr>';
                else:
                    $chldquestions= Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                    $resulthtml.='<tr><td style="background-color: white;"><div class="col-md-3">
                        <span style="font-weight: 700;"> Question '.$count++.'</span>
                        <span class="pull-right">Marks : '.$mark.'</span>
                        </div>
                        <div class="col-md-3 pull-right">
                        <a href="javascript:void(0)" class="showSolutionItem" data-reff="'.$result->TM_CEUQ_Question_Id.'" style="font-size:12px;color: rgb(50, 169, 255);">Report Problem</a>
                        <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:'.$question->TM_QN_QuestionReff.'</span>
                        </div>';
                    $resulthtml.='<div class="col-md-12">
                    <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Question . '</div>'.($question->TM_QN_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>':'').'</div>';
                    foreach($chldquestions AS $key=>$child):
                        $count=$key+1;
                        $resulthtml.='<div class="col-md-12"><p>Part ' . $count . ':</p></div><div class="col-md-12"><div class="col-md-'.($child->TM_QN_Image != ''?'10':'12').'">' . $child->TM_QN_Question . '</div>' . (($child->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $child->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '')) . '</div></div>';
                    endforeach;
                    $resulthtml.='<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div>
                    <div class="col-md-12">

                    <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Solutions . '</div>'.($question->TM_QN_Solution_Image != ''?'<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>':'').'</div>';

                    //$resulthtml.='<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div><div class="col-md-12">'.$question->TM_QN_Solutions.'</div>';

                    $resulthtml.= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestion'.$result->TM_CEUQ_Question_Id.'" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" hidden/></div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden ></div>
                               </div><div class="col-md-12" id="mailSendComplete" style="display: none;">Your Message Has Been Sent To Admin</div></div>

                            </td></tr>';
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }

    public function GetQuestionCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test';
        $criteria->params=array(':test'=>$id);
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        return count($selectedquestion);
    }

    public function GetQuickLimits($total)
    {
        $mode=$total%3;
        if($mode==0):
            $count=$total/3;
            return array('basic'=>$count,'intermediate'=>$count,'advanced'=>$count);
        elseif($mode==1):
            $count=round($total/3);
            return array('basic'=>$count,'intermediate'=>$count+1,'advanced'=>$count);
        elseif($mode==2):
            $count=$total/3;
            return array('basic'=>ceil($count),'intermediate'=>ceil($count),'advanced'=>floor($count));
        endif;

    }

    public function GetBuddies($user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='superuser=0 AND usertype=6 AND status=1 AND id!='.$user;
        $users=User::model()->findAll($criteria);
        $usersjson='';
        foreach($users AS $key=>$user):
            $usersjson.=($key==0?'{"id":'.$user->id.', "name":"'.$user->username.'"}':',{"id":'.$user->id.', "name":"'.$user->username.'"}');
        endforeach;
        return $usersjson;

    }
    public function GetChallengeInvitees($challenge,$user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Recepient_Id!='.$user;
        $users=Chalangerecepient::model()->findAll($criteria);
        $usersjson='Me';
        if(count($users)>0):
            $othercount=count($users);
            $others=" + <a href='javascript:void(0);' class='hover' data-id='$challenge' data-tooltip='tooltip$challenge' style='color: #FFA900;'>".$othercount." Others"."
            <div class='tooltiptable' id='tooltip$challenge' style='display: none; width: 400px;'>
            <a href='javascript:void(0);'class='cls' id='close'>x</a><div class='row'><div class='col-md-12'><ul class='list-group'>".$this->GetChallengeBuddies($challenge)."</ul></div></div>
            </div></a>";
        else:
            $others='';
        endif;
        /*foreach($users AS $key=>$user):
            if($key<3):
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username;
                $usersjson.=','.$challengeuser;
            endif;
        endforeach;*/
        return $usersjson.$others;

    }
    public function GetChallengeBuddies($challenge)
    {
        $userid=Yii::app()->user->id;
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge;
        $users=Chalangerecepient::model()->findAll($criteria);
        $buddies='';
        $userids=array();
        foreach($users AS $party):
            $userids[]=$party->TM_CE_RE_Recepient_Id;
        endforeach;
        foreach($users AS $key=>$user):
            if($user->TM_CE_RE_Status=='0'):
                $status= "Not Started1";
            elseif($user->TM_CE_RE_Status=='1'):
                $status= "Not Started2";
            elseif($user->TM_CE_RE_Status=='2'):
                $status= "Not Completed";
            elseif($user->TM_CE_RE_Status=='3'):
                $status= $this->GetChallengePosition($userids, $user->TM_CE_RE_Recepient_Id);
            elseif($user->TM_CE_RE_Status=='4'):
                $status= "Declined";
            endif;
            if($user->TM_CE_RE_Recepient_Id==$userid):
                $challengeuser='Me';
            else:
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username."(".Student::model()->find(array('condition' => "TM_STU_User_Id='" . $user->TM_CE_RE_Recepient_Id . "'"))->TM_STU_First_Name." ".Student::model()->find(array('condition' => "TM_STU_User_Id='" . $user->TM_CE_RE_Recepient_Id . "'"))->TM_STU_Last_Name. ")";


            endif;

            $buddies.="<li class='list-group-item padding5'>
            <img src='http://lifestyle.iloveindia.com/lounge/images/oblong-face-hairstyles.jpg' class='img-circle tab_imgsmall'>
            <span>$challengeuser</span>
            <span style='margin-left: 35px;position: absolute; right: 0; margin: 2px;'>$status</span>
            </li>";
        endforeach;
        return $buddies;

    }
    public function GetChallenger($id)
    {

        return Student::model()->find(array('condition' => "TM_STU_User_Id='" .$id . "'"))->TM_STU_First_Name." ".Student::model()->find(array('condition' => "TM_STU_User_Id='" . $id . "'"))->TM_STU_Last_Name ;


    }
    public function GetChallangeInviteStatus($challenge)
    {
        $user=Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Recepient_Id!='.$user.' AND TM_CE_RE_Status IN (1,2)');
        $model = Chalangerecepient::model()->count($criteria);
        return $model;
    }
    public function getChallengeLinks($id,$user)
    {
        $challenge=Challenges::model()->findByPk($id);
        $return='';
        if($challenge->TM_CL_Chalanger_Id==$user):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge->TM_CL_Id.' AND TM_CE_RE_Recepient_Id='.$user);
            $recepient = Chalangerecepient::model()->find($criteria);
            $return.='<td data-title="Link">';
                if($recepient->TM_CE_RE_Status=='1'):
                    $return.="<a href='".Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Start</a>";
                elseif($recepient->TM_CE_RE_Status=='2'):
                    $return.="<a href='".Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Continue</a>";
                endif;
            $return.='</td>';
            $return.='<td data-title="Completed">';
                if($this->GetChallangeInviteStatus($id)>0):
                    $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                        Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?')
                    );
                else:
                    $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                        Yii::app()->request->baseUrl.'/student/deletechallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to delete this Challenge?')
                    );
                endif;
            $return.='</td>';
        else:
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id='.$challenge->TM_CL_Id.' AND TM_CE_RE_Recepient_Id='.$user);
            $recepient = Chalangerecepient::model()->find($criteria);

            if($recepient->TM_CE_RE_Status=='0'):
                $return.='<td data-title="Link">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/right.png','',array('class'=>'hvr-buzz-out','title'=>'Accept Challenge')),
                    Yii::app()->request->baseUrl.'/student/acceptchallenge/'.$challenge->TM_CL_Id,
                    array (
                        'type'=>'POST',
                        'success'=>'function(html){ if(html!="no"){$("#challengerow'.$challenge->TM_CL_Id.'").html(html);} }'
                    )
                );
                $return.='</td>';
                $return.='<td></td>';
                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out','title'=>'Cancel Challenge')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                    array (
                        'type'=>'POST',
                        'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                    ),
                    array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='1'):
                $return.='<td></td>';
                $return.='<td data-title="Link">';
                $return.="<a href='".Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Start</a>";
                $return.='</td>';
                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                    );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='2'):
                $return.='<td></td>';
                $return.='<td data-title="Link">';
                $return.="<a href='".Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id))."' class='btn btn-warning'>Continue</a>";
                $return.='</td>';

                $return.='<td data-title="Completed">';
                $return.= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')),
                    Yii::app()->request->baseUrl.'/student/cancelchallenge/'.$challenge->TM_CL_Id,
                        array (
                            'type'=>'POST',
                            'success'=>'function(html){ if(html=="yes"){$("#challengerow'.$challenge->TM_CL_Id.'").remove();} }'
                        ),
                        array ('confirm'=>'Are you sure you want to cancel this Challenge?','id'=>'cancel'.$challenge->TM_CL_Id)
                    );
                $return.='</td>';
            elseif($recepient->TM_CE_RE_Status=='3'):
            elseif($recepient->TM_CE_RE_Status=='4'):
            endif;
        endif;
        return $return;
    }
    public function NextDiffCount($id,$nextdifficulty)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty='.$nextdifficulty.' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge="'.$id.'" AND TM_CEUQ_User_Id="'.Yii::app()->user->id.'")';
        $criteria->limit='1';
        $criteria->order='RAND()';
        $criteria->params=array(':test'=>$id);
        return Chalangequestions::model()->count($criteria);

    }
    public function GetChallengePosition($users,$id)
    {
        $position=array_keys($users, $id);
        $position=$position[0]+1;

        if($position=='1'):
            return $position.'<sup>st</sup><span class="fa fa-trophy size2 brd_r2"></span>';
        elseif($position=='2'):
            return $position.'<sup>nd</sup>';
        elseif($position=='3'):
            return $position.'<sup>rd</sup>';
        else:
            return $position.'<sup>th</sup>';
        endif;
    }
    public function actionSendMail()

    {
        $questionId = $_POST["questionId"];
        $comments = $_POST["comments"];

        $question=Questions::model()->findByPk($questionId);
        $sendQuestion= $question->TM_QN_Question;
        $sendReff= $question->TM_QN_QuestionReff;
        $userId=Yii::app()->user->getId();
        $userDet=User::model()->findByPk($userId);
        $studentDet=Student::model()->findAll(array('condition' => "TM_STU_User_Id='$userId'"));

        $content='<p>A problem has been reported by '.$studentDet->TM_STU_First_Name.$studentDet->TM_STU_Last_Name.'('.$userDet->username.'),</p><p>'.$comments.'</p><p> Thank You</p>';
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;

        $mail->IsSMTP();

        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        $mail->SetFrom('services@tabbiemath.com', 'TabbieMe');
        $mail->Subject = 'Problem reported Ref:'.$sendReff;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($content);
        $mail->AddAddress('rems.varghese@gmail.com', 'Rema');



        if(!$mail->Send()) {

            echo "Mailer Error: " . $mail->ErrorInfo;

        } else {

            echo "Message sent!";
            return success;

        }
    }
    /*Mock Related functions*/

    // view history ends
    public function GetTotal($id)
    {
        $criteria=new CDbCriteria;
        $criteria->select =array('SUM(TM_AR_Marks) AS totalmarks');
        $criteria->condition='TM_AR_Question_Id=:question';
        $criteria->params=array(':question'=>$id);
        $totalmarks=Answers::model()->find($criteria);
        return $totalmarks->totalmarks;
    }
    public function actionGetpdfAnswer($id)
    {
        $results = StudentQuestions::model()->findAll(
            array(
                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7)',
                "order" => 'TM_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $totalquestions=count($results);
        $html = "<table cellpadding='5' border='0' width='100%'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>REVISION SOLUTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";





        foreach ($results AS $key => $result):
            $count = $key + 1;
            $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Type_Id == '1' || $question->TM_QN_Type_Id == '2' || $question->TM_QN_Type_Id == '3' || $question->TM_QN_Type_Id == '4' || $question->TM_QN_Type_Id == '6'):
                    $html .= "<tr><td width='5%'>";
                    $html .= "Question" . $result->TM_STU_QN_Number . ".";
                    $html .= "</td><td width='5%'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='90%' align='right'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";


                    $html .= "<tr><td colspan='3' >";


                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=200&height=200"></td></tr>';
                    endif;
                    if($question->TM_QN_Solutions!=''):
                        $html .= "<tr><td width='5%'>";
                        $html .= "Ans:</td><td width='95%'>";

                        $html .= $question->TM_QN_Solutions;
                        $html .= "</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    else:
                        $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';


                    endif;
                    $totalquestions--;
                    if($totalquestions!=0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '5' || $question->TM_QN_Type_Id == '7'):
                    ;
                    $displaymark = 0;

                    $html .= "<tr><td width='5%'>";
                    $html .= "Question" . $result->TM_STU_QN_Number . ".";
                    $html .= "</td><td width='5%'>Marks: ";
                    $html .= $displaymark;
                    $html .= "</td><td width='90%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";

                    $html .= "<tr><td colspan='3' >";
                    $html .= $question->TM_QN_Question;
                    $html .= "</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=200&height=200"></td></tr>';
                    endif;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;

                        $html .= "<tr><td width='5%'>";
                        $html .= "Part " . $count;
                        $html .= "</td><td colspan='2' width='95%'>";
                        $html .= $childquestion->TM_QN_Question;
                        $html .= "</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=200&height=200"></td></tr>';
                        endif;

                        if($question->TM_QN_Solutions!=''):
                            $html .= "<tr><td width='5%'>";
                            $html .= "Ans:</td><td width='95%'>";

                            $html .= $question->TM_QN_Solutions;
                            $html .= "</td></tr>";
                            if ($question->TM_QN_Solution_Image != ''):
                                $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                            endif;
                        else:
                            $html .= '<tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';


                            $totalquestions--;
                            if($totalquestions!=0):
                                $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                            endif;
                        endif;

                        $count++;
                    endforeach;
                endif;


            endif;
        endforeach;
        $html .= "</table>";

        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; font-style: italic;"><tr>
<td width="33%"><span style="font-weight: lighter; font-style: italic;">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right;font-weight: lighter; ">' . $firstname . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; font-style: italic;"></span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right;font-weight: lighter; ">' . $username . '</td>
</tr></table>';


        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);

        $mpdf->SetWatermarkText(Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name, .04);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; font-style: italic;"><tr>
<td width="100%" align="center"><span style="font-weight: lighter; font-style: italic;" align="center">Disclaimer:This is the property ofTabbie Me Educational Solutions PVT Ltd.</span></td>

</tr></table>
');

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf', 'D');


    }

    public function actionInvitebuddies()
    {
        $user=Yii::app()->user->id;
        if(count($_POST['invitebuddies'])>0):
            for($i=0;$i<count($_POST['invitebuddies']);$i++):
                $recepchalange= new Chalangerecepient();
                $recepchalange->TM_CE_RE_Chalange_Id=$_POST['challengeid'];
                $recepchalange->TM_CE_RE_Recepient_Id=$_POST['invitebuddies'][$i];
                $recepchalange->save(false);
            endfor;
            $criteria=new CDbCriteria;
            $criteria->condition='TM_CE_RE_Chalange_Id='.$_POST['challengeid'].' AND TM_CE_RE_Recepient_Id!='.$user;
            $users=Chalangerecepient::model()->findAll($criteria);
            $usersjson='Me';
            if(count($users)>0):
                $othercount=count($users);
                $others=" + <a href='javascript:void(0);' class='hover' data-id='".$_POST['challengeid']."' data-tooltip='tooltip".$_POST['challengeid']."' style='color: #FFA900;' >".$othercount." Others"."
            <div class='tooltiptable' id='tooltip".$_POST['challengeid']."' style='display: none; width: 400px;'>
            <a href='javascript:void(0);'class='cls' id='close'>x</a><div class='row'><div class='col-xs-6 col-md-12'><ul class='list-group'>".$this->GetChallengeBuddies($_POST['challengeid'])."</ul></div></div>
            </div></a>";
            else:
                $others='';
            endif;
            echo $usersjson.$others;
        else:
            echo "no";
        endif;
    }
    public function actionGetchallengepdfAnswer($id)
    {
        $results=Chalangeuserquestions::model()->findAll(
            array(
                'condition'=>'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test',
                "order"=>'TM_CEUQ_Id ASC',
                'params'=>array(':student'=>Yii::app()->user->id,':test'=>$id)
            )
        );
        $html="<table cellpadding='5' border='0' width='100%'>";
        $html.="<tr><td colspan=3 align='center'><b>Challenge</b></td></tr>";
        $html.="<tr><td colspan=3 align='center'>". date("d/m/Y")."</td></tr>";
        $number=1;
        foreach($results AS $key=>$result):
            if($result->TM_CEUQ_Difficulty==1):
                $mark=5;
            elseif($result->TM_CEUQ_Difficulty==2):
                $mark=8;
            elseif($result->TM_CEUQ_Difficulty==3):
                $mark=12;
            endif;
            $count=$key+1;
            $question=Questions::model()->findByPk($result->TM_CEUQ_Question_Id);
            if( $question->TM_QN_Parent_Id=='0' ):
                if($question->TM_QN_Type_Id=='1' || $question->TM_QN_Type_Id=='2' || $question->TM_QN_Type_Id=='3'|| $question->TM_QN_Type_Id=='4'):
                    $html.="<tr><td  colspan=2 ><b>Question ".$number++."</b> Marks : ".$mark."</td><td align='right'>Reference : ".$question->TM_QN_QuestionReff."</td></tr>";
                    $html.="<tr>";
                    $html.="<td colspan='3' >";
                    $html.=$question->TM_QN_Question;
                    $html.="</td></tr>";
                    if($question->TM_QN_Image!=''):
                        $html.="<tr><td colspan='3' align='right'><img src='". Yii::app()->request->baseUrl . "/site/Imagerender/id/".$question->TM_QN_Image."?size=thumbs&type=question&width=100&height=100'></td></tr>";
                    endif;
                    $html.="<tr><td colspan='3'>Sol:<br>";
                    $html.=$question->TM_QN_Solutions;
                    $html.="</td></tr>";
                    if($question->TM_QN_Solution_Image!=''):
                        $html.="<tr><td colspan='3' align='right'><img src='". Yii::app()->request->baseUrl . "/site/Imagerender/id/".$question->TM_QN_Solution_Image."?size=thumbs&type=solution&width=100&height=100'></td></tr>";
                    endif;
                    $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                endif;
                if($question->TM_QN_Type_Id=='5'):

                    $html.="<tr><td  colspan=2 ><b>Question ".$number++."</b> Marks : ".$mark."</td><td align='right'>Reference : ".$question->TM_QN_QuestionReff."</td></tr>";
                    $html.="<tr>";
                    $html.="<td colspan=2 >";
                    $html.=$question->TM_QN_Question;
                    $html.="</td></tr>";
                    $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                    $count=1;
                    foreach ($questions AS $childquestion):

                        $html.="<tr><td colspan='3'>Part :".$count."<br>";
                        $html.=$childquestion->TM_QN_Question;
                        $html.="</td></tr>";
                        if($childquestion->TM_QN_Image!=''):
                            $html.="<tr><td colspan='3'  align='right'><img src='". Yii::app()->request->baseUrl."/site/Imagerender/id/".$childquestion->TM_QN_Image."?size=thumbs&type=question&width=100&height=100'></td></tr>";
                        endif;
                        $count++;
                    endforeach;
                    $html.="<tr><td colspan='3'>Sol:<br>";
                    $html.=$question->TM_QN_Solutions;
                    $html.="</td></tr>";
                    if($question->TM_QN_Solution_Image!=''):
                        $html.="<tr><td colspan='3' align='right'><img src='". Yii::app()->request->baseUrl . "/site/Imagerender/id/".$question->TM_QN_Solution_Image."?size=thumbs&type=solution&width=100&height=100'></td></tr>";
                    endif;
                    $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                endif;


            endif;
        endforeach;
        $html.="</table>";
        //echo $html;exit;


        $mpdf=new mPDF();
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; font-style: italic;"><tr>
<td width="33%"><span style="font-weight: lighter; font-style: italic;">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right;font-weight: lighter; ">' . $firstname . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; font-style: italic;"></span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right;font-weight: lighter; ">' . $username . '</td>
</tr></table>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText(Student::model()->find(array('condition'=>"TM_STU_User_Id='".Yii::app()->user->Id."'"))->TM_STU_First_Name);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
        <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"><tr>
        <!--<td width="33%"><span style="font-weight: bold; font-style: italic;">{DATE j-m-Y}</span></td>-->
        <td width="33%" align="center" style="font-weight: bold; font-style: italic;">Disclaimer: This is the property of TabbieMe Educational Solutions Pvt Ltd.</td>
        <!--<td width="33%" style="text-align: right; ">Tabbie Me Revision</td>-->
        </tr></table>');

        $mpdf->WriteHTML($html);
        $mpdf->Output('print.pdf','D');


    }
    public function GetChallengeCount($challenge,$difficulty,$limit)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=:difficulty';
        $criteria->limit=$limit;
        $criteria->order='RAND()';
        $criteria->params=array(':test'=>$challenge,':difficulty'=>$difficulty);
        $questions=Chalangequestions::model()->findAll($criteria);
        return count($questions);
    }
    public function ChallengeAddQuestions($challenge,$difficulty,$limit,$user,$iteration)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=:difficulty';
        $criteria->limit=$limit;
        $criteria->order='RAND()';
        $criteria->params=array(':test'=>$challenge,':difficulty'=>$difficulty);
        $questions=Chalangequestions::model()->findAll($criteria);
        $count=$iteration;
        foreach($questions AS $key=>$question):
            $masterquestion=Questions::model()->findByPk($question->TM_CL_QS_Question_Id);
            $count++;
            $userquestion= new Chalangeuserquestions();
            $userquestion->TM_CEUQ_Challenge=$challenge;
            $userquestion->TM_CEUQ_Question_Id=$question->TM_CL_QS_Question_Id;
            $userquestion->TM_CEUQ_Difficulty=$question->TM_CL_QS_Difficulty;
            $userquestion->TM_CEUQ_User_Id=$user;
            $userquestion->TM_CEUQ_Iteration=$count;
            $userquestion->save(false);
            if($masterquestion->TM_QN_Type_Id=='5'):
                $childQuestions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=:parent','params'=>array(':parent'=>$question->TM_CL_QS_Question_Id)));
                    foreach($childQuestions AS $child):
                        $userquestion= new Chalangeuserquestions();
                        $userquestion->TM_CEUQ_Challenge=$challenge;
                        $userquestion->TM_CEUQ_Question_Id=$child->TM_QN_Id;
                        $userquestion->TM_CEUQ_Difficulty=$child->TM_QN_Dificulty_Id;
                        $userquestion->TM_CEUQ_User_Id=$user;
                        $userquestion->TM_CEUQ_Parent_Id=$question->TM_CL_QS_Question_Id;
                        $userquestion->save(false);
                    endforeach;
            endif;
        endforeach;
        return $count;
    }
    public function GetChapterTotal($chapter)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_QN_Topic_Id=:chapter AND TM_QN_Status=0 AND TM_QN_Parent_Id=0';
        $criteria->params=array(':chapter'=>$chapter);
        $questions=Questions::model()->count($criteria);
        return $questions;
    }
}