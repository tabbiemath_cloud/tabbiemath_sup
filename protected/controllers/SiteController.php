<?php

class SiteController extends Controller
{
//    public $layout='//layouts/main';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
        $this->layout='//layouts/main';
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

     public function actionMigrateWirisQuestions()
    {

        $from = 0;
        $questions=Questions::model()->findAll(array('condition'=>"TM_QN_Question LIKE '%<img%'", 'order'=>"TM_QN_Id ASC", 'limit' => "2000"));
        if(count($questions) > 0)
        {
            foreach ($questions as $question) {
                $questionString = $question['TM_QN_Question'];
                $s = explode('data-mathml="',$questionString);
                $t = explode('" ',$s[1]);
                $first = str_replace("«","<",$t[0]);
                $second = str_replace("»",">",$first);
                $third = str_replace("§","&",$second);
                $fourth = str_replace("¨",'"',$third);
                $fifth = str_replace("`","'",$fourth);
                $fifth;
                $question->TM_QN_Question = $fifth;
                $question->TM_QN_UpdatedOn = date('Y-m-d H:i:s');
                $question->update(false);
            }
            echo 'success';
            exit;
        }

    }
    
     public function actionMigrateWirisSolutions()
    {

        $questions=Questions::model()->findAll(array('condition'=>"TM_QN_Solutions LIKE '%<img%'", 'order'=>"TM_QN_Id ASC", 'limit' => "2000"));
        if(count($questions) > 0)
        {
            foreach ($questions as $question) {
                $questionString = $question['TM_QN_Solutions'];
                $s = explode('data-mathml="',$questionString);
                $t = explode('" ',$s[1]);

                $first = str_replace("«","<",$t[0]);
                $second = str_replace("»",">",$first);
                $third = str_replace("§","&",$second);
                $fourth = str_replace("¨",'"',$third);
                $fifth = str_replace("`","'",$fourth);

                $fifth;
                $question->TM_QN_Solutions = $fifth;
                $question->TM_QN_UpdatedOn = date('Y-m-d H:i:s');
                $question->update(false);
            }
            echo 'success';
            exit;
        }

    }
    
    public function actionMigrateWirisAnswers()
    {

        $from = 0;
        $questions=Answers::model()->findAll(array('condition'=>"TM_AR_Answer LIKE '%<img%'", 'order'=>"TM_AR_Id ASC", 'limit' => "2000"));
        if(count($questions) > 0)
        {
            foreach ($questions as $question) {
                $questionString = $question['TM_AR_Answer'];
                $s = explode('data-mathml="',$questionString);
                $t = explode('" ',$s[1]);
                $first = str_replace("«","<",$t[0]);
                $second = str_replace("»",">",$first);
                $third = str_replace("§","&",$second);
                $fourth = str_replace("¨",'"',$third);
                $fifth = str_replace("`","'",$fourth);
                $fifth;
                $question->TM_AR_Answer = $fifth;
                // $question->TM_QN_UpdatedOn = date('Y-m-d H:i:s');
                $question->update(false);
            }
            echo 'success';
            exit;
        }

    }
    public function actionImagerender($id)
    {
        Yii::import('application.extensions.image.Image');
        if($_GET['type']=='question'):
            if($_GET['size']=='large'):
                $image='questionimages/'.$id;
            else:
                $image='questionimages/thumbs/'.$id;
            endif;
        elseif($_GET['type']=='answer'):
            if($_GET['size']=='large'):
                $image='answerimages/'.$id;
            else:
                $image='answerimages/thumbs/'.$id;
            endif;
        elseif($_GET['type']=='solution'):
            if($_GET['size']=='large'):
                $image='solutionimages/'.$id;
            else:
                $image='solutionimages/thumbs/'.$id;
            endif;
        elseif($_GET['type']=='level'):
            $image='levelicons/'.$id;
        elseif($_GET['type']=='user'):
            $image='userimages/'.$id;
        endif;
        $image = new Image('images/'.$image);
        $image->resize($_GET['width'], $_GET['height'])->quality(90);
        $image->render();
    }
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionCheckimages()
    {
        $return='<table class="" border="1" cellpadding="5" cellspacing="0"><thead><tr><th>Question Id</th><th>Reference</th><th>Created By</th><th>Image</th><th>Image Status</th><th>Answer Image</th><th>Answer Image Status</th><th>Solution Image</th><th>Solution Image Status</th></tr></thead><tbody>';
        $questions=Questions::model()->with('answers')->findAll(array('condition'=>"TM_QN_Image!='' OR TM_AR_Image!='' OR TM_QN_Solution_Image!=''"));
        $questionimage='';
        $questionimagestatus='';
        $solutionimage='';
        $solutionimagestatus='';
        $questionnotfound=0;
        $answernotfound=0;
        $solutionfound=0;
        foreach($questions AS $key=>$question):

            if($question->TM_QN_Image!=''):
                $questionimage='Yes';
                if(file_exists('images/questionimages/'.$question->TM_QN_Image)):
                    $questionimagestatus='Found in server';
                else:
                    $questionimagestatus='Not Found in server';
                    $questionnotfound++;
                endif;
            else:
                $questionimagestatus='';
                $questionimage='';
            endif;
            if($question->TM_QN_Solution_Image!=''):
                $solutionimage='Yes';
                if(file_exists('images/questionimages/'.$question->TM_QN_Image)):
                    $solutionimagestatus='Found in server';
                else:
                    $solutionimagestatus='Not Found in server';
                    $solutionfound++;
                endif;
            else:
                $solutionimagestatus='';
                $solutionimage='';
            endif;
            $answerimage='';
            $answerimagestatus='';
            foreach($question->answers AS $answer):
                if($answer->TM_AR_Image!=''):
                    $answerimage='Yes';
                    if(file_exists('images/answerimages/'.$answer->TM_AR_Image)):
                        $answerimagestatus='Found in server';
                    else:
                        $answerimagestatus='Not Found in server';
                        $answernotfound++;
                    endif;
                endif;
            endforeach;
                $return.='<tr>
                            <td>'.$question->TM_QN_Id.'</td>
                            <td>'.$question->TM_QN_QuestionReff.'</td>
                            <td>'.Questions::model()->GetUser($question->TM_QN_CreatedBy).'</td>
                            <td>'.$questionimage.'</td>
                            <td '.($questionimagestatus=='Not Found in server'?'style="background-color:red"':'').'>'.$questionimagestatus.'</td>
                            <td>'.$answerimage.'</td>
                            <td '.($answerimagestatus=='Not Found in server'?'style="background-color:red"':'').'>'.$answerimagestatus.'</td>
                            <td>'.$solutionimage.'</td>
                            <td '.($solutionimagestatus=='Not Found in server'?'style="background-color:red"':'').'>'.$solutionimagestatus.'</td>
                        </tr>';
        endforeach;
        $count=$key+1;
        $return.='</tbody></table>';
        $return.='Total Questions ='.$count.'; Question images not found : '.$questionnotfound.'; Answer images not found : '.$answernotfound.'; Solution Images Not found : '.$solutionfound;
        echo $return;

    }
    public function actionChangestatus()
    {
        if(isset($_POST['item'])):
            if($_POST['item']=='all'):
                $notifications=Notifications::model()->findAll(array('condition'=>'TM_NT_Status=0 AND TM_NT_User='.Yii::app()->user->id));
                foreach($notifications AS $notification):
                    $notification->TM_NT_Status='1';
                    $notification->save(false);
                endforeach;
            else:
                $notification=Notifications::model()->findByPk($_POST['item']);
                $notification->TM_NT_Status='2';
                $notification->save(false);
            endif;
        endif;
    }
    public function actiongetNotifications()
    {
        $notifications=Notifications::model()->findAll(array('condition'=>'TM_NT_User=:user AND TM_NT_Status!=2','params'=>array(':user'=>Yii::app()->user->id),'limit'=>10,'offset'=>$_POST['offset'],'order'=>'TM_NT_Status ASC,TM_NT_Id DESC'));
        $returnval=array();
        $html='';
        $lastid='';
        if(count($notifications)>0):
            foreach($notifications AS $item):
                switch($item->TM_NT_Type){
                    case "Level":
                        $level=Levels::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="">You have earned <span class="label label-warning" class="readnotification" data-id="'.$item->TM_NT_Id.'">'.$level->TM_LV_Name.'</span> Medal</a></li>';
                        break;
                    case "Approve":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('buddies/Buddylist').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.'</span> has approved your buddy request</a></li>';
                        break;
                    case "Request":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('buddies/Buddylist',array('type'=>'pending')).'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.'</span> has sent you a buddy request</a></li>';
                        break;
                    case "Challenge":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('home').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.'</span> has sent you a challenge request</a></li>';
                        break;
                }
                $lastid=$item->TM_NT_Id;
            endforeach;
        else:
            $html.='<li class="nomore"><a href="javascript:void(0)" >No more notifications found</a></li>';
            $lastid='none';
        endif;
        $returnval['html']=$html;
        $returnval['lastitem']=$lastid;
        echo json_encode($returnval);
    }
    public function actiongetBuddies()
    {
        
        if(isset($_POST['query'])):
            $criteria=new CDbCriteria;
            $criteria->condition='TM_BD_Connection_Id='.Yii::app()->user->id.' AND TM_BD_Status=0';
            $users=Buddies::model()->findAll($criteria);
            $usersjson='[';
            foreach($users AS $key=>$student):
                $studentplan=StudentPlan::model()->count(array('condition'=>'TM_SPN_PlanId=:plan AND TM_SPN_StudentId=:student','params'=>array(':plan'=>Yii::app()->session['plan'],':student'=>$student->TM_BD_Student_Id)));
                $studentchallenge=Chalangerecepient::model()->count(array('condition'=>'TM_CE_RE_Chalange_Id=:challenge AND TM_CE_RE_Recepient_Id=:recepient','params'=>array(':challenge'=>$_POST['id'],':recepient'=>$student->TM_BD_Student_Id)));
                if($studentplan>0 & $studentchallenge==0):
                    $user=User::model()->findByPk($student->TM_BD_Student_Id);
                    $usersjson.=($usersjson=='['?'{"id":'.$user->id.', "name":"'.$user->username.' ('.$user->profile->firstname.' '.$user->profile->lastname.')"}':',{"id":'.$user->id.', "name":"'.$user->username.' ('.$user->profile->firstname.' '.$user->profile->lastname.')"}');
                endif;
            endforeach;
            $usersjson.=']';
            echo $usersjson;        
        endif;
    }
    public function actiongetPlanBuddies()
    {
        
        if(isset($_POST['id'])):
            $criteria=new CDbCriteria;
            $criteria->condition='TM_BD_Connection_Id='.Yii::app()->user->id.' AND TM_BD_Status=0';
            $users=Buddies::model()->findAll($criteria);
            $usersjson='0';
            foreach($users AS $key=>$student):
                $studentplan=StudentPlan::model()->count(array('condition'=>'TM_SPN_PlanId=:plan AND TM_SPN_StudentId=:student','params'=>array(':plan'=>Yii::app()->session['plan'],':student'=>$student->TM_BD_Student_Id)));                
                if($studentplan>0):
                    $usersjson++;                    
                endif;
            endforeach;            
            echo $usersjson;        
        endif;
    }
	public function actionSetOrder()
	{
		$chapters=Chapter::model()->findAll(array('order'=>'TM_TP_Id ASC'));
		$count=1;
		foreach($chapters AS $key=>$chapter):
			$chapter->TM_TP_order=$count;
			$chapter->save(false);
			$topics=$chapter->topic;
			$newcount=1;
			foreach($topics AS $topic):
				/*$newtopic=Topic::model()->findByPk($topic->$TM_SN_Id);
				$newtopic->TM_SN_order=$newcount;
				$newtopic->save(false);*/
				$topic->TM_SN_order=$newcount;
				$topic->save(false);
				$newcount++;
			endforeach;
			$count++;
		endforeach;
		
		
	}   
    public function actionDeleteVoucher()
    {
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'DATE(TM_CP_enddate) <DATE(NOW())';
        $coupons = Coupons::model()->findAll($criteria);
        //echo count($coupons);exit;
        foreach($coupons AS $coupon):
            $coupon->TM_CP_Status=1;
            $coupon->save(false);
        endforeach;

    }
    public function actionSubscriptionvalidity()
    {
        $studentplan=StudentPlan::model()->findAll(array('condition'=>'DATE(TM_SPN_ExpieryDate) =DATE(NOW())'));
        foreach($studentplan AS $plan):
            $plan->TM_SPN_Status='1';
            if($plan->save(false)):
            $plancount=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$plan->TM_SPN_StudentId.' AND TM_SPN_Status=0'));
            if($plancount==0)
            {
                $user=User::model()->findByPk($plan->TM_SPN_StudentId);
                $user->status='0';
                $user->save(false);
            }                
            endif;
        endforeach;
    }
    public function actionGetStandardList()
    {        


        if(isset($_POST['id']))
        {
            $data=Standard::model()->with('plan')->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0' AND TM_PN_Visibility=0",
                array(':syllabus'=>(int) $_POST['id']));

            $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
            /*if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Chapter',true);
            endif;*/
            echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
            foreach($data as $value=>$name)
            {                
                if(Yii::app()->session['location']==0):
                    $plancount=Plan::model()->count(array('condition'=>'TM_PN_Standard_Id='.$value.' AND TM_PN_Publisher_Id='.$_POST['publisher'].' AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND (TM_PN_Currency_Id=1 OR TM_PN_Additional_Currency_Id=1)'));
                else:
                    $plancount=Plan::model()->count(array('condition'=>'TM_PN_Standard_Id='.$value.' AND TM_PN_Publisher_Id='.$_POST['publisher'].' AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND (TM_PN_Currency_Id!=1 OR TM_PN_Additional_Currency_Id NOT IN(0,1))'));
                endif;                
                if($plancount!=0):
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                endif;
            }
        }
    }
	public function actionGetPlanList()
    {
        if(isset($_POST['publisher']))
        {
            if(Yii::app()->session['location']=='0'):
                $condition="TM_PN_Publisher_Id=:publisher AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND TM_PN_Type=0 AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND (TM_PN_Currency_Id=1 OR TM_PN_Additional_Currency_Id=1)";
            else:
                $condition="TM_PN_Publisher_Id=:publisher AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND TM_PN_Type=0 AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND (TM_PN_Currency_Id!=1 OR TM_PN_Additional_Currency_Id NOT IN(0,1))";
            endif;             
            $data=Plan::model()->with('currency')->findAll(
                array(
                        'condition'=>$condition,
                        'params'=>array(':publisher'=>$_POST['publisher'],':syllabus'=>$_POST['syllabus'],':standard'=>$_POST['standard'])
                )
            );

            echo CHtml::tag('option',array('value'=>''),'Select Subscription',true);
            foreach($data as $key=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$name->TM_PN_Id),CHtml::encode($name->TM_PN_Name),true);
            }
        }
    }
	public function actionGetPlanDetails()
    {
        $returnarray=array();
        if($_POST['plan']!='')
        {            
            $plan=Plan::model()->with('currency')->with('additional')->findByPk($_POST['plan']);            
            if(Yii::app()->session['location']=='0' & $plan->TM_PN_Currency_Id=='1'):
                $returnarray['planid']=$_POST['plan'];
                $returnarray['planname']=$plan->TM_PN_Name;
                $returnarray['plandetails']='<div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5><strong>'.$plan->TM_PN_Name.'</strong> <br> <br> Validity : '.$plan->TM_PN_Duration.' Days </h5></span></div>
                                              <div class="col-md-6 pull-right"><input type="hidden" value="'.$plan->currency->TM_CR_Code.'" id="currencycode"><input type="hidden" value="'.$plan->currency->TM_CR_Id.'" id="currencyid">
                                            	<h5 class="text-right"><strong id="planrate">'.$plan->currency->TM_CR_Code.' '.$plan->TM_PN_Rate.'</strong></h5>
                                              </div>';
                if($_POST['voucher']!=''):
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Rate*$percentage;
						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                         $couponamount=$voucher->currency->TM_CR_Code.' '.$amount;
                         $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->currency->TM_CR_Code.' '.$voucher->TM_CP_value; 
                    endif;                
                    $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5>Voucher : '.$voucher->TM_CP_Code.' </h5></span></div>
                                              <div class="col-md-6 pull-right">
                                            	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                              </div></div>';
                    $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                else:
                    $schooldiscount=School::model()->findByPk($_POST['school']);                                      
                    if($schooldiscount->TM_SCL_Discount):
                        $returnarray['schoolcoupon']='yes';
                        if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                             $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                             $amount=$plan->TM_PN_Rate*$percentage;
    						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                             $couponamount=$plan->currency->TM_CR_Code.' '.$amount;
                             $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                        elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                            $returnarray['planrate']=$plan->TM_PN_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                            $couponamount=$plan->currency->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                        endif;                
                        $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
        	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                  <div class="col-md-6 pull-right">
                                                	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                  </div></div>';
                        $returnarray['plancurrency']=$plan->currency->TM_CR_Code;                    
                    else:                  
                        $returnarray['coupon']='no';
                        $returnarray['planrate']=$plan->TM_PN_Rate;
                        $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                    endif;
                endif;                                              
            elseif(Yii::app()->session['location']=='0' & $plan->TM_PN_Additional_Currency_Id=='1'):
                $returnarray['planid']=$_POST['plan'];   
				$returnarray['planname']=$plan->TM_PN_Name;				
                $returnarray['plandetails']='<div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5><strong>'.$plan->TM_PN_Name.'</strong> <br> <br> Validity : '.$plan->TM_PN_Duration.' Days </h5></span></div>
                                              <div class="col-md-6 pull-right"><input type="hidden" value="'.$plan->additional->TM_CR_Code.'" id="currencycode"><input type="hidden" value="'.$plan->additional->TM_CR_Id.'" id="currencyid">
                                            	<h5 class="text-right"><strong id="planrate">'.$plan->additional->TM_CR_Code.' '.$plan->TM_PN_Additional_Rate.'</strong></h5>
                                              </div>'; 
                if($_POST['voucher']!=''):
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Additional_Rate*$percentage;
						 $amount=round($amount,2,PHP_ROUND_HALF_UP);
                         $couponamount=$voucher->currency->TM_CR_Code.' '.$amount;
                         $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->currency->TM_CR_Code.' '.$voucher->TM_CP_value; 
                    endif;                
                    $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5>Voucher : '.$voucher->TM_CP_Code.' </h5></span></div>
                                              <div class="col-md-6 pull-right">
                                            	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                              </div></div>';
                    $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                else:
                    $schooldiscount=School::model()->findByPk($_POST['school']);                      
                    if($schooldiscount->TM_SCL_Discount):
                        $returnarray['schoolcoupon']='yes'; 
                        if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                             $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                             $amount=$plan->TM_PN_Additional_Rate*$percentage;
    						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                             $couponamount=$plan->additional->TM_CR_Code.' '.$amount;
                             $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                        elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                            $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                            $couponamount=$plan->additional->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                        endif;                
                        $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
        	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                  <div class="col-md-6 pull-right">
                                                	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                  </div></div>';
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;                    
                    else:                  
                        $returnarray['coupon']='no';
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate;
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                    endif;
                endif;
            elseif(Yii::app()->session['location']=='1' & $plan->TM_PN_Currency_Id!='1'):
                $returnarray['planid']=$_POST['plan'];  
				$returnarray['planname']=$plan->TM_PN_Name;
                $returnarray['plandetails']='<div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5><strong>'.$plan->TM_PN_Name.'</strong> <br> <br> Validity : '.$plan->TM_PN_Duration.' Days </h5></span></div>
                                              <div class="col-md-6 pull-right"><input type="hidden" value="'.$plan->currency->TM_CR_Code.'" id="currencycode"><input type="hidden" value="'.$plan->currency->TM_CR_Id.'" id="currencyid">
                                            	<h5 class="text-right"><strong id="planrate">'.$plan->currency->TM_CR_Code.' '.$plan->TM_PN_Rate.'</strong></h5>
                                              </div>';
                if($_POST['voucher']!=''):
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Rate*$percentage;
						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                         $couponamount=$voucher->currency->TM_CR_Code.' '.$amount;
                         $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->currency->TM_CR_Code.' '.$voucher->TM_CP_value; 
                    endif;                
                    $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5>Voucher : '.$voucher->TM_CP_Code.' </h5></span></div>
                                              <div class="col-md-6 pull-right">
                                            	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                              </div></div>';
                    $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                else:
                    $schooldiscount=School::model()->findByPk($_POST['school']);                      
                    if($schooldiscount->TM_SCL_Discount):
                        $returnarray['schoolcoupon']='yes';
                        if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                             $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                             $amount=$plan->TM_PN_Rate*$percentage;
    						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                             $couponamount=$plan->currency->TM_CR_Code.' '.$amount;
                             $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                        elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                            $returnarray['planrate']=$plan->TM_PN_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                            $couponamount=$plan->currency->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                        endif;                
                        $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
        	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                  <div class="col-md-6 pull-right">
                                                	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                  </div></div>';
                        $returnarray['plancurrency']=$plan->currency->TM_CR_Code;                    
                    else:                  
                        $returnarray['coupon']='no';
                        $returnarray['planrate']=$plan->TM_PN_Rate;
                        $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                    endif;
                endif;
            elseif(Yii::app()->session['location']=='1' & ($plan->TM_PN_Additional_Currency_Id!='1' || $plan->TM_PN_Additional_Currency_Id!='0')):
                $returnarray['planid']=$_POST['plan'];
				$returnarray['planname']=$plan->TM_PN_Name;
                $returnarray['plandetails']='<div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5><strong>'.$plan->TM_PN_Name.'</strong> <br> <br> Validity : '.$plan->TM_PN_Duration.' Days </h5></span></div>
                                              <div class="col-md-6 pull-right"><input type="hidden" value="'.$plan->additional->TM_CR_Code.'" id="currencycode"><input type="hidden" value="'.$plan->additional->TM_CR_Id.'" id="currencyid">
                                            	<h5 class="text-right"><strong id="planrate">'.$plan->additional->TM_CR_Code.' '.$plan->TM_PN_Additional_Rate.'</strong></h5>
                                              </div>'; 
                if($_POST['voucher']!=''):
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Additional_Rate*$percentage;
						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                         $couponamount=$voucher->currency->TM_CR_Code.' '.$amount;
                         $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->currency->TM_CR_Code.' '.$voucher->TM_CP_value; 
                    endif;                
                    $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
    	                                   <span class="text-left"><h5>Voucher : '.$voucher->TM_CP_Code.' </h5></span></div>
                                              <div class="col-md-6 pull-right">
                                            	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                              </div></div>';
                    $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                else:
                    $schooldiscount=School::model()->findByPk($_POST['school']);                    
                    if($schooldiscount->TM_SCL_Discount):
                        $returnarray['schoolcoupon']='yes'; 
                        if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                             $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                             $amount=$plan->TM_PN_Additional_Rate*$percentage;
    						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                             $couponamount=$plan->additional->TM_CR_Code.' '.$amount;
                             $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                        elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                            $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                            $couponamount=$plan->additional->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                        endif;                
                        $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
        	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                  <div class="col-md-6 pull-right">
                                                	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                  </div></div>';
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;                    
                    else:                  
                        $returnarray['coupon']='no';
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate;
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                    endif;
                endif;                                                                                                                                 
            endif;            
                       
            
            $returnarray['error']='no';
        }
        echo json_encode($returnarray);
        
    }    
    public function actionGetCouvheRrate()
    {
        if($_POST['plan']!='' & $_POST['voucher']!='')
        {            
            $returnarray=array();
            $plan=Plan::model()->findByPk($_POST['plan']);            
            $voucher=Coupons::model()->find(array('condition'=>"TM_CP_Status=0 AND TM_CP_Currency_Id='".$_POST['currency']."' AND TM_CP_Code='".$_POST['voucher']."'"));
            $currency=Currency::model()->findByPk($_POST['currency']);
            $couponamount=0;
            if(count($voucher)>0):                        
                if($voucher->TM_CP_Type=='1'):                              
                     $percentage=$voucher->TM_CP_value/100;
                     if($_POST['currency']==$plan->TM_PN_Currency_Id):                 
                        $amount=$plan->TM_PN_Rate*$percentage;
                        $amount=round($amount,2,PHP_ROUND_HALF_UP);						
                     else:
                        $amount=$plan->TM_PN_Additional_Rate*$percentage;
						$amount=round($amount,2,PHP_ROUND_HALF_UP);		
                     endif;   
                     $couponamount=$currency->TM_CR_Code.' '.$amount;
                     $rate=$plan->TM_PN_Rate-$amount;
                elseif($voucher->TM_CP_Type=='0'):   
                     if($_POST['currency']==$plan->TM_PN_Currency_Id):                                         
                        $rate=$plan->TM_PN_Rate-$voucher->TM_CP_value;
                     else:
                        $rate=$plan->TM_PN_Additional_Rate-$voucher->TM_CP_value;                        
                     endif;                             
                    
                    $couponamount=$currency->TM_CR_Code.' '.$voucher->TM_CP_value; 
                endif;            
                $returnarray['voucher']=$voucher->TM_CP_Id;
                $returnarray['error']='no';
                $returnarray['message']='<div class="alert alert-success" role="alert">'.$couponamount.' Has been redeemed</div>';
            else:            
                $returnarray['error']='<div class="alert alert-danger" role="alert">Not a valid voucher</div>'; 
            endif; 
            echo json_encode($returnarray);
        }
        
    } 
    public function actionSetMockorder($id)
    {
            $modelold=MockQuestions::model()->findAll(array(
                        'condition' => 'TM_MQ_Mock_Id=:id',                        
                        'order'=>'TM_MQ_Id ASC',
                        'params' => array(':id' => $id)
                    )
                );
            $key=1;                
            foreach($modelold AS $mock):
                $question=Questions::model()->findByPk($mock->TM_MQ_Question_Id);
                $mock->TM_MQ_Order=$key;
                $mock->TM_MQ_Type=$question->TM_QN_Type_Id;
                $mock->save(false);
                $key++;
            endforeach;
    }       
  	public function actionActivateTrial()
    {
        $questions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=0','limit'=>5000,'offset'=>20000,'order'=>'TM_QN_Id ASC'));
        foreach($questions AS $question):
            $exam=new QuestionExams();
            $exam->TM_QE_Question_Id=$question->TM_QN_Id;
            $exam->TM_QE_Exam_Id='5';
            $exam->save(false);
        endforeach;
    }
	public function actionSetTotalaverage()
    {
        $plans=Plan::model()->findAll(
            array(
                    'condition'=>"TM_PN_Status='0'",                    
            )
        );
        if(count($plans)>0):
            Settotal::model()->deleteAll();
            foreach($plans AS $plan):
                if($plan->TM_PN_Type=='0'):
                    $connection = CActiveRecord::getDbConnection();
                    $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND A.TM_QN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE  TM_CEUQ_Parent_Id=0 )";                 
                    $command=$connection->createCommand($sql);
                    $dataReader=$command->query();
                    $restattemptedcount=$dataReader->read();
                    $restattemptedcount=$restattemptedcount['question'];        
                    $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND A.TM_QN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 )";               
                    $command=$connection->createCommand($sql);
                    $dataReader=$command->query();
                    $restcorrectcount=$dataReader->read();
                    $restcorrectcount=$restcorrectcount['question'];  
                    $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Syllabus_Id='.$plan->TM_PN_Syllabus_Id.' AND TM_QN_Standard_Id='.$plan->TM_PN_Standard_Id.' AND TM_QN_Parent_Id=0 '));
                    $newtotal=new Settotal();
                    $newtotal->TM_TOT_Plan_Id=$plan->TM_PN_Id;
                    $newtotal->TM_TOT_Total_Attempted=$restattemptedcount;
                    $newtotal->TM_TOT_Total_Correct=$restcorrectcount;
                    $newtotal->TM_TOT_Total_Questions=$totalquestions;
                    $newtotal->save(false); 
                else:
                    $connection = CActiveRecord::getDbConnection();
                    $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND A.TM_QN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN (SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Status!=0 )";                 
                    $command=$connection->createCommand($sql);
                    $dataReader=$command->query();
                    $restattemptedcount=$dataReader->read();
                    $restattemptedcount=$restattemptedcount['question']; 
                    $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND A.TM_QN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN (SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Status!=0 AND TM_STHWQT_Marks!=0)";               
                    $command=$connection->createCommand($sql);
                    $dataReader=$command->query();
                    $restcorrectcount=$dataReader->read();
                    $restcorrectcount=$restcorrectcount['question'];
                    $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Syllabus_Id='.$plan->TM_PN_Syllabus_Id.' AND TM_QN_Standard_Id='.$plan->TM_PN_Standard_Id.' AND TM_QN_Parent_Id=0 '));
                    $newtotal=new Settotal();
                    $newtotal->TM_TOT_Plan_Id=$plan->TM_PN_Id;
                    $newtotal->TM_TOT_Total_Attempted=$restattemptedcount;
                    $newtotal->TM_TOT_Total_Correct=$restcorrectcount;
                    $newtotal->TM_TOT_Total_Questions=$totalquestions;
                    $newtotal->save(false);                                                            
                endif;                                                                                                                                        
            endforeach; 
        endif;       
    }	
    /*public function actionSetTotalaverage()
    {
        $plans=Plan::model()->findAll(
            array(
                    'condition'=>"TM_PN_Status='0'",                    
            )
        );
        if(count($plans)>0):
            Settotal::model()->deleteAll();
            foreach($plans AS $plan):
                $connection = CActiveRecord::getDbConnection();
                $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND A.TM_QN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE  TM_CEUQ_Parent_Id=0 )";                 
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $restattemptedcount=$dataReader->read();
                $restattemptedcount=$restattemptedcount['question'];        
                $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND A.TM_QN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 )";               
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $restcorrectcount=$dataReader->read();
                $restcorrectcount=$restcorrectcount['question'];  
                $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Syllabus_Id='.$plan->TM_PN_Syllabus_Id.' AND TM_QN_Standard_Id='.$plan->TM_PN_Standard_Id.' AND TM_QN_Parent_Id=0 '));
                $newtotal=new Settotal();
                $newtotal->TM_TOT_Plan_Id=$plan->TM_PN_Id;
                $newtotal->TM_TOT_Total_Attempted=$restattemptedcount;
                $newtotal->TM_TOT_Total_Correct=$restcorrectcount;
                $newtotal->TM_TOT_Total_Questions=$totalquestions;
                $newtotal->save(false);                                                                                                                                         
            endforeach; 
        endif;       
    }*/
	public function actionReplaceAnswerWiris()
	{
            $answers=Answers::model()->findAll(
                array(
                        'condition'=>"TM_AR_Answer LIKE '%?mspace linebreak=&#168;newline&#168;/?%'",
                        'limit'=>10000
                )
            );
			echo count($answers)."<br>";
			foreach($answers AS $answer):
				echo $answer->TM_AR_Id." - ".$answer->TM_AR_Question_Id."<br>";
				$answer->TM_AR_Answer=str_replace("?mspace linebreak=&#168;newline&#168;/?","«mspace linebreak=¨newline¨/»",$answer->TM_AR_Answer);
				$answer->save(false);
			endforeach;	
	}
	public function actionReplaceQuestionWiris()
	{
            $questions=Questions::model()->findAll(
                array(
                        'condition'=>"TM_QN_Question LIKE '%?mspace linebreak=&#168;newline&#168;/?%'",
                        'limit'=>10000
                )
            );
			echo count($questions)."<br>";			
			foreach($questions AS $question):
				echo $question->TM_QN_Id."<br>";
				$question->TM_QN_Question=str_replace("?mspace linebreak=&#168;newline&#168;/?","«mspace linebreak=¨newline¨/»",$question->TM_QN_Question);
				$question->save(false);
			endforeach;	
	}
	public function actionReplaceSolutionWiris()
	{
            $questions=Questions::model()->findAll(
                array(
                        'condition'=>"TM_QN_Solutions LIKE '%?mspace linebreak=&#168;newline&#168;/?%'",
                        'limit'=>10000
                )
            );
			echo count($questions)."<br>";			
			foreach($questions AS $question):
				echo $question->TM_QN_Id."<br>";
				$question->TM_QN_Solutions=str_replace("?mspace linebreak=&#168;newline&#168;/?","«mspace linebreak=¨newline¨/»",$question->TM_QN_Solutions);
				$question->save(false);
			endforeach;	
	}
  	public function actionAddExams()
    {
        $questions=Questions::model()->findAll(array('condition'=>'TM_QN_Parent_Id=0 AND TM_QN_Type_Id NOT IN (6,7)','limit'=>5000,'offset'=>0,'order'=>'TM_QN_Id ASC'));
        foreach($questions AS $question):
            $exam=new QuestionExams();
            $exam->TM_QE_Question_Id=$question->TM_QN_Id;
            $exam->TM_QE_Exam_Id='3';
            $exam->save(false);                     
        endforeach;
    }  
    public function actionGetSyllabusList() {
        if(isset($_POST['id'])) {

            $data = Plan::model()->findAllByAttributes(array('TM_PN_Publisher_Id' => $_POST['id']));
            $assignedstandards = '';
            foreach ($data AS $key => $plan):
                $assignedstandards .= ($key == 0 ? '' : ',') . $plan->TM_PN_Syllabus_Id;
            endforeach;
            if($assignedstandards!=''):
                $total = Syllabus::model()->findAll(array('condition' => "TM_SB_Status='0'  AND TM_SB_Id IN(" . $assignedstandards . ")"));
                $data = CHtml::listData($total, 'TM_SB_Id', 'TM_SB_Name');
                echo CHtml::tag('option', array('value' => ''), 'Select Syllabus', true);
                foreach ($data as $value => $name) {
                    echo CHtml::tag('option',
                        array('value' => $value), CHtml::encode($name), true);
                }
            else:
                echo CHtml::tag('option', array('value' => ''), 'Select Syllabus', true);
                endif;

        }

    }
    public function actionChangehomeworkstatus()
    {
        $date=date('Y-m-d');
        $studenthws=StudentHomeworks::model()->findAll(array('condition'=>"TM_STHW_PublishDate='$date' AND TM_STHW_Status='7'"));
		//$studenthws=StudentHomeworks::model()->findAll(array('condition'=>"TM_STHW_DueDate >'$date' AND  TM_STHW_PublishDate<'$date' AND TM_STHW_Status='7'"));
		
        foreach($studenthws AS $key=>$studenthw):			
            //$studhw=StudentHomeworks::model()->findByAttributes(array('TM_STHW_HomeWork_Id'=> $studenthw['TM_STHW_HomeWork_Id']));
            $studenthw->TM_STHW_Status='0';
            $studenthw->save(false);
            $notification=new Notifications();
            $notification->TM_NT_User=$studenthw->TM_STHW_Student_Id;
            $notification->TM_NT_Type='HWAssign';
            $notification->TM_NT_Item_Id=$studenthw->TM_STHW_HomeWork_Id;
            $notification->TM_NT_Target_Id=$studenthw->TM_STHW_Assigned_By;
            $notification->TM_NT_Status='0';
            $notification->save(false);
        endforeach;
    } 
    public function actionTest()
    {
        $id='130';
        $password='test';
        echo "test";
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$id));
        $user=User::model()->findByPk($teacher->TM_TH_User_Id);
        $userprofile=Profile::model()->findByPk($teacher->TM_TH_User_Id);
        $school=School::model()->findByPk($teacher->TM_TH_SchoolId);
        $adminEmail = Yii::app()->params['noreplayEmail'];
        $email=$user->email;  
        $message="<p>Dear $userprofile->firstname,</p>
                    <p>You have been added as teacher to school: $school->TM_SCL_Name. Your login details are as below.</p>                    
                    <p><div style='border:solid 1px black;background-color: #E6E6FA;width:450px;height: 150px;padding: 20px;text-align: center;'>
                        <p><b>Username </b>: ".$user->username."</p>
                        <p><b>Password </b>: ".$password."</p>
                    </div></p>
                    <p>Once you login with these details, you can go into settings and change password at any time.</p>
                    <p>Regards,<br>TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($email);
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Teacher Registration ';
        $mail->Body = $message;

        $mail->Send();              
    } 
    public function actionSendteachermail()
    {
        $id='130';
        $password='test';
        echo "test";
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$id));
        $user=User::model()->findByPk($teacher->TM_TH_User_Id);
        $userprofile=Profile::model()->findByPk($teacher->TM_TH_User_Id);
        $school=School::model()->findByPk($teacher->TM_TH_SchoolId);
        $adminEmail = Yii::app()->params['noreplayEmail'];
        $email=$user->email;
        $message="<p>Dear $userprofile->firstname,</p>
                    <p>You have been added as teacher to school: $school->TM_SCL_Name. Your login details are as below.</p>                    
                    <p><div style='border:solid 1px black;background-color: #E6E6FA;width:450px;height: 150px;padding: 20px;text-align: center;'>
                        <p><b>Username </b>: ".$user->username."</p>
                        <p><b>Password </b>: ".$password."</p>
                    </div></p>
                    <p>Once you login with these details, you can go into settings and change password at any time.</p>
                    <p>Regards,<br>TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($email);
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Teacher Registration ';
        $mail->Body = $message;

        return $mail->Send();
    }    
   public function actionWorksheetorder()
    {
        $mocks=Mock::model()->findAll(array('condition'=>'TM_MK_Standard_Id=13','order'=>'TM_MK_Id ASC'));
		$count=1;
        foreach($mocks AS $key=>$mock):			
			$mock->TM_MK_Sort_Order=$count;
			$mock->save(false);
			$count++;
        endforeach;
    }	
	public function actionTestorder()
	{
		$chpaters=Chapter::model()->findAll(array('condition'=>'TM_TP_Syllabus_Id=4 AND TM_TP_Standard_Id=15','order'=>'TM_TP_order ASC'));
		foreach($chpaters AS $key=>$chpater):
			$order=$key+1;
			$chpater->TM_TP_order=$order;
			$chpater->save(false);
		endforeach;
	}
    public function actionStudentrevision()
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT * FROM `tm_student_test` GROUP BY TM_STU_TT_Plan";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $revisions=$dataReader->readAll();
        //echo count($revisions);
        foreach($revisions AS $key=>$revision):
            $plantype=Plan::model()->findByPk($revision['TM_STU_TT_Plan'])->TM_PN_Type;
            //if($plantype==0):echo ($key+1)." :".$revision['TM_STU_TT_Plan']."Revision <br>";else:echo ($key+1)." :".$revision['TM_STU_TT_Plan']."School plan <br>";endif;
        endforeach;

    }
     /*public function actionChangehomeworkstatus()
    {
        $date=date('Y-m-d');
        $studenthws=StudentHomeworks::model()->findAll(array('condition'=>"TM_STHW_PublishDate='$date'"));
        foreach($studenthws AS $studenthw):
            $studhw=StudentHomeworks::model()->findByAttributes(array('TM_STHW_HomeWork_Id'=> $studenthw['TM_STHW_HomeWork_Id']));
            $studhw->TM_STHW_Status='0';
            $studhw->save(false);
            $notification=Notifications::model()->findByAttributes(array('TM_NT_Item_Id'=> $studenthw['TM_STHW_HomeWork_Id']));
            $notification->TM_NT_Status='0';
            $notification->save(false);
        endforeach;
    } */         
}
