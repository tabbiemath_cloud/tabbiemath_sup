<?php

class ParentController extends Controller
{
    public $layout='//layouts/main';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function accessRules()
    {

        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Home','update','Studentupdate','Removestudent','GetStudentForm','GetPlanForm','GetStandardList','GetPlanList','addsubscription','addstudent','cancelsubscription','ChangePassword','GetPlanAmount','PaymentComplete','studenthistory','StudentActivity','GetSolution','Studentprogress','GetChangePassword','uploadImage','uploadImageSave','uploadChildImage','uploadImageSaveChild','GetChallengeBuddies','Checkactiveplan','GetChapterstatus','GetTopicStatus','GetRestStatus','GetMyStatus','Paymentcompletepaypal','Paymentcompletestripe','Studentschoolprogress','Getschoolchapterstatus','Getschooltopicstatus','Getschoolreststatus'),
                'users'=>array('@'),
                'expression'=>'UserModule::isParent()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            )
        );
    }

    public function actionStudenthistory($id)
    {

        $student=User::model()->findByPk($id);
        $plan=Plan::model()->findByPk($_GET['plan']);
        $planmodules=$this->PlanModules($_GET['plan']);
        if($planmodules['revision']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_TT_Student_Id='.$id.' AND TM_STU_TT_Status IN (3,4) AND TM_STU_TT_Plan='.$_GET['plan']);
            $totaltests = StudentTest::model()->findAll($criteria);
        endif;
        if($planmodules['challenge']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Recepient_Id='.$id.' AND TM_CE_RE_Status=3 AND TM_CL_Plan='.$_GET['plan']);
            $challenges = Chalangerecepient::model()->with('challenge')->findAll($criteria);
        endif;
        if($planmodules['mock']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMK_Student_Id='.$id.' AND TM_STMK_Status IN (3,4) AND TM_MK_Status=0 AND  TM_STMK_Plan_Id='.$_GET['plan']);
            $totalmocks = StudentMocks::model()->with('mocks')->findAll($criteria);
        endif;
        $this->render('viewhistory',array('challenges'=>$challenges,'totaltests'=>$totaltests,'totalmocks'=>$totalmocks,'student'=>$student,'plan'=>$plan,'planmodules'=>$planmodules));
    }
    public function GetChallenger($id)
    {
        $user=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$id));
        return $user->TM_STU_First_Name.' '.$user->TM_STU_Last_Name;
    }
    public function actionStudentActivity($id)
    {
        $student=User::model()->findByPk($id);
        $plan=Plan::model()->findByPk($_GET['plan']);
        $planmodules=$this->PlanModules($_GET['plan']);
        if($planmodules['revision']):
            $studenttests=StudentTest::model()->findAll(array('condition'=>"TM_STU_TT_Student_Id='".$id."' AND TM_STU_TT_Status NOT IN (3,4) AND TM_STU_TT_Plan='".$_GET['plan']."'"));
        endif;
        if($planmodules['mock']):
            $studentmocks=StudentMocks::model()->with('mocks')->findAll(array('condition'=>"TM_STMK_Student_Id='".$id."' AND TM_STMK_Status NOT IN (3,4) AND TM_MK_Status='0' AND  TM_STMK_Plan_Id='".$_GET['plan']."'"));
        endif;
        if($planmodules['challenge']):
            $studentshallenges=Chalangerecepient::model()->with('challenge')->findAll(array('condition'=>"TM_CE_RE_Recepient_Id='".$id."' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0' AND TM_CL_Plan='".$_GET['plan']."'"));
        endif;
        $this->render('studentactivity',array('studenttests'=>$studenttests,'challenges'=>$studentshallenges,'studentmocks'=>$studentmocks,'student'=>$student,'plan'=>$plan,'planmodules'=>$planmodules));
    }
    public function actionStudentprogress($id)
    {        
        $student=User::model()->findByPk($id);
        $plan=Plan::model()->findByPk($_GET['plan']);                   
        $week=$this->GetWeek(date('Y-m-d'));
        $planmodules=$this->PlanModules($_GET['plan']);                              
        if($planmodules['challenge']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Recepient_Id=' . $id . ' AND TM_CE_RE_Status=3 AND TM_CL_Plan=' . $_GET['plan']);            
            $challengetotal=Chalangerecepient::model()->with('challenge')->count($criteria);
            $criteria->addCondition("TM_CL_Created_On BETWEEN '".$week[0]."' AND '".$week[0]."'");                        
            $challengeweekstatus = Chalangerecepient::model()->with('challenge')->count($criteria);
        endif;
        if($planmodules['revision']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_TT_Student_Id=' . $id . ' AND TM_STU_TT_Status IN(3,4) AND TM_STU_TT_Plan=' . $_GET['plan']);                        
            $revisiontotal = StudentTest::model()->count($criteria);     
            $criteria->addCondition("TM_STU_TT_CreateOn BETWEEN '".$week[0]."' AND '".$week[0]."'");
            $revisionweekstatus = StudentTest::model()->count($criteria);      
        endif;
        if($planmodules['mock']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMK_Student_Id=' . $id . ' AND TM_STMK_Status IN (3,4) AND TM_MK_Status=0 AND  TM_STMK_Plan_Id=' . $_GET['plan']);            
            $mocktotal = StudentMocks::model()->with('mocks')->count($criteria);
            $criteria->addCondition("TM_STMK_Date BETWEEN '".$week[0]."' AND '".$week[0]."'");
            $mockweekstatus = StudentMocks::model()->with('mocks')->count($criteria);            
        endif;
            $dates = $this->GetWeek(date('Y-m-d'));
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Standard_Id=:standard AND TM_STP_Student_Id=" . $id;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => $plan->TM_PN_Standard_Id);
            $totalpoints = StudentPoints::model()->find($criteria); 
			
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '" . $dates[0] . "' AND '" . $dates[1] . "' AND TM_STP_Student_Id=" . $id;
            $criteria->order = 'totalpoints DESC';            
            $criteria->params = array(':standard' => $plan->TM_PN_Standard_Id);
            $weeklypoints = StudentPoints::model()->find($criteria);         
        $lastvisit=$student->lastvisit;
        if(!$lastvisit):
            $lastlogin='Never logged in';
        else:
            $lastlogin=date("jS F Y ",$lastvisit);
        endif;                                           
        $this->render('studentprogress',array(
        'challengetotal'=>$challengetotal,
        'challengeweekstatus'=>$challengeweekstatus,
        'revisiontotal'=>$revisiontotal,
        'revisionweekstatus'=>$revisionweekstatus,
        'mocktotal'=>$mocktotal,
        'mockweekstatus'=>$mockweekstatus,
        'lastlogin'=>$lastlogin,        
        'student'=>$student,
        'planmodules'=>$planmodules,
        'plan'=>$plan,   
        'weeklypoints'=>$weeklypoints, 
        'totalpoints'=>$totalpoints                                                     
        ));                 
    }
    public function actionGetChapterstatus()
    {
        $plan=Plan::model()->findByPk($_POST['plan']);
        $connection = CActiveRecord::getDbConnection();                       
        $sql="SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$_POST['student']."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$_POST['student']."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$_POST['student']."' AND TM_CEUQ_Parent_Id=0";        
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attempted=$dataReader->readAll();
        $attemptedquestions='';
        foreach($attempted AS $key=>$attemp):
        $attemptedquestions.=($key==0?'':',').$attemp['question'];
        endforeach;         
        $sql1=" SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$_POST['student']."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$_POST['student']."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$_POST['student']."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 ";        
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        $correctones=$dataReader->readAll();        
        $correctquestions='';
        foreach($correctones AS $key=>$correct):
        $correctquestions.=($key==0?'':',').$correct['question'];
        endforeach; 
        $sql="SELECT count(a.TM_QN_Id) AS attemptedcount, a.TM_QN_Topic_Id,b.TM_TP_Name,
        (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Topic_Id=a.TM_QN_Topic_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$correctquestions." )) AS correctcount
                FROM 
                tm_question AS a,tm_topic AS b WHERE 
                a.TM_QN_Parent_Id=0 AND a.TM_QN_Topic_Id=b.TM_TP_Id AND b.TM_TP_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND b.TM_TP_Standard_Id='".$plan->TM_PN_Standard_Id."' AND TM_QN_Id IN (".$attemptedquestions.") group by TM_QN_Topic_Id";                                                         
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $chapters=$dataReader->readAll();                                              
        $this->renderPartial('_overallstatus', array('chapters' => $chapters, 'plan' => $plan,'correctquestions'=>$correctquestions,'attemptedquestions'=>$attemptedquestions,'student'=>$_POST['student'])); 
     }
    public function actionGetTopicStatus()
    {
        $topicstatus=$this->GetTopicStatus($_POST['student']);
        $this->renderPartial('_topicstatus', array('topicstatus' => $topicstatus));         
    }
    public function actionGetRestStatus()
    {
        $progressstatus=$this->GetCompleteStatus($_POST['student'],$_POST['plan']);
        echo json_encode($progressstatus); 
    } 
    public function actionGetMyStatus()
    {
        $progressstatus=$this->GetMyCompleteStatus($_POST['student'],$_POST['plan']);
        echo json_encode($progressstatus);        
    }                   
    public function GetTopicStatus($user)
    {
        $plan=Plan::model()->findByPk($_POST['plan']);
        $connection = CActiveRecord::getDbConnection();
        $sql1=" SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 ";        
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        $correctones=$dataReader->readAll();        
        $correctquestions='';
        foreach($correctones AS $key=>$correct):
        $correctquestions.=($key==0?'':',').$correct['question'];
        endforeach; 
        $sql="SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0";        
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attempted=$dataReader->readAll();
        $attemptedquestions='';
        foreach($attempted AS $key=>$attemp):
        $attemptedquestions.=($key==0?'':',').$attemp['question'];
        endforeach;        
         $sql="SELECT count(DISTINCT(a.TM_QN_Id)) AS attempted, b.TM_SN_Id,b.TM_SN_Name,b.TM_SN_Topic_Id,
        (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Section_Id=a.TM_QN_Section_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$attemptedquestions." )) AS completed,
                (
                    SELECT COUNT(DISTINCT(TM_QN_Id)) AS question 
            		FROM tm_question AS quest 
            		WHERE quest.TM_QN_Section_Id=b.TM_SN_Id 
            		AND quest.TM_QN_Parent_Id=0 
                ) AS total        
                FROM 
                tm_question AS a,tm_section AS b WHERE 
                a.TM_QN_Parent_Id=0 AND a.TM_QN_Section_Id=b.TM_SN_Id AND b.TM_SN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND b.TM_SN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND TM_QN_Id IN (".$correctquestions.") GROUP BY TM_QN_Section_Id HAVING completed > 0 AND attempted < completed ORDER BY attempted DESC,TM_SN_Id ASC LIMIT 5 ";
                                                              
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $topicstatus=$dataReader->readAll();
        //print_r($topicstatus); exit;                        
        return $topicstatus;                      
    }      
    public function GetWeek($week)
    {
        $ts = strtotime($week);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday midnight', $ts);
        $previous_week = strtotime("-1 week +1 day", $ts);
        $previous_week = strtotime("last sunday midnight", $previous_week);
        $next_week = strtotime("+1 week +1 day", $ts);
        $next_week = strtotime("last sunday midnight", $next_week);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next saturday', $start)), date('F Y', $start), date('Y-m-d', $previous_week), date('Y-m-d', $next_week));
    }    
    public function ClosestNumber($array, $number,$chapterarray,$chapter)
    {
    	
        $returnarray=array();
        $highest= max($array);
    	if($highest>$number):
    			$key = array_search($highest, $array);
    			$array[$key] = $number;
                $chapterarray[$key]=$chapter;
                $returnarray[]=$array;
                $returnarray[]=$chapterarray;                
    			return $returnarray;				
    	else:
    		return false;	
    	endif;
    }
    public function GetChapterCount($chapter,$user,$attemptedcount,$correctcount)
    {
        $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Topic_Id='.$chapter.' AND TM_QN_Parent_Id=0 AND TM_QN_Status=0'));              
        if($attemptedcount>0):
            $average=($attemptedcount/$totalquestions)*100;
            if($correctcount>0):
                $correct=round(($correctcount/$attemptedcount)*100);
            else:
                $correct=0;
            endif;
            $balance=round($average)-$correct;
        else:
            $average=0;
            $correct=0;
        endif;

        return array('attempted'=>round($average),'correct'=>$correct,'average'=>round($average));
    } 
    public function GetCompleteStatus($user,$planid)
    {        
        $plan=Plan::model()->findByPk($planid);      
        $syllabus=$plan->TM_PN_Syllabus_Id;
        $standard=$plan->TM_PN_Standard_Id;        
        $restresult=Settotal::model()->find(array('condition'=>'TM_TOT_Plan_Id='.$plan->TM_PN_Id));
        if($restresult->TM_TOT_Total_Attempted>0):
            $restaverage=($restresult->TM_TOT_Total_Attempted/$restresult->TM_TOT_Total_Questions)*100;
            if($restresult->TM_TOT_Total_Correct>0):
                $restcorrect=round(($restresult->TM_TOT_Total_Correct/$restresult->TM_TOT_Total_Attempted)*100);
            else:
                $restcorrect=0;
            endif;
            //$balance=round($restaverage)-$correct;
        else:
            $restaverage=0;
            $restcorrect=0;
        endif;
        return array('restaverage'=>round($restaverage),'restcorrect'=>$restcorrect);                
    } 
    public function GetMyCompleteStatus($user,$planid)
    {
        $plan=Plan::model()->findByPk($planid);                 
        $syllabus=$plan->TM_PN_Syllabus_Id;
        $standard=$plan->TM_PN_Standard_Id;        
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 )";        
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attemptedcount=$dataReader->read();
        $attemptedcount=$attemptedcount['question'];        
        $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 )";               
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $correctcount=$dataReader->read();
        $correctcount=$correctcount['question'];  
        $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Syllabus_Id='.$syllabus.' AND TM_QN_Standard_Id='.$standard.' AND TM_QN_Parent_Id=0 AND TM_QN_Status=0'));              
        if($attemptedcount>0):
            $average=($attemptedcount/$totalquestions)*100;
            if($correctcount>0):
                $correct=round(($correctcount/$attemptedcount)*100);
            else:
                $correct=0;
            endif;
            $balance=round($average)-$correct;
        else:
            $average=0;
            $correct=0;
        endif;

        return array('average'=>round($average),'correct'=>$correct);
    }        
    public function actionHome()
	{

        $parent=User::model()->findByPk(Yii::app()->user->id);
        $students=Student::model()->findAll(array('condition'=>'TM_STU_Parent_Id =:parent','params'=>array(':parent'=>Yii::app()->user->id)));
		$this->render('setup',array('parent'=>$parent,'students'=>$students));
	}
    public function actionUpdate()
    {
        $user=Yii::app()->user->id;
        $connection = CActiveRecord::getDbConnection();
        $sql="UPDATE tm_profiles SET firstname='".addslashes ($_POST['firstname'])."',lastname='".addslashes ($_POST['lastname'])."' WHERE user_id='$user'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $arr = array('firstname' => $_POST['firstname'],'lastname'=> $_POST['lastname'],'error'=>'No');
        echo json_encode($arr);
    }
    public function actionStudentupdate()
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="UPDATE tm_profiles SET firstname='".addslashes ($_POST['firstname'])."',lastname='".addslashes ($_POST['lastname'])."' WHERE user_id='".addslashes ($_POST['studid'])."'";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $sql1="UPDATE tm_student SET TM_STU_First_Name='".addslashes ($_POST['firstname'])."',TM_STU_Last_Name='".addslashes ($_POST['lastname'])."',TM_STU_School='".addslashes ($_POST['school'])."',TM_STU_SchoolCustomname='".addslashes ($_POST['schoolother'])."' WHERE TM_STU_User_Id='".addslashes ($_POST['studid'])."'";
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        if($_POST['school']=='0')
        {
            $schoolname='0';
        }
        else
        {
            $school=School::model()->findByPk($_POST['school']);
            $schoolname=$school->TM_SCL_Name;
        }
        
        $arr = array('firstname' => $_POST['firstname'],'lastname'=> $_POST['lastname'],'school'=>$schoolname,'schoolother'=>$_POST['schoolother'],'error'=>'No');
        echo json_encode($arr);
    }
    public function actionRemovestudent()
    {
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$_POST['studid']));
        $parentstudents=Student::model()->count(array('condition'=>'TM_STU_Parent_Id='.$student->TM_STU_Parent_Id));
        if($parentstudents>1):
            $connection = CActiveRecord::getDbConnection();
            $sql="DELETE FROM tm_student_plan WHERE TM_SPN_StudentId='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $sql="DELETE FROM tm_student WHERE TM_STU_User_Id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $sql="DELETE FROM tm_profiles WHERE user_id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $sql="DELETE FROM tm_users WHERE id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $sql="DELETE FROM tm_buddies WHERE TM_BD_Student_Id='".$_POST['studid']."' OR TM_BD_Connection_Id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();            
            $sql="DELETE FROM tm_notifications WHERE TM_NT_User='".$_POST['studid']."' OR TM_NT_Target_Id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $sql="DELETE FROM tm_studentpoints WHERE TM_STP_Student_Id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();                                
            $sql="DELETE FROM tm_studentlevels WHERE TM_STL_Student_Id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $sql="DELETE FROM tm_chalangerecepient WHERE TM_CE_RE_Recepient_Id='".$_POST['studid']."'";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();               
            $arr = array('id' => $_POST['studid'],'error'=>'No');
            echo json_encode($arr);
        else:
            $arr = array('errortext' => 'Cannot remove student.','error'=>'Yes');
            echo json_encode($arr);
        endif;

    }
    public function actionGetStudentForm()
    {

        $this->renderPartial('_studentform');
    }
    public function actionGetPlanForm()
    {
        $parent=User::model()->findByPk(Yii::app()->user->id);
        $studentdetails=User::model()->findByPk($_POST['student']);
        if(isset($_POST['mode'])):
            $mode=$_POST['mode'];
        else:
            $mode='';
        endif;
        
        $this->renderPartial('_planform',array('parent'=>$parent,'student'=>$_POST['student'],'studentdetails'=>$studentdetails,'mode'=>$mode));
    }
    public function actionGetChangePassword()
    {
        $model = new UserChangePassword;
        $this->renderPartial('_passwordform',array('model'=>$model,'student'=>$_POST['user'],'type'=>$_POST['type']));
    }
    public function actionChangePassword()
    {
        $model = new UserChangePassword;
        if(isset($_POST['UserChangePassword'])) {
            $model->attributes=$_POST['UserChangePassword'];
            if($model->validate()) {
                $new_password = User::model()->notsafe()->findbyPk($_POST['userid']);
                $new_password->password = UserModule::encrypting($model->password);
                $new_password->activkey=UserModule::encrypting(microtime().$model->password);
                $new_password->save();
                $parentstudents=Student::model()->findAll(array('condition'=>'TM_STU_Parent_Id='.$_POST['userid'].' '));
                foreach($parentstudents AS $parentstudent):
                    $studentmodel=Student::model()->findByPk($parentstudent->TM_STU_Id);
                    $studentmodel->TM_STU_Parentpass=$model->password;
                    $studentmodel->save(false);
                endforeach;
                if($_POST['usertype']=='student')
                {
                    $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$_POST['userid']));
                    $student->TM_STU_Password=$model->password;
                    $student->save(false);
                }
                $this->redirect(array("Home"));
            }
        }
    }
    public function actionGetStandardList()
    {        
        if(isset($_POST['id']))
        {
            $data=Standard::model()->with('plan')->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0' AND TM_PN_Visibility='0'",
                array(':syllabus'=>(int) $_POST['id']));
            $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
            /*if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Chapter',true);
            endif;*/
            echo CHtml::tag('option',array('value'=>''),'Select Standard',true);
            $location=User::model()->findByPk(Yii::app()->user->id)->location;            
            foreach($data as $value=>$name)
            {                
                if($location=='0'):
                    $plancount=Plan::model()->count(array('condition'=>'TM_PN_Standard_Id='.$value.' AND TM_PN_Visibility=0  AND (TM_PN_Currency_Id=1 OR TM_PN_Additional_Currency_Id=1)'));
                else:
                    $plancount=Plan::model()->count(array('condition'=>'TM_PN_Standard_Id='.$value.' AND TM_PN_Visibility=0  AND (TM_PN_Currency_Id!=1 OR TM_PN_Additional_Currency_Id NOT IN(0,1))'));
                endif;                
                if($plancount!=0):
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                endif;
            }
        }
    }
    public function actionGetPlanList()
    {
        if(isset($_POST['publisher']))
        {
            $location=User::model()->findByPk(Yii::app()->user->id)->location; 
            if($location=='0'):
                $condition="TM_PN_Publisher_Id=:publisher AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND (TM_PN_Currency_Id=1 OR TM_PN_Additional_Currency_Id=1)";
            else:
                $condition="TM_PN_Publisher_Id=:publisher AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND (TM_PN_Currency_Id!=1 OR TM_PN_Additional_Currency_Id NOT IN(0,1))";
            endif;             
            $data=Plan::model()->with('currency')->findAll(
                array(
                        'condition'=>$condition,
                        'params'=>array(':publisher'=>$_POST['publisher'],':syllabus'=>$_POST['syllabus'],':standard'=>$_POST['standard'])
                )
            );

            echo CHtml::tag('option',array('value'=>''),'Select Subscription',true);
            foreach($data as $key=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$name->TM_PN_Id),CHtml::encode($name->TM_PN_Name),true);
            }
        }
    }
    public function actionGetPlanAmount()
    {
        if(isset($_POST['plan']))
        {
            $plan=Plan::model()->with('currency')->with('additional')->findByPk($_POST['plan']);
            $location=User::model()->findByPk(Yii::app()->user->id)->location; 			 
            if($location=='0' & $plan->TM_PN_Currency_Id=='1'):
                $returnarray['planid']=$_POST['plan'];
                $returnarray['planname']=$plan->TM_PN_Name;
                if($_POST['voucher']!='' & $_POST['voucher']!='-1'): 
                                       
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Rate*$percentage;
                         $amount=round($amount,2,PHP_ROUND_HALF_UP);
                         $couponamount=$voucher->currency->TM_CR_Code.' '.$amount;
                         $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Rate-$voucher->TM_CP_value;                         
                    endif;                
                    $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                else:
                    
                    $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.base64_decode($_POST["student"])));
                    $schooldiscount=School::model()->findByPk($student->TM_STU_School);
                    $returnarray['schoolcoupon']='yes';  
                    if($schooldiscount->TM_SCL_Discount):
                        if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                             $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                             $amount=$plan->TM_PN_Rate*$percentage;
    						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                             $couponamount=$plan->currency->TM_CR_Code.' '.$amount;
                             $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                        elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                            $returnarray['planrate']=$plan->TM_PN_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                            $couponamount=$plan->currency->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                        endif;                
                        $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
        	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                  <div class="col-md-6 pull-right">
                                                	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                  </div></div>';
                        $returnarray['plancurrency']=$plan->currency->TM_CR_Code;                    
                    else:                  
                        $returnarray['coupon']='no';
                        $returnarray['planrate']=$plan->TM_PN_Rate;
                        $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                    endif;                                    
                endif;                                
            elseif($location=='0' & $plan->TM_PN_Additional_Currency_Id=='1'):
                $returnarray['planid']=$_POST['plan'];   
				$returnarray['planname']=$plan->TM_PN_Name;				 
                if($_POST['voucher']!='' & $_POST['voucher']!='-1'):
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Additional_Rate*$percentage;  
                         $amount=round($amount,2,PHP_ROUND_HALF_UP);                       
                         $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$voucher->TM_CP_value;                        
                    endif;                
                    $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                else:
                
                    $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.base64_decode($_POST["student"])));
                    $schooldiscount=School::model()->findByPk($student->TM_STU_School);
                    $returnarray['schoolcoupon']='yes';  
                    if($schooldiscount->TM_SCL_Discount):
                        if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                             $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                             $amount=$plan->TM_PN_Additional_Rate*$percentage;
    						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                             $couponamount=$plan->additional->TM_CR_Code.' '.$amount;
                             $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                        elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                            $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                            $couponamount=$plan->additional->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                        endif;                
                        $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
        	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                  <div class="col-md-6 pull-right">
                                                	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                  </div></div>';
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;                    
                    else:                  
                        $returnarray['coupon']='no';
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate;
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                    endif;                           
                endif;            
            elseif($location=='1' & $plan->TM_PN_Currency_Id!='1'):
                $returnarray['planid']=$_POST['plan'];  
				$returnarray['planname']=$plan->TM_PN_Name;
                if($_POST['voucher']!='' & $_POST['voucher']!='-1'):
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Rate*$percentage;
                         $amount=round($amount,2,PHP_ROUND_HALF_UP);
                         $couponamount=$voucher->currency->TM_CR_Code.' '.$amount;
                         $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->currency->TM_CR_Code.' '.$voucher->TM_CP_value; 
                    endif;                
                    $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                else:
                        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.base64_decode($_POST["student"])));
                        $schooldiscount=School::model()->findByPk($student->TM_STU_School);
                        $returnarray['schoolcoupon']='yes';  
                        if($schooldiscount->TM_SCL_Discount):
                            if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                                 $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                                 $amount=$plan->TM_PN_Rate*$percentage;
        						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                                 $couponamount=$plan->currency->TM_CR_Code.' '.$amount;
                                 $returnarray['planrate']=$plan->TM_PN_Rate-$amount;
                            elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                                $returnarray['planrate']=$plan->TM_PN_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                                $couponamount=$plan->currency->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                            endif;                
                            $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
            	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                      <div class="col-md-6 pull-right">
                                                    	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                      </div></div>';
                            $returnarray['plancurrency']=$plan->currency->TM_CR_Code;                    
                        else:                  
                            $returnarray['coupon']='no';
                            $returnarray['planrate']=$plan->TM_PN_Rate;
                            $returnarray['plancurrency']=$plan->currency->TM_CR_Code;
                        endif;
                endif;            
            elseif($location=='1' & ($plan->TM_PN_Additional_Currency_Id!='1' || $plan->TM_PN_Additional_Currency_Id!='0')):
                $returnarray['planid']=$_POST['plan'];
				$returnarray['planname']=$plan->TM_PN_Name;                
                if($_POST['voucher']!='' & $_POST['voucher']!='-1'):
                    $voucher=Coupons::model()->with('currency')->findByPk($_POST['voucher']);
                    $returnarray['coupon']='yes';
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Additional_Rate*$percentage;
                         $amount=round($amount,2,PHP_ROUND_HALF_UP);
                         $couponamount=$voucher->currency->TM_CR_Code.' '.$amount;
                         $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->currency->TM_CR_Code.' '.$voucher->TM_CP_value; 
                    endif;  
                    $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                else:                    
                    $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.base64_decode($_POST["student"])));
                    $schooldiscount=School::model()->findByPk($student->TM_STU_School);
                    $returnarray['schoolcoupon']='yes';  
                    if($schooldiscount->TM_SCL_Discount):
                        if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                             $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                             $amount=$plan->TM_PN_Additional_Rate*$percentage;
    						 $amount=round($amount,2,PHP_ROUND_HALF_UP);		
                             $couponamount=$plan->additional->TM_CR_Code.' '.$amount;
                             $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$amount;
                        elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                            $returnarray['planrate']=$plan->TM_PN_Additional_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                            $couponamount=$plan->additional->TM_CR_Code.' '.$schooldiscount->TM_SCL_Discount_Amount; 
                        endif;                
                        $returnarray['coupondetails']='<div class="row" id="couponrow"><div class="col-md-6 pull-left">
        	                                   <span class="text-left"><h5>Voucher : School Discount </h5></span></div>
                                                  <div class="col-md-6 pull-right">
                                                	<h5 class="text-right"><strong>'.$couponamount.'</strong></h5>
                                                  </div></div>';
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;                    
                    else:                  
                        $returnarray['coupon']='no';
                        $returnarray['planrate']=$plan->TM_PN_Additional_Rate;
                        $returnarray['plancurrency']=$plan->additional->TM_CR_Code;
                    endif;                
                endif;            
            endif;            
            echo json_encode($returnarray);
        }
    }
    public function actionAddsubscription()
    {
        if(isset($_POST['plan'])):

            $model=new StudentPlan();
            $model->TM_SPN_PlanId=$_POST['plan'];
            $model->TM_SPN_Publisher_Id=$_POST['publisher'];
            $model->TM_SPN_SyllabusId=$_POST['syllabus'];
            $model->TM_SPN_SubjectId='1';
            $model->TM_SPN_StandardId=$_POST['standard'];
            $model->TM_SPN_StudentId=base64_decode($_POST['idst']);
            $model->TM_SPN_Status='1';
            $model->save(false);
            $studentdetails=User::model()->findByPk(base64_decode($_POST['idst']));
            $plan=Plan::model()->findByPk($_POST['plan']);
            pay_page( array ('key' => 'ZDU6OY', 'txnid' => uniqid( $studentdetails->username ), 'amount' => $plan->TM_PN_Rate,
                'firstname' => $_POST['firstname'].' '.$_POST['lastname'], 'email' => $_POST['email'], 'phone' => $_POST['phone'],
//                'productinfo' => 'Subscription of '.$plan->TM_PN_Name.' for '.$studentdetails->username, 'surl' =>$this->createAbsoluteUrl('parent/PaymentComplete',array('supredent'=>base64_encode($_POST['idst']),'application'=>base64_encode($plan->TM_PN_Id),'setup'=>base64_encode('success'))), 'furl' =>  $this->createAbsoluteUrl('parent/PaymentComplete',array('supredent'=>base64_encode($_POST['idst']),'application'=>base64_encode($plan->TM_PN_Id),'setup'=>base64_encode('failed')))),
                'productinfo' => 'Subscription of '.$plan->TM_PN_Name.' for '.$studentdetails->username, 'surl' =>$this->PaymentComplete(), 'furl' =>$this->PaymentComplete()),
                'g4KQpESl' );
            //$this->redirect(array('Home'));
        endif;
    }
	public function actionPaymentcompletepaypal()	
	{
		if(base64_decode($_GET['setup'])=='Completed')
		{
			$plan=Plan::model()->findByPk(base64_decode($_GET['application'])); 
            $duration=$plan->TM_PN_Duration+1;
            $plancount=StudentPlan::model()->count(array('condition'=>'TM_SPN_Status=0 AND TM_SPN_StudentId=:student','params'=>array(':student'=>base64_decode($_GET['supredent']))));                    
            $model=new StudentPlan();
            $model->TM_SPN_PlanId=$plan->TM_PN_Id;
            $model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
            $model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
            $model->TM_SPN_SubjectId='1';
            $model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
            $model->TM_SPN_StudentId=base64_decode($_GET['supredent']);
            $model->TM_SPN_Status='0';
            $model->TM_SPN_PaymentReference=$_GET['transaction'];
            $model->TM_SPN_CouponId=base64_decode($_GET['voucher']);
            $model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
			$model->TM_SPN_Provider='1';
            $parentuserlocation=User::model()->findByPk(Yii::app()->user->id)->location;			
            $currency=$this->getCurrency($parentuserlocation,$plan->TM_PN_Id);                                                                                 
            $model->TM_SPN_Currency=$currency;			
            $model->save(false);
            
            if($plancount>0):
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The additional subscription payment has been authorised. Please note your payment transaction ID : ".$_GET['transaction']));
            else:
                $user=User::model()->findByPk(base64_decode($_GET['supredent']));
                $user->status='1';
                $user->save(false);
                $this->SendStudentMail(base64_decode($_GET['supredent']));
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
            endif;
            $this->redirect(array('Home'));			
		}	
		elseif($_GET['cancel']=='yes')
		{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The transaction has been cancelled. If you are seeing this in error, please contact support@tabbiemath.com."));
           $this->redirect(array('Home'));
		}		
        else{
				Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Pending Payment. Contact Admin to activate your account."));
				$this->redirect(array('Home'));
        }
	}
	public function actionPaymentcompletestripe()	
	{		
        if(base64_decode($_GET['setup'])=='Completed')
		{
			$plan=Plan::model()->findByPk(base64_decode($_GET['application'])); 
            $duration=$plan->TM_PN_Duration+1;
            $plancount=StudentPlan::model()->count(array('condition'=>'TM_SPN_Status=0 AND TM_SPN_StudentId=:student','params'=>array(':student'=>base64_decode($_GET['supredent']))));                    
            $model=new StudentPlan();
            $model->TM_SPN_PlanId=$plan->TM_PN_Id;
            $model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
            $model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
            $model->TM_SPN_SubjectId='1';
            $model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
            $model->TM_SPN_StudentId=base64_decode($_GET['supredent']);
            $model->TM_SPN_Status='0';
            $model->TM_SPN_PaymentReference=$_GET['transaction'];
            $model->TM_SPN_CouponId=base64_decode($_GET['voucher']);
            $model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));  
            $model->TM_SPN_Provider='2';                  
            $parentuserlocation=User::model()->findByPk(Yii::app()->user->id)->location;            
            $currency=$this->getCurrency($parentuserlocation,$plan->TM_PN_Id);                                                                                 
            $model->TM_SPN_Currency=$currency;            
            $model->save(false);
            
            if($plancount>0):
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The additional subscription payment has been authorised. Please note your payment transaction ID : ".$_GET['transaction']));
            else:
                $user=User::model()->findByPk(base64_decode($_GET['supredent']));
                $user->status='1';
                $user->save(false);
                $this->SendStudentMail(base64_decode($_GET['supredent']));
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
            endif;
            $this->redirect(array('Home'));			
		}	
		elseif($_GET['cancel']=='yes')
		{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The transaction has been cancelled. If you are seeing this in error, please contact support@tabbiemath.com."));
           $this->redirect(array('Home'));
		}		
        else{
				Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Pending Payment. Contact Admin to activate your account."));
				$this->redirect(array('Home'));
        }
	} 
    public function actionPaymentComplete()
    {

        if(isset($_GET['supredent'])&& isset($_GET['application']))
        {
            $plan=Plan::model()->findByPk(base64_decode($_GET['application'])); 
            $duration=$plan->TM_PN_Duration+1;
            $plancount=StudentPlan::model()->count(array('condition'=>'TM_SPN_Status=0 AND TM_SPN_StudentId=:student','params'=>array(':student'=>base64_decode($_GET['supredent']))));                    
            $model=new StudentPlan();
            $model->TM_SPN_PlanId=$plan->TM_PN_Id;
            $model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
            $model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
            $model->TM_SPN_SubjectId='1';
            $model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
            $model->TM_SPN_StudentId=base64_decode($_GET['supredent']);
            $model->TM_SPN_Status='0';
            $model->TM_SPN_PaymentReference=$_GET['transaction'];
            $model->TM_SPN_CouponId=base64_decode($_GET['voucher']);
            $model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
            $parentuserlocation=User::model()->findByPk(Yii::app()->user->id)->location;           
            $currency=$this->getCurrency($parentuserlocation,$plan->TM_PN_Id);			
            $model->TM_SPN_Currency=$currency;
            $model->save(false);
            
            if($plancount>0):
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The additional subscription payment has been authorised. Please note your payment transaction ID : ".$_GET['transaction']));
            else:
                $user=User::model()->findByPk(base64_decode($_GET['supredent']));
                $user->status='1';
                $user->save(false);
                $this->SendStudentMail(base64_decode($_GET['supredent']));
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
            endif;
            $this->redirect(array('Home'));
        }
        elseif(isset($_POST['idst']))
        {
            $plan=Plan::model()->findByPk($_POST['plan']); 
            $duration=$plan->TM_PN_Duration+1;
            $plancount=StudentPlan::model()->count(array('condition'=>'TM_SPN_Status=0 AND TM_SPN_StudentId=:student','params'=>array(':student'=>base64_decode($_POST['idst']))));                    
            $model=new StudentPlan();
            $model->TM_SPN_PlanId=$plan->TM_PN_Id;
            $model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
            $model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
            $model->TM_SPN_SubjectId='1';
            $model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
            $model->TM_SPN_StudentId=base64_decode($_POST['idst']);
            $model->TM_SPN_Status='0';        
            $model->TM_SPN_CouponId=$_POST['voucherid'];
            $model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
            $currency=$this->getCurrency($parentuserlocation,$plan->TM_PN_Id);         
            $parentuserlocation=User::model()->findByPk(Yii::app()->user->id)->location;
            $model->TM_SPN_Currency=$currency; 
			$model->save(false);
            
            if($plancount>0):
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The additional subscription has been activated."));
            else:
                $user=User::model()->findByPk(base64_decode($_POST['idst']));
                $user->status='1';
                $user->save(false);
                Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. Subscription has been activated. You may now login to TabbieMath and start using it."));
                $this->SendStudentMail(base64_decode($_POST['idst']));
            endif;
            $this->redirect(array('Home'));
        }
		elseif($_GET['cancel']=='yes')
		{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The transaction has been cancelled. If you are seeing this in error, please contact support@tabbiemath.com."));
            $this->redirect(array('Home'));
		}		
        else{
            Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The payment has failed. Please check for the message from Payu for the failure"));
            $this->redirect(array('Home'));
        }
		

    }
    public function actionAddstudent()
    {
        $student=new Student();
        $user=Yii::app()->user->id;
        $studentfirstname=ltrim($_POST['firstname']);
        $studentlastname=ltrim($_POST['lastname']); 
        $username=strtoupper(substr($studentfirstname, 0,1).substr($studentlastname, 0,1).rand(11111,99999));
        $password=$this->generateRandomString();
        $studentuser=new User();
        $studentuser->username=$username;
        $studentuser->password=md5($password);
        $studentuser->activkey=UserModule::encrypting(microtime().$studentuser->password);
        //$studentuser->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
        $studentuser->superuser=0;
        $studentuser->createtime=time();
        $studentuser->usertype='6';
        $studentuser->status=0;
        $studentuser->email=$username;
        $studentuser->save(false);
        $studentuserprofile=new Profile();
        $studentuserprofile->user_id=$studentuser->id;
        $studentuserprofile->lastname=$_POST['lastname'];
        $studentuserprofile->firstname=$_POST['firstname'];
        $studentuserprofile->save(false);
        $student->TM_STU_Parent_Id=$user;
        $student->TM_STU_User_Id=$studentuser->id;
        $student->TM_STU_First_Name=$studentuserprofile->firstname;
        $student->TM_STU_Last_Name=$studentuserprofile->lastname;        
        $student->TM_STU_School=$_POST['school'];
        if($_POST['school']=='0'):
            $student->TM_STU_SchoolCustomname=$_POST['schoolcustom'];
            $student->TM_STU_City=$_POST['city'];
            $student->TM_STU_State=$_POST['state'];
        else:
            $school=School::model()->findByPk($_POST['school']); 
            $student->TM_STU_City=$school->TM_SCL_City;  
			$student->TM_STU_State=$school->TM_SCL_State;       
        endif;        
        $student->TM_STU_Password=$password;
        $student->TM_STU_CreatedBy=Yii::app()->user->id;
        $student->save(false);
//        $this->SendStudentMail($studentuser->id);
        $this->redirect(array('Home','mode' =>base64_encode('subscription'),'action'=>base64_encode($studentuser->id),'#'=>'studentraw'.$studentuser->id));
    }
    public function SendStudentMail($id)
    {
        
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$id));
        $user=User::model()->findByPk($student->TM_STU_Parent_Id);
        $studentuser=User::model()->findByPk($student->TM_STU_User_Id);
        $adminEmail = Yii::app()->params['noreplayEmail'];
        	$activation_url = $this->createAbsoluteUrl('/user/login/');
        $message="<p>Dear ".$user->profile->firstname."</p>
                    <p>You have added a student</p>
                    <p><b>STUDENT ACCESS</b></p>
                    <p>".$student->TM_STU_First_Name." can now use these details below to login.</p>
                    <p>S/he can change his password or profile picture anytime by logging in and going into personal setup.<br>
                        For any help in getting started, please visit the Help Section at <a href='http://www.tabbiemath.com/demo-faq/'>http://www.tabbiemath.com/demo-faq/<a/></p>
                    <div style='border:solid 1px black;background-color: #E6E6FA;padding: 10px;text-align: center;'>
                        <p style='text-align:center;'>Sign In at <a href='".$activation_url."'> www.tabbiemath.com</a> </p>
                        <p><b>Student Name</b> : ".$student->TM_STU_First_Name." ".$student->TM_STU_Last_Name."</p>
                        <p><b>Username </b>: ".$studentuser->username."</p>
                        <p><b>Password </b>: ".$student->TM_STU_Password."</p>
                    </div>                    
                    <p>Regards,<br>The TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($user->email);
        $mail->AddReplyTo("noreply@tabbiemath.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Registration ';
        $mail->Body = $message;

        return $mail->Send();
    }
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function actionCancelsubscription()
    {
        $studentplan=StudentPlan::model()->findByPk($_POST['subscription']);
        $student=$studentplan->TM_SPN_StudentId;
        $studentplan->TM_SPN_Status='1';
        $studentplan->TM_SPN_CancelDate=date('Y-m-d');
        $studentplan->save(false);
        $subscriptioncount=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student.' AND TM_SPN_Status=0'));
        if($subscriptioncount==0):
            $user=User::model()->findByPk($student);     
            $user->status='0';
            $user->save(false);
        endif;
        $this->redirect(array('Home'));
    }
    public function GetChallengeInvitees($challenge,$user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge.' AND TM_CE_RE_Recepient_Id!='.$user;
        $users=Chalangerecepient::model()->findAll($criteria);
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user));
        $usersjson=$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name;
        if(count($users)>0):
            $othercount=count($users);
            $others = " + <a href='javascript:void(0);' class='hover' data-id='$challenge' data-tooltip='tooltip$challenge' style='color: #FFA900;' data-toggle='modal' data-target='#myinvite'>" . $othercount . " Others" . "</a>";            
        else:
            $others='';
        endif;
        /*foreach($users AS $key=>$user):
            if($key<3):
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username;
                $usersjson.=','.$challengeuser;
            endif;
        endforeach;*/
        return $usersjson.$others;

    }
    public function GetChallengePosition($users, $id)
    {
        $position = array_keys($users, $id);         
        $position1 = $position[0] + 1;       
        if ($position1 == '1'):
            return $position1 . '<sup>st</sup><span class="fa fa-trophy size2 brd_r2"></span>';
        elseif ($position1 == '2'):
            return $position1 . '<sup>nd</sup>';
        elseif ($position1 == '3'):
            return $position1 . '<sup>rd</sup>';
        else:
            return $position1 . '<sup>th</sup>';
        endif;
    }
    public function GetChallengePositionPop($users, $id)
    {
        $position = array_keys($users, $id);
        $position1 = $position[0] + 1;
        $status="<span class='item-right'>";          
        if ($position1 == '1'):            
            $status.="<span class='cupclr'><i class='fa fa-trophy size2'></i></span><span class='mrk'>".$position1."<sup>st</sup></span>";
        elseif ($position1 == '2'):            
            $status.="<span class='mrk'>".$position1."<sup>nd</sup></span>";
        elseif ($position1 == '3'):            
            $status.="<span class='mrk'>".$position1."<sup>rd</sup></span>";
        else:
            $status.="<span class='mrk'>".$position1."<sup>th</sup></span>";            
        endif;
        	$status.='</span>';	
        return $status;
    }
    public function actionGetChallengeBuddies()
    {
        if($_POST['challenge']):
            $challenge=$_POST['challenge'];
            $model=Challenges::model()->findByPk($challenge);
            $userid=Yii::app()->user->id;
            $criteria=new CDbCriteria;
            $criteria->condition='TM_CE_RE_Chalange_Id='.$challenge;
            $criteria->order = 'TM_CE_RE_ObtainedMarks DESC';
            $users=Chalangerecepient::model()->findAll($criteria);
            $buddies='';
            $userids=array();
            foreach($users AS $party):
                $userids[]=$party->TM_CE_RE_Recepient_Id;
            endforeach;
            foreach($users AS $key=>$user):
              if ($user->TM_CE_RE_Status == '0'):                        
                        if($model->TM_CL_Chalanger_Id==$user->TM_CE_RE_Recepient_Id):                    
                            $status="<span class='item-right'><span class='mrk'>Creator</span></span>";
                        else: 
                            $status="<span class='item-right'><span class='mrk'>Awaiting Response</span></span>";
                        endif;                        
                        if($user->TM_CE_RE_Recepient_Id != $userid & $model->TM_CL_Chalanger_Id==$userid):
                                $reminder="<span class='spsp rem" . $user->TM_CE_RE_Recepient_Id . "'>
                                    		  <i class='fa fa-bell rbell'></i><a class='reminder  ' data-id='".$user->TM_CE_RE_Recepient_Id."' style='color: #FFA900;font-size: 10px;'> Send Reminder</a>
                                    		</span>
                                    		<span class='spsp remsend" . $user->TM_CE_RE_Recepient_Id . "' style='color: #9E9E9E;display:none'>Reminder Sent</span>";
                        else:
                            $reminder='';
                        endif;                    
                    elseif ($user->TM_CE_RE_Status == '1'):
                        if($model->TM_CL_Chalanger_Id==$user->TM_CE_RE_Recepient_Id):                    
                            $status="<span class='item-right'><span class='mrk'>Creator</span></span>";
                        else: 
                            $status="<span class='item-right'><span class='mrk'>Accepted</span></span>";
                        endif;
                        if($user->TM_CE_RE_Recepient_Id != $userid & $model->TM_CL_Chalanger_Id==$userid):
                                $reminder="<span class='spsp rem" . $user->TM_CE_RE_Recepient_Id . "'>
                                    		  <i class='fa fa-bell rbell'></i><a class='reminder  ' data-id='".$user->TM_CE_RE_Recepient_Id."' style='color: #FFA900;font-size: 10px;'> Send Reminder</a>
                                    		</span>
                                    		<span  class='spsp remsend" . $user->TM_CE_RE_Recepient_Id . "'  style='color: #9E9E9E;display:none'>Reminder Sent</span>";
                        else:
                            $reminder='';
                        endif;   
                    elseif ($user->TM_CE_RE_Status == '2'):
                        if($model->TM_CL_Chalanger_Id==$user->TM_CE_RE_Recepient_Id):                    
                            $status="<span class='item-right'><span class='mrk'>Creator</span></span>";
                        else: 
                            $status="<span class='item-right'><span class='mrk'>Accepted</span></span>";
                        endif;                        
                        if($user->TM_CE_RE_Recepient_Id != $userid):
                                $reminder="<span class='spsp rem" . $user->TM_CE_RE_Recepient_Id . "'>
                                    		  <i class='fa fa-bell rbell'></i><a class='reminder' data-id='".$user->TM_CE_RE_Recepient_Id."' style='color: #FFA900;font-size: 10px;'> Send Reminder</a>
                                    		</span>
                                    		<span  class='spsp remsend" . $user->TM_CE_RE_Recepient_Id . "' style='color: #9E9E9E;display:none'>Reminder Sent</span>";
                        else:
                            $reminder='';
                        endif;   
                    elseif ($user->TM_CE_RE_Status == '3'):
                        $status = $this->GetChallengePositionPop($userids, $user->TM_CE_RE_Recepient_Id);
                    elseif ($user->TM_CE_RE_Status == '4'):
                        $status="<span class='item-right'><span class='mrk'>Declined</span></span>";
                        $reminder=''; 
                        //$status = "NA".($user->TM_CE_RE_Recepient_Id == $userid?"":"</br><a class='reminder  rem" . $user->TM_CE_RE_Recepient_Id . "'data-id='$user->TM_CE_RE_Recepient_Id' style='color: #FFA900;'>Send Reminder</a></br><span class='remsend" . $user->TM_CE_RE_Recepient_Id . "' hidden style='background-color:rgb(255,255,0)'>Reminder sent</span>")                      
                    endif;
                    if ($user->TM_CE_RE_Recepient_Id == $userid):
                        $challengeuser = 'Me';
                    else:
                        $student = Student::model()->find(array('condition' => "TM_STU_User_Id=" . $user->TM_CE_RE_Recepient_Id));
                        $challengeuser = $student->TM_STU_First_Name . " " . $student->TM_STU_Last_Name;
                        $imgname=$student->TM_STU_Image;
                    endif;
    
                    $student = Student::model()->find(array('condition' => "TM_STU_User_Id=" . $user->TM_CE_RE_Recepient_Id));
                    $imgname=$student->TM_STU_Image;
                        $buddies.="
                            	<li><span class='item'><span class='item-left'>
                            				<img src='".Yii::app()->request->baseUrl."/site/Imagerender/id/".($imgname != '' ? $imgname : 'noimage.png')."?size=large&type=user&width=60&height=60' class='img-circle tab_imgsmall2'>
                            				<span class='item-info'>
                            					<span class='name_display'><b>".$challengeuser."</b></span>".$reminder."                                                                    					
                            				</span>
                            			</span>".$status."
                            		</span>
                            	</li>                    
                        ";
            endforeach;
            echo $buddies;
        endif;

    }
    public function actionGetSolution()
    {
        if (isset($_POST['testid'])):
            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7)',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' =>$_POST['student'], ':test' => $_POST['testid'])
                )
            );
            $resulthtml = '';
            foreach ($results AS $result):
                $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '7'   ):
                        $resulthtml.='<tr><td><div class="col-md-3 padnone">
                    <span> <b>Question'.$result->TM_STU_QN_Number.'</b></span>
                    <span class="pull-right">Marks: '.$displaymark.'</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="'. $question->TM_QN_Id.'" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :'.$question->TM_QN_QuestionReff.'</span>
                    </div><div class="col-md-12">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .=  '<div class="col-md-12 padnone"><b>Ans :</b></div>';
                        $resulthtml .='<div class="col-md-12 ">';

                        if($question->TM_QN_Solutions!=''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .='</div>';


                        $resulthtml.= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" hidden/></div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden ></div>
                           </div>
                            <div class="col-md-12 mailSendComplete' . $question->TM_QN_Id . '" id="" hidden >Your Message Has Been Sent To Admin
                                </div>

                        </td></tr>';
                    else:
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti=1;
                        $displaymark = 0;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;
                            $childresulthtml .='<div class="col-md-12 padnone">Part :'.$countmulti.'</div>';
                            $childresulthtml .='<div class="col-md-12 ">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') ;
                            $childresulthtml .='</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3 padnone"><span><b>Question '.$result->TM_STU_QN_Number.'</b> </span><span class="pull-right">Marks: '.$displaymark.'</span></div>
                                            <div class="col-md-3 pull-right"><a href="javascript:void(0)" class="showSolutionItem" data-reff="'. $question->TM_QN_Id.'" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a><span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :'.$question->TM_QN_QuestionReff.'</span></div>';

                        $resulthtml .= '<div class="col-md-12"><div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12 padnone"><b>Ans :</b></div>';
                        $resulthtml .='<div class="col-md-12 ">';
                        if($question->TM_QN_Solutions!=''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .='</div>';

                        $resulthtml.= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestiontest'.$question->TM_QN_Id.'" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2"name="comments"  id="comments" hidden/></div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="'.$question->TM_QN_QuestionReff.'" data-id="'.$question->TM_QN_Id.'"hidden ></div>
                           </div>
                            <div class="col-md-12 mailSendComplete' . $question->TM_QN_Id . '" id="" hidden >Your Message Has Been Sent To Admin
                                </div>
                        </td></tr>';

                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function PlanModules($id)
    {
        $retarray=array();
        $modules=Planmodules::model()->findAll(array('condition'=>'TM_PM_Plan_Id='.$id));
        $retarray['quick']=false;
        $retarray['difficulty']=false;
        $retarray['challenge']=false;
        $retarray['mock']=false;
        foreach($modules AS $module):
            switch($module->TM_PM_Module)
            {
                case "1":
                    $retarray['quick'] = true;
                    break;
                case "2":
                    $retarray['difficulty'] = true;
                    break;
                case "3":
                    $retarray['challenge'] = true;
                    break;
                case "4":
                    $retarray['mock'] = true;
                    break;
            }
        endforeach;
        if($retarray['quick']==true || $retarray['difficulty']==true):
            $retarray['revision']=true;
        endif;
        return $retarray;
    }
    public function GetBuddies($user)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='superuser=0 AND usertype=6 AND status=1 AND id!='.$user;
        $users=User::model()->findAll($criteria);
        $usersjson='';
        foreach($users AS $key=>$user):
            $usersjson.=($key==0?'{"id":'.$user->id.', "name":"'.$user->username.'"}':',{"id":'.$user->id.', "name":"'.$user->username.'"}');
        endforeach;
        return $usersjson;

    }
    public function GetQuestionCount($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
        $criteria->params=array(':test'=>$id);
        $selectedquestion=StudentQuestions::model()->findAll($criteria);
        return count($selectedquestion);
    }

    public function GetDateDifference($date1,$date2)
    {

        $diff = abs(strtotime($date2) - strtotime($date1));        
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        return $days;   
    }
    public function actionuploadImage()
    {
        $path = Yii::app()->basePath."/../images/userimages/";       
        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","JPG","PNG","GIF","BMP","JPEG");
        if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
        {
    
            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size']/1024;


            if(strlen($name))
            {
    
                list($txt, $ext) = explode(".", $name);
                if(in_array($ext,$valid_formats))
                {
                    if($size<(2000))
                    {

    
                        $user=Yii::app()->user->id;
                        $randomname=$this->generateRandomStringUpload();
                        $imgname=$user.$randomname.'.'.$ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];
                        if(move_uploaded_file($tmp, $path.$imgname))
                        {


                            echo $imgname;
                        }
                        else
                        {
                               echo "failed";
                        }
                         
                    }
                    else{
                        echo "Maximum allowed size is 2Mb";
                    }

                }
                else
                    echo "Invalid file format..";
            }
    
            else
                echo "Please select image..!";
    

        }

    }
    public function actionuploadImageSave(){
        $path = Yii::app()->basePath."/../images/userimages/";


        $valid_formats = array("jpg", "png", "gif", "bmp");
        if(isset($_POST['name']))
        {

            $name = $_POST['name'];


            if(strlen($name)) {

                $user = Yii::app()->user->id;
                $connection = CActiveRecord::getDbConnection();
                $sql = "UPDATE tm_profiles SET image='". addslashes($name)."' WHERE user_id='$user'";
                $command = $connection->createCommand($sql);
                $dataReader = $command->query();
            }

            echo $name;



        }

    }
    public function actionuploadChildImage(){
        $path = Yii::app()->basePath."/../images/userimages/";


        $valid_formats = array("jpg", "png", "gif", "bmp","JPG","PNG","GIF","BMP");
        if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
        {

            $name = $_FILES['photoimgchild']['name'];
            $size = $_FILES['photoimgchild']['size']/1024;


            if(strlen($name))
            {

                list($txt, $ext) = explode(".", $name);
                if(in_array($ext,$valid_formats))
                {
                    if($size<(2000))
                    {

                        $user=$_POST['studId'];
                        $randomname=$this->generateRandomStringUpload();
                        $imgname=$user.$randomname.'.'.$ext;
                        $tmp = $_FILES['photoimgchild']['tmp_name'];
                        if(move_uploaded_file($tmp, $path.$imgname))
                        {
                            echo $imgname;
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 2MB";
                }
                else
                    echo "Invalid file format..";
            }

            else
                echo "Please select image..!";


        }

    }
    public function actionuploadImageSaveChild(){
        $path = Yii::app()->basePath."/../images/userimages/";


        $valid_formats = array("jpg", "png", "gif", "bmp");
        if(isset($_POST['name']))
        {

            $name = $_POST['name'];
            $id = $_POST['id'];
            echo $name;

            $user = Student::model()->find(array('condition' => "TM_STU_User_Id='" . $id . "'"))->TM_STU_Id;

            if(strlen($name)) {
                list($txt, $ext) = explode(".", $name);

                $connection = CActiveRecord::getDbConnection();

                $sql = "UPDATE tm_student SET TM_STU_Image='". addslashes($name)."' WHERE TM_STU_Id='$user'";

                $command = $connection->createCommand($sql);
                $dataReader = $command->query();
            }





        }

    }
    public function generateRandomStringUpload($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function actionCheckactiveplan()
    {
        if(isset($_POST['plan'])):
            $count=StudentPlan::model()->count(array('condition'=>"TM_SPN_StudentId='".base64_decode($_POST['student'])."' AND TM_SPN_PlanId='".$_POST['plan']."' AND TM_SPN_Status='0'"));
            echo $count;
        endif;
    } 
    public function getCurrency($location,$plan)
    {
        $plan=Plan::model()->with('currency')->with('additional')->findByPk($_POST['plan']);
        if($location=='0'& $plan->TM_PN_Currency_Id=='1'):
            return $plan->TM_PN_Currency_Id;
        elseif($location=='0' & $plan->TM_PN_Currency_Id!='1' & $plan->TM_PN_Additional_Currency_Id=='1'):
            return $plan->TM_PN_Additional_Currency_Id;
        elseif($location=='1'& $plan->TM_PN_Currency_Id!='1'):
            return $plan->TM_PN_Currency_Id;
        elseif($location=='1' & ($plan->TM_PN_Additional_Currency_Id!='1' || $plan->TM_PN_Additional_Currency_Id!='0')):
            return $plan->TM_PN_Additional_Currency_Id;            
        endif;
                
    }     
    /**
     * student school progress
     */
    public function actionStudentschoolprogress($id)
    {
            $student=User::model()->findByPk($id);
            $plan=Plan::model()->findByPk($_GET['plan']);                   
            $week=$this->GetWeek(date('Y-m-d'));
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STHW_Student_Id=' . $id . ' AND TM_STHW_Status=5 AND TM_STHW_Plan_Id=' . $_GET['plan']);            
            $mocktotal = StudentHomeworks::model()->with('homework')->count($criteria);             
            $criteria->addCondition("TM_STHW_Date BETWEEN '".$week[0]."' AND '".$week[1]."'");
            $mockweekstatus = StudentHomeworks::model()->with('homework')->count($criteria);            
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Test_Type='Homework' AND TM_STP_Standard_Id=:standard AND TM_STP_Student_Id=" .$id;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => $plan->TM_PN_Standard_Id);
            $totalpoints = StudentPoints::model()->find($criteria);
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Test_Type='Homework' AND TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '" . $week[0] . "' AND '" . $week[1] . "' AND TM_STP_Student_Id=" . $id;
            $criteria->order = 'totalpoints DESC';            
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $weeklypoints = StudentPoints::model()->find($criteria);         
            $lastvisit=$student->lastvisit;
            if(!$lastvisit):
                $lastlogin='Never logged in';
            else:
                $lastlogin=date("jS F Y ",$lastvisit);
            endif;    
            $this->render('studentschoolprogress',array(
            'mocktotal'=>$mocktotal,
            'mockweekstatus'=>$mockweekstatus,
            'lastlogin'=>$lastlogin,        
            'student'=>$student,            
            'plan'=>$plan,   
            'weeklypoints'=>$weeklypoints, 
            'totalpoints'=>$totalpoints                                                     
            ));                                                 
    }
    public function actionGetschoolchapterstatus()
    {
        $plan=Plan::model()->findByPk($_POST['plan']);
        $connection = CActiveRecord::getDbConnection();   
        $sql="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".$_POST['student']."' AND TM_STHWQT_Parent_Id=0";                                   
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attempted=$dataReader->readAll();
        $attemptedquestions='';
        foreach($attempted AS $key=>$attemp):
        $attemptedquestions.=($key==0?'':',').$attemp['question'];
        endforeach;   
        $sql1="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".$_POST['student']."' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Marks!=0";                              
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        $correctones=$dataReader->readAll();        
        $correctquestions='';
        foreach($correctones AS $key=>$correct):
        $correctquestions.=($key==0?'':',').$correct['question'];
        endforeach; 
        $sql="SELECT count(a.TM_QN_Id) AS attemptedcount, a.TM_QN_Topic_Id,b.TM_TP_Name,
        (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Topic_Id=a.TM_QN_Topic_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$correctquestions." )) AS correctcount
                FROM 
                tm_question AS a,tm_topic AS b WHERE 
                a.TM_QN_Parent_Id=0 AND a.TM_QN_Topic_Id=b.TM_TP_Id AND b.TM_TP_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND b.TM_TP_Standard_Id='".$plan->TM_PN_Standard_Id."' AND TM_QN_Id IN (".$attemptedquestions.") group by TM_QN_Topic_Id";                                                         
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $chapters=$dataReader->readAll();                                              
        $this->renderPartial('_overallstatus', array('chapters' => $chapters, 'plan' => $plan,'correctquestions'=>$correctquestions,'attemptedquestions'=>$attemptedquestions,'student'=>$_POST['student'])); 
     }
    public function actionGetschooltopicstatus()
    {
        $topicstatus=$this->Getschooltopicstatus($_POST['student']);
        $this->renderPartial('_topicstatus', array('topicstatus' => $topicstatus));         
    }
    public function Getschooltopicstatus($user)
    {
        $plan=Plan::model()->findByPk($_POST['plan']);
        $connection = CActiveRecord::getDbConnection();
        $sql1="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".$user."' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Marks!=0";                        
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        $correctones=$dataReader->readAll();        
        $correctquestions='';
        foreach($correctones AS $key=>$correct):
        $correctquestions.=($key==0?'':',').$correct['question'];
        endforeach; 
        $sql="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".$user."' AND TM_STHWQT_Parent_Id=0";                
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attempted=$dataReader->readAll();
        $attemptedquestions='';
        foreach($attempted AS $key=>$attemp):
        $attemptedquestions.=($key==0?'':',').$attemp['question'];
        endforeach;        
         $sql="SELECT count(DISTINCT(a.TM_QN_Id)) AS attempted, b.TM_SN_Id,b.TM_SN_Name,b.TM_SN_Topic_Id,
        (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Section_Id=a.TM_QN_Section_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$attemptedquestions." )) AS completed,
                (
                    SELECT COUNT(DISTINCT(TM_QN_Id)) AS question 
            		FROM tm_question AS quest 
            		WHERE quest.TM_QN_Section_Id=b.TM_SN_Id 
            		AND quest.TM_QN_Parent_Id=0 
                ) AS total        
                FROM 
                tm_question AS a,tm_section AS b WHERE 
                a.TM_QN_Parent_Id=0 AND a.TM_QN_Section_Id=b.TM_SN_Id AND b.TM_SN_Syllabus_Id='".$plan->TM_PN_Syllabus_Id."' AND b.TM_SN_Standard_Id='".$plan->TM_PN_Standard_Id."' AND TM_QN_Id IN (".$correctquestions.") GROUP BY TM_QN_Section_Id HAVING completed > 0 AND attempted < completed ORDER BY attempted DESC,TM_SN_Id ASC LIMIT 5 ";
                                                              
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $topicstatus=$dataReader->readAll();
        //print_r($topicstatus); exit;                        
        return $topicstatus;                      
    } 
    public function actionGetschoolreststatus()
    {
        $progressstatus=$this->GetCompleteStatus($_POST['student'],$_POST['plan']);
        echo json_encode($progressstatus); 
    }                        
     /**
      * student school progress ends 
      */
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}