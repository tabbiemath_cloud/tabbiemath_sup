<?php

/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/7/2015
 * Time: 12:37 PM
 */
class StudentController extends Controller
{

    public $layout = '//layouts/main';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + deletetest,deletechallenge,Cancelchallenge,Deletemock', // mewe only allow deletion via POST request
            //'postOnly + copy', // we only allow copy via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('home', 'Revision', 'RevisionTest', 'getTopics', 'TestComplete', 'deletetest', 'starttest', 'GetSolution', 'Challenge', 'showpaper', 'Getpdf', 'SendMail', 'Mock', 'Deletemock', 'startmock', 'Mocktest', 'MockComplete', 'GetmockSolution', 'History', 'GetpdfAnswer', 'Invitebuddies', 'ChallengeGetSolution', 'Redo', 'RevisionRedo', 'GetchallengepdfAnswer', 'RedoComplete', 'GetRedoSolution', 'ChangePlan', 'Leaderboard', 'MockRedo', 'showmockpaper', 'SendMailReminders', 'GetChallengernames', 'GetpdfMockAnswer', 'Getpdfmock', 'Passwordchange','GetRedoSolution','GetpdfCertificate','uploadImageSave','uploadImage','GetChallengeBuddies','ProgressReport','GetChapterstatus','GetTopicStatus','GetRestStatus','GetMyStatus','homeworktest','Homeworkcomplete','Deletehomework','Getpdfhomework','UploadAnswersheet','Showhomeworkpaper','Homeworksubmit','GethomeworkSolution','SchoolLeaderboard','Schoolprogressreport','Getschoolchapterstatus','GetschooltopicStatus','Getschoolreststatus','Printworksheet','GetWorksheets','deleteWorksheet','Starthomework','HomeworkSubmitnow','HomeworkSubmitlater','Printmock','Printmocksolution','Viewworksheet','Download','Printresource','Resource','Printrevision'),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isStudent()'
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('Startchallenge', 'ChallengeTest', 'challengeresult', 'deletechallenge', 'Cancelchallenge', 'Acceptchallenge'),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isStudentChalange($_GET["id"])'
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('Markhomework','Solution','Answer','Savemarks','Markcomplete','SaveStatus','Completeallmarking','Sendreminder'),
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isStudentMarker($_GET["id"])'
            ),                        
            array('allow',
                'actions'=>array('Studentprogressreport','downloadprogress','Studentsummaryreport','Downloadsummary'),
                'users'=>UserModule::getSuperAdmins(),
                ),
            array('deny',  // deny all users
                'users' => array('*'),
            )
        );
    }


	public function actionHome()
    {
        if (Yii::app()->session['quick'] || Yii::app()->session['difficulty']):
            $studenttests = StudentTest::model()->findAll(array('condition' => "TM_STU_TT_Student_Id='" . Yii::app()->user->id . "' AND TM_STU_TT_Status !=3 AND TM_STU_TT_Plan='" . Yii::app()->session['plan'] . "'"));
        endif;
        if (Yii::app()->session['mock']):
            $studentmocks = StudentMocks::model()->with('mocks')->findAll(array('condition' => "TM_STMK_Student_Id='" . Yii::app()->user->id . "' AND TM_STMK_Status !=3 AND TM_MK_Status='0' AND  TM_STMK_Plan_Id='" . Yii::app()->session['plan'] . "'"));
        endif;
        if (Yii::app()->session['challenge']):
            $studentshallenges = Chalangerecepient::model()->with('challenge')->findAll(array('condition' => "TM_CE_RE_Recepient_Id='" . Yii::app()->user->id . "' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0' AND TM_CL_Plan='" . Yii::app()->session['plan'] . "'"));
        endif;
        if (Yii::app()->session['school']):
            $studenthomeworks = StudentHomeworks::model()->with('homework')->findAll(array('condition' => "TM_STHW_Student_Id='" . Yii::app()->user->id . "' AND TM_STHW_Status NOT IN (4,5,7) AND TM_SCH_Status='1' AND  TM_STHW_Plan_Id='" . Yii::app()->session['plan'] . "'"));
            $studentmarkings=Homeworkmarking::model()->with('homework')->findAll(array('condition'=>"TM_HWM_User_Id='".Yii::app()->user->id."' AND TM_HWM_Status=0"));
        endif;
        $this->render('userhome', array('studenttests' => $studenttests, 'challenges' => $studentshallenges, 'studentmocks' => $studentmocks,'studenthomeworks'=>$studenthomeworks,'studentmarkings'=>$studentmarkings));
    }

    public function actionPasswordchange()
    {
        $model = new UserChangePassword;
        if (Yii::app()->user->id) {

            // ajax validator
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'changepassword-form') {
                echo UActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['UserChangePassword'])) {
                $model->attributes = $_POST['UserChangePassword'];
                if ($model->validate()) {
                    $new_password = User::model()->findbyPk(Yii::app()->user->id);
                    $student = Student::model()->find(array('condition' => 'TM_STU_User_Id=' . Yii::app()->user->id));
                    $student->TM_STU_Password = $model->password;
                    $new_password->password = UserModule::encrypting($model->password);
                    $new_password->activkey = UserModule::encrypting(microtime() . $model->password);                   
                    $new_password->save(false);
                    $student->save(false);
                    Yii::app()->user->setFlash('profileMessage', UserModule::t("New password is saved."));
                }
            }
            $this->render('changepassword', array('model' => $model));
        }
    }

    public function actionRevision()
    {        
        $plan = $this->GetStudentDet();
        $chapters = Chapter::model()->findAll(array('condition' => "TM_TP_Syllabus_Id='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'",'order'=>'TM_TP_order ASC'));
        if (isset($_POST['startTest'])):

            if ($_POST['testtype'] == 'Difficulty' || $_POST['testtypeland']=='Difficulty'):            
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id', $_POST['chapter']);
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest = new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id = Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id = $_POST['publisher'];
                $StudentTest->TM_STU_TT_Type = '0';
                $StudentTest->TM_STU_TT_Plan = Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode = '0';
                $StudentTest->save(false);
                $count = 1;
                $totalquestions = 0;
                foreach ($selectedchapters AS $key => $chapter):
                    if ($_POST['chaptertotal' . $chapter->TM_TP_Id] != '0'):
                        $testchapter = new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id = Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id = $StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id = $chapter->TM_TP_Id;
                        $testchapter->save();
                        $topics = '';

                        for ($i = 0; $i < count($_POST['topic' . $chapter->TM_TP_Id]); $i++):
                            $topics = $topics . ($i == 0 ? $_POST['topic' . $chapter->TM_TP_Id][$i] : "," . $_POST['topic' . $chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';

                        $totalquestions = $totalquestions + $_POST['basic' . $chapter->TM_TP_Id] + $_POST['inter' . $chapter->TM_TP_Id] + $_POST['adv' . $chapter->TM_TP_Id];
                        if ($_POST['basic' . $chapter->TM_TP_Id] != '0'):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '1';
                            $limit = $_POST['basic' . $chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if ($_POST['inter' . $chapter->TM_TP_Id] != '0'):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '2';
                            $limit = $_POST['inter' . $chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if ($_POST['adv' . $chapter->TM_TP_Id] != '0'):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '3';
                            $limit = $_POST['adv' . $chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                    endif;
                endforeach;
                if ($count != '0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $selectedquestions = base64_encode('Remove_' . base64_encode($totalquestions));
                    $addedquestions = base64_encode('Remove_' . base64_encode($count - 1));
                    $this->redirect(array('starttest', 'id' => $StudentTest->TM_STU_TT_Id, 'inque' => $selectedquestions, 'outque' => $addedquestions));
                endif;
            elseif($_POST['testtype'] == 'Quick' || $_POST['testtypeland']=='Quick'):
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id', $_POST['quickchapter']);
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest = new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id = Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id = $_POST['publisher'];
                $StudentTest->TM_STU_TT_Type = '0';
                $StudentTest->TM_STU_TT_Plan = Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode = '1';
                $StudentTest->save(false);
                $count = 1;
                $totalquestions = 0;
                foreach ($selectedchapters AS $key => $chapter):
                    if ($_POST['quickchaptertotal' . $chapter->TM_TP_Id] != '0'):
                        $testchapter = new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id = Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id = $StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id = $chapter->TM_TP_Id;
                        $testchapter->save();
                        $quicklimits = $this->GetQuickLimits($_POST['quickchaptertotal' . $chapter->TM_TP_Id]);
                        $topics = '';
                        for ($i = 0; $i < count($_POST['quicktopic' . $chapter->TM_TP_Id]); $i++):
                            $topics = $topics . ($i == 0 ? $_POST['quicktopic' . $chapter->TM_TP_Id][$i] : "," . $_POST['quicktopic' . $chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions = $totalquestions + $_POST['quick' . $chapter->TM_TP_Id];
                        if ($quicklimits['basic'] != 0):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '1';
                            $limit = $quicklimits['basic'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,'0',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if ($quicklimits['intermediate'] != 0):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '2';
                            $limit = $quicklimits['intermediate'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,'0',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if ($quicklimits['advanced'] != 0):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '3';
                            $limit = $quicklimits['advanced'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,'0',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                    endif;
                endforeach;
                if ($count != '0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $selectedquestions = base64_encode('Remove_' . base64_encode($totalquestions));
                    $addedquestions = base64_encode('Remove_' . base64_encode($count - 1));
                    $this->redirect(array('starttest', 'id' => $StudentTest->TM_STU_TT_Id, 'inque' => $selectedquestions, 'outque' => $addedquestions));
                endif;
                
            endif;
        //$this->redirect(array('RevisionDifficulty','id'=>$StudentTest->TM_STU_TT_Id));
        elseif (isset($_POST['addTest'])):
            if ($_POST['testtype'] == 'Difficulty'):
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id', $_POST['chapter']);
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest = new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id = Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id = $_POST['publisher'];
                $StudentTest->TM_STU_TT_Type = '0';
                $StudentTest->TM_STU_TT_Plan = Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode = '0';
                $StudentTest->save(false);
                $count = 1;
                $totalquestions = 0;
                foreach ($selectedchapters AS $key => $chapter):
                    if ($_POST['chaptertotal' . $chapter->TM_TP_Id] != '0'):
                        $testchapter = new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id = Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id = $StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id = $chapter->TM_TP_Id;
                        $testchapter->save();
                        $topics = '';

                        for ($i = 0; $i < count($_POST['topic' . $chapter->TM_TP_Id]); $i++):
                            $topics = $topics . ($i == 0 ? $_POST['topic' . $chapter->TM_TP_Id][$i] : "," . $_POST['topic' . $chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';

                        $totalquestions = $totalquestions + $_POST['basic' . $chapter->TM_TP_Id] + $_POST['inter' . $chapter->TM_TP_Id] + $_POST['adv' . $chapter->TM_TP_Id];
                        if ($_POST['basic' . $chapter->TM_TP_Id] != '0'):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '1';
                            $limit = $_POST['basic' . $chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if ($_POST['inter' . $chapter->TM_TP_Id] != '0'):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '2';
                            $limit = $_POST['inter' . $chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                        if ($_POST['adv' . $chapter->TM_TP_Id] != '0'):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '3';
                            $limit = $_POST['adv' . $chapter->TM_TP_Id];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,0,@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                        endif;
                    endif;
                endforeach;
                if ($count != '0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $this->redirect(array('home'));
                endif;
            else:
                $criteria = new CDbCriteria;
                $criteria->addInCondition('TM_TP_Id', $_POST['quickchapter']);
                $criteria->addInCondition('TM_TP_Id', $_POST['quickchapter']);
                $selectedchapters = Chapter::model()->findAll($criteria);
                $StudentTest = new StudentTest();
                $StudentTest->TM_STU_TT_Student_Id = Yii::app()->user->id;
                $StudentTest->TM_STU_TT_Publisher_Id = $_POST['publisher'];
                $StudentTest->TM_STU_TT_Type = '0';
                $StudentTest->TM_STU_TT_Plan = Yii::app()->session['plan'];
                $StudentTest->TM_STU_TT_Mode = '1';
                $StudentTest->save(false);
                $count = 1;
                $totalquestions = 0;
                foreach ($selectedchapters AS $key => $chapter):
                    if ($_POST['quickchaptertotal' . $chapter->TM_TP_Id] != '0'):
                        $testchapter = new StudentTestChapters();
                        $testchapter->TM_STC_Student_Id = Yii::app()->user->id;
                        $testchapter->TM_STC_Test_Id = $StudentTest->TM_STU_TT_Id;
                        $testchapter->TM_STC_Chapter_Id = $chapter->TM_TP_Id;
                        $testchapter->save();
                        $quicklimits = $this->GetQuickLimits($_POST['quickchaptertotal' . $chapter->TM_TP_Id]);
                        $topics = '';
                        for ($i = 0; $i < count($_POST['quicktopic' . $chapter->TM_TP_Id]); $i++):
                            $topics = $topics . ($i == 0 ? $_POST['quicktopic' . $chapter->TM_TP_Id][$i] : "," . $_POST['quicktopic' . $chapter->TM_TP_Id][$i]);
                        endfor;
                        //$topics='"'.$topics.'"';
                        $totalquestions = $totalquestions + $_POST['quick' . $chapter->TM_TP_Id];
                        if ($quicklimits['basic'] != 0):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '1';
                            $limit = $quicklimits['basic'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,'0',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if ($quicklimits['intermediate'] != 0):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '2';
                            $limit = $quicklimits['intermediate'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,'0',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                        if ($quicklimits['advanced'] != 0):
                            $Syllabus = $plan->TM_SPN_SyllabusId;
                            $Standard = $plan->TM_SPN_StandardId;
                            $Chapter = $chapter->TM_TP_Id;
                            //$Topic=$topics;
                            $Test = $StudentTest->TM_STU_TT_Id;
                            $Student = Yii::app()->user->id;
                            $dificulty = '3';
                            $limit = $quicklimits['advanced'];
                            $command = Yii::app()->db->createCommand("CALL NewSetQuestions(:Syllabus,:Standard,:Chapter,'" . $topics . "',:limit,:Test,:Student,:dificulty,:questioncount,'0',@out)");
                            $command->bindParam(":Syllabus", $Syllabus);
                            $command->bindParam(":Standard", $Standard);
                            $command->bindParam(":Chapter", $Chapter);
                            $command->bindParam(":limit", $limit);
                            $command->bindParam(":Test", $Test);
                            $command->bindParam(":Student", $Student);
                            $command->bindParam(":dificulty", $dificulty);
                            $command->bindParam(":questioncount", $count);
                            $command->query();
                            $count = $count + Yii::app()->db->createCommand("select @out as result;")->queryScalar();

                        endif;
                    endif;
                endforeach;
                if ($count != '0'):
                    $command = Yii::app()->db->createCommand("CALL SetQuestionsNum(:Test)");
                    $command->bindParam(":Test", $StudentTest->TM_STU_TT_Id);
                    $command->query();
                    $this->redirect(array('home'));
                endif;
            endif;
        endif;

        $this->render('revision', array('plan' => $plan, 'chapters' => $chapters));
    }

    public function actionStarttest($id)
    {
        $test = StudentTest::model()->findByPk($id);
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=' . $id . ' AND TM_STU_QN_Type NOT IN (6,7)';
        $totalonline = StudentQuestions::model()->count($criteria);
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=' . $id . ' AND TM_STU_QN_Type IN (6,7)';
        $totalpaper = StudentQuestions::model()->count($criteria);
        $totalquestions = $totalonline + $totalpaper;
        if($totalquestions==$totalpaper)
        {
            $test->TM_STU_TT_Status = '2';
            $test->save(false);
        }
        //$selectedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['inque'])));
        //$addedque=base64_decode(str_replace("Remove_","",base64_decode($_GET['outque'])));
        $this->render('starttest', array('id' => $id, 'test' => $test, 'totalonline' => $totalonline, 'totalpaper' => $totalpaper, 'totalquestions' => $totalquestions));

    }

    public function actionStartchallenge($id)
    {
        $test = Challenges::model()->findByPk($id);
        $this->render('startchallenge', array('id' => $id, 'test' => $test));

    }

    public function actionRevisionTest($id)
    {
         if (isset($_POST['action']['Save'])) {

        if ($_POST['QuestionType'] != '5'):
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
            $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
            $selectedquestion = StudentQuestions::model()->find($criteria);
            if ($_POST['QuestionType'] == '1'):
                $answer = implode(',', $_POST['answer']);
                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
            elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                $answer = $_POST['answer'];
                $selectedquestion->TM_STU_QN_Answer_Id = $answer;
            elseif ($_POST['QuestionType'] == '4'):
                $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
            endif;
            $selectedquestion->TM_STU_QN_Flag = '1';
            $selectedquestion->save(false);
        else:
            $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
            foreach ($questions AS $child):
                $childquestion = StudentQuestions::model()->findByAttributes(
                    array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                );
                if (count($childquestion) == '1'):
                    $selectedquestion = $childquestion;
                else:
                    $selectedquestion = new StudentQuestions();
                endif;
                $selectedquestion->TM_STU_QN_Test_Id = $id;
                $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                if ($child->TM_QN_Type_Id == '1'):
                    $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                    $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                    $answer = $_POST['answer' . $child->TM_QN_Id];
                    $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                elseif ($child->TM_QN_Type_Id == '4'):
                    $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                endif;
                $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                $selectedquestion->TM_STU_QN_Flag = '1';
                $selectedquestion->save(false);
            endforeach;
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
            $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
            $selectedquestion = StudentQuestions::model()->find($criteria);
            $selectedquestion->TM_STU_QN_Flag = '1';
            $selectedquestion->save(false);
        endif;
        $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
            array(
                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                'limit' => 1,
                'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber']-1)
            )
        );
    }
        if (isset($_POST['action']['GoNext'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STU_QN_Flag = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = StudentQuestions::model()->findByAttributes(
                        array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new StudentQuestions();
                    endif;
                    $selectedquestion->TM_STU_QN_Test_Id = $id;
                    $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->TM_STU_QN_Flag = '1';
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                )
            );
        }
        if (isset($_POST['action']['GetPrevios'])):
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0,1,2) AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Number<:tableid AND TM_STU_QN_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number DESC, TM_STU_QN_Flag ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                )
            );
        endif;
        if (isset($_POST['action']['DoLater'])):
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag = '2';
                $selectedquestion->save(false);
            endif;
            $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                )
            );
        endif;
        if (isset($_POST['action']['Skipcomplete'])):
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_Flag = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_Flag = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag = '2';
                $selectedquestion->save(false);
            endif;
            $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                )
            );
            if ($this->GetPaperCount($id) > 0):
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '1';
                $test->save(false);
                $this->redirect(array('showpaper', 'id' => $id));
            else:
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '2';
                $test->save(false);
                $this->redirect(array('TestComplete', 'id' => $id));
            endif;
        endif;
        if (isset($_POST['action']['Complete'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    if(count($_POST['answer'])>0):
                        $answer = implode(',', $_POST['answer']);                    
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    endif;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STU_QN_Flag = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = StudentQuestions::model()->findByAttributes(
                        array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new StudentQuestions();
                    endif;
                    $selectedquestion->TM_STU_QN_Test_Id = $id;
                    $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_Flag = '1';
                $selectedquestion->save(false);
            endif;
            
            if ($this->GetPaperCount($id) > 0):
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '2';
                $test->save(false);
                $this->redirect(array('showpaper', 'id' => $id));
            else:
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '3';
                $test->save(false);
                $this->redirect(array('TestComplete', 'id' => $id));
            endif;
            //$this->redirect(array('TestComplete','id'=>$id));
        }
        if (!isset($_POST['action'])):
            if (isset($_GET['questionnum'])):
                $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                    array(
                        'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0,1,2) AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                        'limit' => 1,
                        'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            else:
                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '1';
                $test->save(false);
                $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                    array(
                        'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Flag IN (0) AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Parent_Id=0',
                        'limit' => 1,
                        'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_Flag ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            endif;
        endif;
        if (count($questionid)):
            $question = Questions::model()->findByPk($questionid->TM_STU_QN_Question_Id);
            $this->renderPartial('showquestion', array('testid' => $id, 'question' => $question, 'questionid' => $questionid));
        else:
            $this->redirect(array('revision'));
        endif;
    }


    public function actionTestComplete($id)
    {
        $nonpaperquestions = $this->GetNonPaperQuestions($id);
        $countnon = count($nonpaperquestions);

        if ($countnon != 0) {

            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7)',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                )
            );
            $totalmarks = 0;
            $earnedmarks = 0;
            $result = '';
            $marks = 0;
            foreach ($results AS $exam):
                $question = Questions::model()->findByPk($exam->TM_STU_QN_Question_Id);
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5'):
                        $displaymark = 0;
                        $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marksdisp AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;

                        endforeach;
                        $marks = 0;
                        if ($question->TM_QN_Type_Id != '4'):
                            if ($exam->TM_STU_QN_Answer_Id != ''):

                                $studentanswer = explode(',', $exam->TM_STU_QN_Answer_Id);
                                foreach ($question->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++) {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($exam->TM_STU_QN_Answer != ''):
                                $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                                $correctanswer .= '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_STU_QN_Answer . '</div></li></ul>';
                                foreach ($question->answers AS $ans):
                                    $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer = trim(strtolower($exam->TM_STU_QN_Answer));
                                    for ($i = 0; $i < count($answeroption); $i++) {
                                        $option = trim($answeroption[$i]);
                                        //fix for < >
                                        if($option=='&gt'):
                                            $newoption='&gt;';
                                        elseif($option=='&lt'):
                                            $newoption='&lt;';
                                        else:
                                            $newoption=$option;
                                        endif;
                                        if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($question->TM_QN_Parent_Id == 0):
                            $result .= '<tr><td><div class="col-md-4">';
                            if ($marks == '0'):
                                $exam->TM_STU_QN_Redo_Flag = '1';
                                $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';

                            elseif ($marks < $displaymark & $marks != '0'):
                                $exam->TM_STU_QN_Redo_Flag = '1';
                                $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STU_QN_Number . '</span>';
                            else:
                                $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                            endif;

                            $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? ' fa fa-flag sty' : ' fa fa-flag stynone') . ' newtest" data-toggle="tooltip" data-placement="left" title="' . ($marks != $displaymark ? '  Unflag question.' : '  Flag question.') . '"></span></a>      Mark: ' . $marks . '/' . $displaymark . '</div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                            //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                            $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                            $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $result .= ' <div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div>
                            <div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div>';
                            $result .= '<div class="col-md-12 Itemtest clickable-row showalert' . $question->TM_QN_Id . '" >

								<div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2 mailadd" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                              </div>
                                 <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id=""  hidden >Your message has been sent to admin
                                </div>  </div>
                             </td></tr>';

                            //glyphicon glyphicon-star sty
                        endif;

                        $exam->TM_STU_QN_Mark = $marks;
                        $exam->save(false);
                        $earnedmarks = $earnedmarks + $marks;

                    else:
                        $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $displaymark = 0;
                        $singlemark = '';
                        foreach ($marksQuestions AS $key => $formarks):
                            $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                            foreach ($gettingmarks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                                $singlemark = $marksdisplay->TM_AR_Marks;
                            endforeach;
                        endforeach;

                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $count = 1;
                        $childresult = '';
                        $childearnedmarks = 0;
                        foreach ($questions AS $childquestion):
                            $childresults = StudentQuestions::model()->find(
                                array(
                                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Question_Id=:questionid',
                                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                                )
                            );
                            $marks = 0;
                            if ($childquestion->TM_QN_Type_Id != '4'):
                                if ($childresults->TM_STU_QN_Answer_Id != ''):

                                    $studentanswer = explode(',', $childresults->TM_STU_QN_Answer_Id);
                                    foreach ($childquestion->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    if (count($studentanswer) > 0):
                                        $correctanswer = '<ul class="list-group"><lh><b>Your Answer:</b></lh>';
                                        for ($i = 0; $i < count($studentanswer); $i++) {
                                            $answer = Answers::model()->findByPk($studentanswer[$i]);
                                            $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                        }
                                        $correctanswer .= '</ul>';
                                    endif;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                                endif;
                            else:
                                if ($childresults->TM_STU_QN_Answer != ''):
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_STU_QN_Answer . '</li></ul>';
                                    foreach ($childquestion->answers AS $ans):
                                        $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                        $useranswer = trim(strtolower($childresults->TM_STU_QN_Answer));
                                        for ($i = 0; $i < count($answeroption); $i++) {
                                            $option = trim($answeroption[$i]);
                                            //fix for < >
                                            if($option=='&gt'):
                                                $newoption='&gt;';
                                            elseif($option=='&lt'):
                                                $newoption='&lt;';
                                            else:
                                                $newoption=$option;
                                            endif;
                                            if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0):
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            endif;
                                        }
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                                endif;

                            endif;
                            if ($childquestion->TM_QN_Parent_Id != 0):
                                $childresult .= '<div class="col-md-12">';
                                if ($marks == '0'):
                                    //$exam->TM_STHWQT_Redo_Flag = '1';
                                    $childresult .= '<span class="clrr"><i class="fa fa fa-times"></i><b> Part ' . $count . '</b></span>';
                                elseif ($marks < $singlemark & $marks != '0'):
                                    //$exam->TM_STHWQT_Redo_Flag = '1';
                                    $childresult .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"><b> Part ' . $count . '</b></span>';
                                else:
                                    $childresult .= '<span ><i class="fa fa fa-check"></i><b> Part ' . $count . '</b></span>';
                                endif;
                                $childresult.=' Mark: ' . $marks . '/' . $singlemark . '';
                                $childresult.='<li class="list-group-item1" style="display:flex ;border:none;">
                                <div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($childquestion->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                                $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                                $childresult .= '';
                                //glyphicon glyphicon-star sty
                            endif;


                            $earnedmarks = $earnedmarks + $marks;
                            $childearnedmarks = $childearnedmarks + $marks;
                            $count++;
                        endforeach;
                        $marks = $childearnedmarks;
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STU_QN_Number . '</span>';
                        else:
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STU_QN_Number . '</span>';
                        endif;

                        $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? ' fa fa-flag sty' : ' fa fa-flag stynone') . ' newtest" data-toggle="tooltip" data-placement="left" title="' . ($marks != $displaymark ? '  Unflag question.' : '  Flag question.') . '"></span></a>    Mark: ' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= $childresult;
                        $result.= '<div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div>
                        <div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div></td></tr>';
                        $result .= '<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div>

                            </div></div>
</td></tr>';
                        $exam->TM_STU_QN_Mark = $marks;
                        $exam->save(false);
                    endif;


                endif;

            endforeach;
            $percentage = ($earnedmarks / $totalmarks) * 100;
            $test = StudentTest::model()->findByPk($id);
            $test->TM_STU_TT_Mark = $earnedmarks;
            $test->TM_STU_TT_TotalMarks = $totalmarks;
            $test->TM_STU_TT_Percentage = round($percentage, 1);
            $test->TM_STU_TT_Status = '3';
            $test->save(false);
            $pointlevels = $this->AddPoints($earnedmarks, $id, 'Revision', Yii::app()->user->id);
            $this->render('showresult', array('test' => $test, 'testresult' => $result, 'pointlevels' => $pointlevels));
        } else {
            $test = StudentTest::model()->findByPk($id);
            $test->TM_STU_TT_Status = '3';
            $test->save(false);
            $result = '<h1 class="h125" style="text-align: center">This is a paper only test. Hence there is no automated correction.<br> Click on view solutions to check the answers. </h1>';
            $this->render('showpaperonlyresult', array('test' => $test, 'testresult' => $result));
        }
        

    }

    public function actionRedoComplete($id)
    {
        $test = StudentTest::model()->findByPk($id);
        if ($test->TM_STU_TT_Redo_Action == 'all'):
            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7)',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                )
            );
        else:
            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7) AND TM_STU_QN_Redo_Flag=1',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                )
            );
        endif;
        $redosolution=$this->RedoSolutionStatic($id);        
        $totalmarks = 0;
        $earnedmarks = 0;
        $result = '';
        $resultsolution = '';
        $marks = 0;
        $questionnumber=1;
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == 0):
                if ($question->TM_QN_Type_Id != '5'):
                    $displaymark = 0;
                    $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marksdisp AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;

                    endforeach;
                    $marks = 0;
                    if ($question->TM_QN_Type_Id != '4' & $exam->TM_STU_QN_Flag != '2'):
                        if ($exam->TM_STU_QN_Answer_Id != ''):

                            $studentanswer = explode(',', $exam->TM_STU_QN_Answer_Id);
                            foreach ($question->answers AS $ans):
                                if ($ans->TM_AR_Correct == '1'):
                                    if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    }
                                endif;
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                                for ($i = 0; $i < count($studentanswer); $i++) {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);
                                    $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($exam->TM_STU_QN_Answer != ''):
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_STU_QN_Answer . '</div></li></ul>';
                            foreach ($question->answers AS $ans):
                                $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                $useranswer = trim(strtolower($exam->TM_STU_QN_Answer));
                                for ($i = 0; $i < count($answeroption); $i++) {
                                    $option = trim($answeroption[$i]);
                                    if (strcmp($useranswer, $option) == 0):
                                        $marks = $marks + $ans->TM_AR_Marks;
                                    endif;
                                }
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                        else:
                            foreach ($question->answers AS $ans):
                                $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                            endforeach;
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;

                    endif;
                    if ($question->TM_QN_Parent_Id == 0):
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $exam->TM_STU_QN_Flag = '0';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $questionnumber . '</span>';

                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_STU_QN_Redo_Flag = '1';
                            $exam->TM_STU_QN_Flag = '0';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question ' . $questionnumber . '</span>';
                        else:
                            $exam->TM_STU_QN_Redo_Flag = '0';
                            $exam->TM_STU_QN_Flag = '1';
                            $exam->TM_STU_QN_RedoStatus = '0';
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $questionnumber . '</span>';
                        endif;

                        $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? 'fa fa-flag sty' : 'fa fa-flag stynone') . ' newtest"  data-toggle="tooltip" data-placement="left" title="' . ($marks != $displaymark ? '  Unflag question.' : '  Flag question.') . '" ></span></a>      Mark: ' . $marks . '/' . $displaymark . '</div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-8"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                        $result .= '<div class="col-md-12 Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comment"name="comments"  id="comment' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2 mailadd" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                 <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div>
                            </div></div>
                             </td></tr>';

                        //glyphicon glyphicon-star sty
                    endif;

                    $exam->TM_STU_QN_Mark = $marks;
                    $exam->TM_STU_QN_RedoStatus = 0;
                    $exam->save(false);
                    $earnedmarks = $earnedmarks + $marks;

                else:
                    $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                    $displaymark = 0;
                    foreach ($marksQuestions AS $key => $formarks):

                        $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($gettingmarks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                    endforeach;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childresult = '';
                    $childearnedmarks = 0;
                    $childresulthtml = '';
                    foreach ($questions AS $childquestion):
                        $childresults = StudentQuestions::model()->find(
                            array(
                                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Question_Id=:questionid',
                                'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                            )
                        );
                        $marks = 0;
                        if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_STU_QN_Flag != '2'):
                            if ($childresults->TM_STU_QN_Answer_Id != ''):

                                $studentanswer = explode(',', $childresults->TM_STU_QN_Answer_Id);
                                foreach ($childquestion->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group"><lh><b>Your Answer:</b></lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++) {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($childresults->TM_STU_QN_Answer != ''):
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_STU_QN_Answer . '</li></ul>';
                                foreach ($childquestion->answers AS $ans):
                                    $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer = trim(strtolower($childresults->TM_STU_QN_Answer));
                                    for ($i = 0; $i < count($answeroption); $i++) {
                                        $option = trim($answeroption[$i]);
                                        if (strcmp($useranswer, $option) == 0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($childquestion->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                            endif;

                        endif;
                        if ($childquestion->TM_QN_Parent_Id != 0):
                            $childresult .= '

                            <div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                            $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $childresult .= '';
                            //glyphicon glyphicon-star sty
                        endif;

                        $childresulthtml .= '<div class="col-md-12 ">Part :' . $count . '</div>';
                        $childresulthtml .= '<div class="col-md-12 padnone">';
                        $childresulthtml .= '<div class="col-md-' . ($childquestion->TM_QN_Image != '' ? '10' : '12') . '">' . $childquestion->TM_QN_Question . '</div>' . ($childquestion->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                        $childresulthtml .= '</div>';
                        $earnedmarks = $earnedmarks + $marks;
                        $childearnedmarks = $childearnedmarks + $marks;
                        $count++;
                    endforeach;


                    $marks = $childearnedmarks;
                    $result .= '<tr><td><div class="col-md-4">';
                    if ($marks == '0'):
                        $exam->TM_STU_QN_Redo_Flag = '1';
                        $exam->TM_STU_QN_Flag = '0';
                        $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $questionnumber . '</span>';
                    elseif ($marks < $displaymark & $marks != '0'):
                        $exam->TM_STU_QN_Redo_Flag = '1';
                        $exam->TM_STU_QN_Flag = '0';
                        $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question ' . $questionnumber . '</span>';
                    else:
                        $exam->TM_STU_QN_Redo_Flag = '0';
                        $exam->TM_STU_QN_Flag = '1';
                        $exam->TM_STU_QN_RedoStatus = '0';
                        $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $questionnumber. '</span>';
                    endif;

                    $result .= ' <a  href="javascript:void(0)"  class="startoggle" data-questionId="' . $question->TM_QN_Id . '" data-questionTypeId="' . $question->TM_QN_Type_Id . '" data-testId="' . $exam->TM_STU_QN_Test_Id . '"><span class="glyphicon ' . ($marks != $displaymark ? 'fa fa-flag sty' : 'fa fa-flag stynone') . ' newtest"  data-toggle="tooltip" data-placement="left" title="' . ($marks != $displaymark ? '  Unflag question.' : '  Flag question.') . '" ></span></a>    Mark: ' . $marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                            <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                    //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                    $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-8"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                    $result .= $childresult . '</td></tr>';
                    $result .= '<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comments"  id="comment' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div>

                            </div></div>
                            </td></tr>';
                    $exam->TM_STU_QN_Mark = $marks;
                    $exam->save(false);
                endif;


            endif;
            $questionnumber++;
        endforeach;

        $test = StudentTest::model()->findByPk($id);
        $test->TM_STU_TT_Status = '3';
        $test->save(false);

        $this->render('showresultredo', array('test' => $test, 'testresult' => $result, 'solution' => $resultsolution,'redosolution'=>$redosolution));
    } 
    public function RedoSolutionStatic($id)
    {
        $test = StudentTest::model()->findByPk($id);
        if ($test->TM_STU_TT_Redo_Action == 'all'):
            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Type NOT IN(6,7)',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                )
            );
        else:
            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Type NOT IN(6,7) AND TM_STU_QN_Redo_Flag=1',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                )
            );
        endif;
        $resulthtml='';
        $questionnumbersol=1;
        foreach ($results AS $result):
            $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
            $displaymark = 0;
            foreach ($marks AS $key => $marksdisplay):
                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
            endforeach;

            if ($question->TM_QN_Type_Id != '5'):

                $resulthtml .= '<tr><td>
					<div class="col-md-3">
                    <span> <b>Question ' . $questionnumbersol . '</b></span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                    </div>
					<div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '
					</div>';
                $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                $resulthtml .= '<div class="col-md-12 padnone">';

                if ($question->TM_QN_Solutions != ''):
                    $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                    if ($question->TM_QN_Solution_Image != ''):
                        $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt="">
							</div>';
                    endif;
                else:
                    $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt="">
						</div>';
                endif;
                $resulthtml .= '</div>';

                $resulthtml .= '<div class="col-md-12 Itemtest clickable-row "   >
                            <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslide"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2 " style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                 <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div>
                            </div></div>
                             </td></tr>';

            elseif ($question->TM_QN_Type_Id == '5'):
                $displaymark = 0;
                $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                $countmulti = 1;
                $displaymark = 0;
                $childresulthtml = '';
                foreach ($multiQuestions AS $key => $chilquestions):

                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marks AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                    $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                    $childresulthtml .= '<div class="col-md-12 padnone">';
                    $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                    $childresulthtml .= '</div>';
                    $countmulti++;
                endforeach;

                $resulthtml .= '<tr><td>
						<div class="col-md-3">
                    <span><b>Question ' . $questionnumbersol . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
					</div>';

                $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                $resulthtml .= '<div class="col-md-12 padnone">';
                if ($question->TM_QN_Solutions != ''):
                    $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                    if ($question->TM_QN_Solution_Image != ''):
                        $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                    endif;
                else:
                    $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                endif;
                $resulthtml .= '</div></td></tr>';



                $resulthtml .= '<tr><td><div class="Itemtest clickable-row "   >
                            <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslide"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right mailSend"id="" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                                <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright"  hidden >Your message has been sent to admin
                                </div>

                            </div></div>
                            </td></tr>';
            endif;
        $questionnumbersol++;
        endforeach;

        return $resulthtml;
    }  
    public function actionGetRedoSolution(){
        if (isset($_POST['testid'])):
            $id=$_POST['testid'];
            $test = StudentTest::model()->findByPk($id);
            if ($test->TM_STU_TT_Redo_Action == 'all'):
                $results = StudentQuestions::model()->findAll(
                    array(
                        'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7)',
                        "order" => 'TM_STU_QN_Number ASC',
                        'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                    )
                );
            else:
                $results = StudentQuestions::model()->findAll(
                    array(
                        'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN(6,7) AND TM_STU_QN_Redo_Flag=1',
                        "order" => 'TM_STU_QN_Number ASC',
                        'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                    )
                );
            endif;
            $resulthtml = '';
            foreach ($results AS $result):
                $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '6' && $question->TM_QN_Type_Id != '7'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span> <b>Question' . $result->TM_STU_QN_Number . '</b></span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                    </div><div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                              <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>

                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '6'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                        <span> <b>Question' . $result->TM_STU_QN_Number . '</b></span>
                        <span class="pull-right">Marks: ' . $question->TM_QN_Totalmarks . '</span>
                        </div>
                        <div class="col-md-3 pull-right ">
                       <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                        <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                        </div><div class="col-md-12 padnone">
                        <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                                  <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                               </div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                               </div>
                               </div>
                                <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                    </div></div>

                            </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $displaymark = $displaymark + $chilquestions->TM_QN_Totalmarks;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question' . $result->TM_STU_QN_Number . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '5'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $displaymark = 0;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question' . $result->TM_STU_QN_Number . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';

                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function actionGetpdf($id)
    {
        $paperquestions = $this->GetPaperQuestions($id);
        $totalquestions = count($paperquestions);
        $nak = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;

        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>PAPER TEST<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";


        foreach ($paperquestions AS $key => $paperquestion):

            $question = Questions::model()->findByPk($paperquestion->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):

                if ($question->TM_QN_Type_Id != '7'):                   
                    $html .= "<tr><td width='15%'><b>Question ".$paperquestion->TM_STU_QN_Number."</b></td><td width='15%'> Marks: ".$question->TM_QN_Totalmarks."</td><td align='right' width='70%'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3'  width='100%'>".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;

                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question ".$paperquestion->TM_STU_QN_Number."</b></td><td width='15%'> Marks: ".$displaymark."</td><td align='right' width='70%'>Ref : " . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
            endif;
        endforeach;
        $html .= "</table>";
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;  color: #afafaf; ">' . $firstname . ' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf; ">' . $username . '</td>
</tr></table>';
        /*require_once (dirname(__FILE__).'/ckeditor/plugins/ckeditor_wiris/integration/pluginbuilder.php');
        $text = $pluginBuilder->newTextService();        
        $params = ["dpi"=>"500"];
        $html = $text->filter($html, $params);*/

        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Educational Solutions PVT Ltd.</span></td>
</tr></table>
');
        $name='PAPER_TEST_'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

    }

    public function actionGetSolution()
    {
        if (isset($_POST['testid'])):
            $results = StudentQuestions::model()->findAll(
                array(
                    'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test',
                    "order" => 'TM_STU_QN_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $_POST['testid'])
                )
            );
            $resulthtml = '';
            foreach ($results AS $result):
                $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '6' && $question->TM_QN_Type_Id != '7'):
                        $resulthtml .= '<tr><td><div class="col-md-4">
                    <span> <b>Question' . $result->TM_STU_QN_Number . '</b></span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                    </div><div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                            $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                        endif;                            
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                              <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>

                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '6'):
                        $resulthtml .= '<tr><td><div class="col-md-4">
                        <span> <b>Question' . $result->TM_STU_QN_Number . '</b></span>
                        <span class="pull-right">Marks: ' . $question->TM_QN_Totalmarks . '</span>
                        </div>
                        <div class="col-md-3 pull-right ">
                       <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                        <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                        </div><div class="col-md-12 padnone">
                        <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                            $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                        endif;  
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                                  <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                               </div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                               </div>
                               </div>
                                <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                    </div></div>
    
                            </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $displaymark = $displaymark + $chilquestions->TM_QN_Totalmarks;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-4">
                    <span><b>Question' . $result->TM_STU_QN_Number . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                            if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                                $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                            endif;  
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '5'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $displaymark = 0;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-4">
                    <span><b>Question' . $result->TM_STU_QN_Number . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                            if ($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-10">' . $question->TM_QN_Solutions . '</div>';
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            elseif($question->TM_QN_Solutions == '' & $question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            elseif($question->TM_QN_Solutions != '' & $question->TM_QN_Solution_Image == ''):
                                $resulthtml .= '<div class="col-md-12">' . $question->TM_QN_Solutions . '</div>';
                            endif;  
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';

                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }

    public function actionGetmockSolution()
    {
        if (isset($_POST['testid'])):
            $results = Studentmockquestions::model()->findAll(
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $_POST['testid']),
                    'order'=>'TM_STMKQT_Order ASC'
                )
            );
            $resulthtml = '';
            foreach ($results AS $questnumberkey => $result):
                $questnumber = $questnumberkey + 1;
                $question = Questions::model()->findByPk($result->TM_STMKQT_Question_Id);

                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '6' && $question->TM_QN_Type_Id != '7'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span> <b>Question' . $questnumber . '</b></span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                    </div><div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                              <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>

                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '6'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                        <span> <b>Question' . $questnumber . '</b></span>
                        <span class="pull-right">Marks: ' . $question->TM_QN_Totalmarks . '</span>
                        </div>
                        <div class="col-md-3 pull-right ">
                       <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                        <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                        </div><div class="col-md-12 padnone">
                        <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                                  <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                               </div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                               </div>
                               </div>
                                <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                    </div></div>
    
                            </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $displaymark = $displaymark + $chilquestions->TM_QN_Totalmarks;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question' . $questnumber . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '5'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $displaymark = 0;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question' . $questnumber . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';

                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }

    public function actionShowpaper($id)

    {        
        $connection = CActiveRecord::getDbConnection();
        if (isset($_POST['coomplete'])) {
                $nonpaperquestions = $this->GetNonPaperQuestions($id);
                $countnon = count($nonpaperquestions);
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type IN (6,7) AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':test' => $id);                
                $paperquestions = StudentQuestions::model()->findAll($criteria);                
                foreach($paperquestions AS $paper):                    
                    $paper->TM_STU_QN_Flag=1;
                    $paper->save(false);
                endforeach;

                $test = StudentTest::model()->findByPk($id);
                $test->TM_STU_TT_Status = '2';                
                $test->save(false);                
                $this->redirect(array('TestComplete', 'id' => $id));
            }




        $paperquestions = $this->GetPaperQuestions($id);


        $this->render('showpaperquestion', array('id' => $id, 'paperquestions' => $paperquestions));

    }

    public function GetStudentDet()
    {
        $user = Yii::app()->user->id;
        $plan = StudentPlan::model()->findAll(array('condition' => 'TM_SPN_Publisher_Id=:publisher AND TM_SPN_PlanId=:plan AND TM_SPN_SyllabusId=:syllabus AND TM_SPN_StandardId=:standard AND TM_SPN_SubjectId=:subject AND TM_SPN_StudentId=:user',
            'params' => array(':publisher' => Yii::app()->session['publisher'], ':plan' => Yii::app()->session['plan'], ':syllabus' => Yii::app()->session['syllabus'], ':standard' => Yii::app()->session['standard'], ':subject' => Yii::app()->session['subject'], ':user' => $user)));
        return $plan[0];
    }

    public function actionGetChallengernames()
    {

        $arr = $_POST['nameids'];
        $name = '';
        if(count($arr)>0):
            for ($i = 0; $i < count($arr); $i++) {                
                $name .= ($i==0?User::model()->findbyPk($arr[$i])->username:",".User::model()->findbyPk($arr[$i])->username);
            }            
        endif;
        echo $name;
    }

    public function actionGetTopics()
    {
        $arr = explode(',', $_POST['topicids']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_TP_Id', $arr);
        $model = Chapter::model()->findAll($criteria);
        $topic = '';
        foreach ($model as $key => $value) {
            $topic = ($key == '0' ? $value->TM_TP_Name : $topic . ',' . $value->TM_TP_Name);
        }
        echo $topic;
    }

    public function GetPaperQuestions($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type IN (6,7) AND TM_STU_QN_Parent_Id=0';
        $criteria->params = array(':test' => $id);
        $criteria->order = 'TM_STU_QN_Number ASC';
        $selectedquestion = StudentQuestions::model()->findAll($criteria);
        return $selectedquestion;
    }

    public function GetNonPaperQuestions($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Parent_Id=0';
        $criteria->params = array(':test' => $id);
        $criteria->order = 'TM_STU_QN_Number ASC';
        $nonpaperquestions = StudentQuestions::model()->findAll($criteria);
        return $nonpaperquestions;
    }

    public function GetPaperQuestionsMock($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type IN (6,7) AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Student_Id=:student';
        $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
        $criteria->order = 'TM_STMKQT_Order ASC';
        $selectedquestion = Studentmockquestions::model()->findAll($criteria);
        return $selectedquestion;
    }

    public function OptionHint($type, $question)
    {
        $returnval = array();
        if ($type == '1'):
            $corectans = Answers::model()->count(
                array(
                    'condition' => 'TM_AR_Question_Id=:question AND TM_AR_Correct=:correct',
                    'params' => array(':question' => $question, ':correct' => '1')
                ));
            $returnval['hint'] = '<span class="optionhint">Select any ' . $corectans . ' options</span>';
            $returnval['correctoptions'] = $corectans;
        elseif ($type == '2'):
            $returnval['hint'] = '<span class="optionhint">Select an option</span>';
            $returnval['correctoptions'] = '1';
        elseif ($type == '3'):
            $returnval['hint'] = '<span class="optionhint">Select either True Or False</span>';
            $returnval['correctoptions'] = '1';
        elseif ($type == '4'):
            $returnval['hint'] = '<span class="optionhint">Enter Your Answer</span>';
            $returnval['correctoptions'] = '0';
        elseif ($type == '5'):
            $returnval['hint'] = '<span class="optionhint">Answer the following questions</span>';
            $returnval['correctoptions'] = '0';
        endif;
        return $returnval;
    }
    public function GetCountChallengeQuestion($challenge)
    {
        $count=Chalangequestions::model()->count(array('condition'=>'TM_CL_QS_Chalange_Id='.$challenge));
        return  $count;
    }
    public function actionDeletetest($id)
    {
        $returnarray=array();        
        if (StudentTest::model()->findByPk($id)->delete()) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STC_Test_Id=' . $id);
            $model = StudentTestChapters::model()->deleteAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_QN_Test_Id=' . $id);
            $model = StudentQuestions::model()->deleteAll($criteria);
            $testcount=StudentTest::model()->count(array('condition'=>"TM_STU_TT_Student_Id='".Yii::app()->user->id."' AND TM_STU_TT_Status !=3 AND TM_STU_TT_Plan='" . Yii::app()->session['plan'] . "'"));
            $returnarray['error']='no';
            $returnarray['testcount']=$testcount;                                    
        } else {
           $returnarray['error']='yes';
        }
        echo json_encode($returnarray);
    }

    public function actionDeletechallenge($id)
    {        
        $returnarray=array();
        $challenge = Challenges::model()->findByPk($id);
        if ($challenge->TM_CL_Chalanger_Id == Yii::app()->user->id):
            if($this->GetChallangeInviteStatus($id)>0):
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $id . ' AND TM_CE_RE_Recepient_Id=' . Yii::app()->user->id);
                $model = Chalangerecepient::model()->find($criteria);
                $model->TM_CE_RE_Status = '4';
                if ($model->save(false)):
                    $criteria = new CDbCriteria;
                    $criteria->addCondition('TM_CEUQ_Challenge=' . $id . ' AND TM_CEUQ_User_Id=' . Yii::app()->user->id);
                    $model = Chalangeuserquestions::model()->deleteAll($criteria);
                    $testcount=Chalangerecepient::model()->with('challenge')->count(array('condition' => "TM_CE_RE_Recepient_Id='" . Yii::app()->user->id . "' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0' AND TM_CL_Plan='" . Yii::app()->session['plan'] . "'"));
                    $returnarray['error']='no';
                    $returnarray['testcount']=$testcount; 
                else:
                    $returnarray['error']='yes';
                endif;            
            else:            
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CL_QS_Chalange_Id=' . $id);
                $model = Chalangequestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CEUQ_Challenge=' . $id);
                $model = Chalangeuserquestions::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition('TM_CE_CP_Challenge_Id=' . $id);
                $model = ChallengeChapters::model()->deleteAll($criteria);
                $criteria = new CDbCriteria;
                $criteria->addCondition("TM_NT_Item_Id='". $id."' AND (TM_NT_Type='Challenge' OR TM_NT_Type='Reminder')");
                $model = Notifications::model()->deleteAll($criteria);                
                $challenge->TM_CL_Status = '1';
                $challenge->save(false);
                $testcount=Chalangerecepient::model()->with('challenge')->count(array('condition' => "TM_CE_RE_Recepient_Id='" . Yii::app()->user->id . "' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0' AND TM_CL_Plan='" . Yii::app()->session['plan'] . "'"));
                $returnarray['error']='no';
                $returnarray['testcount']=$testcount;                 
            endif;
        else:
            $returnarray['error']='yes';
        endif;
        echo json_encode($returnarray);
    }

    public function actionCancelchallenge($id)
    {
        $returnarray=array();
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $id . ' AND TM_CE_RE_Recepient_Id=' . Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status = '4';
        if ($model->save(false)):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CEUQ_Challenge=' . $id . ' AND TM_CEUQ_User_Id=' . Yii::app()->user->id);
            $model = Chalangeuserquestions::model()->deleteAll($criteria);
            $testcount=Chalangerecepient::model()->with('challenge')->count(array('condition' => "TM_CE_RE_Recepient_Id='" . Yii::app()->user->id . "' AND TM_CE_RE_Status IN (0,1,2) AND TM_CL_Status='0' AND TM_CL_Plan='" . Yii::app()->session['plan'] . "'"));
            $returnarray['error']='no';
            $returnarray['testcount']=$testcount; 
        else:
            $returnarray['error']='yes';
        endif;
         echo json_encode($returnarray);
    }

    public function actionAcceptchallenge($id)
    {

        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $id . ' AND TM_CE_RE_Recepient_Id=' . Yii::app()->user->id);
        $model = Chalangerecepient::model()->find($criteria);
        $model->TM_CE_RE_Status = '1';
        $model->TM_CE_RE_Date=date('Y-m-d');
        $return = '';
        if ($model->save(false)):
            $return .= '<td> <span class="rwd-tables thead"><b>Set By</b></span><span class="rwd-tables tbody">' . ($model->challenge->TM_CL_Chalanger_Id == Yii::app()->user->id ? "Me" : $this->GetChallenger($model->challenge->TM_CL_Chalanger_Id)) . '</span></td>';
            $return .= '<td> <span class="rwd-tables thead"><b>Set On</b></span><span class="rwd-tables tbody">' . date("d-m-Y", strtotime($model->challenge->TM_CL_Created_On)) . '</span></td>';
            $return .= '<td><span class="rwd-tables thead"><b>Questions</b></span><span class="rwd-tables tbody">' . $model->challenge->TM_CL_QuestionCount . '</span></td>';
            $return .= '<td><span class="rwd-tables thead"><b>Set For</b></span><span class="rwd-tables tbody">' . $this->GetChallengeInvitees($model->challenge->TM_CL_Id, Yii::app()->user->id) . '</span></td>';
            $return .= '<td class="text-right"></td><td class="text-right"><span >';
            $return .= "<a href='" . Yii::app()->createUrl('student/startchallenge', array('id' => $id)) . "' class='btn btn-warning'>" . (Student::model()->GetChallengeStatus($id, Yii::app()->user->id) > 0 ? 'Continue' : 'Start') . "</a>";
            $return .= '</span></td>';
            $return .= '<td class="text-right">';
            $return .= '<span  data-toggle="tooltip" data-placement="bottom" title="Cancel Challenge">' . CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/cancel.png', '', array('class' => 'hvr-buzz-out')),
                    'student/cancelchallenge/' . $id,
                    array(
                        'type' => 'POST',
                        'success' => 'function(html){ if(html=="yes"){$("#challengerow' . $id . '").remove();} }'
                    ),
                    array('confirm' => 'Are you sure you want to cancel this Challenge?', 'id' => 'cancel' . $id)
                );
            $return .= '</span></td>';
            echo $return;
        else:
            echo "no";
        endif;
    }

    public function actionChallenge()
    {
        $plan = $this->GetStudentDet();
        $mainchapters = Chapter::model()->findAll(array('condition' => "TM_TP_Syllabus_Id='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'",'order'=>'TM_TP_order ASC'));
        if (isset($_POST['Challenge'])):
            if (count($_POST['chapters']) > 0):
                $chapters = '';
                $topics = '';
                $challenge = new Challenges();
                $challenge->TM_CL_Chalanger_Id = Yii::app()->user->id;
                $challenge->TM_CL_Plan = Yii::app()->session['plan'];
                $challenge->TM_CL_QuestionCount = $_POST['Challenge']['questions'];
                $challenge->save(false);
                $recepchalange = new Chalangerecepient();
                $recepchalange->TM_CE_RE_Chalange_Id = $challenge->TM_CL_Id;
                $recepchalange->TM_CE_RE_Recepient_Id = Yii::app()->user->id;
                $recepchalange->TM_CE_RE_Status = '1';
                $recepchalange->TM_CE_RE_Date=date('Y-m-d');
                $recepchalange->save(false);
                if (count($_POST['invitebuddies']) > 0):
                    for ($i = 0; $i < count($_POST['invitebuddies']); $i++):
                        $recepchalange = new Chalangerecepient();
                        $recepchalange->TM_CE_RE_Chalange_Id = $challenge->TM_CL_Id;
                        $recepchalange->TM_CE_RE_Recepient_Id = $_POST['invitebuddies'][$i]; 
                        $recepchalange->save(false);                                               
                        /*if ($recepchalange->save(false)):
                            //$this->SendMailChallengeInvite($_POST['invitebuddies'][$i]);
                        endif;*/
                        $notification = new Notifications();
                        $notification->TM_NT_User = $_POST['invitebuddies'][$i];
                        $notification->TM_NT_Type = 'Challenge';
                        $notification->TM_NT_Target_Id = Yii::app()->user->Id;
                        $notification->TM_NT_Item_Id=$challenge->TM_CL_Id;
                        $notification->TM_NT_Status = '0';
                        $notification->save(false);
                    endfor;
                endif;
                for ($i = 0; $i < count($_POST['chapters']); $i++):
                    $chapters = $chapters . ($i == 0 ? $_POST['chapters'][$i] : "," . $_POST['chapters'][$i]);
                    $chapterid = $_POST['chapters'][$i];
                    $challengechapter = new ChallengeChapters();
                    $challengechapter->TM_CE_CP_Challenge_Id = $challenge->TM_CL_Id;
                    $challengechapter->TM_CE_CP_Chapter_Id = $chapterid;
                    $challengechapter->save(false);
                    for ($j = 0; $j < count($_POST['topic' . $chapterid]); $j++):
                        $topics = $topics . ($i == 0 & $j == 0 ? $_POST['topic' . $chapterid][$j] : "," . $_POST['topic' . $chapterid][$j]);
                    endfor;
                endfor;
                $Syllabus = $plan->TM_SPN_SyllabusId;
                $Standard = $plan->TM_SPN_StandardId;
                //$Topic=$topics;
                $Test = $challenge->TM_CL_Id;
                $Student = Yii::app()->user->id;
                $limit = $challenge->TM_CL_QuestionCount;
                //set questions for basic
                $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'" . $chapters . "','" . $topics . "',:limit,:Test,1,@out)");
                $command->bindParam(":Syllabus", $Syllabus);
                $command->bindParam(":Standard", $Standard);
                $command->bindParam(":limit", $limit);
                $command->bindParam(":Test", $Test);
                $command->query();
                $basiccount = Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                //set questions for intermediate
                $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'" . $chapters . "','" . $topics . "',:limit,:Test,2,@out)");
                $command->bindParam(":Syllabus", $Syllabus);
                $command->bindParam(":Standard", $Standard);
                $command->bindParam(":limit", $limit);
                $command->bindParam(":Test", $Test);
                $command->query();
                $intercount = Yii::app()->db->createCommand("select @out as result;")->queryScalar();
                //set questions for advanced
                $command = Yii::app()->db->createCommand("CALL SetChalangeQuestions(:Syllabus,:Standard,'" . $chapters . "','" . $topics . "',:limit,:Test,3,@out)");
                $command->bindParam(":Syllabus", $Syllabus);
                $command->bindParam(":Standard", $Standard);
                $command->bindParam(":limit", $limit);
                $command->bindParam(":Test", $Test);
                $command->query();
                $advancecount = Yii::app()->db->createCommand("select @out as result;")->queryScalar();
            endif;
            $this->redirect(array('home'));
        endif;
        $this->render('challenge', array('plan' => $plan, 'chapters' => $mainchapters));
    }

    public function actionChallengeTest($id)
    {
        $challengecount = Challenges::model()->findByPk($id)->TM_CL_QuestionCount;
        if (isset($_POST['action']['GoNext'])):
            //change challenge status to started
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $id . ' AND TM_CE_RE_Recepient_Id=' . Yii::app()->user->id);
            $model = Chalangerecepient::model()->find($criteria);
            if ($model->TM_CE_RE_Status == '1'):
                $model->TM_CE_RE_Status = '2';
                $model->save(false);
            endif;
            $chuserquest = Chalangeuserquestions::model()->findByPk($_POST['ChallengeQuestionId']);
            $masterquestion = Questions::model()->findByPk($_POST['QuestionId']);
            $difficulty = $chuserquest->TM_CEUQ_Difficulty;
            if ($difficulty == '1'):
                $marks = '3';
            elseif ($difficulty == '2'):
                $marks = '5';
            elseif ($difficulty == '3'):
                $marks = '8';
            endif;
            if ($_POST['QuestionType'] == '1'):
                $answer = implode(',', $_POST['answer']);
                $chuserquest->TM_CEUQ_Answer_Id = $answer;
                $correctcount = 0;
                $usercorrect = 0;
                
                
                foreach ($masterquestion->answers AS $ans):
                    if ($ans->TM_AR_Correct == '1'):
                        $correctcount++;
                        if (array_search($ans->TM_AR_Id, $_POST['answer']) !== false) {
                            $usercorrect++;
                        }
                    endif;
                endforeach;
                if ($correctcount == $usercorrect):
                    switch ($difficulty) {
                        case "1";
                            $chuserquest->TM_CEUQ_Marks = '3';
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "2";
                            $chuserquest->TM_CEUQ_Marks = '5';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            $chuserquest->TM_CEUQ_Marks = '8';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                    }
                else:
                    switch ($difficulty) {
                        case "1";
                            if ($this->NextDiffCount($id, '1') > 0):
                                $nextdifficulty = '1';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                        case "2";
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            $chuserquest->TM_CEUQ_Marks = '8';
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '1') > 0):
                                    $nextdifficulty = '1';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                    }
                endif;
            elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                $answer = $_POST['answer'];
                $answerarray = array($_POST['answer']);
                $chuserquest->TM_CEUQ_Answer_Id = $answer;
                $correctcount = 0;
                $usercorrect = 0;

                foreach ($masterquestion->answers AS $ans):
                    if ($ans->TM_AR_Correct == '1'):
                    
                        $correctcount++;
                        if (array_search($ans->TM_AR_Id, $answerarray) !== false) {
                            $usercorrect++;
                           
                        }
                    endif;
                endforeach;
                if ($correctcount == $usercorrect):
                    switch ($difficulty) {
                        case "1";
                            $chuserquest->TM_CEUQ_Marks = '3';
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "2";
                            $chuserquest->TM_CEUQ_Marks = '5';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            $chuserquest->TM_CEUQ_Marks = '8';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                    }
                else:
                    switch ($difficulty) {
                        case "1";
                            if ($this->NextDiffCount($id, '1') > 0):
                                $nextdifficulty = '1';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                        case "2";
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            //$chuserquest->TM_CEUQ_Marks = '12';
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '1') > 0):
                                    $nextdifficulty = '1';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                    }
                endif;
            elseif ($_POST['QuestionType'] == '4'):
                $chuserquest->TM_CEUQ_Answer_Id = $_POST['answer'];
                $correctcount = 0;
                $usercorrect = 0;
                foreach ($masterquestion->answers AS $ans):
                    $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                    $useranswer = trim(strtolower($_POST['answer']));
                    for ($i = 0; $i < count($answeroption); $i++) {
                        $option = trim($answeroption[$i]);

                        if($option=='&gt'):
                            $newoption='&gt;';
                        elseif($option=='&lt'):
                            $newoption='&lt;';
                        else:
                            $newoption=$option;
                        endif;
                        if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0):
                            $usercorrect++;
                        endif;
                    }
                endforeach;
                if ($usercorrect != 0):
                    switch ($difficulty) {
                        case "1";
                            $chuserquest->TM_CEUQ_Marks = '3';
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "2";
                            $chuserquest->TM_CEUQ_Marks = '5';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            $chuserquest->TM_CEUQ_Marks = '8';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                    }
                else:
                    switch ($difficulty) {
                        case "1";
                            if ($this->NextDiffCount($id, '1') > 0):
                                $nextdifficulty = '1';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                        case "2";
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '1') > 0):
                                    $nextdifficulty = '1';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                    }
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $masterquestion->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                $totalmarks = 0;
                $marks = 0;
                foreach ($questions AS $child):
                    $selectedquestion = Chalangeuserquestions::model()->find(
                        array('condition' => 'TM_CEUQ_Question_Id=:parent AND TM_CEUQ_Challenge=:challenge AND TM_CEUQ_User_Id=:student',
                            'params' => array(':parent' => $child->TM_QN_Id, ':challenge' => $id, ':student' => Yii::app()->user->id))
                    );

                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);

                        $selectedquestion->TM_CEUQ_Answer_Id = $answer;
                        foreach ($child->answers AS $ans):
                            if ($ans->TM_AR_Correct == '1'):
                                if (array_search($ans->TM_AR_Id, $_POST['answer' . $child->TM_QN_Id]) !== false) {
                                    $marks = $marks + $ans->TM_AR_Marks;
                                }
                            endif;
                            $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                        endforeach;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_CEUQ_Answer_Id = $answer;
                        foreach ($child->answers AS $ans):
                            if ($ans->TM_AR_Correct == '1'):
                                if ($ans->TM_AR_Id == $answer) {
                                    $marks = $marks + $ans->TM_AR_Marks;
                                }
                            endif;

                            $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                        endforeach;

                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_CEUQ_Answer_Id = $_POST['answer' . $child->TM_QN_Id];
                        foreach ($child->answers AS $ans):

                            $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                            $useranswer = trim(strtolower($selectedquestion->TM_CEUQ_Answer_Id));
                            for ($i = 0; $i < count($answeroption); $i++) {
                                $option = trim($answeroption[$i]);
                                //fix for < > issue
                                if($option=='&gt'):
                                    $newoption='&gt;';
                                elseif($option=='&lt'):
                                    $newoption='&lt;';
                                else:
                                    $newoption=$option;
                                endif;
                                if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0):
                                    $marks = $marks + $ans->TM_AR_Marks;
                                endif;
                            }
                            $totalmarks = $totalmarks + $ans->TM_AR_Marks;

                        endforeach;
                    endif;
                    $selectedquestion->save(false);
                endforeach;
                if ($totalmarks == $marks):
                    switch ($difficulty) {
                        case "1";
                            $chuserquest->TM_CEUQ_Marks = '3';
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "2";
                            $chuserquest->TM_CEUQ_Marks = '5';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            $chuserquest->TM_CEUQ_Marks = '8';
                            if ($this->NextDiffCount($id, '3') > 0):
                                $nextdifficulty = '3';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                    }
                else:
                    switch ($difficulty) {
                        case "1";
                            if ($this->NextDiffCount($id, '1') > 0):
                                $nextdifficulty = '1';
                            else:
                                if ($this->NextDiffCount($id, '2') > 0):
                                    $nextdifficulty = '2';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                        case "2";
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '3') > 0):
                                    $nextdifficulty = '3';
                                else:
                                    $nextdifficulty = '1';
                                endif;
                            endif;
                            break;
                        case "3";
                            if ($this->NextDiffCount($id, '2') > 0):
                                $nextdifficulty = '2';
                            else:
                                if ($this->NextDiffCount($id, '1') > 0):
                                    $nextdifficulty = '1';
                                else:
                                    $nextdifficulty = '3';
                                endif;
                            endif;
                            break;
                    }
                endif;
            endif;
            $chuserquest->TM_CEUQ_Status = '1';
            $chuserquest->save(false);
            if ($challengecount == '10' & $chuserquest->TM_CEUQ_Iteration <= '5'):
                $nextcount = $chuserquest->TM_CEUQ_Iteration + 5;

                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=' . $nextdifficulty . ' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge=' . $id . ' AND TM_CEUQ_User_Id=' . Yii::app()->user->id . ')';
                $criteria->limit = '1';
                $criteria->order = 'RAND()';
                $criteria->params = array(':test' => $id);
                $nextquestions = Chalangequestions::model()->find($criteria);
                if (count($nextquestions) > 0):
                    $masterquestion = Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                    $userquestion = new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge = $id;
                    $userquestion->TM_CEUQ_Question_Id = $nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty = $nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration = $nextcount;
                    $userquestion->save(false);
                    if ($masterquestion->TM_QN_Type_Id == '5'):
                        $childQuestions = Questions::model()->findAll(array('condition' => 'TM_QN_Parent_Id=:parent', 'params' => array(':parent' => $nextquestions->TM_CL_QS_Question_Id)));
                        foreach ($childQuestions AS $child):
                            $userquestion = new Chalangeuserquestions();
                            $userquestion->TM_CEUQ_Challenge = $id;
                            $userquestion->TM_CEUQ_Question_Id = $child->TM_QN_Id;
                            $userquestion->TM_CEUQ_Difficulty = $child->TM_QN_Dificulty_Id;
                            $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                            $userquestion->TM_CEUQ_Parent_Id = $nextquestions->TM_CL_QS_Question_Id;
                            $userquestion->save(false);
                        endforeach;
                    endif;
                endif;
            elseif ($challengecount == '15' & $chuserquest->TM_CEUQ_Iteration <= '10'):
                $nextcount = $chuserquest->TM_CEUQ_Iteration + 5;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=' . $nextdifficulty . ' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge=' . $id . ' AND TM_CEUQ_User_Id=' . Yii::app()->user->id . ')';
                $criteria->limit = '1';
                $criteria->order = 'RAND()';
                $criteria->params = array(':test' => $id);
                $nextquestions = Chalangequestions::model()->find($criteria);
                if (count($nextquestions) > 0):
                    $masterquestion = Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                    $userquestion = new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge = $id;
                    $userquestion->TM_CEUQ_Question_Id = $nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty = $nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration = $nextcount;
                    $userquestion->save(false);
                    if ($masterquestion->TM_QN_Type_Id == '5'):
                        $childQuestions = Questions::model()->findAll(array('condition' => 'TM_QN_Parent_Id=:parent', 'params' => array(':parent' => $nextquestions->TM_CL_QS_Question_Id)));
                        foreach ($childQuestions AS $child):
                            $userquestion = new Chalangeuserquestions();
                            $userquestion->TM_CEUQ_Challenge = $id;
                            $userquestion->TM_CEUQ_Question_Id = $child->TM_QN_Id;
                            $userquestion->TM_CEUQ_Difficulty = $child->TM_QN_Dificulty_Id;
                            $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                            $userquestion->TM_CEUQ_Parent_Id = $nextquestions->TM_CL_QS_Question_Id;
                            $userquestion->save(false);
                        endforeach;
                    endif;
                endif;
            elseif ($challengecount == '20' & $chuserquest->TM_CEUQ_Iteration <= '15'):
                $nextcount = $chuserquest->TM_CEUQ_Iteration + 5;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=' . $nextdifficulty . ' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge=' . $id . ' AND TM_CEUQ_User_Id=' . Yii::app()->user->id . ')';
                $criteria->limit = '1';
                $criteria->order = 'RAND()';
                $criteria->params = array(':test' => $id);
                $nextquestions = Chalangequestions::model()->find($criteria);

                if (count($nextquestions) > 0):
                    $masterquestion = Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                    $userquestion = new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge = $id;
                    $userquestion->TM_CEUQ_Question_Id = $nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty = $nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration = $nextcount;
                    $userquestion->save(false);
                    if ($masterquestion->TM_QN_Type_Id == '5'):
                        $childQuestions = Questions::model()->findAll(array('condition' => 'TM_QN_Parent_Id=:parent', 'params' => array(':parent' => $nextquestions->TM_CL_QS_Question_Id)));
                        foreach ($childQuestions AS $child):
                            $userquestion = new Chalangeuserquestions();
                            $userquestion->TM_CEUQ_Challenge = $id;
                            $userquestion->TM_CEUQ_Question_Id = $child->TM_QN_Id;
                            $userquestion->TM_CEUQ_Difficulty = $child->TM_QN_Dificulty_Id;
                            $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                            $userquestion->TM_CEUQ_Parent_Id = $nextquestions->TM_CL_QS_Question_Id;
                            $userquestion->save(false);
                        endforeach;
                    endif;
                endif;
            elseif ($challengecount == '25' & $chuserquest->TM_CEUQ_Iteration <= '20'):
                $nextcount = $chuserquest->TM_CEUQ_Iteration + 5;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=' . $nextdifficulty . ' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge=' . $id . ' AND TM_CEUQ_User_Id=' . Yii::app()->user->id . ')';
                $criteria->limit = '1';
                $criteria->order = 'RAND()';
                $criteria->params = array(':test' => $id);
                $nextquestions = Chalangequestions::model()->find($criteria);
                if (count($nextquestions) > 0):
                    $masterquestion = Questions::model()->findByPk($nextquestions->TM_CL_QS_Question_Id);
                    $userquestion = new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge = $id;
                    $userquestion->TM_CEUQ_Question_Id = $nextquestions->TM_CL_QS_Question_Id;
                    $userquestion->TM_CEUQ_Difficulty = $nextquestions->TM_CL_QS_Difficulty;
                    $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                    $userquestion->TM_CEUQ_Iteration = $nextcount;
                    $userquestion->save(false);
                    if ($masterquestion->TM_QN_Type_Id == '5'):
                        $childQuestions = Questions::model()->findAll(array('condition' => 'TM_QN_Parent_Id=:parent', 'params' => array(':parent' => $nextquestions->TM_CL_QS_Question_Id)));
                        foreach ($childQuestions AS $child):
                            $userquestion = new Chalangeuserquestions();
                            $userquestion->TM_CEUQ_Challenge = $id;
                            $userquestion->TM_CEUQ_Question_Id = $child->TM_QN_Id;
                            $userquestion->TM_CEUQ_Difficulty = $child->TM_QN_Dificulty_Id;
                            $userquestion->TM_CEUQ_User_Id = Yii::app()->user->id;
                            $userquestion->TM_CEUQ_Parent_Id = $nextquestions->TM_CL_QS_Question_Id;
                            $userquestion->save(false);
                        endforeach;
                    endif;
                endif;
            elseif ($challengecount == $chuserquest->TM_CEUQ_Iteration || $this->GetCountChallengeQuestion($id)==$chuserquest->TM_CEUQ_Iteration):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_CE_RE_Chalange_Id=:test AND TM_CE_RE_Recepient_Id=:user';
                $criteria->params = array(':test' => $id, ':user' => Yii::app()->user->id);
                $nextquestions = Chalangerecepient::model()->find($criteria);
                $nextquestions->TM_CE_RE_Status = '3';
                $nextquestions->save(false);
                $this->redirect(array('challengeresult', 'id' => $id));
            endif;

        endif;
        if (Student::model()->GetChallengeStatus($id, Yii::app()->user->id) == 0):
            // insert for basic 3 questions
            $difficulty = 1;
            $easylimit = 3;
            $iteration = 0;
            $easycount = $this->GetChallengeCount($id, $difficulty, $easylimit);
            if ($easycount < $easylimit):
                $interiteration = $this->ChallengeAddQuestions($id, $difficulty, $easycount, Yii::app()->user->id, $iteration);
                $difficulty = 2;
                $newlimit = 2 + ($easylimit-$easycount);
            else:
                $interiteration = $this->ChallengeAddQuestions($id, $difficulty, $easylimit, Yii::app()->user->id, $iteration);
                $difficulty = 2;
                $newlimit = 2;
            endif;
            
            // insert $interlimit intermediate 2 questions
            $intercount = $this->GetChallengeCount($id, $difficulty, $newlimit);            
            if ($intercount < $newlimit):
                $advanceiteration = $this->ChallengeAddQuestions($id, $difficulty, $intercount, Yii::app()->user->id, $interiteration);
                $difficulty = 3;
                $newlimit = 5 - $advanceiteration;
            else:
                $advanceiteration = $this->ChallengeAddQuestions($id, $difficulty, $newlimit, Yii::app()->user->id, $interiteration);
                $difficulty = 3;
                $newlimit = 0;
            endif;            
            // if not enough intermediate insert from advance
            if ($newlimit != 0):
                $advancecount = $this->GetChallengeCount($id, $difficulty, $newlimit);
                if ($advancecount < $newlimit):
                    $iteration = $this->ChallengeAddQuestions($id, $difficulty, $newlimit, Yii::app()->user->id, $advanceiteration);
                else:
                    $iteration = $this->ChallengeAddQuestions($id, $difficulty, $newlimit, Yii::app()->user->id, $advanceiteration);
                endif;
            endif;
        endif;
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_CEUQ_Challenge=:test AND TM_CEUQ_Status=0 AND TM_CEUQ_User_Id=:user AND TM_CEUQ_Parent_Id=0';
        $criteria->limit = '1';
        $criteria->order = 'TM_CEUQ_Iteration ASC';
        $criteria->params = array(':test' => $id, ':user' => Yii::app()->user->id);
        $questioncount = Chalangeuserquestions::model()->count($criteria);
        if($questioncount==0):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_CE_RE_Chalange_Id=:test AND TM_CE_RE_Recepient_Id=:user';
                $criteria->params = array(':test' => $id, ':user' => Yii::app()->user->id);
                $nextquestions = Chalangerecepient::model()->find($criteria);
                $nextquestions->TM_CE_RE_Status = '3';
                $nextquestions->save(false);
            $this->redirect(array('challengeresult', 'id' => $id));
            
        endif;
        $question = Chalangeuserquestions::model()->find($criteria);
        $this->renderPartial('challengequestion', array('testid' => $id, 'challengequestion' => $question, 'question' => $question->question, 'challengecount' => $challengecount));

    }

    public function actionChallengeresult($id)
    {
        $challengecount = Challenges::model()->findByPk($id)->TM_CL_QuestionCount;

        $examtotalmarks=$this->GetChallengetotal($id);
        $criteria = new CDbCriteria;
        $criteria->select = array('SUM(TM_CEUQ_Marks) AS total');
        $criteria->condition = 'TM_CEUQ_Challenge=:test AND TM_CEUQ_User_Id=:user';
        $criteria->params = array(':test' => $id, ':user' => Yii::app()->user->id);
        $studenttotal = Chalangeuserquestions::model()->find($criteria);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $id . ' AND TM_CE_RE_Recepient_Id=' . Yii::app()->user->id);
        $recepient = Chalangerecepient::model()->find($criteria);
        $recepient->TM_CE_RE_ObtainedMarks = $studenttotal->total;
        $points = $studenttotal->total;
        $recepient->save(false);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $id);
        $criteria->order = 'TM_CE_RE_ObtainedMarks DESC';
        $participants = Chalangerecepient::model()->findAll($criteria);
        $recepients = count($participants);
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $id.' AND TM_CE_RE_Status=3');
        $criteria->order = 'TM_CE_RE_ObtainedMarks DESC';
        $participantscompletedd = Chalangerecepient::model()->findAll($criteria);
        $completed = count($participantscompletedd);
        //echo $recepients;exit;
        $userids = array();
        foreach ($participants AS $party):
            $userids[] = $party->TM_CE_RE_Recepient_Id;
        endforeach;


        $results = Chalangeuserquestions::model()->findAll(
            array(
                'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Parent_Id=0 ',
                "order" => 'TM_CEUQ_Id ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $earnedmarks = 0;
        $result = '';
        $qstnnum = 1;
        foreach ($results AS $exam):
            $question = Questions::model()->findByPk($exam->TM_CEUQ_Question_Id);
            $displaymark = 0;
            if ($exam->TM_CEUQ_Difficulty == 1):
                $displaymark = 3;
            elseif ($exam->TM_CEUQ_Difficulty == 2):
                $displaymark = 5;
            elseif ($exam->TM_CEUQ_Difficulty == 3):
                $displaymark = 8;
            endif;
            if ($question->TM_QN_Type_Id != '5'):
                $marks = 0;
                if ($question->TM_QN_Type_Id != '4'):
                    $challengeans = Chalangeuserquestions::model()->find(array('condition' => "TM_CEUQ_Challenge='$id' AND TM_CEUQ_Question_Id='$exam->TM_CEUQ_Question_Id'"));
                    $studentanswer = explode(',', $challengeans->TM_CEUQ_Answer_Id);
                    if (count($studentanswer) > 0):
                        $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                        for ($i = 0; $i < count($studentanswer); $i++) {
                            $answer = Answers::model()->findByPk($studentanswer[$i]);
                            $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=200&height=200" alt=""></div>' : '') . '</li>';
                        }
                        $correctanswer .= '</ul>';
                    else:
                        $correctanswer = '<ul class="list-group"><lh><b>Your Answer:</b></lh><li class="list-group-item1" style="display:flex ;">Question Skipped</li></ul>';
                    endif;
                else:
                    if ($exam->TM_CEUQ_Answer_Id != ''):
                        $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh><li class="list-group-item1" style="display:flex ;"><div class="col-md-12">' . $exam->TM_CEUQ_Answer_Id . '</div></li></ul>';
                    else:
                        $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh><li class="list-group-item1" style="display:flex ;">Question Skipped</li></ul>';
                    endif;
                endif;
                $result .= '<tr><td style="background-color: white;"><div class="col-md-3">';
                if ($exam->TM_CEUQ_Marks == '0'):
                    $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question.' . $qstnnum++ . '</span>';
                /*elseif($marks<$displaymark & $marks!='0'):
                    $result .= '<span class="clrr"><img src="'.Yii::app()->request->baseUrl.'/images/rw.png"></i>  Question.' .$qstnnum++.'</span>';*/
                else:
                    $result .= '<span ><i class="fa fa fa-check"></i>  Question.' . $qstnnum++ . '</span>';
                endif;

                $result .= ' <span class="pull-right">Mark: ' . $exam->TM_CEUQ_Marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-4 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:12px;margin-left: 42%;color: rgb(50, 169, 255);">Report Problem</a>
                            <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:' . $question->TM_QN_QuestionReff . '</span></div>';

                //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                $result .= '<div class="col-md-12">' . $correctanswer . '</div>';
                $result .= ' <div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div>
                <div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div>';
                $result .= '<div class="col-md-12 Itemtest clickable-row ">
                            <div class="sendmailtest suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . ' "name="comments"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right" id="mailSendtest" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                            </div>
                            <div class="col-md-10 alert alert-success alertpadding alertpullright" id="mailSendCompletetest' . $question->TM_QN_Id . '" style="display: none;">Your message has been sent to admin</div>
                            </div>
                             </td></tr>';

            //glyphicon glyphicon-star sty
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));

                $count = 1;
                $studentmarks = 0;
                $totalmarks = 0;
                $childresult = '';
                foreach ($questions AS $childquestion):
                    $childresults = Chalangeuserquestions::model()->find(
                        array(
                            'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Question_Id=:questionid',
                            'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                        )
                    );
                    if ($childquestion->TM_QN_Type_Id != '4'):
                        if ($childresults->TM_CEUQ_Answer_Id != ''):
                            $studentanswer = explode(',', $childresults->TM_CEUQ_Answer_Id);
                            if (count($studentanswer) > 0):
                                $correctanswer = '<ul class="list-group"><lh><b>Your Answer:</b></lh>';
                                for ($i = 0; $i < count($studentanswer); $i++) {
                                    $answer = Answers::model()->findByPk($studentanswer[$i]);
                                    $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=200&height=200" alt=""></div>' : '') . '</li></ul>';

                                }
                                $correctanswer .= '</ul>';
                            endif;
                        else:
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                        endif;
                    else:
                        if ($childresults->TM_CEUQ_Answer_Id != ''):
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_CEUQ_Answer_Id . '</li></ul>';
                        else:
                            $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                        endif;

                    endif;

                    $childresult .= '<div class="col-md-12"><p>Part ' . $count . ':</p> <li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($childquestion->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '')) . '</li></div>';
                    $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                    //glyphicon glyphicon-star sty
                    $count++;
                endforeach;
                $result .= '<tr><td style="background-color: white;"><div class="col-md-3">';
                if ($exam->TM_CEUQ_Marks == '0'):
                    $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question.' . $qstnnum++ . '</span>';
                /*elseif($marks<$displaymark & $marks!='0'):
                    $result .= '<span class="clrr"><img src="'.Yii::app()->request->baseUrl.'/images/rw.png"></i>  Question.' .$qstnnum++.'</span>';*/
                else:
                    $result .= '<span ><i class="fa fa fa-check"></i>  Question.' . $qstnnum++ . '</span>';
                endif;
                $result .= ' <span class="pull-right">Mark: ' . $exam->TM_CEUQ_Marks . '/' . $displaymark . '</span></div>
                            <div class="col-md-4 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:12px;margin-left: 42%;color: rgb(50, 169, 255);">Report Problem</a>
                            <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:' . $question->TM_QN_QuestionReff . '</span></div>';

                //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                $result .= $childresult;
                $result.= '<div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div>
                <div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div>';
                $result .= '<div class="col-md-12 Itemtest clickable-row ">
                            <div class="sendmailtest suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                                <div class="col-md-10" id="maildivslidetest"  >
                                <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                </div>
                                <div class="col-md-2" style="margi-top:15px;">
                                <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '" >
                                </div>
                            </div>
                            <div class="col-md-10 alert alert-success alertpadding alertpullright" id="mailSendCompletetest' . $question->TM_QN_Id . '" style="display: none;">Your message has been sent to admin</div>
                            </div>
                             </td></tr>';
            endif;
        endforeach;
        $pointlevels = $this->AddPoints($studenttotal->total, $id, 'Challenge', Yii::app()->user->id);
        $this->render('challengeresult', array('completed'=>$completed,'totalmarks' => $examtotalmarks, 'studenttotal' => $studenttotal->total, 'points' => $points, 'participants' => $participants, 'recepients' => $recepients, 'userids' => $userids, 'testresult' => $result, 'pointlevels' => $pointlevels, 'testid' => $id));
    }
    public function GetChallengetotal($id)
    {
       $results = Chalangeuserquestions::model()->findAll(
                array(
                    'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Parent_Id=0',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
                )
        );
        $total=0;
        foreach($results AS $challenge):
            if($challenge->TM_CEUQ_Difficulty=='1'):
                $total=$total+3;
            elseif($challenge->TM_CEUQ_Difficulty=='2'):
                $total=$total+5;
            elseif($challenge->TM_CEUQ_Difficulty=='3'):
                $total=$total+8;                            
            endif;
        endforeach; 
        return $total;
    }
public function actionChallengeGetSolution()
    {
        if (isset($_POST['challengeid'])):
            $results = Chalangeuserquestions::model()->findAll(
                array(
                    'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test AND TM_CEUQ_Parent_Id=0',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $_POST['challengeid'])
                )
            );
            $resulthtml = '';
            $count = 1;
            foreach ($results AS $result):
                if ($result->TM_CEUQ_Difficulty == 1):
                    $mark = 5;
                elseif ($result->TM_CEUQ_Difficulty == 2):
                    $mark = 8;
                elseif ($result->TM_CEUQ_Difficulty == 3):
                    $mark = 12;
                endif;
                $question = Questions::model()->findByPk($result->TM_CEUQ_Question_Id);
                if($question->TM_QN_Type_Id!=5):
                    $resulthtml .= '<tr><td style="background-color: white;"><div class="col-md-3">
                        <span style="font-weight: 700;"> Question ' . $count++ . '</span>
                        <span class="pull-right">Marks : ' . $mark . '</span>
                        </div>
                        <div class="col-md-3 pull-right">
                        <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $result->TM_CEUQ_Question_Id . '" style="font-size:12px;color: rgb(50, 169, 255);">Report Problem</a>
                        <span class="pull-right" style="font-size: 11px;padding-top: 1px;">  Ref:' . $question->TM_QN_QuestionReff . '</span>
                        </div>';
                    $resulthtml .= '<div class="col-md-12">
                    <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>';
    
                    $resulthtml .= '<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div>
                    <div class="col-md-12">
    
                    <div class="col-md-10" style="padding-left: 1px;">' . $question->TM_QN_Solutions . '</div>' . ($question->TM_QN_Solution_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>' : '') . '</div>';
    
                    //$resulthtml.='<div class="col-md-12"><span style="font-weight: 700;">Solution</span></div><div class="col-md-12">'.$question->TM_QN_Solutions.'</div>';
    
                    $resulthtml .= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestion' . $result->TM_CEUQ_Question_Id . '" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comments"  id="comment' . $question->TM_QN_Id . '" hidden/></div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden ></div>
                               </div><div class="col-md-10 alert alert-success alertpadding alertpullright" id="mailSendComplete'.$result->TM_CEUQ_Question_Id.'" style="display: none;">Your message has been sent to admin</div></div>
    
                            </td></tr>';
                else:
					$displaymark = 0;
					$multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
					$countmulti = 1;
					$displaymark = 0;
					$childresulthtml = '';
					foreach ($multiQuestions AS $key => $chilquestions):

						$marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
						foreach ($marks AS $key => $marksdisplay):
							$displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
						endforeach;
						$childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
						$childresulthtml .= '<div class="col-md-12 padnone">';
						$childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
						$childresulthtml .= '</div>';
						$countmulti++;
					endforeach;
						$resulthtml .= '<tr><td><div class="col-md-3">
						<span><b>Question' . $count++ . '</b> </span>
						<span class="pull-right">Marks: ' . $mark . '</span>
						</div>
						<div class="col-md-3 pull-right">
					   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
						<span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';

                    $resulthtml .= '<div class="col-md-12 Itemtest clickable-row "><div class="sendmail suggestion' . $result->TM_CEUQ_Question_Id . '" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comments"  id="comment' . $question->TM_QN_Id . '" hidden/></div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden ></div>
                               </div><div class="col-md-10 alert alert-success alertpadding alertpullright" id="mailSendComplete'.$result->TM_CEUQ_Question_Id.'" style="display: none;">Your message has been sent to admin</div></div>
    
                            </td></tr>';					
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }
    public function GetPaperCount($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Type IN (6,7) AND TM_STU_QN_Parent_Id=0';
        $criteria->params = array(':test' => $id);
        $selectedquestion = StudentQuestions::model()->count($criteria);
        return $selectedquestion;
    }

    public function GetPaperCountMock($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type IN (6,7) AND TM_STMKQT_Parent_Id=0';
        $criteria->params = array(':test' => $id);
        $selectedquestion = Studentmockquestions::model()->find($criteria);
        return count($selectedquestion);
    }

    public function GetPaperCountHW($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Type IN (6,7) AND TM_STHWQT_Parent_Id=0';
        $criteria->params = array(':test' => $id);
        $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
        return count($selectedquestion);
    }

    public function GetQuestionCount($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
        $criteria->params = array(':test' => $id);
        $selectedquestion = StudentQuestions::model()->findAll($criteria);
        return count($selectedquestion);
    }

    public function GetChallengeQuestionCount($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_CL_Id=:test ';
        $criteria->params = array(':test' => $id);
        $selectedquestion = Challenges::model()->findAll($criteria);
        //$questioncount=$selectedquestion->TM_CL_QuestionCount;
        return $selectedquestion->TM_CL_QuestionCount;
    }

    public function doLatercount($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Flag=:flag';
        $criteria->params = array(':test' => $id, ':flag' => '2');
        $selectedquestion = StudentQuestions::model()->find($criteria);
        return count($selectedquestion);
    }

    public function GetMockQuestions($id, $question)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Type NOT IN (6,7)';
        $criteria->params = array(':test' => $id);
        $criteria->order = 'TM_STMKQT_Order ASC';
        $selectedquestion = Studentmockquestions::model()->findAll($criteria);
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Type IN (6,7)';
        $criteria->params = array(':test' => $id);
        $papercount = Studentmockquestions::model()->count($criteria);
        $pagination = '';
        foreach ($selectedquestion AS $key => $testquestion):
            $count = $key + 1;
            $class = '';
            if ($testquestion->TM_STMKQT_Question_Id == $question) {
                $questioncount = $count;
            }
            if ($testquestion->TM_STMKQT_Type == '6' || $testquestion->TM_STMKQT_Type == '7') {
                $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
            } else {
                $title = '';
                $class = '';
                if ($testquestion->TM_STMKQT_Status == '0') {
                    $class = ($testquestion->TM_STMKQT_Question_Id == $question ? 'class="active"' : '');
                    $title = ($testquestion->TM_STMKQT_Question_Id == $question ? 'Current Question' : '');
                } elseif ($testquestion->TM_STMKQT_Status == '1') {
                    $class = ($testquestion->TM_STMKQT_Question_Id == $question ? 'class="active"' : 'class="answered"');
                    $title = ($testquestion->TM_STMKQT_Question_Id == $question ? 'Current Question' : 'Already Answered');
                } elseif ($testquestion->TM_STMKQT_Status == '2') {
                    $class = ($testquestion->TM_STMKQT_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                    $title = ($testquestion->TM_STMKQT_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                }
                $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_STMKQT_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('student/mocktest', array('id' => $id, 'questionnum' => base64_encode('question_' . $testquestion->TM_STMKQT_Question_Id)))) . '">' . $count . '</a></li>';
            }
        endforeach;
        return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count, 'papercount' => $papercount);
    }

    public function GetTestQuestions($id, $question)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Type NOT IN (6,7)';
        $criteria->params = array(':test' => $id);
        $criteria->order = 'TM_STU_QN_Number ASC';
        $selectedquestion = StudentQuestions::model()->findAll($criteria);
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Type IN (6,7)';
        $criteria->params = array(':test' => $id);
        $criteria->order = 'TM_STU_QN_Number ASC';
        $papercount = StudentQuestions::model()->count($criteria);
        $pagination = '';
        foreach ($selectedquestion AS $key => $testquestion):
            $count = $key + 1;
            $class = '';
            if ($testquestion->TM_STU_QN_Question_Id == $question) {
                $questioncount = $count;
            }
            if ($testquestion->TM_STU_QN_Type == '6' || $testquestion->TM_STU_QN_Type == '7') {
                $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
            } else {
                $title = '';
                $class = '';
                if ($testquestion->TM_STU_QN_Flag == '0') {
                    $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : '');
                    $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : '');
                } elseif ($testquestion->TM_STU_QN_Flag == '1') {
                    $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="answered"');
                    $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Already Answered');
                } elseif ($testquestion->TM_STU_QN_Flag == '2') {
                    $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                    $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                }
                $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_STU_QN_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('student/RevisionTest', array('id' => $id, 'questionnum' => base64_encode('question_' . $testquestion->TM_STU_QN_Number)))) . '">' . $count . '</a></li>';
            }
        endforeach;
        return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count, 'papercount' => $papercount);
    }

    public function GetChildAnswer($type, $test, $question)
    {

        $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $test, 'TM_STU_QN_Student_Id' => Yii::app()->user->id, 'TM_STU_QN_Question_Id' => $question));
        if (count($questionid) > 0):
            if ($type == '1'):
                if ($questionid->TM_STU_QN_Answer_Id != ''):
                    return explode(',', $questionid->TM_STU_QN_Answer_Id);
                else:
                    return array();
                endif;
            else:
                return $questionid->TM_STU_QN_Answer;
            endif;
        else:
            return array();
        endif;
    }

    public function GetChildAnswerMock($type, $test, $question)
    {

        $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $test, 'TM_STMKQT_Student_Id' => Yii::app()->user->id, 'TM_STMKQT_Question_Id' => $question));
        if (count($questionid) > 0):
            if ($type == '1'):
                if ($questionid->TM_STMKQT_Answer != ''):
                    return explode(',', $questionid->TM_STMKQT_Answer);
                else:
                    return array();
                endif;
            else:
                return $questionid->TM_STMKQT_Answer;
            endif;
        else:
            return array();
        endif;
    }

     public function GetChildAnswerTest($type, $test, $question)
    {

        $questionid = Studenthomeworkquestions::model()->findByAttributes(array('TM_STHWQT_Mock_Id' => $test, 'TM_STHWQT_Student_Id' => Yii::app()->user->id, 'TM_STHWQT_Question_Id' => $question));
        if (count($questionid) > 0):
            if ($type == '1'):
                if ($questionid->TM_STHWQT_Answer != ''):
                    return explode(',', $questionid->TM_STHWQT_Answer);
                else:
                    return array();
                endif;
            else:
                return $questionid->TM_STHWQT_Answer;
            endif;
        else:
            return array();
        endif;
    }

    public function GetQuickLimits($total)
    {
        $mode = $total % 3;
        if ($mode == 0):
            $count = $total / 3;
            return array('basic' => $count, 'intermediate' => $count, 'advanced' => $count);
        elseif ($mode == 1):
            $count = round($total / 3);
            return array('basic' => $count, 'intermediate' => $count + 1, 'advanced' => $count);
        elseif ($mode == 2):
            $count = $total / 3;
            return array('basic' => ceil($count), 'intermediate' => ceil($count), 'advanced' => floor($count));
        endif;

    }

    public function ResultMessage($percentage)
    {
        $hundred = array("Congratulations. All hail the Math Master! ", "Wow ! Time to Celebrate..", "High - five !! Now, that's what I call a score !");
        $ninety = array('Excellent score ! Well done.', 'Excellent.. Next time its 100% !', 'Excellent Score .. Keep it up !');
        $seventyfive = array('Well done, but the 90\'s suit you better !', 'Not a bad score.. Well done, you !', 'Keep it up ! Keep it better !');
        $sixty = array('Decent effort, But you can do better !', ' Well done you ! But you can do better !', 'A decent effort. Aim for higher !');
        $fifty = array('Not bad, but brush up a little more..', 'Do revise and try again !', 'Practice Makes Perfect. Try again !');
        $belowforty = array('Sorry.. do brush up and try again ! ', 'Hmm.. do we need some more practice ?', 'Practice Makes Perfect. Try again !');
        if ($percentage == '100'):
            $rand = array_rand($hundred);
            return $hundred[$rand];
        elseif ($percentage <= 99 & $percentage >= 90):
            $rand = array_rand($ninety);
            return $ninety[$rand];
        elseif ($percentage <= 89 & $percentage >= 75):
            $rand = array_rand($seventyfive);
            return $seventyfive[$rand];
        elseif ($percentage <= 74 & $percentage >= 60):
            $rand = array_rand($sixty);
            return $sixty[$rand];
        elseif ($percentage <= 59 & $percentage >= 40):
            $rand = array_rand($fifty);
            return $fifty[$rand];
        elseif ($percentage < 40):
            $rand = array_rand($belowforty);
            return $belowforty[$rand];
        endif;
        //elseif():
    }

    public function GetBuddies($user)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_BD_Connection_Id=' . Yii::app()->user->id . ' AND TM_BD_Status=0';
        $users = Buddies::model()->findAll($criteria);
        $usersjson = '';
        foreach ($users AS $key => $student):
            $studentplan = StudentPlan::model()->count(array('condition' => 'TM_SPN_PlanId=:plan AND TM_SPN_StudentId=:student', 'params' => array(':plan' => Yii::app()->session['plan'], ':student' => $student->TM_BD_Student_Id)));
            if ($studentplan > 0):
                $user = User::model()->findByPk($student->TM_BD_Student_Id);
                $usersjson .= ($usersjson == '' ? '{"id":' . $user->id . ', "name":"' . $user->username . ' (' . $user->profile->firstname . ' ' . $user->profile->lastname . ')"}' : ',{"id":' . $user->id . ', "name":"' . $user->username . ' (' . $user->profile->firstname . ' ' . $user->profile->lastname . ')"}');
            endif;
        endforeach;
        return $usersjson;

    }

    public function GetChallengeInvitees($challenge, $user)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_CE_RE_Chalange_Id=' . $challenge . ' AND TM_CE_RE_Recepient_Id!=' . $user;
        $users = Chalangerecepient::model()->findAll($criteria);
        $usersjson = 'Me';
        if (count($users) > 0):
            $othercount = count($users);
            $others = " + <a href='javascript:void(0);' class='hover' data-id='$challenge' data-tooltip='tooltip$challenge' style='color: #FFA900;' data-toggle='modal' data-target='#myinvite'>" . $othercount . " Others" . "</a>";
        else:
            $others = '';
        endif;
        /*foreach($users AS $key=>$user):
            if($key<3):
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username;
                $usersjson.=','.$challengeuser;
            endif;
        endforeach;*/
        return $usersjson . $others;

    }

    public function GetChallengeInviteesForResults($challenge, $user)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_CE_RE_Chalange_Id=' . $challenge . ' AND TM_CE_RE_Recepient_Id!=' . $user;
        $users = Chalangerecepient::model()->findAll($criteria);
        $usersjson = '';
        if (count($users) > 0):
            $othercount = count($users);
            $others = "  <a href='javascript:void(0);' class='hover' data-id='$challenge' data-tooltip='tooltip$challenge' style='color: #FFA900;' data-toggle='modal' data-target='#myinvite'>View Status</a>";
        else:
            $others = '';
        endif;
        /*foreach($users AS $key=>$user):
            if($key<3):
                $challengeuser=User::model()->findByPk($user->TM_CE_RE_Recepient_Id)->username;
                $usersjson.=','.$challengeuser;
            endif;
        endforeach;*/
        return $usersjson . $others;

    }

    public function actionGetChallengeBuddies()
    {
        if($_POST['challenge']):
            $challenge=$_POST['challenge'];
            $model=Challenges::model()->findByPk($challenge);
            $userid = Yii::app()->user->id;
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_CE_RE_Chalange_Id=' . $challenge;
            $criteria->order = 'TM_CE_RE_ObtainedMarks DESC';
            $users = Chalangerecepient::model()->findAll($criteria);
            $buddies = '';
            $userids = array();
            foreach ($users AS $party):
                $userids[] = $party->TM_CE_RE_Recepient_Id;
            endforeach;
            foreach ($users AS $key => $user):            
                if ($user->TM_CE_RE_Status == '0'):
                    if($user->TM_CE_RE_Recepient_Id == $userid):
                        $status="<span class='item-right'><span class='mrk'></span></span>";
                    elseif($model->TM_CL_Chalanger_Id==$user->TM_CE_RE_Recepient_Id):                    
                        $status="<span class='item-right'><span class='mrk'>Creator</span></span>";
                    else: 
                        $status="<span class='item-right'><span class='mrk'>Awaiting Response</span></span>";
                    endif;                                       
                    //$status = "Awaiting Response".($user->TM_CE_RE_Recepient_Id == $userid?"":"</br><a class='reminder  rem" . $user->TM_CE_RE_Recepient_Id . "'data-id='$user->TM_CE_RE_Recepient_Id' style='color: #FFA900;'>Send Reminder</a></br><span class='remsend" . $user->TM_CE_RE_Recepient_Id . "' hidden style='background-color:rgb(255,255,0)'>Reminder sent</span>");
                    if($user->TM_CE_RE_Recepient_Id != $userid & $model->TM_CL_Chalanger_Id==$userid):
                            $reminder="<span class='spsp rem" . $user->TM_CE_RE_Recepient_Id . "'>
                                		  <i class='fa fa-bell rbell'></i><a class='reminder  ' data-challenge='".$challenge."' data-id='".$user->TM_CE_RE_Recepient_Id."' style='color: #FFA900;font-size: 10px;'> Send Reminder</a>
                                		</span>
                                		<span class='spsp remsend" . $user->TM_CE_RE_Recepient_Id . "' style='color: #9E9E9E;display:none'>Reminder Sent</span>";
                    else:
                        $reminder='';
                    endif;                    
                elseif ($user->TM_CE_RE_Status == '1'):
                    if($user->TM_CE_RE_Recepient_Id == $userid):
                        $status="<span class='item-right'><span class='mrk'></span></span>";
                    elseif($model->TM_CL_Chalanger_Id==$user->TM_CE_RE_Recepient_Id):                    
                        $status="<span class='item-right'><span class='mrk'>Creator</span></span>";
                    else: 
                        $status="<span class='item-right'><span class='mrk'>Accepted</span></span>";
                    endif;                   
                    //$status = "NA".($user->TM_CE_RE_Recepient_Id == $userid?"":"</br><a class='reminder  rem" . $user->TM_CE_RE_Recepient_Id . "'data-id='$user->TM_CE_RE_Recepient_Id' style='color: #FFA900;'>Send Reminder</a></br><span class='remsend" . $user->TM_CE_RE_Recepient_Id . "' hidden style='background-color:rgb(255,255,0)'>Reminder sent</span>");
                    if($user->TM_CE_RE_Recepient_Id != $userid & $model->TM_CL_Chalanger_Id==$userid):
                            $reminder="<span class='spsp rem" . $user->TM_CE_RE_Recepient_Id . "'>
                                		  <i class='fa fa-bell rbell'></i><a class='reminder  '  data-challenge='".$challenge."'  data-id='".$user->TM_CE_RE_Recepient_Id."' style='color: #FFA900;font-size: 10px;'> Send Reminder</a>
                                		</span>
                                		<span  class='spsp remsend" . $user->TM_CE_RE_Recepient_Id . "'  style='color: #9E9E9E;display:none'>Reminder Sent</span>";
                    else:
                        $reminder='';
                    endif;   
                elseif ($user->TM_CE_RE_Status == '2'):
                    if($user->TM_CE_RE_Recepient_Id == $userid):
                        $status="<span class='item-right'><span class='mrk'></span></span>";
                    elseif($model->TM_CL_Chalanger_Id==$user->TM_CE_RE_Recepient_Id):                    
                        $status="<span class='item-right'><span class='mrk'>Creator</span></span>";
                    else: 
                        $status="<span class='item-right'><span class='mrk'>Accepted</span></span>";
                    endif;
                    //$status = "NA".($user->TM_CE_RE_Recepient_Id == $userid?"":"</br><a class='reminder  rem" . $user->TM_CE_RE_Recepient_Id . "'data-id='$user->TM_CE_RE_Recepient_Id' style='color: #FFA900;'>Send Reminder</a></br><span class='remsend" . $user->TM_CE_RE_Recepient_Id . "' hidden style='background-color:rgb(255,255,0)'>Reminder sent</span>");
                    if($user->TM_CE_RE_Recepient_Id != $userid & $model->TM_CL_Chalanger_Id==$userid):                            
                            $reminder="<span class='spsp rem" . $user->TM_CE_RE_Recepient_Id . "'>
                                		  <i class='fa fa-bell rbell'></i><a class='reminder'  data-challenge='".$challenge."'  data-id='".$user->TM_CE_RE_Recepient_Id."' style='color: #FFA900;font-size: 10px;'> Send Reminder</a>
                                		</span>
                                		<span  class='spsp remsend" . $user->TM_CE_RE_Recepient_Id . "' style='color: #9E9E9E;display:none'>Reminder Sent</span>";
                    else:
                        $reminder='';                        
                    endif;   
                elseif ($user->TM_CE_RE_Status == '3'):
                    $status = $this->GetChallengePositionPop($userids, $user->TM_CE_RE_Recepient_Id);
                elseif ($user->TM_CE_RE_Status == '4' & $user->TM_CE_RE_Recepient_Id != $userid):
                    $status="<span class='item-right'><span class='mrk'>Declined</span></span>";
                    $reminder=''; 
                    //$status = "NA".($user->TM_CE_RE_Recepient_Id == $userid?"":"</br><a class='reminder  rem" . $user->TM_CE_RE_Recepient_Id . "'data-id='$user->TM_CE_RE_Recepient_Id' style='color: #FFA900;'>Send Reminder</a></br><span class='remsend" . $user->TM_CE_RE_Recepient_Id . "' hidden style='background-color:rgb(255,255,0)'>Reminder sent</span>")                      
                endif;
                if ($user->TM_CE_RE_Recepient_Id == $userid):
                    $challengeuser = 'Me';
                else:
                    $student = Student::model()->find(array('condition' => "TM_STU_User_Id=" . $user->TM_CE_RE_Recepient_Id));
                    $challengeuser = $student->TM_STU_First_Name . " " . $student->TM_STU_Last_Name;
                    $imgname=$student->TM_STU_Image;
                endif;

                $student = Student::model()->find(array('condition' => "TM_STU_User_Id=" . $user->TM_CE_RE_Recepient_Id));
                $imgname=$student->TM_STU_Image;
                    $buddies.="
                        	<li><span class='item'><span class='item-left'>
                        				<img src='".Yii::app()->request->baseUrl."/site/Imagerender/id/".($imgname != '' ? $imgname : 'noimage.png')."?size=large&type=user&width=60&height=60' class='img-circle tab_imgsmall2'>
                        				<span class='item-info'>
                        					<span class='name_display'><b>".$challengeuser."</b></span>".$reminder."                                                                    					
                        				</span>
                        			</span>".$status."
                        		</span>
                        	</li>                    
                    ";
                    
            endforeach;
        endif;
        echo $buddies;

    }

    public function GetChallenger($id)
    {
        return Student::model()->find(array('condition' => "TM_STU_User_Id='" . $id . "'"))->TM_STU_First_Name . " " . Student::model()->find(array('condition' => "TM_STU_User_Id='" . $id . "'"))->TM_STU_Last_Name;
    }

    public function GetChallangeInviteStatus($challenge)
    {
        $user = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $challenge . ' AND TM_CE_RE_Recepient_Id!=' . $user . ' AND TM_CE_RE_Status NOT IN (0,4)');
        $model = Chalangerecepient::model()->count($criteria);
        return $model;
    }

    public function getChallengeLinks($id, $user)
    {
        $challenge = Challenges::model()->findByPk($id);
        $return = '';
        if ($challenge->TM_CL_Chalanger_Id == $user):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $challenge->TM_CL_Id . ' AND TM_CE_RE_Recepient_Id=' . $user);
            $recepient = Chalangerecepient::model()->find($criteria);
            $return .= '<td class="text-right"><span >';
            if ($recepient->TM_CE_RE_Status == '1'):
                $return .= "<a href='" . Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id)) . "' class='btn btn-warning'>Start</a>";
            elseif ($recepient->TM_CE_RE_Status == '2'):
                $return .= "<a href='" . Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id)) . "' class='btn btn-warning'>Continue</a>";
            endif;
            $return .= '</span></td>';
            $return .= '<td class="text-right">';
            if ($this->GetChallangeInviteStatus($id) > 0):
                $return .= '<span data-toggle="tooltip" data-placement="bottom" title="Cancel Challenge">' . CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/cancel.png', '', array('class' => 'hvr-buzz-out')),
                        Yii::app()->request->baseUrl . '/student/cancelchallenge/' . $challenge->TM_CL_Id,
                        array(
                            'type' => 'POST',
                            'dataType' => 'json',
                            'success' => 'function(data)                            
                                {
                                    if(data["error"]=="no")
                                        {        
                                            $("#challengerow' . $challenge->TM_CL_Id . '").remove();                                            
                                            if(data["testcount"]==0)
                                            {
                                              $("#challengelistbody").append("<tr><td colspan=\'6\' class=\'text-right\'  ><a href=\''.Yii::app()->createUrl('student/challenge').'\' class=\'btn btn-warning\'>Add</a></td><td></td></tr>")  
                                            }    
                                        }
                                }'
                        ),
                        array('confirm' => 'Are you sure you want to cancel this Challenge?')
                    ) . '</span>';
            else:
                $return .= '<span  data-toggle="tooltip" data-placement="top" title="Delete Challenge">' . CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/trash.png', '', array('class' => 'hvr-buzz-out')),
                        Yii::app()->request->baseUrl . '/student/deletechallenge/' . $challenge->TM_CL_Id,
                        array(
                            'type' => 'POST',
                            'dataType' => 'json',
                            'success' => 'function(data)                            
                                {
                                    if(data["error"]=="no")
                                        {        
                                            $("#challengerow' . $challenge->TM_CL_Id . '").remove();                                            
                                            if(data["testcount"]==0)
                                            {
                                              $("#challengelistbody").append("<tr><td colspan=\'6\' class=\'text-right\'  ><a href=\''.Yii::app()->createUrl('student/challenge').'\' class=\'btn btn-warning\'>Add</a></td><td></td></tr>")  
                                            }    
                                        }
                                }'
                        ),
                        array('confirm' => 'Are you sure you want to delete this Challenge?')
                    ) . '</span>';
            endif;
            $return .= '</td>';
        else:
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Chalange_Id=' . $challenge->TM_CL_Id . ' AND TM_CE_RE_Recepient_Id=' . $user);
            $recepient = Chalangerecepient::model()->find($criteria);

            if ($recepient->TM_CE_RE_Status == '0'):
                $return .= '<td class="text-right" ><span  data-toggle="tooltip" data-placement="bottom" title="Accept Challenge" style="padding-right: 15px;">';
                $return .= CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/right.png', '', array('class' => 'hvr-buzz-out', 'title' => 'Accept Challenge')),
                        Yii::app()->request->baseUrl . '/student/acceptchallenge/' . $challenge->TM_CL_Id,
                        array(
                            'type' => 'POST',
                            'success' => 'function(html){ if(html!="no"){$("#challengerow' . $challenge->TM_CL_Id . '").html(html);} }'
                        )
                    ) . '</span>';
                $return .= '</td>';
                $return .= '<td class="text-right">';
                $return .= '<span  data-toggle="tooltip" data-placement="bottom" title="Cancel Challenge">' . CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/cancel.png', '', array('class' => 'hvr-buzz-out', 'title' => 'Cancel Challenge')),
                        Yii::app()->request->baseUrl . '/student/cancelchallenge/' . $challenge->TM_CL_Id,
                        array(
                            'type' => 'POST',
                            'dataType' => 'json',
                            'success' => 'function(data)                            
                                {
                                    if(data["error"]=="no")
                                        {        
                                            $("#challengerow' . $challenge->TM_CL_Id . '").remove();                                            
                                            if(data["testcount"]==0)
                                            {
                                              $("#challengelistbody").append("<tr><td colspan=\'6\' class=\'text-right\'  ><a href=\''.Yii::app()->createUrl('student/challenge').'\' class=\'btn btn-warning\'>Add</a></td><td></td></tr>")  
                                            }    
                                        }
                                }'
                        ),
                        array('confirm' => 'Are you sure you want to cancel this Challenge?', 'id' => 'cancel' . $challenge->TM_CL_Id)
                    ) . '</span>';
                $return .= '</td>';
            elseif ($recepient->TM_CE_RE_Status == '1'):
                $return .= '<td class="text-right" ><span  >';
                $return .= "<a href='" . Yii::app()->createUrl('student/Startchallenge', array('id' => $challenge->TM_CL_Id)) . "' class='btn btn-warning'>Start</a>";
                $return .= '</span></td>';
                $return .= '<td class="text-right">';
                $return .= '<span data-toggle="tooltip" data-placement="bottom" title="Cancel Challenge">' . CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/cancel.png', '', array('class' => 'hvr-buzz-out')),
                        Yii::app()->request->baseUrl . '/student/cancelchallenge/' . $challenge->TM_CL_Id,
                        array(
                            'type' => 'POST',
                            'dataType' => 'json',
                            'success' => 'function(data)                            
                                {
                                    if(data["error"]=="no")
                                        {        
                                            $("#challengerow' . $challenge->TM_CL_Id . '").remove();                                            
                                            if(data["testcount"]==0)
                                            {
                                              $("#challengelistbody").append("<tr><td colspan=\'6\' class=\'text-right\'  ><a href=\''.Yii::app()->createUrl('student/challenge').'\' class=\'btn btn-warning\'>Add</a></td><td></td></tr>")  
                                            }    
                                        }
                                }'
                        ),
                        array('confirm' => 'Are you sure you want to cancel this Challenge?', 'id' => 'cancel' . $challenge->TM_CL_Id)
                    ) . '</span>';
                $return .= '</td>';
            elseif ($recepient->TM_CE_RE_Status == '2'):
                //$return .= '<td></td>';
                $return .= '<td class="text-right" colspan="2"><span style="padding-right: 15px;">';
                $return .= "<a href='" . Yii::app()->createUrl('student/ChallengeTest', array('id' => $challenge->TM_CL_Id)) . "' class='btn btn-warning'>Continue</a>";
                $return .= '</span></td>';

                $return .= '<td class="text-right">';
                $return .= '<span data-toggle="tooltip" data-placement="bottom" title="Cancel Challenge">' . CHtml::ajaxLink(CHtml::image(Yii::app()->request->baseUrl . '/images/cancel.png', '', array('class' => 'hvr-buzz-out')),
                        Yii::app()->request->baseUrl . '/student/cancelchallenge/' . $challenge->TM_CL_Id,
                        array(
                            'type' => 'POST',
                            'dataType' => 'json',
                            'success' => 'function(data)                            
                                {
                                    if(data["error"]=="no")
                                        {        
                                            $("#challengerow' . $challenge->TM_CL_Id . '").remove();                                            
                                            if(data["testcount"]==0)
                                            {
                                              $("#challengelistbody").append("<tr><td colspan=\'6\' class=\'text-right\'  ><a href=\''.Yii::app()->createUrl('student/challenge').'\' class=\'btn btn-warning\'>Add</a></td><td></td></tr>")  
                                            }    
                                        }
                                }'
                        ),
                        array('confirm' => 'Are you sure you want to cancel this Challenge?', 'id' => 'cancel' . $challenge->TM_CL_Id)
                    ) . '</span>';
                $return .= '</td>';
            elseif ($recepient->TM_CE_RE_Status == '3'):
            elseif ($recepient->TM_CE_RE_Status == '4'):
            endif;
        endif;
        return $return;
    }
    public function NextDiffCount($id, $nextdifficulty)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=' . $nextdifficulty . ' AND TM_CL_QS_Question_Id NOT IN (SELECT TM_CEUQ_Question_Id FROM tm_chalangeuserquestions WHERE TM_CEUQ_Challenge="' . $id . '" AND TM_CEUQ_User_Id="' . Yii::app()->user->id . '")';
        $criteria->limit = '1';
        $criteria->order = 'RAND()';
        $criteria->params = array(':test' => $id);
        return Chalangequestions::model()->count($criteria);

    }

    public function GetChallengePosition($users, $id)
    {
        
        $position = array_keys($users, $id);        
        $position1 = $position[0] + 1;

        if ($position1 == '1'):
            return $position1 . '<sup>st</sup><span class="fa fa-trophy size2 brd_r2"></span>';
        elseif ($position1 == '2'):
            return $position1 . '<sup>nd</sup>';
        elseif ($position1 == '3'):
            return $position1 . '<sup>rd</sup>';
        else:
            return $position1 . '<sup>th</sup>';
        endif;
    }
    public function GetChallengePositionPop($users, $id)
    {
        $position = array_keys($users, $id);
        $position1 = $position[0] + 1;
        $status="<span class='item-right'>";          
        if ($position1 == '1'):            
            $status.="<span class='cupclr'><i class='fa fa-trophy size2'></i></span><span class='mrk'>".$position1."<sup>st</sup></span>";
        elseif ($position1 == '2'):            
            $status.="<span class='mrk'>".$position1."<sup>nd</sup></span>";
        elseif ($position1 == '3'):            
            $status.="<span class='mrk'>".$position1."<sup>rd</sup></span>";
        else:
            $status.="<span class='mrk'>".$position1."<sup>th</sup></span>";            
        endif;
        	$status.='</span>';	
        return $status;
    }
public function actionSendMail()

    {
        $questionId = $_POST["questionId"];
        $comments = $_POST["comments"];

        $question = Questions::model()->findByPk($questionId);
        $sendQuestion = $question->TM_QN_Question;
        $sendReff = $question->TM_QN_QuestionReff;
        $userId = Yii::app()->user->getId();
        $userDet = User::model()->findByPk($userId);
        $studentDet = Student::model()->findAll(array('condition' => "TM_STU_User_Id='$userId'"));

        $content = '<p>A problem has been reported by ' . $studentDet->TM_STU_First_Name . $studentDet->TM_STU_Last_Name . '(' . $userDet->username . '),</p><p>' . $comments . '</p><p> Thank You</p>';
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;

        $mail->IsSMTP();
        $mail->Host = "smtpout.secureserver.net"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;
        $mail->Username = "services@tabbiemath.com"; // SMTP username
        $mail->Password = "Ta88ieMe"; // SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";

        $mail->AddReplyTo("noreply@tabbiemath.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'Problem reported Ref:' . $sendReff;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($content);
        $mail->AddAddress('support@tabbiemath.com');
        //$mail->AddAddress('karthik@solminds.com', 'Karthik');

        if (!$mail->Send()) {

            return "Mailer Error: " . $mail->ErrorInfo;

        } else {

            return success;

        }
    }
    /*Mock Related functions*/
    public function actionMock()
    {
        $plan = $this->GetStudentDet();
        $criteria = new CDbCriteria;
        $condition="";
        if(isset($_GET['search']))
        {
            $formvals=array();
            $mock=$_GET['mock'];
            if($mock!='')
            {
                $formvals['mock']=$_GET['mock'];
                $pieces = explode(" ", $mock);
                $condition .= "AND (";
                foreach($pieces as $key=>$piece)
                {
                    if($key !=0)
                        $condition .=" AND";
                    $condition .= " (TM_MK_Name LIKE '%".$piece."%'  or TM_MK_Description LIKE '%".$piece."%' or TM_MK_Tags LIKE '%".$piece."%') ";
                }
                $condition .= ")";
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Resource=:resource '.$condition.'';
                // AND TM_MK_Id NOT IN ( SELECT TM_STMK_Mock_Id FROM tm_student_mocks WHERE TM_STMK_Student_Id='.Yii::app()->user->id.')
                $criteria->params = array(':publisher' => $plan->TM_SPN_Publisher_Id, ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0',':resource' => '0');
                $criteria->order = 'TM_MK_Sort_Order DESC';
            }
            else
            {
                $criteria->condition = 'TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Resource=0';
                $criteria->params = array(':publisher' => $plan->TM_SPN_Publisher_Id, ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0');
                $criteria->order = 'TM_MK_Sort_Order DESC';
            }
        }
        else
        {
            $criteria->condition = 'TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Resource=0';
            // AND TM_MK_Id NOT IN ( SELECT TM_STMK_Mock_Id FROM tm_student_mocks WHERE TM_STMK_Student_Id='.Yii::app()->user->id.')
            $criteria->params = array(':publisher' => $plan->TM_SPN_Publisher_Id, ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0');
            $criteria->order = 'TM_MK_Sort_Order DESC';
        }
        if(isset($_GET['tags'])):
            $criteria->condition = 'TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Resource=0 AND (TM_MK_Tags REGEXP "'.$_GET['tags'].'" OR TM_MK_Name LIKE "%'.$_GET['tags'].'%" OR TM_MK_Description LIKE "%'.$_GET['tags'].'%")';
            $criteria->params = array(':publisher' => $plan->TM_SPN_Publisher_Id, ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0');
            $criteria->order = 'TM_MK_Sort_Order DESC';
        endif;
        ///echo $criteria->condition;exit;
        $mocks = Mock::model()->findAll($criteria);
        $mockheaders=MockTags::model()->findAll(array('condition'=>'TM_MT_Type=0 AND TM_MT_Standard_Id='.$plan->TM_SPN_StandardId.' '));
        $this->render('mocks', array('mocks' => $mocks,'formvals'=>$formvals,'mockheaders'=>$mockheaders));
    }

    public function actionStartmock($id)
    {
        $test = Mock::model()->findByPk($id);
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_MQ_Mock_Id=' . $id . ' AND TM_MQ_Type NOT IN (6,7)';
        $totalonline = MockQuestions::model()->count($criteria);
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_MQ_Mock_Id=' . $id . ' AND TM_MQ_Type IN (6,7)';
        $totalpaper = MockQuestions::model()->count($criteria);
        $totalquestions = $totalonline + $totalpaper;
        if (isset($_POST['startTest'])):
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_MQ_Mock_Id=:mock';
            $criteria->order = 'TM_MQ_Order ASC';
            $criteria->params = array(':mock' => $id);

            $mockquestions = MockQuestions::model()->findAll($criteria);
            $studentmock = new StudentMocks();
            $studentmock->TM_STMK_Student_Id = Yii::app()->user->id;
            $studentmock->TM_STMK_Mock_Id = $id;
            if ($totalquestions == $totalpaper):
                $studentmock->TM_STMK_Status = '2';
            else:
                $studentmock->TM_STMK_Status = '1';
            endif;
            
            $studentmock->TM_STMK_Plan_Id = Yii::app()->session['plan'];
            $studentmock->save(false);
            foreach ($mockquestions AS $key => $mockquestion):
                $studentmockquestion = new Studentmockquestions();
                $studentmockquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                $studentmockquestion->TM_STMKQT_Mock_Id = $studentmock->TM_STMK_Id;
                $studentmockquestion->TM_STMKQT_Type = $mockquestion->TM_MQ_Type;
                $studentmockquestion->TM_STMKQT_Question_Id = $mockquestion->TM_MQ_Question_Id;
                $studentmockquestion->TM_STMKQT_Order=$mockquestion->TM_MQ_Order;
                $studentmockquestion->TM_STMKQT_Status == '0';
                $studentmockquestion->save(false);
            endforeach;
            if ($totalquestions == $totalpaper):
                $this->redirect(array('Showmockpaper', 'id' => $studentmock->TM_STMK_Id));
            else:
                $this->redirect(array('Mocktest', 'id' => $studentmock->TM_STMK_Id));
            endif;

        endif;

        $this->render('startmock', array('id' => $id, 'test' => $test, 'totalquestions' => $totalquestions, 'totalonline' => $totalonline, 'totalpaper' => $totalpaper));
    }

    public function actionMocktest($id)
    {
        if (isset($_POST['action']['GoNext'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id = $id;
                    $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Id>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Order ASC, TM_STMKQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid'])
                )
            );
        }
        if (isset($_POST['action']['Save'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id = $id;
                    $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Id>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Order ASC, TM_STMKQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid']-1)
                )
            );
        }
        if (isset($_POST['action']['GetPrevios'])):

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id = $id;
                    $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0,1,2) AND TM_STMKQT_Type NOT IN (6,7) AND TM_STMKQT_Id<:tableid AND TM_STMKQT_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Order DESC, TM_STMKQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid'])
                )
            );
        endif;
        if (isset($_POST['action']['DoLater'])):
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_Status = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_Status = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status = '2';
                $selectedquestion->save(false);
            endif;
            $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Id>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Order ASC, TM_STMKQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid'])
                )
            );
        endif;

        if (isset($_POST['action']['Complete'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    if(count($_POST['answer'])>0)
                    {
                        $answer = implode(',', $_POST['answer']);   
                    }   
                    else
                    {
                        $answer='';
                    }                     
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STMKQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studentmockquestions::model()->findByAttributes(
                        array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studentmockquestions();
                    endif;
                    $selectedquestion->TM_STMKQT_Mock_Id = $id;
                    $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STMKQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            if ($this->GetPaperCountMock($id) != 0):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMK_Mock_Id=:mock AND TM_STMK_Student_Id=:student';
                $criteria->params = array(':mock' => $id, ':student' => Yii::app()->user->id);
                $test = StudentMocks::model()->find($id);
                $test->TM_STMK_Status = '2';
                $test->save(false);
                $this->redirect(array('showmockpaper', 'id' => $id));
            else:
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMK_Mock_Id=:mock AND TM_STMK_Student_Id=:student';
                $criteria->params = array(':mock' => $id, ':student' => Yii::app()->user->id);
                $test = StudentMocks::model()->find($id);
                $test->TM_STMK_Status = '3';
                $test->save(false);
                $this->redirect(array('mockcomplete', 'id' => $id));
            endif;
            //$this->redirect(array('TestComplete','id'=>$id));
        }
        if (!isset($_POST['action'])):
            if (isset($_GET['questionnum'])):            
                $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                    array(
                        'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0,1,2) AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Question_Id=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                        'limit' => 1,
                        'order' => 'TM_STMKQT_Question_Id ASC, TM_STMKQT_Status ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            else:
                $test = StudentMocks::model()->find($id);
                $test->TM_STMK_Status = '1';
                $test->save(false);
                $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                    array(
                        'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Status IN (0) AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Parent_Id=0',
                        'limit' => 1,
                        'order' => 'TM_STMKQT_Order ASC, TM_STMKQT_Status ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            endif;
        endif;
        if (count($questionid)):
            $question = Questions::model()->findByPk($questionid->TM_STMKQT_Question_Id);
            $this->renderPartial('mocktest', array('testid' => $id, 'question' => $question, 'questionid' => $questionid));
        else:
            $this->redirect(array('mock'));
        endif;
    }

    public function actionMockComplete($id)
    {
        $paperquestion=count($this->GetPaperQuestionsMock($id));
        $totalquestions=Studentmockquestions::model()->count(array(
                'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id),                
            ));


        if($totalquestions!=$paperquestion):
            $results = Studentmockquestions::model()->findAll(
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type NOT IN(6,7)',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id),
                    'order'=>'TM_STMKQT_Order ASC'
                )
            );            
            $totalmarks = 0;
            $earnedmarks = 0;
            $result = '';
            $marks = 0;
            $questionnumber=1;
            foreach ($results AS $questionkey => $exam):
                //$questionnumber = $questionkey + 1;
                $question = Questions::model()->findByPk($exam->TM_STMKQT_Question_Id);
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5'):
                        $displaymark = 0;
                        $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
    
                        foreach ($marksdisp AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
    
                        endforeach;
                        $marks = 0;
                        if ($question->TM_QN_Type_Id != '4' & $exam->TM_STMKQT_Flag != '2'):
                            if ($exam->TM_STMKQT_Answer != ''):
    
                                $studentanswer = explode(',', $exam->TM_STMKQT_Answer);
                                foreach ($question->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++) {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($exam->TM_STMKQT_Answer != ''):
                                $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                                $correctanswer .= '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_STMKQT_Answer . '</div></li></ul>';
                                foreach ($question->answers AS $ans):
                                    $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer = trim(strtolower($exam->TM_STMKQT_Answer));
                                    for ($i = 0; $i < count($answeroption); $i++) {
                                        $option = trim($answeroption[$i]);
                                        if($option=='&gt'):
                                            $newoption='&gt;';
                                        elseif($option=='&lt'):
                                            $newoption='&lt;';
                                        else:
                                            $newoption=$option;
                                        endif;
                                        if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
    
                        endif;
                        if ($question->TM_QN_Parent_Id == 0):
                            $result .= '<tr><td><div class="col-md-4">';
                            if ($marks == '0'):
                                $exam->TM_STMKQT_Redo_Flag = '1';
                                $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STMKQT_Order . '</span>';
    
                            elseif ($marks < $displaymark & $marks != '0'):
                                $exam->TM_STMKQT_Redo_Flag = '1';
                                $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STMKQT_Order . '</span>';
                            else:
                                $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STMKQT_Order . '</span>';
                            endif;
    
                            $result .= ' Mark: ' . $marks . '/' . $displaymark . '</div>
                                <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                                <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';
    
                            //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                            $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                            $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $result .= ' <div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div><div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div>';
                            $result .= '<div class="col-md-12 Itemtest clickable-row showalert' . $question->TM_QN_Id . '" >
    
    								<div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                    <div class="col-md-10" id="maildivslidetest"  >
                                    <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                    </div>
                                    <div class="col-md-2 mailadd" style="margi-top:15px;">
                                    <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                    </div>
                                  </div>
                                     <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id=""  hidden >Your message has been sent to admin
                                    </div>  </div>
                                 </td></tr>';
    
                            //glyphicon glyphicon-star sty
                        endif;
    
                        $exam->TM_STMKQT_Marks = $marks;
                        $exam->save(false);
                        $earnedmarks = $earnedmarks + $marks;
    
                    else:
                        $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $displaymark = 0;
                        $singlemark = '';
                        foreach ($marksQuestions AS $key => $formarks):
    
                            $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                            foreach ($gettingmarks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                                $singlemark = $marksdisplay->TM_AR_Marks;
                            endforeach;
    
    
    
                        endforeach;
    
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $count = 1;
                        $childresult = '';
                        $childearnedmarks = 0;
                        foreach ($questions AS $childquestion):
                            $childresults = Studentmockquestions::model()->find(
                                array(
                                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Question_Id=:questionid',
                                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                                )
                            );
                            $marks = 0;
                            if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_STMKQT_Flag != '2'):
                                if ($childresults->TM_STMKQT_Answer != ''):
    
                                    $studentanswer = explode(',', $childresults->TM_STMKQT_Answer);
                                    foreach ($childquestion->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    if (count($studentanswer) > 0):
                                        $correctanswer = '<ul class="list-group"><lh><b>Your Answer:</b></lh>';
                                        for ($i = 0; $i < count($studentanswer); $i++) {
                                            $answer = Answers::model()->findByPk($studentanswer[$i]);
                                            $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';
    
                                        }
                                        $correctanswer .= '</ul>';
                                    endif;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                                endif;
                            else:
                                if ($childresults->TM_STMKQT_Answer != ''):
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_STMKQT_Answer . '</li></ul>';
                                    foreach ($childquestion->answers AS $ans):
                                        $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                        $useranswer = trim(strtolower($childresults->TM_STMKQT_Answer));
                                        for ($i = 0; $i < count($answeroption); $i++) {
                                            $option = trim($answeroption[$i]);
                                            //solution for < > issue
                                            if($option=='&gt'):
                                                $newoption='&gt;';
                                            elseif($option=='&lt'):
                                                $newoption='&lt;';
                                            else:
                                                $newoption=$option;
                                            endif;
                                          //fix for multiple options marking
                                          
                                            // if (strcmp($useranswer, $option) == 0):                                            
                                            // if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0):
                                            //     $marks = $marks + $ans->TM_AR_Marks;
                                            // endif;
                                            $optionarray[]=htmlspecialchars_decode($newoption);
                                        }
                                         if (in_array($useranswer, $optionarray)){
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }

                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                                endif;
    
                            endif;
                            if ($childquestion->TM_QN_Parent_Id != 0):
                                $childresult .= '<div class="col-md-12">';
                                if ($marks == '0'):
                                    //$exam->TM_STHWQT_Redo_Flag = '1';
                                    $childresult .= '<span class="clrr"><i class="fa fa fa-times"></i><b> Part ' . $count . '</b></span>';
                                elseif ($marks < $singlemark & $marks != '0'):
                                    //$exam->TM_STHWQT_Redo_Flag = '1';
                                    $childresult .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"><b> Part ' . $count . '</b></span>';
                                else:
                                    $childresult .= '<span ><i class="fa fa fa-check"></i><b> Part ' . $count . '</b></span>';
                                endif;
                                $childresult.='<li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                                $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                                $childresult .= '';
                                //glyphicon glyphicon-star sty
                            endif;
    
    
                            $earnedmarks = $earnedmarks + $marks;
                            $childearnedmarks = $childearnedmarks + $marks;
                            $count++;
                        endforeach;
                        $marks = $childearnedmarks;
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_STMKQT_Redo_Flag = '1';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STMKQT_Order . '</span>';
                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_STMKQT_Redo_Flag = '1';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question' . $exam->TM_STMKQT_Order . '</span>';
                        else:
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STMKQT_Order . '</span>';
                        endif;
    
                        $result .= ' Mark: ' . $marks . '/' . $displaymark . '</span></div>
                                <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                                <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';
    
                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= $childresult;
                        $result.= '<div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div>
                        <div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div></td></tr>';
                        $result .= '<tr><td><div class="Itemtest clickable-row "   >
                                <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                    <div class="col-md-10" id="maildivslidetest"  >
                                    <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                    </div>
                                    <div class="col-md-2" style="margi-top:15px;">
                                    <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                    </div>
                                    <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                    </div>
    
                                </div></div>
    </td></tr>';
                        $exam->TM_STMKQT_Marks = $marks;
                        $exam->save(false);
                    endif;
    
    
                endif;
    $questionnumber++;
            endforeach;
            $percentage = ($earnedmarks / $totalmarks) * 100;
            $test = StudentMocks::model()->findByPk($id);
            $test->TM_STMK_Marks = $earnedmarks;
            $test->TM_STMK_TotalMarks = $totalmarks;
            $test->TM_STMK_Percentage = round($percentage, 1);
            $test->TM_STMK_Status = 3;
            $test->save(false);
            $pointlevels = $this->AddPoints($earnedmarks, $id, 'Mock', Yii::app()->user->id);
            $this->render('showmockresult', array('test' => $test, 'testresult' => $result, 'pointlevels' => $pointlevels, 'id' => $id));
        else:
            $test = StudentMocks::model()->findByPk($id);
            $test->TM_STMK_Status = '3';
            $test->save(false);
            $result = '<h1 class="h125" style="text-align: center">This is a paper only test. Hence there is no automated correction.<br> Click on view solutions to check the answers. </h1>';
            $this->render('showmockpaperonlyresult', array('test' => $test, 'testresult' => $result));        
        endif;
    }

    public function actionDeletemock($id)
    {
        $returnarray=array();
        $studentmock = StudentMocks::model()->findByPk($id);
        if ($studentmock->delete()) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMKQT_Student_Id=' . Yii::app()->user->id . ' AND TM_STMKQT_Mock_Id=' . $id);
            $model = Studentmockquestions::model()->deleteAll($criteria);
            $testcount=StudentMocks::model()->with('mocks')->count(array('condition' => "TM_STMK_Student_Id='" . Yii::app()->user->id . "' AND TM_STMK_Status !=3 AND TM_MK_Status='0' AND  TM_STMK_Plan_Id='" . Yii::app()->session['plan'] . "'"));
            $returnarray['error']='no';
            $returnarray['testcount']=$testcount;      
        } else {
            $returnarray['error']='yes';            
        }
        echo json_encode($returnarray);
    }

    public function actionDeletehomework($id)
    {
        $returnarray=array();
        $studenthw = StudentHomeworks::model()->findByPk($id);
        if ($studenthw->delete()) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STHWQT_Student_Id=' . Yii::app()->user->id . ' AND TM_STHWQT_Mock_Id=' . $studenthw->TM_STHW_HomeWork_Id);
            $model = Studenthomeworkquestions::model()->deleteAll($criteria);
            $returnarray['error']='no';
        } else {
            $returnarray['error']='yes';
        }
        echo json_encode($returnarray);
    }


    // view history

    public function actionHistory()
    {

        if (Yii::app()->session['challenge']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Recepient_Id=' . Yii::app()->user->id . ' AND TM_CE_RE_Status=3 AND TM_CL_Plan=' . Yii::app()->session['plan']);
            $criteria->order = 'TM_CE_RE_Chalange_Id DESC';
            $criteria->limit = '50';
            $challenges = Chalangerecepient::model()->with('challenge')->findAll($criteria);
        endif;
        if (Yii::app()->session['quick'] || Yii::app()->session['difficulty']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_TT_Student_Id=' . Yii::app()->user->id . ' AND TM_STU_TT_Status IN(3,4) AND TM_STU_TT_Plan=' . Yii::app()->session['plan']);
            $criteria->order = 'TM_STU_TT_CreateOn DESC';
            $criteria->limit = '50';
            $totaltests = StudentTest::model()->findAll($criteria);
        endif;
        if (Yii::app()->session['mock']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMK_Student_Id=' . Yii::app()->user->id . ' AND TM_STMK_Status IN (3,4) AND TM_MK_Status=0 AND  TM_STMK_Plan_Id=' . Yii::app()->session['plan']);
            $criteria->order = 'TM_STMK_Date DESC';
            $criteria->limit = '50';
            $totalmocks = StudentMocks::model()->with('mocks')->findAll($criteria);
        endif;
        if (Yii::app()->session['school']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STHW_Student_Id=' . Yii::app()->user->id . ' AND TM_STHW_Status IN (4,5) AND TM_STHW_Plan_Id=' . Yii::app()->session['plan']);
            $criteria->order = 'TM_STHW_Date DESC';
            $criteria->limit = '50';
            $totalhomeworks = StudentHomeworks::model()->with('homework')->findAll($criteria);
        endif;
        $this->render('viewhistory', array('challenges' => $challenges, 'totaltests' => $totaltests, 'totalmocks' => $totalmocks,'totalhomeworks'=>$totalhomeworks));
    }

    // view history ends
    public function GetTotal($id)
    {
        $criteria = new CDbCriteria;
        $criteria->select = array('SUM(TM_AR_Marks) AS totalmarks');
        $criteria->condition = 'TM_AR_Question_Id=:question';
        $criteria->params = array(':question' => $id);
        $totalmarks = Answers::model()->find($criteria);
        return $totalmarks->totalmarks;
    }

    public function actionGetpdfAnswer($id)
    {
        $results = StudentQuestions::model()->findAll(
            array(
                'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test ',
                "order" => 'TM_STU_QN_Number ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $totalquestions = count($results);
        $html = "<table cellpadding='5' border='0' width='100%' >";
        $html .= "<tr><td colspan=3 align='center'><b><u>REVISION SOLUTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";


        foreach ($results AS $key => $result):
            $count = $key + 1;
            $question = Questions::model()->findByPk($result->TM_STU_QN_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Type_Id == '1' || $question->TM_QN_Type_Id == '2' || $question->TM_QN_Type_Id == '3' || $question->TM_QN_Type_Id == '4'):
                    $html .= "<tr><td width='15%'><b>Question ".$result->TM_STU_QN_Number."</b></td><td width='15%'> Marks: ".$displaymark."</td><td width='70%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' width='100%'>".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;               
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '6'):
                    $html .= "<tr><td width='15%'><b>Question " . $result->TM_STU_QN_Number ."</b></td><td width='15%' align='left'>Marks: ".$question->TM_QN_Totalmarks."</td><td width='70%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '5'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childhtml = '';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;

                        $childhtml .= "<tr><td colspan='3' >Part " . $count."</td></tr>";
                        $childhtml .= "<tr><td colspan='3'>".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $count++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question ".$result->TM_STU_QN_Number."</b></td><td width='15%'> Marks: ".$displaymark."</td><td width='70%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";                    
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):

                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    $childhtml = '';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));


                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .= "<tr><td colspan='3' >Part " . $count."</td></tr>";
                        $childhtml .= "<tr><td colspan='3'>".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $count++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question ".$result->TM_STU_QN_Number."</b></td><td width='15%'> Marks: ".$displaymark."</td><td width='70%' align='right'>Ref :" . $question->TM_QN_QuestionReff . "</td></tr>";                    
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;


            endif;
        endforeach;
        $html .= "</table>";         
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf;">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $firstname . ' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $username . '</td>
</tr></table>';
        /*require_once(dirname(__FILE__).'../../../ckeditor/plugins/ckeditor_wiris/integration/pluginbuilder.php');
        $text = $pluginBuilder->newTextService();        
        $params = ["dpi"=>"500"];
        $html = $text->filter($html, $params);*/
        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);

        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style="font-weight: lighter; color: #afafaf;" align="center">TabbieMe Educational Solutions PVT Ltd.</span></td>

</tr></table>
');
        $name='REVISION_SOLUTIONS_'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');


    }

    public function actionInvitebuddies()
    {
        $user = Yii::app()->user->id;
        if (count($_POST['invitebuddies']) > 0):
            for ($i = 0; $i < count($_POST['invitebuddies']); $i++):
                $getcount=Chalangerecepient::model()->count(array('condition'=>"TM_CE_RE_Chalange_Id='".$_POST['challengeid']."' AND TM_CE_RE_Recepient_Id='".$_POST['invitebuddies'][$i]."'"));
                if($getcount==0):
                    $recepchalange = new Chalangerecepient();
                    $recepchalange->TM_CE_RE_Chalange_Id = $_POST['challengeid'];
                    $recepchalange->TM_CE_RE_Recepient_Id = $_POST['invitebuddies'][$i];
                    if ($recepchalange->save(false)):
                        //$this->SendMailChallengeInvite($_POST['invitebuddies'][$i]);
                    endif;
                    $notification = new Notifications();
                    $notification->TM_NT_User = $_POST['invitebuddies'][$i];
                    $notification->TM_NT_Type = 'Challenge';
                    $notification->TM_NT_Target_Id = Yii::app()->user->Id;
                    $notification->TM_NT_Status = '0';
                    $notification->save(false);
                endif;                    
            endfor;
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_CE_RE_Chalange_Id=' . $_POST['challengeid'] . ' AND TM_CE_RE_Recepient_Id!=' . $user;
            $users = Chalangerecepient::model()->findAll($criteria);
            $usersjson = 'Me';
            if (count($users) > 0):
                $othercount = count($users);
                $others = " + <a href='javascript:void(0);' class='hover' data-id='" . $_POST['challengeid'] . "' data-tooltip='tooltip" . $_POST['challengeid'] . "' style='color: #FFA900;'  data-toggle='modal' data-target='#myinvite' >" . $othercount . " Others" . "</a>";
            else:
                $others = '';
            endif;
            
            echo $usersjson . $others;
        else:
            echo "no";
        endif;
    }

    public function actionShowmockpaper($id)
    {
        if (isset($_POST['coomplete'])) {
            //$test=StudentMocks::model()->find(array('condition'=>'TM_STMK_Mock_Ida=:mock AND TM_STMK_Student_Id=:student','params'=>array(':mock'=>$id,':student'=>Yii::app()->user->Id)));
            $test = StudentMocks::model()->findByPk($id);
            $test->TM_STMK_Status = '3';
            $test->save(false);
            $paperquestions=Studentmockquestions::model()->findAll(array('condition'=>'TM_STMKQT_Mock_Id='.$id.' AND TM_STMKQT_Type IN(6,7)'));
            foreach($paperquestions AS $paper):
                $paper->TM_STMKQT_Flag=1;
                $paper->save(false);
            endforeach;
            $this->redirect(array('mockcomplete', 'id' => $id));
        }
        $paperquestions = $this->GetPaperQuestionsMock($id);
        $this->render('showpaperquestionmock', array('id' => $id, 'paperquestions' => $paperquestions));
    }

    public function actionRedo()
    {

        if (isset($_POST['deleteflag'])):
            $questionId = $_POST['questonid'];
            $questionType = $_POST['type'];
            $testId = $_POST['testid'];
            $redoStudent = StudentQuestions::model()->find(array('condition' => "TM_STU_QN_Question_Id='$questionId'AND TM_STU_QN_Test_Id='$testId'"));
            $redoStudent->TM_STU_QN_Redo_Flag = $_POST['deleteflag'];
            if ($_POST['deleteflag'] == '1'):
                $redoStudent->TM_STU_QN_Flag = 0;
            else:
                $redoStudent->TM_STU_QN_Flag = 1;
            endif;
            $redoStudent->save(false);
        endif;
    }

    public function actionRevisionRedo($id)
    {
        $mode = ($_GET['redo'] == '' ? $_POST['redo'] : $_GET['redo']);
        $test = StudentTest::model()->findByPk($id);        
        if ($test->TM_STU_TT_Status == '3'):
            $test->TM_STU_TT_Status = '4';
            $test->TM_STU_TT_Redo_Action = $mode;
            if ($mode == 'all'):
                $redoquestions = StudentQuestions::model()->findAll(array('condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test', 'params' => array(':student' => Yii::app()->user->id, ':test' => $id)));
            else:
                $redoquestions = StudentQuestions::model()->findAll(array('condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Redo_Flag=1', 'params' => array(':student' => Yii::app()->user->id, ':test' => $id)));
            endif;
            foreach ($redoquestions AS $rdq):
                $question = Questions::model()->findByPk($rdq->TM_STU_QN_Question_Id);
                if ($question->TM_QN_Type_Id == '5'):
                    $childquestion = StudentQuestions::model()->findAll(array('condition' => 'TM_STU_QN_Parent_Id=:parent', 'params' => array(':parent' => $rdq->TM_STU_QN_Question_Id)));
                    foreach ($childquestion AS $child):
                        $child->TM_STU_QN_Answer_Id = '';
                        $child->TM_STU_QN_Answer = '';
                        $child->save(false);
                    endforeach;
                endif;
                $rdq->TM_STU_QN_Answer_Id = '';
                $rdq->TM_STU_QN_Answer = '';
                $rdq->TM_STU_QN_Mark = '';
                $rdq->TM_STU_QN_Flag = '0';
                $rdq->TM_STU_QN_RedoStatus='0';
                $rdq->save(false);
            endforeach;
            $test->save(false);
        elseif($test->TM_STU_TT_Status == '4' & $test->TM_STU_TT_Redo_Action!=$mode):
            $test->TM_STU_TT_Redo_Action = $mode;
            /*if ($mode == 'all'):
                $redoquestions = StudentQuestions::model()->findAll(array('condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test', 'params' => array(':student' => Yii::app()->user->id, ':test' => $id)));
            else:
                $redoquestions = StudentQuestions::model()->findAll(array('condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Redo_Flag=1', 'params' => array(':student' => Yii::app()->user->id, ':test' => $id)));
            endif;*/
            $redoquestions = StudentQuestions::model()->findAll(array('condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test', 'params' => array(':student' => Yii::app()->user->id, ':test' => $id)));
            foreach ($redoquestions AS $rdq):
                $question = Questions::model()->findByPk($rdq->TM_STU_QN_Question_Id);
                if ($question->TM_QN_Type_Id == '5'):
                    $childquestion = StudentQuestions::model()->findAll(array('condition' => 'TM_STU_QN_Parent_Id=:parent', 'params' => array(':parent' => $rdq->TM_STU_QN_Question_Id)));
                    foreach ($childquestion AS $child):
                        $child->TM_STU_QN_Answer_Id = '';
                        $child->TM_STU_QN_Answer = '';
                        $child->save(false);
                    endforeach;
                endif;
                $rdq->TM_STU_QN_Answer_Id = '';
                $rdq->TM_STU_QN_Answer = '';
                $rdq->TM_STU_QN_Mark = '';
                $rdq->TM_STU_QN_Flag = '0';
                $rdq->TM_STU_QN_RedoStatus='0';
                $rdq->save(false);
            endforeach;
            $test->save(false);
        endif;
        $condition = '';

        if (isset($_POST['action']['GoNext'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            if ($mode == 'all'):
                $condition = array(
                    'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                    'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                );
            else:
                $condition = array(
                    'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_RedoStatus IN (0,1) AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                    'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                );
            endif;
        }
        if (isset($_POST['action']['Save'])) {
        if ($_POST['QuestionType'] != '5'):
            if (isset($_POST['answer'])):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            else:
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
        else:
            $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
            foreach ($questions AS $child):
                if (isset($_POST['answer' . $child->TM_QN_Id])):
                    $childquestion = StudentQuestions::model()->findByAttributes(
                        array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new StudentQuestions();
                    endif;
                    $selectedquestion->TM_STU_QN_Test_Id = $id;
                    $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endif;
            endforeach;
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
            $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
            $selectedquestion = StudentQuestions::model()->find($criteria);
            $selectedquestion->TM_STU_QN_RedoStatus = '1';
            $selectedquestion->save(false);
        endif;
        if ($mode == 'all'):
            $condition = array(
                'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                'limit' => 1,
                'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber']-1)
            );
        else:
            $condition = array(
                'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_RedoStatus=1 AND TM_STU_QN_Number=:tableid',
                'limit' => 1,
                'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
            );

        endif;
    }
        if (isset($_POST['action']['GetPrevios'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            if ($mode == 'all'):
                $condition = array(
                    'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Number <:tableid AND TM_STU_QN_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number DESC',
                    'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                );
            else:
                $condition = array(
                    'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Number <:tableid AND TM_STU_QN_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number DESC',
                    'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                );
            endif;
        }
        if (isset($_POST['action']['DoLater'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_RedoStatus = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_RedoStatus = '2';
                $selectedquestion->save(false);
            endif;
            if ($mode == 'all'):
                $condition = array(
                    'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                    'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                );
            else:
                $condition = array(
                    'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND  TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Number>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STU_QN_Number ASC, TM_STU_QN_RedoStatus ASC',
                    'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
                );
            endif;
        }
        if (isset($_POST['action']['Complete'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STU_QN_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = StudentQuestions::model()->find($criteria);
                    $selectedquestion->TM_STU_QN_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = StudentQuestions::model()->findByAttributes(
                            array('TM_STU_QN_Question_Id' => $child->TM_QN_Id, 'TM_STU_QN_Test_Id' => $id, 'TM_STU_QN_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new StudentQuestions();
                        endif;
                        $selectedquestion->TM_STU_QN_Test_Id = $id;
                        $selectedquestion->TM_STU_QN_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STU_QN_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STU_QN_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STU_QN_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STU_QN_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STU_QN_Question_Id=:question AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = StudentQuestions::model()->find($criteria);
                $selectedquestion->TM_STU_QN_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $test = StudentTest::model()->findByPk($id);
            $test->TM_STU_TT_Status = '3';
            $test->save(false);
            $this->redirect(array('RedoComplete', 'id' => $id, 'mode' => $mode));
        }
        if (isset($_GET['questionnum']) && $condition == ''):
            if ($mode == 'all'):
                $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                    array(
                        'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                        'limit' => 1,
                        'order' => 'TM_STU_QN_Number ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            else:
                $questionid = StudentQuestions::model()->findByAttributes(array('TM_STU_QN_Test_Id' => $id),
                    array(
                        'condition' => 'TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Number=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                        'limit' => 1,
                        'order' => 'TM_STU_QN_Number ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            endif;
        else:

            if ($condition == ''):
                if ($mode == 'all'):
                    $condition = array(
                        'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student  AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Parent_Id=0',
                        'limit' => 1,
                        'order' => 'TM_STU_QN_RedoStatus ASC,TM_STU_QN_Number ASC',
                        'params' => array(':test' => $id, ':student' => Yii::app()->user->id)
                    );
                else:
                    $condition = array(
                        'condition' => 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Student_Id=:student  AND TM_STU_QN_Type NOT IN (6,7)  AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_RedoStatus IN (0,1) AND TM_STU_QN_Parent_Id=0',
                        'limit' => 1,
                        'order' => ' TM_STU_QN_RedoStatus ASC, TM_STU_QN_Number ASC',
                        'params' => array(':test' => $id, ':student' => Yii::app()->user->id)
                    );
                endif;

            endif;
            $questionid = StudentQuestions::model()->find($condition);

        endif;
        if (count($questionid)):
            $question = Questions::model()->findByPk($questionid->TM_STU_QN_Question_Id);

            $this->renderPartial('showquestionredo', array('testid' => $id, 'question' => $question, 'questionid' => $questionid, 'mode' => $mode));
        else:
            //$this->redirect(array('revision'));
        endif;

    }

    public function actionMockRedo($id)
    {
        $mode = ($_GET['redo'] == '' ? $_POST['redo'] : $_GET['redo']);
        $test = StudentMocks::model()->findByPk($id);
        if ($test->TM_STMK_Status == '1'):
            $test->TM_STMK_Status = '2';
            $test->TM_STMK_Redo_Action = $mode;
            if ($mode == 'all'):
                $redoquestions = Studentmockquestions::model()->findAll(array('condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test', 'params' => array(':student' => Yii::app()->user->id, ':test' => $id)));
            else:
                $redoquestions = Studentmockquestions::model()->findAll(array('condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Redo_Flag=1', 'params' => array(':student' => Yii::app()->user->id, ':test' => $id)));
            endif;
            foreach ($redoquestions AS $rdq):
                $question = Questions::model()->findByPk($rdq->TM_STMKQT_Question_Id);
                if ($question->TM_QN_Type_Id == '5'):
                    $childquestion = Studentmockquestions::model()->findAll(array('condition' => 'TM_STMKQT_Parent_Id=:parent', 'params' => array(':parent' => $rdq->TM_STMKQT_Question_Id)));
                    foreach ($childquestion AS $child):
                        $child->TM_STMKQT_Answer = '';
                        $child->save(false);
                    endforeach;endif;
                $rdq->TM_STMKQT_Answer = '';
                $rdq->TM_STMKQT_Marks = '';
                $rdq->TM_STMKQT_Flag = '0';
                $rdq->TM_STMKQT_Redo_Flag = '1';
                $rdq->TM_STMKQT_RedoStatus = '0';
                $rdq->save(false);
            endforeach;
            $test->save(false);
        endif;
        $condition = '';

        if (isset($_POST['action']['GoNext'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $condition = array(
                'condition' => 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_RedoStatus=0 AND TM_STMKQT_Number>:tableid',
                'limit' => 1,
                'order' => 'TM_STMKQT_Number ASC, TM_STMKQT_RedoStatus ASC',
                'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['GetPrevios'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $condition = array(
                'condition' => 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_Number <:tableid AND TM_STMKQT_Parent_Id=0',
                'limit' => 1,
                'order' => 'TM_STMKQT_Number DESC',
                'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['DoLater'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '2';
                $selectedquestion->save(false);
            endif;
            $condition = array(
                'condition' => 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND  TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_Number>:tableid',
                'limit' => 1,
                'order' => 'TM_STMKQT_Number ASC, TM_STMKQT_RedoStatus ASC',
                'params' => array(':test' => $id, ':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionNumber'])
            );
        }
        if (isset($_POST['action']['Complete'])) {
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STMKQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studentmockquestions::model()->find($criteria);
                    $selectedquestion->TM_STMKQT_RedoStatus = '1';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studentmockquestions::model()->findByAttributes(
                            array('TM_STMKQT_Question_Id' => $child->TM_QN_Id, 'TM_STMKQT_Mock_Id' => $id, 'TM_STMKQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studentmockquestions();
                        endif;
                        $selectedquestion->TM_STMKQT_Mock_Id = $id;
                        $selectedquestion->TM_STMKQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STMKQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STMKQT_Answer_Id = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STMKQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STMKQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STMKQT_Question_Id=:question AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studentmockquestions::model()->find($criteria);
                $selectedquestion->TM_STMKQT_RedoStatus = '1';
                $selectedquestion->save(false);
            endif;
            $test = StudentTest::model()->findByPk($id);
            $test->TM_STU_TT_Status = '1';
            $test->save(false);
            $this->redirect(array('RedoComplete', 'id' => $id, 'mode' => $mode));
        }
        if (isset($_GET['questionnum']) && $condition == ''):
            $questionid = Studentmockquestions::model()->findByAttributes(array('TM_STMKQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Number=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Number ASC',
                    'params' => array(':student' => Yii::app()->user->id)
                )
            );
        else:

            if ($condition == ''):
                $condition = array(
                    'condition' => 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Student_Id=:student  AND TM_STMKQT_Type NOT IN (6,7)  AND TM_STMKQT_Redo_Flag=1 AND TM_STMKQT_RedoStatus=0 AND TM_STMKQT_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_STMKQT_Number ASC',
                    'params' => array(':test' => $id, ':student' => Yii::app()->user->id)
                );

            endif;
            $questionid = Studentmockquestions::model()->find($condition);

        endif;
        if (count($questionid)):
            $question = Questions::model()->findByPk($questionid->TM_STMKQT_Question_Id);

            $this->renderPartial('showquestionredo', array('testid' => $id, 'question' => $question, 'questionid' => $questionid, 'mode' => $mode));
        else:
            //$this->redirect(array('revision'));
        endif;

    }

    public function GetRedoTestQuestions($id, $question, $mode)
    {
        if ($mode == 'flagged'):
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Redo_Flag=1 AND TM_STU_QN_Type NOT IN (6,7)';
            $criteria->params = array(':test' => $id);
            $criteria->order = 'TM_STU_QN_Number ASC';
            $selectedquestion = StudentQuestions::model()->findAll($criteria);
            $pagination = '';
            foreach ($selectedquestion AS $key => $testquestion):
                $count = $key + 1;
                $class = '';
                if ($testquestion->TM_STU_QN_Question_Id == $question) {
                    $questioncount = $count;
                }
                if ($testquestion->TM_STU_QN_Type == '6' || $testquestion->TM_STU_QN_Type == '7') {
                    $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
                } else {
                    $title = '';
                    $class = '';
                    if ($testquestion->TM_STU_QN_RedoStatus == '0') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : '');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : '');
                    } elseif ($testquestion->TM_STU_QN_RedoStatus == '1') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="answered"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Already Answered');
                    } elseif ($testquestion->TM_STU_QN_RedoStatus == '2') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                    }
                    $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_STU_QN_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('student/RevisionRedo', array('id' => $id, 'redo' => $mode, 'questionnum' => base64_encode('question_' . $testquestion->TM_STU_QN_Number)))) . '">' . $count . '</a></li>';
                }


            endforeach;


            return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count);


        else:
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0  AND TM_STU_QN_Type NOT IN (6,7)';
            $criteria->params = array(':test' => $id);
            $criteria->order = 'TM_STU_QN_Number ASC';
            $selectedquestion = StudentQuestions::model()->findAll($criteria);
            $pagination = '';
            foreach ($selectedquestion AS $key => $testquestion):
                $count = $key + 1;
                $class = '';
                if ($testquestion->TM_STU_QN_Question_Id == $question) {
                    $questioncount = $count;
                }
                if ($testquestion->TM_STU_QN_Type == '6' || $testquestion->TM_STU_QN_Type == '7') {
                    $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
                } else {
                    $title = '';
                    $class = '';
                    if ($testquestion->TM_STU_QN_RedoStatus == '0') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : '');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : '');
                    } elseif ($testquestion->TM_STU_QN_RedoStatus == '1') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="answered"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Already Answered');
                    } elseif ($testquestion->TM_STU_QN_RedoStatus == '2') {
                        $class = ($testquestion->TM_STU_QN_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                        $title = ($testquestion->TM_STU_QN_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                    }
                    $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_STU_QN_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('student/RevisionRedo', array('id' => $id, 'redo' => $mode, 'questionnum' => base64_encode('question_' . $testquestion->TM_STU_QN_Number)))) . '">' . $count . '</a></li>';
                }
            endforeach;
            return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count);

        endif;
    }

    public function GetRedoCount($test)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=:test AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Student_Id=:student AND TM_STU_QN_Type NOT IN (6,7) AND TM_STU_QN_Redo_Flag=1';
        $criteria->params = array(':test' => $test, ':student' => Yii::app()->user->id);
        $redoquestions = StudentQuestions::model()->count($criteria);
        return $redoquestions;
    }

    public function GetMockRedoCount($test)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Student_Id=:student AND TM_STMKQT_Type NOT IN (6,7) AND TM_STMKQT_Redo_Flag=1';
        $criteria->params = array(':test' => $test, ':student' => Yii::app()->user->id);
        $redoquestions = Studentmockquestions::model()->count($criteria);
        return $redoquestions;
    }

    public function actionGetchallengepdfAnswer($id)
    {
        $results = Chalangeuserquestions::model()->findAll(
            array(
                'condition' => 'TM_CEUQ_User_Id=:student AND TM_CEUQ_Challenge=:test',
                "order" => 'TM_CEUQ_Id ASC',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id)
            )
        );
        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b>CHALLENGE SOLUTIONS</b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";
        $number = 1;
        foreach ($results AS $key => $result):
            if ($result->TM_CEUQ_Difficulty == 1):
                $mark = 5;
            elseif ($result->TM_CEUQ_Difficulty == 2):
                $mark = 8;
            elseif ($result->TM_CEUQ_Difficulty == 3):
                $mark = 12;
            endif;
            $count = $key + 1;
            $question = Questions::model()->findByPk($result->TM_CEUQ_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):
                if ($question->TM_QN_Type_Id == '1' || $question->TM_QN_Type_Id == '2' || $question->TM_QN_Type_Id == '3' || $question->TM_QN_Type_Id == '4'):
                    $html .= "<tr><td width='15%'><b>Question" . $number++ . "</b></td><td width='15%'> Marks : " . $mark . "</td><td width='70%' align='right'>Reference :" . $question->TM_QN_QuestionReff . "</td></tr>";                    
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= "<tr><td colspan='3' align='right'><img src='" . Yii::app()->request->baseUrl . "/site/Imagerender/id/" . $question->TM_QN_Image . "?size=thumbs&type=question&width=300&height=300'></td></tr>";
                    endif;                    
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif; 
                    $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                endif;
                if ($question->TM_QN_Type_Id == '5'):
                	$displaymark = 0;
					$multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
					$countmulti = 1;
					$displaymark = 0;
					$childresulthtml = '';
					foreach ($multiQuestions AS $key => $chilquestions):

						$marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
						foreach ($marks AS $key => $marksdisplay):
							$displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
						endforeach;						
						$countmulti++;
					endforeach;
                    $html .= "<tr><td width='15%' ><b>Question" . $number++ . "</b></td><td> Marks : " . $marks . "</td><td width='70%' align='right'>Reference :" . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $count = 1;
                    foreach ($questions AS $childquestion):

                        $html .= "<tr><td colspan='3' >Part " . $count."</td></tr>";
                        $html .= "<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $html .= "<tr><td colspan='3' align='right'><img src='" . Yii::app()->request->baseUrl . "/site/Imagerender/id/" . $childquestion->TM_QN_Image . "?size=thumbs&type=question&width=300&height=300'></td></tr>";
                        endif;
                        $count++;
                    endforeach;                    
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif; 
                    $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                
                endif;


            endif;
        endforeach;
        $html .= "</table>";
        /*require_once 'pluginbuilder.php';
        $text = $pluginBuilder->newTextService();        
        $params = ["dpi"=>"500"];
        $html = $text->filter($html, $params);        //echo $html;exit;*/


        $mpdf = new mPDF();
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf;">' . $firstname . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; font-style: italic;"></td>
<td width="33%" style="text-align: right; font-weight: lighter; color: #afafaf;">' . $username . '</td>
</tr></table>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
        <table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"><tr>
        <!--<td width="33%"><span style="font-weight: bold; font-style: italic;">{DATE j-m-Y}</span></td>-->
        <td width="33%" align="center" style="font-weight: bold; color: #afafaf;">TabbieMe Educational Solutions PVT Ltd.</td>
        <!--<td width="33%" style="text-align: right; ">TabbieMe Revision</td>-->
        </tr></table>');
        $name='CHALLENGE_SOLUTIONS_'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');


    }

    public function GetChallengeCount($challenge, $difficulty, $limit)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=:difficulty';
        $criteria->limit = $limit;
        $criteria->order = 'RAND()';
        $criteria->params = array(':test' => $challenge, ':difficulty' => $difficulty);
        $questions = Chalangequestions::model()->findAll($criteria);
        return count($questions);
    }

    public function ChallengeAddQuestions($challenge, $difficulty, $limit, $user, $iteration)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_CL_QS_Chalange_Id=:test AND TM_CL_QS_Difficulty=:difficulty';
        $criteria->limit = $limit;
        $criteria->order = 'RAND()';
        $criteria->params = array(':test' => $challenge, ':difficulty' => $difficulty);
        $questions = Chalangequestions::model()->findAll($criteria);
        $count = $iteration;
        foreach ($questions AS $key => $question):
            $masterquestion = Questions::model()->findByPk($question->TM_CL_QS_Question_Id);
            $count++;
            $userquestion = new Chalangeuserquestions();
            $userquestion->TM_CEUQ_Challenge = $challenge;
            $userquestion->TM_CEUQ_Question_Id = $question->TM_CL_QS_Question_Id;
            $userquestion->TM_CEUQ_Difficulty = $question->TM_CL_QS_Difficulty;
            $userquestion->TM_CEUQ_User_Id = $user;
            $userquestion->TM_CEUQ_Iteration = $count;
            $userquestion->save(false);
            if ($masterquestion->TM_QN_Type_Id == '5'):
                $childQuestions = Questions::model()->findAll(array('condition' => 'TM_QN_Parent_Id=:parent', 'params' => array(':parent' => $question->TM_CL_QS_Question_Id)));
                foreach ($childQuestions AS $child):
                    $userquestion = new Chalangeuserquestions();
                    $userquestion->TM_CEUQ_Challenge = $challenge;
                    $userquestion->TM_CEUQ_Question_Id = $child->TM_QN_Id;
                    $userquestion->TM_CEUQ_Difficulty = $child->TM_QN_Dificulty_Id;
                    $userquestion->TM_CEUQ_User_Id = $user;
                    $userquestion->TM_CEUQ_Parent_Id = $question->TM_CL_QS_Question_Id;
                    $userquestion->save(false);
                endforeach;
            endif;
        endforeach;
        return $count;
    }

    public function GetChapterTotal($chapter, $type)
    {
        $criteria = new CDbCriteria;
        //$criteria->condition = 'TM_QN_Topic_Id=:chapter AND TM_QN_Status=0 AND TM_QN_Parent_Id=0 AND TM_QE_Exam_Id=' . $type;
        $criteria->condition = 'TM_QN_Topic_Id=:chapter AND TM_QN_Parent_Id=0 AND TM_QE_Exam_Id=' . $type;
        $criteria->params = array(':chapter' => $chapter);
        $questions = Questions::model()->with('exams')->count($criteria);
        return $questions;
    }

    public function actionChangePlan($id)
    {
        $plan = StudentPlan::model()->find(array('condition' => 'TM_SPN_StudentId=' . Yii::app()->user->id . ' AND TM_SPN_PlanId=' . $id, 'limit' => 1, 'order' => 'TM_SPN_CreatedOn DESC'));
        $masterplan=Plan::model()->findByPk($plan->TM_SPN_PlanId);
        $modules = Planmodules::model()->findAll(array('condition' => 'TM_PM_Plan_Id=' . $plan->TM_SPN_PlanId));
        Yii::app()->session['plan'] = $plan->TM_SPN_PlanId;
        Yii::app()->session['publisher'] = $plan->TM_SPN_Publisher_Id;
        Yii::app()->session['syllabus'] = $plan->TM_SPN_SyllabusId;
        Yii::app()->session['standard'] = $plan->TM_SPN_StandardId;
        Yii::app()->session['subject'] = $plan->TM_SPN_SubjectId;
        Yii::app()->session['quick'] = false;
        Yii::app()->session['difficulty'] = false;
        Yii::app()->session['challenge'] = false;
        Yii::app()->session['mock'] = false;
        Yii::app()->session['resource'] = false;
        if($masterplan->TM_PN_Type=='1'):
            Yii::app()->session['school'] = true;
        else:
            Yii::app()->session['school'] = false;        
            foreach ($modules AS $module):
                switch ($module->TM_PM_Module) {
                    case "1":
                        Yii::app()->session['quick'] = true;
                        break;
                    case "2":
                        Yii::app()->session['difficulty'] = true;
                        break;
                    case "3":
                        Yii::app()->session['challenge'] = true;
                        break;
                    case "4":
                        Yii::app()->session['mock'] = true;
                        break;
                    case "5":
                        Yii::app()->session['resource'] = true;
                        break;
                }
            endforeach;
        endif;
        $this->redirect(array('home'));
    }

    public function actionLeaderboard()
    {

        if (isset($_GET['startdate'])):
            if ($_GET['mode'] == 'month'):
                $dates = $this->GetMonth($_GET['startdate']);
            else:
                $dates = $this->GetWeek($_GET['startdate']);
            endif;
        else:
            $dates = $this->GetWeek(date('Y-m-d'));
        endif;
        $mode = (isset($_GET['mode']) != '' ? $_GET['mode'] : 'total');
        if($mode=='total'):
        
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints,TM_STP_Student_Id,TM_STP_Date');
            $criteria->condition = "TM_STP_Standard_Id=:standard ";
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->limit = '5';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $studenttotal = StudentPoints::model()->findAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Standard_Id=:standard AND TM_STP_Student_Id=" . Yii::app()->user->id;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $usertotal = StudentPoints::model()->find($criteria);        
        else:        
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints,TM_STP_Student_Id,TM_STP_Date');
            $criteria->condition = "TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '" . $dates[0] . "' AND '" . $dates[1] . "'";
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->limit = '5';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $studenttotal = StudentPoints::model()->findAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '" . $dates[0] . "' AND '" . $dates[1] . "' AND TM_STP_Student_Id=" . Yii::app()->user->id;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $usertotal = StudentPoints::model()->find($criteria);        
        endif;
        $this->render('leaderboard', array('studenttotal' => $studenttotal, 'usertotal' => $usertotal, 'dates' => $dates, 'mode' => $mode));
    }
    public function actionProgressReport()
    {
        $week=$this->GetWeek(date('Y-m-d'));
        $plan = $this->GetStudentDet();                        
        if (Yii::app()->session['challenge']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_CE_RE_Recepient_Id=' . Yii::app()->user->id . ' AND TM_CE_RE_Status=3 AND TM_CL_Plan=' . Yii::app()->session['plan']);            
            $challengetotal=Chalangerecepient::model()->with('challenge')->count($criteria);
            $criteria->addCondition("TM_CL_Created_On BETWEEN '".$week[0]."' AND '".$week[1]."'");                        
            $challengeweekstatus = Chalangerecepient::model()->with('challenge')->count($criteria);
        endif;
        if (Yii::app()->session['quick'] || Yii::app()->session['difficulty']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STU_TT_Student_Id=' . Yii::app()->user->id . ' AND TM_STU_TT_Status IN(3,4) AND TM_STU_TT_Plan=' . Yii::app()->session['plan']);                        
            $revisiontotal = StudentTest::model()->count($criteria);     
            $criteria->addCondition("TM_STU_TT_CreateOn BETWEEN '".$week[0]."' AND '".$week[1]."'");
            $revisionweekstatus = StudentTest::model()->count($criteria);      
        endif;
        if (Yii::app()->session['mock']):
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STMK_Student_Id=' . Yii::app()->user->id . ' AND TM_STMK_Status IN (3,4) AND TM_MK_Status=0 AND  TM_STMK_Plan_Id=' . Yii::app()->session['plan']);            
            $mocktotal = StudentMocks::model()->with('mocks')->count($criteria);
            $criteria->addCondition("TM_STMK_Date BETWEEN '".$week[0]."' AND '".$week[1]."'");
            $mockweekstatus = StudentMocks::model()->with('mocks')->count($criteria);            
        endif;        
            $dates = $this->GetWeek(date('Y-m-d'));
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Standard_Id=:standard AND TM_STP_Student_Id=" . Yii::app()->user->id;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $totalpoints = StudentPoints::model()->find($criteria); 
			
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '" . $dates[0] . "' AND '" . $dates[1] . "' AND TM_STP_Student_Id=" . Yii::app()->user->id;
            $criteria->order = 'totalpoints DESC';            
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $weeklypoints = StudentPoints::model()->find($criteria); 
             
        $lastvisit=User::model()->findByPk(Yii::app()->user->id)->lastvisit;
        if(!$lastvisit):
            $lastlogin='Never logged in';
        else:
            $lastlogin=date("jS F Y ",$lastvisit);
        endif; 
                                     
        $this->render('progressreport',array(
        'challengetotal'=>$challengetotal,
        'challengeweekstatus'=>$challengeweekstatus,
        'revisiontotal'=>$revisiontotal,
        'revisionweekstatus'=>$revisionweekstatus,
        'mocktotal'=>$mocktotal,
        'mockweekstatus'=>$mockweekstatus,
        'lastlogin'=>$lastlogin,
        'weeklypoints'=>$weeklypoints, 
        'totalpoints'=>$totalpoints                             
        ));  
    }    
    public function ClosestNumber($array, $number,$chapterarray,$chapter)
    {
    	
        $returnarray=array();
        $highest= max($array);
    	if($highest>$number):
    			$key = array_search($highest, $array);
    			$array[$key] = $number;
                $chapterarray[$key]=$chapter;
                $returnarray[]=$array;
                $returnarray[]=$chapterarray;                
    			return $returnarray;				
    	else:
    		return false;	
    	endif;
    }
    public function GetTopicStatus($user)
    {
        $plan = $this->GetStudentDet(); 
        $connection = CActiveRecord::getDbConnection();
        $sql1=" SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".Yii::app()->user->id."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".Yii::app()->user->id."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".Yii::app()->user->id."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 ";        
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        $correctones=$dataReader->readAll();        
        $correctquestions='';
        foreach($correctones AS $key=>$correct):
        $correctquestions.=($key==0?'':',').$correct['question'];
        endforeach; 
        $sql="SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".Yii::app()->user->id."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".Yii::app()->user->id."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".Yii::app()->user->id."' AND TM_CEUQ_Parent_Id=0";        
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attempted=$dataReader->readAll();
        $attemptedquestions='';
        foreach($attempted AS $key=>$attemp):
        $attemptedquestions.=($key==0?'':',').$attemp['question'];
        endforeach;        
         $sql="SELECT count(DISTINCT(a.TM_QN_Id)) AS attempted, b.TM_SN_Id,b.TM_SN_Name,b.TM_SN_Topic_Id,
        (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Section_Id=a.TM_QN_Section_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$attemptedquestions." )) AS completed,
                (
                    SELECT COUNT(DISTINCT(TM_QN_Id)) AS question 
            		FROM tm_question AS quest 
            		WHERE quest.TM_QN_Section_Id=b.TM_SN_Id 
            		AND quest.TM_QN_Parent_Id=0 
                ) AS total,((count(DISTINCT(a.TM_QN_Id))/(SELECT COUNT(DISTINCT(TM_QN_Id)) FROM tm_question AS subq WHERE subq.TM_QN_Section_Id=a.TM_QN_Section_Id AND subq.TM_QN_Parent_Id=0 AND subq.TM_QN_Id IN ( ".$attemptedquestions." )))*100) AS average        
                FROM 
                tm_question AS a,tm_section AS b WHERE 
                a.TM_QN_Parent_Id=0 AND a.TM_QN_Section_Id=b.TM_SN_Id AND b.TM_SN_Syllabus_Id='".$plan->TM_SPN_SyllabusId."' AND b.TM_SN_Standard_Id='".$plan->TM_SPN_StandardId."' AND TM_QN_Id IN (".$correctquestions.") GROUP BY TM_QN_Section_Id HAVING completed > 0 AND attempted < completed ORDER BY average ASC LIMIT 5 ";
                //,TM_SN_Id ASC
                                                              
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $topicstatus=$dataReader->readAll();
        //print_r($topicstatus); exit;                        
        return $topicstatus;                     
    }
    public function GetChapterCount($chapter,$user,$attemptedcount,$correctcount)
    {
        $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Topic_Id='.$chapter.' AND TM_QN_Parent_Id=0 AND TM_QN_Status=0'));
        if($attemptedcount>0):
            $average=($attemptedcount/$totalquestions)*100;
            if($correctcount>0):
                $correct=round(($correctcount/$attemptedcount)*100);
            else:
                $correct=0;
            endif;
            $balance=round($average)-$correct;
            if($average>100):
                $average=100;
			endif;
        else:
            $average=0;
            $correct=0;
        endif;

        return array('total'=>$totalquestions,'attempted'=>round($average),'correct'=>$correct,'average'=>round($average));
    }
    public function GetCompleteStatus($user)
    {
        $plan = $this->GetStudentDet();                  
        $syllabus=$plan->TM_SPN_SyllabusId;
        $standard=$plan->TM_SPN_StandardId;        
        $restresult=Settotal::model()->find(array('condition'=>'TM_TOT_Plan_Id='.$plan->TM_SPN_PlanId));
        if($restresult->TM_TOT_Total_Attempted>0):
            $restaverage=($restresult->TM_TOT_Total_Attempted/$restresult->TM_TOT_Total_Questions)*100;
            if($restresult->TM_TOT_Total_Correct>0):
                $restcorrect=round(($restresult->TM_TOT_Total_Correct/$restresult->TM_TOT_Total_Attempted)*100);
            else:
                $restcorrect=0;
            endif;
            //$balance=round($restaverage)-$correct;
        else:
            $restaverage=0;
            $restcorrect=0;
        endif;
        return array('restaverage'=>round($restaverage),'restcorrect'=>$restcorrect);
    } 
    /*public function GetMyCompleteStatus($user)
    {
        $plan = $this->GetStudentDet();                  
        $syllabus=$plan->TM_SPN_SyllabusId;
        $standard=$plan->TM_SPN_StandardId;        
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 )";        

        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attemptedcount=$dataReader->read();
        $attemptedcount=$attemptedcount['question'];        
        $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 )";               

        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $correctcount=$dataReader->read();
        $correctcount=$correctcount['question'];  
        $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Syllabus_Id='.$syllabus.' AND TM_QN_Standard_Id='.$standard.' AND TM_QN_Parent_Id=0 AND TM_QN_Status=0'));              
        if($attemptedcount>0):
            $average=($attemptedcount/$totalquestions)*100;
            if($correctcount>0):
                $correct=round(($correctcount/$attemptedcount)*100);
            else:
                $correct=0;
            endif;
            $balance=round($average)-$correct;
        else:
            $average=0;
            $correct=0;
        endif;

        return array('average'=>round($average),'correct'=>$correct);
    }   */
	public function GetMyCompleteStatus($user)
    {
        $plan = $this->GetStudentDet();
        $plantype=Plan::model()->findByPk($plan->TM_SPN_PlanId)->TM_PN_Type;                  
        $syllabus=$plan->TM_SPN_SyllabusId;
        $standard=$plan->TM_SPN_StandardId;
		//echo $plan->TM_SPN_PlanId.' - '.$plantype.' - '.$syllabus.' - '.$standard.' - '.$user; exit;
        if($plantype=='0'):        
            $connection = CActiveRecord::getDbConnection();
            //$sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 )";       			
			$sql="SELECT question FROM (SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions AS A LEFT JOIN tm_question AS B ON B.TM_QN_Id=A.TM_STU_QN_Question_Id WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND B.TM_QN_Syllabus_Id='".$syllabus."' AND B.TM_QN_Standard_Id='".$standard."' AND B.TM_QN_Parent_Id=0
					UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions AS C LEFT JOIN tm_question AS D ON D.TM_QN_Id=C.TM_STMKQT_Question_Id WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND D.TM_QN_Syllabus_Id='".$syllabus."' AND D.TM_QN_Standard_Id='".$standard."' AND D.TM_QN_Parent_Id=0
					UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions AS E LEFT JOIN tm_question AS f ON f.TM_QN_Id=E.TM_CEUQ_Question_Id WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 AND f.TM_QN_Syllabus_Id='".$syllabus."' AND f.TM_QN_Standard_Id='".$standard."' AND f.TM_QN_Parent_Id=0) t1 GROUP BY question";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $attemptedcount=$dataReader->readAll();
            $attemptedcount=count($attemptedcount);//$attemptedcount['question'];        
            //$sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 )";                   
			$sql="SELECT question FROM (SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions AS A LEFT JOIN tm_question AS B ON B.TM_QN_Id=A.TM_STU_QN_Question_Id WHERE TM_STU_QN_Student_Id='".$user."' AND TM_STU_QN_Mark!=0 AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND B.TM_QN_Syllabus_Id='".$syllabus."' AND B.TM_QN_Standard_Id='".$standard."' AND B.TM_QN_Parent_Id=0
					UNION ALL SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions AS C LEFT JOIN tm_question AS D ON D.TM_QN_Id=C.TM_STMKQT_Question_Id WHERE TM_STMKQT_Student_Id='".$user."' AND TM_STMKQT_Marks!=0 AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND D.TM_QN_Syllabus_Id='".$syllabus."' AND D.TM_QN_Standard_Id='".$standard."' AND D.TM_QN_Parent_Id=0
					UNION ALL SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions AS E LEFT JOIN tm_question AS f ON f.TM_QN_Id=E.TM_CEUQ_Question_Id WHERE TM_CEUQ_User_Id='".$user."' AND TM_CEUQ_Marks!=0  AND TM_CEUQ_Parent_Id=0 AND f.TM_QN_Syllabus_Id='".$syllabus."' AND f.TM_QN_Standard_Id='".$standard."' AND f.TM_QN_Parent_Id=0) t1 GROUP BY question";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $correctcount=$dataReader->readAll();
            $correctcount=count($correctcount);//$correctcount['question'];  
			//echo  $correctcount.' '.$attemptedcount; exit;
            $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Syllabus_Id='.$syllabus.' AND TM_QN_Standard_Id='.$standard.' AND TM_QN_Parent_Id=0 AND TM_QN_Status=0'));              
			//echo  $correctcount.' '.$attemptedcount.' '.$totalquestions; exit;
            if($attemptedcount>0):
                $average=($attemptedcount/$totalquestions)*100;
                if($correctcount>0):
                    $correct=round(($correctcount/$attemptedcount)*100);
                else:
                    $correct=0;
                endif;
                $balance=round($average)-$correct;
            else:
                $average=0;
                $correct=0;
            endif;
        elseif($plantype=='1'):
            $connection = CActiveRecord::getDbConnection();
            $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( 
				SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".$user."' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Status!=0					
				)"; 
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $attemptedcount=$dataReader->read();
            $attemptedcount=$attemptedcount['question'];        
            $sql="SELECT COUNT(TM_QN_Id) AS question FROM tm_question AS A WHERE A.TM_QN_Syllabus_Id='".$syllabus."' AND A.TM_QN_Standard_Id='".$standard."' AND A.TM_QN_Parent_Id=0 AND TM_QN_Id IN ( SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".$user."' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Status!=0 AND TM_STHWQT_Marks!=0 )";               
    
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $correctcount=$dataReader->read();
            $correctcount=$correctcount['question'];  
            $totalquestions=Questions::model()->count(array('condition'=>'TM_QN_Syllabus_Id='.$syllabus.' AND TM_QN_Standard_Id='.$standard.' AND TM_QN_Parent_Id=0 AND TM_QN_Status=0'));              
            if($attemptedcount>0):
                $average=($attemptedcount/$totalquestions)*100;
                if($correctcount>0):
                    $correct=round(($correctcount/$attemptedcount)*100);
                else:
                    $correct=0;
                endif;
                $balance=round($average)-$correct;
            else:
                $average=0;
                $correct=0;
            endif;        
        endif;
        return array('average'=>round($average),'correct'=>$correct);
    }	
    public function actionGetChapterstatus()
    {
        $plan = $this->GetStudentDet();
        //$chapters = Chapter::model()->findAll(array('condition' => "TM_TP_Syllabus_Ida='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'",'order'=>'TM_TP_order ASC'));

        $connection = CActiveRecord::getDbConnection();                       
        $sql="SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".Yii::app()->user->id."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".Yii::app()->user->id."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".Yii::app()->user->id."' AND TM_CEUQ_Parent_Id=0";        
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $attempted=$dataReader->readAll();
        $attemptedquestions='';
        foreach($attempted AS $key=>$attemp):
        $attemptedquestions.=($key==0?'':',').$attemp['question'];
        endforeach;         
        $sql1=" SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".Yii::app()->user->id."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".Yii::app()->user->id."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".Yii::app()->user->id."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 ";        
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        $correctones=$dataReader->readAll();        
        $correctquestions='';
        foreach($correctones AS $key=>$correct):
        $correctquestions.=($key==0?'':',').$correct['question'];
        endforeach; 
        $sql="SELECT count(a.TM_QN_Id) AS attemptedcount, a.TM_QN_Topic_Id,b.TM_TP_Name,
        (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Topic_Id=a.TM_QN_Topic_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$correctquestions." )) AS correctcount
                FROM 
                tm_question AS a,tm_topic AS b WHERE 
                a.TM_QN_Parent_Id=0 AND a.TM_QN_Topic_Id=b.TM_TP_Id AND b.TM_TP_Syllabus_Id='".$plan->TM_SPN_SyllabusId."' AND b.TM_TP_Standard_Id='".$plan->TM_SPN_StandardId."' AND TM_QN_Id IN (".$attemptedquestions.") group by TM_QN_Topic_Id";                                      
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $chapters=$dataReader->readAll();                      
        $this->renderPartial('_overallstatus', array('chapters' => $chapters, 'plan' => $plan,'correctquestions'=>$correctquestions,'attemptedquestions'=>$attemptedquestions)); 
    }
    public function actionGetTopicStatus()
    {
        $topicstatus=$this->GetTopicStatus(Yii::app()->user->id);
        $this->renderPartial('_topicstatus', array('topicstatus' => $topicstatus));         
    }
    public function actionGetRestStatus()
    {
        $progressstatus=$this->GetCompleteStatus(Yii::app()->user->id);
        echo json_encode($progressstatus);        
    } 
    public function actionGetMyStatus()
    {
        $progressstatus=$this->GetMyCompleteStatus(Yii::app()->user->id);
        echo json_encode($progressstatus);        
    }         
    public function AddPoints($earnedmarks, $testid, $testtype, $user)
    {
        $studentpintcount = StudentPoints::model()->count(
            array('condition' => 'TM_STP_Student_Id=:student AND TM_STP_Test_Id=:test AND TM_STP_Test_Type=:type',
                'params' => array(':student' => $user, ':test' => $testid, 'type' => $testtype)
            ));
        if ($studentpintcount == 0) {
            $studentpoint = new StudentPoints();
            $studentpoint->TM_STP_Student_Id = $user;
            $studentpoint->TM_STP_Points = $earnedmarks;
            $studentpoint->TM_STP_Test_Id = $testid;
            $studentpoint->TM_STP_Test_Type = $testtype;
            $studentpoint->TM_STP_Standard_Id = Yii::app()->session['standard'];
            $studentpoint->save(false);
        }
        $criteria = new CDbCriteria;
        $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
        $criteria->condition = 'TM_STP_Student_Id=:student AND TM_STP_Standard_Id=:standard';
        $criteria->params = array(':student' => $user, ':standard' => Yii::app()->session['standard']);
        $studenttotal = StudentPoints::model()->find($criteria);
        $returnarray = array('total' => $studenttotal->totalpoints);
        $levels = Levels::model()->findAll(array('condition' => 'TM_LV_Status=0'));
        $levelcount = Studentlevels::model()->count(array('condition' => 'TM_STL_Student_Id=' . $user));
        foreach ($levels AS $level):
            if ($studenttotal->totalpoints >= $level->TM_LV_Minpoint & $studenttotal->totalpoints < $level->TM_LV_MaxPoint):
                $studentlevelcount = Studentlevels::model()->count(array('condition' => 'TM_STL_Student_Id=' . $user . ' AND TM_STL_Level_Id=' . $level->TM_LV_Id));
                if ($studentlevelcount == 0):
                    $addlevel = new Studentlevels();
                    $addlevel->TM_STL_Level_Id = $level->TM_LV_Id;
                    $addlevel->TM_STL_Student_Id = $user;
                    $addlevel->TM_STL_Order = $levelcount + 1;
                    $addlevel->TM_STL_Date = date('Y-m-d');
                    $addlevel->save(false);
                    $returnarray['newlevel'] = true;
                    $notification = new Notifications();
                    $notification->TM_NT_User = $user;
                    $notification->TM_NT_Type = 'Level';
                    $notification->TM_NT_Target_Id = $level->TM_LV_Id;
                    $notification->TM_NT_Status = '0';
                    $notification->save(false);
                else:
                    $returnarray['newlevel'] = false;
                endif;
                $returnarray['studentlevel'] = $level->TM_LV_Id;
            endif;
        endforeach;
        return $returnarray;
    }
    public function GetWeek($week)
    {
        $ts = strtotime($week);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday midnight', $ts);
        $previous_week = strtotime("-1 week +1 day", $ts);
        $previous_week = strtotime("last sunday midnight", $previous_week);
        $next_week = strtotime("+1 week +1 day", $ts);
        $next_week = strtotime("last sunday midnight", $next_week);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next saturday', $start)), date('F Y', $start), date('Y-m-d', $previous_week), date('Y-m-d', $next_week));
    }

    public function GetMonth($week)
    {
        $ts = strtotime($week);
        $start = (date('m', $ts) == 0) ? $ts : strtotime('first day of this month', $ts);
        $previous_month = strtotime("first day of last month", $ts);
        $next_month = strtotime("first day of next month", $ts);
        return array(date('Y-m-d', $start), date('Y-m-d', strtotime('last day of this month', $start)), date('F Y', $start), date('Y-m-d', $previous_month), date('Y-m-d', $next_month));
    }

    public function actionGetpdfmock($id)
    {
        $paperquestions = Studentmockquestions::model()->findAll(
            array(
                'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Type IN(6,7)',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id),
                'order'=>'TM_STMKQT_Status ASC'
            )
        );
        $totalquestions = count($paperquestions);
        $nak = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;

        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>MOCK PAPER TEST<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";


        foreach ($paperquestions AS $key => $paperquestion):
            $paperquestioncount = $key + 1;
            $question = Questions::model()->findByPk($paperquestion->TM_STMKQT_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):

                if ($question->TM_QN_Type_Id != '7'):
                    $displaymark = 0;
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marks AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $paperquestioncount ."</b></td><td width='15%'>Marks: ".$question->TM_QN_Totalmarks."</td><td align='right' width='70%'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $paperquestioncount ."</b></td><td width='15%'>Marks: ".$displaymark."</td><td align='right' width='70%'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";                
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;
            endif;
        endforeach;
        $html .= "</table>";
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;  color: #afafaf; ">' . $firstname . ' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf; ">' . $username . '</td>
</tr></table>';
        /*require_once 'pluginbuilder.php';
        $text = $pluginBuilder->newTextService();        
        $params = ["dpi"=>"500"];
        $html = $text->filter($html, $params);*/

        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Educational Solutions PVT Ltd.</span></td>
</tr></table>
');

        $name='MOCK_PAPER_TEST_'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

    }

    public function actionGetpdfMockAnswer($id)
    {
        $results = Studentmockquestions::model()->findAll(
            array(
                'condition' => 'TM_STMKQT_Student_Id=:student AND TM_STMKQT_Mock_Id=:test AND TM_STMKQT_Parent_Id=0',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id),
                'order'=>'TM_STMKQT_Order ASC'
            )
        );
        $totalquestions = count($results);
        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>MOCK SOLUTIONS<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>Date:" . date("d/m/Y") . "</td></tr>";
        foreach ($results AS $key => $result):
            $count = $key + 1;
            $question = Questions::model()->findByPk($result->TM_STMKQT_Question_Id);
            if ($question->TM_QN_Parent_Id == '0'):
                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Type_Id == '1' || $question->TM_QN_Type_Id == '2' || $question->TM_QN_Type_Id == '3' || $question->TM_QN_Type_Id == '4'):
                    $html .= "<tr><td width='15%'><b>Question " . $result->TM_STMKQT_Order. "</b></td><td width='15%'> Marks : " . $displaymark . "</td><td width='70%' align='right'>Reference :" . $question->TM_QN_QuestionReff . "</td></tr>";                    
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;                   
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '6'):
                    $html .= "<tr><td width='15%'><b>Question " . $result->TM_STMKQT_Order. "</b></td><td width='15%'> Marks : " . $question->TM_QN_Totalmarks . "</td><td width='70%' align='right'>Reference :" . $question->TM_QN_QuestionReff . "</td></tr>";                   
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif;  
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '5'):

                    $displaymark = 0;


                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $partcount = 1;
                    $childhtml = '';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marks AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                        $childhtml .= "<tr><td colspan='3' >Part " . $partcount."</td></tr>";
                        $childhtml .= "<tr><td colspan='3'>".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;                       
                        $partcount++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $result->TM_STMKQT_Order. "</b></td><td width='15%'> Marks : " . $displaymark . "</td><td width='70%' align='right'>Reference :" . $question->TM_QN_QuestionReff . "</td></tr>";                    
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif; 
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):

                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $partcount = 1;
                    $childhtml = '';
                    foreach ($questions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));


                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .= "<tr><td colspan='3' >Part " . $partcount."</td></tr>";
                        $childhtml .= "<tr><td colspan='3'>".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif; 
                        $partcount++;
                    endforeach;                    
                    $html .= "<tr><td width='15%'><b>Question " . $result->TM_STMKQT_Order. "</b></td><td width='15%'> Marks : " . $displaymark . "</td><td width='70%' align='right'>Reference :" . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    if ($question->TM_QN_Solutions != ''):
                        $html .= "<tr><td colspan='3'>Solution:</td></tr><tr><td colspan='3'>".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=300&height=300" ></td></tr>';
                        endif;
                    else:
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3">Solution:</td></tr><tr><td colspan="3" ><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=large&type=solution&width=200&height=200" ></td></tr>';
                        endif;
                    endif; 
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;


            endif;
        endforeach;
        $html .= "</table>";

        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                <td width="33%"><span style="font-weight: lighter; color: #afafaf;">TabbieMath</span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                <td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $firstname . ' ' . $lastname . '</td></tr>
                <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                <td width="33%" style="text-align: right;font-weight: lighter;color: #afafaf; ">' . $username . '</td>
                </tr></table>';
        /*require_once 'pluginbuilder.php';
        $text = $pluginBuilder->newTextService();        
        $params = ["dpi"=>"500"];
        $html = $text->filter($html, $params);*/
        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);

        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;
        $mpdf->SetHTMLFooter('
<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style="font-weight: lighter; color: #afafaf;" align="center">TabbieMe Educational Solutions PVT Ltd.</span></td>

</tr></table>
');
        $name='MOCK_SOLUTIONS_'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');


    }

    public function actionSendMailReminders()

    {
        $recepientid = $_POST["recepientid"];        
        $userId = Yii::app()->user->getId();
        $notification = new Notifications();
        $notification->TM_NT_User = $recepientid;
        $notification->TM_NT_Type = 'Reminder';
        $notification->TM_NT_Target_Id = Yii::app()->user->Id;
        $notification->TM_NT_Status = '0';
        $notification->TM_NT_Item_Id=$_POST["challengeid"];
        if($notification->save(false)):
        echo "yes";
        endif;
        
    }

    public function SendMailChallengeInvite($id)

    {


        $userId = Yii::app()->user->getId();
        $userDet = User::model()->findByPk($userId);
        $recepiendet = User::model()->findByPk($id);
        $studentDet = Student::model()->find(array('condition' => "TM_STU_User_Id=" . $id));
        $parentDet = User::model()->findByPk($studentDet->TM_STU_Parent_Id);
        $content = '<p>Dear ' . $parentDet->profile->firstname . '</p>

<p>' . $studentDet->TM_STU_First_Name . ' is invited to a challenge by ' . Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name . '</p>
<p>Login to TabbieMath and accept the challenge</p>
<p>' . $this->createAbsoluteUrl('/user/login') . '</p>

<p>Thank you</p>
<p>Support. </p>';
        Yii::import('application.extensions.phpmailer.JPhpMailer');

        $mail = new JPhpMailer;

        $mail->IsSMTP();
        $mail->Host = "smtpout.secureserver.net"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "ssl";
        $mail->Port       = 465;
        $mail->Username = "services@tabbiemath.com"; // SMTP username
        $mail->Password = "Ta88ieMe"; // SMTP password
        $mail->SetFrom('services@tabbiemath.com', 'TabbieMe');
        $mail->Subject = 'Accept the Challenge';
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($content);
        $mail->AddAddress($parentDet->email, '');
        //$mail->AddAddress('karthik@solminds.com', 'Karthik');


        if (!$mail->Send()) {

            echo "Mailer Error: " . $mail->ErrorInfo;

        } else {


            return success;

        }
    }

    public function actionGetpdfCertificate($levelId)
    {

        $criteria = new CDbCriteria;
        $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
        $criteria->condition = 'TM_STP_Student_Id=:student AND TM_STP_Standard_Id=:standard';
        $criteria->params = array(':student' => Yii::app()->user->Id, ':standard' => Yii::app()->session['standard']);
        $studenttotal = StudentPoints::model()->find($criteria);
        $pointlevels = array('total' => $studenttotal->totalpoints);
        $levels = Levels::model()->findAll(array('condition' => 'TM_LV_Status=0'));
        $levelcount = Studentlevels::model()->count(array('condition' => 'TM_STL_Student_Id=' . Yii::app()->user->Id));
        /*foreach ($levels AS $level):


            if ($pointlevels['total'] >= $level->TM_LV_Minpoint & $pointlevels['total'] < $level->TM_LV_MaxPoint):
                $studentlevelcount = Studentlevels::model()->count(array('condition' => 'TM_STL_Student_Id=' . Yii::app()->user->Id . ' AND TM_STL_Level_Id=' . $level->TM_LV_Id));
                if ($studentlevelcount == 0):
                    $addlevel = new Studentlevels();
                    $addlevel->TM_STL_Level_Id = $level->TM_LV_Id;
                    $addlevel->TM_STL_Student_Id = Yii::app()->user->Id;
                    $addlevel->TM_STL_Order = $levelcount + 1;
                    $addlevel->save(false);
                    $pointlevels['newlevel'] = true;

                else:
                    $pointlevels['newlevel'] = false;
                endif;

                $pointlevels['studentlevel'] = $level->TM_LV_Id;


            endif;
        endforeach;*/


        $nak = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;
        $standard = Standard::model()->findbyPk(Yii::app()->session['standard'])->TM_SD_Name;

        $html = '<style>
        body { font-family: lucidahand;  }
.bronze{font-size: 45px;margin-top: 30px;color: #bf8f00;font-weight: 700; margin-bottom: 20px}
.nameb{font-size: 40px;font-weight: 600;text-decoration: underline;}
.namebone{font-size: 40px;font-weight: 700;}
div.rounded {
    border:3mm solid #FFD400;

    border-radius: 3mm / 3mm;
    padding: 1em;
    margin-bottom: 95px;
    }



</style>';

        $html .= '<div class="rounded " style="height: 100%">';

        $html .= '<div  align="center" class="tocenter" style="margin-bottom: 70px;"><img src="' . Yii::app()->request->baseUrl . '/images/logo.png "></div>';
        $html .= '<div  align="center" class="namebone" style="margin-bottom: 70px;">Academic Achievement Award</div>';
        $html .= '<div  align="center" class="tocenter" style="margin-bottom: 50px;"><img src="' . Yii::app()->request->baseUrl . '/images/goloden.png "></div>';
        $html .= '<div  align="center"><big>This is to certify that</big></div>';
        $html .= '<div  align="center" class="nameb"><u>' . $firstname . ' ' . $lastname . '</u></div>';
        /*if($pointlevels['studentlevel']!=''):*/

        $html .= '<div  align="center" style="margin-bottom: 20px"><big>has scored<b> ' . Levels::model()->find(array('condition' => "TM_LV_Id='" . $levelId . "'"))->TM_LV_Minpoint . '</b> points in Standard <b>' . $standard . '</b> to become a </big></div>';
        $html .= '<div  align="center" class="bronze"><b>' . Levels::model()->find(array('condition' => "TM_LV_Id='" . $levelId . "'"))->TM_LV_Name . '</b></div>';
        /*else:
            $html .= '<div  align="center"><big>has scored<b> '.Levels::model()->find(array('condition' => "TM_LV_Id='" .$levelId. "'"))->TM_LV_Minpoint.'</b> points in Standard <b>'.$standard.'</b>  </big></div></div>';
            endif;*/

        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STL_Level_Id='.$levelId.' AND  TM_STL_Student_Id=' . Yii::app()->user->Id;
        $certidate=strtotime(Studentlevels::model()->find($criteria)->TM_STL_Date);


        $html .= "<div  align='center' style='margin-bottom: 10px;'>Date:" . date('d-m-Y',$certidate). "</div>";


        $html .= "</div>";


        /*$header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="33%"><span style="font-weight: lighter; color: #afafaf; ">TabbieMath</span></td>
<td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
<td width="33%" style="text-align: right;font-weight: lighter;  color: #afafaf; ">' . $firstname . ' ' . $lastname . '</td></tr>
<tr><td width="33%"><span style="font-weight: lighter; "></span></td>
<td width="33%" align="center" style="font-weight: lighter; "></td>
<td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf; "></td>
</tr></table>';*/

        $mpdf = new mPDF();
        /*$mpdf->SetHTMLHeader($header);*/
        $mpdf->SetWatermarkText('tabbiemath', .1);
        $mpdf->showWatermarkText = false;

        /*$mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
<td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">Disclaimer : This is the property ofTabbie Me Educational Solutions PVT Ltd.</span></td>
</tr></table>
');*/

        $name=Levels::model()->find(array('condition' => "TM_LV_Id='" . $levelId . "'"))->TM_LV_Name.'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');
    }
    public function generateRandomStringUpload($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }
    public function actionuploadImage(){
        $path = Yii::app()->basePath."/../images/userimages/";


        $valid_formats = array("jpg", "png", "gif", "bmp","JPG","PNG","GIF","BMP");
        if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
        {

            $name = $_FILES['photoimgstud']['name'];
            $size = $_FILES['photoimgstud']['size']/1024;


            if(strlen($name))
            {

                list($txt, $ext) = explode(".", $name);
                if(in_array($ext,$valid_formats))
                {
                    if($size<(2000))
                    {

                        $user=Yii::app()->user->id;
                        $randomname=$this->generateRandomStringUpload();
                        $imgname=$user.$randomname.'.'.$ext;
                        $tmp = $_FILES['photoimgstud']['tmp_name'];
                        if(move_uploaded_file($tmp, $path.$imgname))
                        {



                            echo $imgname;
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 2MB";
                }
                else
                    echo "Invalid file format..";
            }

            else
                echo "Please select image..!";


        }

    }
    public function actionuploadImageSave(){
        $path = Yii::app()->basePath."/../images/userimages/";


        $valid_formats = array("jpg", "png", "gif", "bmp");
        if(isset($_POST['name']))
        {

            $name = $_POST['name'];


            if(strlen($name)) {

                $user = Yii::app()->user->id;
                $connection = CActiveRecord::getDbConnection();
                $sql = "UPDATE tm_student SET TM_STU_Image='". addslashes($name)."' WHERE TM_STU_User_Id='$user'";
                $command = $connection->createCommand($sql);
                $dataReader = $command->query();
            }

            echo $name;



        }

    }
    public function GetRevisionQUestionCount($id)
    {
        $returnval=array();
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=' . $id . ' AND TM_STU_QN_Type NOT IN (6,7)';
        $totalonline = StudentQuestions::model()->count($criteria);
        $returnval['online']=$totalonline;
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id=' . $id . ' AND TM_STU_QN_Type IN (6,7)';
        $totalpaper =StudentQuestions::model()->count($criteria);     
        $returnval['paper']=$totalpaper;   
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STU_QN_Test_Id='. $id;
        $totalquestions =StudentQuestions::model()->count($criteria);
        $returnval['total']=$totalquestions;
        return $returnval;         
    }
    public function actionStudentprogressreport()
    {
        $this->layout = '//layouts/admincolumn2';
        if(isset($_GET['search']))
        {
            $formvals=array();
            $connection = CActiveRecord::getDbConnection();
            $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,b.username AS student,b.id,c.username AS parent,c.email,e.TM_SPN_Id,f.TM_PN_Name,
                 (SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS g WHERE g.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND g.TM_STU_TT_Plan=e.TM_SPN_PlanId ) AS totalrevision,
                 (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS h WHERE h.TM_STMK_Student_Id=a.TM_STU_User_Id AND h.TM_STMK_Plan_Id=e.TM_SPN_PlanId ) AS totalmocks,
                 (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chr LEFT JOIN tm_chalanges AS ch ON chr.TM_CE_RE_Chalange_Id=ch.TM_CL_Id WHERE ch.TM_CL_Plan=e.TM_SPN_PlanId AND chr.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id) AS totalchallenge,
                 (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId) AS totalpoints ";
            if($_GET['date']=='before'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn < '".$_GET['fromdate']."' ) AS periodrevision,
                       (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date < '".$_GET['fromdate']."'  ) AS periodmocks,
                       (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date < '".$_GET['fromdate']."') AS periodchallenge,
                       (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date < '".$_GET['fromdate']."' ) AS periodpoints
                       ";
                endif;
            elseif($_GET['date']=='after'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn > '".$_GET['fromdate']."' ) AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date > '".$_GET['fromdate']."'  ) AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date > '".$_GET['fromdate']."') AS periodchallenge,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date > '".$_GET['fromdate']."' ) AS periodpoints  ";
                endif;
            else:
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!='' && $_GET['todate']):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $formvals['todate']=$_GET['todate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodchallenge,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ) AS periodpoints ";

                endif;
            endif;
            $sql.="FROM tm_student AS a
                  LEFT JOIN tm_users AS b ON b.id=a.TM_STU_User_Id
                  LEFT JOIN tm_users AS c ON c.id=a.TM_STU_Parent_Id
                  INNER JOIN tm_student_plan AS e ON a.TM_STU_User_Id=e.TM_SPN_StudentId
                  INNER JOIN tm_plan AS f ON e.TM_SPN_PlanId=f.TM_PN_Id
                  WHERE a.TM_STU_Status IN(0,1,2)
                  ";
            if($_GET['school']!=''):
                if($_GET['school']=='0'):
                    $formvals['school']=$_GET['school'];
                    $formvals['schooltext']=$_GET['schooltext'];
                    $sql.=" AND a.TM_STU_SchoolCustomname LIKE '%".$_GET['schooltext']."%' ";
                else:
                    $formvals['school']=$_GET['school'];
                    $sql.=" AND a.TM_STU_School='".$_GET['school']."'";
                endif;
            endif;

            if($_GET['plan']!=''):
                $formvals['plan']=$_GET['plan'];
                $sql.=" AND e.TM_SPN_PlanId =".$_GET['plan'];
            endif;
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            $total_pages = count($dataval);

            $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,b.username AS student,b.id,c.username AS parent,c.email,e.TM_SPN_Id,f.TM_PN_Name,
                 (SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS g WHERE g.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND g.TM_STU_TT_Plan=e.TM_SPN_PlanId ) AS totalrevision,
                 (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS h WHERE h.TM_STMK_Student_Id=a.TM_STU_User_Id AND h.TM_STMK_Plan_Id=e.TM_SPN_PlanId ) AS totalmocks,
                 (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chr LEFT JOIN tm_chalanges AS ch ON chr.TM_CE_RE_Chalange_Id=ch.TM_CL_Id WHERE ch.TM_CL_Plan=e.TM_SPN_PlanId AND chr.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id) AS totalchallenge,
                 (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId) AS totalpoints ";
            $criteria='search=search';
            if($_GET['date']=='before'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn < '".$_GET['fromdate']."' ) AS periodrevision,
                       (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date < '".$_GET['fromdate']."'  ) AS periodmocks,
                       (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date < '".$_GET['fromdate']."') AS periodchallenge,
                       (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date < '".$_GET['fromdate']."' ) AS periodpoints
                       ";
                endif;
                $criteria.='&date=before&fromdate='.$_GET['fromdate'].'';
            elseif($_GET['date']=='after'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn > '".$_GET['fromdate']."' ) AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date > '".$_GET['fromdate']."'  ) AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date > '".$_GET['fromdate']."') AS periodchallenge,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date > '".$_GET['fromdate']."' ) AS periodpoints  ";
                endif;
                $criteria.='&date=after&fromdate='.$_GET['fromdate'].'';
            else:
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!='' && $_GET['todate']):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $formvals['todate']=$_GET['todate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodchallenge,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ) AS periodpoints ";

                endif;
                $criteria.='&fromdate='.$_GET['fromdate'].'&todate='.$_GET['todate'].'';
            endif;
            $sql.="FROM tm_student AS a
                  LEFT JOIN tm_users AS b ON b.id=a.TM_STU_User_Id
                  LEFT JOIN tm_users AS c ON c.id=a.TM_STU_Parent_Id
                  INNER JOIN tm_student_plan AS e ON a.TM_STU_User_Id=e.TM_SPN_StudentId
                  INNER JOIN tm_plan AS f ON e.TM_SPN_PlanId=f.TM_PN_Id
                  WHERE a.TM_STU_Status IN(0,1,2)
                  ";
            if($_GET['school']!=''):
                if($_GET['school']=='0'):
                    $formvals['school']=$_GET['school'];
                    $formvals['schooltext']=$_GET['schooltext'];
                    $sql.=" AND a.TM_STU_SchoolCustomname LIKE '%".$_GET['schooltext']."%' ";
                    $criteria.='&school=0';
                    $criteria.='&schooltext='.$_GET['schooltext'].'';
                else:
                    $formvals['school']=$_GET['school'];
                    $sql.=" AND a.TM_STU_School='".$_GET['school']."'";
                    $criteria.='&school='.$_GET['school'].'';
                endif;

            endif;
            $criteria.='&school='.$_GET['school'].'';
            if($_GET['plan']!=''):
                $formvals['plan']=$_GET['plan'];
                $sql.=" AND e.TM_SPN_PlanId =".$_GET['plan'];
                $criteria.='&plan='.$_GET['plan'].'';
            endif;
            $page = $_GET['page'];
            $limit = 50;

            if($page)
                $offset = ($page - 1) * $limit; 			//first item to display on this page
            else
                $offset = 0;
            $sql.=" LIMIT $offset, $limit";
            //echo $criteria;

            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            $dataitems='';
            if($_GET['plan']!=''):
                $sqlchapt="SELECT TM_TP_Id,TM_TP_Name FROM tm_topic AS a LEFT JOIN tm_plan AS b ON b.TM_PN_Standard_Id=a.TM_TP_Standard_Id WHERE TM_PN_Id=".$_GET['plan'];
                $command=$connection->createCommand($sqlchapt);
                $dataReader=$command->query();
                $chapters=$dataReader->readAll();
            endif;
            if(count($dataval)>0):
                foreach($dataval AS $data):
                    //echo $canceldate;exit;
                    $dataitems.="<tr>
                        <td>".$data['student']."</td>
                        <td>".$data['TM_STU_First_Name']." ".$data['TM_STU_Last_Name']."</td>
                        <td>".($data['TM_STU_School']=='0'? $data['TM_STU_SchoolCustomname']: School::model()->findByPk($data['TM_STU_School'])->TM_SCL_Name)."</td>
                        <td>".$data['parent']."</td>
                        <td>".$data['email']."</td>
                        <td>".$data['TM_PN_Name']."</td>
                        <td>".($data['periodrevision']!=''?$data['periodrevision']:'0')."</td>
                        <td>".($data['totalrevision']!=''?$data['totalrevision']:'0')."</td>
                        <td>".($data['periodmocks']!=''?$data['periodmocks']:'0')."</td>
                        <td>".($data['totalmocks']!=''?$data['totalmocks']:'0')."</td>
                        <td>".($data['periodchallenge']!=''?$data['periodchallenge']:'0')."</td>
                        <td>".($data['totalchallenge']!=''?$data['totalchallenge']:'0')."</td>
                        <td>".($data['periodpoints']!=''?$data['periodpoints']:'0')."</td>
                        <td>".($data['totalpoints']!=''?$data['totalpoints']:'0')."</td>";
                    if(count($chapters)>0):
                        foreach($chapters AS $chapter):
                            $totalcount=$this->GetChapterAverage($chapter['TM_TP_Id'],$data['id']);
                            $average=$this->GetChapterCount($chapter['TM_TP_Id'],$data['id'],$totalcount['attemptedcount'],$totalcount['correctcount']);
                            $dataitems.="<td>".$average['average']."%</td>";
                            $dataitems.="<td>".$average['correct']."%</td>";
                        endforeach;
                    endif;
                    $dataitems.="</tr>";
                endforeach;
            else:
                $dataitems="<tr><td colspan='14' class='empty'><span class='empty' style='font-style: italic;'>No results found.</span></td></tr>";
            endif;
            $pages=ceil($total_pages / $limit);
            if($_GET['page']==''):
                $count=1;
            else:
                $count=$_GET['page'];
            endif;
            $nopages='';

            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page):
                    $class='class="active"';
                else:
                    $class='';
                endif;
                $nopages.='<li><a  '.$class.' href="'.Yii::app()->request->baseUrl.'/student/Studentprogressreport?'.$criteria.'&page='.$i.'">'.$i.'</a></li>';
            }

            $nopages.='<li><a href="'.Yii::app()->request->baseUrl.'/student/Studentprogressreport?'.$criteria.'&page='.($count + 1).'">Next</a></li>';
        }

        $this->render('studentreport',array('dataitems'=>$dataitems,'formvals'=>$formvals,'chapters'=>$chapters,'nopages'=>$nopages));

    }
    public function GetChapterAverage($chapter,$student)
    {
        
        $connection = CActiveRecord::getDbConnection();                       
        $sqlat="SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$student."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$student."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$student."' AND TM_CEUQ_Parent_Id=0";                
        $command=$connection->createCommand($sqlat);
        $dataReader=$command->query();
        $attempted=$dataReader->readAll();
        $attemptedquestions='';
        foreach($attempted AS $key=>$attemp):
        $attemptedquestions.=($key==0?'':',').$attemp['question'];
        endforeach;         
        $sql1=" SELECT TM_STU_QN_Question_Id AS question FROM tm_student_questions WHERE TM_STU_QN_Student_Id='".$student."' AND TM_STU_QN_Parent_Id=0 AND TM_STU_QN_Flag!=0 AND TM_STU_QN_Mark!=0 UNION SELECT TM_STMKQT_Question_Id AS question FROM tm_studentmockquestions WHERE TM_STMKQT_Student_Id='".$student."' AND TM_STMKQT_Parent_Id=0 AND TM_STMKQT_Flag!=0 AND TM_STMKQT_Marks!=0 UNION SELECT TM_CEUQ_Question_Id AS question FROM tm_chalangeuserquestions WHERE TM_CEUQ_User_Id='".$student."' AND TM_CEUQ_Parent_Id=0 AND TM_CEUQ_Marks!=0 ";        
        $command=$connection->createCommand($sql1);
        $dataReader=$command->query();
        $correctones=$dataReader->readAll();        
        $correctquestions='';
        foreach($correctones AS $key=>$correct):
        $correctquestions.=($key==0?'':',').$correct['question'];
        endforeach;
        if($attemptedquestions!=''): 
        $sql="SELECT count(a.TM_QN_Id) AS attemptedcount, a.TM_QN_Topic_Id,b.TM_TP_Name, ";
            if($correctquestions!=''):
                $sql.="(SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Topic_Id=a.TM_QN_Topic_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$correctquestions." )) AS correctcount ";
            else:
                $sql.="0 AS correctcount";
            endif;
        $sql.=" FROM 
                tm_question AS a,tm_topic AS b WHERE 
                a.TM_QN_Parent_Id=0 AND a.TM_QN_Topic_Id=b.TM_TP_Id AND b.TM_TP_Id='".$chapter."' AND TM_QN_Id IN (".$attemptedquestions.") group by TM_QN_Topic_Id";
        else:
            $sql="SELECT 0 AS attemptedcount, 0 AS correctcount FROM tm_question";
        endif;                                                      
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $chapters=$dataReader->read();         
        return $chapters;          
    }
   public function actionDownloadprogress()
    {
 			$connection = CActiveRecord::getDbConnection();            
            $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,b.username AS student,b.id,c.username AS parent,c.email,e.TM_SPN_Id,f.TM_PN_Name,
                 (SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS g WHERE g.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND g.TM_STU_TT_Plan=e.TM_SPN_PlanId ) AS totalrevision,
                 (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS h WHERE h.TM_STMK_Student_Id=a.TM_STU_User_Id AND h.TM_STMK_Plan_Id=e.TM_SPN_PlanId ) AS totalmocks,
                 (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chr LEFT JOIN tm_chalanges AS ch ON chr.TM_CE_RE_Chalange_Id=ch.TM_CL_Id WHERE ch.TM_CL_Plan=e.TM_SPN_PlanId AND chr.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id) AS totalchallenge, 
                 (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId) AS totalpoints ";
            if($_GET['date']=='before'):                              
                if($_GET['fromdate']!=''):                                  
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn < '".$_GET['fromdate']."' ) AS periodrevision,
                       (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date < '".$_GET['fromdate']."'  ) AS periodmocks,
                       (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date < '".$_GET['fromdate']."') AS periodchallenge,
                       (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date < '".$_GET['fromdate']."' ) AS periodpoints 
                       ";                    
                endif;
            elseif($_GET['date']=='after'):                
                if($_GET['fromdate']!=''):                   
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn > '".$_GET['fromdate']."' ) AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date > '".$_GET['fromdate']."'  ) AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date > '".$_GET['fromdate']."') AS periodchallenge,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date > '".$_GET['fromdate']."' ) AS periodpoints  ";                    
                endif;
            else:                
                if($_GET['fromdate']!='' && $_GET['todate']):                                                       
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodchallenge, 
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ) AS periodpoints ";                                     
                endif;
            endif;                                        
            $sql.="FROM tm_student AS a 
                  LEFT JOIN tm_users AS b ON b.id=a.TM_STU_User_Id 
                  LEFT JOIN tm_users AS c ON c.id=a.TM_STU_Parent_Id                  
                  INNER JOIN tm_student_plan AS e ON a.TM_STU_User_Id=e.TM_SPN_StudentId
                  INNER JOIN tm_plan AS f ON e.TM_SPN_PlanId=f.TM_PN_Id 
                  WHERE a.TM_STU_Status IN(0,1,2)                 
                  ";    
            if($_GET['school']!=''):                
                $sql.=" AND a.TM_STU_School LIKE '%".$_GET['school']."%' ";
            endif;
            if($_GET['plan']!=''):                
                $sql.=" AND e.TM_SPN_PlanId =".$_GET['plan'];                
            endif;                                                                      
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            $dataitems='';
            if($_GET['plan']!=''):
                $sqlchapt="SELECT TM_TP_Id,TM_TP_Name FROM tm_topic AS a LEFT JOIN tm_plan AS b ON b.TM_PN_Standard_Id=a.TM_TP_Standard_Id WHERE TM_PN_Id=".$_GET['plan'];
                $command=$connection->createCommand($sqlchapt);
                $dataReader=$command->query();
                $chapters=$dataReader->readAll();
            endif;  
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=progressreport.csv');
            $output = fopen('php://output', 'w');
            $headers=array(
            "Student Username",
            "Student Name",
            "School",
            "Parent Username",
            "Parent Email",
            "Subbscription",
            "Number of Revisions",
            "Total Number of Revisions",
            "Number of Mocks",
            "Total Number of Mocks",
            "Number of Challenges",
            "Total Number of Challenges",
            "Points Scored",
            "Total Points Scored"
            );                        
            
            if(count($chapters)>0):
                foreach($chapters AS $chapter):
                    array_push($headers,$chapter['TM_TP_Name'].": % Completed");
                    array_push($headers,$chapter['TM_TP_Name'].": % Correcct");
                endforeach;
            endif; 
            function cleanData(&$str)
            {
                $str = preg_replace("/\t/", "\\t", $str);
                $str = preg_replace("/\r?\n/", "\\n", $str);
                if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
            }            
            fputcsv($output,$headers );            
            if(count($dataval)>0):
                $flag = false;
                foreach($dataval AS $data):
                    $row=array($data['student'],$data['TM_STU_First_Name']." ".$data['TM_STU_Last_Name'],
                        ($data['TM_STU_School']=='0'? $data['TM_STU_SchoolCustomname']: School::model()->findByPk($data['TM_STU_School'])->TM_SCL_Name),
                    $data['parent'],$data['email'],$data['TM_PN_Name'],$data['periodrevision'],
                    $data['totalrevision'],$data['periodmocks'],$data['totalmocks'],$data['periodchallenge'],$data['totalchallenge'],
                    $data['periodpoints'],$data['totalpoints']);
                    if(count($chapters)>0):
                        foreach($chapters AS $chapter):
                            $totalcount=$this->GetChapterAverage($chapter['TM_TP_Id'],$data['id']);
                            $average=$this->GetChapterCount($chapter['TM_TP_Id'],$data['id'],$totalcount['attemptedcount'],$totalcount['correctcount']);
                            array_push($row,$average['average']);
                            array_push($row,$average['correct']);
                        endforeach;
                    endif;
                    if(!$flag) {
                        // display field/column names as first row
                        fputcsv($output, $row);
                        $flag = true;
                    }
                    array_walk($row, 'cleanData');
                    fputcsv($output, $row);
                endforeach;            
            endif;                                        
    }

    public function actionStarthomework($id)
    {
        $criteria=new CDbCriteria;
        $criteria->condition='TM_HQ_Homework_Id='.$id.' AND TM_HQ_Type NOT IN (6,7)';
        $totalonline=HomeworkQuestions::model()->count($criteria);
        $criteria=new CDbCriteria;
        $criteria->condition='TM_HQ_Homework_Id='.$id.' AND TM_HQ_Type IN (6,7)';
        $totalpaper=HomeworkQuestions::model()->count($criteria);
        $totalquestions=$totalonline+$totalpaper;
        $test = StudentHomeworks::model()->findByAttributes(array('TM_STHW_HomeWork_Id'=>$id,'TM_STHW_Student_Id'=>Yii::app()->user->id));
        $this->render('starthomework', array('id' => $id, 'test' => $test, 'totalonline' => $totalonline, 'totalpaper' => $totalpaper, 'totalquestions' => $totalquestions));
    }

    public function actionHomeworktest($id)
    {
        if (isset($_POST['action']['GoNext'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STHWQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studenthomeworkquestions::model()->findByAttributes(
                        array('TM_STHWQT_Question_Id' => $child->TM_QN_Id, 'TM_STHWQT_Mock_Id' => $id, 'TM_STHWQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studenthomeworkquestions();
                    endif;
                    $selectedquestion->TM_STHWQT_Mock_Id = $id;
                    $selectedquestion->TM_STHWQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STHWQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STHWQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STHWQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = Studenthomeworkquestions::model()->findByAttributes(array('TM_STHWQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Type NOT IN (6,7) AND  TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Order>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STHWQT_Order ASC,TM_STHWQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid'])
                )
            );
        }
        if (isset($_POST['action']['Save'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STHWQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studenthomeworkquestions::model()->findByAttributes(
                        array('TM_STHWQT_Question_Id' => $child->TM_QN_Id, 'TM_STHWQT_Mock_Id' => $id, 'TM_STHWQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studenthomeworkquestions();
                    endif;
                    $selectedquestion->TM_STHWQT_Mock_Id = $id;
                    $selectedquestion->TM_STHWQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STHWQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STHWQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STHWQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = Studenthomeworkquestions::model()->findByAttributes(array('TM_STHWQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Type NOT IN (6,7) AND  TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Id>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STHWQT_Order ASC, TM_STHWQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid']-1)
                )
            );
        }
        if (isset($_POST['action']['GetPrevios'])):

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    $answer = implode(',', $_POST['answer']);
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STHWQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studenthomeworkquestions::model()->findByAttributes(
                        array('TM_STHWQT_Question_Id' => $child->TM_QN_Id, 'TM_STHWQT_Mock_Id' => $id, 'TM_STHWQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studenthomeworkquestions();
                    endif;
                    $selectedquestion->TM_STHWQT_Mock_Id = $id;
                    $selectedquestion->TM_STHWQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STHWQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STHWQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STHWQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            $questionid = Studenthomeworkquestions::model()->findByAttributes(array('TM_STHWQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Status IN (0,1,2) AND TM_STHWQT_Type NOT IN (6,7) AND TM_STHWQT_Order<:tableid AND TM_STHWQT_Parent_Id=0',
                    'limit' => 1,
                    'order' => 'TM_STHWQT_Order DESC, TM_STHWQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid'])
                )
            );
        endif;
        if (isset($_POST['action']['DoLater'])):
            if ($_POST['QuestionType'] != '5'):
                if (isset($_POST['answer'])):
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                    if ($_POST['QuestionType'] == '1'):
                        $answer = implode(',', $_POST['answer']);
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                        $answer = $_POST['answer'];
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($_POST['QuestionType'] == '4'):
                        $selectedquestion->TM_STHWQT_Answer = $_POST['answer'];
                    endif;
                    $selectedquestion->TM_STHWQT_Status = '2';
                    $selectedquestion->save(false);
                else:
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                    $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                    $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                    $selectedquestion->TM_STHWQT_Status = '2';
                    $selectedquestion->save(false);
                endif;
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    if (isset($_POST['answer' . $child->TM_QN_Id])):
                        $childquestion = Studenthomeworkquestions::model()->findByAttributes(
                            array('TM_STHWQT_Question_Id' => $child->TM_QN_Id, 'TM_STHWQT_Mock_Id' => $id, 'TM_STHWQT_Student_Id' => Yii::app()->user->id)
                        );
                        if (count($childquestion) == '1'):
                            $selectedquestion = $childquestion;
                        else:
                            $selectedquestion = new Studenthomeworkquestions();
                        endif;
                        $selectedquestion->TM_STHWQT_Mock_Id = $id;
                        $selectedquestion->TM_STHWQT_Student_Id = Yii::app()->user->id;
                        $selectedquestion->TM_STHWQT_Question_Id = $child->TM_QN_Id;
                        if ($child->TM_QN_Type_Id == '1'):
                            $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                            $selectedquestion->TM_STHWQT_Answer = $answer;
                        elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                            $answer = $_POST['answer' . $child->TM_QN_Id];
                            $selectedquestion->TM_STHWQT_Answer = $answer;
                        elseif ($child->TM_QN_Type_Id == '4'):
                            $selectedquestion->TM_STHWQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                        endif;
                        $selectedquestion->TM_STHWQT_Parent_Id = $_POST['QuestionId'];
                        $selectedquestion->save(false);
                    endif;
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                $selectedquestion->TM_STHWQT_Status = '2';
                $selectedquestion->save(false);
            endif;
            $questionid = Studenthomeworkquestions::model()->findByAttributes(array('TM_STHWQT_Mock_Id' => $id),
                array(
                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Type NOT IN (6,7) AND  TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Order>:tableid',
                    'limit' => 1,
                    'order' => 'TM_STHWQT_Order ASC, TM_STHWQT_Status ASC',
                    'params' => array(':student' => Yii::app()->user->id, ':tableid' => $_POST['QuestionTableid'])
                )
           );
        endif;

        if (isset($_POST['action']['Complete'])) {

            if ($_POST['QuestionType'] != '5'):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                if ($_POST['QuestionType'] == '1'):
                    if(count($_POST['answer'])>0)
                    {
                        $answer = implode(',', $_POST['answer']);
                    }
                    else
                    {
                        $answer='';
                    }
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '2' || $_POST['QuestionType'] == '3'):
                    $answer = $_POST['answer'];
                    $selectedquestion->TM_STHWQT_Answer = $answer;
                elseif ($_POST['QuestionType'] == '4'):
                    $selectedquestion->TM_STHWQT_Answer = $_POST['answer'];
                endif;
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            else:
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $_POST['QuestionId'] . "'", "order" => "TM_QN_Id ASC"));
                foreach ($questions AS $child):
                    $childquestion = Studenthomeworkquestions::model()->findByAttributes(
                        array('TM_STHWQT_Question_Id' => $child->TM_QN_Id, 'TM_STHWQT_Mock_Id' => $id, 'TM_STHWQT_Student_Id' => Yii::app()->user->id)
                    );
                    if (count($childquestion) == '1'):
                        $selectedquestion = $childquestion;
                    else:
                        $selectedquestion = new Studenthomeworkquestions();
                    endif;
                    $selectedquestion->TM_STHWQT_Mock_Id = $id;
                    $selectedquestion->TM_STHWQT_Student_Id = Yii::app()->user->id;
                    $selectedquestion->TM_STHWQT_Question_Id = $child->TM_QN_Id;
                    if ($child->TM_QN_Type_Id == '1'):
                        $answer = implode(',', $_POST['answer' . $child->TM_QN_Id]);
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '2' || $child->TM_QN_Type_Id == '3'):
                        $answer = $_POST['answer' . $child->TM_QN_Id];
                        $selectedquestion->TM_STHWQT_Answer = $answer;
                    elseif ($child->TM_QN_Type_Id == '4'):
                        $selectedquestion->TM_STHWQT_Answer = $_POST['answer' . $child->TM_QN_Id];
                    endif;
                    $selectedquestion->TM_STHWQT_Parent_Id = $_POST['QuestionId'];
                    $selectedquestion->save(false);
                endforeach;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHWQT_Question_Id=:question AND TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0';
                $criteria->params = array(':question' => $_POST['QuestionId'], ':student' => Yii::app()->user->id, ':test' => $id);
                $selectedquestion = Studenthomeworkquestions::model()->find($criteria);
                $selectedquestion->TM_STHWQT_Status = '1';
                $selectedquestion->save(false);
            endif;
            if ($this->GetPaperCountHW($id) != 0):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $homework->TM_STHW_Status = '2';
                $homework->save(false);
                $this->redirect(array('showhomeworkpaper', 'id' => $id));
            else:
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
                $homeworks=StudentHomeworks::model()->find($criteria);
                $homework = StudentHomeworks::model()->findByPk($homeworks->TM_STHW_Id);
                //code by amal
                if($_POST['files']==0):
                    $homework->TM_STHW_Status = '4';
                    $redirect=array('homeworkcomplete', 'id' => $id);
                elseif($_POST['files']==1):
                    if($_POST['action']['Complete']=='SUBMIT LATER'):
                        $hwanswers=New Studenthomeworkanswers();
                        $hwanswers->TM_STHWAR_Student_Id=Yii::app()->user->id;
                        $hwanswers->TM_STHWAR_HW_Id=$id;
                        $target_path = "homework/";
                        $filename=basename( $_FILES['Answersheet']['name']);
                        if($filename!=''):
                            $target_path = $target_path.$filename ;
                            if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
                                $hwanswers->TM_STHWAR_Filename=$filename;
                            }
                            $hwanswers->save(false);
                        endif;
                        $homework->TM_STHW_Status = '3';
                        $redirect=array('home');
                    else:
                        $hwanswers=New Studenthomeworkanswers();
                        $hwanswers->TM_STHWAR_Student_Id=Yii::app()->user->id;
                        $hwanswers->TM_STHWAR_HW_Id=$id;
                        $target_path = "homework/";
                        $filename=uniqid(). basename( $_FILES['Answersheet']['name']);
                        $target_path = $target_path.$filename ;
                        if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
                            $hwanswers->TM_STHWAR_Filename=$filename;
                        }
                        $hwanswers->save(false);
                        $this->redirect(array('Homeworkcomplete','id'=>$id));
                    endif;
                endif;
                //ends
                //print_r($homework->TM_STHW_Status);exit;
                $homework->save(false);
                $this->redirect($redirect);
            endif;
            //$this->redirect(array('homeworkcomplete','id'=>$id));
        }

        if (!isset($_POST['action'])):
            if (isset($_GET['questionnum'])):
                $questionid = Studenthomeworkquestions::model()->findByAttributes(array('TM_STHWQT_Mock_Id' => $id),
                    array(
                        'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Status IN (0,1,2) AND TM_STHWQT_Type NOT IN (6,7)  AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Question_Id=' . str_replace("question_", "", base64_decode($_GET['questionnum'])),
                        'limit' => 1,
                        'order' => 'TM_STHWQT_Question_Id ASC, TM_STHWQT_Status ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            else:
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                //echo $homework->TM_STHW_Id;exit;
                $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $test->TM_STHW_Status = '1';
                $test->save(false);
                $questionid = Studenthomeworkquestions::model()->findByAttributes(array('TM_STHWQT_Mock_Id' => $id),
                    array(
                        'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Status IN (0) AND TM_STHWQT_Type NOT IN (6,7)  AND TM_STHWQT_Parent_Id=0',
                        'limit' => 1,
                        'order' => 'TM_STHWQT_Order ASC, TM_STHWQT_Status ASC',
                        'params' => array(':student' => Yii::app()->user->id)
                    )
                );
            endif;
        endif;
        if (count($questionid)):
            $question = Questions::model()->findByPk($questionid->TM_STHWQT_Question_Id);
            //print_r($question);exit;
            $this->renderPartial('homeworktest', array('testid' => $id, 'question' => $question, 'questionid' => $questionid));
        endif;
    }

    public function actionShowhomeworkpaper($id)
    {        
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
        $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
        $homework=StudentHomeworks::model()->find($criteria);
        $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
        $homework->TM_STHW_Status = '2';
        $homework->save(false);
        $paperquestions = $this->GetPaperQuestionsHomework($id);
        $this->render('showpaperquestionhw', array('id' => $id, 'paperquestions' => $paperquestions));
    }
    public function GetHomeWorkQuestions($id, $question)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Type NOT IN (6,7) AND TM_STHWQT_Student_Id=:student';
        $criteria->params = array(':test' => $id,':student' => Yii::app()->user->id);
        $criteria->order = 'TM_STHWQT_Order ASC';
        $selectedquestion = Studenthomeworkquestions::model()->findAll($criteria);
        //print_r($selectedquestion);exit;
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Type IN (6,7) AND TM_STHWQT_Student_Id=:student';
        $criteria->params = array(':test' => $id,':student' => Yii::app()->user->id);
        $papercount = Studenthomeworkquestions::model()->count($criteria);
        $pagination = '';
        foreach ($selectedquestion AS $key => $testquestion):
            $count = $key + 1;
            $class = '';
            if ($testquestion->TM_STHWQT_Question_Id == $question) {
                $questioncount = $count;
            }
            if ($testquestion->TM_STHWQT_Type == '6' || $testquestion->TM_STHWQT_Type == '7') {
                $pagination .= '<li class="skip" ><a href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Paper Only Question">' . $count . '</a></li>';
            } else {
                $title = '';
                $class = '';
                if ($testquestion->TM_STHWQT_Status == '0') {
                    $class = ($testquestion->TM_STHWQT_Question_Id == $question ? 'class="active"' : '');
                    $title = ($testquestion->TM_STHWQT_Question_Id == $question ? 'Current Question' : '');
                } elseif ($testquestion->TM_STHWQT_Status == '1') {
                    $class = ($testquestion->TM_STHWQT_Question_Id == $question ? 'class="active"' : 'class="answered"');
                    $title = ($testquestion->TM_STHWQT_Question_Id == $question ? 'Current Question' : 'Already Answered');
                } elseif ($testquestion->TM_STHWQT_Status == '2') {
                    $class = ($testquestion->TM_STHWQT_Question_Id == $question ? 'class="active"' : 'class="skiped"');
                    $title = ($testquestion->TM_STHWQT_Question_Id == $question ? 'Current Question' : 'Skipped Question');
                }
                $pagination .= '<li ' . $class . ' ><a data-toggle="tooltip" data-placement="bottom" title="' . $title . '" href="' . ($testquestion->TM_STHWQT_Question_Id == $question ? 'javascript:void(0)' : Yii::app()->createUrl('student/homeworktest', array('id' => $id, 'questionnum' => base64_encode('question_' . $testquestion->TM_STHWQT_Question_Id)))) . '">' . $count . '</a></li>';
            }
        endforeach;
        return array('pagination' => $pagination, 'questioncount' => $questioncount, 'totalcount' => $count, 'papercount' => $papercount);
    }
    public function actionHomeworkcomplete($id)
    {
        $connection = CActiveRecord::getDbConnection();
        $paperquestion=count($this->GetPaperQuestionsHomework($id));
        $totalquestions=Studenthomeworkquestions::model()->count(array(
            'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test',
            'params' => array(':student' => Yii::app()->user->id, ':test' => $id),
        ));
        //echo $totalquestions;exit;
        if($totalquestions!=$paperquestion):
            $results = Studenthomeworkquestions::model()->findAll(
                array(
                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Type NOT IN(6,7)',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id),
                    'order'=>'TM_STHWQT_Order ASC'
                )
            );
            $totalmarks = 0;
            $earnedmarks = 0;
            $result = '';
            $marks = 0;
            $questionnumber=1;
            foreach ($results AS $questionkey => $exam):
                //$questionnumber = $questionkey + 1;
                $question = Questions::model()->findByPk($exam->TM_STHWQT_Question_Id);
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5'):
                        $displaymark = 0;
                        $marksdisp = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id' AND TM_AR_Marks NOT IN(0)"));

                        foreach ($marksdisp AS $key => $marksdisplay):
                            $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                        $marks = 0;
                        if ($question->TM_QN_Type_Id != '4' & $exam->TM_STHWQT_Flag != '2'):
                            if ($exam->TM_STHWQT_Answer != ''):
                                $studentanswer = explode(',', $exam->TM_STHWQT_Answer);
                                foreach ($question->answers AS $ans):
                                    if ($ans->TM_AR_Correct == '1'):
                                        if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        }
                                    endif;
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                if (count($studentanswer) > 0):
                                    $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                                    for ($i = 0; $i < count($studentanswer); $i++) {
                                        $answer = Answers::model()->findByPk($studentanswer[$i]);
                                        $correctanswer .= '<li class="list-group-item1" style="display:flex ;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li>';
                                    }
                                    $correctanswer .= '</ul>';
                                endif;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        else:
                            if ($exam->TM_STHWQT_Answer != ''):
                                $correctanswer = '<ul class="list-group" ><lh><b>Your Answer:</b></lh>';
                                $correctanswer .= '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;"><div class="col-md-12">' . $exam->TM_STHWQT_Answer . '</div></li></ul>';
                                foreach ($question->answers AS $ans):
                                    $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                    $useranswer = trim(strtolower($exam->TM_STHWQT_Answer));
                                    for ($i = 0; $i < count($answeroption); $i++) {
                                        $option = trim($answeroption[$i]);

                                        //fix for < > issue
                                        if($option=='&gt'):
                                            $newoption='&gt;';
                                        elseif($option=='&lt'):
                                            $newoption='&lt;';
                                        else:
                                            $newoption=$option;
                                        endif;
                                        if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0):
                                            $marks = $marks + $ans->TM_AR_Marks;
                                        endif;
                                    }
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                            else:
                                foreach ($question->answers AS $ans):
                                    $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                endforeach;
                                $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                            endif;
                        endif;
                        if ($question->TM_QN_Parent_Id == 0):
                            $result .= '<tr><td><div class="col-md-4">';
                            if ($marks == '0'):
                                $exam->TM_STHWQT_Redo_Flag = '1';
                                $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STHWQT_Order . '</span>';

                            elseif ($marks < $displaymark & $marks != '0'):
                                $exam->TM_STHWQT_Redo_Flag = '1';
                                $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question.' . $exam->TM_STHWQT_Order . '</span>';
                            else:
                                $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STHWQT_Order . '</span>';
                            endif;

                            $result .= ' Mark: ' . $marks . '/' . $displaymark . '</div>
                                <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                                <span class="pull-right" style="font-size:11px; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                            //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                            $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                            $result .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                            $result .= ' <div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div><div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div>';
                            $result .= '<div class="col-md-12 Itemtest clickable-row showalert' . $question->TM_QN_Id . '" >

    								<div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                    <div class="col-md-10" id="maildivslidetest"  >
                                    <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                    </div>
                                    <div class="col-md-2 mailadd" style="margi-top:15px;">
                                    <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                    </div>
                                  </div>
                                     <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id=""  hidden >Your message has been sent to admin
                                    </div>  </div>
                                 </td></tr>';
                            //glyphicon glyphicon-star sty
                        endif;
                        $exam->TM_STHWQT_Marks = $marks;
                        $exam->save(false);
                        $earnedmarks = $earnedmarks + $marks;
                    else:
                        // $marksQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $displaymark = 0;
                        $singlemark = '';
                        // foreach ($marksQuestions AS $key => $formarks):
                        //     $gettingmarks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$formarks->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        //     foreach ($gettingmarks AS $key => $marksdisplay):
                        //         $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                        //         $singlemark = $marksdisplay->TM_AR_Marks;
                        //     endforeach;
                        // endforeach;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $count = 1;
                        $childresult = '';
                        $childearnedmarks = 0;
                        foreach ($questions AS $childquestion):
                             
                            $sql4 ="SELECT sum(TM_AR_Marks) AS totalMark,TM_AR_Marks AS singleMark FROM tm_answer WHERE TM_AR_Question_Id = '".$childquestion['TM_QN_Id']."' AND TM_AR_Marks NOT IN(0)";
                            $command=$connection->createCommand($sql4);
                            $dataReader=$command->query();
                            $total1=$dataReader->read();
                              
                            $displaymark = $displaymark + $total1['totalMark'];
                            $singlemark = $total1['singleMark'];
                                    


                            $childresults = Studenthomeworkquestions::model()->find(
                                array(
                                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Question_Id=:questionid',
                                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id, ':questionid' => $childquestion->TM_QN_Id)
                                )
                            );
                            $marks = 0;
                            if ($childquestion->TM_QN_Type_Id != '4' & $childresults->TM_STHWQT_Flag != '2'):
                                if ($childresults->TM_STHWQT_Answer != ''):

                                    $studentanswer = explode(',', $childresults->TM_STHWQT_Answer);
                                    foreach ($childquestion->answers AS $ans):
                                        if ($ans->TM_AR_Correct == '1'):
                                            if (array_search($ans->TM_AR_Id, $studentanswer) !== false) {
                                                $marks = $marks + $ans->TM_AR_Marks;
                                            }
                                        endif;
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    if (count($studentanswer) > 0):
                                        $correctanswer = '<ul class="list-group"><lh><b>Your Answer:</b></lh>';
                                        for ($i = 0; $i < count($studentanswer); $i++) {
                                            $answer = Answers::model()->findByPk($studentanswer[$i]);
                                            $correctanswer .= '<li class="list-group-item1" style="display:flex ;border:none;background-color:inherit;"><div class="col-md-' . ($answer->TM_AR_Image != '' ? '10' : '12') . '">' . $answer->TM_AR_Answer . '</div>' . ($answer->TM_AR_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $answer->TM_AR_Image . '?size=thumbs&type=answer&width=100&height=100" alt=""></div>' : '') . '</li></ul>';

                                        }
                                        $correctanswer .= '</ul>';
                                    endif;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks = $totalmarks + $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit;display:flex;">Question Skipped</li></ul>';
                                endif;
                            else:
                                if ($childresults->TM_STHWQT_Answer != ''):
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1" style="border:none;background-color:inherit;display:flex;">' . $childresults->TM_STHWQT_Answer . '</li></ul>';
                                    foreach ($childquestion->answers AS $ans):
                                        $answeroption = explode(';', strtolower(strip_tags($ans->TM_AR_Answer)));
                                        $useranswer = trim(strtolower($childresults->TM_STHWQT_Answer));
                                        $marked = 0;
                                        for ($i = 0; $i < count($answeroption); $i++) {
                                            $option = trim($answeroption[$i]);

                                            //fix for < > issue
                                            if($option=='&gt'):
                                                $newoption='&gt;';
                                            elseif($option=='&lt'):
                                                $newoption='&lt;';
                                            else:
                                                $newoption=$option;
                                            endif;
                                            if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0 && $marked ==0):
                                                $marks += $ans->TM_AR_Marks;
                                                $marked = 1;
                                            endif;
                                        }
                                        $totalmarks += $ans->TM_AR_Marks;
                                    endforeach;
                                else:
                                    foreach ($childquestion->answers AS $ans):
                                        $totalmarks += $ans->TM_AR_Marks;
                                    endforeach;
                                    $correctanswer = '<ul class="list-group"><li class="list-group-item1"  style="border:none;background-color:inherit; display:flex;">Question Skipped</li></ul>';
                                endif;

                            endif;
                            if ($childquestion->TM_QN_Parent_Id != 0):
                                $childresult .= '<div class="col-md-12">';
                                if ($marks == '0'):
                                    //$exam->TM_STHWQT_Redo_Flag = '1';
                                    $childresult .= '<span class="clrr"><i class="fa fa fa-times"></i><b> Part ' . $count . '</b></span>';
                                elseif ($marks < $singlemark & $marks != '0'):
                                    //$exam->TM_STHWQT_Redo_Flag = '1';
                                    $childresult .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"><b> Part ' . $count . '</b></span>';
                                else:
                                    $childresult .= '<span ><i class="fa fa fa-check"></i><b> Part ' . $count . '</b></span>';
                                endif;
                                $childresult.=' Mark: ' . $marks . '/' . $singlemark . '';
                                $childresult.='<li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-10">' . $childquestion->TM_QN_Question . '</div>' . (($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=thumbs&type=question&width=100&height=100" alt=""></div>' : '')) . '</li></div>';
                                $childresult .= ' <div class="col-md-12">' . $correctanswer . '</div>';
                                $childresult .= '';
                                //glyphicon glyphicon-star sty
                            endif;


                            $earnedmarks = $earnedmarks + $marks;
                            $childearnedmarks = $childearnedmarks + $marks;
                            $count++;
                        endforeach;
                        $marks = $childearnedmarks;
                        $result .= '<tr><td><div class="col-md-4">';
                        if ($marks == '0'):
                            $exam->TM_STHWQT_Redo_Flag = '1';
                            $result .= '<span class="clrr"><i class="fa fa fa-times"></i>  Question ' . $exam->TM_STHWQT_Order . '</span>';
                        elseif ($marks < $displaymark & $marks != '0'):
                            $exam->TM_STHWQT_Redo_Flag = '1';
                            $result .= '<span class="clrr"><img src="' . Yii::app()->request->baseUrl . '/images/rw.png"></i>  Question' . $exam->TM_STHWQT_Order . '</span>';
                        else:
                            $result .= '<span ><i class="fa fa fa-check"></i>  Question ' . $exam->TM_STHWQT_Order . '</span>';
                        endif;

                        $result .= ' Mark: ' . $marks . '/' . $displaymark . '</span></div>
                                <div class="col-md-3 pull-right"><a  href="javascript:void(0)" class="showSolutionItemtest" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"></u>Report Problem</u></a>
                                <span class="pull-right" style="font-size:11px ; padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        //$result .= '<div class="' . ($question->TM_QN_Image != '' ? 'col-md-8' : 'col-md-12') . '">' . $question->TM_QN_Question . '</div>';
                        $result .= '<div class="col-md-12"><li class="list-group-item1" style="display:flex ;border:none;"><div class="col-md-9">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-3"><img class="img-responsive img_right pull-right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</li></div>';
                        $result .= $childresult;
                        $result.= '<div class="col-md-12"><b style="margin-left: 20px;">Solution :</b></div>
                                <div class="col-md-12" ><div class="col-md-12"><p>'.$question->TM_QN_Solutions.'</p></div></div></td></tr>';
                        $result .= '<tr><td><div class="Itemtest clickable-row "   >
                                <div class="sendmailtest suggestion' . $question->TM_QN_Id . '" style="display:none;" id="sendmailtestalert">
                                    <div class="col-md-10" id="maildivslidetest"  >
                                    <textarea name="comment" placeholder="Enter Your Comments Here" class="form-control2 comments' . $question->TM_QN_Id . '"name="comments"  id="comments' . $question->TM_QN_Id . '" ></textarea>
                                    </div>
                                    <div class="col-md-2" style="margi-top:15px;">
                                    <input type="button" class=" btn btn-warning pull-right"id="mailSendtest" value="Send"  data-id="' . $question->TM_QN_Id . '" >
                                    </div>
                                    <div class="col-md-10 mailSendCompletetest' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                    </div>

                                </div></div>
                                </td></tr>';
                        $exam->TM_STHWQT_Marks = $marks;
                        $exam->save(false);
                    endif;
                endif;
                $questionnumber++;
            endforeach;
            $paperqstns = Studenthomeworkquestions::model()->findAll(
                array(
                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Type IN(6,7)',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $id),
                    'order'=>'TM_STHWQT_Order ASC'
                )
            );

            $totpprmarks=0;
            foreach($paperqstns AS $paper):
                $paperqstn = Questions::model()->findByPk($paper->TM_STHWQT_Question_Id);
                $totpprmarks=$totpprmarks + $paperqstn->TM_QN_Totalmarks;
            endforeach;
            $total=$totalmarks + $totpprmarks;
            $percentage = ($earnedmarks / $total) * 100;
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
            $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
            $homework=StudentHomeworks::model()->find($criteria);
            $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
            $test->TM_STHW_Marks = $earnedmarks;
            $test->TM_STHW_TotalMarks = $total;
            $test->TM_STHW_Percentage = round($percentage);
            $test->TM_STHW_Status = 4;
            $test->save(false);
            $onlinetotal=$totalmarks;
            $onlineper=($earnedmarks / $totalmarks) * 100;
            $this->render('showhomeworkresult', array('test' => $test, 'testresult' => $result, 'pointlevels' => $pointlevels, 'id' => $id,'onlinetotal'=>$onlinetotal,'onlineper'=>$onlineper));
        else:
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
            $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
            $homework=StudentHomeworks::model()->find($criteria);
            $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
            $test->TM_STHW_Status = '4';
            $test->save(false);
            $result = '<h1 class="h125" style="text-align: center">This is a paper only test. Hence there is no automated correction.<br> Click on view solutions to check the answers. </h1>';
            $this->render('showhomeworkpaperonlyresult', array('test' => $test, 'testresult' => $result));
        endif;
    }

    public function actionHomeworksubmit($id)
    {
       
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
        $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
        $homework=StudentHomeworks::model()->find($criteria);
        $papertotal=$homework->TM_STHW_TotalMarks;
        /*$paperquestions= Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$id.' AND TM_STHWQT_Type IN (6,7) AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id='.Yii::app()->user->id));
        foreach($paperquestions AS $question):
            $master=Questions::model()->findByPk($question->TM_STHWQT_Question_Id);
            $papertotal=$papertotal+$master->TM_QN_Totalmarks;
        endforeach;*/
        $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
        $test->TM_STHW_Status = '4';
        $test->TM_STHW_TotalMarks = $papertotal;
        $test->save(false);
        $schoolhomework=Schoolhomework::model()->findByPk($homework->TM_STHW_Id);
        $notification=new Notifications();
        $notification->TM_NT_User=$schoolhomework->TM_SCH_CreatedBy;
        $notification->TM_NT_Type='HWSubmit';
        $notification->TM_NT_Item_Id=$homework->TM_STHW_HomeWork_Id;
        $notification->TM_NT_Target_Id=Yii::app()->user->id;
        $notification->TM_NT_Status='0';
        $notification->save(false);
        if($test->TM_STHW_Type=='1' || $test->TM_STHW_Type=='3'):
            Yii::app()->user->setFlash('profileMessage', UserModule::t("Homework submitted."));
            $this->redirect(array('home'));
        else:
            $this->redirect(array('Homeworkcomplete','id'=>$homework->TM_STHW_HomeWork_Id));
        endif;
    }

        public function actionHomeworkSubmitnow($id)
    {
        $hwid=$id;
        $student=Yii::app()->user->Id;
        $getcount=Studenthomeworkanswers::model()->count(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$hwid));
        if($getcount==0):
            if($_POST['submit']=='submitlater'):
                $hwanswers=New Studenthomeworkanswers();
                $hwanswers->TM_STHWAR_Student_Id=$student;
                $hwanswers->TM_STHWAR_HW_Id=$hwid;
                $target_path = "homework/";
                $filename=basename( $_FILES['Answersheet']['name']);
                if($filename!=''):
                    $target_path = $target_path.$filename ;
                    if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
                        $hwanswers->TM_STHWAR_Filename=$filename;
                    }
                    $hwanswers->save(false);
                endif;
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $hwid, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $homework->TM_STHW_Status = '3';
                $homework->save(false);
                $this->redirect(array('home'));
            endif;
            $hwanswers=New Studenthomeworkanswers();
            $hwanswers->TM_STHWAR_Student_Id=$student;
            $hwanswers->TM_STHWAR_HW_Id=$hwid;
            $target_path = "homework/";
            $filename=uniqid(). basename( $_FILES['Answersheet']['name']);
            $target_path = $target_path.$filename ;
            if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
                $hwanswers->TM_STHWAR_Filename=$filename;
            }
            if($hwanswers->save(false)):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $hwid, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $homework->TM_STHW_Status = '6';
                $homework->save(false);
                $papertotal=$homework->TM_STHW_TotalMarks;
                /*$paperquestions= Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$hwid.' AND TM_STHWQT_Type IN (6,7) AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id='.Yii::app()->user->id));
                foreach($paperquestions AS $question):
                    $master=Questions::model()->findByPk($question->TM_STHWQT_Question_Id);
                    $papertotal=$papertotal+$master->TM_QN_Totalmarks;
                endforeach;*/
                $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $test->TM_STHW_Status = '4';
                $test->TM_STHW_TotalMarks = $papertotal;
                $test->save(false);
                $schoolhomework=Schoolhomework::model()->findByPk($homework->TM_STHW_Id);
                $notification=new Notifications();
                $notification->TM_NT_User=$schoolhomework->TM_SCH_CreatedBy;
                $notification->TM_NT_Type='HWSubmit';
                $notification->TM_NT_Item_Id=$homework->TM_STHW_HomeWork_Id;
                $notification->TM_NT_Target_Id=Yii::app()->user->id;
                $notification->TM_NT_Status='0';
                $notification->save(false);
                if($test->TM_STHW_Type=='1' || $test->TM_STHW_Type=='3'):
                    Yii::app()->user->setFlash('profileMessage', UserModule::t("Homework submited."));
                    $this->redirect(array('home'));
                else:
                    $this->redirect(array('Homeworkcomplete','id'=>$homework->TM_STHW_HomeWork_Id));
                endif;
            endif;
        else:
            $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$hwid));
            $target_path = "homework/";
            $filename=uniqid(). basename( $_FILES['Answersheet']['name']);
            $target_path = $target_path.$filename ;
            if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
                $hwanswers->TM_STHWAR_Filename=$filename;
            }
            if($hwanswers->save(false)):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $hwid, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $homework->TM_STHW_Status = '6';
                $homework->save(false);
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $hwid, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                $papertotal=$homework->TM_STHW_TotalMarks;
                /*$paperquestions= Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Mock_Id='.$hwid.' AND TM_STHWQT_Type IN (6,7) AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id='.Yii::app()->user->id));
                foreach($paperquestions AS $question):
                    $master=Questions::model()->findByPk($question->TM_STHWQT_Question_Id);
                    $papertotal=$papertotal+$master->TM_QN_Totalmarks;
                endforeach;*/
                $test = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $test->TM_STHW_Status = '4';
                $test->TM_STHW_TotalMarks = $papertotal;
                $test->save(false);
                $schoolhomework=Schoolhomework::model()->findByPk($homework->TM_STHW_Id);
                $notification=new Notifications();
                $notification->TM_NT_User=$schoolhomework->TM_SCH_CreatedBy;
                $notification->TM_NT_Type='HWSubmit';
                $notification->TM_NT_Item_Id=$homework->TM_STHW_HomeWork_Id;
                $notification->TM_NT_Target_Id=Yii::app()->user->id;
                $notification->TM_NT_Status='0';
                $notification->save(false);
                if($test->TM_STHW_Type=='1' || $test->TM_STHW_Type=='3'):
                    Yii::app()->user->setFlash('profileMessage', UserModule::t("Homework submited."));
                    $this->redirect(array('home'));
                else:
                    $this->redirect(array('Homeworkcomplete','id'=>$homework->TM_STHW_HomeWork_Id));
                endif;
            endif;
        endif;
    }

    public function actionHomeworkSubmitlater($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
        $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
        $homework=StudentHomeworks::model()->find($criteria);
        $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
        $homework->TM_STHW_Status = '3';
        $homework->save(false);
        $this->redirect(array('home'));
    }

    public function GetPaperQuestionsHomework($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Type IN (6,7) AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Student_Id=:student';
        $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
        $criteria->order = 'TM_STHWQT_Order ASC';
        $selectedquestion = Studenthomeworkquestions::model()->findAll($criteria);
        return $selectedquestion;
    }

    public function actionGetpdfhomework($id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
        $criteria->params = array(':test' => $id, 'student' => Yii::app()->user->id);
        $homework=StudentHomeworks::model()->find($criteria);
        //echo $homework->TM_STHW_Id;exit;
        $studenthw = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
        $studenthw->TM_STHW_Status = '2';
        $studenthw->save(false);   
        $studenschool=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));        
        $school=School::model()->findByPK($studenschool->TM_STU_School_Id);        
        $paperquestions = Studenthomeworkquestions::model()->findAll(
            array(
                'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Type IN(6,7)',
                'params' => array(':student' => Yii::app()->user->id, ':test' => $id),
                'order'=>'TM_STHWQT_Status ASC'
            )

        );
        //echo count($paperquestions);
        $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
        $html .= "<tr><td colspan=3 align='center'><b><u>Homework<u></b></td></tr>";
        $html .= "<tr><td colspan=3 align='center'>" . date("d/m/Y") . "</td></tr>";
        foreach ($paperquestions AS $key => $paperquestion):
            $paperquestioncount = $key + 1;
            $question = Questions::model()->findByPk($paperquestion->TM_STHWQT_Question_Id);
            //print_r($question);exit;
            if ($question->TM_QN_Parent_Id == '0'):

                if ($question->TM_QN_Type_Id != '7'):
                    $displaymark = 0;
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                    foreach ($marks AS $key => $marksdisplay):
                        $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $paperquestioncount ."</b></td><td width='15%'>Marks: ".$question->TM_QN_Totalmarks."</td><td align='right' width='70%'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                        $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='15%'><b>Question " . $paperquestioncount ."</b></td><td width='15%'>Marks: ".$displaymark."</td><td align='right' width='70%'>Ref:" . $question->TM_QN_QuestionReff . "</td></tr>";
                    $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td colspan="3" align="right"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" /></td></tr>';
                    endif;

                endif;
            endif;
        endforeach;
        $html .= "</table>";
        //echo $html;exit;
        $firstname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_First_Name;
        $lastname = Student::model()->find(array('condition' => "TM_STU_User_Id='" . Yii::app()->user->Id . "'"))->TM_STU_Last_Name;
        $username = User::model()->findbyPk(Yii::app()->user->Id)->username;

        $header = '<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                <td width="33%"><span style="font-weight: lighter; color: #afafaf; ">For use by '.$school->TM_SCL_Name.' only</span></td>
                <td width="33%" align="center" style="font-weight: lighter; color: #afafaf; "></td>
                <td width="33%" style="text-align: right;font-weight: lighter;  color: #afafaf; ">' . $firstname . ' ' . $lastname . '</td></tr>
                <tr><td width="33%"><span style="font-weight: lighter; "></span></td>
                <td width="33%" align="center" style="font-weight: lighter; "></td>
                <td width="33%" style="text-align: right;font-weight: lighter; color: #afafaf; ">' . $username . '</td>
                </tr></table>';
        /*require_once 'pluginbuilder.php';
        $text = $pluginBuilder->newTextService();
        $params = ["dpi"=>"500"];
        $html = $text->filter($html, $params);*/

        $mpdf = new mPDF();

        $mpdf->SetHTMLHeader($header);
        $mpdf->SetWatermarkText($username, .1);
        $mpdf->showWatermarkText = true;

        $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
        <td width="100%" align="center"><span style=" font-weight: bolder; color: #afafaf;  " align="center">TabbieMe Ltd.</span></td>
        </tr></table>
        ');

        $name='HOMEWORK'.time().'.pdf';
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');
    }
    public function actionUploadAnswersheet()
    {
        $hwid=$_POST['homeworkid'];
        $student=Yii::app()->user->Id;
        $hwanswers=New Studenthomeworkanswers();
        $hwanswers->TM_STHWAR_Student_Id=$student;
        $hwanswers->TM_STHWAR_HW_Id=$hwid;
        $target_path = "homework/";
        $filename=basename( $_FILES['Answersheet']['name']);
        $target_path = $target_path.$filename ;
        if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
            $hwanswers->TM_STHWAR_Filename=$filename;
        }
        if($hwanswers->save(false)):
            $criteria = new CDbCriteria;
            $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
            $criteria->params = array(':test' => $hwid, 'student' => Yii::app()->user->id);
            /*$homework=StudentHomeworks::model()->find($criteria);
            $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
            $homework->TM_STHW_Status = '6';
            $homework->save(false);*/
            $this->redirect(array('home'));
        endif;        
        /*$getcount=Studenthomeworkanswers::model()->count(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$hwid));
        
        if($getcount==0):
            $hwanswers=New Studenthomeworkanswers();
            $hwanswers->TM_STHWAR_Student_Id=$student;
            $hwanswers->TM_STHWAR_HW_Id=$hwid;
            $target_path = "homework/";
            $filename=uniqid(). basename( $_FILES['Answersheet']['name']);
            $target_path = $target_path.$filename ;
            if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
                $hwanswers->TM_STHWAR_Filename=$filename;
            }
            if($hwanswers->save(false)):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $hwid, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $homework->TM_STHW_Status = '6';
                $homework->save(false);
                $this->redirect(array('home'));
            endif;
        else:
            $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$hwid));
            $target_path = "homework/";
            $filename=uniqid(). basename( $_FILES['Answersheet']['name']);
            $target_path = $target_path.$filename ;
            if(move_uploaded_file($_FILES['Answersheet']['tmp_name'], $target_path)) {
                $hwanswers->TM_STHWAR_Filename=$filename;
            }
            if($hwanswers->save(false)):
                $criteria = new CDbCriteria;
                $criteria->condition = 'TM_STHW_HomeWork_Id=:test AND TM_STHW_Student_Id=:student';
                $criteria->params = array(':test' => $hwid, 'student' => Yii::app()->user->id);
                $homework=StudentHomeworks::model()->find($criteria);
                $homework = StudentHomeworks::model()->findByPk($homework->TM_STHW_Id);
                $homework->TM_STHW_Status = '6';
                $homework->save(false);
                $this->redirect(array('home'));
            endif;
        endif;*/

    }

    //code by amal
    public function actionGethomeworkSolution()
    {
        if (isset($_POST['testid'])):
            $results = Studenthomeworkquestions::model()->findAll(
                array(
                    'condition' => 'TM_STHWQT_Student_Id=:student AND TM_STHWQT_Mock_Id=:test AND TM_STHWQT_Parent_Id=0',
                    'params' => array(':student' => Yii::app()->user->id, ':test' => $_POST['testid']),
                    'order'=>'TM_STHWQT_Order ASC'
                )
            );
            //print_r($results);exit;
            $resulthtml = '';
            foreach ($results AS $questnumberkey => $result):
                $questnumber = $questnumberkey + 1;
                $question = Questions::model()->findByPk($result->TM_STHWQT_Question_Id);

                $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                $displaymark = 0;
                foreach ($marks AS $key => $marksdisplay):
                    $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                endforeach;
                if ($question->TM_QN_Parent_Id == 0):
                    if ($question->TM_QN_Type_Id != '5' && $question->TM_QN_Type_Id != '6' && $question->TM_QN_Type_Id != '7'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span> <b>Question' . $questnumber . '</b></span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right ">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                    </div><div class="col-md-12 padnone">
                    <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                              <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>

                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '6'):
                        $resulthtml .= '<tr><td><div class="col-md-3">
                        <span> <b>Question' . $questnumber . '</b></span>
                        <span class="pull-right">Marks: ' . $question->TM_QN_Totalmarks . '</span>
                        </div>
                        <div class="col-md-3 pull-right ">
                       <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px; color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                        <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span>
                        </div><div class="col-md-12 padnone">
                        <div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div></div></div>';
                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';

                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';


                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                                  <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                               <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                               </div>
                               <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                               </div>
                               </div>
                                <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                    </div></div>

                            </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $displaymark = $displaymark + $chilquestions->TM_QN_Totalmarks;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question' . $questnumber . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';
                    elseif ($question->TM_QN_Type_Id == '5'):
                        $displaymark = 0;
                        $multiQuestions = Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$question->TM_QN_Id'"));
                        $countmulti = 1;
                        $displaymark = 0;
                        $childresulthtml = '';
                        foreach ($multiQuestions AS $key => $chilquestions):

                            $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$chilquestions->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));

                            foreach ($marks AS $key => $marksdisplay):
                                $displaymark = $displaymark + (float)$marksdisplay->TM_AR_Marks;
                            endforeach;
                            $childresulthtml .= '<div class="col-md-12 ">Part :' . $countmulti . '</div>';
                            $childresulthtml .= '<div class="col-md-12 padnone">';
                            $childresulthtml .= '<div class="col-md-' . ($chilquestions->TM_QN_Image != '' ? '10' : '12') . '">' . $chilquestions->TM_QN_Question . '</div>' . ($chilquestions->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $chilquestions->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '');
                            $childresulthtml .= '</div>';
                            $countmulti++;
                        endforeach;

                        $resulthtml .= '<tr><td><div class="col-md-3">
                    <span><b>Question' . $questnumber . '</b> </span>
                    <span class="pull-right">Marks: ' . $displaymark . '</span>
                    </div>
                    <div class="col-md-3 pull-right">
                   <a href="javascript:void(0)" class="showSolutionItem" data-reff="' . $question->TM_QN_Id . '" style="font-size:11px;color: rgb(50, 169, 255);"><u>Report Problem</u></a>
                    <span class="pull-right" style="font-size: 11px;  padding-top: 1px;">Ref :' . $question->TM_QN_QuestionReff . '</span></div>';

                        $resulthtml .= '<div class="col-md-' . ($question->TM_QN_Image != '' ? '10' : '12') . '">' . $question->TM_QN_Question . '</div>' . ($question->TM_QN_Image != '' ? '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=thumbs&type=question&width=200&height=200" alt=""></div>' : '') . '</div>' . $childresulthtml;

                        $resulthtml .= '<div class="col-md-12"><b>Ans :</b></div>';
                        $resulthtml .= '<div class="col-md-12 padnone">';
                        if ($question->TM_QN_Solutions != ''):
                            $resulthtml .= '<div class="' . ($question->TM_QN_Solution_Image != '' ? 'col-md-10' : 'col-md-12') . '">' . $question->TM_QN_Solutions . '</div>';
                            if ($question->TM_QN_Solution_Image != ''):
                                $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                            endif;
                        else:
                            $resulthtml .= '<div class="col-md-2"><img class="img-responsive img_right" src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=200&height=200" alt=""></div>';
                        endif;
                        $resulthtml .= '</div>';

                        $resulthtml .= '<div class="col-md-12 Itemtest clickable-row ">
                           <div class="sendmail suggestiontest' . $question->TM_QN_Id . '" style="display:none;">
                           <div class="col-md-10" id="maildivslide"  ><textarea  placeholder="Enter Your Comments Here" class="form-control2 comment' . $question->TM_QN_Id . '"name="comment"  id="comment' . $question->TM_QN_Id . '" hidden/>
                           </div>
                           <div class="col-md-2" style="margi-top:15px;"> <input type="button" class=" btn btn-warning pull-right"id="mailSend" value="Send"  data-reff="' . $question->TM_QN_QuestionReff . '" data-id="' . $question->TM_QN_Id . '"hidden >
                           </div>
                           </div>
                            <div class="col-md-10 mailSendComplete' . $question->TM_QN_Id . ' alert alert-success alertpadding alertpullright" id="" hidden >Your message has been sent to admin
                                </div></div>
                        </td></tr>';

                    endif;
                endif;
            endforeach;

        endif;
        echo $resulthtml;
    }
    //ends
    /**
     * school leaderboard
     */
	public function actionSchoolLeaderboard()
    {
        if (isset($_GET['startdate'])):
            if ($_GET['mode'] == 'month'):
                $dates = $this->GetMonth($_GET['startdate']);
            else:
                $dates = $this->GetWeek($_GET['startdate']);
            endif;
        else:
            $dates = $this->GetWeek(date('Y-m-d'));
        endif;
        $mode = (isset($_GET['mode']) != '' ? $_GET['mode'] : 'total');
        $school=User::model()->findByPK(Yii::app()->user->id)->school_id;
        if($mode=='total'):        
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints,TM_STP_Student_Id,TM_STP_Date');
            $criteria->join ='INNER JOIN tm_users AS u ON u.id=t.TM_STP_Student_Id ';
            $criteria->condition = "TM_STP_Standard_Id=:standard AND u.school_id=".$school;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->limit = '5';            
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $studenttotal = StudentPoints::model()->findAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->join ='INNER JOIN tm_users AS u ON u.id=t.TM_STP_Student_Id ';
            $criteria->condition = "TM_STP_Standard_Id=:standard AND u.school_id=".$school." AND TM_STP_Student_Id=" . Yii::app()->user->id;                        
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $usertotal = StudentPoints::model()->find($criteria);
        else:        
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints,TM_STP_Student_Id,TM_STP_Date');
            $criteria->join ='INNER JOIN tm_users AS u ON u.id=t.TM_STP_Student_Id ';            
            $criteria->condition = "TM_STP_Standard_Id=:standard AND u.school_id=".$school." AND TM_STP_Date BETWEEN '" . $dates[0] . "' AND '" . $dates[1] . "'";
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->limit = '5';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $studenttotal = StudentPoints::model()->findAll($criteria);
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->join ='INNER JOIN tm_users AS u ON u.id=t.TM_STP_Student_Id ';
            $criteria->condition = "TM_STP_Standard_Id=:standard AND u.school_id=".$school." AND  TM_STP_Date BETWEEN '" . $dates[0] . "' AND '" . $dates[1] . "' AND TM_STP_Student_Id=" . Yii::app()->user->id;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $usertotal = StudentPoints::model()->find($criteria);        
        endif;
        $this->render('leaderboard', array('studenttotal' => $studenttotal, 'usertotal' => $usertotal, 'dates' => $dates, 'mode' => $mode,'school'=>true));
    }
        
     /**
      * school leaderboard ends
      */
      /**
       * school module progress report
       */
       public function actionSchoolprogressreport()
       {
            $week=$this->GetWeek(date('Y-m-d'));
            $plan = $this->GetStudentDet();
            $criteria = new CDbCriteria;
            $criteria->addCondition('TM_STHW_Student_Id=' . Yii::app()->user->id . ' AND TM_STHW_Status=5 AND TM_STHW_Plan_Id=' . Yii::app()->session['plan']);
            $mocktotal = StudentHomeworks::model()->with('homework')->count($criteria);             
            $criteria->addCondition("TM_STHW_Date BETWEEN '".$week[0]."' AND '".$week[1]."'");
            $mockweekstatus = StudentHomeworks::model()->with('homework')->count($criteria);            
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Test_Type='Homework' AND TM_STP_Standard_Id=:standard AND TM_STP_Student_Id=" . Yii::app()->user->id;
            $criteria->order = 'totalpoints DESC';
            $criteria->group = 'TM_STP_Student_Id';
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $totalpoints = StudentPoints::model()->find($criteria); 
			
            $criteria = new CDbCriteria;
            $criteria->select = array('SUM(TM_STP_Points) AS totalpoints');
            $criteria->condition = "TM_STP_Test_Type='Homework' AND TM_STP_Standard_Id=:standard AND TM_STP_Date BETWEEN '" . $week[0]. "' AND '" . $week[1] . "' AND TM_STP_Student_Id=" . Yii::app()->user->id;
            $criteria->order = 'totalpoints DESC';            
            $criteria->params = array(':standard' => Yii::app()->session['standard']);
            $weeklypoints = StudentPoints::model()->find($criteria); 
            $lastvisit=User::model()->findByPk(Yii::app()->user->id)->lastvisit;
            if(!$lastvisit):
                $lastlogin='Never logged in';
            else:
                $lastlogin=date("jS F Y ",$lastvisit);
            endif;
           $this->render('schoolprogressreport',array(
        'mocktotal'=>$mocktotal,
        'mockweekstatus'=>$mockweekstatus,
        'lastlogin'=>$lastlogin,
        'weeklypoints'=>$weeklypoints, 
        'totalpoints'=>$totalpoints                             
        ));                             
                                             
       }
      /**
       * school module progress report ends
       */ 
       /**
        * school chapter status
        */      
        public function actionGetschoolchapterstatus()
        {
            $plan = $this->GetStudentDet();
            //$chapters = Chapter::model()->findAll(array('condition' => "TM_TP_Syllabus_Ida='$plan->TM_SPN_SyllabusId' AND TM_TP_Standard_Id='$plan->TM_SPN_StandardId'",'order'=>'TM_TP_order ASC'));    
            $connection = CActiveRecord::getDbConnection();                       
            $sql="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".Yii::app()->user->id."' AND TM_STHWQT_Parent_Id=0";                       
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $attempted=$dataReader->readAll();
            $attemptedquestions='';
            foreach($attempted AS $key=>$attemp):
            $attemptedquestions.=($key==0?'':',').$attemp['question'];
            endforeach;         
            $sql1="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".Yii::app()->user->id."' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Marks!=0";        
            $command=$connection->createCommand($sql1);
            $dataReader=$command->query();
            $correctones=$dataReader->readAll();
            $correctquestions='';
            foreach($correctones AS $key=>$correct):
            $correctquestions.=($key==0?'':',').$correct['question'];
            endforeach; 
            $sql="SELECT count(a.TM_QN_Id) AS attemptedcount, a.TM_QN_Topic_Id,b.TM_TP_Name,
            (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Topic_Id=a.TM_QN_Topic_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$correctquestions." )) AS correctcount
                    FROM 
                    tm_question AS a,tm_topic AS b WHERE 
                    a.TM_QN_Parent_Id=0 AND a.TM_QN_Topic_Id=b.TM_TP_Id AND b.TM_TP_Syllabus_Id='".$plan->TM_SPN_SyllabusId."' AND b.TM_TP_Standard_Id='".$plan->TM_SPN_StandardId."' AND TM_QN_Id IN (".$attemptedquestions.") group by TM_QN_Topic_Id";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $chapters=$dataReader->readAll();
            $this->renderPartial('_overallstatus', array('chapters' => $chapters, 'plan' => $plan,'correctquestions'=>$correctquestions,'attemptedquestions'=>$attemptedquestions)); 
        }        
        /**
         * school chapter status ends
         */
         /**
          * get topic status
          */
        public function actionGetschooltopicstatus()
        {
            $topicstatus=$this->Getschooltopicstatus(Yii::app()->user->id);
            $this->renderPartial('_topicstatus', array('topicstatus' => $topicstatus));         
        }          
        public function Getschooltopicstatus($user)
        {
            $plan = $this->GetStudentDet(); 
            $connection = CActiveRecord::getDbConnection();
            $sql1="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".Yii::app()->user->id."' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Marks!=0";        
            $command=$connection->createCommand($sql1);
            $dataReader=$command->query();
            $correctones=$dataReader->readAll();        
            $correctquestions='';
            foreach($correctones AS $key=>$correct):
            $correctquestions.=($key==0?'':',').$correct['question'];
            endforeach; 
            $sql="SELECT TM_STHWQT_Question_Id AS question FROM tm_studenthomeworkquestions WHERE TM_STHWQT_Student_Id='".Yii::app()->user->id."' AND TM_STHWQT_Parent_Id=0";        
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $attempted=$dataReader->readAll();
            $attemptedquestions='';
            foreach($attempted AS $key=>$attemp):
            $attemptedquestions.=($key==0?'':',').$attemp['question'];
            endforeach;        
             $sql="SELECT count(DISTINCT(a.TM_QN_Id)) AS attempted, b.TM_SN_Id,b.TM_SN_Name,b.TM_SN_Topic_Id,
            (SELECT COUNT(DISTINCT(TM_QN_Id)) AS question FROM tm_question AS sub WHERE sub.TM_QN_Section_Id=a.TM_QN_Section_Id AND sub.TM_QN_Parent_Id=0 AND sub.TM_QN_Id IN ( ".$attemptedquestions." )) AS completed,
                    (
                        SELECT COUNT(DISTINCT(TM_QN_Id)) AS question 
                		FROM tm_question AS quest 
                		WHERE quest.TM_QN_Section_Id=b.TM_SN_Id 
                		AND quest.TM_QN_Parent_Id=0 
                    ) AS total        
                    FROM 
                    tm_question AS a,tm_section AS b WHERE 
                    a.TM_QN_Parent_Id=0 AND a.TM_QN_Section_Id=b.TM_SN_Id AND b.TM_SN_Syllabus_Id='".$plan->TM_SPN_SyllabusId."' AND b.TM_SN_Standard_Id='".$plan->TM_SPN_StandardId."' AND TM_QN_Id IN (".$correctquestions.") GROUP BY TM_QN_Section_Id HAVING completed > 0 AND attempted < completed ORDER BY attempted DESC,TM_SN_Id ASC LIMIT 5 ";
                                                                  
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $topicstatus=$dataReader->readAll();
            //print_r($topicstatus); exit;                        
            return $topicstatus;                     
        }          
      /**
       * get topic status
       */
        public function actionGetschoolreststatus()
        {
            $progressstatus=$this->GetCompleteStatus(Yii::app()->user->id);
            echo json_encode($progressstatus);        
        }        
       
/** Teacher marking action **/
    public function actionMarkhomework($id)
    {
        $this->layout = '//layouts/teachermarkig';
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));
        $notstartedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id.' AND TM_STHW_Status NOT IN (4,5)','order'=>'TM_STHW_Status ASC,student.TM_STU_First_Name ASC'));                        
        $homework=Schoolhomework::model()->findByPk($id);
        $this->render('teachermarking',array(
            'startedstudents'=>$startedstudents,
            'homework'=>$homework,
            'notstartedstudents'=>$notstartedstudents
        )); 
          
    }

    //code by amal
    public function actionSolution()
    {
        $student=$_POST['student'];
        $homework=$_POST['homework'];
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $solution='';
        foreach($hwquestions AS $hwq):
            $question=Questions::model()->find(array('condition'=>'TM_QN_Id='.$hwq->TM_STHWQT_Question_Id));
            $solution.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4>$question->TM_QN_Solutions</li>";
        endforeach;
        echo $solution;
    }

    public function actionAnswer()
    {
        $student=$_POST['student'];
        $homework=$_POST['homework'];
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $answer='';
        foreach($hwquestions AS $hwq):
            $question=Questions::model()->find(array('condition'=>'TM_QN_Id='.$hwq->TM_STHWQT_Question_Id));
            if($hwq['TM_STHWQT_Type']==4):
                $studanswer=$hwq->TM_STHWQT_Answer;
                $correctans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$hwq->TM_STHWQT_Question_Id.''));
                $correctanswer=$correctans->TM_AR_Answer;
            else:
                $studentanswer=Answers::model()->findByPk($hwq->TM_STHWQT_Answer);
                $studanswer=$studentanswer->TM_AR_Answer;
                $correctans=Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$hwq->TM_STHWQT_Question_Id.' AND TM_AR_Correct=1'));
                $correctanswer=$correctans->TM_AR_Answer;
            endif;
            if($hwq['TM_STHWQT_Type']==6 || $hwq['TM_STHWQT_Type']==7):
                $answer.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4></li>";
            else:
                $answer.="<li><h4 class='media-heading'>$question->TM_QN_Question</h4>$studanswer<p>Correct</p>$correctanswer</li>";
            endif;
        endforeach;
        echo $answer;
    }
    //ends
    public function actionSavemarks()
    {
        $student=$_POST["student"];
        $homework=$_POST["homework"];
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));        
        foreach($hwquestions AS $hwq):
            if($hwq->TM_STHWQT_Marks!=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id]):
                $hwq->TM_STHWQT_Marks=$_POST['setmark'.$student.$hwq->TM_STHWQT_Question_Id];
                $hwq->save(false);
            endif;
        endforeach;        
        echo "saved";
    }
    public function actionMarkcomplete()
    {
        $studentvalue=$_POST["student"];
        $homeworkvalue=$_POST["homework"];
        $homework=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$homeworkvalue.' AND TM_STHW_Student_Id='.$studentvalue));
        $homework->TM_STHW_Status='5';
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$studentvalue.' AND TM_STHWQT_Mock_Id='.$homeworkvalue,'order'=>'TM_STHWQT_Order ASC'));
		$totalmarks=0;
        foreach($hwquestions AS $hwq):
            if($hwq->TM_STHWQT_Marks!=$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id]):
                $hwq->TM_STHWQT_Marks=$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id];
                $hwq->save(false);
            endif;
			$totalmarks=$totalmarks+$_POST['setmark'.$studentvalue.$hwq->TM_STHWQT_Question_Id];
        endforeach;
		$homework->TM_STHW_Marks=$totalmarks;
		$percentage = ($totalmarks / $homework->TM_STHW_TotalMarks) * 100;
		$homework->TM_STHW_Percentage=$percentage;		
		$homework->save(false);
        echo "saved";
    }
    public function actionSaveStatus($id)
    {
        $model=Schoolhomework::model()->findByPk($id);
        if($model->TM_SCH_Status==0):
            $model->TM_SCH_Status=1;
        else:
            $model->TM_SCH_Status=0;
        endif;
        $model->save(false);
        $this->redirect(array('home'));

    }
    public function actionCompleteallmarking($id)
    {        
        $homework=Schoolhomework::model()->findByPk($id);
        $startedstudents=StudentHomeworks::model()->findAll(array('join'=>'INNER  JOIN tm_student AS student ON student.TM_STU_User_Id=t.TM_STHW_Student_Id','condition'=>'TM_STHW_HomeWork_Id='.$id));
        foreach($startedstudents AS $started):
            $started->TM_STHW_Status='5';
            $started->save(false);             
            $pointlevels = $this->AddPoints($started->TM_STHW_Marks, $id, 'Homework', $started->TM_STHW_Student_Id);       
        endforeach;
        $homework->TM_SCH_Status='2';
        $homework->save(false);
        $this->redirect(array('home'));
    }
    public function actionSendreminder()
    {
        $notification=new Notifications();
        $notification->TM_NT_User=$_POST['student'];
        $notification->TM_NT_Type='HWReminder';
        $notification->TM_NT_Item_Id=$_POST['homework'];
        $notification->TM_NT_Target_Id=Yii::app()->user->id;
        $notification->TM_NT_Status='0';
        $notification->save(false);        
        echo "yes";
    }
    public function GetQuestionColumnsCompleted($homework,$student)
    {
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $returnraw='';
        $count=1;
        foreach($hwquestions AS $hwq):
            $totalmarks=$this->Getmarks($hwq->TM_STHWQT_Question_Id);
            $checkclass='';
            /*$returnraw.='<td><figure class="thumbnail completed">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
                            <figcaption class="text-center">'.$hwq->TM_STHWQT_Marks.'</figcaption>                           																														
						</figure>
					</td>';*/
            if($hwq->TM_STHWQT_Type!='5'):
                $returnraw.='<td><figure class="thumbnail completed">
                                <figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
                                <figcaption class="text-center">'.$hwq->TM_STHWQT_Marks.'</figcaption>
                            </figure>
                        </td>';
            else:
                $childquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Parent_Id='.$hwq->TM_STHWQT_Question_Id.' '));
                $childreturnraw='';
                foreach($childquestions AS $key=>$childquestion):
                    $childreturnraw.='<td><figure class="thumbnail completed">
                                <figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'.'.($key + 1).'</figcaption>
                                <figcaption class="text-center">'.$childquestion->TM_STHWQT_Marks.'</figcaption>
                            </figure>
                        </td>';
                endforeach;
                $returnraw.=$childreturnraw;
            endif;
            $count++;          
        endforeach;
        return $returnraw;
    }
    public function GetQuestionColumns($homework,$student)
    {
        $hwquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Parent_Id=0 AND TM_STHWQT_Mock_Id='.$homework,'order'=>'TM_STHWQT_Order ASC'));
        $returnraw='';
        $count=1;
        foreach($hwquestions AS $hwq):
            $totalmarks=$this->Getmarks($hwq->TM_STHWQT_Question_Id);
            $checkclass='';
            if($hwq->TM_STHWQT_Type=='6' || $hwq->TM_STHWQT_Type=='7'):
                $checkclass='myred';
            elseif($hwq->TM_STHWQT_Marks==$totalmarks):
                $checkclass='mygreen';
            elseif(($hwq->TM_STHWQT_Type!='6' || $hwq->TM_STHWQT_Type!='7') & $hwq->TM_STHWQT_Marks!=$totalmarks):
                $checkclass='myyellow';
            endif;
            /*$returnraw.='<td><figure class="thumbnail '.$checkclass.'">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
							<span class="badge mybdg">'.$totalmarks.'</span>
                            <input type="hidden" id="totmarks'.$hwq->TM_STHWQT_Question_Id.'" value="'.$totalmarks.'">
                            <input type="hidden" class="error'.$student.'" id="error'.$student.$hwq->TM_STHWQT_Question_Id.'" value="0">
							<input class="form-control setmark'.$student.' mark" data-id="'.$hwq->TM_STHWQT_Question_Id.'" data-student="'.$student.'" id="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" name="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" value="'.$hwq->TM_STHWQT_Marks.'" type="text">
						</figure>
					</td>';*/
            if($hwq->TM_STHWQT_Type!='5'):
                $returnraw.='<td><figure class="thumbnail '.$checkclass.'">
							<figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'</figcaption>
							<span class="badge mybdg">'.$totalmarks.'</span>
                            <input type="hidden" id="totmarks'.$hwq->TM_STHWQT_Question_Id.'" value="'.$totalmarks.'">
                            <input type="hidden" class="error'.$student.'" id="error'.$student.$hwq->TM_STHWQT_Question_Id.'" value="0">
							<input class="form-control setmark'.$student.' mark" data-id="'.$hwq->TM_STHWQT_Question_Id.'" data-student="'.$student.'" id="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" name="setmark'.$student.$hwq->TM_STHWQT_Question_Id.'" value="'.$hwq->TM_STHWQT_Marks.'" type="text">
						</figure>
						<span id="markinfo'.$hwq->TM_STHWQT_Question_Id.'" style="color:red;display: none;"></span>
					</td>';
            else:
                $childquestions=Studenthomeworkquestions::model()->findAll(array('condition'=>'TM_STHWQT_Student_Id='.$student.' AND TM_STHWQT_Mock_Id='.$homework.' AND TM_STHWQT_Parent_Id='.$hwq->TM_STHWQT_Question_Id.' '));
                $childreturnraw='';
                foreach($childquestions AS $key=>$childquestion):
                    $questionType = Questions::model()->findByPk($childquestion->TM_STHWQT_Question_Id)->TM_QN_Type_Id;
                    $childtotalmarks=$this->ChildGetmarks($childquestion->TM_STHWQT_Question_Id);
                    
                    // code by vivek - multipart text entry questions 
                    if($questionType == 4)
                    {
                        $answers = Answers::model()->find(array('condition'=>'TM_AR_Question_Id='.$childquestion->TM_STHWQT_Question_Id));
                        // var_dump($answers);exit;
                        $answeroption = explode(';', strtolower(strip_tags($answers->TM_AR_Answer)));
                        $useranswer = trim(strtolower($childquestion->TM_STHWQT_Answer));
                        $marked = 0;
                        $marks = 0;
                        for ($i = 0; $i < count($answeroption); $i++) {
                            $option = trim($answeroption[$i]);

                            //fix for < > issue
                            if($option=='&gt'):
                                $newoption='&gt;';
                            elseif($option=='&lt'):
                                $newoption='&lt;';
                            else:
                                $newoption=$option;
                            endif;
                            if (strcmp($useranswer, htmlspecialchars_decode($newoption)) == 0 && $marked ==0)
                            {
                                $marks = $answers->TM_AR_Marks;
                                $marked = 1;
                            }
                        }
                        $childmarks = $marks; 
                    }
                    else
                    {

                        $answer=Answers::model()->findByPk($childquestion->TM_STHWQT_Answer);
                        if($answer->TM_AR_Correct==1):
                            $marks=$answer->TM_AR_Marks;
                        else:
                            $marks=0;
                        endif;
                        if($childquestion->TM_STHWQT_Marks!=0):
                            $childmarks=$childquestion->TM_STHWQT_Marks;
                        else:
                            $childmarks=$marks;
                        endif;
                    }
                    if($childmarks==$childtotalmarks):
                        $checkclass='mygreen';
                    else:
                        $checkclass='myyellow';
                    endif;
                    $childreturnraw.='<td><figure class="thumbnail '.$checkclass.'">
                        <figcaption class="text-center">Qn '.$hwq->TM_STHWQT_Order.'.'.($key + 1).'</figcaption>
                        <span class="badge mybdg">'.$childtotalmarks.'</span>
                        <input type="hidden" id="totmarks'.$childquestion->TM_STHWQT_Question_Id.'" value="'.$childtotalmarks.'">
                        <input type="hidden" class="error'.$student.'" id="error'.$student.$childquestion->TM_STHWQT_Question_Id.'" value="0">
                        <input class="form-control setmark'.$student.' mark" data-id="'.$childquestion->TM_STHWQT_Question_Id.'" data-student="'.$student.'" id="setmark'.$student.$childquestion->TM_STHWQT_Question_Id.'" name="setmark'.$student.$childquestion->TM_STHWQT_Question_Id.'" value="'.$childmarks.'" type="text">
                    </figure>
                    <span id="markinfo'.$childquestion->TM_STHWQT_Question_Id.'" style="color:red;display: none;"></span>
                </td>';
                endforeach;
                $returnraw.=$childreturnraw;
            endif;
            $count++;          
        endforeach;
        return $returnraw;
        
    }

    public function ChildGetmarks($questionid)
    {
        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$questionid' AND TM_AR_Marks NOT IN(0)"));
        $total = 0;
        foreach ($marks AS $key => $marksdisplay):
            $total = $total + (float)$marksdisplay->TM_AR_Marks;
        endforeach;
        return $total;
    }
    public function Getmarks($questionid)
    {
            $question = Questions::model()->findByPk($questionid);
            if ($question->TM_QN_Parent_Id == '0'):
                if($question->TM_QN_Type_Id!='5' && $question->TM_QN_Type_Id!='6' && $question->TM_QN_Type_Id!='7'):
                    $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$question->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                    $total = 0;
                    foreach ($marks AS $key => $marksdisplay):
                        $total = $total + (float)$marksdisplay->TM_AR_Marks;
                    endforeach;
                elseif($question->TM_QN_Type_Id=='5'):
                    $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                    $total=0;                    
                    foreach ($childquestions AS $childquestion):
                        $marks = Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childquestion->TM_QN_Id'AND TM_AR_Marks NOT IN(0)"));
                        foreach ($marks AS $key => $marksdisplay):
                            $total = $total + (float)$marksdisplay->TM_AR_Marks;
                        endforeach;
                    endforeach;                    
                elseif($question->TM_QN_Type_Id=='6'):
                    $total=$question->TM_QN_Totalmarks;
                elseif($question->TM_QN_Type_Id=='7'):
                    $childquestions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'"));
                    $total=0;                    
                    foreach ($childquestions AS $childquestion):
                        $total = $total + (float)$childquestion->TM_QN_Totalmarks;
                    endforeach;                                        
                endif;                
            endif;
            return $total;        
    }
    /** Teacher marking action ends **/   
        //code by amal
    public function actionPrintworksheet($id)
    {
        $schoolhomework=Schoolhomework::model()->findByPk($id);
        if($schoolhomework->TM_SCH_Worksheet==''):
            $homeworkqstns=HomeworkQuestions::model()->findAll(array(
                'condition' => 'TM_HQ_Homework_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_HQ_Order ASC'
            ));
            $username=User::model()->findByPk(Yii::app()->user->id)->username;
            $hwemoorkdata=Schoolhomework::model()->findByPK($id);
            $homework=$hwemoorkdata->TM_SCH_Name;
            $showoptionmaster=$hwemoorkdata->TM_SCH_PublishType;

            $html = "<style>
            p{
            margin-top: 0;
            margin-bottom: 0;
            }
            </style>
            <table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";
            //$html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>";
            $html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";

            /*$student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));
            $studentname=$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name;
            $schoolname=School::model()->findByPK($schoolhomework->TM_SCH_School_Id)->TM_SCL_Name;
            $html="<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
            $html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>";
            $html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";*/
            $totalquestions=count($homeworkqstns);
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            $totalmarks=0;
            foreach ($homeworkqstns AS $key => $homeworkqstn):

                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_HQ_Question_Id);
                if($showoptionmaster==3):
                    if($question->TM_QN_Show_Option==1):
                        $showoption=true;
                    else:
                        $showoption=false;
                    endif;
                else:
                    $showoption=true;
                endif;
                //code by amal
                if($homeworkqstn->TM_HQ_Type!=5):
                    if($homeworkqstn->TM_HQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_HQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_HQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_HQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_HQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_HQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                if ($question->TM_QN_Type_Id!='7'):
                    $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount ."</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>( " . $marks . " )</td></tr>";
                    //$html .= "<tr><td colspan='3' >".$question->TM_QN_Question.$question->TM_QN_Show_Option."</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                    endif;
                    if($homeworkqstn->TM_HQ_Type==6):
                        $showoption=false;
                    endif;
                    if($showoption):
                        foreach($question->answers AS $key=>$answer):
                            if($answer->TM_AR_Image!=''):
                                $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                            else:
                                $ansoptimg='';
                            endif;
                            $ans=$answer->TM_AR_Answer;
                            $find='<p>';
                            $pos = strpos($ans, $find);
                            if ($pos === false) {
                                $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")".$answer->TM_AR_Answer."</p> $ansoptimg</td><td></td></tr>";
                            } else {
                                $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p style="margin:0;">'.$alphabets[$key].') ', 0,3)."$ansoptimg</td><td></td></tr>";
                            }
                        endforeach;
                    endif;
                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html .= "<tr><td width='6%'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td></td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html .= '<tr><td width="6%"></td><td align="left" style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td></td></tr>';
                            endif;
                            if($showoption):
                                foreach($childquestion->answers AS $key=>$answer):
                                    if($answer->TM_AR_Image!=''):
                                        $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                    else:
                                        $ansoptimg='';
                                    endif;
                                    $ans=$answer->TM_AR_Answer;
                                    $find='<p>';
                                    $pos = strpos($ans, $find);
                                    if ($pos === false)
                                    {
                                        $html.="<tr><td></td><td style='padding:0;padding-top:5px;'><p style='vertical-align: top;float:left;margin:0;'>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
                                            <td align='right'>$ansoptimg</td></tr>";
                                    }
                                    else
                                    {
                                        $html.="<tr><td width='6%'></td><td style='padding:0;padding-top:5px;'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."$ansoptimg</td><td></td></tr>";

                                    }
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><br></td></tr>';
                        //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml .="<tr><td width='6%' style='padding:0;'></td><td align='left' style='padding:0;'>Part." . $countpart."</td>td align='right' width='6%' style='padding:0;'></td></tr>";
                        $childhtml .="<tr><td width='6%' style='padding:0;'></td><td  style='padding:0;'>".$childquestion->TM_QN_Question."</td><td align='right' width='6%' style='padding:0;'></td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml .= '<tr><td align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $countpart++;
                    endforeach;
                    $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount ."</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>( " . $displaymark . " )</td></tr>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<tr><td width="6%"></td><td align="left" style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td></td></tr>';
                    endif;
                    $html .= $childhtml;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        $html .= '<tr ><td colspan="3"><br></td></tr>';
                        //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                    endif;
                endif;
                $totalmarks=$totalmarks+$marks;
            endforeach;
            $html .= "</table>";
            $schoolhw=Schoolhomework::model()->findByPK($id);
            $school=School::model()->findByPK($schoolhw->TM_SCH_School_Id);
            $standradname=Standard::model()->findByPK($schoolhw->TM_SCH_Standard_Id)->TM_SD_Name;
            $total=$totalmarks;
            $header = '<div>
            <table cellpadding="5" border="0" style="margin-bottom:10px;font-size:14px;font-family: STIXGeneral;" width="100%">
                <tr><td colspan=4 align="center" style="padding:0;"><b>'.$school->TM_SCL_Name.'</b></td></tr>
                <tr><td colspan=4 align="center" style="padding:0;"><b>'.$homework.'</b></td></tr>
                <tr>
                    <td width="20%" style="padding:0">Student Name :</td>
                    <td width="50%" style="padding:0"> </td>
                    <td width="20%" style="padding:0">Date :</td>
                    <td width="10%" style="padding:0"> '.date('d/m/Y').'</td>
                </tr>
                <tr>
                    <td width="20%" style="padding:0">Standard :</td>
                    <td width="50%" style="padding:0">  '.$standard.'</td>
                    <td width="20%" style="padding:0">Division : </td>
                    <td width="10%" style="padding:0"> </td>
                </tr>
                <tr>
                    <td width="20%" style="padding:0">Total Marks :</td>
                    <td width="50%" style="padding:0"> '.$total.' </td>
                    <td width="20%" style="padding:0">Marks Scored :</td>
                    <td width="10%" style="padding:0"> </td>
                </tr>
                <tr>
                    <td width="100%" colspan=4 >Instructions for Student : '.$schoolhw->TM_SCH_Comments.'</td>
                </tr>
            </table></div>';
            $html=$header.$html;

             $this->render('printpdf',array('html'=>$html));


            /*$mpdf = new mPDF();
            //$stylesheet = file_get_contents('http://hosting.solminds.com/dev/schoolstaging/css/pdf.css');
            //$mpdf->SetHTMLHeader($header);
            $mpdf->SetWatermarkText($username, .1);
            $mpdf->showWatermarkText = false;

            $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
            	<tr>
            		<td width="100%" colspan="2">
            			&copy; Copyright @ TabbieMe Ltd. All rights reserved .
            		</td>
            	</tr>
            	<tr>
            		<td width="80%" >
            			www.tabbiemath.com  - The one stop shop for Maths Revision.
            		</td>
                    <td align="right" >Page #{PAGENO}</td>
            	</tr>
            </table>');

            //echo $html;exitt;
            $name='WORKSHEET'.time().'.pdf';
            //$mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML($header.$html,2);
            $mpdf->Output($name, 'I');*/
        else:
            $this->redirect(Yii::app()->request->baseUrl."/worksheets/".$schoolhomework->TM_SCH_Worksheet);
        endif;

    }
    ////ends
    public function HasAnswersheets($homework)
    {
        $count=Studenthomeworkanswers::model()->count(array('condition'=>'TM_STHWAR_HW_Id='.$homework.' AND TM_STHWAR_Student_Id='.Yii::app()->user->id));
        return $count;
    }
    public function actiondeleteWorksheet()
    {
        $homeworks=Studenthomeworkanswers::model()->findByPk($_POST['worksheet'])->delete();
        echo "yes";
                
    }
    public function actionGetWorksheets()
    {
        $homeworks=Studenthomeworkanswers::model()->findAll(array('condition'=>'TM_STHWAR_HW_Id='.$_POST['homework'].' AND TM_STHWAR_Student_Id='.Yii::app()->user->id));
        $html='';
        foreach($homeworks AS $homework):
            $html.='<div class="well" id="file'.$homework->TM_STHWAR_Id.'"><a href="'.Yii::app()->request->baseUrl.'/homework/'.$homework->TM_STHWAR_Filename.'" target="_blank"><span class="glyphicon glyphicon-file"></span>'.$homework->TM_STHWAR_Filename.'</a> <a class="deleteworksheet" data-value="'.$homework->TM_STHWAR_Id.'" title="delete" style="float:right"><span class="glyphicon glyphicon-trash"></span></a></div>';
        endforeach; 
        if($html==''):
            $html='<div class="well">No files found</div> ';
        endif;       
        echo $html;
    } 
    public function CheckAnswersheetCount($student,$homework)
    {
        $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        if(count($hwanswers)>0 ||$hwanswers->TM_STHWAR_Filename!=''):
            return true;            
        else:
            return false;            
        endif;        
    }
    public function GetAnswersheetCount($student,$homework)
    {
        $hwanswers=Studenthomeworkanswers::model()->findAll(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        return $hwanswers;
    }    
    public function CheckAnswersheet($student,$homework)
    {
      $hwanswers=Studenthomeworkanswers::model()->find(array('condition'=>'TM_STHWAR_Student_Id='.$student.' AND TM_STHWAR_HW_Id='.$homework));
        if(count($hwanswers)>0 ||$hwanswers->TM_STHWAR_Filename!=''):
            return Yii::app()->request->baseUrl.'/homework/'.$hwanswers->TM_STHWAR_Filename;
        else:
            return false;            
        endif;        
    }

    public function CheckCommentCount($student,$homework)
    {
        $hwanswers=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_Student_Id='.$student.' AND TM_STHW_HomeWork_Id='.$homework));
        if(count($hwanswers)>0 &&$hwanswers->TM_STHW_Comments_File!=''):
            return true;
        else:
            return false;
        endif;
    }
    public function GetCommentCount($student,$homework)
    {
        $hwanswers=StudentHomeworks::model()->findAll(array('condition'=>'TM_STHW_Student_Id='.$student.' AND TM_STHW_HomeWork_Id='.$homework));
        return $hwanswers;
    }

    public function actionDownload()
    {
        if(isset($_REQUEST["file"])){
            // Get parameters
            $file = urldecode($_REQUEST["file"]); // Decode URL-encoded string
            $filepath = "homework/" . $file;

            // Process download
            if(file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                exit;
            }
        }
    }

	public function actionStudentsummaryreport()
    {
        $this->layout = '//layouts/admincolumn2';
        if(isset($_GET['search']))
        {
            $formvals=array();
            $connection = CActiveRecord::getDbConnection();
            $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,b.username AS student,b.id,c.username AS parent,c.email,e.TM_SPN_Id,f.TM_PN_Name,
                 (SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS g WHERE g.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND g.TM_STU_TT_Plan=e.TM_SPN_PlanId ) AS totalrevision,
                 (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS h WHERE h.TM_STMK_Student_Id=a.TM_STU_User_Id AND h.TM_STMK_Plan_Id=e.TM_SPN_PlanId ) AS totalmocks,
                 (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chr LEFT JOIN tm_chalanges AS ch ON chr.TM_CE_RE_Chalange_Id=ch.TM_CL_Id WHERE ch.TM_CL_Plan=e.TM_SPN_PlanId AND chr.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id) AS totalchallenge,
                 (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS w WHERE w.TM_STHW_Student_Id=a.TM_STU_User_Id AND w.TM_STHW_Plan_Id=e.TM_SPN_PlanId ) AS totalassesments,
                 (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId) AS totalpoints ";
            if($_GET['date']=='before'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn < '".$_GET['fromdate']."' ) AS periodrevision,
                       (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date < '".$_GET['fromdate']."'  ) AS periodmocks,
                       (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date < '".$_GET['fromdate']."') AS periodchallenge,
                       (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                       (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date < '".$_GET['fromdate']."' ) AS periodpoints
                       ";
                endif;
            elseif($_GET['date']=='after'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn > '".$_GET['fromdate']."' ) AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date > '".$_GET['fromdate']."'  ) AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date > '".$_GET['fromdate']."') AS periodchallenge,
                           (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date > '".$_GET['fromdate']."' ) AS periodpoints  ";
                endif;
            else:
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!='' && $_GET['todate']):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $formvals['todate']=$_GET['todate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodchallenge,
                           (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ) AS periodpoints ";

                endif;
            endif;
            $sql.="FROM tm_student AS a
                  LEFT JOIN tm_users AS b ON b.id=a.TM_STU_User_Id
                  LEFT JOIN tm_users AS c ON c.id=a.TM_STU_Parent_Id
                  INNER JOIN tm_student_plan AS e ON a.TM_STU_User_Id=e.TM_SPN_StudentId
                  INNER JOIN tm_plan AS f ON e.TM_SPN_PlanId=f.TM_PN_Id
                  WHERE a.TM_STU_Status IN(0,1,2)
                  ";
            if($_GET['school']!=''):
                if($_GET['school']=='0'):
                    $formvals['school']=$_GET['school'];
                    $formvals['schooltext']=$_GET['schooltext'];
                    $sql.=" AND a.TM_STU_SchoolCustomname LIKE '%".$_GET['schooltext']."%' ";
                else:
                    $formvals['school']=$_GET['school'];
                    $sql.=" AND a.TM_STU_School='".$_GET['school']."'";
                endif;
            endif;

            if($_GET['plan']!=''):
                $formvals['plan']=$_GET['plan'];
                $sql.=" AND e.TM_SPN_PlanId =".$_GET['plan'];
            endif;
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            $total_pages = count($dataval);

            $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,a.TM_STU_SchoolCustomname,a.TM_STU_School_Id,b.username AS student,b.id,c.username AS parent,c.email,e.TM_SPN_Id,f.TM_PN_Name,
                 (SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS g WHERE g.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND g.TM_STU_TT_Plan=e.TM_SPN_PlanId ) AS totalrevision,
                 (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS h WHERE h.TM_STMK_Student_Id=a.TM_STU_User_Id AND h.TM_STMK_Plan_Id=e.TM_SPN_PlanId ) AS totalmocks,
                 (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chr LEFT JOIN tm_chalanges AS ch ON chr.TM_CE_RE_Chalange_Id=ch.TM_CL_Id WHERE ch.TM_CL_Plan=e.TM_SPN_PlanId AND chr.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id) AS totalchallenge,
                 (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS w WHERE w.TM_STHW_Student_Id=a.TM_STU_User_Id AND w.TM_STHW_Plan_Id=e.TM_SPN_PlanId ) AS totalassesments,
                 (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId) AS totalpoints ";
            $criteria='search=search';
            if($_GET['date']=='before'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn < '".$_GET['fromdate']."' ) AS periodrevision,
                       (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date < '".$_GET['fromdate']."'  ) AS periodmocks,
                       (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date < '".$_GET['fromdate']."') AS periodchallenge,
                       (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Status=4 AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                       (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date < '".$_GET['fromdate']."' ) AS periodpoints
                       ";
                endif;
                $criteria.='&date=before&fromdate='.$_GET['fromdate'].'';
            elseif($_GET['date']=='after'):
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!=''):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn > '".$_GET['fromdate']."' ) AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date > '".$_GET['fromdate']."'  ) AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date > '".$_GET['fromdate']."') AS periodchallenge,
                           (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Status=4 AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date > '".$_GET['fromdate']."' ) AS periodpoints  ";
                endif;
                $criteria.='&date=after&fromdate='.$_GET['fromdate'].'';
            else:
                $formvals['date']=$_GET['date'];
                if($_GET['fromdate']!='' && $_GET['todate']):
                    $formvals['fromdate']=$_GET['fromdate'];
                    $formvals['todate']=$_GET['todate'];
                    $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodchallenge,
                           (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Status=4 AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ) AS periodpoints ";

                endif;
                $criteria.='&fromdate='.$_GET['fromdate'].'&todate='.$_GET['todate'].'';
            endif;
            $sql.="FROM tm_student AS a
                  LEFT JOIN tm_users AS b ON b.id=a.TM_STU_User_Id
                  LEFT JOIN tm_users AS c ON c.id=a.TM_STU_Parent_Id
                  INNER JOIN tm_student_plan AS e ON a.TM_STU_User_Id=e.TM_SPN_StudentId
                  INNER JOIN tm_plan AS f ON e.TM_SPN_PlanId=f.TM_PN_Id
                  WHERE a.TM_STU_Status IN(0,1,2)
                  ";
            if($_GET['school']!=''):
                if($_GET['school']=='0'):
                    $formvals['school']=$_GET['school'];
                    $formvals['schooltext']=$_GET['schooltext'];
                    $sql.=" AND a.TM_STU_SchoolCustomname LIKE '%".$_GET['schooltext']."%' ";
                    $criteria.='&school=0';
                    $criteria.='&schooltext='.$_GET['schooltext'].'';
                else:
                    $formvals['school']=$_GET['school'];
                    $sql.=" AND a.TM_STU_School='".$_GET['school']."'";
                    $criteria.='&school='.$_GET['school'].'';
                endif;

            endif;
            $criteria.='&school='.$_GET['school'].'';
            if($_GET['plan']!=''):
                $formvals['plan']=$_GET['plan'];
                $sql.=" AND e.TM_SPN_PlanId =".$_GET['plan'];
                $criteria.='&plan='.$_GET['plan'].'';
            endif;
            $page = $_GET['page'];
            $limit = 50;

            if($page)
                $offset = ($page - 1) * $limit; 			//first item to display on this page
            else
                $offset = 0;
            $sql.=" LIMIT $offset, $limit";
            //echo $sql;exit;
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            $dataitems='';
            if(count($dataval)>0):
                foreach($dataval AS $data):
                    $dataitems.="<tr>
                        <td>".$data['student']."</td>
                        <td>".$data['TM_STU_First_Name']." ".$data['TM_STU_Last_Name']."</td>
                        <td>".($data['TM_STU_School']=='0'? $data['TM_STU_SchoolCustomname']: School::model()->findByPk($data['TM_STU_School_Id'])->TM_SCL_Name)."</td>
                        <td>".$data['parent']."</td>
                        <td>".$data['email']."</td>
                        <td>".$data['TM_PN_Name']."</td>
                        <td>".($data['periodrevision']!=''?$data['periodrevision']:'0')."</td>
                        <td>".($data['totalrevision']!=''?$data['totalrevision']:'0')."</td>
                        <td>".($data['periodmocks']!=''?$data['periodmocks']:'0')."</td>
                        <td>".($data['totalmocks']!=''?$data['totalmocks']:'0')."</td>
                        <td>".($data['periodchallenge']!=''?$data['periodchallenge']:'0')."</td>
                        <td>".($data['totalchallenge']!=''?$data['totalchallenge']:'0')."</td>
                        <td>".($data['periodassesments']!=''?$data['periodassesments']:'0')."</td>
                        <td>".($data['totalassesments']!=''?$data['totalassesments']:'0')."</td>
                        <td>".($data['periodpoints']!=''?$data['periodpoints']:'0')."</td>
                        <td>".($data['totalpoints']!=''?$data['totalpoints']:'0')."</td>";
                    $dataitems.="</tr>";
                endforeach;
            else:
                $dataitems="<tr><td colspan='16' class='empty'><span class='empty' style='font-style: italic;'>No results found.</span></td></tr>";
            endif;
            $pages=ceil($total_pages / $limit);
            if($_GET['page']==''):
                $count=1;
            else:
                $count=$_GET['page'];
            endif;
            $nopages='';

            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page):
                    $class='class="active"';
                else:
                    $class='';
                endif;
                $nopages.='<li><a  '.$class.' href="'.Yii::app()->request->baseUrl.'/student/Studentsummaryreport?'.$criteria.'&page='.$i.'">'.$i.'</a></li>';
            }

            $nopages.='<li><a href="'.Yii::app()->request->baseUrl.'/student/Studentsummaryreport?'.$criteria.'&page='.($count + 1).'">Next</a></li>';
        }

        $this->render('studentsummary',array('dataitems'=>$dataitems,'formvals'=>$formvals,'nopages'=>$nopages));

    }
	
	public function actionDownloadsummary()
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,a.TM_STU_SchoolCustomname,a.TM_STU_School_Id,b.username AS student,b.id,c.username AS parent,c.email,e.TM_SPN_Id,f.TM_PN_Name,
                 (SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS g WHERE g.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND g.TM_STU_TT_Plan=e.TM_SPN_PlanId ) AS totalrevision,
                 (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS h WHERE h.TM_STMK_Student_Id=a.TM_STU_User_Id AND h.TM_STMK_Plan_Id=e.TM_SPN_PlanId ) AS totalmocks,
                 (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chr LEFT JOIN tm_chalanges AS ch ON chr.TM_CE_RE_Chalange_Id=ch.TM_CL_Id WHERE ch.TM_CL_Plan=e.TM_SPN_PlanId AND chr.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id) AS totalchallenge,
                 (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS w WHERE w.TM_STHW_Student_Id=a.TM_STU_User_Id AND w.TM_STHW_Plan_Id=e.TM_SPN_PlanId ) AS totalassesments,
                 (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId) AS totalpoints ";
        if($_GET['date']=='before'):
            if($_GET['fromdate']!=''):
                $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn < '".$_GET['fromdate']."' ) AS periodrevision,
                       (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date < '".$_GET['fromdate']."'  ) AS periodmocks,
                       (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date < '".$_GET['fromdate']."') AS periodchallenge,
                       (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Status=4 AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                       (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date < '".$_GET['fromdate']."' ) AS periodpoints
                       ";
            endif;
        elseif($_GET['date']=='after'):
            if($_GET['fromdate']!=''):
                $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn > '".$_GET['fromdate']."' ) AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date > '".$_GET['fromdate']."'  ) AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date > '".$_GET['fromdate']."') AS periodchallenge,
                           (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Status=4 AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date > '".$_GET['fromdate']."' ) AS periodpoints  ";
            endif;
        else:
            if($_GET['fromdate']!='' && $_GET['todate']):
                $sql.=",(SELECT COUNT(TM_STU_TT_Id) FROM tm_student_test AS ga WHERE ga.TM_STU_TT_Student_Id=a.TM_STU_User_Id AND ga.TM_STU_TT_Plan=e.TM_SPN_PlanId AND ga.TM_STU_TT_CreateOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodrevision,
                           (SELECT COUNT(TM_STMK_Id) FROM tm_student_mocks AS ha WHERE ha.TM_STMK_Student_Id=a.TM_STU_User_Id AND ha.TM_STMK_Plan_Id=e.TM_SPN_PlanId AND ha.TM_STMK_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodmocks,
                           (SELECT COUNT(TM_CE_RE_Id) FROM tm_chalangerecepient AS chra LEFT JOIN tm_chalanges AS cha ON chra.TM_CE_RE_Chalange_Id=cha.TM_CL_Id WHERE cha.TM_CL_Plan=e.TM_SPN_PlanId AND chra.TM_CE_RE_Recepient_Id=a.TM_STU_User_Id AND chra.TM_CE_RE_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."') AS periodchallenge,
                           (SELECT COUNT(TM_STHW_Id) FROM tm_student_homeworks AS wa WHERE wa.TM_STHW_Student_Id=a.TM_STU_User_Id AND wa.TM_STHW_Plan_Id=e.TM_SPN_PlanId AND wa.TM_STHW_Status=4 AND wa.TM_STHW_Date < '".$_GET['fromdate']."'  ) AS periodassesments,
                           (SELECT SUM(TM_STP_Points) FROM tm_studentpoints AS sp WHERE sp.TM_STP_Student_Id=a.TM_STU_User_Id AND sp.TM_STP_Standard_Id=e.TM_SPN_StandardId AND sp.TM_STP_Date BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ) AS periodpoints ";
            endif;
        endif;
        $sql.="FROM tm_student AS a
                  LEFT JOIN tm_users AS b ON b.id=a.TM_STU_User_Id
                  LEFT JOIN tm_users AS c ON c.id=a.TM_STU_Parent_Id
                  INNER JOIN tm_student_plan AS e ON a.TM_STU_User_Id=e.TM_SPN_StudentId
                  INNER JOIN tm_plan AS f ON e.TM_SPN_PlanId=f.TM_PN_Id
                  WHERE a.TM_STU_Status IN(0,1,2)
                  ";
        if($_GET['school']!=''):
            $sql.=" AND a.TM_STU_School LIKE '%".$_GET['school']."%' ";
        endif;
        if($_GET['plan']!=''):
            $sql.=" AND e.TM_SPN_PlanId =".$_GET['plan'];
        endif;
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataval=$dataReader->readAll();
        $dataitems='';

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=summaryreport.csv');
        $output = fopen('php://output', 'w');
        $headers=array(
            "Student Username",
            "Student Name",
            "School",
            "Parent Username",
            "Parent Email",
            "Subbscription",
            "Number of Revisions",
            "Total Number of Revisions",
            "Number of Mocks",
            "Total Number of Mocks",
            "Number of Challenges",
            "Total Number of Challenges",
            "Number of Practice",
            "Total Number of Practice",
            "Points Scored",
            "Total Points Scored"
        );

        function cleanData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
        }
        fputcsv($output,$headers );
        if(count($dataval)>0):
            $flag = false;
            foreach($dataval AS $data):
                $row=array($data['student'],$data['TM_STU_First_Name']." ".$data['TM_STU_Last_Name'],
                    ($data['TM_STU_School']=='0'? $data['TM_STU_SchoolCustomname']: School::model()->findByPk($data['TM_STU_School_Id'])->TM_SCL_Name),$data['parent'],$data['email'],$data['TM_PN_Name'],$data['periodrevision'],
                    $data['totalrevision'],$data['periodmocks'],$data['totalmocks'],$data['periodchallenge'],$data['totalchallenge'],
                    $data['periodassesments'],$data['totalassesments'],$data['periodpoints'],$data['totalpoints']);
                if(!$flag) {
                    // display field/column names as first row
                    fputcsv($output, $row);
                    $flag = true;
                }
                array_walk($row, 'cleanData');
                fputcsv($output, $row);
            endforeach;
        endif;
    }
    public function actionPrintmock($id)
    {       
        $schoolhomework=Mock::model()->findByPk($id);
        $user=User::model()->findByPk(Yii::app()->user->id);
        $report=New WorksheetprintReport();
        //$report->TM_WPR_Date=date("Y-m-d H:i:s");
        $report->TM_WPR_Username=$user->username;
        $profile=Profile::model()->findByPk(Yii::app()->user->id);
        $report->TM_WPR_FirstName=$profile['firstname'];
        $report->TM_WPR_LastName=$profile['lastname'];
        $report->TM_WPR_Standard=Yii::app()->session['standard'];
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id.''));
        if($student['TM_STU_School']!=0):
            $schoolname=School::model()->findByPk($student['TM_STU_School'])->TM_SCL_Name;
            $schoolid=$student['TM_STU_School'];
        else:
            $schoolname=$student['TM_STU_SchoolCustomname'];
            $schoolid=0;
        endif;
        $type=1;
        $location=$student['TM_STU_City'];
        $report->TM_WPR_School_id=$schoolid;
        $report->TM_WPR_School=$schoolname;
        $report->TM_WPR_Type=$type;
        $report->TM_WPR_Location=$location;
        $hwemoorkdata=Mock::model()->findByPK($id);
        $homework=$hwemoorkdata->TM_MK_Name;
        $report->TM_WPR_Worksheetname=$homework;
        if($schoolhomework->TM_MK_Worksheet==''):
            $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $username=User::model()->findByPk(Yii::app()->user->id)->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;  
            $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
            $html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>"; 
            $html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";
            $totalquestions=count($homeworkqstns);         
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):
    
                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
				$showoption=false; 
				if($question->TM_QN_Show_Option==1):
					$showoption=true;
				else:
					$showoption=false;
				endif;				
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks: " . $marks . " ( &nbsp; )</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        if($showoption):
                            foreach($question->answers AS $key=>$answer):
    							if($answer->TM_AR_Image!=''):
                                    $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                else:
                                    $ansoptimg='';
                                endif;
                                $ans=$answer->TM_AR_Answer;
                                $find='<p>';
                                $pos = strpos($ans, $find);
                                if ($pos === false) {
                                    $html.="<tr><td colspan='2'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
    								<td align='right'>$ansoptimg</td></tr>";
                                } else {
                                    $html.="<tr><td colspan='2'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td>
    								<td align='right'>$ansoptimg</td></tr>";
                                }
                            endforeach;
                        endif;						
                        if($question->TM_QN_Type_Id=='5'):
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            foreach ($questions AS $key=>$childquestion):
                                $html .= "<tr><td colspan='3'>".$childquestion->TM_QN_Question." </td></tr>";
                                if ($childquestion->TM_QN_Image != ''):
                                    $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                                endif;
                                if($showoption):
                                    foreach($childquestion->answers AS $key=>$answer):
    									if($answer->TM_AR_Image!=''):
                                            $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                        else:
                                            $ansoptimg='';
                                        endif;
                                        $ans=$answer->TM_AR_Answer;
                                        $find='<p>';
                                        $pos = strpos($ans, $find);
                                        if ($pos === false)
                                        {
                                            $html.="<tr><td colspan='2'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
    										<td align='right'>$ansoptimg</td></tr>";
                                        }
                                        else
                                        {
                                            $html.="<tr><td colspan='2'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td>
    										<td align='right'>$ansoptimg</td></tr>";
                                        }
                                    endforeach;
                                endif;								
                            endforeach;
                        endif;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"><br></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    if ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $countpart = 1;
                        $childhtml = "";
                        foreach ($questions AS $childquestion):
                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                            $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                            $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $childhtml .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            $countpart++;
                        endforeach;
                        $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks: " . $displaymark . " ( &nbsp; )</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $html .= $childhtml;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"><br></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />                    
                        endif;
                    endif;
            endforeach;
    		$html .= "</table></body></html>";
    		$schoolhw=Mock::model()->findByPK($id);
            //$school=School::model()->findByPK($schoolhw->TM_SCH_School_Id);
            $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));
            $header = '<html><body style="background-image:url('.Yii::app()->request->baseUrl . '/images/pdfbg.png);background-repeat: no-repeat;background-position: center center;">
            <div style="border-bottom:solid 2px #000;margin-bottom:10px;"><table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                    <td width="50%"><span style="font-weight: lighter; ">Student Name : </span> '.$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name.'</td>                    
                    <td width="50%"><span style="font-weight: lighter; ">Date : </span>'.date('d/m/Y').'</td>                    
                    </tr></table></div>';
    
            $mpdf = new mPDF();
            $html=$header.$html;
            //$mpdf->SetHTMLHeader($header);
            $mpdf->SetWatermarkText($username, .1);
            $mpdf->showWatermarkText = false;
    
            $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
            	<tr>
            		<td width="100%" colspan="2">
            			&copy; Copyright @ TabbieMe Ltd. All rights reserved .
            		</td>
            	</tr>
            	<tr>
            		<td width="80%" >
            			www.tabbiemath.com  - The one stop shop for Maths Revision.
            		</td>
                    <td align="right" >Page #{PAGENO}</td>
            	</tr>
            </table>');
    
            //echo $html;exit;
            $report->save(false);
            $name='WORKSHEET'.time().'.pdf';
            $mpdf->WriteHTML($html);
            $mpdf->Output($name, 'I');
        else:
            $report->save(false);
            $this->redirect(Yii::app()->request->baseUrl."/worksheets/".$schoolhomework->TM_MK_Worksheet);
        endif;

    }  
    public function actionPrintmocksolution($id)
    {        
            $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $username=User::model()->findByPk(Yii::app()->user->id)->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;  
            $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
            $html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>"; 
            $html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";
            $totalquestions=count($homeworkqstns);         
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):
    
                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
				$showoption=false;                       
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                    if ($question->TM_QN_Type_Id!='7'):
                        $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $marks . "</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;                                              
                        if($question->TM_QN_Type_Id=='5'):
                            $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                            foreach ($questions AS $key=>$childquestion):
                                $html .= "<tr><td colspan='3' >".$childquestion->TM_QN_Question." </td></tr>";
                                if ($childquestion->TM_QN_Image != ''):
                                    $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                                endif;
                                if($showoption):
                                    foreach($childquestion->answers AS $key=>$answer):
                                        if($answer->TM_AR_Image!=''):
                                            $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                        else:
                                            $ansoptimg='';
                                        endif;
                                        $ans=$answer->TM_AR_Answer;
                                        $find='<p>';
                                        $pos = strpos($ans, $find);
                                        if ($pos === false)
                                        {
                                            $html.="<tr><td colspan='3'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
                                            <td align='right'>$ansoptimg</td></tr>";
                                        }
                                        else
                                        {
                                            $html.="<tr><td colspan='3'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td>
                                            <td align='right'>$ansoptimg</td></tr>";
                                        }
                                    endforeach;
                                endif;
                            endforeach;
                        endif;

                        $html .= "<tr><td width='15%'><b>Solution </b></td><td align='right' width='70%'></td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Solutions."</td></tr>";
                        if ($question->TM_QN_Solution_Image != ''):
                            $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Solution_Image . '?size=thumbs&type=solution&width=300&height=300"></td></tr>';
                        endif;                          
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"><br></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                        endif;
                    endif;
                    if ($question->TM_QN_Type_Id == '7'):
                        $displaymark = 0;
                        $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                        $countpart = 1;
                        $childhtml = "";
                        foreach ($questions AS $childquestion):
                            $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                            $childhtml .="<tr><td colspan='3' align='left'>Part." . $countpart."</td></tr>";
                            $childhtml .="<tr><td colspan='3' >".$childquestion->TM_QN_Question."</td></tr>";
                            if ($childquestion->TM_QN_Image != ''):
                                $childhtml .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                            endif;
                            $countpart++;
                        endforeach;
                        $html .= "<tr><td width='15%'><b>Question " . $homeworkqstncount ."</b></td><td align='right' width='70%'>Marks:" . $displaymark . "</td></tr>";
                        $html .= "<tr><td colspan='3' >".$question->TM_QN_Question."</td></tr>";
                        if ($question->TM_QN_Image != ''):
                            $html .= '<tr><td colspan="3" align="left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td></tr>';
                        endif;
                        $html .= $childhtml;
                        $totalquestions--;
                        if ($totalquestions != 0):
                            $html .= '<tr ><td colspan="3"><br></td></tr>';
                            //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />                    
                        endif;
                    endif;
            endforeach;
    		$html .= "</table></body></html>";
    		$schoolhw=Mock::model()->findByPK($id);
            $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));
            $header = '<html><body style="background-image:url('.Yii::app()->request->baseUrl . '/images/pdfbg.png);background-repeat: no-repeat;background-position: center center;">
                <div style="border-bottom:solid 2px #000;margin-bottom:10px;"><table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; "><tr>
                    <td width="25%"><span style="font-weight: lighter; ">Student Name :</span></td>
                    <td width="25%" align="center" style="font-weight: lighter; ">'.$student->TM_STU_First_Name.'</td>
                    <td width="25%"><span style="font-weight: lighter; ">Date : </span></td>
                    <td width="25%" align="center" style="font-weight: lighter; ">'.date('d/m/Y').'</td>
                    </tr></table></div>';
    
            $mpdf = new mPDF();
    
            //$mpdf->SetHTMLHeader($header);
            $mpdf->SetWatermarkText($username, .1);
            $mpdf->showWatermarkText = false;
            $html=$header.$html;
            $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
            	<tr>
            		<td width="100%" >
            			Copyright c TabbieMe Education Services Pvt Ltd
            		</td>
            	</tr>
            	<tr>
            		<td width="100%" >
            			Register on www.tabbiemath.com for Maths worksheets & solutions
            		</td>
            	</tr>
            </table>');
    
            //echo $html;exit;
            $name='WORKSHEET'.time().'.pdf';
            $mpdf->WriteHTML($html);
            $mpdf->Output($name, 'I');
    }

    public function actionViewworksheet()
    {
        $id=$_POST['worksheetid'];
        $schoolhomework=Mock::model()->findByPk($id);
        $user=User::model()->findByPk(Yii::app()->user->id);
        if($schoolhomework->TM_MK_Worksheet==''):
            $homeworkqstns=MockQuestions::model()->findAll(array(
                'condition' => 'TM_MQ_Mock_Id=:test',
                'params' => array(':test' => $id),
                'order'=>'TM_MQ_Order ASC'
            ));
            $user=User::model()->findByPk(Yii::app()->user->id);
            $username=$user->username;
            $hwemoorkdata=Mock::model()->findByPK($id);
            $school=$hwemoorkdata->TM_MK_Name;
            $homework=$hwemoorkdata->TM_MK_Name;
            $showoptionmaster=3;
            $html = "<table cellpadding='5' border='0' width='100%' style='font-family:lucidasansregular;'>";
            //$html .= "<tr><td colspan=3 align='center'><u><b>".$homework."</b><u></td></tr>";
            //$html .= "<tr><td colspan=3 align='center'>&nbsp;</td></tr>";
            $totalquestions=count($homeworkqstns);
            $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
            foreach ($homeworkqstns AS $key => $homeworkqstn):

                $homeworkqstncount = $key + 1;
                $question = Questions::model()->findByPk($homeworkqstn->TM_MQ_Question_Id);
                if($question->TM_QN_Show_Option==1):

                    $showoption=true;
                else:
                    $showoption=false;
                endif;
                ///$showoption=false;
                //code by amal
                if($homeworkqstn->TM_MQ_Type!=5):
                    if($homeworkqstn->TM_MQ_Type==4):
                        $answers=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                        $marks=$answers->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$homeworkqstn->TM_MQ_Question_Id' AND TM_AR_Correct='1' "));
                        $childtotmarks=0;
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        $marks=$childtotmarks;
                    endif;
                endif;
                if($homeworkqstn->TM_MQ_Type==5):
                    $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$homeworkqstn->TM_MQ_Question_Id' "));
                    $childtotmarks=0;
                    foreach($childqstns AS $childqstn):
                        if($childqstn->TM_QN_Type_Id==4):
                            $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                            $childtotmarks=$answer->TM_AR_Marks;
                        else:
                            $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                            foreach($answers AS $answer):
                                $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                            endforeach;
                            $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                        endif;
                    endforeach;
                    $marks=$childtotmarks;
                endif;
                if($homeworkqstn->TM_MQ_Type==6):
                    $marks=$question->TM_QN_Totalmarks;
                endif;
                //ends
                if ($question->TM_QN_Type_Id!='7'):
                    $html .= "<tr><td><div class='col-md-3'><span><b>Question " . $homeworkqstncount ."</b></span></div>
                    <div class='col-md-3 pull-right'><span class='pull-right' style='font-size: 14px;  padding-top: 1px;'>Marks:" . $marks . " ( &nbsp;&nbsp;&nbsp; )</span>
                    </div>
                    <div class='col-md-12 padnone'><div class='col-md-12'><p>".$question->TM_QN_Question."</p>
                    </div></div>";
                    if ($question->TM_QN_Image != ''):
                        $html .= '<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300">
                        </div>';
                    endif;
                    $html.="</td></tr>";
                    if($showoption):
                        foreach($question->answers AS $key=>$answer):
                            if($answer->TM_AR_Image!=''):
                                $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                            else:
                                $ansoptimg='';
                            endif;
                            $ans=$answer->TM_AR_Answer;
                            $find='<p>';
                            $pos = strpos($ans, $find);
                            if ($pos === false) {
                                $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</p></div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";
                                //$html.="<tr><td colspan='2'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td><td align='right'>$ansoptimg</td></tr>";
                            } else {
                                $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";

                            }
                        endforeach;
                    endif;
                    if($question->TM_QN_Type_Id=='5'):
                        $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach ($questions AS $key=>$childquestion):
                            $html.="<tr><td><div class='col-md-12 padnone'>
                                    <div class='col-md-12'><p>".$childquestion->TM_QN_Question."</p></div>
	                                </div>";
                            if ($childquestion->TM_QN_Image != ''):
                                $html.='<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></div>';
                            endif;
                            $html.="</td></tr>";
                            if($showoption):
                                foreach($childquestion->answers AS $key=>$answer):
                                    if($answer->TM_AR_Image!=''):
                                        $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                    else:
                                        $ansoptimg='';
                                    endif;
                                    $ans=$answer->TM_AR_Answer;
                                    $find='<p>';
                                    $pos = strpos($ans, $find);
                                    if ($pos === false)
                                    {
                                        $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</p></div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";
                                    }
                                    else
                                    {
                                        $html.="<tr><td>
                                                <div class='col-md-12 padnone'>
                                                <div class='col-md-12'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</div></div>
                                                <div class='col-md-12 pull-right'>$ansoptimg</div></td></tr>";
                                    }
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        //$html .= '<tr ><td colspan="3"><br></td></tr>';
                        //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                    endif;
                endif;
                if ($question->TM_QN_Type_Id == '7'):
                    $displaymark = 0;
                    $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                    $countpart = 1;
                    $childhtml = "";
                    foreach ($questions AS $childquestion):
                        $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                        $childhtml.="<tr><td><div class='col-md-12 pull-left'>Part." . $countpart."</div>
                                        <div class='col-md-12'>".$childquestion->TM_QN_Question."</div>";
                        if ($childquestion->TM_QN_Image != ''):
                            $childhtml.='<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></div>';
                        endif;
                        $childhtml.="</td></tr>";
                        $countpart++;
                    endforeach;
                    $html.="<tr><td><div class='col-md-3'><span><b>Question " . $homeworkqstncount ."</b></span>
	                        </div>
	                        <div class='col-md-3 pull-right'>
		                    <span class='pull-right' style='font-size: 14px;  padding-top: 1px;'>Marks:" . $displaymark . " ( )</span>
	                        </div>
	                        <div class='col-md-12 padnone'>
                            <div class='col-md-12'><p>".$question->TM_QN_Question."</p>
		                    </div>
	                        </div>";
                    if ($question->TM_QN_Image != ''):
                        $html.='<div class="col-md-12 pull-left"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></div>';
                    endif;
                    $html.="</td></tr>";
                    $html .= $childhtml;
                    $totalquestions--;
                    if ($totalquestions != 0):
                        //$html .= '<tr ><td colspan="3"><br></td></tr>';
                        //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                    endif;
                endif;
            endforeach;
            $html .= "</table></body></html>";
        endif;
        $print="<a href='".Yii::app()->createUrl('student/printmock',array('id'=>$id))."' class='btn btn-warning btn-xs' target='_blank'>
                        <span style='padding-right: 10px;'>Print</span>
                        <i class='fa fa-print fa-lg'></i></a>";
        $arr=array('result' => $html,'print'=>$print);
        echo json_encode($arr);
    }

    public function actionPrintresource()
    {
        $id=$_POST['worksheetid'];
        $user=User::model()->findByPk(Yii::app()->user->id);
        $report=New WorksheetprintReport();
        //$report->TM_WPR_Date=date("Y-m-d H:i:s");
        $report->TM_WPR_Username=$user->username;
        $profile=Profile::model()->findByPk(Yii::app()->user->id);
        $report->TM_WPR_FirstName=$profile['firstname'];
        $report->TM_WPR_LastName=$profile['lastname'];
        $report->TM_WPR_Standard=Yii::app()->session['standard'];
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id.''));
        if($student['TM_STU_School']!=0):
            $schoolname=School::model()->findByPk($student['TM_STU_School'])->TM_SCL_Name;
            $schoolid=$student['TM_STU_School'];
        else:
            $schoolname=$student['TM_STU_SchoolCustomname'];
            $schoolid=0;
        endif;
        $type=1;
        $location=$student['TM_STU_City'];
        $report->TM_WPR_School_id=$schoolid;
        $report->TM_WPR_School=$schoolname;
        $report->TM_WPR_Type=$type;
        $report->TM_WPR_Location=$location;
        $hwemoorkdata=Mock::model()->findByPK($id);
        $homework=$hwemoorkdata->TM_MK_Name;
        $report->TM_WPR_Worksheetname=$homework;
        $report->save(false);
    }

    public function actionResource()
    {
        $plan = $this->GetStudentDet();
        $criteria = new CDbCriteria;
        if(isset($_GET['search']))
        {
            $formvals=array();
            $mock=$_GET['resource'];
            if($mock!='')
            {
                $formvals['mock']=$_GET['resource'];
                //$criteria = new CDbCriteria;
                $criteria->condition = 'TM_MK_Name LIKE "%'.$mock.'%" AND TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Resource=:resource';
                // AND TM_MK_Id NOT IN ( SELECT TM_STMK_Mock_Id FROM tm_student_mocks WHERE TM_STMK_Student_Id='.Yii::app()->user->id.')
                $criteria->params = array(':publisher' => '3', ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0',':resource' => '1');
                $criteria->order = 'TM_MK_Sort_Order DESC';
            }
            else
            {
                $criteria->condition = 'TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Resource=1';
                $criteria->params = array(':publisher' => '3', ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0');
                $criteria->order = 'TM_MK_Sort_Order DESC';
            }
        }
        else
        {
            $criteria->condition = 'TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Status=:status AND TM_MK_Resource=1';
            // AND TM_MK_Id NOT IN ( SELECT TM_STMK_Mock_Id FROM tm_student_mocks WHERE TM_STMK_Student_Id='.Yii::app()->user->id.')
            $criteria->params = array(':publisher' => '3', ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0');
            $criteria->order = 'TM_MK_Sort_Order DESC';
        }
        if(isset($_GET['tags'])):
            $criteria->condition = 'TM_MK_Publisher_Id=:publisher AND TM_MK_Syllabus_Id=:syllabus AND TM_MK_Standard_Id=:standard AND TM_MK_Resource=1 AND TM_MK_Status=:status AND (TM_MK_Tags REGEXP "'.$_GET['tags'].'" OR TM_MK_Name LIKE "%'.$_GET['tags'].'%" OR TM_MK_Description LIKE "%'.$_GET['tags'].'%")';
            $criteria->params = array(':publisher' => '3', ':syllabus' => $plan->TM_SPN_SyllabusId, ':standard' => $plan->TM_SPN_StandardId, ':status' => '0');
            $criteria->order = 'TM_MK_Sort_Order DESC';
        endif;
        //echo $criteria->condition;
        $mocks = Mock::model()->findAll($criteria);
        $mockheaders=MockTags::model()->findAll(array('condition'=>'TM_MT_Type=1 AND TM_MT_Standard_Id='.$plan->TM_SPN_StandardId.' '));
        $this->render('resources', array('mocks' => $mocks,'formvals'=>$formvals,'mockheaders'=>$mockheaders));
    }
     public function actionPrintrevision($id)
    {
        $revisionqstns=StudentQuestions::model()->findAll(array(
            'condition' => 'TM_STU_QN_Test_Id=:test',
            'params' => array(':test' => $id),
            'order'=>'TM_STU_QN_Number ASC'
        ));
        $chapters=StudentTestChapters::model()->findAll(array('condition'=>'TM_STC_Student_Id='.Yii::app()->user->id.' AND TM_STC_Test_Id='.$id.' '));
        $revisionname='';
        foreach ($chapters AS $key => $chapter):
            $revisionname.=($key != '0' ? ' ,' : '') . $chapter->chapters->TM_TP_Name;
        endforeach;
        $username=User::model()->findByPk(Yii::app()->user->id)->username;
        $html = "<table cellpadding='5' border='0' width='100%' style='font-size:14px;font-family: STIXGeneral;'>";
        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;text-align:center' align='center'><u><b>".$revisionname."</b><u></td><td width='6%' style='vertical-align: top;padding:0;'></td></tr>";
        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td colspan=3 align='center'>&nbsp;</td><td width='6%' style='vertical-align: top;padding:0;'></td></tr>";
        $totalquestions=count($revisionqstns);
        $alphabets=array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F');
        $totalmarks=0;
        foreach ($revisionqstns AS $key => $revisionqstn):
            $homeworkqstncount = $key + 1;
            $question = Questions::model()->findByPk($revisionqstn->TM_STU_QN_Question_Id);
            $questionid=$revisionqstn->TM_STU_QN_Question_Id;
            $showoption=true;
            if($revisionqstn->TM_STU_QN_Type!=5):
                if($revisionqstn->TM_STU_QN_Type==4):
                    $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$questionid' "));
                    $marks=$answer->TM_AR_Marks;
                else:
                    $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$questionid' AND TM_AR_Correct='1' "));
                    $childtotmarks=0;
                    foreach($answers AS $answer):
                        $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                    endforeach;
                    //$childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                    $marks=$childtotmarks;
                endif;
            endif;
            if($revisionqstn->TM_STU_QN_Type==5):
                $childqstns=Questions::model()->findAll(array('condition' => "TM_QN_Parent_Id='$questionid' "));
                $childtotmarks=0;
                foreach($childqstns AS $childqstn):
                    if($childqstn->TM_QN_Type_Id==4):
                        $answer=Answers::model()->find(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' "));
                        $childtotmarks=$answer->TM_AR_Marks;
                    else:
                        $answers=Answers::model()->findAll(array('condition' => "TM_AR_Question_Id='$childqstn->TM_QN_Id' AND TM_AR_Correct='1' "));
                        foreach($answers AS $answer):
                            $childtotmarks=$childtotmarks + $answer->TM_AR_Marks;
                        endforeach;
                        $childtotmarks=$childtotmarks + $answers->TM_AR_Marks;
                    endif;
                endforeach;
                $marks=$childtotmarks;
            endif;
            if($revisionqstn->TM_STU_QN_Type==6):
                $marks=$question->TM_QN_Totalmarks;
            endif;
            if ($revisionqstn->TM_STU_QN_Type!='7'):
                $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>( " . $marks . " )</td></tr>";
                // $html .= "<tr></tr>";
                if ($question->TM_QN_Image != ''):
                    $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%"" style="vertical-align: top;padding:0;"></td></tr>';
                endif;
                /*if($revisionqstn->TM_STU_QN_Type==6):
                    $showoption=false;
                endif;
                if($showoption):
                    foreach($question->answers AS $key=>$answer):
                        if($answer->TM_AR_Image!=''):
                            $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                        else:
                            $ansoptimg='';
                        endif;
                        $ans=$answer->TM_AR_Answer;
                        $find='<p>';
                        $pos = strpos($ans, $find);
                        if ($pos === false) {
                            $html.="<tr><td colspan='2'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
                                    <td align='right'>$ansoptimg</td></tr>";
                        } else {
                            $html.="<tr><td colspan='2'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td>
                                    <td align='right'>$ansoptimg</td></tr>";
                        }
                    endforeach;
                endif;*/
                if($revisionqstn->TM_STU_QN_Type=='5'):
                    $questions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                    foreach ($questions AS $key=>$childquestion):
                        $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>".$childquestion->TM_QN_Question." </td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                        if ($childquestion->TM_QN_Image != ''):
                            $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                        endif;
                        /*if($showoption):
                            foreach($childquestion->answers AS $key=>$answer):
                                if($answer->TM_AR_Image!=''):
                                    $ansoptimg="<img class='img-responsive img_right' src='".Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$answer->TM_AR_Image.'?size=thumbs&type=answer&width=100&height=100'."' alt=''>";
                                else:
                                    $ansoptimg='';
                                endif;
                                $ans=$answer->TM_AR_Answer;
                                $find='<p>';
                                $pos = strpos($ans, $find);
                                if ($pos === false)
                                {
                                    $html.="<tr><td colspan='2'><p>".$alphabets[$key].")".$answer->TM_AR_Answer."</td>
                                            <td align='right'>$ansoptimg</td></tr>";
                                }
                                else
                                {
                                    $html.="<tr><td colspan='2'>".substr_replace($answer->TM_AR_Answer, '<p>'.$alphabets[$key].') ', 0,3)."</td>
                                            <td align='right'>$ansoptimg</td></tr>";
                                }
                            endforeach;
                        endif;*/
                    endforeach;
                endif;
                $totalquestions--;
                if ($totalquestions != 0):
                    $html .= '<tr ><td colspan="3"></td></tr>';
                    //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                endif;
            endif;
            if ($question->TM_QN_Type_Id == '7'):
                $displaymark = 0;
                $questions = Questions::model()->findAll(array("condition" => "TM_QN_Parent_Id ='" . $question->TM_QN_Id . "'", "order" => "TM_QN_Id ASC"));
                $countpart = 1;
                $childhtml = "";
                foreach ($questions AS $childquestion):
                    $displaymark = $displaymark + $childquestion->TM_QN_Totalmarks;
                    $childhtml .="<tr>td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>Part." . $countpart."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td></tr>";
                    $childhtml .="<tr><td width='6%' style='vertical-align: top;padding:0;'></td><td style='padding:0;'>".$childquestion->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'></td</tr>";
                    if ($childquestion->TM_QN_Image != ''):
                        $childhtml .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $childquestion->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                    endif;
                    $countpart++;
                endforeach;
                $html .= "<tr><td width='6%' style='vertical-align: top;padding:0;'><b>" . $homeworkqstncount .".</b></td><td style='padding:0;'>".$question->TM_QN_Question."</td><td align='right' width='6%' style='vertical-align: top;padding:0;'>( " . $displaymark . " )</td></tr>";
                // $html .= "<tr></tr>";
                if ($question->TM_QN_Image != ''):
                    $html .= '<tr><td width="6%" style="vertical-align: top;padding:0;"></td><td style="padding:0;"><img src="' . Yii::app()->request->baseUrl . '/site/Imagerender/id/' . $question->TM_QN_Image . '?size=large&type=question&width=300&height=300"></td><td align="right" width="6%" style="vertical-align: top;padding:0;"></td></tr>';
                endif;
                $html .= $childhtml;
                $totalquestions--;
                if ($totalquestions != 0):
                    $html .= '<tr ><td colspan="3"></td></tr>';
                    //<hr style="background:#0f080c; border:0; height:1px; padding-bottom: 0;" />
                endif;
            endif;
            $totalmarks=$totalmarks+$marks;
        endforeach;
        $html .= "</table></div>";
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.Yii::app()->user->id));
        $studentname=$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name;
        $schoolname=($student->TM_STU_School=='0'? $student->TM_STU_SchoolCustomname: School::model()->findByPk($student->TM_STU_School)->TM_SCL_Name);
        $studentplan=StudentPlan::model()->find(array('condition'=>'TM_SPN_StudentId='.Yii::app()->user->id.' '));
        $standradname=Standard::model()->findByPK($studentplan->TM_SPN_StandardId)->TM_SD_Name;

        $header = '
            <div><table width="100%" style="font-size:14px;font-family: STIXGeneral;margin-bottom:10px;">
            <tr><td width="50%"><span style="font-weight: lighter; ">Student Name : </span> '.$studentname.'</td>
            <td width="50%" style="text-align:right;"><span style="font-weight: lighter; ">Date : </span>'.date('d/m/Y').'</td></tr>
            </table>';

        $report=New WorksheetprintReport();
        //$report->TM_WPR_Date=date("Y-m-d H:i:s");
        $type=1;
        $location=$student['TM_STU_City'];
        $user=User::model()->findByPk(Yii::app()->user->id);
        $report->TM_WPR_Username=$user->username;
        $profile=Profile::model()->findByPk(Yii::app()->user->id);
        $report->TM_WPR_FirstName=$profile['firstname'];
        $report->TM_WPR_LastName=$profile['lastname'];
        $report->TM_WPR_Standard=Yii::app()->session['standard'];
        $report->TM_WPR_School_id=$student->TM_STU_School;
        $report->TM_WPR_School=$schoolname;
        $report->TM_WPR_Type=$type;
        $report->TM_WPR_Location=$location;
        $report->TM_WPR_Worksheetname=$revisionname;
        $report->save(false);

        $html=$header.$html;
        $this->render('printpdf',array('html'=>$html));
        // $mpdf = new mPDF();

        // //$mpdf->SetHTMLHeader($header);
        // $mpdf->SetWatermarkText($username, .1);
        // $mpdf->showWatermarkText = false;

        // $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: lighter; ">
        //      <tr>
        //          <td width="100%" colspan="2">
        //              &copy; Copyright @ TabbieMe Ltd. All rights reserved .
        //          </td>
        //      </tr>
        //      <tr>
        //          <td width="80%" >
        //              www.tabbiemath.com  - The one stop shop for Maths Revision.
        //          </td>
        //             <td align="right" >Page #{PAGENO}</td>
        //      </tr>
        //     </table>');

        // //echo $html;exit;
        // $name='REVISION'.time().'.pdf';
        // $mpdf->WriteHTML($header.$html);
        // $mpdf->Output($name, 'I');
    }
}   
