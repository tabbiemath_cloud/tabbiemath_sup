<?php

class LevelsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('PrintCertificate'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudent()'
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','view','create','update'),
				'users'=>UserModule::getSuperAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Levels;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Levels']))
		{
            $model->attributes=$_POST['Levels'];
            $model->TM_LV_icon=CUploadedFile::getInstance($model,'TM_LV_icon');
			if($model->save()):
                    $model->TM_LV_icon->saveAs('images/levelicons/'.$model->TM_LV_icon);
                    $this->redirect(array('view','id'=>$model->TM_LV_Id));
            endif;
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
    public function actionPrintCertificate($id)
    {
        $studentlevel=Studentlevels::model()->count(array('condition'=>'TM_STL_Student_Id=:student AND TM_STL_Level_Id=:level','params'=>array(':student'=>Yii::app()->user->id,':level'=>$id)));
        $item=Levels::model()->findByPk($id);
        $mpdf = new mPDF('','', 0, '', 5, 5, 5, 5, 9, 9, 'L');
        $stylesheet = file_get_contents(dirname(__FILE__)."/../../css/frontendbootstrap.min.css");
        $mpdf->WriteHTML($stylesheet,1);
        $html='<div style="width:100%;height:100%;border:solid 10px #FFD400 ;">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td style="padding-top:50px; "><img src="'.Yii::app()->request->baseUrl.'/images/logo.png" alt="Logo" style="margin-left:36%;"></td>
                                </tr>
                                <tr><td style="text-align:center;"><h1 style="font-weight:700;color:#7A7E81">TEST</h1></td></tr>
                            </table>
                            </div>';
        $mpdf->WriteHTML($html, 2);
        $mpdf->Output();
        /*if($studentlevel>0):
            $item=Levels::model()->findByPk($id);
            $mpdf = new mPDF();
            $stylesheet = file_get_contents('stylesheet.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML('<div id="mydiv"><p>HTML content goes here...</p></div>', 2);
            $mpdf->Output();
        else:
            $this->redirect(array('student/home'));
        endif;*/

    }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $icon=$model->TM_LV_icon;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Levels']))
		{
            $model->attributes=$_POST['Levels'];
            if(CUploadedFile::getInstance($model,'TM_LV_icon')):
                $model->TM_LV_icon=CUploadedFile::getInstance($model,'TM_LV_icon');
            else:
                $model->TM_LV_icon=$icon;
            endif;
            if($model->save()):
                if(CUploadedFile::getInstance($model,'TM_LV_icon')):
                    $model->TM_LV_icon->saveAs('images/levelicons/'.$model->TM_LV_icon);
                endif;
                $this->redirect(array('view','id'=>$model->TM_LV_Id));
            endif;
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Levels');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Levels('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Levels']))
			$model->attributes=$_GET['Levels'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Levels the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Levels::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Levels $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='levels-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
