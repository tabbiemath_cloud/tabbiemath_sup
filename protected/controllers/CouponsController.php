<?php

class CouponsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('GetCouvheRrate'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','index','view'),
                'users'=>UserModule::getSuperAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Coupons;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Coupons']))
		{
			$model->attributes=$_POST['Coupons'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_CP_Id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Coupons']))
		{
			$model->attributes=$_POST['Coupons'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->TM_CP_Id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Coupons');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Coupons('search');
/*		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Coupons']))
			$model->attributes=$_GET['Coupons'];*/

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Coupons the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Coupons::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    public function actionGetCouvheRrate()
    {
        if($_POST['plan']!='' & $_POST['voucher']!='')
        {            
            $location=User::model()->findByPk(Yii::app()->user->id)->location;
			$returnarray=array();
            $plan=Plan::model()->with('currency')->with('additional')->findByPk($_POST['plan']);
			$voucher=Coupons::model()->with('currency')->find(array('condition'=>"TM_CP_Status=0 AND TM_CP_Code='".$_POST['voucher']."'"));
			if(count($voucher)>0): 
				if($location=='0' & $plan->TM_PN_Currency_Id=='1'):
                    $currency=Currency::model()->findByPk($plan->TM_PN_Currency_Id);
                    if($voucher->TM_CP_Type=='1'):                              
						$percentage=$voucher->TM_CP_value/100;                 
						$amount=$plan->TM_PN_Rate*$percentage;
						$amount=round($amount,2,PHP_ROUND_HALF_UP);
						$couponamount=$amount.' '.$currency->TM_CR_Code;
						$rate=$plan->TM_PN_Rate-$amount;
                        $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;	                         
                    elseif($voucher->TM_CP_Type=='0'):   
						$rate=$plan->TM_PN_Rate-$voucher->TM_CP_value;
						$couponamount=$voucher->TM_CP_value.' '.$currency->TM_CR_Code; 
                        $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;					                                                
                    endif;				
				elseif($location=='0' & $plan->TM_PN_Additional_Currency_Id=='1'):
                    $currency=Currency::model()->findByPk($plan->TM_PN_Additional_Currency_Id);
					if($voucher->TM_CP_Type=='1'):                              
						 $percentage=$voucher->TM_CP_value/100;                 
						 $amount=$plan->TM_PN_Additional_Rate*$percentage;
						 $amount=round($amount,2,PHP_ROUND_HALF_UP);
						 $couponamount=$amount.' '.$currency->TM_CR_Code;
						 $rate=$plan->TM_PN_Additional_Rate-$amount;
                         $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;						 
					elseif($voucher->TM_CP_Type=='0'):                
						$rate=$plan->TM_PN_Additional_Rate-$voucher->TM_CP_value;
						$couponamount=$voucher->TM_CP_value.' '.$currency->TM_CR_Code;
                        $returnarray['rate']=$rate.' '.$currency->TM_CR_Code; 
					endif;				
				elseif($location=='1' & $plan->TM_PN_Currency_Id!='1'):
                    $currency=Currency::model()->findByPk($plan->TM_PN_Currency_Id);


                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Rate*$percentage;
                         $couponamount=$amount.' '.$currency->TM_CR_Code;
                         $rate=$plan->TM_PN_Rate-$amount;
                         $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $rate=$plan->TM_PN_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->TM_CP_value.' '.$currency->TM_CR_Code;
                        $returnarray['rate']=$rate.' '.$currency->TM_CR_Code; 
                    endif; 				
				elseif($location=='1' & ($plan->TM_PN_Additional_Currency_Id!='1' || $plan->TM_PN_Additional_Currency_Id!='0')):
                    $currency=Currency::model()->findByPk($plan->TM_PN_Additional_Currency_Id);
                    if($voucher->TM_CP_Type=='1'):                              
                         $percentage=$voucher->TM_CP_value/100;                 
                         $amount=$plan->TM_PN_Additional_Rate*$percentage;
                         $amount=round($amount,2,PHP_ROUND_HALF_UP);
                         $couponamount=$amount.' '.$currency->TM_CR_Code;
                         $rate=$plan->TM_PN_Additional_Rate-$amount;
                         $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;
                    elseif($voucher->TM_CP_Type=='0'):                
                        $rate=$plan->TM_PN_Additional_Rate-$voucher->TM_CP_value;
                        $couponamount=$voucher->TM_CP_value.' '.$currency->TM_CR_Code; 

                        $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;
                    endif; 				
				endif;
                
                $returnarray['voucher']=$voucher->TM_CP_Id;
                $returnarray['error']='no';
                $returnarray['message']='<div class="alert alert-success" role="alert">'.$couponamount.' Has been redeemed</div>';				
            else:
                $returnarray['rate']=$plan->TM_PN_Rate.' '.$currency->TM_CR_Code;
                $returnarray['error']='<div class="alert alert-danger" role="alert">Not a valid voucher</div>'; 
            endif;

            echo json_encode($returnarray);
        }
        elseif($_POST['plan']!='' & $_POST['voucher']=='' & ($_POST['school']!='' || $_POST['school']!='0'))
        {
            $location=User::model()->findByPk(Yii::app()->user->id)->location;
			$returnarray=array();
            $plan=Plan::model()->with('currency')->with('additional')->findByPk($_POST['plan']);
            $school=School::model()->findByPk($_POST['school']);
            if($school->TM_SCL_Discount)
            {
                if($location=='0' & $plan->TM_PN_Currency_Id=='1'):
                	$currency=Currency::model()->findByPk($plan->TM_PN_Currency_Id);
                	if($school->TM_SCL_Discount_Type=='1'):                              
                		$percentage=$school->TM_SCL_Discount_Amount/100;                 
                		$amount=$plan->TM_PN_Rate*$percentage;
                		$amount=round($amount,2,PHP_ROUND_HALF_UP);
                		$couponamount=$amount.' '.$currency->TM_CR_Code;
                		$rate=$plan->TM_PN_Rate-$amount;
                		$returnarray['rate']=$rate.' '.$currency->TM_CR_Code;	                         
                	elseif($school->TM_SCL_Discount_Type=='0'):   
                		$rate=$plan->TM_PN_Rate-$school->TM_SCL_Discount_Amount;
                		$couponamount=$school->TM_SCL_Discount_Amount.' '.$currency->TM_CR_Code; 
                		$returnarray['rate']=$rate.' '.$currency->TM_CR_Code;					                                                
                	endif;				
                elseif($location=='0' & $plan->TM_PN_Additional_Currency_Id=='1'):
                	$currency=Currency::model()->findByPk($plan->TM_PN_Additional_Currency_Id);
                	if($school->TM_SCL_Discount_Type=='1'):                              
                		 $percentage=$school->TM_SCL_Discount_Amount/100;                 
                		 $amount=$plan->TM_PN_Additional_Rate*$percentage;
                		 $amount=round($amount,2,PHP_ROUND_HALF_UP);
                		 $couponamount=$amount.' '.$currency->TM_CR_Code;
                		 $rate=$plan->TM_PN_Additional_Rate-$amount;
                		 $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;						 
                	elseif($school->TM_SCL_Discount_Type=='0'):                
                		$rate=$plan->TM_PN_Additional_Rate-$school->TM_SCL_Discount_Amount;
                		$couponamount=$school->TM_SCL_Discount_Amount.' '.$currency->TM_CR_Code;
                		$returnarray['rate']=$rate.' '.$currency->TM_CR_Code; 
                	endif;				
                elseif($location=='1' & $plan->TM_PN_Currency_Id!='1'):
                	$currency=Currency::model()->findByPk($plan->TM_PN_Currency_Id);
                
                
                	if($school->TM_SCL_Discount_Type=='1'):                              
                		 $percentage=$school->TM_SCL_Discount_Amount/100;                 
                		 $amount=$plan->TM_PN_Rate*$percentage;
                		 $couponamount=$amount.' '.$currency->TM_CR_Code;
                		 $rate=$plan->TM_PN_Rate-$amount;
                		 $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;
                	elseif($school->TM_SCL_Discount_Type=='0'):                
                		$rate=$plan->TM_PN_Rate-$school->TM_SCL_Discount_Amount;
                		$couponamount=$school->TM_SCL_Discount_Amount.' '.$currency->TM_CR_Code;
                		$returnarray['rate']=$rate.' '.$currency->TM_CR_Code; 
                	endif; 				
                elseif($location=='1' & ($plan->TM_PN_Additional_Currency_Id!='1' || $plan->TM_PN_Additional_Currency_Id!='0')):
                	$currency=Currency::model()->findByPk($plan->TM_PN_Additional_Currency_Id);
                	if($school->TM_SCL_Discount_Type=='1'):                              
                		 $percentage=$school->TM_SCL_Discount_Amount/100;                 
                		 $amount=$plan->TM_PN_Additional_Rate*$percentage;
                		 $amount=round($amount,2,PHP_ROUND_HALF_UP);
                		 $couponamount=$amount.' '.$currency->TM_CR_Code;
                		 $rate=$plan->TM_PN_Additional_Rate-$amount;
                		 $returnarray['rate']=$rate.' '.$currency->TM_CR_Code;
                	elseif($school->TM_SCL_Discount_Type=='0'):                
                		$rate=$plan->TM_PN_Additional_Rate-$school->TM_SCL_Discount_Amount;
                		$couponamount=$school->TM_SCL_Discount_Amount.' '.$currency->TM_CR_Code; 
                		$returnarray['rate']=$rate.' '.$currency->TM_CR_Code;
                	endif; 				
                endif;                
            }
            else
            {
                $returnarray['rate']=$plan->TM_PN_Rate.' '.$currency->TM_CR_Code;
                $returnarray['error']='no';                
            }
            echo json_encode($returnarray);            
        }
        
    }
	/**
	 * Performs the AJAX validation.
	 * @param Coupons $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='coupons-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
