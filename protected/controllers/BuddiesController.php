<?php

class BuddiesController extends Controller
{
    public $layout = '//layouts/main';

    // Uncomment the following methods and override them if needed
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Buddylist','GetBuddies','GetBuddyRequests','GetMyBuddies','Invitebuddies','BuddySearch','AddBuddy','RejectBuddy','AddNew','removeBuddy'),
                'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudent()'
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            )
        );
    }
    public function actionBuddylist()
    {

        $buddies = Buddies::model()->findAll(array('condition' => "TM_BD_Connection_Id='" . Yii::app()->user->id . "' AND TM_BD_Status='0'"));
        $this->render('home', array('buddies' => $buddies));
    }

    public function actionGetBuddies()
    {


    }

    public function actionGetBuddyRequests()
    {
        $buddies = Buddies::model()->findAll(array('condition' => "(TM_BD_Student_Id='" . Yii::app()->user->id . "' AND TM_BD_Status='1') OR (TM_BD_Connection_Id='" . Yii::app()->user->id . "' AND TM_BD_Status='1')"));
        $html = '';
        if(count($buddies)>0):

        foreach ($buddies AS $buddyname):


                if($buddyname->TM_BD_Connection_Id==Yii::app()->user->id):
                    $buddyuser=User::model()->findbyPk($buddyname->TM_BD_Student_Id);
                    $studentimage=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$buddyname->TM_BD_Student_Id));


                    $html .= '<li href="#" class="list-group-item col-md-4 mrgbotm1 brdno text-left buddy'.$buddyname->TM_BD_Id.'">
                                    <img class="img-thumbnail img-circle" src="'.Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($studentimage->TM_STU_Image!=''?$studentimage->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140">
                   <label class="fno">'.$buddyuser->profile->firstname.' '.$buddyuser->profile->lastname.'<br>'.$buddyuser->username.'</label>';
                    $html .= '<div class="bottom_button">
							<span class="label label-success labelposition">Request sent</span>
                             <a class="btn btn-warning btn-outline btn-xs rejectbuddy mybutton1" data-parent="buddy'.$buddyname->TM_BD_Id.'" data-id="'.$buddyname->TM_BD_Id.'">Cancel</a></div></li>';                
                else:
                    $buddyuser=User::model()->findbyPk($buddyname->TM_BD_Connection_Id);
                    $studentimage=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$buddyname->TM_BD_Connection_Id));

                    $html .= '<li href="#" class="list-group-item col-md-4 mrgbotm1 brdno text-left buddy'.$buddyname->TM_BD_Id.'">
                                    <img class="img-thumbnail img-circle" src="'.Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($studentimage->TM_STU_Image!=''?$studentimage->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140">
                   <label class="fno">'.$buddyuser->profile->firstname.' '.$buddyuser->profile->lastname.'<br>'.$buddyuser->username.'</label>';
                    $html .= '<div class="bottom_button">
                    <a class="btn btn-warning btn-outline btn-xs mybutton1 confirmbuddy" data-parent="buddy'.$buddyname->TM_BD_Id.'" data-id="'.$buddyname->TM_BD_Id.'">Confirm</a>
                    <a class="btn btn-warning btn-outline btn-xs mybutton1 rejectbuddy" data-parent="buddy'.$buddyname->TM_BD_Id.'" data-id="'.$buddyname->TM_BD_Id.'">Reject</a>
                  </div></li>';
                endif;
            endforeach;

        else:
            $html='<li href="#" class="list-group-item col-md-4 brdno text-left">
                            <!--<img class="img-thumbnail img-circle" src="http://bootdey.com/img/Content/user_1.jpg">-->
                            <label class="fno"> No pending requests found</label>
                        </li>';
        endif;
        echo $html;

    }
    public function actionGetMyBuddies()
    {
        $buddies = Buddies::model()->findAll(array('condition' => "TM_BD_Connection_Id='" . Yii::app()->user->id . "' AND TM_BD_Status='0'"));
        $html = '';
        if(count($buddies)>0):
            foreach ($buddies AS $buddyname):
                $buddyuser=User::model()->findbyPk($buddyname->TM_BD_Student_Id);
                $studentimage=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$buddyname->TM_BD_Student_Id));           
                $html .= '<li href="#" class="list-group-item col-md-4 mrgbotm1 brdno text-left buddy'.$buddyname->TM_BD_Id.'">
                                <img class="img-thumbnail img-circle" src="'.Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($studentimage->TM_STU_Image!=''?$studentimage->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140">
                                <label class="fno">'.$buddyuser->profile->firstname.' '.$buddyuser->profile->lastname.'<br>'.$buddyuser->username.'</label>';
                if($buddyname->TM_BD_Status=='0'):
                    $html .= '<div class="bottom_button">
								<a class="btn btn-warning btn-outline btn-xs removebuddy mybutton1" data-parent="buddy'.$buddyname->TM_BD_Id.'" data-id="'.$buddyname->TM_BD_Id.'">Remove</a></div></li>';
                else:
                    $html .= '<div class="bottom_button">								
								<span class="label label-success labelposition">Request sent</span>  
                             <a class="btn btn-warning btn-outline btn-xs rejectbuddy mybutton1" data-parent="buddy'.$buddyname->TM_BD_Id.'" data-id="'.$buddyname->TM_BD_Id.'">Cancel</a></div></li>';
                endif;
            endforeach;
        else:
            $html .= '<li href="#" class="list-group-item col-md-4 brdno text-left">
                            <!--<img class="img-thumbnail img-circle" src="http://bootdey.com/img/Content/user_1.jpg">-->
                            <label class="fno"> No buddies found</label>
                        </li>';
        endif;

        echo $html;

    }

    public function actionBuddySearch()
    {

        if (isset($_POST['buddyname']))
            $buddies = User::model()->findAll(array('condition' => "username LIKE'" . $_POST['buddyname'] . "%' AND usertype='6' AND id!=".Yii::app()->user->id));
            //$buddienames = Student::model()->findAll(array('condition' => "TM_STU_First_Name LIKE'" . $_POST['buddyname'] . "%' AND usertype='6' AND id!=".Yii::app()->user->id));

       /* $query = new Query;

        $query  ->select(['SELECT *  '])

            ->from('tm_users  a')

            ->join(  'JOIN',

                'tm_student  b',

                'tm_student.TM_STU_User_Id =tm_users.id',
                'a.username LIKE" . $_POST["buddyname"] . "% AND usertype="6" AND id!=".Yii::app()->user->id',
					'b.TM_STU_First_Name LIKE" . $_POST["buddyname"] . "%'

	            );

	$command = $query->createCommand();

	$data = $command->queryAll();
        print_r($data);*/




        if (count($buddies) != 0) {
            $html = '';
            foreach ($buddies AS $buddyname):

                $buddy= Buddies::model()->find(array('condition' => "(TM_BD_Student_Id='" . Yii::app()->user->id . "' AND TM_BD_Connection_Id=".$buddyname->id.") OR TM_BD_Student_Id='" . $buddyname->id . "' AND TM_BD_Connection_Id=".Yii::app()->user->id.""));
                if(count($buddy)!=0 & ($buddy->TM_BD_Status==2 & $buddy->TM_BD_Connection_Id==Yii::app()->user->id)):
                    
                else:
                    $studentimage=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$buddyname->id));                 
                    $html .= '<li href="#" class="list-group-item col-md-4 mrgbotm1 brdno text-left buddy'.$buddyname->id.'">
                                <img class="img-thumbnail img-circle" src="'.Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($studentimage->TM_STU_Image!=''?$studentimage->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140">
                                <label class="fno">'.$buddyname->profile->firstname.' '.$buddyname->profile->lastname.'<br>'.$buddyname->username.'</label>';
                    if(count($buddy)!=0)
                    {
                        if($buddy->TM_BD_Status!='2'):
                            if($buddy->TM_BD_Student_Id==Yii::app()->user->id & $buddy->TM_BD_Status=='1'):
								$html.='<div class="bottom_button">										
										<a class="btn btn-warning btn-outline btn-xs confirmbuddy mybutton1" data-parent="buddy'.$buddyname->id.'" data-id="'.$buddy->TM_BD_Id.'">Confirm</a> 
										<a class="btn btn-warning btn-outline btn-xs rejectbuddy mybutton1" data-parent="buddy'.$buddyname->id.'" data-id="'.$buddy->TM_BD_Id.'">Cancel</a> 
									</div></li>';
                            elseif($buddy->TM_BD_Student_Id==$buddyname->id & $buddy->TM_BD_Status=='1'):								
                                $html .= '<div class="bottom_button">
											<span class="label label-success labelposition">Request sent</span>
											<a class="btn btn-warning btn-outline btn-xs rejectbuddy mybutton1" data-parent="buddy'.$buddyname->id.'" data-id="'.$buddy->TM_BD_Id.'">Cancel</a>
										</div>
										</li>';
                            else:
                                $html .= '<div class="bottom_button">
											<a class="btn btn-warning btn-outline btn-xs removebuddy mybutton1" data-parent="buddy'.$buddyname->id.'" data-id="'.$buddy->TM_BD_Id.'">Remove</a>
										</div></li>';
                            endif;
                        else:
                            $html .= '<div class="bottom_button">
								<a class="btn btn-warning btn-outline btn-xs addnew mybutton1" data-parent="buddy'.$buddyname->id.'" data-id="'.$buddyname->id.'">Add Buddy</a>
                         </div></li>';
                        endif;
                    }
                    else
                    {
                        $html .= '<div class="bottom_button">
                         <a class="btn btn-warning btn-outline btn-xs addnew mybutton1" data-parent="buddy'.$buddyname->id.'" data-id="'.$buddyname->id.'">Add Buddy</a>
                         </div></li>';
    
                    }
                endif;

            endforeach;

            echo $html;
        } else {

            $html= '<li href="#" class="list-group-item col-md-4 brdno text-left">
                            <!--<img class="img-thumbnail img-circle" src="http://bootdey.com/img/Content/user_1.jpg">-->
                            <label class="fno"> No such user found</label>
                        </li>';
            echo $html;
        }


    }
    public function actionAddBuddy(){
        $buddyid = $_POST["buddyid"];
        $buddy=Buddies::model()->findByPk($buddyid);
        $buddy->TM_BD_Status=0;
        if($buddy->save(false)):
            $rejected=Buddies::model()->find(array('condition'=>'TM_BD_Connection_Id='.$buddy->TM_BD_Student_Id.' AND TM_BD_Student_Id='.$buddy->TM_BD_Connection_Id));
            if(count($rejected)>0):
                $rejected->delete();
            endif;
            $newbuddy= new Buddies();
            $newbuddy->TM_BD_Connection_Id=$buddy->TM_BD_Student_Id;
            $newbuddy->TM_BD_Student_Id=$buddy->TM_BD_Connection_Id;
            $newbuddy->TM_BD_Status='0';
            $newbuddy->save(false);
            $notification=new Notifications();
            $notification->TM_NT_User=$buddy->TM_BD_Connection_Id;
            $notification->TM_NT_Type='Approve';
            $notification->TM_NT_Target_Id=$buddy->TM_BD_Student_Id;
            $notification->TM_NT_Status='0';
            $notification->save(false);
            echo "yes";
        endif;
    }
    public function actionRejectBuddy(){
        $buddyid = $_POST["buddyid"];
        $buddy=Buddies::model()->findByPk($buddyid);
        $buddy->TM_BD_Status='2';
        $buddy->save(false);
        echo "yes";
    }
    public function actionremoveBuddy(){
        $buddyid = $_POST["buddyid"];
        $buddy=Buddies::model()->findByPk($buddyid);
        $buddylist=Buddies::model()->findAll(array('condition'=>'(TM_BD_Student_Id=:buddy1 AND TM_BD_Connection_Id=:buddy2) OR (TM_BD_Student_Id=:buddy2 AND TM_BD_Connection_Id=:buddy1)','params'=>array(':buddy1'=>$buddy->TM_BD_Student_Id,'buddy2'=>$buddy->TM_BD_Connection_Id)));
        foreach($buddylist AS $list):
            $list->delete();
        endforeach;
        echo "yes";
    }
    public function actionAddNew(){
        $buddy=new Buddies();
        $buddy->TM_BD_Connection_Id=Yii::app()->user->id;
        $buddy->TM_BD_Student_Id = $_POST["buddyid"];
        $buddy->TM_BD_Status=1;
        if($buddy->save(false)):
            $notification=new Notifications();
            $notification->TM_NT_User=$_POST["buddyid"];
            $notification->TM_NT_Type='Request';
            $notification->TM_NT_Target_Id=Yii::app()->user->id;
            $notification->TM_NT_Status='0';
            $notification->save(false);
            echo $buddy->TM_BD_Id;
        endif;

    }
}