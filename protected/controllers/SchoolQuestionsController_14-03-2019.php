<?php

class SchoolQuestionsController extends Controller
{
    
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request			
			//'postOnly + copy', // we only allow copy via POST request
		);
	}    
    public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','importquestions','Addquestions','systemquestions','Hidequestions','create','Gettopiclist','Uploadimage','UploadimageTemp','Deleteimagetemp','Deleteimage','View','update','Copy','Delete','Deletequestion'),
                'users'=>array('@'),
                'expression'=>'UserModule::isAllowedSchool()'				
			),  
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','importquestions','Addquestions','systemquestions','Hidequestions','create','Uploadimage','UploadimageTemp','Deleteimagetemp','Deleteimage','View','update','Copy','Delete','Deletequestion'),
                'users'=>array('@'),
                'expression'=>'UserModule::isTeacherAllowed()'				
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('Gettopiclist'),
                'users'=>array('@'),
                'expression'=>'UserModule::isTeacher()'				
			),             
                                 
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','importquestions','Addquestions','systemquestions','Hidequestions','create','Gettopiclist','Uploadimage','UploadimageTemp','Deleteimagetemp','Deleteimage','View','update','Copy','Delete','Deletequestion'),
                'users'=>array('@'),
                'expression'=>'UserModule::isSchoolStaffAdmin($_GET["id"])'
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('Gettopiclist','uploadimagetemp','deleteimage'),
                'users'=>array('@'),
            ),
                              
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}    
    
    public function actionAdmin($id)
	{
         
        $usermode=Yii::app()->session['mode'];
            $standards=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId)','condition'=>'TM_SPN_SchoolId='.$id));        
            $planstandards='';
            foreach($standards AS $key=>$standard):
                $planstandards.=($key==0?$standard->TM_SPN_StandardId:','.$standard->TM_SPN_StandardId);
            endforeach;        
        if(Yii::app()->user->isSchoolAdmin() || $usermode=='TeacherAdmin'):            
            $this->layout = '//layouts/schoolcolumn1';                       
        elseif(Yii::app()->user->isTeacher()):            
            $this->layout = '//layouts/teacher';
            $standards=TeacherStandards::model()->findAll(array('select'=>'DISTINCT(TM_TE_ST_Standard_Id)','condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));
            $planstandards='';
            foreach($standards AS $key=>$standard):
                $planstandards.=($key==0?$standard->TM_TE_ST_Standard_Id:','.$standard->TM_TE_ST_Standard_Id);
            endforeach;                       
        endif; 
		$model=new Questions('search');
        $model->scenario = 'search';
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Questions']))
			$model->attributes=$_GET['Questions'];
        $user=User::model()->findbyPk(Yii::app()->user->id);
        $teacher=$user->usertype;
        if(isset($_GET['search'])):
            $questiontype=$_GET['createdby'];
        endif;
        if($teacher==9 || $usermode=='Teacher'):
            $type=$model->teacher($id,$questiontype);
        else:
            $type=$model->searchschool($id,$questiontype);
        endif;       
		$this->render('admin',array(
			'model'=>$model,
            'id'=>$id,
            'teacher'=>$teacher,
            'usermode'=>$usermode,
            'type'=>$type,
            'planstandards'=>$planstandards
		));
	}
    public function actionImportquestions($id)
    {
        $usermode=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin() || $usermode=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn1';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';               
        endif;
        $standards=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId)','condition'=>'TM_SPN_SchoolId='.$id));
        $planstandards='';
        foreach($standards AS $key=>$standard):
            $planstandards.=($key==0?$standard->TM_SPN_StandardId:','.$standard->TM_SPN_StandardId);
        endforeach;
        $model=new Questions('search');
        $model->scenario = 'search';
        $model->unsetAttributes();
        if(isset($_GET['Questions']))
            $model->attributes=$_GET['Questions'];
        //$model=new Questions('searchmastermanage');
		$this->render('import',array(
			'model'=>$model,
            'id'=>$id,
            'planstandards'=>$planstandards
		));             
    }
    public function actionSystemquestions($id)
    {
        $usermode=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin() || $usermode=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn1';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';               
        endif;
        $standards=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId)','condition'=>'TM_SPN_SchoolId='.$id));
        $planstandards='';
        foreach($standards AS $key=>$standard):
            $planstandards.=($key==0?$standard->TM_SPN_StandardId:','.$standard->TM_SPN_StandardId);
        endforeach;
        $model=new Questions('search');
        $model->scenario = 'search';
        $model->unsetAttributes();
        if(isset($_GET['Questions']))
            $model->attributes=$_GET['Questions'];
        //$model=new Questions('searchmastermanage');
		$this->render('managesystemquestions',array(
			'model'=>$model,
            'id'=>$id,
            'planstandards'=>$planstandards
		));             
    }
    public function actionAddquestions($id)
    {
        $arr = explode(',', $_POST['theIds']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_SQ_Question_Id' ,$arr );
        $criteria->addCondition('TM_SQ_School_Id='.$id);
        $model = SchoolQuestions::model()->findAll($criteria);
        foreach ($model as $value) {
              $value->TM_SQ_ImportStatus='0';
              $value->save(false);                 
        }
    }
    public function actionHidequestions($id)
    {
        $arr = explode(',', $_POST['theIds']);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('TM_SQ_Question_Id' ,$arr );
        $criteria->addCondition('TM_SQ_School_Id='.$id);
        $model = SchoolQuestions::model()->findAll($criteria);       
        foreach ($model as $value) {
              $value->TM_SQ_ImportStatus='1';
              $value->save(false);                 
        }
    }  
    
    public function actionCreate($id)
	{
        $usermode=Yii::app()->session['mode'];
            $standards=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId)','condition'=>'TM_SPN_SchoolId='.$id));
            $planstandards='';
            foreach($standards AS $key=>$standard):
                $planstandards.=($key==0?$standard->TM_SPN_StandardId:','.$standard->TM_SPN_StandardId);
            endforeach;         
        if(Yii::app()->user->isSchoolAdmin() || $usermode=='TeacherAdmin'):            
            $this->layout = '//layouts/schoolcolumn1';
            
        elseif(Yii::app()->user->isTeacher()):            
            $this->layout = '//layouts/teacher';
            $standards=TeacherStandards::model()->findAll(array('select'=>'DISTINCT(TM_TE_ST_Standard_Id)','condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));
            $planstandards='';
            foreach($standards AS $key=>$standard):
                $planstandards.=($key==0?$standard->TM_TE_ST_Standard_Id:','.$standard->TM_TE_ST_Standard_Id);
            endforeach;                       
        endif;      
        $model=new Questions;
        $model->TM_QN_Dificulty_Id=2;
        $school=School::model()->findByPk($id);        
               
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Questions']))
		{
            //print_r($_POST['Questions']); exit;
            $model->attributes=$_POST['Questions'];
            if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionchoice'];

                $model->TM_QN_Option_Count=$_POST['Questions']['TM_QN_Option_Countchoice'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='3')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTF'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;

            }
            elseif($model->TM_QN_Type_Id=='4')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTEXT'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='5')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionpart'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='6')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionPaperOnly'];
                $model->TM_QN_Totalmarks=$_POST['Papermarks'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='7')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionMPPaperOnly'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            $tempimagesol=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'solution',':section'=>'main')));
            if($tempimagesol->image!=''):
                $model->TM_QN_Solution_Image=$tempimagesol->image;
            endif;
            $model->TM_QN_CreatedBy=Yii::app()->user->id;
            //$model->TM_QN_QuestionReff=rand(11111111, 99999999);
            /*$model->TM_QN_UpdatedBy=Yii::app()->user->id;
            $model->TM_QN_UpdatedOn=date('Y-m-d');*/
			if($model->save())
            {
                
                // insert into answers table
                $total=0;
                if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
                {

                    for($i=0;$i<$model->TM_QN_Option_Count;$i++)
                    {

                        $identifier=$i+1;
                        $answer=new Answers();
                        $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                        $answer->TM_AR_Answer=$_POST['answer'][$i];
                        if(isset($_POST['correctanswer'.$identifier]) & $model->TM_QN_Type_Id=='1')
                        {
                            $answer->TM_AR_Correct=$_POST['correctanswer'.$identifier];
                            $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                            $total+=$_POST['marks'.$identifier];
                        }
                        elseif($model->TM_QN_Type_Id=='2' & isset($_POST['correctanswer']))
                        {
                            if($_POST['correctanswer']==$identifier)
                            {
                                $answer->TM_AR_Correct='1';
                                $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                $total+= $_POST['marks'.$identifier];
                            }
                            else
                            {
                                $answer->TM_AR_Correct='0';
                                $answer->TM_AR_Marks='0';
                            }

                        }
                        $tempimageanswer=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier)));
                        if($tempimageanswer->image!=''):
                            $answer->TM_AR_Image=$tempimageanswer->image;
                        endif;
                        $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                        $answer->TM_AR_Status='0';
                        $answer->save(false);
                    }

                }
                elseif($model->TM_QN_Type_Id=='3')
                {
                    $answer=new Answers();
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='True';
                    if($_POST['correctanswerTF']=='True')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                        $total+=$_POST['marksTF'];
                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                    $answer=new Answers();
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='False';
                    if($_POST['correctanswerTF']=='False')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                        $total+=$_POST['marksTF'];
                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='4')
                {
                    $answer=new Answers();
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer=$_POST['answeroptions'];
                    $answer->TM_AR_Marks=$_POST['marksTEXT'];
                    $total+=$_POST['marksTEXT'];
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='5')
                {
                    for($i=0;$i<$_POST['multipleqstnum'];$i++)
                    {
                        $identifier=$i+1;
                        $question=new Questions();
                        $question->TM_QN_Parent_Id=$model->TM_QN_Id;
                        $question->TM_QN_Publisher_Id=$model->TM_QN_Publisher_Id;
                        $question->TM_QN_School_Id=$model->TM_QN_School_Id;                        
                        $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                        $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                        $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                        $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                        $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                        $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;
                        $question->TM_QN_Type_Id=$_POST['multypartType'][$i];
                        $question->TM_QN_Question=$_POST['multypartQuestions'][$i];
                        $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                        if($tempimagechild->image!=''):
                            $question->TM_QN_Image=$tempimagechild->image;
                        endif;

                        if($_POST['multypartType'][$i]=='1' | $_POST['multypartType'][$i]=='2')
                        {
                            $question->TM_QN_Option_Count=$model->TM_QN_Option_Count;
                        }
                        elseif($_POST['multypartType'][$i]=='4')
                        {
                            $idcount=$i+1;
                            if(isset($_POST['Editor'.$idcount])):
                                $question->TM_QN_Editor=$_POST['Editor'.$idcount];
                            endif;
                        }
                        $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                        $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                        $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                        $question->TM_QN_Status=$model->TM_QN_Status;
                        if($question->save(false))
                        {

                            $identifier=$i+1;
                            if($_POST['multypartType'][$i]=='1')
                            {

                                for($j=0;$j<$_POST['multypartOptions'.$identifier];$j++)
                                {
                                    $multiidenti=$j+1;
                                    $answer=new Answers();
                                    $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                    $answer->TM_AR_Answer=$_POST['answer'.$identifier][$j];
                                    if(isset($_POST['correctanswer'.$identifier.$multiidenti]))
                                    {
                                        $answer->TM_AR_Correct=$_POST['correctanswer'.$identifier.$multiidenti];
                                        $answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
                                        $total+=$_POST['marks'.$identifier.$multiidenti];
                                    }
                                    $tempimagechildans=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier.$multiidenti)));
                                    if($tempimagechildans->image!=''):
                                        $answer->TM_AR_Image=$tempimagechildans->image;
                                    endif;


                                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                    $answer->TM_AR_Status='0';
                                    $answer->save(false);
                                }
                            }

                            if($_POST['multypartType'][$i]=='2')
                            {

                                for($j=0;$j<$_POST['multypartOptions'.$identifier];$j++)
                                {
                                    $multiidenti=$j+1;
                                    $answer=new Answers();
                                    $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                    $answer->TM_AR_Answer=$_POST['answer'.$identifier][$j];
                                    if($_POST['correctanswer'.$identifier]==$multiidenti)
                                    {
                                        $answer->TM_AR_Correct='1';
                                        $answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
                                        $total+=$_POST['marks'.$identifier.$multiidenti];
                                    }
                                    $tempimagechildans=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier.$multiidenti)));
                                    if($tempimagechildans->image!=''):
                                        $answer->TM_AR_Image=$tempimagechildans->image;
                                    endif;


                                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                    $answer->TM_AR_Status='0';
                                    $answer->save(false);
                                }
                            }                            
                            elseif($_POST['multypartType'][$i]=='3')
                            {
                                $answer=new Answers();
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='True';
                                if($_POST['correctanswer'.$identifier]=='True')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                                $answer=new Answers();
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='False';
                                if($_POST['correctanswer'.$identifier]=='False')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                    $total+=$_POST['marks'.$identifier];
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                            elseif($_POST['multypartType'][$i]=='4')
                            {
                                $identifier=$i+1;
                                $answer=new Answers();
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer=$_POST['answeroptions'.$identifier];
                                $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                $total+=$_POST['marks'.$identifier];
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                        }


                    }
                }
                elseif($model->TM_QN_Type_Id=='6')
                {
                    $total+=$_POST['Papermarks'];
                }
                elseif($model->TM_QN_Type_Id=='7')
                {
                   for($i=0;$i<$_POST['PoMPqstnum'];$i++)
                   {
                       $identifier=$i+1;
                       $question=new Questions();
                       $question->TM_QN_Parent_Id=$model->TM_QN_Id;
                       $question->TM_QN_Publisher_Id=$model->TM_QN_Publisher_Id;
                       $question->TM_QN_School_Id=$model->TM_QN_School_Id;                       
                       $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                       $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                       $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                       $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                       $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                       $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;
                       $question->TM_QN_Type_Id=$model->TM_QN_Type_Id;
                       $question->TM_QN_Question=$_POST['PapermultypartQuestions'][$i];
                       $question->TM_QN_Solutions=$model->TM_QN_Solutions;
                       $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                       $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                       $question->TM_QN_Totalmarks=$_POST['PaperMPmarks'][$i];
                       $total+=$_POST['PaperMPmarks'][$i];
                       $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                       $question->TM_QN_Status=$model->TM_QN_Status;

                       $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                       if($tempimagechild->image!=''):
                           $question->TM_QN_Image=$tempimagechild->image;
                       endif;
                       $question->save(false);
                   }
                }
                //update the totalMark into table questions
                Questions::model()->updateByPk($model->TM_QN_Id, array(
                    'TM_QN_Totalmarks' => $total
                ));
                
                $question=new SchoolQuestions();
                $question->TM_SQ_School_Id=$id;
                $question->TM_SQ_Question_Type=$model->TM_QN_Type_Id;
                $question->TM_SQ_Question_Id=$model->TM_QN_Id;
                $question->TM_SQ_Standard_Id=$model->TM_QN_Standard_Id;
                $question->TM_SQ_Chapter_Id=$model->TM_QN_Topic_Id;
                $question->TM_SQ_Topic_Id=$model->TM_QN_Section_Id;
                $question->TM_SQ_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                $question->TM_SQ_Publisher_id=$model->TM_QN_Publisher_Id;
                $question->TM_SQ_Type='0';
                $question->TM_SQ_Status='0';
                $question->save(false);                 
                $criteria = new CDbCriteria;
                $criteria->addCondition('tempid='.$_POST['imagetempid']);
                $tempdelete = Imagetemp::model()->deleteAll($criteria);
                $this->redirect(array('view','id'=>$id,'question'=>$model->TM_QN_Id));

            }

		}

		$this->render('create',array(
			'model'=>$model,
            'id'=>$id,
            'school'=>$school,
            'standards'=>$planstandards
		));
	}    
    public function actionGettopiclist()
    {
        if(isset($_POST['id']))
        {
            $data=Topic::model()->findAll("TM_SN_Topic_Id=:chapter AND TM_SN_Status='0' ORDER BY TM_SN_order ASC",
                array(':chapter'=>(int) $_POST['id']));
            $data=CHtml::listData($data,'TM_SN_Id','TM_SN_Name');
            echo CHtml::tag('option',array('value'=>''),'Select Topic',true);
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$value),CHtml::encode($name),true);
            }
        }
    }
    public function actionDeleteimage()
    {
        if(isset($_POST['id'])):
            if($_POST['type']=='question')
            {
                $question=Questions::model()->findByPk($_POST['id']);  
                $image=$question->TM_QN_Image;
                if(unlink(Yii::app()->basePath."/../images/questionimages/".$image) & unlink(Yii::app()->basePath."/../images/questionimages/thumbs/".$image) )
                {
                    $question->TM_QN_Image='';
                    $question->save(false);
                    echo "yes";
                }
            }
            elseif($_POST['type']=='answer')
            {
                $answer=Answers::model()->findByPk($_POST['id']);
                $image=$answer->TM_AR_Image;
                if(unlink(Yii::app()->basePath."/../images/answerimages/".$image) & unlink(Yii::app()->basePath."/../images/answerimages/thumbs/".$image) )
                {
                    $answer->TM_AR_Image='';
                    $answer->save(false);
                    echo "yes";
                }
            }
            elseif($_POST['type']=='solution')
            {
                $question=Questions::model()->findByPk($_POST['id']);                
                $image=$question->TM_QN_Solution_Image;
                if(unlink(Yii::app()->basePath."/../images/solutionimages/".$image) & unlink(Yii::app()->basePath."/../images/solutionimages/thumbs/".$image) )
                {
                    $question->TM_QN_Solution_Image='';
                    $question->save(false);
                    echo "yes";
                }
            }
        endif;
    }    
    public function actionDeleteimagetemp()
    {
        if(isset($_POST['imagetempid'])):
            $imagetemp=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
            if($_POST['type']=='question')
            {
                if(unlink(Yii::app()->basePath."/../images/questionimages/".$imagetemp->image) & unlink(Yii::app()->basePath."/../images/questionimages/thumbs/".$imagetemp->image) )
                {
                    Imagetemp::model()->deleteAll(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
                    echo "yes";
                }
            }
            elseif($_POST['type']=='answer')
            {
                if(unlink(Yii::app()->basePath."/../images/answerimages/".$imagetemp->image) & unlink(Yii::app()->basePath."/../images/answerimages/thumbs/".$imagetemp->image) )
                {
                    Imagetemp::model()->deleteAll(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
                    echo "yes";
                }
            }
            elseif($_POST['type']=='solution')
            {
                if(unlink(Yii::app()->basePath."/../images/solutionimages/".$imagetemp->image) & unlink(Yii::app()->basePath."/../images/solutionimages/thumbs/".$imagetemp->image) )
                {
                    Imagetemp::model()->deleteAll(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>$_POST['type'],':section'=>$_POST['section'])));
                    echo "yes";
                }
            }
        endif;
    }
    
    public function actionUploadimageTemp()
    {
        $fieldname=$_POST['imagefieldname'];
        $type=$_POST['imagetype'];
        $section=$_POST['imagesection'];
        $tempid=$_POST['imagetempid'];
        $tempimagecount=Imagetemp::model()->count(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        if($tempimagecount>0):
            $imagetemp=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        else:
            $imagetemp=new Imagetemp();
        endif;
        $imagetemp->type=$type;
        $imagetemp->section=$section;
        $imagetemp->tempid=$tempid;
        $path='';
        if($_FILES[$fieldname]["error"]!=4)
        {
            $temp = explode(".",$_FILES[$fieldname]["name"]);

            $imagename=md5($_FILES[$fieldname]['name'].rand(999,9999)). '.' .end($temp);
            switch($type){
                case 'question':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/questionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/questionimages/thumbs/'.$imagename);
                    }
                    $path='questionimages';
                break;
                case 'answer':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/answerimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/answerimages/thumbs/'.$imagename);
                    }
                    $path='answerimages';
                break;
                case 'solution':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/solutionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/solutionimages/thumbs/'.$imagename);
                    }
                    $path='solutionimages';
                break;
            }
            $imagetemp->image=$imagename;
            $imagetemp->save(false);
            echo Yii::app()->request->baseUrl."/images/".$path."/thumbs/" . $imagename;
        }
    }


    public function actionUploadimage()
    {
        $fieldname=$_POST['imagefieldname'];
        $type=$_POST['imagetype'];
        $section=$_POST['imagesection'];
        $tempid=$_POST['imagetempid'];
        $tempimagecount=Imagetemp::model()->count(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        if($tempimagecount>0):
            $imagetemp=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$tempid,':type'=>$type,':section'=>$section)));
        else:
            $imagetemp=new Imagetemp();
        endif;
        $imagetemp->type=$type;
        $imagetemp->section=$section;
        $imagetemp->tempid=$tempid;
        $path='';
        if($_FILES[$fieldname]["error"]!=4)
        {
            $temp = explode(".",$_FILES[$fieldname]["name"]);

            $imagename=md5($_FILES[$fieldname]['name'].rand(999,9999)). '.' .end($temp);
            switch($type){
                case 'question':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/questionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/questionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/questionimages/thumbs/'.$imagename);
                    }
                    $path='questionimages';
                    break;
                case 'answer':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/answerimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/answerimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/answerimages/thumbs/'.$imagename);
                    }
                    $path='answerimages';
                    break;
                case 'solution':
                    if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], Yii::app()->basePath."/../images/solutionimages/" . $imagename))
                    {

                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(800, 800)->quality(75);
                        $image->save();
                        Yii::import('application.extensions.image.Image');
                        $image = new Image('images/solutionimages/'.$imagename);
                        $image->resize(350,250);
                        $image->save('images/solutionimages/thumbs/'.$imagename);
                    }
                    $path='solutionimages';
                    break;
            }
            $imagetemp->image=$imagename;
            $imagetemp->save(false);
            echo Yii::app()->request->baseUrl."/images/".$path."/thumbs/" . $imagename;
        }
    } 
	public function actionView($id)
	{	
        $usermode=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin() || $usermode=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn1';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';
        endif;
        $user=User::model()->findByPk(Yii::app()->user->id);
        $this->render('view',array(
			'model'=>Questions::model()->findByPk($_GET['question']),
            'id'=>$id,
            'user'=>$user
		));
	} 
	public function actionUpdate($id)
	{
        $usermode=Yii::app()->session['mode'];
        if(Yii::app()->user->isSchoolAdmin() || $usermode=='TeacherAdmin'):
            $this->layout = '//layouts/schoolcolumn1';
        elseif(Yii::app()->user->isTeacher()):
            $this->layout = '//layouts/teacher';
        endif;		
        $model=Questions::model()->findByPk($_GET['question']);
        //print_r($model);exit;
        $school=School::model()->findByPk($id);
        $standards=SchoolPlan::model()->findAll(array('select'=>'DISTINCT(TM_SPN_StandardId)','condition'=>'TM_SPN_SchoolId='.$id));
        $planstandards='';
        foreach($standards AS $key=>$standard):
            $planstandards.=($key==0?$standard->TM_SPN_StandardId:','.$standard->TM_SPN_StandardId);
        endforeach; 
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
        $oldcount=$model->TM_QN_Option_Count;
        $oldtype=$model->TM_QN_Type_Id;
		if(isset($_POST['Questions']))
		{
			$model->attributes=$_POST['Questions'];
            if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
            {

                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionchoice'];
                $model->TM_QN_Option_Count=$_POST['Questions']['TM_QN_Option_Countchoice'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='3')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTF'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='4')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionTEXT'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='5')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_Questionpart'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;

            }
            elseif($model->TM_QN_Type_Id=='6')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionPaperOnly'];
                $model->TM_QN_Totalmarks=$_POST['Papermarks'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }
            elseif($model->TM_QN_Type_Id=='7')
            {
                $model->TM_QN_Question=$_POST['Questions']['TM_QN_QuestionMPPaperOnly'];
                $tempimage=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>'main')));
                if($tempimage->image!=''):
                    $model->TM_QN_Image=$tempimage->image;
                endif;
            }


            $tempimagesol=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'solution',':section'=>'main')));
            if($tempimagesol->image!=''):
                $model->TM_QN_Solution_Image=$tempimagesol->image;
            endif;
            $model->TM_QN_UpdatedOn=date('Y-m-d');
            $model->TM_QN_UpdatedBy=Yii::app()->user->id;
			if($model->save(false))
            {
                // insert into answers table
                if($model->TM_QN_Type_Id=='1' | $model->TM_QN_Type_Id=='2')
                {
                    $answerid=array();
                    for($i=0;$i<$model->TM_QN_Option_Count;$i++)
                    {

                        $identifier=$i+1;
                        if(isset($_POST['answerid'.$identifier])):
                            $answer=Answers::model()->findByPk($_POST['answerid'.$identifier]);
                        else:
                            $answer=new Answers();
                        endif;
                        $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                        $answer->TM_AR_Answer=$_POST['answer'][$i];
                        switch ($model->TM_QN_Type_Id) {
                            case "1":
                                if(isset($_POST['correctanswer'.$identifier]))
                                {
                                    $answer->TM_AR_Correct=$_POST['correctanswer'.$identifier];
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                break;
                            case "2":
                                if($_POST['correctanswer']==$identifier)
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                break;
                        }


                        $tempimageanswer=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier)));
                        if($tempimageanswer->image!=''):
                            $answer->TM_AR_Image=$tempimageanswer->image;
                        endif;
                        $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                        $answer->TM_AR_Status='0';
                        $answer->save(false);
                        $answerid[$i]=$answer->TM_AR_Id;
                    }
                    $criteria = new CDbCriteria;
                    $criteria->addNotInCondition('TM_AR_Id' ,$answerid );
                    $criteria->addCondition('TM_AR_Question_Id='.$model->TM_QN_Id);
                    $deleanswers = Answers::model()->findAll($criteria);
                    foreach ($deleanswers as $value) {
                        $value->delete();
                    }
                }
                elseif($model->TM_QN_Type_Id=='3')
                {
                    $answer=Answers::model()->findByPk($_POST['answeridTF1']);
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='True';
                    if($_POST['correctanswerTF']=='True')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                    }
                    else
                    {
                        $answer->TM_AR_Correct='0';
                        $answer->TM_AR_Marks='0';
                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                    $answer=Answers::model()->findByPk($_POST['answeridTF2']);
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer='False';
                    if($_POST['correctanswerTF']=='False')
                    {
                        $answer->TM_AR_Correct='1';
                        $answer->TM_AR_Marks=$_POST['marksTF'];
                    }

                    else
                    {
                        $answer->TM_AR_Correct='0';
                        $answer->TM_AR_Marks='0';
                    }
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='4')
                {
                    $answer=Answers::model()->findByPk($_POST['answeridTEXT']);
                    $answer->TM_AR_Question_Id=$model->TM_QN_Id;
                    $answer->TM_AR_Answer=$_POST['answeroptions'];
                    $answer->TM_AR_Marks=$_POST['marksTEXT'];
                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                    $answer->TM_AR_Status='0';
                    $answer->save(false);
                }
                elseif($model->TM_QN_Type_Id=='5')
                {
                    $childquestions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$id."'"));
                    $countchild=count($childquestions);                    
					for($i=0;$i<$_POST['multipleqstnum'];$i++)
                    {
                        
                        $identifier=$i+1;                        
                        $question=Questions::model()->findByPk($_POST['multyquestionid'.$identifier]);                        
						 if(isset($_POST['multyquestionid'.$identifier])):
                            $question=Questions::model()->findByPk($_POST['multyquestionid'.$identifier]);
                        else:
                            $question=new Questions();
							$question->TM_QN_Type_Id=$_POST['multypartType'.$identifier];
                        endif;                                               
                        $question->TM_QN_Parent_Id=$model->TM_QN_Id;                        
                        $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                        $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                        $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                        $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                        $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                        $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;

                        $question->TM_QN_Question=$_POST['multypartQuestions'.$identifier];

                        $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                        if($tempimagechild->image!=''):
                            $question->TM_QN_Image=$tempimagechild->image;
                        endif;
						if(!isset($_POST['multyquestionid'.$identifier])):
							if($_POST['multypartType'.$identifier]=='1' | $_POST['multypartType'.$identifier]=='2')
							{
								$question->TM_QN_Option_Count=$model->TM_QN_Option_Count;
							}
							elseif($_POST['multypartType'.$identifier]=='4')
							{
								$id=$i+1;
								if(isset($_POST['Editor'.$id])):
									$question->TM_QN_Editor=$_POST['Editor'.$id];
								endif;
							}
						endif;
                        $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                        $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                        $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                        $question->TM_QN_Status=$model->TM_QN_Status;
                        if($question->save(false))
                        {

							if(isset($_POST['multyquestionid'.$identifier])):
								$type=$question->TM_QN_Type_Id;
								$parts=count($question->answers);
							else:
								$type=$_POST['multypartType'.$identifier];
								$parts=$_POST['multypartOptions'.$identifier];
							endif;
                            if($type=='1' | $type=='2')
                            {

								for($j=0;$j<$parts;$j++)
                                {

                                    $multiidenti=$j+1;  
                                                            
									if(isset($_POST['answeridOPT'.$identifier.$multiidenti]))
									{
										$answer=Answers::model()->findByPk($_POST['answeridOPT'.$identifier.$multiidenti]);
									}
									else
									{
										$answer=new Answers();
									}

                                    $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                    $answer->TM_AR_Answer=$_POST['answer'.$identifier][$j];   
                                                                  
									if($type=='1')
									{
										if(isset($_POST['correctanswer'.$identifier.$multiidenti]))
										{

											$answer->TM_AR_Correct=$_POST['correctanswer'.$identifier.$multiidenti];
											$answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
										}
										else
										{
											$answer->TM_AR_Correct='0';
											$answer->TM_AR_Marks='0';
										}
									}
									else
									{
										if($_POST['correctanswer'.$identifier]==$answer->TM_AR_Id || $_POST['correctanswer'.$identifier]==$multiidenti)
										{

											$answer->TM_AR_Correct='1';
											$answer->TM_AR_Marks=$_POST['marks'.$identifier.$multiidenti];
										}
										else
										{
											$answer->TM_AR_Correct='0';
											$answer->TM_AR_Marks='0';
										}
									}
                                    $tempimagechildans=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'answer',':section'=>$identifier.$multiidenti)));
                                    if($tempimagechildans->image!=''):
                                        $answer->TM_AR_Image=$tempimagechildans->image;
                                    endif;
                                    $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                    $answer->TM_AR_Status='0';
                                    $answer->save(false);
                                }
                                
                            }
                            elseif($type=='3')
                            {
								if(isset($_POST['answeridTF'.$identifier.'2']))
								{
									$answer=Answers::model()->findByPk($_POST['answeridTF'.$identifier.'1']);
								}
								else
								{
									$answer=new Answers();
								}
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='True';
                                if($_POST['correctanswer'.$identifier]=='True')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
								if(isset($_POST['answeridTF'.$identifier.'2']))
								{
									$answer=Answers::model()->findByPk($_POST['answeridTF'.$identifier.'2']);
								}
								else
								{
									$answer=new Answers();
								}
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer='False';
                                if($_POST['correctanswer'.$identifier]=='False')
                                {
                                    $answer->TM_AR_Correct='1';
                                    $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                }
                                else
                                {
                                    $answer->TM_AR_Correct='0';
                                    $answer->TM_AR_Marks='0';
                                }
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                            elseif($type=='4')
                            {
                                $identifier=$i+1;
								if(isset($_POST['answeridTEXT'.$identifier]))
								{
									$answer=Answers::model()->findByPk($_POST['answeridTEXT'.$identifier]);
								}
								else
								{
									$answer=new Answers();
								}
                                $answer->TM_AR_Question_Id=$question->TM_QN_Id;
                                $answer->TM_AR_Answer=$_POST['answeroptions'.$identifier];
                                $answer->TM_AR_Marks=$_POST['marks'.$identifier];
                                $answer->TM_AR_CreatedBy=Yii::app()->user->id;
                                $answer->TM_AR_Status='0';
                                $answer->save(false);
                            }
                        }
                  


                    }                                         
                    /*if($countchild>$_POST['multipleqstnum']):
                        $deletecount=$countchild-$_POST['multipleqstnum'];
                     foreach($childquestions AS $childquestion):
    
                         if($deletecount<0){
    
                             $post=Questions::model()->findByPk($childquestion->TM_QN_Id ); // assuming there is a post whose ID is 10
                             $post->delete();
    
                         }
                        $deletecount--;

                     endforeach;

                    endif;*/

                }
                elseif($model->TM_QN_Type_Id=='7')
                {
                    $childquestions=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$id."'"));
                    $countchild=count($childquestions);


                    if($countchild>$_POST['PoMPqstnum']):
                        $deletecount=$countchild-$_POST['PoMPqstnum'];


                        foreach($childquestions AS $childquestion):

                            if($deletecount<=0){

                                $post=Questions::model()->findByPk($childquestion->TM_QN_Id ); // assuming there is a post whose ID is 10
                                $post->delete();


                            }

                            $deletecount--;

                        endforeach;

                    endif;
                    for($i=0;$i<$_POST['PoMPqstnum'];$i++)
                    {
                        $identifier=$i+1;
                        if(isset($_POST['papermultyquestionid'.$identifier])):
                            $question=Questions::model()->findByPk($_POST['papermultyquestionid'.$identifier]);
                        else:
                            $question=new Questions();
                        endif;
                        $question->TM_QN_Parent_Id=$model->TM_QN_Id;
                        $question->TM_QN_Publisher_Id=$model->TM_QN_Publisher_Id;
                        $question->TM_QN_Syllabus_Id=$model->TM_QN_Syllabus_Id;
                        $question->TM_QN_Standard_Id=$model->TM_QN_Standard_Id;
                        $question->TM_QN_Subject_Id=$model->TM_QN_Subject_Id;
                        $question->TM_QN_Topic_Id=$model->TM_QN_Topic_Id;
                        $question->TM_QN_Section_Id=$model->TM_QN_Section_Id;
                        $question->TM_QN_Dificulty_Id=$model->TM_QN_Dificulty_Id;
                        $question->TM_QN_Type_Id=$model->TM_QN_Type_Id;
                        $question->TM_QN_Question=$_POST['PapermultypartQuestions'][$i];
                        $question->TM_QN_Solutions=$model->TM_QN_Solutions;
                        $question->TM_QN_Teacher_Id=$model->TM_QN_Teacher_Id;
                        $question->TM_QN_Pattern=$model->TM_QN_Pattern;
                        $question->TM_QN_Totalmarks=$_POST['PaperMPmarks'][$i];
                        $question->TM_QN_CreatedBy=$model->TM_QN_CreatedBy;
                        $question->TM_QN_Status=$model->TM_QN_Status;
                        $tempimagechild=Imagetemp::model()->find(array('condition'=>'tempid=:temp AND type=:type AND section=:section','params'=>array(':temp'=>$_POST['imagetempid'],':type'=>'question',':section'=>$identifier)));
                        if($tempimagechild->image!=''):
                            $question->TM_QN_Image=$tempimagechild->image;
                        endif;
                        $question->save(false);

                    }
                }
            }
            $this->redirect(array('view','id'=>$id,'question'=>$model->TM_QN_Id));
		}

		$this->render('update',array(
			'model'=>$model,
            'id'=>$id,			               
            'school'=>$school,
            'standards'=>$planstandards
		));
	}
    public function actionCopy($id)
    {
        if(isset($_GET['question'])):
            $question=Questions::model()->findByPk($_GET['question']);
            $type=$question->TM_QN_Type_Id;
            $newquestion= new Questions();
            $newquestion->attributes=$question->attributes;
            $newquestion->TM_QN_Id=NULL;
            $newquestion->TM_QN_Question=$question->TM_QN_Question;
            $newquestion->TM_QN_Option_Count=$question->TM_QN_Option_Count;
            $newquestion->TM_QN_CreatedBy=Yii::app()->user->id;
            $newquestion->TM_QN_CreatedOn=date('Y-m-d');
            $newquestion->TM_QN_UpdatedBy='0';
            $newquestion->TM_QN_UpdatedOn='0000-00-00';
            $user=User::model()->findbyPk(Yii::app()->user->id);
            $newquestion->TM_QN_School_Id=$id;
            $newquestion->TM_QN_QuestionReff=rand(11111111, 99999999);
            switch($type)
            {
                case '5':
                    $questionparts=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                    if($newquestion->save(false)):
                        $question=new SchoolQuestions();
                        $user=User::model()->findbyPk(Yii::app()->user->id);
                        $usertype=$user->usertype;
                        if($usertype==9):
                            $schoolid=Yii::app()->session['school'];
                        else:
                            $schoolid=$id;
                        endif;
                        $question->TM_SQ_School_Id=$schoolid;
                        $question->TM_SQ_Question_Type=$newquestion->TM_QN_Type_Id;
                        $question->TM_SQ_Question_Id=$newquestion->TM_QN_Id;
                        $question->TM_SQ_Standard_Id=$newquestion->TM_QN_Standard_Id;
                        $question->TM_SQ_Chapter_Id=$newquestion->TM_QN_Topic_Id;
                        $question->TM_SQ_Topic_Id=$newquestion->TM_QN_Section_Id;
                        $question->TM_SQ_Syllabus_Id=$newquestion->TM_QN_Syllabus_Id;
                        $question->TM_SQ_Publisher_id=$newquestion->TM_QN_Publisher_Id;
                        $question->TM_SQ_Type='0';
                        $question->TM_SQ_Status='0';
                        $question->save(false);                        
                        foreach($questionparts AS $parts):
                            //$parts=Questions::model()->findByPk($parts->TM_QN_Question);
                            $newpart= new Questions();
                            $newpart->attributes=$parts->attributes;
                            $newpart->TM_QN_Id=NULL;
                            $newpart->TM_QN_Question=$parts->TM_QN_Question;
                            $newpart->TM_QN_Parent_Id=$newquestion->TM_QN_Id;
                            if($newpart->save(false)):
                                foreach($parts->answers AS $ans):
                                    $answer=Answers::model()->findByPk($ans->TM_AR_Id);
                                    $newanswer=new Answers();
                                    $newanswer->attributes=$answer->attributes;
                                    $newanswer->TM_AR_Id=NULL;
                                    $newanswer->TM_AR_Answer=$answer->TM_AR_Answer;
                                    $newanswer->TM_AR_Question_Id=$newpart->TM_QN_Id;
                                    $newanswer->save(false);
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    break;
                case '6':
                    $newquestion->save(false);
                    break;
                case '7':
                    if($newquestion->save(false)):
                        $questionparts=Questions::model()->findAll(array("condition"=>"TM_QN_Parent_Id ='".$question->TM_QN_Id."'","order"=>"TM_QN_Id ASC"));
                        foreach($questionparts AS $parts):
                            $newpart= new Questions();
                            $newpart->attributes=$parts->attributes;
                            $newpart->TM_QN_Id=NULL;
                            $newpart->TM_QN_Question=$parts->TM_QN_Question;
                            $newpart->TM_QN_Parent_Id=$newquestion->TM_QN_Id;
                            $newpart->save(false);
                        endforeach;
                    endif;
                    break;
                default:
                    if($newquestion->save(false)):
                        $schoolqstns=New SchoolQuestions();
                        $user=User::model()->findbyPk(Yii::app()->user->id);
                        $usertype=$user->usertype;
                        if($usertype==9):
                            $schoolid=Yii::app()->session['school'];
                        else:
                            $schoolid=$id;
                        endif;
                        $schoolqstns->TM_SQ_School_Id=$schoolid;
                        $schoolqstns->TM_SQ_Question_Type=$newquestion->TM_QN_Type_Id;
                        $schoolqstns->TM_SQ_QuestionReff=$newquestion->TM_QN_QuestionReff;
                        $schoolqstns->TM_SQ_Question_Id=$newquestion->TM_QN_Id;
                        $schoolqstns->TM_SQ_Chapter_Id=$newquestion->TM_QN_Topic_Id;
                        $schoolqstns->TM_SQ_Topic_Id=$newquestion->TM_QN_Section_Id;
                        $schoolqstns->TM_SQ_Syllabus_Id=$newquestion->TM_QN_Syllabus_Id;
                        $schoolqstns->TM_SQ_Publisher_id=$newquestion->TM_QN_Publisher_Id;
                        $schoolqstns->TM_SQ_Standard_Id=$newquestion->TM_QN_Standard_Id;
                        if($question->TM_QN_School_Id==0):
                            $qstntype=1;
                        else:
                            $qstntype=0;
                        endif;
                        $schoolqstns->TM_SQ_Type=$qstntype;
                        $schoolqstns->TM_SQ_Status=0;
                        $schoolqstns->save(false);
                        $qstexams=Questions::model()->getExams($question->TM_QN_Id);
                        foreach($qstexams AS $exam):
                            $exams=New QuestionExams();
                            $exams->TM_QE_Exam_Id=$exam['Id'];
                            $exams->TM_QE_Question_Id=$newquestion->TM_QN_Id;
                            $exams->save(false);
                        endforeach;
                        foreach($question->answers AS $ans):
                            $answer=Answers::model()->findByPk($ans->TM_AR_Id);
                            $newanswer=new Answers();
                            $newanswer->attributes=$answer->attributes;
                            $newanswer->TM_AR_Id=NULL;
                            $newanswer->TM_AR_Answer=$answer->TM_AR_Answer;
                            $newanswer->TM_AR_Question_Id=$newquestion->TM_QN_Id;
                            $newanswer->save(false);
                        endforeach;
                    endif;
            }
            $this->redirect(array('update','id'=>$id,'question'=>$newquestion->TM_QN_Id,'action'=>'copy'));
        endif;
    }
    public function actionDelete($id)
    {
        $question=Questions::model()->findByPk($_GET['question']);
        $question->TM_QN_Status='1';
        if($question->save(false)):
            $schoolquestion=SchoolQuestions::model()->find(array('condition'=>'TM_SQ_Question_Id='.$_GET['question']));
            $schoolquestion->TM_SQ_Status='1';
            $schoolquestion->save(false);
        endif;                           

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin','id'=>$id));        
    }

    public function actionDeletequestion($id)
    {
        $question=Questions::model()->findByPk($_GET['question']);
        $question->TM_QN_Status='1';
        if($question->save(false)):
            $schoolquestion=SchoolQuestions::model()->find(array('condition'=>'TM_SQ_Question_Id='.$_GET['question']));
            //print_r($schoolquestion);exit;
            $schoolquestion->TM_SQ_Status='1';
            $schoolquestion->save(false);
        endif;
        $user=User::model()->findbyPk(Yii::app()->user->id);
        $usertype=$user->usertype;
        if($usertype==9):
            $schoolid=Yii::app()->session['school'];
        else:
            $schoolid=$id;
        endif;

        //echo $schoolid;exit;
        $this->redirect(array('admin','id'=>$schoolid));
    }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}