<?php

class PlanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admincolumn2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
        return array(
            'accessControl',
        );
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
/*			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
			),*/
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','index','view','Report','Export','manageschoolplan','viewschoolplan','updateschoolplan','createschoolplan','deleteschoolplan','UpdateGateway'),
                'users'=>UserModule::getSuperAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$models=$this->loadModel($id);
        $modules='';
        $planmodules=Plan::model()->getModules($id);
        foreach($planmodules AS $key=>$module):
            $modules.=($key==0?'':',').Planmodules::itemAlias("PlanModules",$module['Id']);
        endforeach;
        $this->render('view',array(
			'model'=>$models,
			'modules'=>$modules,
		));
	}
	public function actionviewschoolplan($id)
	{
		$models=$this->loadModel($id);
        $modules='';
        $planmodules=Plan::model()->getModules($id);
        $this->render('viewschoolplan',array(
			'model'=>$models,			
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Plan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Plan']))
		{

			$model->attributes=$_POST['Plan'];
			if($model->save()):
                for($i=0;$i<count($model->planmodules);$i++):
                    $newmodule=new Planmodules();
                    $newmodule->TM_PM_Plan_Id=$model->TM_PN_Id;
                    $newmodule->TM_PM_Module=$model->planmodules[$i];
                    $newmodule->save();
                endfor;
				$this->redirect(array('view','id'=>$model->TM_PN_Id));
            endif;
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	public function actionCreateschoolplan()
    {
        $model=new Plan;
        $model->scenario = 'createschoolplan';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        if(isset($_POST['Plan']))
        {
        
        	$model->attributes=$_POST['Plan'];
        	if($model->save()):                
        		$this->redirect(array('viewschoolplan','id'=>$model->TM_PN_Id));
            endif;
        }
        
        $this->render('createschoolplan',array(
        	'model'=>$model,
        ));  
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Plan']))
		{
			$model->attributes=$_POST['Plan'];
            print_r($model->planmodules);
			if($model->save()):
                Planmodules::model()->deleteAll(array('condition'=>'TM_PM_Plan_Id='.$id));
                for($i=0;$i<count($model->planmodules);$i++):
                    $newmodule=new Planmodules();
                    $newmodule->TM_PM_Plan_Id=$id;
                    $newmodule->TM_PM_Module=$model->planmodules[$i];
                    $newmodule->save();
                endfor;
                $this->redirect(array('view','id'=>$model->TM_PN_Id));
            endif;
		}

		$this->render('update',array(
			'model'=>$model,
			'modules'=>Plan::model()->getModules($id),
		));
	}
	public function actionUpdateschoolplan($id)
	{
		$model=$this->loadModel($id);
        $model->scenario = 'createschoolplan';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Plan']))
		{
			$model->attributes=$_POST['Plan'];            
			if($model->save()):
                $this->redirect(array('viewschoolplan','id'=>$model->TM_PN_Id));
            endif;
		}

		$this->render('updateschoolplan',array(
			'model'=>$model,			
		));
	} 


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{

        $model=Plan::model()->findByPk($id);;
        $model->TM_PN_Status='111';        
        if($model->save(false)):
            $studentplans=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_PlanId='.$id));
            foreach($studentplans AS $sp):                
                $student=$sp->TM_SPN_StudentId; 
                $sp->TM_SPN_CancelDate=date('y-m-d');          
            	$sp->TM_SPN_Status='1';
                $sp->save(false);
                $studentplans=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$sp->TM_SPN_StudentId.' AND TM_SPN_Status=0'));
                if($studentplans==0)
                {
                    $user=User::model()->findByPk($sp->TM_SPN_StudentId);
                    if($user):
                        $user->status='0';
                        $user->save(false);
                    endif;
                }                                
            endforeach;                        
            //$this->loadModel($id)->delete()
            ///Planmodules::model()->deleteAll(array('condition'=>'TM_PM_Plan_Id='.$id));
        endif;

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionReport()
    {
        if(isset($_POST['search']))
        {
            $formvals=array();
            $connection = CActiveRecord::getDbConnection();
            $sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,a.TM_STU_SchoolCustomname,a.TM_STU_City,a.TM_STU_State,a.TM_STU_Status,";
            $sql.="b.TM_SPN_PlanId,b.TM_SPN_PaymentReference,b.TM_SPN_Provider,DATE_FORMAT(b.TM_SPN_CreatedOn,'%d-%m-%Y') AS date,DATE_FORMAT(b.TM_SPN_CancelDate,'%d-%m-%Y') AS canceldate,b.TM_SPN_Status,b.TM_SPN_Currency,b.TM_SPN_CouponId,";
            $sql.="DATE_FORMAT(b.TM_SPN_ExpieryDate,'%d-%m-%Y') AS expirydate,c.firstname,c.lastname,c.phonenumber,d.TM_PN_Name,d.TM_PN_Status,d.TM_PN_Rate,e.TM_CP_Id,e.TM_CP_Type,e.TM_CP_value,e.TM_CP_Code,";
            $sql.="f.TM_SB_Name,g.TM_SD_Name,h.TM_CON_Name,j.email,j.location,i.username,k.TM_CR_Name FROM tm_student AS a ";
            $sql.="INNER JOIN tm_student_plan AS b ON a.TM_STU_User_Id=b.TM_SPN_StudentId INNER JOIN tm_profiles AS c ON a.TM_STU_Parent_Id=c.user_id INNER JOIN tm_plan AS d ON b.TM_SPN_PlanId=d.TM_PN_Id ";
            $sql.="LEFT JOIN tm_coupons AS e ON b.TM_SPN_CouponId=e.TM_CP_Id ";
            $sql.="INNER JOIN tm_syllabus AS f ON b.TM_SPN_SyllabusId=f.TM_SB_Id INNER JOIN tm_standard AS g ON b.TM_SPN_StandardId=g.TM_SD_Id ";
            $sql.="LEFT JOIN tm_country AS h ON a.TM_STU_Country_Id=h.TM_CON_Id ";
            $sql.="INNER JOIN tm_users AS i ON a.TM_STU_User_Id=i.id ";
            $sql.="INNER JOIN tm_users AS j ON a.TM_STU_Parent_Id=j.id ";
            $sql.="INNER JOIN tm_currency AS k ON d.TM_PN_Currency_Id=k.TM_CR_Id WHERE a.TM_STU_Status IN(0,1,2)";

            if($_POST['school']!=''):
                if($_POST['school']=='0'):
                    $formvals['school']=$_POST['school'];
                    $formvals['schooltext']=$_POST['schooltext'];
                    $sql.=" AND a.TM_STU_SchoolCustomname LIKE '%".$_POST['schooltext']."%' ";
                else:
                    $formvals['school']=$_POST['school'];                    
                    $sql.=" AND a.TM_STU_School='".$_POST['school']."'";
                endif;                                
            endif;
            if($_POST['vouchercode']!=''):
                $formvals['vouchercode']=$_POST['vouchercode'];
                $sql.=" AND e.TM_CP_Code LIKE '%".$_POST['vouchercode']."%' ";
            endif;
            if($_POST['plan']!=''):
                $formvals['plan']=$_POST['plan'];
                $plan="";
                foreach($_POST['plan'] AS $key=>$plans):
                    if($key==0):
                        $plan.=$plans;
                    else:
                        $plan.=','.$plans;
                    endif;
                endforeach;
                if($plan!=''):
                    $formvals['plan']=$plan;
                    $sql.=" AND b.TM_SPN_PlanId IN ($plan) ";
                endif;
            endif;
            if($_POST['status']!='all'):
                if($_POST['status']=='both'):
                    $formvals['status']=$_POST['status'];
                    $sql.=" AND b.TM_SPN_Status IN (0,1)";
                else:
                    $formvals['status']=$_POST['status'];
                    $sql.=" AND b.TM_SPN_Status='".$_POST['status']."' ";
                endif;
            else:
                $formvals['status']=$_POST['status'];
            endif;
            if($_POST['date']=='before'):
                $formvals['date']=$_POST['date'];
                if($_POST['fromdate']!=''):
                    $formvals['fromdate']=$_POST['fromdate'];
                    $sql.=" AND b.TM_SPN_CreatedOn < '".$_POST['fromdate']."' ";
                endif;
            elseif($_POST['date']=='after'):
                $formvals['date']=$_POST['date'];
                if($_POST['fromdate']!=''):
                    $formvals['fromdate']=$_POST['fromdate'];
                    $sql.=" AND b.TM_SPN_CreatedOn > '".$_POST['fromdate']."' ";
                endif;
            else:
                if($_POST['fromdate']!='' && $_POST['todate']):
                    $formvals['fromdate']=$_POST['fromdate'];
                    $formvals['todate']=$_POST['todate'];
                    $sql.=" AND b.TM_SPN_CreatedOn BETWEEN '".$_POST['fromdate']."' AND '".$_POST['todate']."' ";
                endif;
            endif;
            //echo $sql;exit;
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $dataval=$dataReader->readAll();
            $dataitems='';
            if(count($dataval)>0):
                foreach($dataval AS $data):
                    if($data['TM_SPN_Status']==0):
                        $status="Active";
                    else:
                        $status="Inactive";
                    endif;
                    if($data['TM_STU_Status']==0):
                        $studstatus="Active";
                    elseif($data['TM_STU_Status']==1):
                        $studstatus="Inactive";
                    else:
                        $studstatus="Blocked";
                    endif;
                    if($data['canceldate']=='00-00-0000'):
                        $canceldate='';
                    else:
                        $canceldate=$data['canceldate'];
                    endif;
                    if($data['expirydate']=='00-00-0000'):
                        $expirydate='';
                    else:
                        $expirydate=$data['expirydate'];
                    endif;
                    $planrates=$this->Getplanrates($data['TM_SPN_Currency'],$data['TM_SPN_PlanId'],$data['TM_CP_Id'],$data['location'],$data['TM_SPN_CouponId'],$data['TM_STU_School'],$data['TM_SPN_Provider']);
                    if($data['TM_CP_Type']==0):
                        $planrate=$data['TM_PN_Rate'];
                        $cpnvalue=$data['TM_CP_value'];
                        $finalprice=$planrate - $cpnvalue;
                    elseif($data['TM_CP_Type']==1):
                        $planrate=$data['TM_PN_Rate'];
                        $cpnper=$data['TM_CP_value'];
                        $cpnvalue=($planrate * $cpnper) / 100;
                        $finalprice=$planrate - $cpnvalue;
                    endif;
                    if($data['TM_SPN_Currency']!='0'):
                        $currency=Currency::model()->findByPk($data['TM_SPN_Currency'])->TM_CR_Name;
                    else:
                        $currency=$data['TM_CR_Name'];  
                    endif;
                    $dataitems.="<tr>
                        <td>".$data['username']."</td>
                        <td>".$data['TM_STU_First_Name']." ".$data['TM_STU_Last_Name']."</td>
                        <td>".($data['TM_STU_School']=='0'? $data['TM_STU_SchoolCustomname']: School::model()->findByPk($data['TM_STU_School'])->TM_SCL_Name)."</td>
                        <td>".$studstatus."</td>
                        <td>".$data['TM_STU_City']."</td>
                        <td>".$data['TM_STU_State']."</td>
                        <td>".$data['TM_CON_Name']."</td>
                        <td>".$data['firstname']." ".$data['lastname']."</td>
                        <td>".$data['email']."</td>
                        <td>".$data['phonenumber']."</td>
                        <td>".$data['TM_PN_Name']."</td>
                        <td>".$data['TM_SB_Name']."</td>
                        <td>".$data['TM_SD_Name']."</td>
                        <td>".$data['TM_SPN_PaymentReference']."</td>
                        <td>".StudentPlan::model()->GetGateway($data['TM_SPN_Provider'])."</td>
                        <td>".$data['date']."</td>
                        <td>".$expirydate."</td>
                        <td>".$canceldate."</td>
                        <td>".$status."</td>
                        <td>".($data['TM_SPN_CouponId']=='-1'? 'School Discount': $data['TM_CP_Code'] )."</td>
                        <td>".$currency."</td>
                        <td>".$planrates['rate']."</td>
                        <td>".($data['TM_SPN_Provider']==3?'0':$planrates['discountrate'])."</td>
                        </tr>";
                endforeach;

            else:
                $dataitems="<tr><td colspan='22' class='empty'><span class='empty' style='font-style: italic;'>No results found.</span></td></tr>";
            endif;
        }
        $this->render('subscriptionreport',array('dataitems'=>$dataitems,'formvals'=>$formvals));
    }
	public function actionExport()
    {
        //$table=$_POST['table'];
        //$html = str_get_html($table);
        $connection = CActiveRecord::getDbConnection(); 
		$sql="SELECT a.TM_STU_First_Name,a.TM_STU_Last_Name,a.TM_STU_School,a.TM_STU_SchoolCustomname,a.TM_STU_City,a.TM_STU_State,a.TM_STU_Status,";
		$sql.="b.TM_SPN_PlanId,b.TM_SPN_PaymentReference,b.TM_SPN_Provider,DATE_FORMAT(b.TM_SPN_CreatedOn,'%d-%m-%Y') AS date,DATE_FORMAT(b.TM_SPN_CancelDate,'%d-%m-%Y') AS canceldate,b.TM_SPN_Status,b.TM_SPN_Currency,b.TM_SPN_CouponId,";
		$sql.="DATE_FORMAT(b.TM_SPN_ExpieryDate,'%d-%m-%Y') AS expirydate,c.firstname,c.lastname,c.phonenumber,d.TM_PN_Name,d.TM_PN_Status,d.TM_PN_Rate,e.TM_CP_Id,e.TM_CP_Type,e.TM_CP_value,e.TM_CP_Code,";
		$sql.="f.TM_SB_Name,g.TM_SD_Name,h.TM_CON_Name,j.email,j.location,i.username,k.TM_CR_Name FROM tm_student AS a ";
		$sql.="INNER JOIN tm_student_plan AS b ON a.TM_STU_User_Id=b.TM_SPN_StudentId INNER JOIN tm_profiles AS c ON a.TM_STU_Parent_Id=c.user_id INNER JOIN tm_plan AS d ON b.TM_SPN_PlanId=d.TM_PN_Id ";
		$sql.="LEFT JOIN tm_coupons AS e ON b.TM_SPN_CouponId=e.TM_CP_Id ";
		$sql.="INNER JOIN tm_syllabus AS f ON b.TM_SPN_SyllabusId=f.TM_SB_Id INNER JOIN tm_standard AS g ON b.TM_SPN_StandardId=g.TM_SD_Id ";
		$sql.="LEFT JOIN tm_country AS h ON a.TM_STU_Country_Id=h.TM_CON_Id ";
		$sql.="INNER JOIN tm_users AS i ON a.TM_STU_User_Id=i.id ";
		$sql.="INNER JOIN tm_users AS j ON a.TM_STU_Parent_Id=j.id ";
		$sql.="INNER JOIN tm_currency AS k ON d.TM_PN_Currency_Id=k.TM_CR_Id WHERE a.TM_STU_Status IN(0,1,2)";        

        if($_GET['school']!=''):
            if($_GET['school']=='0'):
                $sql.=" AND a.TM_STU_SchoolCustomname LIKE '%".$_GET['schooltext']."%' ";
            else:                                        
                $sql.=" AND a.TM_STU_School='".$_GET['school']."'";
            endif;                      
        endif;
        if($_GET['vouchercode']!=''):
            $sql.=" AND e.TM_CP_Code LIKE '%".$_GET['vouchercode']."%' ";
        endif;
        if($_GET['plan']!=''):
            $plan=$_GET['plan'];
            if($plan!=''):
                $sql.=" AND b.TM_SPN_PlanId IN ($plan) ";
            endif;
        endif;
        if($_GET['status']!='all'):
            if($_GET['status']=='both'):
                $sql.=" AND b.TM_SPN_Status IN (0,1)";
            else:
                $sql.=" AND b.TM_SPN_Status='".$_GET['status']."' ";
            endif;
        endif;
        if($_GET['date']=='before'):
            if($_GET['fromdate']!=''):
                $sql.=" AND b.TM_SPN_CreatedOn < '".$_GET['fromdate']."' ";
            endif;
        elseif($_GET['date']=='after'):
            if($_GET['fromdate']!=''):
                $sql.=" AND b.TM_SPN_CreatedOn > '".$_GET['fromdate']."' ";
            endif;
        else:
            if($_GET['fromdate']!='' && $_GET['todate']):
                $sql.=" AND b.TM_SPN_CreatedOn BETWEEN '".$_GET['fromdate']."' AND '".$_GET['todate']."' ";
            endif;
        endif;
        //echo $sql;exit;
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $dataval=$dataReader->readAll();        
        foreach($dataval AS $data):
            if($data['TM_SPN_Status']==0):
                $status="Active";
            else:
                $status="Inactive";
            endif;
            if($data['TM_STU_Status']==0):
                $studstatus="Active";
            elseif($data['TM_STU_Status']==1):
                $studstatus="Inactive";
            else:
                $studstatus="Blocked";
            endif;
            if($data['canceldate']=='00-00-0000'):
                $canceldate='';
            else:
                $canceldate=$data['canceldate'];
            endif;
            if($data['expirydate']=='00-00-0000'):
                $expirydate='';
            else:
                $expirydate=$data['expirydate'];
            endif;

           /*if($data['TM_CP_Type']==0):
                $planrate=$data['TM_PN_Rate'];
                $cpnvalue=$data['TM_CP_value'];
                $finalprice=$planrate - $cpnvalue;
            elseif($data['TM_CP_Type']==1):
                $planrate=$data['TM_PN_Rate'];
                $cpnper=$data['TM_CP_value'];
                $cpnvalue=($planrate * $cpnper) / 100;
                $finalprice=$planrate - $cpnvalue;
            endif;
                               
            if($data['TM_SPN_Currency']!='0'):
                $currency=Currency::model()->findByPk($data['TM_SPN_Currency'])->TM_CR_Name;
            else:
                  $currency=$data['TM_CR_Name'];  
            endif;  */                
            $planrates=$this->Getplanrates($data['TM_SPN_Currency'],$data['TM_SPN_PlanId'],$data['TM_CP_Id'],$data['location'],$data['TM_SPN_CouponId'],$data['TM_STU_School'],$data['TM_SPN_Provider']);
            if($data['TM_CP_Type']==0):
                $planrate=$data['TM_PN_Rate'];
                $cpnvalue=$data['TM_CP_value'];
                $finalprice=$planrate - $cpnvalue;
            elseif($data['TM_CP_Type']==1):
                $planrate=$data['TM_PN_Rate'];
                $cpnper=$data['TM_CP_value'];
                $cpnvalue=($planrate * $cpnper) / 100;
                $finalprice=$planrate - $cpnvalue;
            endif;
            if($data['TM_SPN_Currency']!='0'):
                $currency=Currency::model()->findByPk($data['TM_SPN_Currency'])->TM_CR_Name;
            else:
                $currency=$data['TM_CR_Name'];  
            endif;            
            $items[]=array($data['username'],
                $data['TM_STU_First_Name'].' '.$data['TM_STU_Last_Name'],
                ($data['TM_STU_School']=='0'? $data['TM_STU_SchoolCustomname']: School::model()->findByPk($data['TM_STU_School'])->TM_SCL_Name),
                $studstatus,
                $data['TM_STU_City'],
                $data['TM_STU_State'],
                $data['TM_CON_Name'],
                $data['firstname'].' '.$data['lastname'],
                $data['email'],
                $data['phonenumber'],
                $data['TM_PN_Name'],
                $data['TM_SB_Name'],
                $data['TM_SD_Name'],
                $data['TM_SPN_PaymentReference'],
                StudentPlan::model()->GetGateway($data['TM_SPN_Provider']),
                $data['date'],
                $expirydate,
                $canceldate,
                $status,
                ($data['TM_SPN_CouponId']=='-1'? 'School Discount': $data['TM_CP_Code'] ),
                $currency,
                $planrates['rate'],
                ($data['TM_SPN_Provider']==3?'0':$planrates['discountrate']),
            );		
        endforeach;        
        function cleanData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
        }


        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=SubscriptionReport.csv');
        $output = fopen('php://output', 'w');

        // output the column headings
        fputcsv($output, array(
            "Student Username",
            "Student Name",
            "School Name",
            "Student Status",
            "City",
            "State",
            "Country",
            "Parent Name",
            "Parent Email",
            "Parent Phone Number",
            "Plan",
            "Syllabus",
            "Standard",
            "Transaction Code",
            "Gateway",
            "Activated On",
            "Expired On",
            "Cancelled On",
            "Subscription Status",
            "Coupon Code",
            "Currency",
            "Price of Subscription",
            "Final Price Paid"
        ));

        $flag = false;
        foreach($items as $row) {
            if(!$flag) {
                // display field/column names as first row
                fputcsv($output, $row);
                $flag = true;
            }
            array_walk($row, 'cleanData');
            fputcsv($output, $row);
        }
    }	
	public function actionDeleteschoolplan($id)
	{
		$this->loadModel($id)->delete();            
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
    

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Plan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Plan('search');
/*		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Plan']))
			$model->attributes=$_GET['Plan'];*/

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionManageschoolplan()
	{

		$model=new Plan('searchschool');
		if(isset($_GET['Plan']))
            $model->attributes=$_GET['Plan'];
/*		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Plan']))
			$model->attributes=$_GET['Plan'];*/

		$this->render('schoolplanadmin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Plan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Plan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Plan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='plan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function Getplanrates($currency,$plan,$coupon,$location,$coupinid=NULL,$school=NULL,$provider)
    {        
        $masterplan=Plan::model()->with('currency')->with('additional')->findByPk($plan);
        $masstercoupon=Coupons::model()->findByPk($coupon);
        if($coupon):
        
            if($masstercoupon->TM_CP_Type=='1'):                              
                $percentage=$masstercoupon->TM_CP_value/100;
                if($currency==$masterplan->TM_PN_Currency_Id):                 
                    $amount=$masterplan->TM_PN_Rate*$percentage;
                    $amount=round($amount,2,PHP_ROUND_HALF_UP);
                    $actualrate=$masterplan->TM_PN_Rate;
                else:
                    $amount=$masterplan->TM_PN_Additional_Rate*$percentage;
                    $amount=round($amount,2,PHP_ROUND_HALF_UP);
                    $actualrate=$masterplan->TM_PN_Additional_Rate;
                endif;   
                $discountrate=$actualrate-$amount;
            elseif($masstercoupon->TM_CP_Type=='0'):   
                if($currency==$masterplan->TM_PN_Currency_Id):                                         
                    $discountrate=$masterplan->TM_PN_Rate-$masstercoupon->TM_CP_value;
                    $actualrate=$masterplan->TM_PN_Rate;
                else:
                    $discountrate=$masterplan->TM_PN_Additional_Rate-$masstercoupon->TM_CP_value;
                    $actualrate=$masterplan->TM_PN_Additional_Rate;                        
                endif;       
            endif;
        else:
            
            if($currency==$masterplan->TM_PN_Currency_Id):
                if($coupinid=='-1'):
                    $schooldiscount=School::model()->findByPk($school);
                    if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                         $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                         $amount=$masterplan->TM_PN_Rate*$percentage;
                    	 $amount=round($amount,2,PHP_ROUND_HALF_UP);		                         
                         $discountrate=$masterplan->TM_PN_Rate-$amount;
                    elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                        $discountrate=$masterplan->TM_PN_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                    else:
                        $discountrate=$masterplan->TM_PN_Rate;
                    endif;
                    $actualrate=$masterplan->TM_PN_Rate;                
                else:
                    $actualrate=$masterplan->TM_PN_Rate;
                    $discountrate=$masterplan->TM_PN_Rate;
                endif;                                           
            else:
                if($coupinid=='-1'):                   
                    $schooldiscount=School::model()->findByPk($school);
                    if($schooldiscount->TM_SCL_Discount_Type=='1'):                              
                         $percentage=$schooldiscount->TM_SCL_Discount_Amount/100;                 
                         $amount=$masterplan->TM_PN_Additional_Rate*$percentage;
                    	 $amount=round($amount,2,PHP_ROUND_HALF_UP);		                         
                         $discountrate=$masterplan->TM_PN_Additional_Rate-$amount;
                    elseif($schooldiscount->TM_SCL_Discount_Type=='0'):                
                        $discountrate=$masterplan->TM_PN_Additional_Rate-$schooldiscount->TM_SCL_Discount_Amount;
                    else:
                        $discountrate=$masterplan->TM_PN_Additional_Rate;
                    endif;
                    $actualrate=$masterplan->TM_PN_Additional_Rate;
                else:
                    if($provider==3):
                        $actualrate=$masterplan->TM_PN_Rate;
                        $discountrate=$masterplan->TM_PN_Rate;
                    else:
                        $actualrate=$masterplan->TM_PN_Additional_Rate;
                        $discountrate=$masterplan->TM_PN_Additional_Rate;
                    endif;
                endif;
            endif;
        endif;                                  
        return array('rate'=>$actualrate,'discountrate'=>$discountrate);
    }

    public function actionUpdateGateway()
    {
        $connection = CActiveRecord::getDbConnection();
        $sql="SELECT TM_STU_User_Id FROM `tm_student` WHERE `TM_STU_School` = 24";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        $datavals=$dataReader->readAll();
        //echo count($datavals);exit;
        $count=0;
        foreach($datavals AS $dataval):
            $studentplans=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId='.$dataval['TM_STU_User_Id'].' '));
            if(count($studentplans)>0){
                foreach($studentplans AS $studentplan):
                    $studentplan->TM_SPN_Provider=3;
                    $studentplan->save(false);
                endforeach;
            }
            $count++;
        endforeach;
        echo $count;
    }
}
