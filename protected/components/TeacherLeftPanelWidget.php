<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Karthik
 * Date: 11/10/14
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
Yii::import('zii.widgets.CPortlet');

class TeacherLeftPanelWidget  extends CPortlet
{

    protected function renderContent()
    {
        $user=User::model()->findByPk(Yii::app()->user->id);
        $standards=TeacherStandards::model()->findAll(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id));        
        $this->render('teacherleftpanelwidget',array('user'=>$user,'standards'=>$standards));
    }
    public function GetActiveMenu($url,$needle)
    {    
        if (strpos($url,$needle) !== false) {
            return true;
        }
    }
}