<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse" style="background-color: #222D32;">
	
	<div class="outter">
	<!--<img src="http://lifestyle.iloveindia.com/lounge/images/oblong-face-hairstyles.jpg" class="img-circle">-->
		<h1><?php echo ucfirst($user->profile->firstname);?></h1>
	 </div>
     <?php if(Yii::app()->user->isTeacher()):?>
        <div style="width: 100%;height: auto;margin-bottom: 10px;margin-top: 10px;">
            <center>
                <?php foreach($standards AS $standard):
                    if(Yii::app()->session['standard']==$standard->TM_TE_ST_Standard_Id):
                    ?>
                    <a class="btn btn-success btn-outline btn-sm outlbutmgr active" href="javascript:void(0);"><?php echo $standard->standard->TM_SD_Name;?></a>
                    <?php else:?>
                    <a class="btn btn-success btn-outline btn-sm outlbutmgr" href="<?php echo Yii::app()->createUrl('teachers/ChangeStandard',array('id'=>$standard->standard->TM_SD_Id))?>" ><?php echo $standard->standard->TM_SD_Name;?></a>
                    <?php endif;?>
                <?php endforeach;?>
            <center>
        </div> 
        <?php endif;?>    
        <ul class="nav" id="side-menu">
            <?php if(Yii::app()->user->isTeacher()):?>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'teacherhome')?'class="active"':'');?>>                
                <a href="<?php echo Yii::app()->createUrl('teacherhome')?>"><i class="fa fa-dashboard fa-fw fa-lg"></i>Dashboard</a>
            </li>
           <!-- /. <li>
                <a href="<?php echo Yii::app()->createUrl('teachers/CreateCustomeHomework')?>"><i class=fa fa-edit fa-fw fa-lg"></i> Custom Homework<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="flot.html">Flot Charts</a>
                    </li>
                    <li>
                        <a href="morris.html">Morris.js Charts</a>
                    </li>
                </ul>
                
            </li> 
            <li>
                <a href="javacript:void(0);"><i class="fa fa-table fa-fw fa-lg"></i> Quick Homework</a>
            </li>-->
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'worksheet')?'class="active"':'');?>>
                <?php if(Yii::app()->session['userlevel']==1):?>
                <a href="<?php echo Yii::app()->createUrl('teachers/listworksheets')?>"><i class="fa fa-list-alt fa-fw fa-lg"></i> Diagnostic Quiz</a>
                <?php else:?>
                <a href="<?php echo Yii::app()->createUrl('teachers/listworksheets')?>"><i class="fa fa-list-alt fa-fw fa-lg"></i>Worksheet</a>
                <?php endif;?>
            </li>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'quick')?'class="active"':'');?>>
                <a href="<?php echo Yii::app()->createUrl('teachers/quickhomework')?>"><i class="fa fa-table fa-fw fa-lg"></i> Quick Practice</a>
            </li>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'custom')?'class="active"':'');?>>
                <a href="<?php echo Yii::app()->createUrl('teachers/listcustomehomework')?>"><i class="fa fa-edit fa-fw fa-lg"></i> Custom Practice</a>
            </li>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'resource')?'class="active"':'');?>>
                <a href="<?php echo Yii::app()->createUrl('teachers/listresources')?>"><i class="fa fa-list-alt fa-fw fa-lg"></i> Resources</a>
            </li>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'history')?'class="active"':'');?>>
                <a href="<?php echo Yii::app()->createUrl('teachers/history')?>"><i class="fa fa-history fa-fw fa-lg"></i>History</a>
            </li>
			<!--<li <?php /*echo ($this->GetActiveMenu(Yii::app()->request->url,'groups')?'class="active"':'');*/?>>
                <a href="<?php /*echo Yii::app()->createUrl('SchoolGroups/managegroups',array('id'=>$user->school_id))*/?>"><i class="fa fa-users fa-fw fa-lg"></i> Manage Groups</a>
            </li>
			<li <?php /*echo ($this->GetActiveMenu(Yii::app()->request->url,'SchoolQuestions')?'class="active"':'');*/?>>
                <a href="javacript:void(0);"><i class="fa fa-circle fa-lg"></i> Manage Questions <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php /*echo Yii::app()->createUrl('SchoolQuestions/admin',array('id'=>$user->school_id))*/?>">List Questions</a>
                    </li>
                    <li>
                        <a href="<?php /*echo Yii::app()->createUrl('SchoolQuestions/create',array('id'=>$user->school_id))*/?>">Create Questions</a>
                    </li>
                    <!--<li>
                        <a href="<?php /*echo Yii::app()->createUrl('SchoolQuestions/systemquestions',array('id'=>$user->school_id))*/?>">Manage System Questions</a>
                    </li>  
                    <li>
                        <a href="<?php /*echo Yii::app()->createUrl('SchoolQuestions/importquestions',array('id'=>$user->school_id))*/?>">Import System Questions</a>
                    </li>  --> 
                <!--</ul> -->
            <!--</li>-->

            <?php /*if(Yii::app()->user->isTeacherAdmin()):*/?><!--
            <li >
                <a href="<?php /*echo Yii::app()->createUrl('schoolhome')*/?>" ><i class="fa fa-user-md fa-fw fa-lg"></i>Switch to admin</a>
            </li>
            <?php /*endif;*/?>
            <li>
                <a href="<?php /*echo Yii::app()->createUrl('user/logout')*/?>"><i class="fa fa-sign-out fa-fw fa-lg"></i>Logout</a>
            </li>-->
			<!--<li>
                <a href="javacript:void(0);"><i class="fa fa-question-circle fa-fw fa-lg"></i> Manage Questions</a>
            </li>
			<li>
                <a href="javacript:void(0);"><i class="fa fa-history fa-fw fa-lg"></i> History</a>
            </li>-->
        <?php else:?>
            <li>
                <a href="<?php echo Yii::app()->createUrl('staffhome')?>"><i class="fa fa-dashboard fa-fw fa-lg"></i> My Stuff</a>
            </li> 
            <li>
                <a href="<?php echo Yii::app()->createUrl('user/logout')?>"><i class="fa fa-sign-out fa-fw fa-lg"></i>Logout</a>
            </li>                   
        <?php endif;?>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>