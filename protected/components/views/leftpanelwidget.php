<?php if(!Yii::app()->user->isGuest):?>

<div class="col-sm-3 col-md-2 sidebar">
   
    <?php if(Yii::app()->controller->GetParent()){?>
         <div class="outter">
            <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($user->profile->image!=''?$user->profile->image:"noimage.png").'?size=large&type=user&width=140&height=140';?>" class="img-circle" id="leftpanelimg">
            <h1><?php echo $user->profile->firstname;?></h1>        
        </div>        
        <?php }
        else{
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user->id));
        ?>  

        <div class="outter">      
        <!--<img src="<?php /*echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($student->TM_STU_Image!=''?$student->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140';*/?>" class="img-circle" id="leftpanelimgstud">-->
        <?php if(Yii::app()->session['userlevel']==1):?>
            <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($student->TM_STU_Image!=''?$student->TM_STU_Image:"noimage.png").'?size=large&type=user&width=140&height=140';?>" class="img-circle" id="leftpanelimgstud">
        <?php else:?>
            <img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/'.($student->TM_STU_Image!=''?$student->TM_STU_Image:"noimage1.png").'?size=large&type=user&width=140&height=140';?>" class="img-circle" id="leftpanelimgstud">
        <?php endif;?>
        <h1><?php echo $user->profile->firstname;?></h1>        
        </div> 
        <?php };?>






        <div style="width: 100%;height: auto;margin-bottom: 15px;">
            <center>
                <?php foreach($plans AS $plan):
                    if(Yii::app()->session['plan']==$plan->TM_PN_Id):
                    ?>
                    <a class="btn btn-success btn-outline btn-sm outlbutmgr active" href="javascript:void(0);"><?php echo $plan->TM_PN_Name;?></a>
                    <?php else:?>
                    <a class="btn btn-success btn-outline btn-sm outlbutmgr" href="<?php echo Yii::app()->createUrl('student/ChangePlan',array('id'=>$plan->TM_PN_Id))?>" ><?php echo $plan->TM_PN_Name;?></a>
                    <?php endif;?>
                <?php endforeach;?>
                <center>
        </div>
    <?php if(Yii::app()->controller->GetParent()):?>
        <ul class="nav nav-sidebar">
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'home')?'class="active"':'');?> ><a href="<?php echo Yii::app()->createUrl('parenthome')?>"><span class="stuff"></span> MY HOME <span class="sr-only">(current)</span></a></li>
            <!--<li <?php /*echo ($this->GetActiveMenu(Yii::app()->request->url,'mock')?'class="active"':'');*/?>><a href="<?php /*echo Yii::app()->createUrl('student/mock')*/?>"><span class="mocks"></span>MOCKS</a></li>
            <li <?php /*echo ($this->GetActiveMenu(Yii::app()->request->url,'challenge')?'class="active"':'');*/?>><a href="<?php /*echo Yii::app()->createUrl('student/challenge')*/?>"><span class="chall"></span>CHALLENGE</a></li>
            <li <?php /*echo ($this->GetActiveMenu(Yii::app()->request->url,'history')?'class="active"':'');*/?>><a href="<?php /*echo Yii::app()->createUrl('student/history')*/?>"><span class="history"></span>VIEW HISTORY</a></li>-->
            <li <?php //echo ($this->GetActiveMenu(Yii::app()->request->url,'home')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('user/logout')?>"><span class="logout glyphicon glyphicon-off"></span> LOGOUT</a></li>
        </ul>
    <?php elseif(Yii::app()->controller->GetTeacher()):?>
        <ul class="nav nav-sidebar">
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'teacherhome')?'class="active"':'');?> ><a href="<?php echo Yii::app()->createUrl('teacherhome')?>"><span class="stuff"></span> MY HOME <span class="sr-only">(current)</span></a></li>
            <li <?php //echo ($this->GetActiveMenu(Yii::app()->request->url,'home')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('user/logout')?>"><span class="logout glyphicon glyphicon-off"></span> LOGOUT</a></li>
        </ul>
    <?php else:?>
    <ul class="nav nav-sidebar">
        <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'home')?'class="active"':'');?> ><a href="<?php echo Yii::app()->createUrl('home')?>"><span class="stuff"></span> MY STUFF <span class="sr-only">(current)</span></a></li>
        <?php if(Yii::app()->session['quick'] || Yii::app()->session['difficulty']):?>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'revision')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('student/revision')?>"><span class="revice"></span>REVISE</a></li>
        <?php endif;?>
        <?php if(Yii::app()->session['mock']):?>
            <?php if(Yii::app()->session['userlevel']==1):?>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'mock')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('student/mock')?>"><span class="mocks"></span>DIAGNOSTIC QUIZ</a></li>
             <?php else:?>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'mock')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('student/mock')?>"><span class="mocks"></span>WORKSHEET</a></li>
             <?php endif;?>
        <?php endif;?>
        <?php if(Yii::app()->session['resource']):?>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'resource')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('student/resource')?>"><span class="mocks"></span>RESOURCE</a></li>
        <?php endif;?>
        <?php if(Yii::app()->session['challenge']):?>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'challenge')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('student/challenge')?>"><span class="chall"></span>CHALLENGE</a></li>
        <?php endif;?>
        <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'history')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('student/history')?>"><span class="history"></span>VIEW HISTORY</a></li>
        <!--<li <?php /*echo ($this->GetActiveMenu(Yii::app()->request->url,'leaderboard')?'class="active"':'');*/?>><a href="<?php /*echo Yii::app()->createUrl('student/Leaderboard')*/?>"><span class="history"></span>LEADERBOARD</a></li>-->
        <!--<li <?php /*//echo ($this->GetActiveMenu(Yii::app()->request->url,'home')?'class="active"':'');*/?>><a href="<?php /*echo Yii::app()->createUrl('user/logout')*/?>"><span class="logout glyphicon glyphicon-off"></span> LOGOUT</a></li>-->
    </ul>
    <?php endif;?>
</div>
<?php else:?>
<div class="col-sm-3 col-md-2 sidebar">
    <div class="outter"><img src="<?php echo Yii::app()->request->baseUrl.'/site/Imagerender/id/noimage.png?size=large&type=user&width=140&height=140';?>" class="img-circle">
        <h1>Guest</h1>

    </div>
        <ul class="nav nav-sidebar">
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'home')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('trial/home')?>" ><span class="revice"></span>REVISE</a></li>
            <li ><a href="javascript:void(0)" class="mocktest" data-toggle="modal" data-target="#guestmessage"><span class="mocks"></span>Worksheet</a></li>
            <li ><a href="javascript:void(0)" class="challengetest" data-toggle="modal" data-target="#guestmessageChallenge"><span class="chall"></span>CHALLENGE</a></li>
            <li <?php echo ($this->GetActiveMenu(Yii::app()->request->url,'history')?'class="active"':'');?>><a href="<?php echo Yii::app()->createUrl('trial/history')?>" ><span class="history"></span>VIEW HISTORY</a></li>
        </ul>
</div>
    <div class="modal fade" id="guestmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center">Worksheet</h4>
                </div>
                <div class="modal-body" id="myModalContent">
                    Worksheets are a list of set question papers.</br>
                    This function is available to registered users only.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="guestmessageChallenge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel" style="text-align: center">Challenge</h4>
                </div>
                <div class="modal-body" id="myModalContent">
                    Challenge is a computer adaptive test , which will increase in difficulty according to how you answer.</br>
                    You can also set challenge with your buddies and have friendly competitions.</br>
                    This function is available only to subscribed users.</br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php Yii::app()->clientScript->registerScript('guestscript', "
        $('.mocktest').click(function(){
            $('#myModalLabel').html('Mock');
            $('#myModalContent').html('<p>Mocks are a list of set question papers.</p><p>This function is available to registered users only.</p>');
        });
        $('.challengetest').click(function(){
            $('#myModalLabel').html('Challenge');
            $('#myModalContent').html('<p>Challenge is a computer adaptive test , which will increase in difficulty according to how you answer. You can also set challenge with your buddies and have friendly competitions.</p><p>This function is available only to subscribed users.</p>');
        });
    ");
endif;?>