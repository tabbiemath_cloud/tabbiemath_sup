<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Karthik
 * Date: 11/10/14
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
Yii::import('zii.widgets.CPortlet');

class LeftPanelWidget  extends CPortlet
{

    protected function renderContent()
    {
        $user=User::model()->findByPk(Yii::app()->user->id);
        $plans=Plan::model()->with(array('student'=>array('select'=>false,'joinType'=>'INNER JOIN','condition'=>'TM_SPN_StudentId='.Yii::app()->user->id)))->findAll(array('condition'=>'TM_SPN_Status=0','order'=>'TM_SPN_CreatedOn ASC'));
        $this->render('leftpanelwidget',array('user'=>$user,'plans'=>$plans));
    }
    public function GetActiveMenu($url,$needle)
    {
        if (strpos($url,$needle) !== false) {
            return true;
        }
    }
}