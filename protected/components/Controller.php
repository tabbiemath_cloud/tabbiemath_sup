<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/admincolumn2';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public static function GetParent()
    {

        if(Yii::app()->user->isParent())
        {
            return true;

        }
        else
        {
            return false;
        }
    }
    public static function GetTeacher()
    {

        if(Yii::app()->user->isTeacher())
        {
            return true;

        }
        else
        {
            return false;
        }
    }	
    public function getParentUser()
    {
        $user=User::model()->findByPk(Yii::app()->user->id);
        $return=array();
        if($user->usertype=='7')
        {

            $return[]=$user->username;
        }
       return $return;
    }
    public function getNotificationCount()
    {
        $count=Notifications::model()->count(array('condition'=>'TM_NT_User=:user AND TM_NT_Status=0 AND TM_NT_Type!=:level','params'=>array(':user'=>Yii::app()->user->id,':level'=>'Level')));
        return $count;
    }
    public function getNotifications()
    {
        $notifications=Notifications::model()->findAll(array('condition'=>'TM_NT_User=:user AND TM_NT_Type!=:level','params'=>array(':user'=>Yii::app()->user->id,':level'=>'Level'),'limit'=>20,'order'=>'TM_NT_Status ASC,TM_NT_Id DESC'));
        $notificationscount=Notifications::model()->count(array('condition'=>'TM_NT_User=:user AND TM_NT_Type!=:level','params'=>array(':user'=>Yii::app()->user->id,':level'=>'Level')));
        $html='';
        $lastid='';
        if(count($notifications)>0):
            foreach($notifications AS $item):
                switch($item->TM_NT_Type){
                    case "Level":
                        /*$level=Levels::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="">You have earned <span class="label label-warning" class="readnotification" data-id="'.$item->TM_NT_Id.'">'.$level->TM_LV_Name.'</span> Medal</a></li>';*/
                        break;
                    case "Reminder":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('home').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.' </span> has sent you a challenge reminder</a></li>';                        
                        break;                        
                    case "Approve":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('buddies/Buddylist').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.'</span> has approved your buddy request</a></li>';
                        break;
                    case "Request":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('buddies/Buddylist',array('type'=>'pending')).'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.'</span> has sent you a buddy request</a></li>';
                        break;
                    case "Challenge":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('home').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.' </span> has sent you a challenge request</a></li>';
                        break;
                    case "HWAssign":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('home').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.' </span> has assigned a homework</a></li>';
                        break;
                    case "HWSubmit":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('home').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.' </span> has submited a homework</a></li>';
                        break;  
                    case "HWReminder":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $homework=Schoolhomework::model()->findByPk($item->TM_NT_Item_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('home').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.' </span> has sent a reminder for '.$homework->TM_SCH_Name.'</a></li>';
                        break; 
                    case "HWMarking":
                        $user=User::model()->findByPk($item->TM_NT_Target_Id);
                        $homework=Schoolhomework::model()->findByPk($item->TM_NT_Item_Id);
                        $html.='<li  class="'.($item->TM_NT_Status!='2'?'unread':'').'"><a href="'.Yii::app()->createUrl('home').'" class="readnotification"  data-id="'.$item->TM_NT_Id.'"><span class="label label-warning">'.$user->username.' </span> has assigned marking of  '.$homework->TM_SCH_Name.'</a></li>';
                        break;                                                                      
                        

                }
                $lastid=$item->TM_NT_Id;
            endforeach;
            $html.='<input type="hidden" value="'.$lastid.'" id="lastitem">';
            if($notificationscount>20):
                $html.='<li class="loadmore"><a href="javascript:void(0)" class="text-center">View more</a></li>';
            endif;
        else:
            $html='<li>No notifications found.</li>';
        endif;
        return $html;
    }
    public function getLevels()
    {
        $studentlevels=Studentlevels::model()->findAll(array('condition'=>'TM_STL_Student_Id='.Yii::app()->user->id,'order'=>'TM_STL_Order DESC'));
        $html='';
        foreach($studentlevels AS $studentlevel):
            $level=Levels::model()->findByPk($studentlevel->TM_STL_Level_Id);
            $html.='<li data-toggle="tooltip" data-placement="bottom"
                                                  title="'.$level->TM_LV_Name.': Print Certificate"><a href="'.Yii::app()->createUrl('student/getpdfCertificate',array('levelId'=>$studentlevel->TM_STL_Level_Id)).'"><img src="'.Yii::app()->request->baseUrl.'/site/Imagerender/id/'.$level->TM_LV_icon.'?size=large&type=level&width=50&height=50" style="margin-top: 4px;"></a></li>';
        endforeach;
        return $html;

    }

}