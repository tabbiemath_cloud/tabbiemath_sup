<?php

class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANED=-1;
	
	/**
	 * The followings are the available columns in table 'users':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $activkey
	 * @var integer $createtime
	 * @var integer $lastvisit
	 * @var integer $superuser
	 * @var integer $usertype
	 * @var integer $status
	 * @var integer $publisher
	 * @var integer $school_id
	 * @var string passtemp
     * @var string location
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		
		return ((Yii::app()->getModule('user')->isAdmin())?array(
			array('username, password, email', 'required'),
			//array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),            
			array('username', 'unique', 'message' => UserModule::t("This users name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This email address already exists.")),
			///array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANED)),
			array('superuser', 'in', 'range'=>array(0,1)),
			//array('usertype', 'in', 'range'=>array(1,2,3,4,5,6,7)),
			array('username, email, createtime, lastvisit, superuser, status,usertype', 'required'),
			array('createtime, lastvisit, superuser, status,school_id', 'numerical', 'integerOnly'=>true),
            array('publisher,passtemp,location,school_id', 'safe'),
            array('username, email, createtime, lastvisit, superuser,usertype', 'safe', 'on'=>'editstudent'),
            array('status', 'checkplan', 'on'=>'editstudent')                                    
		):(/*(Yii::app()->user->id==$this->id)?array(
			array('username, email', 'required'),
			//array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('email', 'email'),
            array('publisher,school_id', 'safe'),
            array('email', 'unique', 'message' => UserModule::t("This email address already exists.")),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			//array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			//array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
		):*/((Yii::app()->getModule('user')->isSchoolAdmin())?array(
			array('username, email, createtime, lastvisit, superuser, status,usertype', 'required'),
			//array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),            
			array('username', 'unique', 'message' => UserModule::t("This users name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This email address already exists.")),
			//array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANED)),
			array('superuser', 'in', 'range'=>array(0,1)),
			//array('usertype', 'in', 'range'=>array(1,2,3,4,5,6,7)),
			array('username, email, createtime, lastvisit, superuser, status,usertype', 'required'),
			array('createtime, lastvisit, superuser, status,school_id', 'numerical', 'integerOnly'=>true),
            array('publisher,school_id', 'safe'),
            array('username, email, createtime, lastvisit, superuser,usertype', 'safe', 'on'=>'editstudent'),
            array('status', 'checkplan', 'on'=>'editstudent')                                    
		):array())));
	}
    public function checkplan($attribute)
    {
        if($this->$attribute=='1'):
            $plans=StudentPlan::model()->count(array('condition'=>"TM_SPN_StudentId='".$this->id."' AND TM_SPN_Status=0"));
            if($plans==0):
                $this->addError($attribute, 'Student does not have any active plan');                               
            endif;        
        endif;
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		$relations = array(
			'profile'=>array(self::HAS_ONE, 'Profile', 'user_id'),
            'admins'=>array(self::HAS_MANY, 'Adminstandards', 'TM_SA_Admin_Id'),
            'buddies'=>array(self::HAS_MANY, 'Buddies', 'TM_BD_Student_Id'),
            'homework'=>array(self::HAS_MANY, 'StudentHomeworks', 'TM_STHW_Student_Id'),
            'student'=>array(self::HAS_MANY, 'Student', 'TM_STU_User_Id'),
            'teacher'=>array(self::HAS_MANY, 'Teachers', 'TM_TH_User_Id'),
            'points'=>array(self::HAS_MANY, 'StudentPoints', 'TM_STP_Student_Id'),            
		);
		if (isset(Yii::app()->getModule('user')->relations)) $relations = array_merge($relations,Yii::app()->getModule('user')->relations);
		return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'username'=>UserModule::t("Username"),
			'password'=>UserModule::t("Password"),
			'verifyPassword'=>UserModule::t("Verify Password"),
			'email'=>UserModule::t("E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'id' => UserModule::t("Id"),
			'activkey' => UserModule::t("Activation key"),
			'createtime' => UserModule::t("Registration date"),
			'lastvisit' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Adminstrator"),
			'usertype' => UserModule::t("User Type"),
			'status' => UserModule::t("Status"),
            'userlevel' => UserModule::t("User Type"),
            'school_id' => UserModule::t("School"),
		);
	}
	
	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactvie'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANED,
            ),
            'superuser'=>array(
                'condition'=>'superuser=1',
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, activkey, createtime, lastvisit, superuser, status,location,userlevel',
            ),
        );
    }
	
	public function defaultScope()
    {
        return array(
            'select' => 'id, username, email, createtime, lastvisit, superuser, status,usertype,publisher,school_id,passtemp,location,userlevel',
        );
    }
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('Not active'),
				self::STATUS_ACTIVE => UserModule::t('Active'),
				self::STATUS_BANED => UserModule::t('Banned'),
			),
            'UserLevel' => array(
                '0' => UserModule::t('Regular'),
                '1' => UserModule::t('Professional'),
            ),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
			'UserTypes' => array(
				'1' => UserModule::t('Super Admin'),
				'2' => UserModule::t('Question Admin'),
				'3' => UserModule::t('Student Admin'),
				'4' => UserModule::t('Account Admin'),
				//'5' => UserModule::t('Account Admin'),
				//'6' => UserModule::t('Student'),
			),
			'StaffTypes' => array(
				'1' => UserModule::t('School Super Admin'),
				'2' => UserModule::t('School Admin'),
				'3' => UserModule::t('Support Admin'),
				////'6' => UserModule::t('Student'),
			),
            'SchoolStaffTypes' => array(
                //'1' => UserModule::t('School Super Admin'),
                '2' => UserModule::t('School Admin'),
                '3' => UserModule::t('Support Admin'),
                //'6' => UserModule::t('Student'),
            ),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

    public static function SubscriptionSchool($user)
    {
        $c = new CDbCriteria;
        $c->select = 't.TM_SPN_Id, t.TM_SPN_Publisher_Id,t.TM_SPN_StudentId,t.TM_SPN_PlanId,t.TM_SPN_SyllabusId,t.TM_SPN_StandardId,t.TM_SPN_SubjectId,t.TM_SPN_Status,t.TM_SPN_PaymentReference,t.TM_SPN_CouponId,t.TM_SPN_CreatedOn,t.TM_SPN_ExpieryDate,t.TM_SPN_CancelDate';
        $c->join = "INNER JOIN tm_school_plan tm_school_plan ON t.TM_SPN_PlanId = tm_school_plan.TM_SPN_PlanId";
        $c->compare("t.TM_SPN_StudentId", $user);

        $plan = StudentPlan::model()->find($c);
        $Plan = Plan::model()->findByPk($plan->TM_SPN_PlanId);

        if (count($Plan) > 0):


            $return = $Plan->TM_PN_Name;

            return $return;
        else:
            $return = 'Not subscription';
        endif;
    }

    public static function StandardSchool($user)
    {
        $c = new CDbCriteria;
        $c->select = 't.TM_SPN_Id, t.TM_SPN_Publisher_Id,t.TM_SPN_StudentId,t.TM_SPN_PlanId,t.TM_SPN_SyllabusId,t.TM_SPN_StandardId,t.TM_SPN_SubjectId,t.TM_SPN_Status,t.TM_SPN_PaymentReference,t.TM_SPN_CouponId,t.TM_SPN_CreatedOn,t.TM_SPN_ExpieryDate,t.TM_SPN_CancelDate';
        $c->join = "INNER JOIN tm_school_plan tm_school_plan ON t.TM_SPN_PlanId = tm_school_plan.TM_SPN_PlanId";
        $c->compare("t.TM_SPN_StudentId", $user);

        $standard = StudentPlan::model()->find($c);
        $Standard = Standard::model()->findByPk($standard->TM_SPN_StandardId);

        if (count($Standard) > 0):


            $return = $Standard->TM_SD_Name;

            return $return;

        endif;
    }
    public static function Subscription($user)
    {
        $studentplans=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId='.$user));
        $return='';
        if(count($studentplans)>0):
            foreach($studentplans AS $key=>$studentplan): 
                $Plan=Plan::model()->findByPk($studentplan->TM_SPN_PlanId);               
                $return.=($key==0?'':',').$Plan->TM_PN_Name;
            endforeach;
            return $return;
        else:
            $return='Not subscription';
        endif;        
    }

    public static function School($user)
    {        
             
        $school=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user));
        return ($school->TM_STU_School=='0'? $school->TM_STU_SchoolCustomname: School::model()->findByPk($school->TM_STU_School_Id)->TM_SCL_Name);              
    }
    public static function SchoolCustom($user)
    {        
             
        $school=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user));
        return ($school->TM_STU_School=='0'? $school->TM_STU_SchoolCustomname: School::model()->findByPk($school->TM_STU_School)->TM_SCL_Name);              
    }    
    public static function EmailStatus($user)
    {        
        $status=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user))->TM_STU_Email_Status;
        if($status=='1'):
            return 'Sent';
        else:
            return 'Not Sent';
        endif;
             
    } 
    public static function EmailStatusactive($user)
    {        
        $status=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user))->TM_STU_Email_Status;
        if($status=='1'):
            return 'disabled';
        else:
            return '';
        endif;
             
    }       
    public static function StaffSchool($user)
    {                 
        return School::model()->find(array('condition'=>'TM_SCL_Id='.$user))->TM_SCL_Name;      
    }
    public static function StaffRole($user,$school)
    {
        $schoolstaff=SchoolStaffs::model()->find(array('condition'=>'TM_SCS_School_Id='.$school.' AND TM_SCS_User_Id='.$user));
        $totals=User::itemAlias('StaffTypes');
        $roles=$totals[$schoolstaff->TM_SCS_Type];
        return $roles;
    }
    public static function StudentName($user)
    {        
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user));
        return $student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name;      
    }
    public static function StudentPassword($user)
    {
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user));
        $result='<a href="javascript:void(0);" class="showtooltip showpassword" id="showpassword'.$user.'" data-student="'.$student->TM_STU_Id.'" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Show/Hide password">
                <span id="maskpassword'.$student->TM_STU_Id.'" style="display: inline;">xxxxxxxx</span>
                <span class="unmaskpassword" id="unmaskpassword'.$student->TM_STU_Id.'" style="display: none;">'.$student->TM_STU_Password.'</span></a>';
        return $result;
    }
    public static function StaffName($user)
    {        
        $staff=Profile::model()->find(array('condition'=>'user_id='.$user));
        return $staff->firstname.' '.$staff->lastname;      
    }     
    public static function Creator($user)
    {        
        $creator=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user))->TM_STU_CreatedBy;
        if($creator!='0'):
            $user=User::model()->findByPk($creator)->profile;
            return $user->firstname.' '.$user->lastname; 
        else:
            return '';
        endif;
              
    } 
    public static function Updator($user)
    {        
        $creator=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user))->TM_STU_UpdatedBy;
        if($creator!='0'):
            $user=User::model()->findByPk($creator)->profile;
            return $user->firstname.' '.$user->lastname; 
        else:
            return '';
        endif;
              
    } 
    public static function Teacherupdator($user)
    {        
        $creator=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.$user))->TM_TH_UpdatedBy;
        if($creator!='0'):
            $user=User::model()->findByPk($creator)->profile;
            return $user->firstname.' '.$user->lastname; 
        else:
            return '';
        endif;
              
    }          
    public function GetUser($userid)
    {
        $username=User::model()->findByPk($userid)->username;
        return $username;
    }
    public function getAddurl($id)
    {
        return "<a href='".Yii::app()->createUrl('user/admin/parentaddstudent',array('id'=>$id))."'>Add Student</a>";
    }

    public function getManageurl($id)
    {
        return "<a href='".Yii::app()->createUrl('user/admin/studentlist',array('parentid'=>$id))."'>Manage Students</a>";
    }    
    public function getParent($userid)
    {
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$userid));
        $parent=User::model()->findByPk($student->TM_STU_Parent_Id)->profile;
        return $parent->firstname.' '.$parent->lastname;
    }
    public function getParentEmail($userid)
    {
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$userid));
        $parent=User::model()->findByPk($student->TM_STU_Parent_Id);
        return $parent->email;
    }
    public function getstandards($userid)
    {
        $c = new CDbCriteria;
        $c->select ='t.TM_SD_Id, t.TM_SD_Name';
        $c->join = "INNER JOIN tm_teacher_standards tm_teacher_standards ON t.TM_SD_Id = tm_teacher_standards.TM_TE_ST_Standard_Id";
        $c->compare("tm_teacher_standards.TM_TE_ST_User_Id", $userid);
        $result=Standard::model()->findAll($c);
        if(count($result)!=0):
            $standardname='';
            foreach($result AS $key=>$res):
            $standardname.=($key==0?$res->TM_SD_Name:','.$res->TM_SD_Name);
            endforeach;
            echo $standardname;
        else:
            echo "No standards assigned";
        endif;
    }    
    
}