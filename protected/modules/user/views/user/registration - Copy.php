<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
    UserModule::t("Registration"),
);
?>
<div class="row">
    <div class="col-md-8 col-md-offset-1">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo UserModule::t("Registration"); ?></h3>
            </div>
            <div class="panel-body">
                <?php if(Yii::app()->user->hasFlash('registration')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo Yii::app()->user->getFlash('registration'); ?>
                    </div>
                <?php endif;?>
                <?php $form=$this->beginWidget('UActiveForm', array(
                    'id'=>'registration-form',
                    'enableAjaxValidation'=>true,
                    'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
                    'htmlOptions' => array('enctype'=>'multipart/form-data'),
                )); ?>
                <fieldset>
                    <legend>Parent Details</legend>
                    <?php
                    $profileFields=$profile->getFields();
                    if ($profileFields) {
                        foreach($profileFields as $field) {
                            ?>
                            <div class="form-group">
<!--                                --><?php //echo $form->labelEx($profile,$field->varname); ?>
                                <?php
                                if ($field->widgetEdit($profile)) {
                                    echo $field->widgetEdit($profile);
                                } elseif ($field->range) {
                                    echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                                } elseif ($field->field_type=="TEXT") {
                                    echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                                } else {
                                    echo $form->textField($profile,$field->varname,array('class'=>'form-control','placeholder'=>$field->varname,'size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                }
                                ?>
                                <?php echo $form->error($profile,$field->varname); ?>
                            </div>
                        <?php
                        }
                    }
                    ?>
                    <div class="form-group">
                        <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'E-mail')); ?>
                        <?php echo $form->error($model,'email'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>'Username')); ?>
                        <?php echo $form->error($model,'username'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->PasswordField($model,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                        <?php echo $form->error($model,'password'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->PasswordField($model,'verifyPassword',array('class'=>'form-control','placeholder'=>'verify Password')); ?>
                        <?php echo $form->error($model,'verifyPassword'); ?>
                    </div>
                    <legend>Student Details</legend>
                    <div class="form-group">
                        <?php echo $form->textField($student,'TM_STU_First_Name',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                        <?php echo $form->error($student,'TM_STU_First_Name'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($student,'TM_STU_Last_Name',array('class'=>'form-control','placeholder'=>'Last Name')); ?>
                        <?php echo $form->error($student,'TM_STU_Last_Name'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($student,'TM_STU_School',array('class'=>'form-control','placeholder'=>'Name of School')); ?>
                        <?php echo $form->error($student,'TM_STU_School'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($student,'TM_STU_City',array('class'=>'form-control','placeholder'=>'City')); ?>
                        <?php echo $form->error($student,'TM_STU_City'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($student,'TM_STU_State',array('class'=>'form-control','placeholder'=>'State')); ?>
                        <?php echo $form->error($student,'TM_STU_State'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->dropDownList($student,'TM_STU_Country_Id',CHtml::listData(Country::model()->findAll(array('condition' => 'TM_CON_Status=0','order' => 'TM_CON_Name')),'TM_CON_Id','TM_CON_Name'),array('empty'=>'Select Country','class'=>'form-control')); ?>
                        <?php echo $form->error($student,'TM_STU_Country_Id'); ?>
                    </div>
                    <?php if (UserModule::doCaptcha('registration')): ?>
                        <div class="form-group">
                            <?php echo $form->textField($model,'verifyCode',array('class'=>'form-control','placeholder'=>'Verification Code')); ?>
                            <?php echo $form->error($model,'verifyCode'); ?>
                            <?php $this->widget('CCaptcha'); ?>


                            <p class="hint"><?php echo UserModule::t("Please enter the letters as they are shown in the image above."); ?>
                                <br/><?php echo UserModule::t("Letters are not case-sensitive."); ?></p>
                        </div>
                    <?php endif; ?>
                    <!-- Change this to a button or input when using this as a form -->
                    <?php echo CHtml::submitButton(UserModule::t("Register "),array('class'=>'btn btn-warning btn-lg btn-block')); ?>

                </fieldset>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

