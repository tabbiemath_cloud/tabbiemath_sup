<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
    UserModule::t("Registration"),
);
?>
<style>
	.clry{
    background-color: #FFF !important;
    color: #999 !important;
    border: 1px solid #BDBAB7 !important;
    }
	.totalbord{border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;}

::-webkit-input-placeholder {
 color: #fff !important;
}
:-moz-placeholder { /* older Firefox*/

 color: #fff !important;

}
::-moz-placeholder { /* Firefox 19+ */ 
 color: #fff !important; 
} 
:-ms-input-placeholder { 
 color: #fff !important;
}

.bgrg_form{    padding: 15px;
    margin: 0 auto;
    background-color: rgba(71, 71, 71, 0.78);
    border-radius: 8px;
    border: 1px solid rgba(130, 130, 130, 0.18);
    color: white;}
 
 
 .reg_bdy{background-color: rgba(71, 71, 71, 0.78);
    border-radius: 8px;
    color: white !important;    padding-right: 20px;
    padding-left: 20px;}
	
	.colorgraph {
  height: 1px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}
	
	.errorMessage2 {
    padding: 5px;   
    color: #F0776C;  
}
.table td
{
    border-top: 0px !important;
      border-bottom: 1px solid rgba(255, 251, 251, 0.14);
}
</style>
<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "ZDU6OY"; //Please change this value with live key for production
$hash_string = '';
// Merchant Salt as provided by Payu
$SALT = "g4KQpESl"; //Please change this value with live salt for production

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";

$action = '';
$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
    foreach($_POST as $key => $value) {
        $posted[$key] = $value;

    }
}
//print_r($posted);exit;
if(empty($posted['txnid'])) {
    // Generate random transaction id
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
    $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
    $hashVarsSeq = explode('|', $hashSequence);
    foreach($hashVarsSeq as $hash_var) {
        $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
        $hash_string .= '|';
    }
    $hash_string .= $SALT;
    //echo $hash_string;exit;
    $hash = strtolower(hash('sha512', $hash_string));
    //echo $hash;exit;
    $action = $PAYU_BASE_URL . '/_payment';
} elseif(!empty($posted['hash'])) {
    $hash = $posted['hash'];
    $action = $PAYU_BASE_URL . '/_payment';
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-6 reg_bdy col-md-offset-3">
            <h2 class="text-center">Add a Subscription</h2>
            <hr class="colorgraph">
            <?php echo (Yii::app()->session['location']=='0'?CHtml::beginForm($action,'post',array('enctype'=>'multipart/form-data','id'=>'sbuscribeform')):CHtml::beginForm(Yii::app()->request->baseUrl.'/payment/process.php','post',array('enctype'=>'multipart/form-data','id'=>'sbuscribeform'))); ?>
                    <div class="col-md-12">
                        <div class="panel-body">                        
                            <fieldset>
                                <?php 

                                $publisherscount=Publishers::model()->with('plan')->count(array('condition'=>'TM_PR_Status=0 AND TM_PN_Visibility=0','order' => 'TM_PR_Name'));
                                if($publisherscount==1):
                                    $publishers=Publishers::model()->with('plan')->find(array('condition'=>'TM_PR_Status=0 AND TM_PN_Visibility=0','order' => 'TM_PR_Name'));
                                     echo CHtml::activehiddenField($model,'TM_SPN_Publisher_Id',array('value' => $publishers->TM_PR_Id));?>
                                    <div class="form-group">
                                            <?php 

                                            $syllabuses= Syllabus::model()->with('plan')->findAll(array('condition'=>'TM_SB_Status=0 AND TM_PN_Visibility=0','order' => 'TM_SB_Name'));
        /*syllabuses even when there are no active plans starts*/
                $plan_ids =array();                         
                foreach ($syllabuses as $key => $syllabuse) {

                 $data=Standard::model()->with('plan')->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0' AND TM_PN_Visibility=0",array(':syllabus'=>$syllabuse->TM_SB_Id));
                        $standard_ids=array();
                        foreach($data as $value=>$name)
                        {                
                            if(Yii::app()->session['location']==0):
                                $plancount=Plan::model()->count(array('condition'=>'TM_PN_Standard_Id='.$value.' AND TM_PN_Publisher_Id='.$publishers->TM_PR_Id.' AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND (TM_PN_Currency_Id=1 OR TM_PN_Additional_Currency_Id=1)'));
                            else:
                                $plancount=Plan::model()->count(array('condition'=>'TM_PN_Standard_Id='.$value.' AND TM_PN_Publisher_Id='.$publishers->TM_PR_Id.' AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND (TM_PN_Currency_Id!=1 OR TM_PN_Additional_Currency_Id NOT IN(0,1))'));
                            endif;                
                            if($plancount!=0):
        if($publishers->TM_PR_Id!='')
        {
            if(Yii::app()->session['location']=='0'):
                $condition="TM_PN_Publisher_Id=:publisher AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND TM_PN_Type=0 AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND (TM_PN_Currency_Id=1 OR TM_PN_Additional_Currency_Id=1)";
            else:
                $condition="TM_PN_Publisher_Id=:publisher AND TM_PN_Visibility=0 AND TM_PN_Type=0 AND TM_PN_Type=0 AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND (TM_PN_Currency_Id!=1 OR TM_PN_Additional_Currency_Id NOT IN(0,1))";
            endif;             
            $data=Plan::model()->with('currency')->findAll(
                array(
                        'condition'=>$condition,
                        'params'=>array(':publisher'=>$publishers->TM_PR_Id,':syllabus'=>$syllabuse->TM_SB_Id,':standard'=>$value)
                )
            );

            if(count($data)>0)
            {
                 array_push($plan_ids,$syllabuse->TM_SB_Id);
            }
        } 
                            endif;

                        }

                }
$syllabus= implode(",",$plan_ids);
  /*syllabuses even when there are no active plans ends*/
                                            echo CHtml::activeDropDownList($model,'TM_SPN_SyllabusId',CHtml::listData(Syllabus::model()->with('plan')->findAll(array('condition'=>'TM_SB_Status=0 AND TM_SB_Id IN ('.$syllabus.')','order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty' => 'Select Syllabus','class'=>'form-control clry validate','data-item'=>'Syllabus')); ?>
                                            <?php echo CHtml::error($model,'TM_SPN_SyllabusId'); ?>                                    
                                    </div>                                      
                                <?php else:?>                   
                                <div class="form-group">
                                        <?php echo CHtml::activeDropDownList($model,'TM_SPN_Publisher_Id',CHtml::listData(Publishers::model()->with('plan')->findAll(array('condition'=>'TM_PR_Status=0 AND TM_PN_Visibility=0','order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),array('empty' => 'Select Publisher','class'=>'form-control clry validate','data-item'=>'Publisher')); ?>
                                        <?php echo CHtml::error($model,'TM_SPN_Publisher_Id'); ?>                                                                                                            
                                </div>
                                <div class="form-group">
                                        <?php echo CHtml::activeDropDownList($model,'TM_SPN_SyllabusId',array(''=>'Select Syllabus'),array('empty' => 'Select Syllabus','class'=>'form-control clry validate','data-item'=>'Syllabus')); ?>
                                        <?php echo CHtml::error($model,'TM_SPN_SyllabusId'); ?>                                    
                                </div>                                 
                                <?php endif; ?>

                                <div class="form-group"> 
                                    <?php echo CHtml::activeDropDownList($model,'TM_SPN_StandardId',array(''=>'Select Standard'),array('empty' => 'Select Standard','class'=>'form-control clry validate','data-item'=>'Standard')); ?>
                                    <?php echo CHtml::error($model,'TM_SPN_StandardId'); ?>                                  
                                </div>
                                <div class="form-group">
                                    <?php echo CHtml::activeDropDownList($model,'TM_SPN_PlanId',array(''=>'Select Subscription'),array('class'=>'form-control clry validate','data-item'=>'Plan')); ?>
                                    <?php echo CHtml::activeHiddenField($model,'TM_SPN_CouponId'); ?>
                                    <?php echo CHtml::error($model,'TM_SPN_PlanId'); ?>                                                                    
                                </div> 
                                                                                      
                            </fieldset>                        
                        </div>
                        <div class="row">  
                          <div class="col-md-12">
                          <p class="hint" style="text-align: justify;"><?php echo UserModule::t("Adding a subscription will give unlimited access to the Tabbie modules enabled for that subscription, for the duration specified. "); ?></p>  
                          </div>
                        </div>
                        <div class="row" id="planrow">  

                        </div>
                        <div class="row totalbord" style="border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;">  
                          <div class="col-md-6 pull-left">
                        	<h3 class="text-left">Total</h3>
                          </div>
                          <div class="col-md-6 pull-right">
                        	<h3 class="text-right"><strong id="totalrate">0</strong></h3>
                          </div>
                        </div>
                        <div class="row" style="margin-top: 15px;text-align: right;">  
                        
                        	    <?php if(Yii::app()->session['location']=='0'):?>
                                    <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                                    <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                                    <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
                                    <input type="hidden" name="productinfo" value="Student Subscription" />
                                    <input type="hidden" name="surl" value="https://app.tabbiemath.com/response.php" />   <!--Please change this parameter value with your success page absolute url like http://mywebsite.com/response.php. -->
                                    <input type="hidden" name="furl" value="https://app.tabbiemath.com/response.php" />
									<input type="hidden" name="udf4" value="<?php echo base64_encode($id);?>">
									<input type="hidden" name="username" value="<?php echo $parent->username;?>">                                            
									
                                    <input type="hidden" name="amount" id="amounthidden" value="<?php if(isset($_POST['amount']))echo $_POST['amount']; ?>">
                                    <input type="hidden" name="udf2" id="planid"  value="<?php if(isset($_POST['StudentPlan']['TM_SPN_PlanId']))echo $_POST['StudentPlan']['TM_SPN_PlanId']; ?>">
                                    <input type="hidden" name="udf3" id="voucheridhidden" value="<?php if(isset($_POST['udf3']))echo $_POST['udf3']; ?>">



									<input type="hidden" name="udf1" value="register">
									
									
									<input type="hidden" class="form-control subscripvalid" name="phone"   data-item="Phone Number"   value="<?php echo ($parent->phonenumber==''?'000000000':$parent->phonenumber);?>">
									<input type="hidden" class="form-control subscripvalid" name="firstname" data-item="First Name" value="<?php echo $parent->firstname;?>">
									<input type="hidden" class="form-control subscripvalid" name="lastname"  data-item="Last Name" value="<?php echo $parent->lastname;?>">
									<input type="hidden" class="form-control subscripvalid" name="email"  data-item="Email"  value="<?php echo $parent->email;?>"> 
									<button type="button" data-toggle="modal" data-target="#addvoucher" class="btn btn-default " style="margin-bottom: 10px;min-width: 159px;">Redeem Voucher</button>
									<?php echo CHtml::submitButton(UserModule::t("Continue to Payment"),array('class'=>'btn btn-warning submit-payment-payu','style'=>'margin-bottom: 10px;')); ?>
								<?php else:?>
                                    <input type="hidden" name="idst" value="<?php echo base64_encode($id);?>">
									<input type="hidden" name="cmd" value="_xclick" />
									<input type="hidden" name="no_shipping" value="1" />
									<input type="hidden" name="no_note" value="1" />
									<!--<input type="hidden" name="lc" value="UK" />-->
									<input type="hidden" name="currency_code" id="currency_code" value="" />
									<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
									<input type="hidden" name="name" value="<?php echo $parent->firstname.' '.$parent->lastname;?>"  />                                    																		
									<input type="hidden" name="email" value="<?php echo $parent->email;?>">
									<input type="hidden" name="phone" value="<?php echo $parent->phonenumber;?>">
									<input type="hidden" name="item_name" id="item_name"  value="" / >
									<input type="hidden" name="amount" id="amounthidden" value="">                                    
									<input type="hidden" name="custom"  id="custom" value="<?php echo base64_encode($id);?>" / >
									<input type="hidden" name="plan" id="planid"  value="">
                                    
                                    <input type="hidden" name="stripeBillingName" value="<?php echo $parent->firstname.' '.$parent->lastname;?>"  />
                                    <input type="hidden" name="stripeEmail" value="<?php echo $parent->email;?>"  />
                                    <input type="hidden" name="source" value="registration"  />
                                    
									<input type="hidden" name="voucherid" id="voucheridhidden" value="">                                   									
									<button type="button" data-toggle="modal" data-target="#addvoucher" class="btn btn-default " style="margin-bottom: 10px;min-width: 159px;">Redeem Voucher</button>
                                    <br />   									
                                    <?php echo CHtml::submitButton(UserModule::t("Pay with Credit/Debit Card"),array('class'=>'btn btn-warning','style'=>'margin-bottom: 10px;','id'=>'stripe-button')); ?>
                                    <?php echo CHtml::submitButton(UserModule::t("Pay with PayPal"),array('class'=>'btn btn-warning','style'=>'margin-bottom: 10px;','id'=>'paypalbutton')); ?>
                                                                                                            									
								<?php endif;?>	
                        </div>
                        <div class="row"  style="margin-top: 15px;">  
                          <span class="pull-right" style="text-align: right;">
                            <?php if(Yii::app()->session['location']=='0'): ?>
                        	  <p class="hint"><?php echo UserModule::t("Secure Payment with Payu."); ?><img  src="<?php echo Yii::app()->request->baseUrl.'/images/pauylogo.jpg';?>" style="width: 80px;margin-left: 5px;"/></p>
                            <?php else:
                            /*?>
                               <p class="hint"><?php echo UserModule::t("Secure Payment with PayPal."); ?><img  src="<?php echo Yii::app()->request->baseUrl.'/images/paypal_lgo.jpg';?>" style="width: 80px;margin-left: 5px;"/></p> 
                               <p class="hint" style="text-align: justify;color: white;">NOTE: If you are unable to complete payment through the automated payment engine, not to worry. Just send a note to support@tabbiemath.com, and we can register you in manually.</p> 
                            <?php */
                            endif;?>
                        <span>
                        </div>                                                                                                                        
                                                                      
                    </div>                        
            <?php echo CHtml::endForm(); ?>         
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="addvoucher" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h3 class="modal-title" id="mySmallModalLabel">Redeem Voucher</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <dl>
                                <dt>Enter voucher code</dt>   
                                <dd>                                
                                    <input id="voucher" name="voucher" class="form-control voucher"  data-item="Voucher"/>
                                </dd>
                            </dl>
                        </div>
                        <div class="col-md-6 col-sm-6 vouchererror">
                        
                        </div>
                    </div>
                </div>
                <div class="modal-footer">                                            
                        <button type="button" class="btn btn-warning redeem">Redeem</button>
                        <button type="button" class="btn btn-warning vouchercancel" data-dismiss="modal">Cancel</button>                    
                </div>
            </div><!-- /.modal-content -->

        </div>

    </div>    
</div>
<div class="overlay"></div> 
<img src="<?php echo Yii::app()->request->baseUrl;?>/images/preloader.gif" id="loader">
<script src="https://checkout.stripe.com/checkout.js"></script>  
<?php 

Yii::app()->clientScript->registerScript('jsfunctions', "  
    $(document).on('change','#StudentPlan_TM_SPN_SyllabusId',function(){
    var publisher=$('#StudentPlan_TM_SPN_Publisher_Id').val();

        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('site/GetStandardList')."',
          data: { id:$(this).val(),publisher: publisher}
            })
        .done(function( html ) {            
            $('#StudentPlan_TM_SPN_StandardId').html(html);
            $('#StudentPlan_TM_SPN_PlanId').html('<option >Select Subscription</option>');
            
        });
    })
    $(document).on('change','#StudentPlan_TM_SPN_Publisher_Id',function(){

        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('site/GetSyllabusList')."',
              data: { id:$(this).val()}
            })
        .done(function( html ) {
            $('#StudentPlan_TM_SPN_SyllabusId').html(html);
            $('#StudentPlan_TM_SPN_PlanId').html('<option >Select Subscription</option>');

        });
    })
    $(document).on('change','#StudentPlan_TM_SPN_StandardId',function(){

        var publisher=$('#StudentPlan_TM_SPN_Publisher_Id').val();
        var syllabus=$('#StudentPlan_TM_SPN_SyllabusId').val();
        var standard=$('#StudentPlan_TM_SPN_StandardId').val();
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('site/GetPlanList')."',
              data: { publisher:publisher,syllabus:syllabus,standard:standard}
            })
        .done(function( html ) {
            $('#StudentPlan_TM_SPN_PlanId').html(html);
        });
    });
    $(document).on('change','#StudentPlan_TM_SPN_PlanId',function(){
        var voucher=$('#StudentPlan_TM_SPN_CouponId').val();
        var plan=$(this).val();
        if(plan!='')
        {
            getRate(plan,voucher);    
        }
        else
        {
            $('#planrow').html('<td colspan=\"2\"></td>');
            $('#totalrate').text('0');                        
        }
        
    });
    $(document).on('click','.redeem',function(){        
        var voucher=$('#voucher').val();
        var plan=$('#StudentPlan_TM_SPN_PlanId').val(); 
        var currency=$('#currencyid').val();       
        if(voucher=='')
        {
            $('.vouchererror').html('<div class=\"alert alert-danger voucheralert\" role=\"alert\">Enter vocher code</div>');
            setTimeout(function(){                        
                        $('.vouchererror').html('');
            },5000)
        }
        if(plan=='')
        {
            $('.vouchererror').html('<div class=\"alert alert-danger voucheralert\" role=\"alert\">Select a plan</div>');
            setTimeout(function(){                        
                        $('.vouchererror').html('');
            },5000)
        }
        if(voucher!='' & plan!='')
        {
            $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('site/GetCouvherRate')."',                  
                  dataType:'json',
                  data: { plan:plan,voucher:voucher,currency:currency}
                })
            .done(function( data ) {                
                if(data.error=='no')
                {                    
                    $('#voucheridhidden').val(data.voucher);
                    $('#StudentPlan_TM_SPN_CouponId').val(data.voucher);
                    $('.vouchererror').html(data.message);
                    setTimeout(function(){                        
                        $('.voucher').val('')
                        $('.vouchererror').html('');
                        $('.vouchercancel').trigger('click'); 
                        getRate(plan,data.voucher)                       
                    },5000)                    
                }
                else
                {                    
                    $('.vouchererror').html(data.error);                    
                    setTimeout(function(){
                        $('.voucher').val('')
                        $('.vouchererror').html('');
                    },5000)
                }
            });
        }
    })  
    $('#sbuscribeform').submit(function(){
        
        var error=0;
        $('.validationalert').remove();        
        $('.validate').each(function(){
            if($(this).val()=='')
            {
               $(this).after('<div class=\"alert alert-danger validationalert\" role=\"alert\">Select a '+$(this).attr('data-item')+'</div>');
               error++; 
            }                        
        });
        if(error==0)
        {
            if($('#planid').val()!='' & $('#amounthidden').val()==0)
            {
                $(this).attr('action','".Yii::app()->request->baseUrl."/user/registration/PaymentComplete');
                return true;
            }
            else
            {
                return true;
            }
            
        }
        else
        {
            return false;
                    
        }                                                   
        
    });
    function getRate(plan,voucher)
    {       
        $.ajax({
            type: 'POST',
            url: '".Yii::app()->createUrl('site/GetPlanDetails')."',                  
            dataType:'json',              
            data: { plan:plan,voucher:voucher,school:".$parent->student_school."},
            beforeSend: function() {
                $('#planrow').html('<td colspan=\'2\' align=\'center\'><img src=\'".Yii::app()->request->baseUrl."/images/imageloader.gif\'></td>');         
            },              
        })
        .done(function( data ){                
            if(data.error=='no')
                $('#planid').val(data.planid);            
            {                    
                $('#couponrow').remove();
                $('#planrow').html(data.plandetails);
                if(data.coupon=='yes')
                {
                   $('#planrow').after(data.coupondetails) 
                }
                if(data.schoolcoupon=='yes')
                {
                   $('#planrow').after(data.coupondetails) 
                    $('#voucheridhidden').val('-1');                    
                }
                
                $('#totalrate').text(data.plancurrency+' '+data.planrate);
                $('#amounthidden').val(data.planrate);
                $('#item_name').val(data.planname);
                $('#currency_code').val(data.plancurrency);                                                                                     
                if(data.planrate==0)
                {
                  $('#paypalbutton').hide();
                  $('#stripe-button').val('Continue');

                }
                $('#custom').val('register-".$id."'+'-'+data.planid+'-'+'". md5($parent->username)."-'+voucher);
            }
        });         
    }    
 ");
 ?>   

<script>
        $('#stripe-button').click(function(){
            if($('#planid').val()!='' & $('#amounthidden').val()==0)
            {                
                $('#sbuscribeform').attr('action', "<?php echo Yii::app()->request->baseUrl.'/user/registration/PaymentComplete';?>");
                $('#sbuscribeform').submit(); 
                $('.overlay').show();
                $('#loader').show();                               
            }
            else
            {
               var handler = StripeCheckout.configure({
            key: 'pk_live_wcSYngDrinVh1yamt56OA5vB',            
            locale: 'auto',
            token: function(token) {
                
                var $id = $('<input type=hidden name=stripeToken />').val(token.id);            
                $('#sbuscribeform').attr('action', "<?php echo Yii::app()->request->baseUrl.'/stripe/charge.php';?>");
                $('.overlay').show();
                $('#loader').show();
                $('#sbuscribeform').append($id).submit();
            }
          });            
          var amount = ($('#amounthidden').val()*100);
          amount=amount.toFixed(0);
          var currency=$('#currency_code').val();
          handler.open({            
            amount:      amount,            
            email:'<?php echo $parent->email;?>',
            name:        'Tabbieme Subscription',
            //image:       '{% static "img/marketplace.png" %}',
            //description: 'Purchase Products',
            panelLabel:  'Subscribe',            
            allowRememberMe:false,
            currency:currency, 					
			billingAddress:true,            
            });

          return false;
            }            
        
        });
</script>
<script>
    $(document).ready(function(){
        var hash = '<?php echo $hash ?>';
        if(hash == '') {
            return;
        }
        // setInterval(function() { $('.submit-payment-payu').trigger('click'); }, 5000);
        document.getElementById("sbuscribeform").submit();// Form submission
    });
</script>