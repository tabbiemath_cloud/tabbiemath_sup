<?php
$this->breadcrumbs=array(
	UserModule::t("Users"),
);
$this->menu=array(
    array('label'=>'Manage Users', 'class'=>'nav-header'),
    array('label'=>'List User', 'url'=>array('/user/admin')),
    array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),

);
?>
<?php if(UserModule::isAdmin()) {
    ?><!--<ul class="actions">
    <li><?php /*echo CHtml::link(UserModule::t('Manage User'),array('/user/admin')); */?></li>
    <li><?php /*echo CHtml::link(UserModule::t('Manage Profile Field'),array('profileField/admin')); */?></li>
</ul>--><!-- actions --><?php
} ?>
<div class="row-fluid">
    <div class="col-lg-12">
        <h3><?php echo UserModule::t("List User"); ?></h3>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                array(
                    'name' => 'username',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id))',
                ),
                array(
                    'name' => 'createtime',
                    'value' => 'date("d.m.Y H:i:s",$data->createtime)',
                ),
                array(
                    'name' => 'lastvisit',
                    'value' => '(($data->lastvisit)?date("d.m.Y H:i:s",$data->lastvisit):UserModule::t("Not visited"))',
                ),
            ),
        )); ?>
    </div>
</div>
