  <img class="loghed1" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/>
<!--    <div class="alert alert-danger" style="text-align:center">
   <strong>TabbieMath will not be available on Friday  15/05/2020, between 9 am – 11 am IST due to essential application upgrade. Apologies for any inconvenience and thank you for your cooperation.</strong>
</div>  -->
       <?php echo CHtml::beginForm('', 'post', array('class'=>'form-signin')); ?>
	
        <h2 class="form-signin-heading"><?php echo UserModule::t("Login"); ?></h2>
		 <?php if(Yii::app()->user->hasFlash('activation')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo Yii::app()->user->getFlash('activation'); ?>
                    </div>
          <?php endif;?>
        
		<label for="inputEmail" class="sr-only">Email address</label>       
        <?php echo CHtml::activeTextField($model,'username',array('type'=>'email','class'=>'form-control brd_bot_non','placeholder'=>'Please enter your username or email')) ?>
                            <?php echo CHtml::error($model,'username'); ?>
		
		<label for="inputPassword" class="sr-only">Password</label>
        <?php echo CHtml::activePasswordField($model,'password',array('type'=>'password','class'=>'form-control','placeholder'=>'Password')) ?>
                            <?php echo CHtml::error($model,'password'); ?>
                            <?php echo CHtml::error($model,'status'); ?>
        
		<div class="checkbox">
          <label class="sub_line">
            <?php echo CHtml::link(UserModule::t("Forgot Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
          </label>
		  
		  <label class="visible-xs sub_line">Do not have an account? <?php echo CHtml::link(UserModule::t("Register "),'javascript:void(0)',array('class' => 'registerclick')); ?></label>
        </div>
        
		<?php echo CHtml::submitButton(UserModule::t("Login"),array('class'=>'btn btn-warning btn-lg btn-block')); ?>
		
      <?php echo CHtml::endForm(); ?> 
    </div>	

	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Please choose your location</h3>
      </div>
      <div class="modal-body">
        <ul class="list-group">
          <li class="list-group-item"><a href="<?php echo Yii::app()->createUrl("user/registration/setlocation",array("id"=>1));?>"><b>India</b></a></li>
          <li class="list-group-item"><a href="<?php echo Yii::app()->createUrl("user/registration/setlocation",array("id"=>1));?>"><b>Outside India</b></a></li>
        </ul> 
		<p>Please Note: We ask you this information so that we can route you the appropriate secure payment gateway</p>	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>
<script>
$(function(){
    $('.registerclick').click(function(){
        $('#myModal').modal('show')
            
    })
    
})
</script>