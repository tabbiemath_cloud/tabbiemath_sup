<?php
$this->breadcrumbs=array(
	(UserModule::t('Users'))=>array('adminlist'),
	$model->username=>array('view','id'=>$model->id),
	(UserModule::t('Update')),
);
$this->menu=array(
    array('label'=>'Manage Users', 'class'=>'nav-header'),
//    array('label'=>'Create User', 'url'=>array('create')),
    array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
//    array('label'=>'List User', 'url'=>array('/user')),
    array('label'=>'List User', 'url'=>array('adminlist')),
//    array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),

);
?>

<h3><?php echo  UserModule::t('Update User')." ".$model->id; ?></h3>

<?php /*echo $this->renderPartial('_menu', array(
		'list'=> array(
			CHtml::link(UserModule::t('Create User'),array('create')),
			CHtml::link(UserModule::t('View User'),array('view','id'=>$model->id)),
		),
	)); */

	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile,'standards'=>$standards,'schools'=>$schools)); ?>