<?php
$this->breadcrumbs=array(
	UserModule::t('Student')=>array('studentlist'),
	UserModule::t('Manage'),
);
$session=new CHttpSession;
$session->open(); 
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List Students', 'url'=>array('studentlist'.($session['urlstring']!=''?'?'.$session['urlstring']:''))),
    array('label'=>'Change Password', 'url'=>array('studentpassword','id'=>$model->id)),    
    array('label'=>'Edit Student', 'url'=>array('Editstudent','id'=>$model->id)),
    array('label'=>'Register Student', 'url'=>array('addstudent')),    
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<h3>View <?php echo $student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover" id="yw0">
            <tr>
                <th>Parent</th>
                <td><?php echo $parent->profile->firstname.' '.$parent->profile->lastname;?></td>
            </tr>
            <tr>
                <th>Username</th>
                <td><?php echo $model->username;?></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><?php echo $student->TM_STU_First_Name;?></td>
            </tr>
            
            <tr>
                <th>Last Name</th>
                <td><?php echo $student->TM_STU_Last_Name;?></td>
            </tr>
            <tr>
                <th>Name of School</th>
                <td><?php echo ($student->TM_STU_School=='0'? $student->TM_STU_SchoolCustomname : School::model()->findByPk($student->TM_STU_School)->TM_SCL_Name);?></td>
            </tr>
            <tr>
                <th>City</th>
                <td><?php echo $student->TM_STU_City;?></td>
            </tr>
            <tr>
                <th>State</th>
                <td><?php echo $student->TM_STU_State;?></td>
            </tr>
            <?php if($student->TM_STU_Country_Id!=''):  ?>          
            <tr>
                <th>Country</th>
                <td><?php                                
                $country=Country::model()->find(array('condition'=>'TM_CON_Id='.$student->TM_STU_Country_Id));
                echo $country->TM_CON_Name;?></td>
            </tr>
            <tr>
                <th>User Type</th>
                <td><?php echo User::itemAlias("UserLevel",$model->userlevel);?></td>
        </tr>
            <?php endif;?>
            <?php if(count($plans)>0):?>                      
            <tr>
                <th colspan="2" style="text-align: center;">Subscriptions</th>                
            </tr>
            <tr>
                <td colspan="2">
                <table width="100%" class="table table-bordered table-hover">
                    <tr>
                        <th>Syllabus</th>
                        <th>Standard</th>
                        <th>Plan</th>
                        <th>Activated On</th>
                        <th>Status</th>                        
                    </tr>
                    <?php foreach($plans AS $plan):?>
                    <tr>
                        <td><?php echo Syllabus::model()->findByPk($plan->TM_SPN_SyllabusId)->TM_SB_Name;?></td>
                        <td><?php echo Standard::model()->findByPk($plan->TM_SPN_StandardId)->TM_SD_Name;?></td>
                        <td><?php echo Plan::model()->findByPk($plan->TM_SPN_PlanId)->TM_PN_Name;?></td>
                        <td><?php echo date('d-m-Y',strtotime($plan->TM_SPN_CreatedOn));?></td>
                        <td><?php echo ($plan->TM_SPN_Status=='0'?'Active':'Inactive');?></td>                        
                    </tr>
                    <?php endforeach;?>
                </table>
                </td>
            </tr>
            <?php else:?>
             <tr>
                <th >Subscriptions</th>
                <td >No Subscriptions</td>                
            </tr>
            <?php endif;?>
        </table>
    </div>
</div>    