
    <div class="col-lg-12">
        <h3><?php echo $student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name.' Plans'; ?></h3>
        <div class="col-lg-4 pull-left" style="margin-bottom: 10px;padding-left: 0px;;">            
                <a href="<?php echo Yii::app()->createUrl('user/admin/addplan',array('id'=>$student->TM_STU_User_Id))?>" class="btn btn-warning btn-block">Add subscription</a>            
        </div>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,
            'columns'=>array(                
                array(
                    'name'=>'Plan',
                    'type'=>'raw',
                    'value'=>'Plan::model()->findByPk($data->TM_SPN_PlanId)->TM_PN_Name',
                ),
                'TM_SPN_PaymentReference',     
                array(
                    'name' => 'TM_SPN_CouponId',
                    'value' => 'Coupons::model()->Getcouponname($data->TM_SPN_CouponId)',
                ),  
                array(
                    'name' => 'TM_SPN_Currency',
                    'value' => 'StudentPlan::model()->GetCurrency($data->TM_SPN_Currency)',
                ),				
                array(
                    'name' => 'Subscribed On',
                    'value' => 'date("d-m-Y",strtotime($data->TM_SPN_CreatedOn))',
                ), 
                                                          
                array(
                    'name' => 'expires On',
                    'type'=>'raw',
                    'value'=>'StudentPlan::model()->GetExpiredate($data->TM_SPN_ExpieryDate)',
                ),
                array(
                    'name' => 'TM_SPN_Provider',
                    'type'=>'raw',
                    'value'=>'StudentPlan::model()->GetGateway($data->TM_SPN_Provider)',                    
                ),                                           
                array(
                    'name' => 'Cancelled On',
                    'type'=>'raw',
                    'value'=>'StudentPlan::model()->GetCancelldate($data->TM_SPN_CancelDate)',                    
                ), 
                array(
                    'name'=>'status',
                    'value'=>'StudentPlan::itemAlias("SubsciptionStatus",$data->TM_SPN_Status)',
                ),                                       
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{activate}{inactivate}{deleteplan}',//
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (                        
                        'deleteplan'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',                            
                            'options'=>array(
                                'title'=>'Delete Subscription',
                                'class'=>'deleteplan'
                            ),
                            'url'=>'Yii::app()->createUrl("user/admin/deleteplan",array("id"=>$data->TM_SPN_Id,"student"=>$data->TM_SPN_StudentId))',
                        ),
                        'activate'=>array(
                            'label'=>'<span class="glyphicon glyphicon-ok"></span>',
                            'visible'=>'$data->TM_SPN_Status>0',
                            'options'=>array(
                                'title'=>'Activate Subscription',
                                'class'=>'activate'
                            ),
                            'url'=>'Yii::app()->createUrl("user/admin/activate",array("id"=>$data->TM_SPN_Id))',
                        ),
                        'inactivate'=>array(
                            'label'=>'<span class="glyphicon glyphicon-remove"></span>',
                            'visible'=>'$data->TM_SPN_Status<1',
                            'options'=>array(
                                'title'=>'Deactivate Subscription',
                                'class'=>'deactivate'
                            ),
                            'url'=>'Yii::app()->createUrl("user/admin/deactivate",array("id"=>$data->TM_SPN_Id))',
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
        )); ?>
    </div>
