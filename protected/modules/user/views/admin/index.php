<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('adminlist'),
	UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Users', 'class'=>'nav-header'),
    array('label'=>'List Users', 'url'=>array('adminlist')),
    array('label'=>'Create User', 'url'=>array('create')),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),

);
?>
<?php /*echo $this->renderPartial('_menu', array(
    'list'=> array(
        CHtml::link(UserModule::t('Create User'),array('create')),
    ),
));
*/?>
<div class="row-fluid">
    <div class="col-lg-12">
        <h3><?php echo UserModule::t("Manage Users"); ?></h3>

        <form method="get" id="searchform">
        <table class="table table-bordered table-hover table-striped admintable">
            <tr><th colspan="6"><h5><?php echo UserModule::t("Search"); ?></h5></th></tr>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th></th>
                <th></th>
            </tr>
           <tr>
                <td><input type="text" name="username" id="username" class="form-control" value="<?php echo ($_GET['username']!=''?$_GET['username']:'');?>"/></td>
                <td><input type="text" name="email" id="parent"  class="form-control" value="<?php echo ($_GET['email']!=''?$_GET['email']:'');?>" /></td>                                
                <td><button class="btn btn-warning btn-lg btn-block" name="search" value="search" type="submit">Search</button></td>
                <td><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
            </tr>
        </table>
        </form>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                //'id',
                array('header'=>'SN.',

                'value'=>'++$row',

                ),
                'username',
/*                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
                ),
                array(
                    'name' => 'username',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->username),array("admin/view","id"=>$data->id))',
                ),*/
                array(
                    'name'=>'email',
                    'type'=>'raw',
                    'value'=>'CHtml::link(CHtml::encode($data->email), "mailto:".$data->email)',
                ),
                array(
                    'name' => 'createtime',
                    'value' => 'date("d.m.Y H:i:s",$data->createtime)',
                ),
                array(
                    'name' => 'lastvisit',
                    'value' => '(($data->lastvisit)?date("d-m-Y H:i:s",$data->lastvisit):UserModule::t("Not visited"))',
                ),
                array(
                    'name'=>'status',
                    'value'=>'User::itemAlias("UserStatus",$data->status)',
                ),
                array(
                    'name'=>'superuser',
                    'value'=>'User::itemAlias("AdminStatus",$data->superuser)',
                ),
                array(
                    'name'=>'roles',
                    'value' => array($this,'gridCreateRoles'),
                ),				
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            )),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            )),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
        )); ?>
    </div>
</div>
