<?php
$this->breadcrumbs=array(
	UserModule::t('Student')=>array('studentlist'),
	UserModule::t('Manage'),
);
$session=new CHttpSession;
$session->open();
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List Students', 'url'=>array('studentlist'.($session['urlstring']!=''?'?'.$session['urlstring']:''))),
    array('label'=>'Register Student', 'url'=>array('addstudent')),    
    array('label'=>'Import Student (Self revision)', 'url'=>array('ImportStudents')),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<?php /*echo $this->renderPartial('_menu', array(
    'list'=> array(
        CHtml::link(UserModule::t('Create User'),array('create')),
    ),
));
*/?>
<div class="row-fluid">
    <h3><?php echo UserModule::t("Manage Students"); ?></h3>
    <div class="col-lg-12">
        <form method="get" id="searchform">
        <table class="table table-bordered table-hover table-striped admintable">
            <tr><th colspan="7"><h5><?php echo UserModule::t("Search"); ?></h5></th></tr>
            <tr>
                <th>Student Name</th>
                <th>Username</th>
                <th>Parent Name</th>
                <th>School</th>
                <th>Plan</th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td><input type="text" name="student" id="student" class="form-control" value="<?php echo ($_GET['student']!=''?$_GET['student']:'');?>"/></td>
                <td><input type="text" name="username" id="username" class="form-control" value="<?php echo ($_GET['username']!=''?$_GET['username']:'');?>"/></td>
                <td><input type="text" name="parent" id="parent"  class="form-control" value="<?php echo ($_GET['parent']!=''?$_GET['parent']:'');?>" /></td>
                <td>
                    <select name="school" id="school" class="form-control">                                    
                        <?php 
                            $data=CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');
                            
                            echo CHtml::tag('option',
                                           array('value'=>''),'Select School',true);
                            foreach($data as $value=>$name)
                            {
                                if($_POST['school']==$value):
                                    $selected=array('value'=>$value,'selected'=>'selected');              
                                else:
                                    $selected=array('value'=>$value);
                                endif;
                                echo CHtml::tag('option',
                                           array('value'=>$value),CHtml::encode($name),true);
                            }
                            echo CHtml::tag('option',array('value'=>'0'),'Other',true);                                    
                        ?>
                    </select>
                    <input type="text" name="schooltext" id="schooltext" class="form-control" value="<?php echo ($_GET['schooltext']!=''?$_GET['schooltext']:'');?>" />                
                </td>
                <td>
                    <select name="plan" id="plan" class="form-control">
                        <option value="0">Select Plan</option>
                        <?php
                            $plans=Plan::model()->findAll(array('condition'=>'TM_PN_Status=0'));
                            
                            foreach($plans AS $plan):
                                if($_GET['plan']==$plan->TM_PN_Id):
                                    $selected='selected';
                                else:
                                    $selected='';
                                endif;;
                                echo "<option value='".$plan->TM_PN_Id."' ".$selected.">".$plan->TM_PN_Name."</option>";   
                            endforeach;
                        ?>
                    </select>
                </td>
                <td><button class="btn btn-warning btn-lg btn-block" name="search" value="search" type="submit">Search</button></td>
                <td><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
            </tr>
        </table>
        </form>
        <?php 
        if($dataProvider!=''):
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,        
            'columns'=>array(
                array(
                    'name'=>'Student',
                    'type'=>'raw',
                    'value'=>'User::StudentName($data->id)',
                ),                
                'username',
                array(
                    'name'=>'Password',
                    'type'=>'raw',
                    'value'=>'User::StudentPassword($data->id)',
                ),
/*                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
                ),
                array(
                    'name' => 'username',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->username),array("admin/view","id"=>$data->id))',
                ),*/
                array(
                    'name'=>'Parent',
                    'type'=>'raw',
                    'value'=>'User::model()->getParent($data->id)',
                ),
                array(
                    'name'=>'Parent email',
                    'type'=>'raw',
                    'value'=>'User::model()->getParentEmail($data->id)',
                ),
                array(
                    'name'=>'School',
                    'type'=>'raw',
                    'value'=>'User::SchoolCustom($data->id)',
                ),                  
                array(
                    'name'=>'Created By',
                    'type'=>'raw',
                    'value'=>'User::Creator($data->id)',
                ),              
                array(
                    'name' => 'createtime',
                    'value' => 'date("d-m-Y",$data->createtime)',
                ),
                array(
                    'name'=>'Updated By',
                    'type'=>'raw',
                    'value'=>'User::Updator($data->id)',
                ),                 
                array(
                    'name'=>'status',
                    'value'=>'User::itemAlias("UserStatus",$data->status)',
                ),                                
                array(
                    'name'=>'subscription',
                    'value'=>'User::Subscription($data->id)',
                ),                                
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{subscription}{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'subscription'=>array(
                            'label'=>'<span class="glyphicon glyphicon glyphicon-log-in "></span>',
                            'options'=>array(
                                'title'=>'Manage Subscription',
                                'class'=>'subscription',
                                ),
                            'url'=>'Yii::app()->createUrl("user/admin/Studentplan",array("id"=>$data->id))',
                            ),
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            ),
                            'url'=>'Yii::app()->createUrl("user/admin/Viewstudent",array("id"=>$data->id))'),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            ),
                            'url'=>'Yii::app()->createUrl("user/admin/Editstudent",array("id"=>$data->id))'),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
        ));
        endif; ?>
    </div>
</div>
<div class="modal fade" id="invitModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
            </div>
            <div class="modal-body" id="managesubscription">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(function(){
    $('#schooltext').hide();
    $('#school').change(function(){
        if($(this).val()=='0')
        {
            $('#schooltext').show();
        }   
        else
        {
            $('#schooltext').hide();
        }
    });    
    $('.subscription').click(function(){
        var url=$(this).attr('href');
        $.ajax({
            type: 'POST',
            url: url,
            data: {url: url}
        })
        .done(function (data) {
            $('#managesubscription').html(data);
        });
        $('#invitModel').modal('toggle');
        return false;
    })
});
$(document).on('click','.activate',function(){
    var url= $(this).attr('href');
});

$(document).on('click','.deactivate',function(){
    var url= $(this).attr('href');
});
$('#clear').click(function(){        
    $('#student ').val('');
    $('#parent ').val('');
    $('#school ').val('');
    $('#plan ').val('0');
    return true;
         
});
$(document).on('click','.deleteplan',function(){
    if(confirm('Are you sure you want to delte this item?'))
    {
        return true
    }
    else
    {
        return false;
    }
});
$('.showtooltip').tooltip();
$('.showpassword').click(function(){
    var student=$(this).attr('data-student');
    $('#maskpassword'+student).toggle();
    $('#unmaskpassword'+student).toggle();
});
</script>
