<style>
    <?php 
    if($student->TM_STU_School=='0'):?>
    .otherschool
    {
        display:block;
    }
    <?php else:?>
    .otherschool
    {
        display:none;
    }    
    <?php endif;?>
</style>    
<?php
$session=new CHttpSession;
$session->open();
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List Students', 'url'=>array('studentlist'.($session['urlstring']!=''?'?'.$session['urlstring']:''))),           
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<?php if(Yii::app()->user->hasFlash('registration')): ?>
    <div class="row">
        <div class="col-md-10 nopadding">
            <h3>Welcome !</h3>
        </div>
        <div class="col-lg-10 login-panel panel panel-default">
            <div class="panel-body">
                <p>We are excited to welcome you as the newest member of our growing community. </p>
                <p>We have sent you an email with all the details to get you and your child started. If you cannot find the email, please check in your junk folder too, just in case we have ended up there. </p>
                <p>So, what are you waiting for... Lets get started ! <a href="<?php echo Yii::app()->request->baseUrl;?>">Click Here To Login</a></p>
            </div>
        </div>
    </div>
<?php else:?>
<div class="row">
<h3><?php echo  UserModule::t('Add Student'); ?></h3>
<div class="col-lg-10 login-panel panel panel-default">

    <?php $form=$this->beginWidget('UActiveForm', array(
    'id'=>'registration-form',        
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>                				
            <div class="col-md-6">
                   <div class="panel-body">                       
                        <legend  style="color: #F47502;">Student Details</legend>
                            <div class="form-group" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_First_Name',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                                <?php echo $form->error($student,'TM_STU_First_Name'); ?>
                            </div>
                            <div class="form-group" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_Last_Name',array('class'=>'form-control','placeholder'=>'Last Name')); ?>
                                <?php echo $form->error($student,'TM_STU_Last_Name'); ?>
                            </div>
                           <div class="form-group" style="min-height:70px;">
                                <?php echo $form->dropDownList($student,'TM_STU_Country_Id',CHtml::listData(Country::model()->findAll(array('condition' => 'TM_CON_Status=0','order' => 'TM_CON_Id')),'TM_CON_Id','TM_CON_Name'),array('empty'=>'Select Country','class'=>'form-control clry',
                                'ajax' => array(
                                'type'=>'POST', //request type
                                'url'=>CController::createUrl('registration/getschools'), //url to call.
                                //Style: CController::createUrl('currentController/methodToCall')
                                'update'=>'#Student_TM_STU_School', //selector to update
                                //'data'=>'js:javascript statement' 
                                //leave out the data key to pass all form values through
                                ))); ?>                                
                                <?php echo $form->error($student,'TM_STU_Country_Id'); ?>
                            </div>
                            <div class="form-group" style="min-height:70px;">
                                <?php echo $form->dropDownList($student,'TM_STU_School',array('empty'=>'Select School'),array('class'=>'form-control clry')); ?>
                                <?php echo $form->error($student,'TM_STU_School'); ?>
                            </div>                              
                            <div class="form-group otherschool" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_SchoolCustomname',array('class'=>'form-control clry otherschool','placeholder'=>'School Name')); ?>
                                <?php echo $form->error($student,'TM_STU_SchoolCustomname'); ?>
                            </div>                            
                            <div class="form-group otherschool" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_City',array('class'=>'form-control clry otherschool','placeholder'=>'City')); ?>
                                <?php echo $form->error($student,'TM_STU_City'); ?>
                            </div>
                            <div class="form-group otherschool" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_State',array('class'=>'form-control clry otherschool','placeholder'=>'State')); ?>
                                <?php echo $form->error($student,'TM_STU_State'); ?>
                            </div>


                            <div class="form-group" style="min-height:70px;">
                            <?php echo CHtml::submitButton(UserModule::t("Register "),array('class'=>'btn btn-warning  pull-right')); ?>
                            </div>
                </div>

            </div>
			               
    <?php $this->endWidget(); ?>
</div>
</div>
<?php endif;
if($student->TM_STU_Country_Id!=''):
Yii::app()->clientScript->registerScript('loadschools', "
            $.ajax({
              method: 'POST',
              url: '".Yii::app()->createUrl('user/registration/getschools')."',
              data: { country:".$student->TM_STU_Country_Id.",school:'".$student->TM_STU_School."'}
            })
              .done(function( data ) {
                $('#Student_TM_STU_School').html(data);
              });
");
endif;

Yii::app()->clientScript->registerScript('terms', "
    $('#Student_TM_STU_School').change(function(){
        if($(this).val()=='0')
        {
            $('.otherschool').show();
        }   
        else
        {
            $('.otherschool').hide();
        }
    });    
");
?>