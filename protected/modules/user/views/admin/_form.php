<div class="row brd1">

<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'username',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextField($model,'username',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'username'); ?>
            </div>
        </div>


	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model,'password',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
		<?php echo CHtml::activePasswordField($model,'password',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<?php echo CHtml::error($model,'password'); ?>
            </div>
	</div>

	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model,'email',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
		<?php echo CHtml::activeTextField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<?php echo CHtml::error($model,'email'); ?>
            </div>
	</div>

	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model,'superuser',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
		<?php echo CHtml::activeDropDownList($model,'superuser',User::itemAlias('AdminStatus'),array('class'=>'form-control')); ?>
		<?php echo CHtml::error($model,'superuser'); ?>
            </div>
	</div>

	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model,'usertype',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
            <?php
                    $model->usertype=0;
        		if(!$model->isNewRecord):
                    $selected=array();
                    $selectedroles=UserRoles::model()->findAll(array('condition'=>'TM_UR_User_Id='.$model->id));
                    $selectionvalues='';
                    foreach($selectedroles AS $key=>$avail):                    
                        $selected[$avail->TM_UR_Role]=array('selected'=>true);
                        $selectionvalues.=($key==0?$avail->TM_UR_Role:','.$avail->TM_UR_Role);
                    endforeach;                     
                    echo CHtml::activeDropDownList($model,'usertype',User::itemAlias('UserTypes'),array('class'=>'form-control','options' => $selected,'multiple'=>'multiple','style'=>'min-height:130px;'));                       
                else:
                    echo CHtml::activeDropDownList($model,'usertype',User::itemAlias('UserTypes'),array('class'=>'form-control','multiple'=>'multiple','style'=>'min-height:130px;'));
                                                        
                endif;
            ?>
		<?php  ?>
		<?php echo CHtml::error($model,'usertype'); ?>
            </div>
	</div>

	<div class="form-group questionadmin">
		<?php echo CHtml::activeLabelEx($model,'publisher',array('class'=>'col-sm-3 control-label')); ?>		
        <div class="col-sm-9">
		<?php 
		$publishers=CHtml::listData(Publishers::model()->findAll(array("condition"=>"TM_PR_Status = '0'",'order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name');
		 echo CHtml::activeDropDownList($model,'publisher',$publishers,array('class'=>'form-control')); ?>		
		<?php echo CHtml::error($model,'usertype'); ?>
            </div>
	</div>
	<!-- <div class="form-group questionadmin">
		<label class="col-sm-3 control-label required" for="syllabus">Syllabus</label>
        <div class="col-sm-9">
		<?php echo CHtml::dropDownList('syllabus','',CHtml::listData(Syllabus::model()->findAll(array("condition"=>"TM_SB_Status = '0'",'order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty' => 'Select Syllabus','class'=>'form-control','ajax' =>
                array(
                    'type'=>'POST', //request type
                    'url'=>CController::createUrl('/chapter/Getstandard'), //url to call.
//                    'beforeSend'=>'$("#Chapter_TM_TP_Standard_Id").empty()',
    //Style: CController::createUrl('currentController/methodToCall')
                    'update'=>'#standard', //selector to update
                'data'=>'js:{id:$(this).val()}'
                //leave out the data key to pass all form values through
                ))); ?>
		<?php echo CHtml::error($model,'usertype'); ?>
            </div>
	</div> -->
	<div class="form-group questionadmin">
		<label class="col-sm-3 control-label required" for="standard">Standard</label>
        <div class="col-sm-9">
        	<?php
        		if(!$model->isNewRecord):
                    $selected=array();
                    foreach($standards AS $key=>$avail):
                        $selected[]=$avail['Id'];
                    endforeach;                    
                endif;
            ?>
		<?php echo CHtml::dropDownList('standard',$selected,CHtml::listData(Standard::model()->findAll(array("condition"=>"TM_SD_Status = '0'",'order' => 'TM_SD_Name')),'TM_SD_Id','TM_SD_Name'),array('empty' => 'Select Standard','class'=>'form-control', 'multiple' => 'multiple')); ?>
		<?php echo CHtml::error($model,'usertype'); ?>
            </div>
	</div>
	<div class="form-group schooladmin" style="display: none;">
		<label class="col-sm-3 control-label required" for="school">School</label>
        <div class="col-sm-9">
        	<?php
        		if(!$model->isNewRecord):
                    $selected=array();
                    foreach($schools AS $key=>$savail):
                        $selected[]=$savail['Id'];
                    endforeach;                    
                endif;
                                
            ?>
		<?php echo CHtml::dropDownList('schools',$selected,CHtml::listData(School::model()->findAll(array('order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name'),array('empty' => 'Select School','class'=>'form-control', 'multiple' => 'multiple')); ?>
		<?php echo CHtml::error($model,'usertype'); ?>
            </div>
	</div>    
	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model,'status',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
		<?php echo CHtml::activeDropDownList($model,'status',User::itemAlias('UserStatus'),array('class'=>'form-control')); ?>
		<?php echo CHtml::error($model,'status'); ?>
            </div>
	</div>
<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<div class="form-group">
		<?php echo CHtml::activeLabelEx($profile,$field->varname,array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
		<?php 
		if ($field->widgetEdit($profile)) {
			echo $field->widgetEdit($profile);
		} elseif ($field->range) {
			echo CHtml::activeDropDownList($profile,$field->varname,Profile::range($field->range),array('class'=>'form-control'));
		} elseif ($field->field_type=="TEXT") {
			echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50,'class'=>'form-control'));
		} else {
			echo CHtml::activeTextField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255),'class'=>'form-control'));
		}
		 ?>

		<?php echo CHtml::error($profile,$field->varname); ?>
	</div>
    </div>
			<?php
			}
		}
?>
        <div class="form-group">

                <div class="col-sm-3 pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                    </div>
             
            </div>
	</div>
    </div>
<?php echo CHtml::endForm(); ?>

</div><!-- form -->
<?php 
Yii::app()->clientScript->registerScript('studentadmin', "
	$('#User_usertype').change(function(){
        var values=	 $(this).val();             	   
		if(values.indexOf('2')!= -1 )
		{
			$('.questionadmin').slideDown('slow');
		}
		else
		{
			$('.questionadmin').slideUp('fast');
		}
        if(values.indexOf('4')!= -1 )
		{
			$('.schooladmin').slideDown('slow');
		}
		else
		{
			$('.schooladmin').slideUp('fast');
		}        
        

	})
");
if(!$model->isNewRecord):
	Yii::app()->clientScript->registerScript('showstudentadmin', "
			var type='".$selectionvalues."';
			if(type.indexOf('2')!= -1 )
			{
				$('.questionadmin').show();
			}
			if(type.indexOf('4')!= -1 )
			{
				$('.schooladmin').show();
			}            
	");
endif;
?>
