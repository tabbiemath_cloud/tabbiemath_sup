<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('adminlist'),
	UserModule::t('Create'),
);
$this->menu=array(
    array('label'=>'Manage Users', 'class'=>'nav-header'),
    array('label'=>'List Users', 'url'=>array('adminlist')),

);
?>
<h1><?php echo UserModule::t("Create User"); ?></h1>

<?php 
	/*echo $this->renderPartial('_menu',array(
		'list'=> array(),
	));*/
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>