<style>
    <?php 
    if($model->TM_STU_School=='0'):?>
    .otherschool
    {
        display:block;
    }
    <?php else:?>
    .otherschool
    {
        display:none;
    }    
    <?php endif;?>
</style>    
<?php
$this->breadcrumbs=array(
	UserModule::t('Student')=>array('studentlist'),
	UserModule::t('Manage'),
);
$session=new CHttpSession;
$session->open();
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List Students', 'url'=>array('studentlist'.($session['urlstring']!=''?'?'.$session['urlstring']:''))),
    array('label'=>'Change Password', 'url'=>array('studentpassword','id'=>$user->id)),
    array('label'=>'View Student', 'url'=>array('Viewstudent','id'=>$user->id)),    
    array('label'=>'Register Student', 'url'=>array('addstudent')),    
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
Yii::app()->clientScript->registerScript('cancel', "
    $('.cancelbtn').click(function(){
        window.location = '".Yii::app()->createUrl('user/admin/studentlist')."';
    });
    $('#Student_TM_STU_School').change(function(){
        if($(this).val()=='0')
        {
            $('.otherschool').show();
        }   
        else
        {
            $('.otherschool').hide();
        }
    });     
");

if($model->TM_STU_Country_Id!=''):
Yii::app()->clientScript->registerScript('loadschools', "
            $.ajax({
              method: 'POST',
              url: '".Yii::app()->createUrl('user/registration/getschools')."',
              data: { country:".$model->TM_STU_Country_Id.",school:'".$model->TM_STU_School."'}
            })
              .done(function( data ) {
                $('#Student_TM_STU_School').html(data);
              });
");
endif;
?>
<h3>Edit <?php echo $model->TM_STU_First_Name.' '.$model->TM_STU_Last_Name; ?></h3>
<div class="row brd1">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'student-form',
	/// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_STU_student_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($user,'username',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_STU_student_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_STU_First_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_STU_First_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_STU_First_Name'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_STU_Last_Name',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_STU_Last_Name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_STU_Last_Name'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_STU_Country_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_STU_Country_Id',CHtml::listData(Country::model()->findAll(array('condition'=>"TM_CON_Status =0",'order' => 'TM_CON_Id')),'TM_CON_Id','TM_CON_Name'),array('empty'=>'Select Country','class'=>'form-control','ajax' => array(
                    'type'=>'POST', //request type
                    'url'=>CController::createUrl('/user/registration/getschools'), //url to call.
                    //Style: CController::createUrl('currentController/methodToCall')
                    'update'=>'#Student_TM_STU_School', //selector to update
                    //'data'=>'js:javascript statement'
                    //leave out the data key to pass all form values through
                )));?>
                <?php echo $form->error($model,'TM_STU_Country_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'TM_STU_School',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($model,'TM_STU_School',array('empty'=>'Select School'),array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_STU_School'); ?>
            </div>
        </div>
        <div class="form-group otherschool">
            <?php echo $form->labelEx($model,'TM_STU_SchoolCustomname',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_STU_SchoolCustomname',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_STU_SchoolCustomname'); ?>
            </div>
        </div>
        <div class="form-group otherschool">
            <?php echo $form->labelEx($model,'TM_STU_City',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_STU_City',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_STU_City'); ?>
            </div>
        </div>
        <div class="form-group otherschool">
            <?php echo $form->labelEx($model,'TM_STU_State',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->textField($model,'TM_STU_State',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'TM_STU_State'); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($user,'status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($user,'status',User::itemAlias('UserStatus'),array('class'=>'form-control')); ?>
                <?php echo $form->error($user,'status'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($user,'userlevel',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->dropDownList($user,'userlevel',User::itemAlias('UserLevel'),array('class'=>'form-control')); ?>
                <?php echo $form->error($user,'userlevel'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="raw">
                <div class="col-sm-3 pull-right">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                </div>
                <div class="col-lg-3 pull-right">
                    <button class="btn btn-warning btn-lg btn-block cancelbtn" type="reset" value="Reset">Cancel</button>
                </div>
            </div>
        </div>
    </div>
        <?php echo CHtml::errorSummary($user); ?>
    <?php 

    $this->endWidget(); ?>
</div><!-- form -->
    