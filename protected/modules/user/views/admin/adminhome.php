 <!-- /.row -->
            <div class="row">
            <?php if(UserModule::isAllowedQuestion()):?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                           <a href="<?php echo Yii::app()->createUrl('questions/create')?>">
						   <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-question-circle fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Create Question</div>
                                  </div>
                            </div>
                        </div>
                     
                        
                        </a>
                    </div>
                </div>
                <?php endif;?>
                <?php if(UserModule::isSuperAdmin()):?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('coupons/admin')?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa fa-ticket fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Coupons</div>
                                                                 </div>
                            </div>
                        </div>
                       
                         
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('questions/importcsv')?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-upload fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Import Questions</div>
                                                                 </div>
                            </div>
                        </div>


                        </a>
                    </div>
                </div>
                <?php endif;?>
                 <?php if(UserModule::isAllowedQuestion()):?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('mock/admin')?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-th-list fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Mock</div>
                         
                                </div>
                            </div>
                        </div>
                        
                         
                        </a>
                    </div>
                </div>
                <?php endif;?>                
                 <?php if(UserModule::isAllowedStudent()):?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('user/admin/studentlist')?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Students</div>
                         
                                </div>
                            </div>
                        </div>
                        
                         
                        </a>
                    </div>
                </div>
                <?php endif;?>
                <?php if(UserModule::isSuperAdmin()):?>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <a href="<?php echo Yii::app()->createUrl('mock/consolidatedreport')?>">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-th-list fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">Thematic Report</div>
                                        </div>
                                    </div>
                                </div>


                            </a>
                        </div>
                    </div>
                <?php endif;?>

                <?php if(UserModule::isAccountAdmin()):?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php echo Yii::app()->createUrl('school/admin')?>">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-graduation-cap fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">Manage Schools</div>

                                    </div>
                                </div>
                            </div>


                        </a>
                    </div>
                </div>
                <?php endif;?>
                <!--<div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php /*echo Yii::app()->createUrl('user/admin')*/?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Users</div>

                                </div>
                            </div>
                        </div>


                        </a>
                    </div>
                </div>-->
				 <!--<div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="<?php /*echo Yii::app()->createUrl('plan/admin')*/?>">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-pencil-square fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Manage Plan</div>
                                </div>
                            </div>
                        </div>
                        
                         
                        </a>
                    </div>
                </div>-->

				<!--<div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <a href="#">
						<div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-cogs fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">Settings</div>
                                </div>
                            </div>
                        </div>
                        
                         
                        </a>
                    </div>
                </div>-->
           
            </div>
            <!-- /.row -->
<style type="text/css">
    #wrapper
    {
        padding-left:0px !important;
    }
</style>