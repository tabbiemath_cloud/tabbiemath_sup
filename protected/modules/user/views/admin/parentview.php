<?php
$this->breadcrumbs=array(
	UserModule::t('Parent')=>array('parentlist'),
	UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Parent', 'class'=>'nav-header'),
    array('label'=>'List Parents', 'url'=>array('parentlist')),
    array('label'=>'Change Password', 'url'=>array('parentpassword','id'=>$model->id)),
    array('label'=>'Edit Parent', 'url'=>array('Editparent','id'=>$model->id)),       
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<h3>View <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover" id="yw0">
            <tr>
                <th>Username</th>
                <td><?php echo $model->username;?></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><?php echo $model->profile->firstname;?></td>
            </tr>
            
            <tr>
                <th>Last Name</th>
                <td><?php echo $model->profile->lastname;?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $model->email;?></td>
            </tr> 
            
            <tr>
                <th>Phone Number</th>
                <td><?php echo $model->profile->phonenumber;?></td>
            </tr>           
        </table>
    </div>
</div>    