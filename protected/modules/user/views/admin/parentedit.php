<?php
$this->breadcrumbs=array(
	UserModule::t('Parent')=>array('parentlist'),
	UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Parent', 'class'=>'nav-header'),
    array('label'=>'List Parents', 'url'=>array('parentlist')),
    array('label'=>'Change Password', 'url'=>array('parentpassword','id'=>$model->id)),
    array('label'=>'View Parent', 'url'=>array('Viewparent','id'=>$model->id)),       
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<h3>Edit  <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>
<div class="row brd1">

<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data')); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'username',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeTextField($model,'username',array('size'=>20,'maxlength'=>20,'class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'username'); ?>
            </div>
        </div>

	<div class="form-group">
		<?php echo CHtml::activeLabelEx($model,'email',array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
		<?php echo CHtml::activeTextField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
		<?php echo CHtml::error($model,'email'); ?>
            </div>
	</div>

<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<div class="form-group">
		<?php echo CHtml::activeLabelEx($profile,$field->varname,array('class'=>'col-sm-3 control-label')); ?>
        <div class="col-sm-9">
		<?php 
		if ($field->widgetEdit($profile)) {
			echo $field->widgetEdit($profile);
		} elseif ($field->range) {
			echo CHtml::activeDropDownList($profile,$field->varname,Profile::range($field->range),array('class'=>'form-control'));
		} elseif ($field->field_type=="TEXT") {
			echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50,'class'=>'form-control'));
		} else {
			echo CHtml::activeTextField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255),'class'=>'form-control'));
		}
		 ?>

		<?php echo CHtml::error($profile,$field->varname); ?>
	</div>
    </div>
			<?php
			}
		}
?>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'status',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'status',User::itemAlias('UserStatus'),array('class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'status'); ?>
            </div>
        </div>
        <div class="form-group">

                <div class="col-sm-3 pull-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                    </div>
             
            </div>
	</div>
    </div>
<?php echo CHtml::endForm(); ?>

</div><!-- form -->
<?php 
Yii::app()->clientScript->registerScript('studentadmin', "
	$('#User_usertype').change(function(){
        var values=	 $(this).val();      	   
		if(values.indexOf('2')!= -1 )
		{
			$('.questionadmin').slideDown('slow');
		}
		else
		{
			$('.questionadmin').slideUp('fast');
		}

	})
");
if(!$model->isNewRecord):
	Yii::app()->clientScript->registerScript('showstudentadmin', "
			var type='".$selectionvalues."';
			if(type.indexOf('2')!= -1 )
			{
				$('.questionadmin').show();
			}
	");
endif;
?>
