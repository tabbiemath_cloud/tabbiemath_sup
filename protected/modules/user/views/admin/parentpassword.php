<?php
$this->breadcrumbs=array(
	UserModule::t('Parent')=>array('parentlist'),
	UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Parent', 'class'=>'nav-header'),
    array('label'=>'List Parents', 'url'=>array('parentlist')),
    array('label'=>'Edit Parent', 'url'=>array('editparent','id'=>$parent->id)),
    array('label'=>'View Parent', 'url'=>array('Viewparent','id'=>$parent->id)),       
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<h3>Change Password of <?php echo $student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name; ?></h3>
<div class="row brd1">
<?php $form=$this->beginWidget('UActiveForm', array(
	'id'=>'changepassword-form',
	'enableAjaxValidation'=>true,
)); ?>
    <div class="panel-body form-horizontal payment-form">
        <?php if(Yii::app()->user->hasFlash('passwordchange')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo Yii::app()->user->getFlash('passwordchange'); ?>
            </div>
        <?php endif; ?>        
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required</strong></div>        
        <div class="form-group">
            <?php echo $form->labelEx($model,'password',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'verifyPassword',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo $form->passwordField($model,'verifyPassword',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
                <?php echo $form->error($model,'verifyPassword'); ?>
            </div>
        </div> 
        <div class="form-group">
            <div class="raw">
            <div class="col-sm-3 pull-right">
            	<?php echo CHtml::submitButton(UserModule::t("Save"),array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                
            </div>
            </div>
        </div>       
    </div>
<?php $this->endWidget(); ?>    
</div>    