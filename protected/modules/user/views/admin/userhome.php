    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <div class="contentarea">

            <div class="bs-example">

                <div class="sky1">
                    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/sky1.png','',array('class'=>'img-responsive')); ?>
                </div>
                <h1 class="h125">You have the following self revisions to complete</h1>
                <div class="panel panel-default">
                    <!-- Default panel contents -->

                    <!-- Table -->
                    <div class="table-responsive-vertical shadow-z-1">
                        <!-- Table starts here -->


                        <div class="table-responsive shadow-z-1">
                            <table class="table table-hover table-mc-light-blue">
                                <thead>
                                <tr>
                                    <th>Published On</th>
                                    <th>Publisher</th>
                                    <th>Topic</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody class="font2">
                                <tr>
                                    <td data-title="Date">15-09-2015</td>
                                    <td data-title="Publisher">Publisher 1</td>
                                    <td data-title="Division">Division</td>
                                    <td data-title="Link"><a href="#" target="_blank" class="btn btn-warning">Continue</a></td>
                                    <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/solutions.png','',array('class'=>'hvr-pop')); ?></a></td>
                                    <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/right.png','',array('class'=>'hvr-pop')); ?></a></td>
                                    <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')); ?></a></td>
                                </tr>
                                <tr>
                                    <td data-title="Date">15-09-2015</td>
                                    <td data-title="Publisher">Publisher 1</td>
                                    <td data-title="Division">Division</td>
                                    <td data-title="Link"><a href="#" target="_blank" class="btn btn-warning">Continue</a></td>
                                    <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/solutions.png','',array('class'=>'hvr-pop')); ?></a></td>
                                    <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/right.png','',array('class'=>'hvr-pop')); ?></a></td>
                                    <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')); ?></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>








                    </div>

                </div>
                <div class="sky3"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/sky3.png','',array('class'=>'img-responsive')); ?></div>
            </div>







            <div class="bs-example">


                <h1 class="h125" style="padding-top: 0;">You have the following open challenges</h1>
                <div class="panel panel-default">
                    <!-- Default panel contents -->

                    <!-- Table -->
                    <div class="table-responsive shadow-z-1">
                        <table class="table table-hover table-mc-light-blue">
                            <thead>
                            <tr>
                                <th>Set By</th>
                                <th>Set On</th>
                                <th>Set For</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody class="font2">
                            <tr>
                                <td data-title="Date">My Self</td>
                                <td data-title="Publisher">13-08-2015</td>
                                <td data-title="Division">My Self  + 10 more</td>
                                <td data-title="Link"><a href="#" target="_blank" class="btn btn-warning">Continue</a></td>
                                <td data-title="Link"></td>
                                <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')); ?></a></td>
                            </tr>

                            <tr>
                                <td data-title="Date">My Self</td>
                                <td data-title="Publisher">14-08-2015</td>
                                <td data-title="Division">My Self</td>
                                <td data-title="Link"><a href="#" target="_blank" class="btn btn-warning">Continue</a></td>
                                <td data-title="Link"><a href="#" target="_blank" class="btn btn-info">Continue</a></td>
                                <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')); ?></a></td>
                            </tr>

                            <tr>
                                <td data-title="Date">My Self</td>
                                <td data-title="Publisher">14-08-2015</td>
                                <td data-title="Division">My Self</td>
                                <td data-title="Link"><a href="#" target="_blank" class="btn btn-warning">Continue</a></td>
                                <td data-title="Link"><a href="#" target="_blank" class="btn btn-info">Continue</a></td>
                                <td data-title="Completed"><a href="#"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/trash.png','',array('class'=>'hvr-buzz-out')); ?></a></td>
                            </tr>



                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="sky2">
                    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/sky2.png','',array('class'=>'img-responsive')); ?>
                </div>
                <div class="sky3">
                    <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/sky3.png','',array('class'=>'img-responsive')); ?>
                </div>
            </div>









        </div>


    </div>
