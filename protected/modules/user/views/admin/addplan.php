<?php
$session=new CHttpSession;
$session->open(); 
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List Students', 'url'=>array('studentlist'.($session['urlstring']!=''?'?'.$session['urlstring']:''))),    
    ///array('label'=>'Student Plan', 'url'=>array('Studentplan','id'=>$id)),   
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<h3><?php echo  UserModule::t('Add Plan') ?></h3>

<div class="row brd1">
<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data','id'=>'planform')); ?>
    <div class="panel-body form-horizontal payment-form">
        <div class="well well-sm"><strong>Fields with <span class="required">*</span> are required.</strong></div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_Publisher_Id',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_Publisher_Id',CHtml::listData(Publishers::model()->findAll(array('condition'=>'TM_PR_Status=0','order' => 'TM_PR_Name')),'TM_PR_Id','TM_PR_Name'),array('empty' => 'Select Publisher','class'=>'form-control')); ?>                
                <?php echo CHtml::error($model,'TM_SPN_Publisher_Id'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_SyllabusId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_SyllabusId',CHtml::listData(Syllabus::model()->findAll(array('condition'=>'TM_SB_Status=0','order' => 'TM_SB_Name')),'TM_SB_Id','TM_SB_Name'),array('empty' => 'Select Syllabus','class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_SyllabusId'); ?>
            </div>
        </div>  
        
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_StandardId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_StandardId',array(''=>'Select Standard'),array('empty' => 'Select Standard','class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_StandardId'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'Subscription Date',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activedateField($model,'TM_SPN_CreatedOn',array('class'=>'form-control','value'=>date('Y-m-d'))); ?>
                <?php echo CHtml::error($model,'TM_SPN_CreatedOn'); ?>
            </div>
        </div>        
          
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_PlanId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
                <?php echo CHtml::activeDropDownList($model,'TM_SPN_PlanId',array(''=>'Select Plan'),array('class'=>'form-control')); ?>
                <?php echo CHtml::error($model,'TM_SPN_PlanId'); ?>
            </div>
        </div>        
        <div class="form-group">
            <?php echo CHtml::activeLabelEx($model,'TM_SPN_CouponId',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-3">
                <?php echo CHtml::activeTextField($model,'TM_SPN_CouponId',array('class'=>'form-control','style'=>'width:200px;')); ?>
                <?php echo CHtml::error($model,'TM_SPN_CouponId'); ?>
                
            </div>
            <div class="col-sm-2">
                <a href="javascript:void(0)" id="redeem">Recalculate</a>
            </div>
            <div class="col-sm-4">
              <div class="vouchererror" ></div>
            </div>  
        </div>  
         <div class="form-group">

            <div class="col-sm-3 pull-right">
                    <input type="hidden" id="planexist" value="0" />
                    <?php echo CHtml::activeHiddenfield($model,'TM_SPN_StudentId',array('value'=>$id,'class'=>'form-control')); ?>
                    <?php echo CHtml::activeHiddenfield($model,'TM_SPN_SubjectId',array('value'=>'1','class'=>'form-control')); ?>                    
	              <?php echo CHtml::submitButton('Add Plan',array('class'=>'btn btn-warning btn-lg btn-block')); ?>
            </div>
            
            <div class="col-md-3 pull-right" id="planrate" style="font-weight: bold;;"></div>
        </div>     
<?php echo CHtml::endForm(); ?>
</div>
<?php 

Yii::app()->clientScript->registerScript('jsfunctions', "  
    $(document).on('change','#StudentPlan_TM_SPN_SyllabusId',function(){
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('user/admin/GetStandardList')."',
              data: { id:$(this).val()}
            })
        .done(function( html ) {            
            $('#StudentPlan_TM_SPN_StandardId').html(html);
            $('#StudentPlan_TM_SPN_PlanId').html('<option >Select</option>');
            
        });
    })
    $(document).on('change','#StudentPlan_TM_SPN_StandardId',function(){

        var publisher=$('#StudentPlan_TM_SPN_Publisher_Id').val();
        var syllabus=$('#StudentPlan_TM_SPN_SyllabusId').val();
        var standard=$('#StudentPlan_TM_SPN_StandardId').val();
        $.ajax({
              type: 'POST',
              url: '".Yii::app()->createUrl('user/admin/GetPlanList')."',
              data: { publisher:publisher,syllabus:syllabus,standard:standard}
            })
        .done(function( html ) {
            $('#StudentPlan_TM_SPN_PlanId').html(html);
        });
    })
    $(document).on('click','#redeem',function(){
        var voucher=$('#StudentPlan_TM_SPN_CouponId').val();
        var plan=$('#StudentPlan_TM_SPN_PlanId').val();
            $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('coupons/GetCouvherRate')."',                  
                  dataType:'json',
                  data: { plan:plan,voucher:voucher}
                })
            .done(function( data ) {                
                if(data.error=='no')
                {                    
                    $('#planrate').html('Total Amount: '+data.rate);
                }
                else
                {                    
                    $('.vouchererror').html(data.error);
                    $('#planrate').html('Total Amount: '+data.rate);                    
                    setTimeout(function(){
                        $('#StudentPlan_TM_SPN_CouponId').val('')
                        $('.vouchererror').html('');
                    },5000)
                }
            });
    });
    $(document).on('change','#StudentPlan_TM_SPN_PlanId',function(){
            var plan=$(this).val();
            var student=$('#StudentPlan_TM_SPN_StudentId').val();        
            checkPlan(plan,student);      
    });
    function checkPlan(plan,student)
    {
        $('.error').hide();
        $.ajax({
          type: 'POST',
          url: '".Yii::app()->createUrl('user/admin/checkactiveplan')."',
          data: { plan:plan,student:student}
        })
          .done(function( data ) {
            if(data>0)
            {
                $('#StudentPlan_TM_SPN_PlanId').after( '<div class=\"error alert alert-danger\">Plan already active </div>' );
                $('#planexist').val('1');
            }
            else
            {
                $('#planexist').val('0');
            }               
        });                
    }        
    $(document).on('change','#StudentPlan_TM_SPN_PlanId',function(){
        var voucher=$('#StudentPlan_TM_SPN_CouponId').val();
        var plan=$(this).val();
            $.ajax({
                  type: 'POST',
                  url: '".Yii::app()->createUrl('coupons/GetCouvherRate')."',                  
                  dataType:'json',
                  data: { plan:plan,voucher:voucher,school:".$school."}
                })
            .done(function( data ) {                
                if(data.error=='no')
                {                    
                    $('#planrate').html('Total Amount: '+data.rate);
                }
                else
                {                    
                    $('.vouchererror').html(data.error);
                    $('#planrate').html('Total Amount: '+data.rate);                    
                    setTimeout(function(){
                        $('#StudentPlan_TM_SPN_CouponId').val('')
                        $('.vouchererror').html('');
                    },5000)
                }
            });
    }); 
    $('#planform').submit(function(){
        if($('#planexist').val()==0)
        {
            return true;
        }    
        else
        {
            $('#StudentPlan_TM_SPN_PlanId').after( '<div class=\"error alert alert-danger\">Plan already active </div>' );
            return false;
        }
    });   
 ");
 
 ?>
 