<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('adminlist'),
	$model->username,
);
$this->menu=array(
    array('label'=>'Manage Users', 'class'=>'nav-header'),
    //array('label'=>'Create User', 'url'=>array('create')),
    //array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
    //array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'List User', 'url'=>array('adminlist')),
    //array('label'=>'Manage User', 'url'=>array('admin')),
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<h3>View <?php echo $model->profile->firstname.' '.$model->profile->lastname; ?></h3>
<div class="row brd1">
    <div class="col-lg-12">
        <table class="table table-bordered table-hover" id="yw0">
            <tr>
                <th>Username</th>
                <td><?php echo $model->username;?></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><?php echo $model->profile->firstname;?></td>
            </tr>
            
            <tr>
                <th>Last Name</th>
                <td><?php echo $model->profile->lastname;?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $model->email;?></td>
            </tr> 
            
            <tr>
                <th>Phone Number</th>
                <td><?php echo $model->profile->phonenumber;?></td>
            </tr>
            <tr>
                <th>Roles</th>
                <td><?php 
                        $roles=UserRoles::model()->findAll(array('condition'=>'TM_UR_User_Id='.$model->id));
                        $questionadmin=false;
                        foreach($roles AS $role):
                            if($role->TM_UR_Role=='2'):
                                $questionadmin=true;
                            endif;
                            echo User::itemAlias("UserTypes",$role->TM_UR_Role).'<br>';
                        endforeach;
                    ?></td>
            </tr> 
            <?php if($questionadmin):?>
            <tr>
                <th>Publisher</th>
                <td><?php echo Publishers::model()->find(array('condition'=>'TM_PR_Id='.$model->publisher))->TM_PR_Name; ?></td>
            </tr>
            <tr>
                <th>Standard</th>
                <td>
                <?php 
                        $classes=Adminstandards::model()->findAll(array('condition'=>'TM_SA_Admin_Id='.$model->id));
                        if(count($classes)>0):                        
                            foreach($classes AS $class):
                                echo $class->standard->TM_SD_Name.'<br>';
                            endforeach;
                        endif;
                    ?>
                </td>
            </tr>              
            <?php endif;?>        
        </table>
    </div>
</div>