<?php
$session=new CHttpSession;
$session->open(); 
$this->menu=array(
    array('label'=>'Manage Students', 'class'=>'nav-header'),
    array('label'=>'List Students', 'url'=>array('studentlist'.($session['urlstring']!=''?'?'.$session['urlstring']:''))),           
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<style>
    <?php 
    if($student->TM_STU_School=='0'):?>
    .otherschool
    {
        display:block;
    }
    <?php else:?>
    .otherschool
    {
        display:none;
    }    
    <?php endif;?>
::-webkit-input-placeholder {
 color: #fff !important;
}
:-moz-placeholder { /* older Firefox*/

 color: #fff !important;

}
::-moz-placeholder { /* Firefox 19+ */ 
 color: #fff !important; 
} 
:-ms-input-placeholder { 
 color: #fff !important;
}
 
</style>
<?php if(Yii::app()->user->hasFlash('registration')): ?>
    <div class="row">
        <div class="col-md-10 nopadding">
            <h3>Welcome !</h3>
        </div>
        <div class="col-lg-10 login-panel panel panel-default">
            <div class="panel-body">
                <p>We are excited to welcome you as the newest member of our growing community. </p>
                <p>We have sent you an email with all the details to get you and your child started. If you cannot find the email, please check in your junk folder too, just in case we have ended up there. </p>
                <p>So, what are you waiting for... Lets get started ! <a href="<?php echo Yii::app()->request->baseUrl;?>">Click Here To Login</a></p>
            </div>
        </div>
    </div>
<?php else:?>
<div class="row">
<h3><?php echo  UserModule::t('Add Student'); ?></h3>
<div class="col-lg-10 login-panel panel panel-default">

    <?php $form=$this->beginWidget('UActiveForm', array(
    'id'=>'registration-form',        
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>
                <div class="col-md-6">
                        <div class="panel-body">


                            <fieldset>
                                <legend  style="color: #F47502;">Parent Details</legend>
                                <?php
                                $profileFields=$profile->getFields();
                                if ($profileFields) {
                                    foreach($profileFields as $field) {
                                        ?>
                                        <div class="form-group" style="min-height:70px;">
                                            <?php //echo $form->labelEx($profile,$field->varname); ?>
                                            <?php
                                            if ($field->widgetEdit($profile)) {
                                                echo $field->widgetEdit($profile);
                                            } elseif ($field->range) {
                                                echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                                            } elseif ($field->field_type=="TEXT") {
                                                echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                                            } else {
                                                echo $form->textField($profile,$field->varname,array('class'=>'form-control','placeholder'=>$field->title,'size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                                            }
                                            ?>
                                            <?php echo $form->error($profile,$field->varname); ?>
                                        </div>
                                    <?php
                                    }
                                }
                                ?>
                                <div class="form-group" style="min-height:70px;">
                                    <?php echo $form->textField($model,'email',array('class'=>'form-control existvalidation','placeholder'=>'E-mail')); ?>
                                    <?php echo $form->error($model,'email'); ?>
                                </div>
                                <!--<div class="form-group" style="min-height:70px;">
                                    <?php //echo $form->textField($model,'username',array('class'=>'form-control','placeholder'=>'Username')); ?>
                                    <?php //echo $form->error($model,'username'); ?>
                                </div>-->
                                <div class="form-group" style="min-height:70px;">
                                    <?php echo $form->PasswordField($model,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                                    <?php echo $form->error($model,'password'); ?>
                                </div>

                        </div>

                </div>				
            <div class="col-md-6">
                   <div class="panel-body">
                       <fieldset>
                        <legend  style="color: #F47502;">Student Details</legend>
                            <div class="form-group" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_First_Name',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                                <?php echo $form->error($student,'TM_STU_First_Name'); ?>
                            </div>
                            <div class="form-group" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_Last_Name',array('class'=>'form-control','placeholder'=>'Last Name')); ?>
                                <?php echo $form->error($student,'TM_STU_Last_Name'); ?>
                            </div>
                           <div class="form-group username_" style="min-height:70px;">
                               <?php echo $form->textField($student,'TM_STU_student_Id',array('class'=>'form-control clry existvalidation','placeholder'=>'Enter Preferred Username')); ?>
                               <?php echo $form->error($student,'TM_STU_student_Id'); ?>
                           </div>
                           <div class="form-group" style="min-height:70px;">
                              <?php echo $form->PasswordField($student,'TM_STU_Password',array('class'=>'form-control clry','placeholder'=>'Enter Preferred Password')); ?>
                              <?php echo $form->error($student,'TM_STU_Password'); ?>
                          </div>
                           <div class="form-group" style="min-height:70px;">
                                <?php echo $form->dropDownList($student,'TM_STU_Country_Id',CHtml::listData(Country::model()->findAll(array('condition' => 'TM_CON_Status=0','order' => 'TM_CON_Id')),'TM_CON_Id','TM_CON_Name'),array('empty'=>'Select Country','class'=>'form-control clry',
                                'ajax' => array(
                                'type'=>'POST', //request type
                                'url'=>CController::createUrl('registration/getschools'), //url to call.
                                //Style: CController::createUrl('currentController/methodToCall')
                                'update'=>'#Student_TM_STU_School', //selector to update
                                //'data'=>'js:javascript statement' 
                                //leave out the data key to pass all form values through
                                ))); ?>                                
                                <?php echo $form->error($student,'TM_STU_Country_Id'); ?>
                            </div>
                           <div class="form-group" style="min-height:70px;">
                                <?php echo $form->dropDownList($student,'TM_STU_School',array('empty'=>'Select School'),array('class'=>'form-control clry')); ?>
                                <?php echo $form->error($student,'TM_STU_School'); ?>
                            </div>                              
                            <div class="form-group otherschool" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_SchoolCustomname',array('class'=>'form-control clry otherschool','placeholder'=>'School Name')); ?>
                                <?php echo $form->error($student,'TM_STU_SchoolCustomname'); ?>
                            </div>                            
                            <div class="form-group otherschool" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_City',array('class'=>'form-control clry otherschool','placeholder'=>'City')); ?>
                                <?php echo $form->error($student,'TM_STU_City'); ?>
                            </div>
                            <div class="form-group otherschool" style="min-height:70px;">
                                <?php echo $form->textField($student,'TM_STU_State',array('class'=>'form-control clry otherschool','placeholder'=>'State')); ?>
                                <?php echo $form->error($student,'TM_STU_State'); ?>
                            </div>
                           <div class="form-group" style="min-height:70px;">
                               <?php echo $form->dropDownList($model,'userlevel',User::itemAlias('UserLevel'),array('class'=>'form-control')); ?>
                               <?php echo $form->error($model,'userlevel'); ?>
                           </div>
                       </fieldset>

                   </div>

            </div>


            <div class="col-md-12"> <div class="panel-body"><?php echo CHtml::submitButton(UserModule::t("Register "),array('class'=>'btn btn-warning  pull-right registration')); ?></div></div>
    <?php $this->endWidget(); ?>
</div>
</div>
<?php endif;
if($student->TM_STU_Country_Id!=''):
Yii::app()->clientScript->registerScript('loadschools', "
            $.ajax({
              method: 'POST',
              url: '".Yii::app()->createUrl('user/registration/getschools')."',
              data: { country:".$student->TM_STU_Country_Id.",school:'".$student->TM_STU_School."'}
            })
              .done(function( data ) {
                $('#Student_TM_STU_School').html(data);
              });
");
endif;

Yii::app()->clientScript->registerScript('terms', "
    $('#Student_TM_STU_School').change(function(){
        if($(this).val()=='0')
        {
            $('.otherschool').show();
        }   
        else
        {
            $('.otherschool').hide();
        }
    });    
");
?>
<script>
jQuery(function($) {
    $(".existvalidation").focusout(function(){
        // alert('success');
        var parentEmail = $('#User_email').val();
        var studentUserName = $('#Student_TM_STU_student_Id').val();
        if(studentUserName != '') {
            $('.registration').attr('disabled','disabled');
            $.ajax({
                method: 'POST',
                url: '<?= Yii::app()->createUrl('school/isexist') ?>',
                dataType:'json',
                data: { parentEmail:parentEmail,studentUserName:studentUserName},
                success(data){
                    if(data.error == 'no') {
                        // alert('TRUE');
                        $('#Student_TM_STU_student_Id_em_').remove();
                        $('.registration').removeAttr('disabled');
                        return true;
                    }
                    else
                        // alert('FALSE');
                        $('#Student_TM_STU_student_Id_em_').show();
                        $('.username_').append('<div class="errorMessage" id="Student_TM_STU_student_Id_em_">Username already exists</div>');
                        
                        
                }
            });
        }        
    });
});
</script>
