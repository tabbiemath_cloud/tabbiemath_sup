<?php
$this->breadcrumbs=array(
	UserModule::t('Student')=>array('parentlist'),
	UserModule::t('Manage'),
);
$this->menu=array(
    array('label'=>'Manage Parent', 'class'=>'nav-header'),
    array('label'=>'List Parents', 'url'=>array('parentlist')),
    //array('label'=>'Register Parent', 'url'=>array('addparent')),    
    //array('label'=>'Manage Profile Field', 'url'=>array('profileField/admin')),
);
?>
<?php /*echo $this->renderPartial('_menu', array(
    'list'=> array(
        CHtml::link(UserModule::t('Create User'),array('create')),
    ),
));
*/?>
<div class="row-fluid">
    <div class="col-lg-12">
        <h3><?php echo UserModule::t("Manage Parents"); ?></h3>
         <form method="get" id="searchform">
        <table class="table table-bordered table-hover table-striped admintable">
            <tr><th colspan="6"><h5><?php echo UserModule::t("Search"); ?></h5></th></tr>
            <tr>
                <th>Username</th>
                <th>Name</th>
                <th>Email</th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td><input type="text" name="username" id="username" class="form-control" value="<?php echo ($_GET['username']!=''?$_GET['username']:'');?>"/></td>
                <td><input type="text" name="name" id="student" class="form-control" value="<?php echo ($_GET['name']!=''?$_GET['name']:'');?>"/></td>
                <td><input type="text" name="email" id="parent"  class="form-control" value="<?php echo ($_GET['email']!=''?$_GET['email']:'');?>" /></td>                                
                <td><button class="btn btn-warning btn-lg btn-block" name="search" value="search" type="submit">Search</button></td>
                <td><button class="btn btn-warning btn-lg btn-block" id="clear" name="clear" value="clear" type="submit">Clear</button></td>
            </tr>
        </table>
        </form>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider'=>$dataProvider,        
            'columns'=>array( 
                'id',              
                array(
                    'name'=>'First Name',
                    'type'=>'raw',
                    'value'=>'$data->profile->firstname',
                ),  
                array(
                    'name'=>'Last Name',
                    'type'=>'raw',
                    'value'=>'$data->profile->lastname',
                ),                               
                'email',
                array(
                    'name'=>'status',
                    'value'=>'User::itemAlias("UserStatus",$data->status)',
                ),                                                                                 
                array(
                    'name'=>'Add Students',
                    'type'=>'raw',
                    'value'=>'User::getAddurl($data->id)',
                ),                                                 
                array(
                    'name'=>'Manage Students',
                    'type'=>'raw',
                    'value'=>'User::getManageurl($data->id)',
                ),                                              
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'deleteButtonImageUrl'=>false,
                    'updateButtonImageUrl'=>false,
                    'viewButtonImageUrl'=>false,
                    'buttons'=>array
                    (
                        'view'=>array(
                            'label'=>'<span class="glyphicon glyphicon-eye-open"></span>',
                            'options'=>array(
                                'title'=>'View'
                            ),
                            'url'=>'Yii::app()->createUrl("user/admin/Viewparent",array("id"=>$data->id))'),
                        'update'=>array(
                            'label'=>'<span class="glyphicon glyphicon-pencil"></span>',
                            'options'=>array(
                                'title'=>'Update'
                            ),
                            'url'=>'Yii::app()->createUrl("user/admin/Editparent",array("id"=>$data->id))'),
                        'delete'=>array(
                            'label'=>'<span class="glyphicon glyphicon-trash"></span>',
                            'options'=>array(

                                'title'=>'Delete',
                            ),
                        )
                    ),
                ),
            ),'itemsCssClass'=>"table table-bordered table-hover table-striped admintable"
        )); ?>
    </div>
</div>
<div class="modal fade " id="invitModel" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
            </div>
            <div class="modal-body" id="managesubscription">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="inviteclose">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
$(function(){
    $('.subscription').click(function(){
        var url=$(this).attr('href');
        $.ajax({
            type: 'POST',
            url: url,
            data: {url: url}
        })
        .done(function (data) {
            $('#managesubscription').html(data);
        });
        $('#invitModel').modal('toggle');
        return false;
    })
})
$(docuemnt).on('click','.activate',function(){
    var url= $(this).attr('href');
})

$(docuemnt).on('click','.deactivate',function(){
    var url= $(this).attr('href');
})
</script>