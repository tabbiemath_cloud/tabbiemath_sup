<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Reset Password");?>





  <img class="loghed1" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/>  
  <div class="container con_mar_top">
   
	
	<?php
                if(Yii::app()->user->hasFlash('activation') | Yii::app()->user->hasFlash('recoveryMessage')):
                 if(Yii::app()->user->hasFlash('activation')): ?>
					<div class="alert alert-success text-center">
					<?php echo Yii::app()->user->getFlash('activation'); ?>
					</div>                
                <?php endif;?>
                
            	<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
					<div class="alert alert-success text-center">
					<?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
					</div> 
                     <div class="col-md-3 pull-right">
                        <a href="<?php echo Yii::app()->createUrl('user/login')?>" class="btn btn-warning btn-lg btn-block">Back to Login</a>
                    </div>             
                <?php endif;
                else:?>                
                <!--<h5><?php /*echo UserModule::t("Please fill out the following form with your login credentials:"); */?></h5>-->
				 <?php echo CHtml::beginForm('', 'post', array('class'=>'form-signin')); ?>
				  <h2 class="form-signin-heading"><?php echo UserModule::t("Reset Password"); ?></h2>
                    <fieldset>
                        <div class="form-group">
                            <h4>To reset student's password please login as parent and use change password function there.</h4>
                            <h4>To reset parent's password please enter email address below</h4>
                            <?php echo CHtml::activeTextField($form,'login_or_email',array('class'=>'form-control','placeholder'=>'Please enter your email address')) ?>
                            <h4 class="errormsg" style="display: none; color: red; font-size: 17px; font-family: monospace;"> This email is not registered.</h4>
                            <?php echo CHtml::error($form,'login_or_email'); ?>                               
                        </div>                        
                        <!-- Change this to a button or input when using this as a form -->   
					<div class="pull-right">
                        <button type="button" class="btn btn-warning btn-lg btn-block" id ="restore" >Restore</button>
                    <?php /*echo CHtml::submitButton(UserModule::t("Restore"),array('class'=>'btn btn-warning btn-lg btn-block')); */?>
                    </div>
                                      
                    <div class="col-md-4 pull-right">
                        <a href="<?php echo Yii::app()->createUrl('user/login')?>" class="btn btn-warning btn-lg btn-block">Cancel</a>
                    </div>						
                    </fieldset>  
                   
                <?php echo CHtml::endForm(); 
                
                endif;?>
  
  </div>

<script type="text/javascript">

    $(document).on('click', '#restore', function() {
        var value = $("#UserRecoveryForm_login_or_email").val();
        $.ajax({
            type:"post",
            dataType: "json",
            url:'recovery/emailcheck',
            data:  {value:value},
            success: function(data){
                if(data.error=='No')
                {

                    $(".errormsg").hide();
                    $('.form-signin').submit();
                }
                else
                {
                    $(".errormsg").show();
                    return false;

                }
            }
        });

    });

</script>
  