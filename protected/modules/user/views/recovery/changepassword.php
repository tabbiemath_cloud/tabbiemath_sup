<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");?>





  <img class="loghed1" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/>  
  <div class="container con_mar_top">
   
	
                <?php if(Yii::app()->user->hasFlash('activation')): ?>
                    <div class="alert alert-success text-center" role="alert">
                        <?php echo Yii::app()->user->getFlash('activation'); ?>
                    </div>
                    <div class="col-md-3 pull-right">
                        <a href="<?php echo Yii::app()->createUrl('user/login')?>" class="btn btn-warning btn-lg btn-block">Back to Login</a>
                    </div> 
                <?php else:?>                
                <!--<h5><?php /*echo UserModule::t("Please fill out the following form with your login credentials:"); */?></h5>-->
				 <?php echo CHtml::beginForm('', 'post', array('class'=>'form-signin')); ?>
				  <h2 class="form-signin-heading"><?php echo UserModule::t("Change Password"); ?></h2>                                      
                    <fieldset> 
                        <div class="form-group">                                                   
                                <?php echo CHtml::activePasswordField($form,'password',array('class'=>'form-control brd_top_non','placeholder'=>'Please enter your new password')) ?>                                
                                <?php echo CHtml::error($form,'password'); ?>                            
                        </div>
                        <div class="form-group">
                            <?php echo CHtml::activePasswordField($form,'verifyPassword',array('class'=>'form-control brd_top_non','placeholder'=>'Please retype your password')) ?>
                            <?php echo CHtml::error($form,'verifyPassword'); ?>                            
                        </div>                        
                        <!-- Change this to a button or input when using this as a form                      
                    <div class="col-md-12"><p class="hint">
    							<?php //echo UserModule::t("Minimum 4 characters required for password"); ?>
    							</p></div>-->   
                    <div class="col-md-4 pull-right">
                        <?php echo CHtml::submitButton(UserModule::t("OK"),array('class'=>'btn btn-warning btn-lg btn-block')); ?>
                    </div>
                    </fieldset> 
                   
                <?php echo CHtml::endForm(); 
                
                endif;?>
  
  </div> 