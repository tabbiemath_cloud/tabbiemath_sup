<?php

class RecoveryController extends Controller
{
	public $defaultAction = 'recovery';
	
	/**
	 * Recovery password
	 */
	public function actionRecovery () {
		$form = new UserRecoveryForm;
		if (Yii::app()->user->id) {
		    	$this->redirect(Yii::app()->controller->module->returnUrl);
		    } else {
				$email = ((isset($_GET['email']))?$_GET['email']:'');
				$activkey = ((isset($_GET['activkey']))?$_GET['activkey']:'');				
				if ($email&&$activkey) {					
					$form2 = new UserChangePassword;
		    		$find = User::model()->notsafe()->findByAttributes(array('email'=>urldecode($email)));					
		    		if(isset($find)&&$find->activkey==$activkey) {						
			    		if(isset($_POST['UserChangePassword'])) {
							$form2->attributes=$_POST['UserChangePassword'];
							if($form2->validate()) {
								$find->password = Yii::app()->controller->module->encrypting($form2->password);
								$find->activkey=Yii::app()->controller->module->encrypting(microtime().$form2->password);
								if ($find->status==0) {
									$find->status = 1;
								}
								$find->save();
				                Yii::app()->user->setFlash('activation',UserModule::t("Your password has been changed successfuly"));
				                ///$this->redirect(Yii::app()->controller->module->loginUrl);
							}
						} 
						$this->render('changepassword',array('form'=>$form2));
		    		} else {
		    			Yii::app()->user->setFlash('recoveryMessage',UserModule::t("Incorrect recovery link."));
						$this->redirect(Yii::app()->controller->module->recoveryUrl);
		    		}
		    	} else {
			    	if(isset($_POST['UserRecoveryForm'])) {
			    		$form->attributes=$_POST['UserRecoveryForm'];
			    		if($form->validate()) {
			    			$user = User::model()->notsafe()->findbyPk($form->user_id);
							if($user->usertype==7)
							{
								$students=Student::model()->findAll(array('condition'=>'TM_STU_Parent_Id='.$form->user_id));
								$html='';
								foreach($students as $student)
								{
									$username=User::model()->find(array('condition'=>'id='.$student->TM_STU_User_Id));

									$studentplans=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId='.$student->TM_STU_User_Id));

									$html.="<p><b>".$student->TM_STU_First_Name.' '.$student->TM_STU_Last_Name."</b></p>
											<p>Username: ".$username->username."</p>
											<p>Passowrd: ".$student->TM_STU_Password."</p>";

									if($studentplans)
									{
										$html.= "<p>Package Subscribed to: ";
										foreach($studentplans as $key =>  $studentplan)
										{
											$plan=Plan::model()->find(array('condition'=>'TM_PN_Id='.$studentplan->TM_SPN_PlanId));
											if($key==0)
											{
												$html.= $plan->TM_PN_Name;

											}
											else{
												$html.=",".$plan->TM_PN_Name;
											}
										}
										$html.="</p>";
									}
									else
									{
										$html.= "<p>Package Subscribed to: Not Subscribed</p>";
									}
								}
								$subject = UserModule::t("You have requested for Password Recovery");
								$message = "<p>Dear ".$user->profile->firstname.",</p>
											<p>You are receiving this email because you clicked on Forgot password Link in Tabbie.</p>
											<p>The following user/s registered under this email:</p>
											".$html."
											<p>If you have any issues, please contact support at support@tabbiemath.com. </p>
											<p>Regards,<br>The TabbieMath Team</p>";
								UserModule::sendMail($user->email,$subject,$message);
								Yii::app()->user->setFlash('recoveryMessage',UserModule::t(" Password has been sent to your email address"));
								$this->refresh();
							}
							else
							{
								$activation_url = 'http://' . $_SERVER['HTTP_HOST'].$this->createUrl(implode(Yii::app()->controller->module->recoveryUrl),array("activkey" => $user->activkey, "email" => $user->email));

								$subject = UserModule::t("You have requested for Password Recovery");
								$message = "<p>Dear ".$user->profile->firstname.",</p><p>You have requested to reset your password. To set a new password, please click on the link below. <br><a href='".$activation_url."'>Change password</a></p><p>If you have any issues, please contact support at support@tabbiemath.com. </p><p>Regards,<br>The TabbieMath Team";
								UserModule::sendMail($user->email,$subject,$message);

								Yii::app()->user->setFlash('recoveryMessage',UserModule::t("Instructions to reset your password has been sent to your email address"));
								$this->refresh();
							}
							/*$activation_url = 'http://' . $_SERVER['HTTP_HOST'].$this->createUrl(implode(Yii::app()->controller->module->recoveryUrl),array("activkey" => $user->activkey, "email" => $user->email));
							
							$subject = UserModule::t("You have requested for Password Recovery");
			    			$message = "<p>Dear ".$user->profile->firstname.",</p><p>You have requested to reset your password. To set a new password, please click on the link below. <br><a href='".$activation_url."'>Change password</a></p><p>If you have any issues, please contact support at support@tabbiemath.com. </p><p>Regards,<br>The TabbieMath Team";							
			    			UserModule::sendMail($user->email,$subject,$message);
			    			
							Yii::app()->user->setFlash('recoveryMessage',UserModule::t("Instructions to reset your password has been sent to your email address"));
			    			$this->refresh();*/
			    		}
			    	}
		    		$this->render('recovery',array('form'=>$form));
		    	}
		    }
	}

	public function actionEmailcheck()
	{
		$key=$_POST['value'];


		$user=User::model()->findByAttributes(array('email'=>$key));


		if($user)
		{
			$arr = array('error' => 'No');
			echo  json_encode($arr);

		}
		else
		{
			$arr = array('error' => 'Yes');
			echo  json_encode($arr);


		}
	}

}