<?php

class RegistrationController extends Controller
{
	public $defaultAction = 'registration';
	

	/**
	 * Declares class-based actions.
	 */

	public function actions()
	{
		return (isset($_POST['ajax']) && $_POST['ajax']==='registration-form')?array():array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
    public function actionSetlocation($id)
    {
       Yii::app()->session['location'] = $id;        
       $this->redirect(Yii::app()->controller->module->registrationUrl);
    }    
	/**
	 * Registration user
	 */
	public function actionRegistration() { 	   
		$model = new RegistrationForm;
		$profile=new Profile;
		$student=new Student();
		$model->scenario = 'registration';
		$profile->regMode = true; 
		
		// ajax validator
		if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
		{
			echo UActiveForm::validate(array($model,$profile));
			Yii::app()->end();
		}
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'registration-form') {
			echo UActiveForm::validate(array($model, $profile));
			Yii::app()->end();
		}

		if (Yii::app()->user->isAdmin()) {

			$this->redirect(Yii::app()->controller->module->returnUrladmin);
		}

		elseif (Yii::app()->user->isStudent()) {


			$this->redirect(Yii::app()->controller->module->returnUrlStudent);
		}

		elseif (Yii::app()->user->isParent()) {

			$this->redirect(Yii::app()->controller->module->returnUrlParent);
		}


		else
		{
			if(isset($_POST['RegistrationForm'])) {
				$model->attributes=$_POST['RegistrationForm'];
				$model->usertype='7';
				$model->username=$model->email;
				$profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
				$student->attributes=((isset($_POST['Student'])?$_POST['Student']:array()));

				if($model->validate()&  $profile->validate()&  $student->validate())
				{
					$soucePassword = $model->password;
					$passwordnoncrypted = $this->generateRandomString();
					$temp=new Userpass();                        
				   // $temp->password=$passwordnoncrypted;                                                
					$temp->lastname=$profile->lastname;
					$temp->firstname=$profile->firstname;
					$temp->phonenumber=$profile->phonenumber;
					$temp->username=$model->email;
					$temp->email=$model->email;
					$temp->activkey=UserModule::encrypting(microtime().$model->password);;
					$temp->createtime=time();
					$temp->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;;
					$temp->student_lastname=$student->TM_STU_Last_Name;
					$temp->student_firstname=$student->TM_STU_First_Name;
					$temp->student_school=$student->TM_STU_School;
					if($temp->student_school=='0'):
						$temp->schoolcustomname=$student->TM_STU_SchoolCustomname;
					endif;
					$temp->student_city=$student->TM_STU_City;
					$temp->student_state=$student->TM_STU_State;
					$temp->student_country=$student->TM_STU_Country_Id; 
					$temp->studentid=$student->TM_STU_student_Id;
					$temp->password=$student->TM_STU_Password;
					$temp->location=Yii::app()->session['location']; 
					if($temp->save(false))
					{
						$this->redirect(array("/user/registration/subscribe", 'id' => $temp->id));
					}                     
				}
			}
			$this->render('/user/registration',array('model'=>$model,'profile'=>$profile,'student'=>$student));
		} 
	}
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function actionSubscribe($id)
    {
        $studentplan=new StudentPlan();
        $parent=Userpass::model()->findByPk($id);
        //$id=base64_encode($_GET['subcription']);             
        $this->render('/user/studentsubscription',array(
			'model'=>$studentplan,
            'id'=>$id,            
            'parent'=>$parent,            
		));  
    }
    public function checkUsername($username)
    {
        $usercount=User::model()->cound(array('condition'=>'username='.$username));
        if($usercount>0):
            return false;
        else:
            return true;
        endif;
    }
	public function actionPaymentcompletepaypal()
	{
		if(base64_decode($_GET['setup'])=='Completed')
		{
			$plan=Plan::model()->findByPk(base64_decode($_GET['application']));
			$duration=$plan->TM_PN_Duration+1;
			$tempdetails=Userpass::model()->find(array('condition'=>'id='.base64_decode($_GET['supredent'])));
			if(count($tempdetails)>0):
				$studentuser = new User();
				$studentfirstname=ltrim($tempdetails->student_firstname);
				$studentlastname=ltrim($tempdetails->student_lastname);
				$username = strtoupper(substr($studentfirstname, 0, 1) . substr($studentlastname, 0, 1) . rand(11111, 99999));
				$schoolid=$tempdetails['student_school'];
				$password = $this->generateRandomString();
				$studentuser->username = $username;
				$studentuser->school_id = $schoolid;
				$studentuser->password = md5($password);
				$studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
				$studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
				$studentuser->superuser = 0;
				$studentuser->usertype = '6';
				$studentuser->createtime = time();
				$studentuser->email=$username;
				$studentuser->status = '1';
				$checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$tempdetails->email . '"'));
				if (count($checkprevmail) != 0):
					$parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "'.$studentfirstname.'" AND TM_STU_Last_Name LIKE "'.$studentlastname.'" AND TM_STU_Parent_Id='.$checkprevmail->id));
					if(count($parentStudent)>0):
						$connection = CActiveRecord::getDbConnection();
						/*$sql="DELETE tm_student_plan FROM tm_student_plan INNER JOIN tm_plan ON tm_plan.TM_PN_Id = tm_student_plan.TM_SPN_PlanId WHERE tm_plan.TM_PN_Type = 1 AND tm_student_plan.TM_SPN_StudentId=".$parentStudent['TM_STU_User_Id']." ";
                        $command=$connection->createCommand($sql);
                        $dataReader=$command->query();*/
						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = $plan->TM_PN_Status;
						$sttudentnewplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
						if($sttudentnewplan->save(false)):
							$sql="DELETE FROM tm_group_students WHERE TM_GRP_STUId=".$parentStudent['TM_STU_User_Id']." AND TM_GRP_SCL_Id=".$schoolid." ";
							$command=$connection->createCommand($sql);
							$dataReader=$command->query();
							$schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$schoolid."' AND TM_SCL_SD_Id='".$plan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if(count($schoolgrps)>0):
								$grpstudents=New GroupStudents();
								$grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id=$plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id=$schoolid;
								$grpstudents->TM_GRP_SD_Id=$plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId=$parentStudent->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName=$studentfirstname.' '.$studentlastname;
								$grpstudents->save(false);
							endif;
						endif;
						Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".urldecode($_GET['transaction'])));
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						// UserModule::sendRegistrationMail($checkprevmail->id,$parentStudent->TM_STU_User_Id,$parentStudent->TM_STU_Parentpass,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					else:
						$studentuser->save(false);
						$studentuserprofile = new Profile();
						$studentuserprofile->user_id = $studentuser->id;
						$studentuserprofile->lastname = $studentlastname;
						$studentuserprofile->firstname = $studentfirstname;
						$studentuserprofile->save(false);

						$student = new Student();
						$student->TM_STU_Parent_Id=$checkprevmail->id;
						$student->TM_STU_User_Id = $studentuser->id;
						$student->TM_STU_First_Name = $studentfirstname;
						$student->TM_STU_Last_Name = $studentlastname;
						$student->TM_STU_Password = $password;
						$student->TM_STU_School=$tempdetails->student_school;
						$student->TM_STU_School_Id=$tempdetails->student_school;
						$student->TM_STU_Email=$tempdetails->email;
						if($tempdetails->student_school=='0'):
							$student->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
							$student->TM_STU_City=$tempdetails->student_city;
							$student->TM_STU_State=$tempdetails->student_state;
						else:
							$school=School::model()->findByPk($tempdetails->student_school);
							$student->TM_STU_City=$school->TM_SCL_City;
							$student->TM_STU_State=$school->TM_SCL_State;
						endif;
						$student->TM_STU_Country_Id=$tempdetails->student_country;
						$student->TM_STU_CreatedBy=$checkprevmail->id;
						$student->TM_STU_Status= '0';
						$studentmodel=Student::model()->find(array('condition'=>'TM_STU_Parent_Id='.$checkprevmail->id.''));
						$parentpswd=$studentmodel->TM_STU_Parentpass;
						$student->TM_STU_Parentpass=$parentpswd;
						$student->save(false);

						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = '0';
						$sttudentnewplan->TM_SPN_SubjectId='1';
						$sttudentnewplan->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
						$sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
						if (base64_decode($_POST['voucherid'], true)) {
							$voucher=base64_decode($_POST['voucherid']);
						} else {
							$voucher=$_POST['voucherid'];
						}
						$sttudentnewplan->TM_SPN_CouponId=$voucher;
						$sttudentnewplan->TM_SPN_PaymentReference=$_GET['transaction'];
						$sttudentnewplan->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
						$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
						$sttudentnewplan->TM_SPN_Currency=$currency;
						$sttudentnewplan->TM_SPN_Provider='1';
						$sttudentnewplan->save(false);

						if ($student->save(false)) {
							$schoolgrps = SchoolGroups::model()->find(array('condition' => "TM_SCL_Id='" . $schoolid . "' AND TM_SCL_SD_Id='" . $plan->TM_PN_Standard_Id . "' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if (count($schoolgrps) > 0):
								$grpstudents = New GroupStudents();
								$grpstudents->TM_GRP_Id = $schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id = $plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id = $schoolid;
								$grpstudents->TM_GRP_SD_Id = $plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId = $student->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName = $studentfirstname . ' ' . $studentlastname;
								$grpstudents->save(false);
							endif;
						}
						Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".urldecode($_GET['transaction'])));
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						UserModule::sendRegistrationMail($checkprevmail->id,$studentuser->id,$parentpswd,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					endif;
				else:
					$parent = new User;
					$parentprofile=new Profile;
					$parent->username=$tempdetails->email;
					$parentpassword=$this->generateRandomString();
					$parent->password=UserModule::encrypting($parentpassword);
					$parent->email=$tempdetails->email;
					$parent->activkey=$tempdetails->activkey;
					$parent->createtime=$tempdetails->createtime;
					$parent->usertype='7';
					$parent->status='1';
					$parent->location=$tempdetails->location;
					$parent->save(false);
					$parentprofile->user_id=$parent->id;
					$parentprofile->lastname=$tempdetails->lastname;
					$parentprofile->firstname=$tempdetails->firstname;
					$parentprofile->phonenumber=$tempdetails->phonenumber;
					$parentprofile->save(false);
					$studentfirstname=ltrim($tempdetails->student_firstname);
					$studentlastname=ltrim($tempdetails->student_lastname);
					$studentusername=strtoupper(substr($studentfirstname, 0,1).substr($studentlastname, 0,1).rand(11111,99999));
					$studentpassword=$this->generateRandomString();
					$studentuser= new User;
					$studentuser->username=$studentusername;
					$studentuser->password=UserModule::encrypting($studentpassword);
					$studentuser->email=$studentusername;
					$studentuser->activkey=UserModule::encrypting(microtime().$studentpassword);
					$studentuser->createtime=time();
					$studentuser->usertype='6';
					$studentuser->school_id =$tempdetails->student_school;
					$studentuser->status='1';
					$studentuser->save(false);
					$studentuserprofile=new Profile();
					$studentuserprofile->user_id=$studentuser->id;
					$studentuserprofile->firstname=$tempdetails->student_firstname;
					$studentuserprofile->lastname=$tempdetails->student_lastname;
					$studentuserprofile->save(false);
					$studentmaster=new Student();
					$studentmaster->TM_STU_Parent_Id=$parent->id;
					$studentmaster->TM_STU_User_Id=$studentuser->id;
					$studentmaster->TM_STU_First_Name=$tempdetails->student_firstname;
					$studentmaster->TM_STU_Last_Name=$tempdetails->student_lastname;
					$studentmaster->TM_STU_Password=$studentpassword;
					$studentmaster->TM_STU_Parentpass=$parentpassword;
					$studentmaster->TM_STU_School=$tempdetails->student_school;
					$studentmaster->TM_STU_School_Id=$tempdetails->student_school;
					if($tempdetails->student_school=='0'):
						$studentmaster->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
						$studentmaster->TM_STU_City=$tempdetails->student_city;
						$studentmaster->TM_STU_State=$tempdetails->student_state;
					else:
						$school=School::model()->findByPk($tempdetails->student_school);
						$studentmaster->TM_STU_City=$school->TM_SCL_City;
						$studentmaster->TM_STU_State=$school->TM_SCL_State;
					endif;
					$studentmaster->TM_STU_Country_Id=$tempdetails->student_country;
					$studentmaster->TM_STU_CreatedBy=$parent->id;
					$studentmaster->TM_STU_Status= '0';
					$studentmaster->save(false);
					$model=new StudentPlan();
					$model->TM_SPN_PlanId=$plan->TM_PN_Id;
					$model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
					$model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
					$model->TM_SPN_SubjectId='1';
					$model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
					$model->TM_SPN_StudentId=$studentuser->id;
					$model->TM_SPN_Status='0';
					$model->TM_SPN_PaymentReference=$_GET['transaction'];
					$model->TM_SPN_CouponId=base64_decode($_GET['voucher']);
					$model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
					$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
					$model->TM_SPN_Currency=$currency;
					$model->TM_SPN_Provider='1';
					$model->save(false);
					Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".urldecode($_GET['transaction'])));
					Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
					UserModule::sendRegistrationMail($parent->id,$studentuser->id,$parentpassword,$this->createAbsoluteUrl('/user/login'));
					$tempdetails->delete();
					$this->redirect(Yii::app()->controller->module->registrationUrl);
				endif;
			else:
				Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Invalid Details. Contact Admin to activate your account."));
				$this->redirect(Yii::app()->controller->module->registrationUrl);
			endif;
		}
		elseif($_GET['cancel']=='yes')
		{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The transaction has been cancelled. If you are seeing this in error, please contact support@tabbiemath.com."));
			$this->redirect(Yii::app()->controller->module->registrationUrl);
		}
		else{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Pending Payment. Contact Admin to activate your account."));
			$this->redirect(Yii::app()->controller->module->registrationUrl);
		}
	}
	public function actionPaymentComplete()
	{
		if(isset($_GET['supredent'])&& isset($_GET['application']))
		{
			$plan=Plan::model()->findByPk(base64_decode($_GET['application']));
			$duration=$plan->TM_PN_Duration+1;
			$tempdetails=Userpass::model()->find(array('condition'=>'id='.base64_decode($_GET['supredent'])));
			if(count($tempdetails)>0):
				$studentuser = new User();
				$studentfirstname=ltrim($tempdetails->student_firstname);
				$studentlastname=ltrim($tempdetails->student_lastname);
				$username = strtoupper(substr($studentfirstname, 0, 1) . substr($studentlastname, 0, 1) . rand(11111, 99999));
				$schoolid=$tempdetails['student_school'];
				$password = $this->generateRandomString();
				$studentuser->username = $username;
				$studentuser->school_id = $schoolid;
				$studentuser->password = md5($password);
				$studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
				$studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
				$studentuser->superuser = 0;
				$studentuser->usertype = '6';
				$studentuser->createtime = time();
				$studentuser->email=$username;
				$studentuser->status = '1';
				$checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$tempdetails->email . '"'));
				if (count($checkprevmail) != 0):
					$parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "'.$studentfirstname.'" AND TM_STU_Last_Name LIKE "'.$studentlastname.'" AND TM_STU_Parent_Id='.$checkprevmail->id));
					if(count($parentStudent)>0):
						$connection = CActiveRecord::getDbConnection();
						/*$sql="DELETE tm_student_plan FROM tm_student_plan INNER JOIN tm_plan ON tm_plan.TM_PN_Id = tm_student_plan.TM_SPN_PlanId WHERE tm_plan.TM_PN_Type = 1 AND tm_student_plan.TM_SPN_StudentId=".$parentStudent['TM_STU_User_Id']." ";
                        $command=$connection->createCommand($sql);
                        $dataReader=$command->query();*/
						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = $plan->TM_PN_Status;
						$sttudentnewplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
						if($sttudentnewplan->save(false)):
							$sql="DELETE FROM tm_group_students WHERE TM_GRP_STUId=".$parentStudent['TM_STU_User_Id']." AND TM_GRP_SCL_Id=".$schoolid." ";
							$command=$connection->createCommand($sql);
							$dataReader=$command->query();
							$schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$schoolid."' AND TM_SCL_SD_Id='".$plan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if(count($schoolgrps)>0):
								$grpstudents=New GroupStudents();
								$grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id=$plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id=$schoolid;
								$grpstudents->TM_GRP_SD_Id=$plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId=$parentStudent->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName=$studentfirstname.' '.$studentlastname;
								$grpstudents->save(false);
							endif;
						endif;
						Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						// UserModule::sendRegistrationMail($checkprevmail->id,$parentStudent->TM_STU_User_Id,$parentStudent->TM_STU_Parentpass,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					else:
						$studentuser->save(false);
						$studentuserprofile = new Profile();
						$studentuserprofile->user_id = $studentuser->id;
						$studentuserprofile->lastname = $studentlastname;
						$studentuserprofile->firstname = $studentfirstname;
						$studentuserprofile->save(false);

						$student = new Student();
						$student->TM_STU_Parent_Id=$checkprevmail->id;
						$student->TM_STU_User_Id = $studentuser->id;
						$student->TM_STU_First_Name = $studentfirstname;
						$student->TM_STU_Last_Name = $studentlastname;
						$student->TM_STU_Password = $password;
						$student->TM_STU_School=$tempdetails->student_school;
						$student->TM_STU_School_Id=$tempdetails->student_school;
						$student->TM_STU_Email=$tempdetails->email;
						if($tempdetails->student_school=='0'):
							$student->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
							$student->TM_STU_City=$tempdetails->student_city;
							$student->TM_STU_State=$tempdetails->student_state;
						else:
							$school=School::model()->findByPk($tempdetails->student_school);
							$student->TM_STU_City=$school->TM_SCL_City;
							$student->TM_STU_State=$school->TM_SCL_State;
						endif;
						$student->TM_STU_Country_Id=$tempdetails->student_country;
						$student->TM_STU_CreatedBy=$checkprevmail->id;
						$student->TM_STU_Status= '0';
						$studentmodel=Student::model()->find(array('condition'=>'TM_STU_Parent_Id='.$checkprevmail->id.''));
						$parentpswd=$studentmodel->TM_STU_Parentpass;
						$student->TM_STU_Parentpass=$parentpswd;
						$student->save(false);

						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = '0';
						$sttudentnewplan->TM_SPN_SubjectId='1';
						$sttudentnewplan->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
						$sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
						if (base64_decode($_POST['voucherid'], true)) {
							$voucher=base64_decode($_POST['voucherid']);
						} else {
							$voucher=$_POST['voucherid'];
						}
						$sttudentnewplan->TM_SPN_CouponId=$voucher;
						$sttudentnewplan->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
						$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
						$sttudentnewplan->TM_SPN_Currency=$currency;
						$sttudentnewplan->save(false);

						if ($student->save(false)) {
							$schoolgrps = SchoolGroups::model()->find(array('condition' => "TM_SCL_Id='" . $schoolid . "' AND TM_SCL_SD_Id='" . $plan->TM_PN_Standard_Id . "' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if (count($schoolgrps) > 0):
								$grpstudents = New GroupStudents();
								$grpstudents->TM_GRP_Id = $schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id = $plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id = $schoolid;
								$grpstudents->TM_GRP_SD_Id = $plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId = $student->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName = $studentfirstname . ' ' . $studentlastname;
								$grpstudents->save(false);
							endif;
						}
						Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						UserModule::sendRegistrationMail($checkprevmail->id,$studentuser->id,$parentpswd,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					endif;
				else:
					$parent = new User;
					$parentprofile=new Profile;
					$parent->username=$tempdetails->email;
					$parentpassword=$this->generateRandomString();
					$parent->password=UserModule::encrypting($parentpassword);
					$parent->email=$tempdetails->email;
					$parent->activkey=$tempdetails->activkey;
					$parent->createtime=$tempdetails->createtime;
					$parent->usertype='7';
					$parent->status='1';
					$parent->location=$tempdetails->location;
					$parent->save(false);
					$parentprofile->user_id=$parent->id;
					$parentprofile->lastname=$tempdetails->lastname;
					$parentprofile->firstname=$tempdetails->firstname;
					$parentprofile->phonenumber=$tempdetails->phonenumber;
					$parentprofile->save(false);
					$studentfirstname=ltrim($tempdetails->student_firstname);
					$studentlastname=ltrim($tempdetails->student_lastname);
					$studentusername=strtoupper(substr($studentfirstname, 0,1).substr($studentlastname, 0,1).rand(11111,99999));
					$studentpassword=$this->generateRandomString();
					$studentuser= new User;
					$studentuser->username=$studentusername;
					$studentuser->password=UserModule::encrypting($studentpassword);
					$studentuser->email=$studentusername;
					$studentuser->activkey=UserModule::encrypting(microtime().$studentpassword);
					$studentuser->createtime=time();
					$studentuser->usertype='6';
					$studentuser->school_id =$tempdetails->student_school;
					$studentuser->status='1';
					$studentuser->save(false);
					$studentuserprofile=new Profile();
					$studentuserprofile->user_id=$studentuser->id;
					$studentuserprofile->firstname=$tempdetails->student_firstname;
					$studentuserprofile->lastname=$tempdetails->student_lastname;
					$studentuserprofile->save(false);
					$studentmaster=new Student();
					$studentmaster->TM_STU_Parent_Id=$parent->id;
					$studentmaster->TM_STU_User_Id=$studentuser->id;
					$studentmaster->TM_STU_First_Name=$tempdetails->student_firstname;
					$studentmaster->TM_STU_Last_Name=$tempdetails->student_lastname;
					$studentmaster->TM_STU_Password=$studentpassword;
					$studentmaster->TM_STU_Parentpass=$parentpassword;
					$studentmaster->TM_STU_School=$tempdetails->student_school;
					$studentmaster->TM_STU_School_Id=$tempdetails->student_school;
					if($tempdetails->student_school=='0'):
						$studentmaster->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
						$studentmaster->TM_STU_City=$tempdetails->student_city;
						$studentmaster->TM_STU_State=$tempdetails->student_state;
					else:
						$school=School::model()->findByPk($tempdetails->student_school);
						$studentmaster->TM_STU_City=$school->TM_SCL_City;
						$studentmaster->TM_STU_State=$school->TM_SCL_State;
					endif;
					$studentmaster->TM_STU_Country_Id=$tempdetails->student_country;
					$studentmaster->TM_STU_CreatedBy=$parent->id;
					$studentmaster->TM_STU_Status= '0';
					$studentmaster->save(false);
					$model=new StudentPlan();
					$model->TM_SPN_PlanId=$plan->TM_PN_Id;
					$model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
					$model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
					$model->TM_SPN_SubjectId='1';
					$model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
					$model->TM_SPN_StudentId=$studentuser->id;
					$model->TM_SPN_Status='0';
					$model->TM_SPN_PaymentReference=$_GET['transaction'];
					if (base64_decode($_POST['voucherid'], true)) {
						$voucher=base64_decode($_POST['voucherid']);
					} else {
						$voucher=$_POST['voucherid'];
					}
					$model->TM_SPN_CouponId=$voucher;
					$model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
					$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
					$model->TM_SPN_Currency=$currency;
					$model->save(false);
					Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
					Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
					UserModule::sendRegistrationMail($parent->id,$studentuser->id,$parentpassword,$this->createAbsoluteUrl('/user/login'));
					$tempdetails->delete();
					$this->redirect(Yii::app()->controller->module->registrationUrl);
				endif;
			else:
				Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Invalid Details. Contact Admin to activate your account."));
				$this->redirect(Yii::app()->controller->module->registrationUrl);
			endif;
		}
		elseif(isset($_POST['idst']))
		{
			$plan=Plan::model()->findByPk($_POST['plan']);
			$duration=$plan->TM_PN_Duration+1;
			$tempdetails=Userpass::model()->find(array('condition'=>'id='.base64_decode($_POST['idst'])));
			if(count($tempdetails)>0):
				$studentuser = new User();
				$studentfirstname=ltrim($tempdetails->student_firstname);
				$studentlastname=ltrim($tempdetails->student_lastname);
				$username = strtoupper(substr($studentfirstname, 0, 1) . substr($studentlastname, 0, 1) . rand(11111, 99999));
				$schoolid=$tempdetails['student_school'];
				$password = $this->generateRandomString();
				$studentuser->username = $username;
				$studentuser->school_id = $schoolid;
				$studentuser->password = md5($password);
				$studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
				$studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
				$studentuser->superuser = 0;
				$studentuser->usertype = '6';
				$studentuser->createtime = time();
				$studentuser->email=$username;
				$studentuser->status = '1';
				$checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$tempdetails->email . '"'));
				if (count($checkprevmail) != 0):
					$parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "'.$studentfirstname.'" AND TM_STU_Last_Name LIKE "'.$studentlastname.'" AND TM_STU_Parent_Id='.$checkprevmail->id));
					if(count($parentStudent)>0):
						$connection = CActiveRecord::getDbConnection();
						/*$sql="DELETE tm_student_plan FROM tm_student_plan INNER JOIN tm_plan ON tm_plan.TM_PN_Id = tm_student_plan.TM_SPN_PlanId WHERE tm_plan.TM_PN_Type = 1 AND tm_student_plan.TM_SPN_StudentId=".$parentStudent['TM_STU_User_Id']." ";
						$command=$connection->createCommand($sql);
						$dataReader=$command->query();*/
						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = $plan->TM_PN_Status;
						$sttudentnewplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
						if($sttudentnewplan->save(false)):
							$sql="DELETE FROM tm_group_students WHERE TM_GRP_STUId=".$parentStudent['TM_STU_User_Id']." AND TM_GRP_SCL_Id=".$schoolid." ";
							$command=$connection->createCommand($sql);
							$dataReader=$command->query();
							$schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$schoolid."' AND TM_SCL_SD_Id='".$plan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if(count($schoolgrps)>0):
								$grpstudents=New GroupStudents();
								$grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id=$plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id=$schoolid;
								$grpstudents->TM_GRP_SD_Id=$plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId=$parentStudent->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName=$studentfirstname.' '.$studentlastname;
								$grpstudents->save(false);
							endif;
						endif;
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						UserModule::sendRegistrationMail($checkprevmail->id,$parentStudent->TM_STU_User_Id,$parentStudent->TM_STU_Parentpass,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					else:
						$studentuser->save(false);
						$studentuserprofile = new Profile();
						$studentuserprofile->user_id = $studentuser->id;
						$studentuserprofile->lastname = $studentlastname;
						$studentuserprofile->firstname = $studentfirstname;
						$studentuserprofile->save(false);

						$student = new Student();
						$student->TM_STU_Parent_Id=$checkprevmail->id;
						$student->TM_STU_User_Id = $studentuser->id;
						$student->TM_STU_First_Name = $studentfirstname;
						$student->TM_STU_Last_Name = $studentlastname;
						$student->TM_STU_Password = $password;
						$student->TM_STU_School=$tempdetails->student_school;
						$student->TM_STU_School_Id=$tempdetails->student_school;
						$student->TM_STU_Email=$tempdetails->email;
						if($tempdetails->student_school=='0'):
							$student->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
							$student->TM_STU_City=$tempdetails->student_city;
							$student->TM_STU_State=$tempdetails->student_state;
						else:
							$school=School::model()->findByPk($tempdetails->student_school);
							$student->TM_STU_City=$school->TM_SCL_City;
							$student->TM_STU_State=$school->TM_SCL_State;
						endif;
						$student->TM_STU_Country_Id=$tempdetails->student_country;
						$student->TM_STU_CreatedBy=$checkprevmail->id;
						$student->TM_STU_Status= '0';
						$studentmodel=Student::model()->find(array('condition'=>'TM_STU_Parent_Id='.$checkprevmail->id.''));
						$parentpswd=$studentmodel->TM_STU_Parentpass;
						$student->TM_STU_Parentpass=$parentpswd;
						$student->save(false);

						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = '0';
						$sttudentnewplan->TM_SPN_SubjectId='1';
						$sttudentnewplan->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
						$sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
						if (base64_decode($_POST['voucherid'], true)) {
							$voucher=base64_decode($_POST['voucherid']);
						} else {
							$voucher=$_POST['voucherid'];
						}
						$sttudentnewplan->TM_SPN_CouponId=$voucher;
						$sttudentnewplan->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
						$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
						$sttudentnewplan->TM_SPN_Currency=$currency;
						$sttudentnewplan->save(false);

						if ($student->save(false)) {
							$schoolgrps = SchoolGroups::model()->find(array('condition' => "TM_SCL_Id='" . $schoolid . "' AND TM_SCL_SD_Id='" . $plan->TM_PN_Standard_Id . "' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if (count($schoolgrps) > 0):
								$grpstudents = New GroupStudents();
								$grpstudents->TM_GRP_Id = $schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id = $plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id = $schoolid;
								$grpstudents->TM_GRP_SD_Id = $plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId = $student->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName = $studentfirstname . ' ' . $studentlastname;
								$grpstudents->save(false);
							endif;
						}
						//Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						UserModule::sendRegistrationMail($checkprevmail->id,$studentuser->id,$parentpswd,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					endif;
				else:
					$parent = new User;
					$parentprofile=new Profile;
					$parent->username=$tempdetails->username;
					$parentpassword=$this->generateRandomString();
					$parent->password=UserModule::encrypting($parentpassword);
					$parent->email=$tempdetails->email;
					$parent->activkey=$tempdetails->activkey;
					$parent->createtime=$tempdetails->createtime;
					$parent->usertype='7';
					$parent->status='1';
					$parent->location=$tempdetails->location;
					$parent->save(false);
					$parentprofile->user_id=$parent->id;
					$parentprofile->lastname=$tempdetails->lastname;
					$parentprofile->firstname=$tempdetails->firstname;
					$parentprofile->phonenumber=$tempdetails->phonenumber;
					$parentprofile->save(false);
					$studentfirstname=ltrim($tempdetails->student_firstname);
					$studentlastname=ltrim($tempdetails->student_lastname);
					$studentusername=strtoupper(substr($studentfirstname, 0,1).substr($studentlastname, 0,1).rand(11111,99999));
					$studentpassword=$this->generateRandomString();
					$studentuser= new User;
					$studentuser->username=$studentusername;
					$studentuser->password=UserModule::encrypting($studentpassword);
					$studentuser->email=$studentusername;
					$studentuser->activkey=UserModule::encrypting(microtime().$studentpassword);
					$studentuser->createtime=time();
					$studentuser->usertype='6';
					$studentuser->school_id =$tempdetails->student_school;
					$studentuser->status='1';
					$studentuser->save(false);
					$studentuserprofile=new Profile();
					$studentuserprofile->user_id=$studentuser->id;
					$studentuserprofile->firstname=$tempdetails->student_firstname;
					$studentuserprofile->lastname=$tempdetails->student_lastname;
					$studentuserprofile->save(false);
					$studentmaster=new Student();
					$studentmaster->TM_STU_Parent_Id=$parent->id;
					$studentmaster->TM_STU_User_Id=$studentuser->id;
					$studentmaster->TM_STU_First_Name=$tempdetails->student_firstname;
					$studentmaster->TM_STU_Last_Name=$tempdetails->student_lastname;
					$studentmaster->TM_STU_Password=$studentpassword;
					$studentmaster->TM_STU_Parentpass=$parentpassword;
					$studentmaster->TM_STU_School=$tempdetails->student_school;
					$studentmaster->TM_STU_School_Id=$tempdetails->student_school;
					if($tempdetails->student_school=='0'):
						$studentmaster->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
						$studentmaster->TM_STU_City=$tempdetails->student_city;
						$studentmaster->TM_STU_State=$tempdetails->student_state;
					else:
						$school=School::model()->findByPk($tempdetails->student_school);
						$studentmaster->TM_STU_City=$school->TM_SCL_City;
						$studentmaster->TM_STU_State=$school->TM_SCL_State;
					endif;
					$studentmaster->TM_STU_Country_Id=$tempdetails->student_country;
					$studentmaster->TM_STU_CreatedBy=$parent->id;
					$studentmaster->TM_STU_Status= '0';
					$studentmaster->save(false);
					$model=new StudentPlan();
					$model->TM_SPN_PlanId=$plan->TM_PN_Id;
					$model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
					$model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
					$model->TM_SPN_SubjectId='1';
					$model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
					$model->TM_SPN_StudentId=$studentuser->id;
					$model->TM_SPN_Status='0';
					if (base64_decode($_POST['voucherid'], true)) {
						$voucher=base64_decode($_POST['voucherid']);
					} else {
						$voucher=$_POST['voucherid'];
					}
					$model->TM_SPN_CouponId=$voucher;
					$model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
					$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
					$model->TM_SPN_Currency=$currency;
					$model->save(false);
					//Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));
					Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
					UserModule::sendRegistrationMail($parent->id,$studentuser->id,$parentpassword,$this->createAbsoluteUrl('/user/login'));
					$tempdetails->delete();
					$this->redirect(Yii::app()->controller->module->registrationUrl);
				endif;
			else:
				Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Invalid Details. Contact Admin to activate your account."));
				$this->redirect(Yii::app()->controller->module->registrationUrl);
			endif;

		}
		elseif($_GET['cancel']=='yes')
		{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The transaction has been cancelled. If you are seeing this in error, please contact support@tabbiemath.com."));
			$this->redirect(Yii::app()->controller->module->registrationUrl);
		}
		else{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The payment has failed. Please check for the message from Payu for the failure"));
			$this->redirect(Yii::app()->controller->module->registrationUrl);
		}
	}
	public function actionPaymentcompletestripe()
	{

		if(base64_decode($_GET['setup'])=='Completed')
		{
			$plan=Plan::model()->findByPk(base64_decode($_GET['application']));
			$duration=$plan->TM_PN_Duration+1;
			$tempdetails=Userpass::model()->find(array('condition'=>'id='.base64_decode($_GET['supredent'])));
			if(count($tempdetails)>0):
				$studentuser = new User();
				$studentfirstname=ltrim($tempdetails->student_firstname);
				$studentlastname=ltrim($tempdetails->student_lastname);
				$username = strtoupper(substr($studentfirstname, 0, 1) . substr($studentlastname, 0, 1) . rand(11111, 99999));
				$schoolid=$tempdetails['student_school'];
				$password = $this->generateRandomString();
				$studentuser->username = $username;
				$studentuser->school_id = $schoolid;
				$studentuser->password = md5($password);
				$studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
				$studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
				$studentuser->superuser = 0;
				$studentuser->usertype = '6';
				$studentuser->createtime = time();
				$studentuser->email=$username;
				$studentuser->status = '1';
				$checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$tempdetails->email . '"'));
				if (count($checkprevmail) != 0):
					$parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "'.$studentfirstname.'" AND TM_STU_Last_Name LIKE "'.$studentlastname.'" AND TM_STU_Parent_Id='.$checkprevmail->id));
					if(count($parentStudent)>0):
						$connection = CActiveRecord::getDbConnection();
						/*$sql="DELETE tm_student_plan FROM tm_student_plan INNER JOIN tm_plan ON tm_plan.TM_PN_Id = tm_student_plan.TM_SPN_PlanId WHERE tm_plan.TM_PN_Type = 1 AND tm_student_plan.TM_SPN_StudentId=".$parentStudent['TM_STU_User_Id']." ";
                        $command=$connection->createCommand($sql);
                        $dataReader=$command->query();*/
						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = $plan->TM_PN_Status;
						$sttudentnewplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
						if($sttudentnewplan->save(false)):
							$sql="DELETE FROM tm_group_students WHERE TM_GRP_STUId=".$parentStudent['TM_STU_User_Id']." AND TM_GRP_SCL_Id=".$schoolid." ";
							$command=$connection->createCommand($sql);
							$dataReader=$command->query();
							$schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$schoolid."' AND TM_SCL_SD_Id='".$plan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if(count($schoolgrps)>0):
								$grpstudents=New GroupStudents();
								$grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id=$plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id=$schoolid;
								$grpstudents->TM_GRP_SD_Id=$plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId=$parentStudent->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName=$studentfirstname.' '.$studentlastname;
								$grpstudents->save(false);
							endif;
						endif;
						Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".urldecode($_GET['transaction'])));
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						// UserModule::sendRegistrationMail($checkprevmail->id,$parentStudent->TM_STU_User_Id,$parentStudent->TM_STU_Parentpass,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					else:
						$studentuser->save(false);
						$studentuserprofile = new Profile();
						$studentuserprofile->user_id = $studentuser->id;
						$studentuserprofile->lastname = $studentlastname;
						$studentuserprofile->firstname = $studentfirstname;
						$studentuserprofile->save(false);

						$student = new Student();
						$student->TM_STU_Parent_Id=$checkprevmail->id;
						$student->TM_STU_User_Id = $studentuser->id;
						$student->TM_STU_First_Name = $studentfirstname;
						$student->TM_STU_Last_Name = $studentlastname;
						$student->TM_STU_Password = $password;
						$student->TM_STU_School=$tempdetails->student_school;
						$student->TM_STU_School_Id=$tempdetails->student_school;
						$student->TM_STU_Email=$tempdetails->email;
						if($tempdetails->student_school=='0'):
							$student->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
							$student->TM_STU_City=$tempdetails->student_city;
							$student->TM_STU_State=$tempdetails->student_state;
						else:
							$school=School::model()->findByPk($tempdetails->student_school);
							$student->TM_STU_City=$school->TM_SCL_City;
							$student->TM_STU_State=$school->TM_SCL_State;
						endif;
						$student->TM_STU_Country_Id=$tempdetails->student_country;
						$student->TM_STU_CreatedBy=$checkprevmail->id;
						$student->TM_STU_Status= '0';
						$studentmodel=Student::model()->find(array('condition'=>'TM_STU_Parent_Id='.$checkprevmail->id.''));
						$parentpswd=$studentmodel->TM_STU_Parentpass;
						$student->TM_STU_Parentpass=$parentpswd;
						$student->save(false);

						$sttudentnewplan = new StudentPlan();
						$sttudentnewplan->TM_SPN_PlanId = $plan->TM_PN_Id;
						$sttudentnewplan->TM_SPN_SyllabusId = $plan->TM_PN_Syllabus_Id;
						$sttudentnewplan->TM_SPN_StandardId = $plan->TM_PN_Standard_Id;
						$sttudentnewplan->TM_SPN_Status = '0';
						$sttudentnewplan->TM_SPN_SubjectId='1';
						$sttudentnewplan->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
						$sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
						if (base64_decode($_POST['voucherid'], true)) {
							$voucher=base64_decode($_POST['voucherid']);
						} else {
							$voucher=$_POST['voucherid'];
						}
						$sttudentnewplan->TM_SPN_CouponId=$voucher;
						$sttudentnewplan->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
						$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
						$sttudentnewplan->TM_SPN_Currency=$currency;
						$sttudentnewplan->TM_SPN_Provider='2';
						$sttudentnewplan->TM_SPN_PaymentReference=$_GET['transaction'];
						$sttudentnewplan->save(false);

						if ($student->save(false)) {
							$schoolgrps = SchoolGroups::model()->find(array('condition' => "TM_SCL_Id='" . $schoolid . "' AND TM_SCL_SD_Id='" . $plan->TM_PN_Standard_Id . "' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
							if (count($schoolgrps) > 0):
								$grpstudents = New GroupStudents();
								$grpstudents->TM_GRP_Id = $schoolgrps->TM_SCL_GRP_Id;
								$grpstudents->TM_GRP_PN_Id = $plan->TM_PN_Id;
								$grpstudents->TM_GRP_SCL_Id = $schoolid;
								$grpstudents->TM_GRP_SD_Id = $plan->TM_PN_Standard_Id;
								$grpstudents->TM_GRP_STUId = $student->TM_STU_User_Id;
								$grpstudents->TM_GRP_STUName = $studentfirstname . ' ' . $studentlastname;
								$grpstudents->save(false);
							endif;
						}
						Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".urldecode($_GET['transaction'])));
						Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
						UserModule::sendRegistrationMail($checkprevmail->id,$studentuser->id,$parentpswd,$this->createAbsoluteUrl('/user/login'));
						$tempdetails->delete();
						$this->redirect(Yii::app()->controller->module->registrationUrl);
					endif;
				else:
					$parent = new User;
					$parentprofile=new Profile;
					$parent->username=$tempdetails->email;
					$parentpassword=$this->generateRandomString();
					$parent->password=UserModule::encrypting($parentpassword);
					$parent->email=$tempdetails->email;
					$parent->activkey=$tempdetails->activkey;
					$parent->createtime=$tempdetails->createtime;
					$parent->usertype='7';
					$parent->status='1';
					$parent->location=$tempdetails->location;
					$parent->save(false);
					$parentprofile->user_id=$parent->id;
					$parentprofile->lastname=$tempdetails->lastname;
					$parentprofile->firstname=$tempdetails->firstname;
					$parentprofile->phonenumber=$tempdetails->phonenumber;
					$parentprofile->save(false);
					$studentfirstname=ltrim($tempdetails->student_firstname);
					$studentlastname=ltrim($tempdetails->student_lastname);
					$studentusername=strtoupper(substr($studentfirstname, 0,1).substr($studentlastname, 0,1).rand(11111,99999));
					$studentpassword=$this->generateRandomString();
					$studentuser= new User;
					$studentuser->username=$studentusername;
					$studentuser->password=UserModule::encrypting($studentpassword);
					$studentuser->email=$studentusername;
					$studentuser->activkey=UserModule::encrypting(microtime().$studentpassword);
					$studentuser->createtime=time();
					$studentuser->usertype='6';
					$studentuser->school_id =$tempdetails->student_school;
					$studentuser->status='1';
					$studentuser->save(false);
					$studentuserprofile=new Profile();
					$studentuserprofile->user_id=$studentuser->id;
					$studentuserprofile->firstname=$tempdetails->student_firstname;
					$studentuserprofile->lastname=$tempdetails->student_lastname;
					$studentuserprofile->save(false);
					$studentmaster=new Student();
					$studentmaster->TM_STU_Parent_Id=$parent->id;
					$studentmaster->TM_STU_User_Id=$studentuser->id;
					$studentmaster->TM_STU_First_Name=$tempdetails->student_firstname;
					$studentmaster->TM_STU_Last_Name=$tempdetails->student_lastname;
					$studentmaster->TM_STU_Password=$studentpassword;
					$studentmaster->TM_STU_Parentpass=$parentpassword;
					$studentmaster->TM_STU_School=$tempdetails->student_school;
					$studentmaster->TM_STU_School_Id=$tempdetails->student_school;
					if($tempdetails->student_school=='0'):
						$studentmaster->TM_STU_SchoolCustomname=$tempdetails->schoolcustomname;
						$studentmaster->TM_STU_City=$tempdetails->student_city;
						$studentmaster->TM_STU_State=$tempdetails->student_state;
					else:
						$school=School::model()->findByPk($tempdetails->student_school);
						$studentmaster->TM_STU_City=$school->TM_SCL_City;
						$studentmaster->TM_STU_State=$school->TM_SCL_State;
					endif;
					$studentmaster->TM_STU_Country_Id=$tempdetails->student_country;
					$studentmaster->TM_STU_CreatedBy=$parent->id;
					$studentmaster->TM_STU_Status= '0';
					$studentmaster->save(false);
					$model=new StudentPlan();
					$model->TM_SPN_PlanId=$plan->TM_PN_Id;
					$model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
					$model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
					$model->TM_SPN_SubjectId='1';
					$model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
					$model->TM_SPN_StudentId=$studentuser->id;
					$model->TM_SPN_Status='0';
					$model->TM_SPN_PaymentReference=$_GET['transaction'];
					$model->TM_SPN_CouponId=base64_decode($_GET['voucher']);
					$model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));
					$model->TM_SPN_Provider='2';
					$currency=$this->getCurrency($tempdetails->location,$plan->TM_PN_Id);
					$model->TM_SPN_Currency=$currency;
					$model->save(false);

					Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".urldecode($_GET['transaction'])));
					Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration."));
					UserModule::sendRegistrationMail($parent->id,$studentuser->id,$parentpassword,$this->createAbsoluteUrl('/user/login'));
					$tempdetails->delete();
					$this->redirect(Yii::app()->controller->module->registrationUrl);
				endif;
			else:
				Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Invalid Details. Contact Admin to activate your account."));
				$this->redirect(Yii::app()->controller->module->registrationUrl);
			endif;
		}
		elseif($_GET['cancel']=='yes')
		{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t($_GET['message']));
			$this->redirect(Yii::app()->controller->module->registrationUrl);
		}
		else{
			Yii::app()->user->setFlash('subscriptionfail',UserModule::t("Pending Payment. Contact Admin to activate your account."));
			$this->redirect(Yii::app()->controller->module->registrationUrl);
		}
	}
	public function getCurrency($location,$plan)
    {
        $plan=Plan::model()->with('currency')->with('additional')->findByPk($plan);
        if($location=='0'& $plan->TM_PN_Currency_Id=='1'):
            return $plan->TM_PN_Currency_Id;
        elseif($location=='0' & $plan->TM_PN_Currency_Id!='1' & $plan->TM_PN_Additional_Currency_Id=='1'):
            return $plan->TM_PN_Additional_Currency_Id;
        elseif($location=='1'& $plan->TM_PN_Currency_Id!='1'):
            return $plan->TM_PN_Currency_Id;
        elseif($location=='1' & ($plan->TM_PN_Additional_Currency_Id!='1' || $plan->TM_PN_Additional_Currency_Id!='0')):
            return $plan->TM_PN_Additional_Currency_Id;            
        endif;
                
    }
    public function actionGetschools()
    {
        $country=$_POST['Student']['TM_STU_Country_Id'];        
        if(isset($_POST['country']))
        {
           $country=$_POST['country'];
        }
        $data=CHtml::listData(School::model()->findAll(array('condition' => 'TM_SCL_Country_Id='.$country,'order' => 'TM_SCL_Name')),'TM_SCL_Id','TM_SCL_Name');
        
        echo CHtml::tag('option',
                       array('value'=>''),'Select School',true);
        foreach($data as $value=>$name)
        {
            if($_POST['school']==$value):
                $selected=array('value'=>$value,'selected'=>'selected');              
            else:
                $selected=array('value'=>$value);
            endif;
            echo CHtml::tag('option',
                       $selected,CHtml::encode($name),true);
        }
        if($_POST['school']=='0'):
            $optionval=array('value'=>'0','selected'=>'selected');
        else:
            $optionval=array('value'=>'0');
        endif;        
        echo CHtml::tag('option',
           $optionval,'Other',true);        
    }	
}