<?php

class ActivationController extends Controller
{
	public $defaultAction = 'activation';

	
	/**
	 * Activation user account
	 */
	public function actionActivation () {
		$email = $_GET['email'];
		$activkey = $_GET['activkey'];
		if ($email&&$activkey) {
			$find = User::model()->notsafe()->findByAttributes(array('email'=>$email));
			if (isset($find)&&$find->status) {
                Yii::app()->user->setFlash('activation',UserModule::t("Your account is active."));
                $this->redirect(Yii::app()->controller->module->loginUrl);
			    //$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is active.")));
			} elseif(isset($find->activkey) && ($find->activkey==$activkey)) {
				$find->activkey = UserModule::encrypting(microtime());
				$find->status = 1;
				$find->save();
                /*$studnets=Student::model()->findAll(array('condition'=>'TM_STU_Parent_Id=:parent','params'=>array(':parent'=>$find->id)));
                foreach($studnets AS $studnet):
                        $studentuser=User::model()->findByPk($studnet->TM_STU_User_Id);
                        $studentuser->status=1;
                        $studentuser->save(false);
                endforeach;*/
                Yii::app()->user->setFlash('activation',UserModule::t("Your account is activated. Please login"));
                $this->redirect(Yii::app()->controller->module->loginUrl);
			    //$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is activated.")));
			} else {
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
			}
		} else {
			$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
		}
	}

}