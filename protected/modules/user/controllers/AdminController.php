<?php

class AdminController extends Controller
{
	public $defaultAction = 'admin';
    public $layout='//layouts/admincolumn2';
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return CMap::mergeArray(parent::filters(),array(
			'accessControl', // perform access control for CRUD operations
		));
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('adminhome'),
				'users'=>UserModule::getAdmins(),
			),  
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','adminlist','delete','create','update','view',),
                'users'=>array('@'),
                'expression'=>'UserModule::isSuperAdmin()'				
			),                       
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('studentlist','ImportStudents','parentlist','studentplan','Deleteplan','Addplan','GetStandardList','GetPlanList','addstudent','Viewstudent','Editstudent','Studentpassword','Activate','Deactivate','Parentaddstudent','Viewparent','Editparent','Parentpassword','Checkactiveplan','Checkactiveplanschool'),
                'users'=>array('@'),
                'expression'=>'UserModule::isAllowedStudent()'				
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('home'),
				'users'=>array('@'),
                'expression'=>'Yii::app()->user->isStudent()'
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Manages all models.
	 */

	public function actionAdmin()
	{

		$dataProvider=new CActiveDataProvider('User', array(			
			'pagination'=>array(
				'pageSize'=>Yii::app()->controller->module->user_page_size,
			),
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
    public function actionAddstudent()
    {
   	    $model = new User;
        $profile=new Profile;
        $student=new Student();                        

        if(isset($_POST['User'])) {
            $model->attributes=$_POST['User'];
            $model->usertype='7';
            $model->username=$model->email;
            $profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
            $student->attributes=((isset($_POST['Student'])?$_POST['Student']:array()));
            
            $soucePassword = $model->password;
            $passwordnoncrypted = $_POST['User']['password'];                        
            $model->activkey=UserModule::encrypting(microtime().$model->password);
            $model->password=UserModule::encrypting($model->password);						
            $model->createtime=time();
            $model->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
            $model->superuser=0;
            $model->status='1';
            $model->passtemp=$soucePassword;
            $studentfirstname=ltrim($student->TM_STU_First_Name);
            $studentlastname=ltrim($student->TM_STU_Last_Name);                            
            $username=strtoupper(substr($studentfirstname, 0,1).substr($studentlastname, 0,1).rand(11111,99999));
            // $password=$this->generateRandomString();
            $studentid = $_POST['Student']['TM_STU_student_Id'];
            $password= $_POST['Student']['TM_STU_Password'];
            $studentuser=new User();
            $studentuser->username=$studentid;
            $studentuser->password=md5($password);
            $studentuser->activkey=UserModule::encrypting(microtime().$studentuser->password);
            $studentuser->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
            $studentuser->superuser=0;
            $studentuser->usertype='6';
            $studentuser->createtime=time();
            $studentuser->email=$username;
            $studentuser->userlevel=$_POST['User']['userlevel'];
            $studentuser->status=((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);
            
            // vivek - 07-08-2018
            $checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$model->email . '"'));
            if($profile->validate() &  $student->validate())
            {

                if (count($checkprevmail) != 0):
                    if($studentid!=''){
                        $studentdetails=User::model()->find(array('condition'=>"username='".$studentid."'"));
                        if(count($studentdetails)>0) {
                            $parentStudent=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studentdetails->id));
                        }
                    }
                    else{
                        $parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "' . $student->TM_STU_First_Name . '" AND TM_STU_Last_Name LIKE "' . $student->TM_STU_Last_Name . ' " AND TM_STU_Parent_Id='.$checkprevmail->id));
                    }
                    if(count($parentStudent)>0){
                        $parentStudent->TM_STU_Country_Id = $_POST['Student']['TM_STU_Country_Id'];
                        $studentdetails->school_id = $_POST['Student']['TM_STU_School'];
                        $parentStudent->TM_STU_School = $_POST['Student']['TM_STU_School'];
                        $parentStudent->TM_STU_School_Id=$_POST['Student']['TM_STU_School'];
                        $parentStudent->save(false);
                        $studentdetails->save(false);
                        $studentUserId = $studentdetails->id;
                    }
                    else{

                        $studentuser->save(false);
                        $studentUserId = $studentuser->id;

                        $studentuserprofile=new Profile();
                        $studentuserprofile->user_id=$studentuser->id;
                        $studentuserprofile->lastname=$student->TM_STU_Last_Name;
                        $studentuserprofile->firstname=$student->TM_STU_First_Name;
                        $studentuserprofile->save(false);
                        $student->TM_STU_student_Id = $studentid;
                        $student->TM_STU_Parent_Id=$checkprevmail->id;
                        $student->TM_STU_Password=$password;
                        $student->TM_STU_User_Id=$studentuser->id;
                        $student->TM_STU_CreatedBy=Yii::app()->user->id;                            
                        if($student->TM_STU_School!='0'):                             
                            $school=School::model()->findByPk($student->TM_STU_School); 
                            $student->TM_STU_City=$school->TM_SCL_City;  
                            $student->TM_STU_State=$school->TM_SCL_State;                    
                        endif; 
                        
                        $student->save(false);                            
                        
                    }    

                else:

                    if ($model->save()) {
                        $profile->user_id=$model->id;
                        $profile->save();
                        $studentuser->save(false);
                        $studentUserId = $studentuser->id; 
                        $studentuserprofile=new Profile();
                        $studentuserprofile->user_id=$studentuser->id;
                        $studentuserprofile->lastname=$student->TM_STU_Last_Name;
                        $studentuserprofile->firstname=$student->TM_STU_First_Name;
                        $studentuserprofile->save(false);
                        $student->TM_STU_student_Id = $studentid;
                        $student->TM_STU_Parent_Id=$model->id;
                        $student->TM_STU_Password=$password;
                        $student->TM_STU_User_Id=$studentuser->id;
                        $student->TM_STU_CreatedBy=Yii::app()->user->id;                            
                        if($student->TM_STU_School!='0'):                             
                            $school=School::model()->findByPk($student->TM_STU_School); 
                            $student->TM_STU_City=$school->TM_SCL_City;  
                            $student->TM_STU_State=$school->TM_SCL_State;                    
                        endif; 
                        
                        $student->save(false);                            
                        //$this->redirect(array('/user/admin/studentlist'));
                    }
                endif;
                $this->redirect(array('/user/admin/addplan','id'=>$studentUserId,'mode'=>'registration'));															
            }
             else $profile->validate();
		} 
        $this->render('addstudent',array('model'=>$model,'profile'=>$profile,'student'=>$student));               
    }
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }    
	public function actionAdminlist()
{

        if(isset($_GET['search'])):
            $condition='usertype NOT IN(6,7,8,9,11)';
            if($_GET['username']!='' ||  $_GET['email']!=''):
                if($_GET['username']!=''):
                    $condition.=" AND username LIKE '%".$_GET['username']."%'";                    
                endif;
                if($_GET['email']!=''):
                    $condition.=" AND email LIKE '%".$_GET['email']."%'";                    
                endif;  
                                         
            endif; 
            $dataProvider=new CActiveDataProvider('User', array(
            'criteria'=>array('condition'=>$condition),           
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));

        else:        
        $dataProvider=new CActiveDataProvider('User', array(
            'criteria'=>array('condition'=>'usertype NOT IN(6,7,8,9,11)'),
            'pagination'=>array(
                 'pageSize'=>100,
            ),
        ));
endif;
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }
    public function actionStudentplan($id)
    {
        $dataProvider=new CActiveDataProvider('StudentPlan', array(
			'criteria'=>array('condition'=>'TM_SPN_StudentId='.$id),
			'pagination'=>array(
				'pageSize'=>Yii::app()->controller->module->user_page_size,
			),
		));         
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$id));             
		$this->renderPartial('studentplanlist',array(
			'dataProvider'=>$dataProvider,
            'student'=>$student
		));      
    }
    public function actionDeleteplan($id)
    {		
    	$plan = StudentPlan::model()->findByPk($id);
        $student=$plan->TM_SPN_StudentId;            
    	$plan->delete();
        $studentplans=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student.' AND TM_SPN_Status=0'));
        if($studentplans==0)
        {
            $user=User::model()->findByPk($student);
            $user->status='0';
            $user->save(false);
        }
    	$this->redirect(array('/user/admin/studentlist'));	      				  													
    }
    public function actionDeactivate($id)
    {
        $plan = StudentPlan::model()->findByPk($id);  
        $student=$plan->TM_SPN_StudentId; 
        $plan->TM_SPN_CancelDate=date('y-m-d');          
    	$plan->TM_SPN_Status='1';
        $plan->save(false);
        $studentplans=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student.' AND TM_SPN_Status=0'));
        if($studentplans==0)
        {
            $user=User::model()->findByPk($student);
            $user->status='0';
            $user->save(false);
        }
    	$this->redirect(array('/user/admin/studentlist'));
    }
    public function actionActivate($id)
    {
        $plan = StudentPlan::model()->findByPk($id); 
        $student=$plan->TM_SPN_StudentId;                   
    	$plan->TM_SPN_Status='0';
        $plan->save(false);
        $studentplans=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student.' AND TM_SPN_Status=0'));
        if($studentplans>0)
        {
            $user=User::model()->findByPk($student);
            $user->status='1';
            $user->save(false);
        }
        $this->redirect(array('/user/admin/studentlist'));              
    }    
	public function actionStudentlist()
	{
        $session=new CHttpSession;
        $session->open();             
        $session['urlstring']=str_replace(".html","",$_SERVER["QUERY_STRING"]);	   
	   if(isset($_GET['search'])):
            $condition='usertype =6';
            $join='';
            if($_GET['student']!='' || $_GET['username']!='' || $_GET['parent']!='' || $_GET['school']!='' || $_GET['parentid']!=''):
                $join.='LEFT JOIN tm_student AS st ON st.TM_STU_User_Id=id ';
                if($_GET['student']!=''):
                    $condition.=" AND (st.TM_STU_First_Name LIKE '%".$_GET['student']."%' OR st.TM_STU_Last_Name LIKE '%".$_GET['student']."%') ";
                endif;
                if($_GET['username']!=''):
                    $condition.=" AND username LIKE '%".$_GET['username']."%'  ";
                endif;
                if($_GET['school']!=''):
                    if($_GET['school']=='0'):
                        $condition.=" AND (st.TM_STU_SchoolCustomname LIKE '%".$_GET['schooltext']."%') ";
                    else:                                        
                        $condition.=" AND (st.TM_STU_School='".$_GET['school']."')";
                    endif;                                                    
                endif;
                if($_GET['parent']!=''):
                    $parents=Profile::model()->findAll(array('condition'=>"lastname LIKE '%".$_GET['parent']."%' OR firstname LIKE '%".$_GET['parent']."%'"));
                    if(count($parents)>0):
                        $parentsids='';
                        foreach($parents AS $key=>$parent):

                            if($key==0):
                               $parentsids.=$parent->user_id;
                            else:
                                $parentsids.=','.$parent->user_id;
                            endif;
                        endforeach;
                        $condition.=" AND (st.TM_STU_Parent_Id IN (".$parentsids.")) ";
                    else:
                        $condition.=" AND (st.TM_STU_Email='".$_GET['parent']."')";
                    endif;
                endif;
            endif; 
            if($_GET['school']!='' && $_GET['student']==''):            
                if($_GET['school']=='0'):
                    $condition.=" AND (st.TM_STU_SchoolCustomname LIKE '%".$_GET['schooltext']."%') ";
                else:                                               
                    $condition.=" AND (st.TM_STU_School='".$_GET['school']."')";
                endif;              
            endif;             
            if($_GET['plan']!='0'):
            $condition.=" AND (pl.TM_SPN_PlanId='".$_GET['plan']."') ";
            $join.='LEFT JOIN tm_student_plan AS pl ON pl.TM_SPN_StudentId=id ';
            endif;
           //echo $condition;exit;
       		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array('condition'=>$condition,'join'=>$join),			
			'pagination'=>array(
				'pageSize'=>1000,
			),
		  ));
       elseif($_GET['parentid']!=''):
            $join.='LEFT JOIN tm_student AS st ON st.TM_STU_User_Id=id ';
            $condition.=" st.TM_STU_Parent_Id=".$_GET['parentid'].""; 
       		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array('condition'=>$condition,'join'=>$join),			
			'pagination'=>array(
				'pageSize'=>1000,
			),
		  ));                 
       elseif(isset($_GET['search'])):
       		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array('condition'=>'usertype =6'),			
			'pagination'=>array(
				'pageSize'=>1000,
			),
		));
       else:
		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array('condition'=>'usertype =20'),			
			'pagination'=>array(
				'pageSize'=>1,
			),
		));
        endif;
		$this->render('studentlist',array(
			'dataProvider'=>$dataProvider,
		));
	}
    public function actionParentlist()
	{        
		if(isset($_GET['search'])):
            $condition='usertype =7';
            $join='';
            if($_GET['username']!='' || $_GET['name']!='' || $_GET['email']!=''):
                if($_GET['username']!=''):
                    $condition.=" AND username LIKE '%".$_GET['username']."%'";                    
                endif;
                if($_GET['email']!=''):
                    $condition.=" AND email LIKE '%".$_GET['email']."%'";                    
                endif;  
                if($_GET['name']!=''):
                    $join.='LEFT JOIN tm_profiles AS st ON st.user_id=id ';
                    $condition.=" AND (lastname LIKE '%".$_GET['name']."%' OR firstname LIKE '%".$_GET['name']."%')";                    
                endif;                               
            endif;
            $dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array('condition'=>$condition,'join'=>$join),			
			'pagination'=>array(
				'pageSize'=>1000,
			),
		));
        else:        
            $dataProvider=new CActiveDataProvider('User', array(
    			'criteria'=>array('condition'=>'usertype IN(7)'),			
    			'pagination'=>array(
    				'pageSize'=>1000,
    			),
    		));
        endif;
		$this->render('parentlist',array(
			'dataProvider'=>$dataProvider,
		));
	}
    public function actionParentaddstudent($id)
    {
        $student=new Student();                        
        if(isset($_POST['Student'])) {
            $student->attributes=((isset($_POST['Student'])?$_POST['Student']:array()));
            if($student->validate()):
                $studentfirstname=str_replace(' ', '', $student->TM_STU_First_Name);
                $studentlastname=str_replace(' ', '', $student->TM_STU_Last_Name);                
    			$username=strtoupper(substr($studentfirstname, 0,1).substr($studentlastname, 0,1).rand(11111,99999));
                $password=$this->generateRandomString();
                $studentuser=new User();
                $studentuser->username=$username;
                $studentuser->password=md5($password);
                $studentuser->activkey=UserModule::encrypting(microtime().$studentuser->password);
                $studentuser->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
                $studentuser->superuser=0;
                $studentuser->usertype='6';
                $studentuser->createtime=time();
                $studentuser->email=$username;
                $studentuser->status=((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);
                $studentuser->save(false);
                $studentuserprofile=new Profile();
                $studentuserprofile->user_id=$studentuser->id;
                $studentuserprofile->lastname=$student->TM_STU_Last_Name;
                $studentuserprofile->firstname=$student->TM_STU_First_Name;
                $studentuserprofile->save(false);
                $student->TM_STU_Parent_Id=$id;
                $student->TM_STU_Password=$password;
                $student->TM_STU_User_Id=$studentuser->id;
                $student->TM_STU_CreatedBy=Yii::app()->user->id;
    			if ($student->save(false)) {    			     
                     $this->redirect(array('/user/admin/addplan','id'=>$studentuser->id,'mode'=>'newstudent'));
                    ///$this->redirect(array('/user/admin/parentlist'));															
    			}
            endif;					
		} 
        $this->render('addstudentparent',array('student'=>$student));                 
    }
    public function SendStudentMail($id)
    {
        
        
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$id));        
        $user=User::model()->findByPk($student->TM_STU_Parent_Id);        
        $studentuser=User::model()->findByPk($student->TM_STU_User_Id);
        $adminEmail = Yii::app()->params['noreplayEmail'];
        	$activation_url = $this->createAbsoluteUrl('/user/login/');
        $message="<p>Dear ".$user->profile->firstname."</p>
                    <p>You have added a student</p>
                    <p><b>STUDENT ACCESS</b></p>
                    <p>".$student->TM_STU_First_Name." can now use these details below to login.</p>
                    <p>S/he can change his password or profile picture anytime by logging in and going into personal setup.<br>
                        For any help in getting started, please visit the Help Section at <a href='http://www.tabbiemath.com/demo-faq/'>http://www.tabbiemath.com/demo-faq/<a/></p>
                    <div style='border:solid 1px black;background-color: #E6E6FA;padding: 10px;text-align: center;'>
                        <p style='text-align:center;'>Sign In at <a href='".$activation_url."'> www.tabbiemath.com</a> </p>
                        <p><b>Student Name</b> : ".$student->TM_STU_First_Name." ".$student->TM_STU_Last_Name."</p>
                        <p><b>Username </b>: ".$studentuser->username."</p>
                        <p><b>Password </b>: ".$student->TM_STU_Password."</p>
                    </div>                    
                    <p>Regards,<br>The TabbieMath Team</p>
                  ";                 
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($user->email);
        $mail->AddReplyTo("noreply@tabbiemath.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Registration ';
        $mail->Body = $message;

        return $mail->Send();
    }
    public function actionAddplan($id)
    {
        $studentplan=new StudentPlan();
        $student=User::model()->findByPk($id);
        $studentprofile=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$id));
        $school=School::model()->findByPk($studentprofile->TM_STU_School);
        if(isset($_POST['StudentPlan'])):  
                     
        	$studentplan->attributes=$_POST['StudentPlan'];   
            $plan=Plan::model()->findByPk($studentplan->TM_SPN_PlanId);  
            $duration=$plan->TM_PN_Duration+1;
            if($_POST['StudentPlan']['TM_SPN_CouponId']!=''):                 
                $voucher=Coupons::model()->find(array('condition'=>"TM_CP_Status=0 AND TM_CP_Code='".$_POST['StudentPlan']['TM_SPN_CouponId']."'"));            
                $studentplan->TM_SPN_CouponId=$voucher->TM_CP_Id;
            else:
                if($school->TM_SCL_Discount):
                    $studentplan->TM_SPN_CouponId='-1';                    
                endif;
            endif;    
            $studentplan->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));            
			$studentplan->TM_SPN_Provider='3';
            ///$studentplan->TM_SPN_CreatedOn=date('Y-m-d h:s')         
			if($studentplan  ->save()):            
                
                $student->status='1';
                $student->save(false);
                $plancount=StudentPlan::model()->count(array('condition'=>'TM_SPN_StudentId='.$student->id));
                if($plancount=='1'):
                    if($_GET['mode']=='registration'):
                        $studnetprofile=Student::model()->find(array('condition'=>'TM_STU_User_Id ='.$student->id));
                        $parent=User::model()->findByPk($studnetprofile->TM_STU_Parent_Id);
                        // $activation_url = $this->createAbsoluteUrl('/user/activation/activation',array("activkey" => $parent->activkey, "email" => $parent->email));                            
    				    UserModule::sendRegistrationMail($parent->id,$student->id,$parent->passtemp,$this->createAbsoluteUrl('/user/login'));
                    else:                    
                        $this->SendStudentMail($student->id);                    
                    endif;
                endif;
            	$this->redirect(array('/user/admin/studentlist'));	 
            endif;
        endif;
        $this->render('addplan',array(
			'model'=>$studentplan,
            'id'=>$id,
            'school'=>$studentprofile->TM_STU_School
		));
    }
    public function actionCheckactiveplan()
    {
        if(isset($_POST['plan'])):
            $count=StudentPlan::model()->count(array('condition'=>"TM_SPN_StudentId='".$_POST['student']."' AND TM_SPN_PlanId='".$_POST['plan']."' AND TM_SPN_Status='0'"));
            echo $count;
        endif;
    }   
    public function actionCheckactiveplanschool()
    {
        if(isset($_POST['plan'])):
            $count=SchoolPlan::model()->count(array('condition'=>"TM_SPN_SchoolId='".$_POST['school']."' AND TM_SPN_PlanId='".$_POST['plan']."' AND TM_SPN_Status='0'"));
            echo $count;
        endif;
    }       
	public function actionAdminhome()
	{
        $this->layout = '//layouts/admincolumn1';
		$this->render('adminhome');
	}
    public function actionGetPlanList()
    {
        if(isset($_POST['publisher']))
        {
            $data=Plan::model()->findAll(
                array(
                        'condition'=>"TM_PN_Publisher_Id=:publisher AND TM_PN_Syllabus_Id=:syllabus AND TM_PN_Standard_Id=:standard AND TM_PN_Status='0' AND TM_PN_Type=0",
                        'params'=>array(':publisher'=>$_POST['publisher'],':syllabus'=>$_POST['syllabus'],':standard'=>$_POST['standard'])
                )
            );

            echo CHtml::tag('option',array('value'=>''),'Select',true);
            foreach($data as $key=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$name->TM_PN_Id),CHtml::encode($name->TM_PN_Name." (".$name->TM_PN_Rate.")"),true);
            }
        }
    }


	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
        $this->layout='//layouts/admincolumn2';
		$model = $this->loadModel();
		$this->render('adminview',array(
			'model'=>$model,
		));
	}
	public function actionViewstudent()
	{
        $this->layout='//layouts/admincolumn2';
		$model = $this->loadModel();        
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$model->id));        
        $parent=User::model()->findByPk($student->TM_STU_Parent_Id);
        $plans=StudentPlan::model()->findAll(array('condition'=>'TM_SPN_StudentId='.$model->id));        
		$this->render('studentview',array(
			'model'=>$model,
            'student'=>$student,
            'parent'=>$parent,
            'plans'=>$plans
		));
	} 
    public function actionEditstudent()
    {
        $user = $this->loadModel();
        $model=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$user->id));
        if(isset($_POST['Student'])):
            $model->attributes=$_POST['Student'];
            $model->TM_STU_student_Id = $_POST['User']['username'];
            //echo $model->TM_STU_student_Id; exit;
            //print_r($model->attributes); exit;
            $user->status=$_POST['User']['status'];
            $user->userlevel=$_POST['User']['userlevel'];
            $user->username = $_POST['User']['username'];
            $oldemail=$user->email;
            $user->email='noemail@noemail.noemail';
            $user->scenario='editstudent';
            if($model->validate()& $user->validate()):
                if($model->TM_STU_School!='0'):
                    $school=School::model()->findByPk($model->TM_STU_School);
                    $model->TM_STU_City=$school->TM_SCL_City;
                    $model->TM_STU_State=$school->TM_SCL_State;
                endif;
                $model->TM_STU_UpdatedBy = Yii::app()->user->id;
                $model->save(false);
                $user->email=$oldemail;
                $user->save(false);
                $this->redirect(array('Viewstudent','id'=>$user->id));
            endif;
        endif;
        $this->render('studentedit',array(
			'user'=>$user,
            'model'=>$model
		));
    }    

    public function actionStudentpassword()
    {
		$model = new UserChangePassword;
        $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$_GET['id']));			
		if(isset($_POST['UserChangePassword'])) {
				$model->attributes=$_POST['UserChangePassword'];
				if($model->validate()) {
					$new_password = User::model()->notsafe()->findbyPk($_GET['id']);
					$new_password->password = UserModule::encrypting($model->password);
					$new_password->activkey=UserModule::encrypting(microtime().$model->password);
					$new_password->save(false);
                    $student->TM_STU_Password=$model->password;
                    $student->save(false);
					Yii::app()->user->setFlash('passwordchange',UserModule::t("New password is saved."));
					$this->refresh();
				}
		}
		$this->render('studentpassword',array('model'=>$model,'student'=>$student));
    }   
	public function actionViewparent()
	{
        $this->layout='//layouts/admincolumn2';
		$model = $this->loadModel();                              
		$this->render('parentview',array(
			'model'=>$model,
            
		));
	}
    public function actionEditparent()
    {
        $model = $this->loadModel(); 
        $profile=$model->profile;                
        if(isset($_POST['User'])):
            $model->attributes=$_POST['User'];
            $profile->attributes=$_POST['Profile'];
			if($model->validate() && $profile->validate()) {
    			 $model->save();
                 $profile->save();
                 $this->redirect(array('Viewparent','id'=>$model->id));
			}
			
        endif;
        $this->render('parentedit',array(			
            'model'=>$model,
            'profile'=>$profile
		));
    }             
    
    public function actionParentpassword()
    {
		$model = new UserChangePassword;  
        $parent = $this->loadModel();
        if(isset($_POST['UserChangePassword'])) {
            $model->attributes=$_POST['UserChangePassword'];
            if($model->validate()) {
                $new_password = User::model()->notsafe()->findbyPk($_GET['id']);
                $new_password->password = UserModule::encrypting($model->password);
                $new_password->activkey=UserModule::encrypting(microtime().$model->password);
                $new_password->save();
                $students=Student::model()->findAll(array('condition'=>'TM_STU_Parent_Id='.$_GET['id'].' '));
                foreach($students AS $student):
                    $studentmodel=Student::model()->findByPk($student->TM_STU_Id);
                    $studentmodel->TM_STU_Parentpass=$model->password;
                    $studentmodel->save(false);
                endforeach;
                Yii::app()->user->setFlash('passwordchange',UserModule::t("New password is saved."));
                $this->refresh();
            }
        }
		$this->render('parentpassword',array('model'=>$model,'parent'=>$parent));
    }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;
		$profile=new Profile;
		if(isset($_POST['User']))
		{
		
            $model->attributes=$_POST['User'];
			$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
			$model->createtime=time();
			$model->lastvisit=time();			
            $usertypesselected=$_POST['User']['usertype'];            	
			$profile->attributes=$_POST['Profile'];
			$profile->user_id=0;
			if($model->validate()&&$profile->validate()) {
                $model->usertype='1';
				$model->password=Yii::app()->controller->module->encrypting($model->password);
				if($model->save()) {
					$profile->user_id=$model->id;
					$profile->save();
                    for($i=0;$i<count($usertypesselected);$i++):
                        $types= new UserRoles();
                        $types->TM_UR_User_Id=$model->id;
                        $types->TM_UR_Role=$usertypesselected[$i];
                        $types->save(false);
                    endfor;
                    if(in_array("2", $usertypesselected))
    				{
    					$standrads=$_POST['standard'];    										
    					for($i=0;$i<count($standrads);$i++)
    					{												
    						$standard=new Adminstandards();
    						$standard->TM_SA_Admin_Id=$model->id;
    						$standard->TM_SA_Standard_Id=$standrads[$i];
    						$standard->save(false);
    					}						
    
    				}
                    if(in_array("4", $usertypesselected))
    				{
    					$schools=$_POST['schools'];    										
    					for($i=0;$i<count($schools);$i++)
    					{												
    						$school=new Adminschools();
    						$school->TM_SA_Admin_Id=$model->id;
    						$school->TM_SA_School_Id=$schools[$i];
    						$school->save(false);
    					}						
    
    				}
                    //code by amal
                    $password=$_POST['User']['password'];
                    $this->SendAdminMail($model->id,$password);
                    //ends                    
				}
				if($model->usertype=='6' || $model->usertype=='7'):
					$this->redirect(array('studentlist'));
				else:
					$this->redirect(array('adminlist'));
				endif;
			} else $profile->validate();
		}

		$this->render('create',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$profile=$model->profile;
		if(isset($_POST['User']))
		{
            $model->attributes=$_POST['User'];
            $profile->attributes=$_POST['Profile'];
            $usertypesselected=$_POST['User']['usertype'];            	
			if($model->validate()&&$profile->validate()) {
				$old_password = User::model()->notsafe()->findByPk($model->id);
				if ($old_password->password!=$model->password) {
					$model->password=Yii::app()->controller->module->encrypting($model->password);
					$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
				}
				$model->save();
				$profile->save();
                $roles=UserRoles::model()->deleteAll(array('condition'=>'TM_UR_User_Id='.$model->id));
                for($i=0;$i<count($usertypesselected);$i++):
                    $types= new UserRoles();
                    $types->TM_UR_User_Id=$model->id;
                    $types->TM_UR_Role=$usertypesselected[$i];
                    $types->save(false);
                endfor;               
				if(in_array("2", $usertypesselected))
				{
					$standrads=$_POST['standard'];
					Adminstandards::model()->deleteAll(array('condition'=>'TM_SA_Admin_Id='.$model->id));						
					for($i=0;$i<count($standrads);$i++)
					{												
						$standard=new Adminstandards();
						$standard->TM_SA_Admin_Id=$model->id;
						$standard->TM_SA_Standard_Id=$standrads[$i];
						$standard->save(false);
					}						

				}
                elseif(in_array("4", $usertypesselected))
				{
					$schools=$_POST['schools'];
                    Adminschools::model()->deleteAll(array('condition'=>'TM_SA_Admin_Id='.$model->id));    										
					for($i=0;$i<count($schools);$i++)
					{												
						$school=new Adminschools();
						$school->TM_SA_Admin_Id=$model->id;
						$school->TM_SA_School_Id=$schools[$i];
						$school->save(false);
					}						

				}
				else
				{
					Adminstandards::model()->deleteAll(array('condition'=>'TM_SA_Admin_Id='.$model->id));
				}
				$this->redirect(array('view','id'=>$model->id));
			} else $profile->validate();
		}

		$this->render('update',array(
			'model'=>$model,
			'profile'=>$profile,
			'standards'=>Adminstandards::model()->getStandards($model->id),
			'schools'=>Adminschools::model()->getSchools($model->id)            	
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel();
            if($model->usertype=='6'):
                $connection = CActiveRecord::getDbConnection();
                $sql="DELETE FROM tm_student_plan WHERE TM_SPN_StudentId='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $sql="DELETE FROM tm_student WHERE TM_STU_User_Id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $sql="DELETE FROM tm_profiles WHERE user_id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $sql="DELETE FROM tm_users WHERE id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $sql="DELETE FROM tm_buddies WHERE TM_BD_Student_Id='".$model->id."' OR TM_BD_Connection_Id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $sql="DELETE FROM tm_notifications WHERE TM_NT_User='".$model->id."' OR TM_NT_Target_Id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();                
                $sql="DELETE FROM tm_studentpoints WHERE TM_STP_Student_Id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();                                
                $sql="DELETE FROM tm_studentlevels WHERE TM_STL_Student_Id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();   
                $sql="DELETE FROM tm_chalangerecepient WHERE TM_CE_RE_Recepient_Id='".$model->id."'";
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();              
            elseif($model->usertype=='7'):            
                $profile = Profile::model()->findByPk($model->id);
                if(count($profile)>0):
                    $profile->delete();
                endif;
    			$model->delete();
            else:
    			$profile = Profile::model()->findByPk($model->id);
    			$profile->delete();
    			$model->delete();
            endif;
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(array('/user/admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}	
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=User::model()->notsafe()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	public function actionGetStandardList()
    {
        if(isset($_POST['id']))
        {
            $data=Standard::model()->findAll("TM_SD_Syllabus_Id=:syllabus AND TM_SD_Status='0'",
                array(':syllabus'=>(int) $_POST['id']));

            $data=CHtml::listData($data,'TM_SD_Id','TM_SD_Name');
            /*if(count($data)!='1'):
                echo CHtml::tag('option',array('value'=>''),'Select Chapter',true);
            endif;*/
            echo CHtml::tag('option',array('value'=>''),'Select',true);
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option',
                    array('value'=>$value),CHtml::encode($name),true);
            }
        }
    }
public function gridCreateRoles($data,$row)
    {

     $user = $data->id;
     //do your stuff for finding the username or name with $user
     //for eg.
     $detail = UserRoles::model()->findAll('TM_UR_User_Id='.$user);
        $number=count($detail);
     // make sure what ever model you are calling is accessible from this controller other wise you have to import the model on top of the controller above class of the controller.
        $roles='';
        $totals=User::itemAlias('UserTypes');
        if($number!=1):
            foreach($detail AS $key=>$detail):
                if($key+1<$number):
                    $roles.=$totals[$detail->TM_UR_Role].',    ';
                else:
                    $roles.=$totals[$detail->TM_UR_Role];
                endif;
            endforeach;
        else:
            foreach($detail AS $key=>$detail):
                $roles.=$totals[$detail->TM_UR_Role];
            endforeach;
            endif;

     return   $roles;
    }
    /**
     * Send admin email function by amal
     */
 //code by amal
    public function SendAdminMail($userid,$password)
    {
        $user=User::model()->findByPk($userid);
        $profile=Profile::model()->findByPk($userid);
        $email=$user->email;
        $adminEmail = Yii::app()->params['noreplayEmail'];
        $userroles=UserRoles::model()->findAll('TM_UR_User_Id='.$userid);
        $count=count($userroles);
        $roles="";
        if($count!=1):
            foreach($userroles AS $key=>$userrole):
                if($key+1<$count):
                    $roles.=User::itemAlias("UserTypes",$userrole->TM_UR_Role).",";
                else:
                    $roles.=User::itemAlias("UserTypes",$userrole->TM_UR_Role);
                endif;
            endforeach;
        else:
            foreach($userroles AS $key=>$userrole):
                $roles.=User::itemAlias("UserTypes",$userrole->TM_UR_Role);
            endforeach;
        endif;
        $message="<p>Hi ".$profile['firstname'].",</p>
                    <p>You have been added as admin in TabbieMath. Your login cedentials are in as shown below.</p>
                    <p><div style='border:solid 1px black;background-color: #E6E6FA;width:450px;height: 150px;padding: 20px;text-align: center;'>
                        <p><b>Username </b>: ".$user->username."</p>
                        <p><b>Password </b>: ".$password."</p>
                        <p><b>User Type </b>: ".$roles."</p>
                    </div></p>
                    <p>Regards,<br>The TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
        $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
        $mail->SMTPAuth = true; // turn on SMTP authentication
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
        $mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($email);
        $mail->AddReplyTo("noreply@tabbieme.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath: Admin Access';
        $mail->Body = $message;

        return $mail->Send();
    }
    //ends     
     /**
      * Ends
      */
      public function actionImportStudents()
      {
          if(Yii::app()->user->isSchoolAdmin() || Yii::app()->user->isTeacherAdmin()):
              $this->layout = '//layouts/schoolcolumn2';
          endif;
          $importlog=array();
          if(isset($_POST['submit'])):
              $file = fopen($_FILES['importcsv']['tmp_name'], "r");
              $count=1;
              $values=fgetcsv($file, 1000, ",");
              while(($values=fgetcsv($file, 1000, ","))!==FALSE)
              {
                  $importlog[$count]['status']=$this->GetImportitems($values);
                  //echo $importlog[$count]['status'];exit;
                  if($importlog[$count]['status']=='')
                  {
                      $school=School::model()->find(array('condition'=>'TM_SCL_Name LIKE "'.$values[4].'" '));
                      $schoolid=$school['TM_SCL_Id'];
                      if($values[0]!=''):
                          $user=User::model()->find(array(
                                  'condition' => 'username=:name',
                                  'params' => array(':name' => $values[2])
                              )
                          );
  
                          if(User::model()->count('id=:value',array(':value'=>$user->id)))
                          {
                              $student=Student::model()->find(array(
                                      'condition' => 'TM_STU_User_Id=:id',
                                      'params' => array(':id' => $user->id)
                                  )
                              );
                              if(count($student) > 0)
                              {
                                  if($values[8]=='Send'):
                                      $mailstatus=1;
                                  else:
                                      $mailstatus=0;
                                  endif;
                                  if($values[7]=='Active'):
                                      $status=1;
                                  else:
                                      $status=0;
                                  endif;
                                  if($values[18]=='Pro'):
                                      $userlevel=1;
                                  else:
                                      $userlevel=0;
                                  endif;
                                  // var_dump($student);exit;
                                  $student->TM_STU_First_Name=$values[0];
                                  $student->TM_STU_Last_Name=$values[1];
                                  if($values[3]!=''): 
                                      $student->TM_STU_Password =$values[3];
                                  endif;
                                  $student->TM_STU_Email_Status=$mailstatus;
                                  if($values[14]!=''):
                                      $student->TM_STU_Parentpass=$values[14];
                                  endif;
  
                                  $student->save(false);
  
                                  $connection = CActiveRecord::getDbConnection();
                                  if($values[9] != '') {
                                      $sql="DELETE tm_student_plan FROM tm_student_plan INNER JOIN tm_plan ON tm_plan.TM_PN_Id = tm_student_plan.TM_SPN_PlanId WHERE tm_plan.TM_PN_Type = 1 AND tm_student_plan.TM_SPN_StudentId=".$user->id." ";
                                      $command=$connection->createCommand($sql);
                                      $dataReader=$command->query();
                                      $std=Standard::model()->find(array(
                                              'condition' => 'TM_SD_Name=:name',
                                              'params' => array(':name' => $values[9])
                                          )
                                      );
                                      if(count($std) > 0) {
                                          $newplan = Plan::model()->find(array(
                                                  'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                                  'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                                              )
                                          );
                                          if(count($newplan)>0):
                                              $sttudentnewplan=New StudentPlan();
                                              $sttudentnewplan->TM_SPN_StudentId = $user->id;
                                              $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                              $sttudentnewplan->TM_SPN_SyllabusId = $newplan->TM_PN_Syllabus_Id;
                                              $sttudentnewplan->TM_SPN_StandardId = $newplan->TM_PN_Standard_Id;
                                              $sttudentnewplan->TM_SPN_Status = 0;
                                              $sttudentnewplan->TM_SPN_Provider = 3;
                                              $sttudentnewplan->save(false);
                                          endif;
                                      }
                                  }
  
                                  $sql="SELECT b.TM_PN_Name FROM tm_student_plan AS a INNER JOIN tm_plan AS b ON a.TM_SPN_PlanId=b.TM_PN_Id WHERE a.TM_SPN_StudentId=".$user->id." AND a.TM_SPN_Status=0 AND b.TM_PN_Type=0";
                                  $command=$connection->createCommand($sql);
                                  $dataReader=$command->query();
                                  $revplans=$dataReader->readAll();
                                  $exrevplans='';
                                  foreach($revplans AS $key=>$revplan):
                                      if($key==0):
                                          $exrevplans.=$revplan['TM_PN_Name'];
                                      else:
                                          $exrevplans.=','.$revplan['TM_PN_Name'];
                                      endif;
                                  endforeach;
                                  //echo 'rev -'.count($revplans).'new -'.$values[16];exit;
                                  if(count($revplans)==0 && $values[16]!=''):
                                      //echo "asd";exit;
                                      $newplans = explode(',', $values[16]);
                                      foreach($newplans AS $newplan):
                                          $data=Plan::model()->find(array(
                                                  'condition' => 'TM_PN_Name=:name AND TM_PN_Type=0',
                                                  'params' => array(':name' => $newplan)
                                              )
                                          );
  
                                          $sql="SELECT a.TM_SPN_Id,b.TM_PN_Name FROM tm_student_plan AS a INNER JOIN tm_plan AS b ON a.TM_SPN_PlanId=b.TM_PN_Id WHERE a.TM_SPN_StudentId=".$user->id." AND a.TM_SPN_Status=1 AND a.TM_SPN_PlanId=".$data->TM_PN_Id." AND b.TM_PN_Type=0";
                                          $command=$connection->createCommand($sql);
                                          $dataReader=$command->query();
                                          $inactiveplans=$dataReader->readAll();
                                          //echo count($inactiveplans);exit;
                                          if(count($inactiveplans)>0):
                                              foreach($inactiveplans AS $inactiveplan):
                                                  $model=StudentPlan::model()->deleteAll(array('condition'=>'TM_SPN_Id='.$inactiveplan['TM_SPN_Id'].' '));
                                              endforeach;
                                              if(count($data)>0):
                                                  $newstudplan=New StudentPlan();
                                                  $newstudplan->TM_SPN_StudentId = $user->id;
                                                  $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                  $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                  $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                  $newstudplan->TM_SPN_Status = 0;
                                                  $newstudplan->TM_SPN_Provider = 3;
                                                  $newstudplan->save(false);
                                              endif;
                                          else:
                                              if(count($data)>0):
                                                  $newstudplan=New StudentPlan();
                                                  $newstudplan->TM_SPN_StudentId = $user->id;
                                                  $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                  $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                  $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                  $newstudplan->TM_SPN_Status = 0;
                                                  $newstudplan->TM_SPN_Provider = 3;
                                                  $newstudplan->save(false);
                                              endif;
                                          endif;
                                      endforeach;
                                  elseif(count($revplans)!=0 && $values[16]!=''):
                                      //echo "asd1";exit;
                                      $newrevplans = explode(',', $values[16]);
                                      $revplans = explode(',', $exrevplans);
                                      //print_r($revplans);echo "<br>";print_r($newrevplans);
                                      if(count($newrevplans)==count($revplans)):
                                          for($i=0;$i<count($newrevplans);$i++){
                                              for($i=0;$i<count($revplans);$i++) {
                                                  if ($revplans[$i] != $newrevplans[$i]):
                                                      //echo "yes";exit;
                                                      $plan = Plan::model()->find(array('condition' => 'TM_PN_Name LIKE "' . $revplans[$i] . '" '));
                                                      $studentplan = StudentPlan::model()->find(array('condition' => 'TM_SPN_PlanId=' . $plan['TM_PN_Id'] . ' AND TM_SPN_StudentId=' . $user->id . ' '));
                                                      $studentplan->TM_SPN_Status = 1;
                                                      $studentplan->save(false);
                                                      $data = Plan::model()->find(array(
                                                              'condition' => 'TM_PN_Name=:name AND TM_PN_Type=0',
                                                              'params' => array(':name' => $newrevplans[$i])
                                                          )
                                                      );
                                                      $sql="SELECT a.TM_SPN_Id,b.TM_PN_Name FROM tm_student_plan AS a INNER JOIN tm_plan AS b ON a.TM_SPN_PlanId=b.TM_PN_Id WHERE a.TM_SPN_StudentId=".$user->id." AND a.TM_SPN_Status=1 AND a.TM_SPN_PlanId=".$data->TM_PN_Id." AND b.TM_PN_Type=0";
                                                      $command=$connection->createCommand($sql);
                                                      $dataReader=$command->query();
                                                      $inactiveplans=$dataReader->readAll();
                                                      //echo count($inactiveplans);exit;
                                                      if(count($inactiveplans)>0):
                                                          foreach($inactiveplans AS $inactiveplan):
                                                              $model=StudentPlan::model()->deleteAll(array('condition'=>'TM_SPN_Id='.$inactiveplan['TM_SPN_Id'].' '));
                                                          endforeach;
                                                          if(count($data)>0):
                                                              $newstudplan=New StudentPlan();
                                                              $newstudplan->TM_SPN_StudentId = $user->id;
                                                              $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                              $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                              $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                              $newstudplan->TM_SPN_Status = 0;
                                                              $newstudplan->TM_SPN_Provider = 3;
                                                              $newstudplan->save(false);
                                                          endif;
                                                      else:
                                                          if(count($data)>0):
                                                              $newstudplan=New StudentPlan();
                                                              $newstudplan->TM_SPN_StudentId = $user->id;
                                                              $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                              $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                              $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                              $newstudplan->TM_SPN_Status = 0;
                                                              $newstudplan->TM_SPN_Provider = 3;
                                                              $newstudplan->save(false);
                                                          endif;
                                                      endif;
  
                                                  endif;
                                              }
  
                                          }
                                      endif;
                                      if(sizeof($newrevplans) > sizeof($revplans)){
                                          $result = array_diff($newrevplans,$revplans);
                                          //print_r($result);exit;
                                          foreach($result as $newplan){
                                              $data=Plan::model()->find(array('condition' => 'TM_PN_Name=:name AND TM_PN_Type=0',
                                                  'params' => array(':name' => $newplan)));
                                              $sql="SELECT a.TM_SPN_Id,b.TM_PN_Name FROM tm_student_plan AS a INNER JOIN tm_plan AS b ON a.TM_SPN_PlanId=b.TM_PN_Id WHERE a.TM_SPN_StudentId=".$user->id." AND a.TM_SPN_Status=1 AND a.TM_SPN_PlanId=".$data->TM_PN_Id." AND b.TM_PN_Type=0";
                                              $command=$connection->createCommand($sql);
                                              $dataReader=$command->query();
                                              $inactiveplans=$dataReader->readAll();
                                              //echo count($inactiveplans);exit;
                                              if(count($inactiveplans)>0):
                                                  foreach($inactiveplans AS $inactiveplan):
                                                      $model=StudentPlan::model()->deleteAll(array('condition'=>'TM_SPN_Id='.$inactiveplan['TM_SPN_Id'].' '));
                                                  endforeach;
                                                  if(count($data)>0):
                                                      $newstudplan=New StudentPlan();
                                                      $newstudplan->TM_SPN_StudentId = $user->id;
                                                      $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                      $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                      $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                      $newstudplan->TM_SPN_Status = 0;
                                                      $newstudplan->TM_SPN_Provider = 3;
                                                      $newstudplan->save(false);
                                                  endif;
                                              else:
                                                  if(count($data)>0):
                                                      $newstudplan=New StudentPlan();
                                                      $newstudplan->TM_SPN_StudentId = $user->id;
                                                      $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                      $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                      $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                      $newstudplan->TM_SPN_Status = 0;
                                                      $newstudplan->TM_SPN_Provider = 3;
                                                      $newstudplan->save(false);
                                                  endif;
                                              endif;
                                          }
                                      }
                                      else
                                      {
                                          $result = array_diff($revplans,$newrevplans);
                                          foreach($result as $inactiveplan){
                                              $plan = Plan::model()->find(array('condition' => 'TM_PN_Name LIKE "' . $inactiveplan . '" '));
                                              $studentplan = StudentPlan::model()->find(array('condition' => 'TM_SPN_PlanId=' . $plan['TM_PN_Id'] . ' AND TM_SPN_StudentId=' . $user->id . ' '));
                                              $studentplan->TM_SPN_Status = 1;
                                              $studentplan->save(false);
                                          }
                                      }
                                      //print_r($result);exit;
                                      $matchvalue = array_intersect($newrevplans, $revplans);
  
                                  elseif(count($revplans)!=0 && $values[16]==''):
                                      //echo "asd2";exit;
                                      $revplans = explode(',', $exrevplans);
                                      //print_r($revplans);exit;
                                      for($i=0;$i<count($revplans);$i++) {
                                          $plan = Plan::model()->find(array('condition' => 'TM_PN_Name LIKE "' . $revplans[$i] . '" '));
                                          $studentplan = StudentPlan::model()->find(array('condition' => 'TM_SPN_PlanId=' . $plan['TM_PN_Id'] . ' AND TM_SPN_StudentId=' . $user->id . ' '));
                                          $studentplan->TM_SPN_Status = 1;
                                          $studentplan->save(false);
                                      }
                                  endif;
  
                                  $studentuser=User::model()->findByPk($user->id);
                                  if($values[3]!=''):
                                      $studentuser->password=md5($values[3]);
                                  endif;
                                  $studentuser->status=$status;
                                  $studentuser->userlevel=$userlevel;
                                  $studentuser->save(false);
                                  $studentuserprofile =Profile::model()->findByPk($user->id);
                                  $studentuserprofile->firstname = $values[0];
                                  $studentuserprofile->lastname = $values[1];
                                  $studentuserprofile->save(false);
                                  $parentuser=User::model()->findByPk($student->TM_STU_Parent_Id);
                                  if($values[14]!=''):
                                      $parentuser->password=md5($values[14]);
                                  endif;
                                  $importlog[$count]['status']=$this->GetImportparent($values,$parentuser->id);
                                  if($importlog[$count]['status']==''):
                                      $importlog[$count]['student']='1';
                                      $parentuser->username=$values[13];
                                      $parentuser->email=$values[13];
                                      $parentuser->save(false);
                                      // $student->TM_STU_Email=$values[13];
                                      // $student->save(false);
                                      $allStudents = Student::model()->findAll(array('condition'=>'TM_STU_Parent_Id='.$parentuser->id.' '));
                                      foreach($allStudents AS $eachstudents) {
                                          $eachstudents->TM_STU_Email=$values[13];
                                          $eachstudents->save(false);
                                      }
                                  else:
                                      $importlog[$count]['student']='0';
                                  endif;
                                  // $parentuser->save(false);
                                  $parentprofile=Profile::model()->findByPk($student->TM_STU_Parent_Id);
                                  $parentprofile->firstname = $values[10];
                                  $parentprofile->lastname = $values[11];
                                  $parentprofile->phonenumber = $values[12];
                                  $parentprofile->save(false);
                                  $importlog[$count]['student']='1';
                              }
                              else
                              {
                                  $importlog[$count]['student']='2'; //username exists
                              }
                          }
                          else
                          {
                              $student = new Student();
                              $school = School::model()->findByPk($schoolid);
                              $studentuser = new User();
                              $parentmodel = new User;
                              $parentprofile=new Profile;
                              $optionalusername=$values[15];
                              $student->TM_STU_School_Id = $schoolid;
                              $student->TM_STU_City = $school->TM_SCL_City;
                              $student->TM_STU_State = $school->TM_SCL_State;
                              $student->TM_STU_Country_Id = $school->TM_SCL_Country_Id;
                              $student->TM_STU_School = $schoolid;
                              $student->TM_STU_Email=$values[13];
                              $mailstatus=0;
                              $student->TM_STU_Email_Status=$mailstatus;
                              if($values[3]!='')
                              {
                                  $student->TM_STU_Password =$values[3];
                                  $studentuser->password = md5($values[3]);
                              }
                              else
                              {
                                  $password = $this->generateRandomString();
                                  $student->TM_STU_Password =$password;
                                  $studentuser->password = md5($password);
                              }
                              //$username = strtoupper(substr($values[0], 0, 1) . substr($values[1], 0, 1) . rand(11111, 99999));
                              $username = $values[2];
                              if($values[14]!=''):
                                  $passwordparent=$values[14];
                              else:
                                  $passwordparent = $this->generateRandomString();
                              endif;
                              $student->TM_STU_Parentpass=$passwordparent;
                              $parentmodel->username=$values[13];
                              $parentmodel->password=md5($passwordparent);
                              $parentmodel->email=$values[13];							
                              $studentuser->username = $values[2];                          
                              $studentuser->email = $values[2];
                              //print_r($studentuser->username);exit;
                              $studentuser->school_id = $schoolid;
                              $studentuser->activkey = UserModule::encrypting(microtime() . $studentuser->password);
                              $studentuser->lastvisit = ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) ? time() : 0;
                              $studentuser->superuser = 0;
                              $studentuser->usertype = '6';
                              $studentuser->createtime = time();
                              if($values[7]=='Active'):
                                  $status=1;
                              else:
                                  $status=0;
                              endif;
                              if($values[18]=='Pro'):
                                  $userlevel=1;
                              else:
                                  $userlevel=0;
                              endif;
                              $studentuser->status = $status;
                              $studentuser->userlevel=$userlevel;
                              $checkprevmail = User::model()->find(array('condition' => 'usertype=7 AND email ="' .$values[13]. '"'));
  
                              if (count($checkprevmail) != 0):
                                  if($optionalusername!=''):
                                      $studentdetails=User::model()->find(array('condition'=>"username='".$optionalusername."'"));
                                      $parentStudent=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studentdetails->id));
                                  else:
                                      $parentStudent = Student::model()->find(array('condition' => 'TM_STU_First_Name LIKE "' . $values[0] . '" AND TM_STU_Last_Name LIKE "' . $values[1] . '" AND TM_STU_Parent_Id='.$checkprevmail->id));
                                  endif;
                                  if(count($parentStudent)>0):
                                      $connection = CActiveRecord::getDbConnection();
                                      if ($values[9] != '') {
                                          $sql="DELETE tm_student_plan FROM tm_student_plan INNER JOIN tm_plan ON tm_plan.TM_PN_Id = tm_student_plan.TM_SPN_PlanId WHERE tm_plan.TM_PN_Type = 1 AND tm_student_plan.TM_SPN_StudentId=".$parentStudent->TM_STU_User_Id." ";
                                          $command=$connection->createCommand($sql);
                                          $dataReader=$command->query();
                                          $std=Standard::model()->find(array(
                                                  'condition' => 'TM_SD_Name=:name',
                                                  'params' => array(':name' => $values[9])
                                              )
                                          );
                                          if(count($std) > 0) {
                                              $newplan = Plan::model()->find(array(
                                                      'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                                      'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                                                  )
                                              );
                                              $sttudentnewplan = new StudentPlan();
                                              $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                              $currentplan = Plan::model()->findByPk($newplan->TM_PN_Id);
                                              $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                              $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                              $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                              $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                              $sttudentnewplan->TM_SPN_Provider = 3;
                                              $sttudentnewplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
                                              $sttudentnewplan->save(false);
                                          }
                                      }
                                      $newplans = explode(',', $values[16]);
                                      foreach($newplans AS $newplan):
                                          $data=Plan::model()->find(array(
                                                  'condition' => 'TM_PN_Name=:name AND TM_PN_Type=0 AND TM_PN_Status=0',
                                                  'params' => array(':name' => $newplan)
                                              )
                                          );
  
                                          if(count($data)>0):
                                              $planExist = StudentPlan::model()->find(array(
                                                  'condition' => 'TM_SPN_StudentId=:student AND TM_SPN_PlanId=:plan',
                                                  'params' => array(':student' => $parentStudent->TM_STU_User_Id, ':plan' => $data->TM_PN_Id)
                                              ));
                                              if(!count($planExist) > 0) 
                                              {
                                                  $newstudplan=New StudentPlan();
                                                  $newstudplan->TM_SPN_StudentId = $parentStudent->TM_STU_User_Id;
                                                  $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                  $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                  $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                  $newstudplan->TM_SPN_Status = 0;
                                                  $newstudplan->TM_SPN_Provider = 3;
                                                  $newstudplan->save(false);
                                              }
                                          endif;
                                      endforeach;
                                      //code by amal
                                      $parentStudent->TM_STU_City = $school->TM_SCL_City;
                                      $parentStudent->TM_STU_State = $school->TM_SCL_State;
                                      $parentStudent->TM_STU_Country_Id = $school->TM_SCL_Country_Id;
                                      $parentStudent->TM_STU_School = $school->TM_SCL_Id;
                                      $parentStudent->TM_STU_student_Id = $username;
                                      $studentusermodel=User::model()->findByPk($parentStudent->TM_STU_User_Id);
                                      $studentusermodel->username=$username;
                                      $importlog[$count]['status']=$this->GetImportuseritems($values);
                                      if($importlog[$count]['status']==''):
                                          $importlog[$count]['student']='1';
                                          $parentStudent->save(false);
                                          $studentusermodel->save(false);
                                      else:
                                          $importlog[$count]['student']='0';
                                      endif;
                                  //ends
                                  else:
                                      $parent=Student::model()->find(array('condition'=>'TM_STU_Parent_Id='.$checkprevmail->id.''));
                                      $parentpswd=$parent->TM_STU_Parentpass;
                                      $student->TM_STU_Parentpass=$parentpswd;
                                      $importlog[$count]['status']=$this->GetImportuseritems($values);
                                      if($importlog[$count]['status']==''):
                                          $importlog[$count]['student']='1';
                                          $studentuser->save(false);
                                          $studentuserprofile = new Profile();
                                          $studentuserprofile->user_id = $studentuser->id;
                                          $studentuserprofile->lastname = $values[1];
                                          $studentuserprofile->firstname = $values[0];
                                          $studentuserprofile->save(false);
                                          if($values[9] != '') {
                                              $std=Standard::model()->find(array(
                                                      'condition' => 'TM_SD_Name=:name',
                                                      'params' => array(':name' => $values[9])
                                                  )
                                              );
                                              if(count($std) > 0) {
                                                  $newplan = Plan::model()->find(array(
                                                          'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                                          'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                                                      )
                                                  );
                                                  $sttudentnewplan = new StudentPlan();
                                                  $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                                  $currentplan = Plan::model()->findByPk($newplan->TM_PN_Id);
                                                  $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                                  $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                                  $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                                  $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                                  $sttudentnewplan->TM_SPN_Provider = 3;
                                                  $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                                                  $sttudentnewplan->save(false);
                                              }
                                          }
                                          $newplans = explode(',', $values[16]);
                                          foreach($newplans AS $newplan):
                                              $data=Plan::model()->find(array(
                                                      'condition' => 'TM_PN_Name=:name AND TM_PN_Type=0',
                                                      'params' => array(':name' => $newplan)
                                                  )
                                              );
  
                                              if(count($data)>0):
                                                  $newstudplan=New StudentPlan();
                                                  $newstudplan->TM_SPN_StudentId = $studentuser->id;
                                                  $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                  $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                  $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                  $newstudplan->TM_SPN_Status = 0;
                                                  $newstudplan->TM_SPN_Provider = 3;
                                                  $newstudplan->save(false);
                                              endif;
                                          endforeach;
                                          $student->TM_STU_First_Name=$values[0];
                                          $student->TM_STU_Last_Name=$values[1];
                                          $student->TM_STU_student_Id = $values[2];
                                          $student->TM_STU_Parent_Id=$checkprevmail->id;
                                          $student->TM_STU_User_Id = $studentuser->id;
                                          $student->TM_STU_CreatedBy = Yii::app()->user->id;
                                          $student->TM_STU_student_Id = $values[2];
                                          $student->save(false);
                                          //$this->SendStudentMail($studentuser->id);
                                          $parentprofile=Profile::model()->findByPk($checkprevmail->id);
                                          $parentprofile->firstname = $values[10];
                                          $parentprofile->lastname = $values[11];
                                          $parentprofile->phonenumber = $values[12];
                                          $parentprofile->save(false);
                                          //code by amal
                                          $schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$schoolid."' AND TM_SCL_SD_Id='".$currentplan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
                                          if(count($schoolgrps)>0):
                                              $grpstudents=New GroupStudents();
                                              $grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
                                              $grpstudents->TM_GRP_PN_Id=$newplan->TM_PN_Id;
                                              $grpstudents->TM_GRP_SCL_Id=$schoolid;
                                              $grpstudents->TM_GRP_SD_Id=$currentplan->TM_PN_Standard_Id;
                                              $grpstudents->TM_GRP_STUId=$student->TM_STU_User_Id;
                                              $grpstudents->TM_GRP_STUName=$values[0].' '.$values[1];
                                              $grpstudents->save(false);
                                              $groupid=$schoolgrps->TM_SCL_GRP_Id;
                                              $homeworks=Homeworkgroup::model()->findAll(array('condition'=>'TM_HMG_Groupe_Id='.$groupid.' '));
                                              if(count($homeworks)>0):
                                                  foreach($homeworks AS $homework):
                                                      $schhomeworks=Schoolhomework::model()->find(array('condition'=>'TM_SCH_Id='.$homework['TM_HMG_Homework_Id'].' AND TM_SCH_Status=1'));
                                                      if(count($schhomeworks)>0):
                                                          $studenthw=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$schhomeworks['TM_SCH_Id'].''));
                                                          $studhomework=New StudentHomeworks();
                                                          $studhomework->TM_STHW_Student_Id=$student->TM_STU_User_Id;
                                                          $studhomework->TM_STHW_HomeWork_Id=$schhomeworks['TM_SCH_Id'];
                                                          $studhomework->TM_STHW_Plan_Id=$studenthw['TM_STHW_Plan_Id'];
                                                          if(date('Y-m-d')>=$studenthw['TM_STHW_PublishDate']):
                                                              $studhomework->TM_STHW_Status=0;
                                                          else:
                                                              $studhomework->TM_STHW_Status=7;
                                                          endif;
                                                          $studhomework->TM_STHW_Assigned_By=$studenthw['TM_STHW_Assigned_By'];
                                                          $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                                                          $studhomework->TM_STHW_DueDate=$studenthw['TM_STHW_DueDate'];
                                                          $studhomework->TM_STHW_Comments=$studenthw['TM_STHW_Comments'];
                                                          $studhomework->TM_STHW_PublishDate=$studenthw['TM_STHW_PublishDate'];
                                                          $studhomework->TM_STHW_Type=$studenthw['TM_STHW_Type'];
                                                          $studhomework->TM_STHW_ShowSolution=$studenthw['TM_STHW_ShowSolution'];
                                                          $studhw=StudentHomeworks::model()->find(array('condition' => 'TM_STHW_HomeWork_Id='.$schhomeworks['TM_SCH_Id'].' AND TM_STHW_Student_Id='.$student->TM_STU_User_Id.' '));
                                                          $count=count($studhw);
                                                          if($count==0)
                                                          {
                                                              if(date('Y-m-d')>=$studenthw['TM_STHW_PublishDate']):
                                                                  $notification=new Notifications();
                                                                  $notification->TM_NT_User=$student->TM_STU_User_Id;
                                                                  $notification->TM_NT_Type='HWAssign';
                                                                  $notification->TM_NT_Item_Id=$schhomeworks['TM_SCH_Id'];
                                                                  $notification->TM_NT_Target_Id=$studenthw['TM_STHW_Assigned_By'];
                                                                  $notification->TM_NT_Status='0';
                                                                  $notification->save(false);
                                                              endif;
                                                              $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => 'TM_HQ_Homework_Id='.$schhomeworks['TM_SCH_Id'].' '));
                                                              foreach($hwquestions AS $hwquestion):
                                                                  $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                                                                  $studhwqstns=New Studenthomeworkquestions();
                                                                  $studhwqstns->TM_STHWQT_Mock_Id=$schhomeworks['TM_SCH_Id'];
                                                                  $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                                                                  $studhwqstns->TM_STHWQT_Student_Id=$student->TM_STU_User_Id;
                                                                  $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                                                                  $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                                                                  $studhwqstns->save(false);
                                                                  $questions=Questions::model()->findAllByAttributes(array(
                                                                      'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                                                                  ));
                                                                  //print_r($questions);
                                                                  if(count($questions)!=0):
                                                                      foreach($questions AS $childqstn):
                                                                          $studhwqstns=New Studenthomeworkquestions();
                                                                          $studhwqstns->TM_STHWQT_Mock_Id=$schhomeworks['TM_SCH_Id'];
                                                                          $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                                                          $studhwqstns->TM_STHWQT_Student_Id=$student->TM_STU_User_Id;
                                                                          $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                                                          $studhwqstns->save(false);
                                                                      endforeach;
                                                                  endif;
                                                              endforeach;
                                                              $homeworktotal=$this->Gethomeworktotal($schhomeworks['TM_SCH_Id']);
                                                              $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                                                              $studhomework->save(false);
                                                          }
                                                      endif;
                                                  endforeach;
                                              endif;
                                          endif;
                                      else:
                                          $importlog[$count]['student']='0';
                                      endif;
                                  endif;
                              else:
                                  $checkprevuser=User::model()->find(array('condition' => 'email ="' .$values[13]. '"'));
                                  if(count($checkprevuser)==0)
                                  {
                                      $parentmodel->usertype='7';
                                      $parentmodel->status='1';
                                      $parentmodel->save(false);
                                      ///print_r($parentmodel);exit;
                                      $parentprofile->user_id=$parentmodel->id;
                                      $parentprofile->lastname = $values[11];
                                      $parentprofile->firstname = $values[10];
                                      $parentprofile->phonenumber = $values[12];
                                      $parentprofile->save(false);
                                      $studentuser->email = $username;
                                      $importlog[$count]['status']=$this->GetImportuseritems($values);
                                      if($importlog[$count]['status']==''):
                                          $importlog[$count]['student']='1';
                                          $studentuser->save(false);
                                          $studentuserprofile = new Profile();
                                          $studentuserprofile->user_id = $studentuser->id;
                                          $studentuserprofile->lastname = $values[1];
                                          $studentuserprofile->firstname = $values[0];
                                          $studentuserprofile->save(false);
                                          if($values[9] != '') {
                                              $std=Standard::model()->find(array(
                                                      'condition' => 'TM_SD_Name=:name',
                                                      'params' => array(':name' => $values[9])
                                                  )
                                              );
                                              if(count($std) > 0) {
                                                  $newplan = Plan::model()->find(array(
                                                          'condition' => 'TM_PN_Standard_Id=:id AND TM_PN_Type=:type',
                                                          'params' => array(':id' => $std->TM_SD_Id,':type'=>1)
                                                      )
                                                  );
                                                  $sttudentnewplan = new StudentPlan();
                                                  $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                                  $currentplan = Plan::model()->findByPk($newplan->TM_PN_Id);
                                                  $sttudentnewplan->TM_SPN_PlanId = $newplan->TM_PN_Id;
                                                  $sttudentnewplan->TM_SPN_SyllabusId = $currentplan->TM_PN_Syllabus_Id;
                                                  $sttudentnewplan->TM_SPN_StandardId = $currentplan->TM_PN_Standard_Id;
                                                  $sttudentnewplan->TM_SPN_Status = $currentplan->TM_PN_Status;
                                                  $sttudentnewplan->TM_SPN_Provider = 3;
                                                  $sttudentnewplan->TM_SPN_StudentId = $studentuser->id;
                                                  $sttudentnewplan->save(false);
                                              }
                                          }
                                          $revplans = explode(',', $values[16]);
                                          foreach($revplans AS $revplan):
                                              $data=Plan::model()->find(array(
                                                      'condition' => 'TM_PN_Name=:name AND TM_PN_Type=0',
                                                      'params' => array(':name' => $revplan)
                                                  )
                                              );
  
                                              if(count($data)>0):
                                                  $newstudplan=New StudentPlan();
                                                  $newstudplan->TM_SPN_StudentId = $studentuser->id;
                                                  $newstudplan->TM_SPN_PlanId = $data->TM_PN_Id;
                                                  $newstudplan->TM_SPN_SyllabusId = $data->TM_PN_Syllabus_Id;
                                                  $newstudplan->TM_SPN_StandardId = $data->TM_PN_Standard_Id;
                                                  $newstudplan->TM_SPN_Status = 0;
                                                  $newstudplan->TM_SPN_Provider = 3;
                                                  $newstudplan->save(false);
                                              endif;
                                          endforeach;
                                          //$student->TM_STU_Parent_Id=$id;
                                          $student->TM_STU_First_Name=$values[0];
                                          $student->TM_STU_Last_Name=$values[1];
                                          //$student->TM_STU_Password = $password;
                                          $student->TM_STU_Parent_Id=$parentmodel->id;
                                          $student->TM_STU_User_Id = $studentuser->id;
                                          $student->TM_STU_CreatedBy = Yii::app()->user->id;
                                          $student->TM_STU_student_Id = $values[2];
                                          $student->save(false);
                                          //$this->SendStudentMail($studentuser->id);
                                          $schoolgrps=SchoolGroups::model()->find(array('condition'=>"TM_SCL_Id='".$schoolid."' AND TM_SCL_SD_Id='".$currentplan->TM_PN_Standard_Id."' AND TM_SCL_GRP_Name LIKE '%ALL%'"));
                                          if(count($schoolgrps)>0):
                                              $grpstudents=New GroupStudents();
                                              $grpstudents->TM_GRP_Id=$schoolgrps->TM_SCL_GRP_Id;
                                              $grpstudents->TM_GRP_PN_Id=$newplan->TM_PN_Id;
                                              $grpstudents->TM_GRP_SCL_Id=$schoolid;
                                              $grpstudents->TM_GRP_SD_Id=$currentplan->TM_PN_Standard_Id;
                                              $grpstudents->TM_GRP_STUId=$student->TM_STU_User_Id;
                                              $grpstudents->TM_GRP_STUName=$values[0].' '.$values[1];
                                              $grpstudents->save(false);
                                              $groupid=$schoolgrps->TM_SCL_GRP_Id;
                                              $homeworks=Homeworkgroup::model()->findAll(array('condition'=>'TM_HMG_Groupe_Id='.$groupid.' '));
                                              if(count($homeworks)>0):
                                                  foreach($homeworks AS $homework):
                                                      $schhomeworks=Schoolhomework::model()->find(array('condition'=>'TM_SCH_Id='.$homework['TM_HMG_Homework_Id'].' AND TM_SCH_Status=1'));
                                                      if(count($schhomeworks)>0):
                                                          $studenthw=StudentHomeworks::model()->find(array('condition'=>'TM_STHW_HomeWork_Id='.$schhomeworks['TM_SCH_Id'].''));
                                                          $studhomework=New StudentHomeworks();
                                                          $studhomework->TM_STHW_Student_Id=$student->TM_STU_User_Id;
                                                          $studhomework->TM_STHW_HomeWork_Id=$schhomeworks['TM_SCH_Id'];
                                                          $studhomework->TM_STHW_Plan_Id=$studenthw['TM_STHW_Plan_Id'];
                                                          if(date('Y-m-d')>=$studenthw['TM_STHW_PublishDate']):
                                                              $studhomework->TM_STHW_Status=0;
                                                          else:
                                                              $studhomework->TM_STHW_Status=7;
                                                          endif;
                                                          $studhomework->TM_STHW_Assigned_By=$studenthw['TM_STHW_Assigned_By'];
                                                          $studhomework->TM_STHW_Assigned_On=date('Y-m-d');
                                                          $studhomework->TM_STHW_DueDate=$studenthw['TM_STHW_DueDate'];
                                                          $studhomework->TM_STHW_Comments=$studenthw['TM_STHW_Comments'];
                                                          $studhomework->TM_STHW_PublishDate=$studenthw['TM_STHW_PublishDate'];
                                                          $studhomework->TM_STHW_Type=$studenthw['TM_STHW_Type'];
                                                          $studhomework->TM_STHW_ShowSolution=$studenthw['TM_STHW_ShowSolution'];
                                                          $studhw=StudentHomeworks::model()->find(array('condition' => 'TM_STHW_HomeWork_Id='.$schhomeworks['TM_SCH_Id'].' AND TM_STHW_Student_Id='.$student->TM_STU_User_Id.' '));
                                                          $count=count($studhw);
                                                          if($count==0)
                                                          {
                                                              if(date('Y-m-d')>=$studenthw['TM_STHW_PublishDate']):
                                                                  $notification=new Notifications();
                                                                  $notification->TM_NT_User=$student->TM_STU_User_Id;
                                                                  $notification->TM_NT_Type='HWAssign';
                                                                  $notification->TM_NT_Item_Id=$schhomeworks['TM_SCH_Id'];
                                                                  $notification->TM_NT_Target_Id=$studenthw['TM_STHW_Assigned_By'];
                                                                  $notification->TM_NT_Status='0';
                                                                  $notification->save(false);
                                                              endif;
                                                              $hwquestions=HomeworkQuestions::model()->findAll(array('condition' => 'TM_HQ_Homework_Id='.$schhomeworks['TM_SCH_Id'].' '));
                                                              foreach($hwquestions AS $hwquestion):
                                                                  $question=Questions::model()->findByPk($hwquestion->TM_HQ_Question_Id);
                                                                  $studhwqstns=New Studenthomeworkquestions();
                                                                  $studhwqstns->TM_STHWQT_Mock_Id=$schhomeworks['TM_SCH_Id'];
                                                                  $studhwqstns->TM_STHWQT_Question_Id=$hwquestion->TM_HQ_Question_Id;
                                                                  $studhwqstns->TM_STHWQT_Student_Id=$student->TM_STU_User_Id;
                                                                  $studhwqstns->TM_STHWQT_Order=$hwquestion->TM_HQ_Order;
                                                                  $studhwqstns->TM_STHWQT_Type=$hwquestion->TM_HQ_Type;
                                                                  $studhwqstns->save(false);
                                                                  $questions=Questions::model()->findAllByAttributes(array(
                                                                      'TM_QN_Parent_Id'=> $hwquestion->TM_HQ_Question_Id
                                                                  ));
                                                                  //print_r($questions);
                                                                  if(count($questions)!=0):
                                                                      foreach($questions AS $childqstn):
                                                                          $studhwqstns=New Studenthomeworkquestions();
                                                                          $studhwqstns->TM_STHWQT_Mock_Id=$schhomeworks['TM_SCH_Id'];
                                                                          $studhwqstns->TM_STHWQT_Question_Id=$childqstn->TM_QN_Id;
                                                                          $studhwqstns->TM_STHWQT_Student_Id=$student->TM_STU_User_Id;
                                                                          $studhwqstns->TM_STHWQT_Parent_Id=$childqstn->TM_QN_Parent_Id;
                                                                          $studhwqstns->save(false);
                                                                      endforeach;
                                                                  endif;
                                                              endforeach;
                                                              $homeworktotal=$this->Gethomeworktotal($schhomeworks['TM_SCH_Id']);
                                                              $studhomework->TM_STHW_TotalMarks=$homeworktotal;
                                                              $studhomework->save(false);
                                                          }
                                                      endif;
                                                  endforeach;
                                              endif;
                                          endif;
                                      else:
                                          $importlog[$count]['student']='0';
                                      endif;
                                  }
                                  else{
                                      $importlog[$count]['student']='0';
                                      $importlog[$count]['status']="User with ".$values[13]." exists";
                                  }
                              endif;
                              ///$importlog[$count]['student']='1';
                          }
                      endif;
                      $count++;
                  }
                  else
                  {
                      $importlog[$count]['student']='0';
                      $count++;
                  }
  
              }
          endif;
          $this->render('studentimport', array(
              'importlog'=>$importlog
          ));
      }
      public function GetImportparent($values,$userid)
      {
          $message="";
          $email=addslashes($values[13]);
          $count=User::model()->count(array('condition'=>"(email='".$email."' OR username='".$email."') AND usertype!=7"));
          if($count > 0)
          {
              $message.="Cannot update parent email : Email already exists";
          }
          $count=User::model()->count(array('condition'=>"(email='".$email."' OR username='".$email."') AND usertype=7 AND id!=".$userid." "));
          if($count > 0)
          {
              $message.="Parent exists with Email: ".$email."";
          }
          return $message;
      }
    public function GetImportitems($values)
    {
        $message="";
        if(count(array_filter($values))<7)
        {
            $message.='Invalid File Format';
        }

        return $message;
    }
    public function GetImportuseritems($values)
    {
        $message="";
        if(preg_match('/[^A-Za-z0-9.@]/', $values[2])) {
            $message.='Username allows only letters,numbers,@ and "." <br>';
        }
        if($values[2]=='')
        {
            $message.='Username cannot be blank<br>';
        }
        return $message;
    }
    public function Gethomeworktotal($homeworkid)
    {
        $questions= HomeworkQuestions::model()->findAll(array('condition'=>'TM_HQ_Homework_Id='.$homeworkid));
        $homeworktotal=0;
        foreach($questions AS $question):
            $question=Questions::model()->findByPk($question->TM_HQ_Question_Id);
            switch($question->TM_QN_Type_Id)
            {
                case 4:
                    $marks=Answers::model()->find(array('select'=>'TM_AR_Marks AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' '));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
                case 5:
                    $command = Yii::app()->db->createCommand();
                    $row = Yii::app()->db->createCommand(array(
                        'select' => array('SUM(TM_AR_Marks) AS totalmarks'),
                        'from' => 'tm_answer',
                        'where' => 'TM_AR_Question_Id IN (SELECT TM_QN_Id FROM 	tm_question WHERE TM_QN_Parent_Id='.$question->TM_QN_Id.')',
                    ))->queryRow();
                    $homeworktotal=$homeworktotal+$row['totalmarks'];
                    break;
                case 6:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                case 7:
                    echo $question->TM_QN_Type_Id;
                    $homeworktotal=$homeworktotal+$question->TM_QN_Totalmarks;
                    break;
                default:
                    $marks=Answers::model()->find(array('select'=>'SUM(TM_AR_Marks) AS totalmarks', 'condition'=>'TM_AR_Question_Id='.$question->TM_QN_Id.' AND TM_AR_Correct=1'));
                    $homeworktotal=$homeworktotal+$marks->totalmarks;
                    break;
            }

        endforeach;
        return $homeworktotal;
    }
        
}