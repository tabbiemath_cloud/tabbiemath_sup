<?php

class LoginController extends Controller
{
    public $layout='//layouts/admincolumn1';
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					
					if (strpos(Yii::app()->user->returnUrl,'/index.php')!==false)
                        if(Yii::app()->user->isAdmin()):
                            $this->SetType();
                            $this->lastViset();
						    $this->redirect(Yii::app()->controller->module->returnUrladmin);
                        else:
                            if(Yii::app()->user->isStudent()):
                                $this->SetPlan();
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlStudent);
                            elseif(Yii::app()->user->isParent()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlParent);
                            elseif(Yii::app()->user->isSchoolAdmin()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlSchoolAdmin);
                            elseif(Yii::app()->user->isTeacher()):
                                $this->lastViset();
                                $this->SetStandard();
                                $this->redirect(Yii::app()->controller->module->returnUrlTeacher);                                
                            elseif(Yii::app()->user->isMarker()):    
                                $this->lastViset();                                                        
                                $this->redirect(Yii::app()->controller->module->returnUrlMarker);                            
                            else:
                                $this->redirect(Yii::app()->controller->module->returnUrl);
                            endif;
                        endif;
					else
                        if(Yii::app()->user->isAdmin()):
                            $this->lastViset();
                             $this->SetType();
                            $this->redirect(Yii::app()->controller->module->returnUrladmin);
                        else:
                            if(Yii::app()->user->isStudent()):
                                $this->SetPlan();
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlStudent);
                            elseif(Yii::app()->user->isParent()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlParent);
                            elseif(Yii::app()->user->isSchoolAdmin()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlSchoolAdmin);
                            elseif(Yii::app()->user->isTeacher()):
                                $this->lastViset();
                                $this->SetStandard();
                                $this->redirect(Yii::app()->controller->module->returnUrlTeacher);
                            elseif(Yii::app()->user->isMarker()):    
                                $this->lastViset();                                                        
                                $this->redirect(Yii::app()->controller->module->returnUrlMarker);                                 
                            else:
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrl);
                            endif;
                        endif;
				}
			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
        {
            if(Yii::app()->user->isAdmin()):
                 $this->SetType();
                $this->redirect(Yii::app()->controller->module->returnUrladmin);
            else:
                if(Yii::app()->user->isStudent()):
                    $this->SetPlan();
                    $this->lastViset();
                    $this->redirect(Yii::app()->controller->module->returnUrlStudent);
                elseif(Yii::app()->user->isParent()):
                    $this->lastViset();
                    $this->redirect(Yii::app()->controller->module->returnUrlParent);
                elseif(Yii::app()->user->isTeacher()):
                    $this->lastViset();
                    $this->SetStandard();
                    $this->redirect(Yii::app()->controller->module->returnUrlTeacher); 
                elseif(Yii::app()->user->isMarker()):    
                    $this->lastViset();                                                        
                    $this->redirect(Yii::app()->controller->module->returnUrlMarker);                                        
                else:
                    $this->lastViset();
                    $this->redirect(Yii::app()->controller->module->returnUrl);
                endif;
            endif;
        }
	}
    /*public function actionLogintesting()
	{
        if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					
					if (strpos(Yii::app()->user->returnUrl,'/index.php')!==false)
                        if(Yii::app()->user->isAdmin()):
                            $this->SetType();
                            $this->lastViset();
						    $this->redirect(Yii::app()->controller->module->returnUrladmin);
                        else:
                            if(Yii::app()->user->isStudent()):
                                $this->SetPlan();
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlStudent);
                            elseif(Yii::app()->user->isParent()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlParent);
                            elseif(Yii::app()->user->isSchoolAdmin()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlSchoolAdmin);
                            elseif(Yii::app()->user->isTeacher()):
                                $this->lastViset();
                                $this->SetStandard();
                                $this->redirect(Yii::app()->controller->module->returnUrlTeacher);                                
                            elseif(Yii::app()->user->isMarker()):    
                                $this->lastViset();                                                        
                                $this->redirect(Yii::app()->controller->module->returnUrlMarker);                            
                            else:
                                $this->redirect(Yii::app()->controller->module->returnUrl);
                            endif;
                        endif;
					else
                        if(Yii::app()->user->isAdmin()):
                            $this->lastViset();
                             $this->SetType();
                            $this->redirect(Yii::app()->controller->module->returnUrladmin);
                        else:
                            if(Yii::app()->user->isStudent()):
                                $this->SetPlan();
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlStudent);
                            elseif(Yii::app()->user->isParent()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlParent);
                            elseif(Yii::app()->user->isSchoolAdmin()):
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrlSchoolAdmin);
                            elseif(Yii::app()->user->isTeacher()):
                                $this->lastViset();
                                $this->SetStandard();
                                $this->redirect(Yii::app()->controller->module->returnUrlTeacher);
                            elseif(Yii::app()->user->isMarker()):    
                                $this->lastViset();                                                        
                                $this->redirect(Yii::app()->controller->module->returnUrlMarker);                                 
                            else:
                                $this->lastViset();
                                $this->redirect(Yii::app()->controller->module->returnUrl);
                            endif;
                        endif;
				}
			}
			// display the login form
			$this->render('/user/logintest',array('model'=>$model));
		} else
        {
            if(Yii::app()->user->isAdmin()):
                 $this->SetType();
                $this->redirect(Yii::app()->controller->module->returnUrladmin);
            else:
                if(Yii::app()->user->isStudent()):
                    $this->SetPlan();
                    $this->lastViset();
                    $this->redirect(Yii::app()->controller->module->returnUrlStudent);
                elseif(Yii::app()->user->isParent()):
                    $this->lastViset();
                    $this->redirect(Yii::app()->controller->module->returnUrlParent);
                elseif(Yii::app()->user->isTeacher()):
                    $this->lastViset();
                    $this->SetStandard();
                    $this->redirect(Yii::app()->controller->module->returnUrlTeacher); 
                elseif(Yii::app()->user->isMarker()):    
                    $this->lastViset();                                                        
                    $this->redirect(Yii::app()->controller->module->returnUrlMarker);                                        
                else:
                    $this->lastViset();
                    $this->redirect(Yii::app()->controller->module->returnUrl);
                endif;
            endif;
        }
	}*/	
	public function lastViset() {
		$lastVisit = User::model()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();        
		$lastVisit->save(false);
	}
    private function SetType()
    {
        Yii::app()->session['usertype']=User::model()->findbyPk(Yii::app()->user->id)->usertype;        
    }
    private function SetPlan()
    {
        $plan=StudentPlan::model()->find(array('condition'=>'TM_SPN_StudentId='.Yii::app()->user->id,'limit'=>1,'order'=>'TM_SPN_CreatedOn DESC'));
        $masterplan=Plan::model()->findByPk($plan->TM_SPN_PlanId);
                
        $modules=Planmodules::model()->findAll(array('condition'=>'TM_PM_Plan_Id='.$plan->TM_SPN_PlanId));
        Yii::app()->session['plan'] = $plan->TM_SPN_PlanId;
        Yii::app()->session['publisher'] = $plan->TM_SPN_Publisher_Id;
        Yii::app()->session['syllabus'] = $plan->TM_SPN_SyllabusId;
        Yii::app()->session['standard'] = $plan->TM_SPN_StandardId;
        Yii::app()->session['subject'] = $plan->TM_SPN_SubjectId;
        if($masterplan->TM_PN_Type=='1'):
            Yii::app()->session['school'] = true;
        else:
            Yii::app()->session['school'] = false;        
            foreach($modules AS $module):
                switch($module->TM_PM_Module)
                {
                    case "1":
                        Yii::app()->session['quick'] = true;
                        break;
                    case "2":
                        Yii::app()->session['difficulty'] = true;
                        break;
                    case "3":
                        Yii::app()->session['challenge'] = true;
                        break;
                    case "4":
                        Yii::app()->session['mock'] = true;
                        break;
                    case "5":
                        Yii::app()->session['resource'] = true;
                        break;
                    case "6":
                        Yii::app()->session['blueprint'] = true;
                        break;
                }
            endforeach;            
        endif;
    }
    private function SetStandard()
    {
        $standards=TeacherStandards::model()->find(array('condition'=>'TM_TE_ST_User_Id='.Yii::app()->user->id,'order'=>'TM_TE_ST_Id ASC'));
        $user=User::model()->findByPk(Yii::app()->user->id);
        $syllabus=Standard::model()->find(array('condition'=>'TM_SD_Id='.$standards->TM_TE_ST_Standard_Id));
        Yii::app()->session['standard'] = $standards->TM_TE_ST_Standard_Id;
        Yii::app()->session['school'] = $user->school_id;
        Yii::app()->session['syllabus'] = $syllabus->TM_SD_Syllabus_Id;
        Yii::app()->session['mode'] = "Teacher";
    }       
}