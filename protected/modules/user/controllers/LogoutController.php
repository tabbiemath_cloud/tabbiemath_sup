<?php

class LogoutController extends Controller
{
	public $defaultAction = 'logout';
	
	/**
	 * Logout the current user and redirect to returnLogoutUrl.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();                
        if(isset($_GET['referrer'])):            
            $this->redirect(Yii::app()->createUrl('trial/home'));
        else:        
            $this->redirect(Yii::app()->controller->module->returnLogoutUrl);            
        endif;
	}

}