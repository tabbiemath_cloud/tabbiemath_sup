<?php

class RegistrationController extends Controller
{
	public $defaultAction = 'registration';
	


	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return (isset($_POST['ajax']) && $_POST['ajax']==='registration-form')?array():array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
	/**
	 * Registration user
	 */
	public function actionRegistration() {
            $model = new RegistrationForm;
            $profile=new Profile;
            $student=new Student();

            $profile->regMode = true;
            
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
			{
				echo UActiveForm::validate(array($model,$profile));
				Yii::app()->end();
			}
			
		    if (Yii::app()->user->id) {
		    	$this->redirect(Yii::app()->controller->module->profileUrl);
		    } else {
		    	if(isset($_POST['RegistrationForm'])) {
					$model->attributes=$_POST['RegistrationForm'];
                    $model->usertype='7';
					$profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
                    $student->attributes=((isset($_POST['Student'])?$_POST['Student']:array()));

					if($model->validate()&  $profile->validate()&  $student->validate())
					{
						$soucePassword = $model->password;
						$passwordnoncrypted = $_POST['RegistrationForm']['password'];
						$model->activkey=UserModule::encrypting(microtime().$model->password);
						$model->password=UserModule::encrypting($model->password);
						$model->verifyPassword=UserModule::encrypting($model->verifyPassword);
						$model->createtime=time();
						$model->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
						$model->superuser=0;
						$model->status=((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);
						
						if ($model->save()) {
							$profile->user_id=$model->id;
							$profile->save();
                            $username=strtoupper(substr($student->TM_STU_First_Name, 0,1).substr($student->TM_STU_Last_Name, 0,1).rand(11111,99999));
                            $password=$this->generateRandomString();
                            $studentuser=new User();
                            $studentuser->username=$username;
                            $studentuser->password=md5($password);
                            $studentuser->activkey=UserModule::encrypting(microtime().$studentuser->password);
                            $studentuser->lastvisit=((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;
                            $studentuser->superuser=0;
                            $studentuser->usertype='6';
                            $studentuser->createtime=time();
                            $studentuser->email=$username;
                            $studentuser->status=((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);
                            $studentuser->save(false);
                            $studentuserprofile=new Profile();
                            $studentuserprofile->user_id=$studentuser->id;
                            $studentuserprofile->lastname=$student->TM_STU_Last_Name;
                            $studentuserprofile->firstname=$student->TM_STU_First_Name;
                            $studentuserprofile->save(false);
                            $student->TM_STU_Parent_Id=$model->id;
                            $student->TM_STU_Password=$password;
                            $student->TM_STU_User_Id=$studentuser->id;
                            $student->save(false);
							if (Yii::app()->controller->module->sendActivationMail) {
								//$activation_url = $this->createAbsoluteUrl('/user/activation/activation',array("activkey" => $model->activkey, "email" => $model->email));
								//UserModule::sendRegistrationMail($model->id,$passwordnoncrypted,$activation_url);
                                $activation_url = $this->createAbsoluteUrl('/user/login');
                                $temp=new Userpass();
                                $temp->user=$model->id;
                                $temp->password=$passwordnoncrypted;
                                $temp->url=$activation_url; 
                                $temp->save(false);                               
							}							
							if ((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin) {
									$identity=new UserIdentity($model->username,$soucePassword);
									$identity->authenticate();
									Yii::app()->user->login($identity,0);
									$this->redirect(Yii::app()->controller->module->returnUrl);
							} else {
								if (!Yii::app()->controller->module->activeAfterRegister&&!Yii::app()->controller->module->sendActivationMail) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Contact Admin to activate your account."));
								} elseif(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please {{login}}.",array('{{login}}'=>CHtml::link(UserModule::t('Login'),Yii::app()->controller->module->loginUrl))));
								} elseif(Yii::app()->controller->module->loginNotActiv) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email or login."));
								} else {
								    $this->redirect(array("/user/registration/subscribe", 'id' => $model->id));
									//Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email."));
								}
								$this->refresh();
							}
						}
					} else $profile->validate();
				}
			    $this->render('/user/registration',array('model'=>$model,'profile'=>$profile,'student'=>$student));
		    }
	}
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function actionSubscribe($id)
    {
        $studentplan=new StudentPlan();
        $parent=User::model()->findByPk($id);
        $student=Student::model()->find(array('condition'=>'TM_STU_Parent_Id='.$id));
        $studentdetails=User::model()->findByPk($student->TM_STU_User_Id);
        $id=base64_encode($_GET['subcription']);             
        $this->render('/user/studentsubscription',array(
			'model'=>$studentplan,
            'id'=>$id,
            'student'=>$student->TM_STU_User_Id,
            'parent'=>$parent,
            'studentdetails'=>$studentdetails
		));  
    }
    public function checkUsername($username)
    {
        $usercount=User::model()->cound(array('condition'=>'username='.$username));
        if($usercount>0):
            return false;
        else:
            return true;
        endif;
    }
    public function actionPaymentComplete()
    {       
        if(isset($_GET['supredent'])&& isset($_GET['application']))
        {
            $plan=Plan::model()->findByPk(base64_decode($_GET['application']));
            $duration=$plan->TM_PN_Duration+1;            
            $student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.base64_decode($_GET['supredent'])));                                          
            $model=new StudentPlan();
            $model->TM_SPN_PlanId=$plan->TM_PN_Id;
            $model->TM_SPN_Publisher_Id=$plan->TM_PN_Publisher_Id;
            $model->TM_SPN_SyllabusId=$plan->TM_PN_Syllabus_Id;
            $model->TM_SPN_SubjectId='1';
            $model->TM_SPN_StandardId=$plan->TM_PN_Standard_Id;
            $model->TM_SPN_StudentId=base64_decode($_GET['supredent']);
            $model->TM_SPN_Status='0';
            $model->TM_SPN_PaymentReference=$_GET['transaction'];
            $model->TM_SPN_CouponId=base64_decode($_GET['voucher']);
            $model->TM_SPN_ExpieryDate=date('Y-m-d', strtotime("+".$duration." days"));            
            $model->save(false);
            $user=User::model()->findByPk(base64_decode($_GET['supredent']));
            $user->status='1';
            $user->save(false);
            $parent=User::model()->findByPk($student->TM_STU_Parent_Id);
            $parent->status='1';
            $parent->save(false);            
            $tempdetails=Userpass::model()->find(array('condition'=>'user='.$student->TM_STU_Parent_Id));                        
            Yii::app()->user->setFlash('subscriptionsuccess',UserModule::t("Congratulations. The payment has been successful. You may now login to TabbieMath and start using it. Please note your payment transaction ID : ".$_GET['transaction']));            
            Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email."));
            UserModule::sendRegistrationMail($student->TM_STU_Parent_Id,$tempdetails->password,$tempdetails->url);
            $tempdetails->delete();
            $this->redirect(Yii::app()->controller->module->registrationUrl);
        }
        else{
            Yii::app()->user->setFlash('subscriptionfail',UserModule::t("The payment has failed. Please check for the message from Payu for the failure. Contact Admin to activate your account."));
            $this->redirect(Yii::app()->controller->module->registrationUrl);
        }

    }    
}