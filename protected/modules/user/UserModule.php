<?php
/**
 * Yii-User module
 * 
 * @author Mikhail Mangushev <mishamx@gmail.com> 
 * @link http://yii-user.googlecode.com/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @version $Id: UserModule.php 105 2011-02-16 13:05:56Z mishamx $
 */

class UserModule extends CWebModule
{
	/**
	 * @var int
	 * @desc items on page
	 */
	public $user_page_size = 10;
	
	/**
	 * @var int
	 * @desc items on page
	 */
	public $fields_page_size = 10;
	
	/**
	 * @var string
	 * @desc hash method (md5,sha1 or algo hash function http://www.php.net/manual/en/function.hash.php)
	 */
	public $hash='md5';
	
	/**
	 * @var boolean
	 * @desc use email for activation user account
	 */
	public $sendActivationMail=true;
	
	/**
	 * @var boolean
	 * @desc allow auth for is not active user
	 */
	public $loginNotActiv=false;
	
	/**
	 * @var boolean
	 * @desc activate user on registration (only $sendActivationMail = false)
	 */
	public $activeAfterRegister=false;
	
	/**
	 * @var boolean
	 * @desc login after registration (need loginNotActiv or activeAfterRegister = true)
	 */
	public $autoLogin=false;
	
	public $registrationUrl = array("/user/registration");
    public $subscriptionUrl = array("/user/registration/subscribe");
	public $recoveryUrl = array("/user/recovery/recovery");
	public $loginUrl = array("/user/login");
	public $logoutUrl = array("/user/logout");
	public $profileUrl = array("/user/profile");
	public $returnUrladmin = array("/administration");
	public $returnUrlStudent = array("/home");
	public $returnUrlParent = array("/parenthome");
    public $returnUrlSchoolAdmin = array("/schoolhome");
    public $returnUrlTeacher = array("/teacherhome");
    public $returnUrlMarker = array("/staffhome");
	public $returnUrl = array("/user/profile");
	public $returnLogoutUrl = array("/user/login");
	
	public $fieldsMessage = '';
	
	/**
	 * @var array
	 * @desc User model relation from other models
	 * @see http://www.yiiframework.com/doc/guide/database.arr
	 */
	public $relations = array();
	
	/**
	 * @var array
	 * @desc Profile model relation from other models
	 */
	public $profileRelations = array();
	
	/**
	 * @var boolean
	 */
	public $captcha = array('registration'=>true);
	
	/**
	 * @var boolean
	 */
	//public $cacheEnable = false;
	
	public $tableUsers = '{{users}}';
	public $tableProfiles = '{{profiles}}';
	public $tableProfileFields = '{{profile_fields}}';
	
	static private $_user;
	static private $_admin;
	static private $_allowed;
    static private $_allowedschool;
	static private $_allowedstudent;    
	static private $_student;
	static private $_parent;
	static private $_schoolstaff;
	static private $_schoolstaffadmin;    
	static private $_schooladmin;    
	static private $_teacher;
    static private $_admins;
	static private $_superadmin;
	static private $_marker;
	static private $_teacheradmin;
	static private $_managequestions;
	static private $_teacherallowed;    
    static private $_accountadmins;	
    static private $_accountadmin;
	/**
	 * @var array
	 * @desc Behaviors for models
	 */
	public $componentBehaviors=array();
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'user.models.*',
			'user.components.*',
		));
	}
	
	public function getBehaviorsFor($componentName){
        if (isset($this->componentBehaviors[$componentName])) {
            return $this->componentBehaviors[$componentName];
        } else {
            return array();
        }
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	
	/**
	 * @param $str
	 * @param $params
	 * @param $dic
	 * @return string
	 */
	public static function t($str='',$params=array(),$dic='user') {
		return Yii::t("UserModule.".$dic, $str, $params);
	}
	
	/**
	 * @return hash string.
	 */
	public static function encrypting($string="") {
		$hash = Yii::app()->getModule('user')->hash;
		if ($hash=="md5")
			return md5($string);
		if ($hash=="sha1")
			return sha1($string);
		else
			return hash($hash,$string);
	}
	
	/**
	 * @param $place
	 * @return boolean 
	 */
	public static function doCaptcha($place = '') {
		if(!extension_loaded('gd'))
			return false;
		if (in_array($place, Yii::app()->getModule('user')->captcha))
			return Yii::app()->getModule('user')->captcha[$place];
		return false;
	}
	
	/**
	 * Return admin status.
	 * @return boolean
	 */
	public static function isAdmin() {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_admin)) {
				if(self::user()->superuser):

					self::$_admin = true;
				else:
					self::$_admin = false;	
				endif;
			}
			return self::$_admin;
		}
	}
	public static function isSuperAdmin() {	   
		if(Yii::app()->user->isGuest)
			return false;
		else {		  					
			if (!isset(self::$_superadmin)) {
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id='.self::user()->id.' AND TM_UR_Role=1'));
                if($rolecount>0):
                    self::$_superadmin = true;
				else:              
					self::$_superadmin = false;	
				endif;
			}
			return self::$_superadmin;
		}
	}
    public static function isAccountAdmin() {
        if(Yii::app()->user->isGuest)
            return false;
        else {
            if (!isset(self::$_accountadmin)) {
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id!=1 AND TM_UR_User_Id='.self::user()->id.' AND TM_UR_Role=4'));

                if($rolecount>0):
                    self::$_accountadmin = true;
                else:
                    self::$_accountadmin = false;
                endif;
            }
            return self::$_accountadmin;
        }
    }
    public static function isAllowedQuestion() {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_allowed)) {
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id='.self::user()->id.' AND TM_UR_Role IN (1,2)'));                
                if($rolecount>0):
                    self::$_allowed = true;
				else:              
					self::$_allowed = false;	
				endif;
			}
			return self::$_allowed;
		}
	}
	public static function isAllowedSchool() {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_allowedschool)) {
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id='.self::user()->id.' AND TM_UR_Role IN (1,4)'));   
                if($rolecount>0):
                    self::$_allowedschool = true;
				else:              
					self::$_allowedschool = false;	
				endif;
			}
			return self::$_allowedschool;
		}
	}
	public static function isAllowedStudent() {
	   
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_allowedstudent)) {		
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id='.self::user()->id.' AND TM_UR_Role IN (1,3)'));
                if($rolecount>0):
                    self::$_allowedstudent = true;
				else:              
					self::$_allowedstudent = false;	
				endif;
			}
			return self::$_allowedstudent;
		}
	}    
	public static function isStudent() {

		if(Yii::app()->user->isGuest)
        {
            return false;
        }
		else {

			if (!isset(self::$_student)) {

				if(self::user()->usertype=='6'):
					self::$_student = true;
				else:
					self::$_student = false;
                endif;
			}
			return self::$_student;
		}
	}
    public function isSchoolAdmin()
    {
		if(Yii::app()->user->isGuest)
        {
            return false;
        }
		else {

			if (!isset(self::$_schoolstaff)) {

				if(self::user()->usertype=='8' || self::user()->usertype=='10'):
					self::$_schoolstaff = true;
				else:
					self::$_schoolstaff = false;
                endif;
			}
			return self::$_schoolstaff;
		}        
        
    }
    public static function isTeacher()
    {
        if(Yii::app()->user->isGuest)
        {
            return false;
        }
        else {

            if (!isset(self::$_teacher)) {

                if(self::user()->usertype=='9' || self::user()->usertype=='12'):
                    self::$_teacher = true;
                else:
                    self::$_teacher = false;
                endif;
            }
            return self::$_teacher;
        }
    }
    public static function isTeacherAllowed()
    {
        if(Yii::app()->user->isGuest)
        {
            return false;
        }
        else {

            if (!isset(self::$_teacherallowed)) {
                $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.Yii::app()->user->id))->TM_TH_Manage_Questions;
                if((self::user()->usertype=='9' || self::user()->usertype=='12') & $teacher=='1'):
                    self::$_teacherallowed = true;
                else:
                    self::$_teacherallowed = false;
                endif;
            }
            return self::$_teacherallowed;
        }
    }    
    public static function isTeacherManageQUestion()
    {
        $teacher=Teachers::model()->find(array('condition'=>'TM_TH_User_Id='.Yii::app()->user->id))->TM_TH_Manage_Questions;        
        if($teacher=='1'):
            self::$_managequestions = true;
        else:
            self::$_managequestions = false;
        endif;
       return self::$_managequestions; 
    }    
    public static function isTeacherAdmin()
    {
        if(Yii::app()->user->isGuest)
        {
            return false;
        }
        else {

            if (!isset(self::$_teacheradmin)) {

                if(self::user()->usertype=='12'):
                    self::$_teacheradmin = true;
                else:
                    self::$_teacheradmin = false;
                endif;
            }
            return self::$_teacheradmin;
        }
    }
    public static function isMarker() {

        if(Yii::app()->user->isGuest)
        {
            return false;
        }
        else {

            if (!isset(self::$_marker)) {

                if(self::user()->usertype=='11'):
                    self::$_marker = true;
                else:
                    self::$_marker = false;
                endif;
            }
            return self::$_marker;
        }
    }    
	public static function isSchoolStaffAdmin($id) {

		if(Yii::app()->user->isGuest)
        {
            return false;
        }
		else {

			if (!isset(self::$_schoolstaffadmin)) {

				if(self::user()->school_id==$id &&  (self::user()->usertype=='8' || self::user()->usertype=='10' || self::user()->usertype=='12')):
					self::$_schoolstaffadmin = true;
				else:
					self::$_schoolstaffadmin = false;
                endif;
			}
			return self::$_schoolstaffadmin;
		}
	}
    public static function isSchoolStaffSSupAdmin($id)
    {

		if(Yii::app()->user->isGuest)
        {
            return false;
        }
		else {

			if (!isset(self::$_schooladmin)) {

				if(self::user()->school_id==$id &&   self::user()->usertype=='8' || self::user()->usertype=='12'):
					self::$_schooladmin = true;
				else:
					self::$_schooladmin = false;
                endif;
			}
			return self::$_schooladmin;
		}
	}  
	public static function isParent() {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_parent)) {
				if(self::user()->usertype=='7')
					self::$_parent = true;
				else
					self::$_parent = false;
			}
			return self::$_parent;
		}
	}
	public static function isStudentChalange($id) {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_student)) {
				if(self::user()->usertype=='6'):
                    $receppient=Chalangerecepient::model()->count(array('condition'=>'TM_CE_RE_Recepient_Id='.Yii::app()->user->id.' AND TM_CE_RE_Chalange_Id='.$id));
                    if($receppient>0):
                        self::$_student = true;
                    else:
                        self::$_student = false;
                    endif;
				else:
					self::$_student = false;
                endif;
			}
			return self::$_student;
		}
	}
	public static function isStudentMarker($id) {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_marker)) {
				if(self::user()->usertype=='6'):
                    $receppient=Homeworkmarking::model()->count(array('condition'=>'TM_HWM_User_Id='.Yii::app()->user->id.' AND TM_HWM_Homework_Id='.$id));
                    if($receppient>0):
                        self::$_marker = true;
                    else:
                        self::$_marker = false;
                    endif;
				else:
					self::$_marker = false;
                endif;
			}
			return self::$_marker;
		}
	}    

	/**
	 * Return admins.
	 * @return array syperusers names
	 */	
	public static function getQuestionAdmins() {
		if (!self::$_admins) {
			$admins = User::model()->active()->superuser()->findAll();
			$return_name = array();
			foreach ($admins as $admin):
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id='.$admin->id.' AND TM_UR_Role IN (1,2)'));
				if($rolecount>0):
					array_push($return_name,$admin->username);
				endif;				
			endforeach;
			self::$_admins = $return_name;
		}
		return self::$_admins;
	}
	public static function getAdmins() {
		if (!self::$_admins) {
			$admins = User::model()->active()->superuser()->findAll();
			$return_name = array();
			foreach ($admins as $admin):
				//if($admin->usertype=='2' || $admin->usertype=='1'):
					array_push($return_name,$admin->username);
				//endif;
			endforeach;
			self::$_admins = $return_name;
		}
		return self::$_admins;
	}
    
	public static function getSuperAdmins() {
		if (!self::$_admins) {
			$admins = User::model()->active()->superuser()->findAll();
			$return_name = array();
			foreach ($admins as $admin):
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id='.$admin->id.' AND TM_UR_Role=1'));
				if($rolecount>0):
					array_push($return_name,$admin->username);
				endif;
			endforeach;
			self::$_admins = $return_name;
		}
		return self::$_admins;
	}
	public static function getAccountAdmins() {
		if (!self::$_accountadmins) {
			$admins = User::model()->active()->superuser()->findAll();
			$return_name = array();
			foreach ($admins as $admin):
                $rolecount=UserRoles::model()->count(array('condition'=>'TM_UR_User_Id='.$admin->id.' AND TM_UR_Role IN(1,4)'));
				if($rolecount>0):
					array_push($return_name,$admin->username);
				endif;
			endforeach;
			self::$_accountadmins = $return_name;
		}
		return self::$_accountadmins;
	}    
    public static function getParentUser()
    {
        if (!self::$_admins) {
            $admins = User::model()->active()->findAll(array('condition'=>'usertype=7'));
            $return_name = array();
            foreach ($admins as $admin):
                    array_push($return_name,$admin->username);
            endforeach;
            self::$_admins = $return_name;
        }
        return self::$_admins;
    }
/**********************************************************************************************************
*   School admin section
**********************************************************************************************************/     
    public function GetSchoolDetails()
    {
        $user=User::model()->findByPk(Yii::app()->user->id);
        $school=School::model()->findByPk($user->school_id);
        return $school;     
    } 
/**********************************************************************************************************    
*   School admin section Ends
**********************************************************************************************************/       
	/**
	 * Send mail method
	 */
	public static function sendMail($email,$subject,$message) {
        $adminEmail = Yii::app()->params['noreplayEmail'];

        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
		$mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->SMTPSecure = "tls";
		$mail->Port       = 587;
		$mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
		$mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($email);
        $mail->AddReplyTo("noreply@tabbiemath.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $message;

        return $mail->Send();
	}
	public static function sendRegistrationMail($userid,$studentuserid,$password,$activation_url) {
		$user=User::model()->findByPk($userid);
		//$student=Student::model()->find(array('condition'=>'TM_STU_Parent_Id='.$userid));
		$student=Student::model()->find(array('condition'=>'TM_STU_User_Id='.$studentuserid));
		$studentuser=User::model()->findByPk($studentuserid);
		$adminEmail = Yii::app()->params['noreplayEmail'];
		$message="<p>Dear ".$student->TM_STU_First_Name."</p>
                   <p>Thank you for registering on to TabbieMath - the one stop shop for Maths revision !  Below are your login details  </p>
                    <div style='border:solid 1px black;background-color: #E6E6FA;padding: 10px;text-align: center;'>                        
                        <p style='text-align:center;'><a href='".$activation_url."'> www.tabbiemath.com</a> </p>
                        <p><b>Student Name</b> : ".$student->TM_STU_First_Name." ".$student->TM_STU_Last_Name."</p>
                        <p><b>Username </b>: ".$studentuser->username."</p>
                        <p><b>Password </b>: ".$student->TM_STU_Password."</p>
                    </div>                   
                    <p>For any help in getting started, please visit the Help Section at http://www.tabbiemath.com/demo-faq/
                   </p>
                    <p>Regards,<br>TabbieMath Team</p>
                  ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->IsSMTP();
		$mail->Host = "email-smtp.eu-west-1.amazonaws.com"; // SMTP servers
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->SMTPSecure = "tls";
		$mail->Port       = 587;
		$mail->Username = "AKIAWY7CM4EBL7VOXPZI"; // SMTP username
		$mail->Password = "BNXTltxtaif6R5p8n6GaXoE0sWhCY/W8nny7Foq6G/3k"; /// SMTP password
        //To show up in the From Email & Reply Email - Change this to the id that client needs in the original email
        $mail->From = 'services@tabbiemath.com';
        $mail->FromName = "TabbieMath";
        $mail->AddAddress($user->email);
        $mail->AddReplyTo("noreply@tabbiemath.com","noreply");
        $mail->IsHTML(true);
        $mail->Subject = 'TabbieMath Registration';
        $mail->Body = $message;

        return $mail->Send();
	}
	
	/**
	 * Return safe user data.
	 * @param user id not required
	 * @return user object or false
	 */
	public static function user($id=0) {
		if ($id) 
			return User::model()->active()->findbyPk($id);
		else {
			if(Yii::app()->user->isGuest) {
				return false;
			} else {
				if (!self::$_user)
					self::$_user = User::model()->active()->findbyPk(Yii::app()->user->id);
				return self::$_user;
			}
		}
	}
	
	/**
	 * Return safe user data.
	 * @param user id not required
	 * @return user object or false
	 */
	public function users() {
		return User;
	}
}
